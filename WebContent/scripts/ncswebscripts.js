// method called on page load
function _initPage() {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof initPage == "function" ) {
		initPage();
	}

}

// method called on page unload
function _unloadPage() {
	// insert site-wide code here


	// you typically would run this after all site-wide
	// execution has processed, but it's really up to
	// the design of your site to when you may want to run this
	if( typeof unloadPage == "function" ) {
		unloadPage();
	}

}