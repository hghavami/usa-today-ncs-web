<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@taglib
	uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/accountMaintHome_mainBodyarea.jsp" type="page"></tiles:put>
	<tiles:put name="leftNavContentArea"
		value="/tilesContent/accountMaintHome_leftNavContentArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="documentTitle" value="Subscriber Account Maintenance" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/accountMaintHome_headarea.jsp" type="page"></tiles:put>
</tiles:insert>