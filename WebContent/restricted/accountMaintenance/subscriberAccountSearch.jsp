<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/restricted/accountMaintenance/subscriberAccountSearch_mainBodyArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="leftNavContentArea"
		value="/tilesContent/subscriberAccountSearch_leftNavArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="documentTitle" value="Email Search"
		direct="true" type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/subscriberAccountSearch_headerArea.jsp"
		type="page"></tiles:put>
</tiles:insert>