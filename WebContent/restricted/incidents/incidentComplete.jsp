<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@taglib
	uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/incidentComplete_mainBodyarea.jsp" type="page"></tiles:put>
	<tiles:put name="leftNavContentArea"
		value="/tilesContent/incidentComplete_leftNavContentArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="documentTitle"
		value="iQuEST - USA TODAY NCS - New Incident Complete" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/incidentComplete_headarea.jsp" type="page"></tiles:put>
</tiles:insert>