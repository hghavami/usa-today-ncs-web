<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%-- tpl:metadata --%><%-- jsf:pagecode language="java" location="/JavaSource/pagecode/restricted/incidents/KKKincidentComplete1.java" --%><%-- /jsf:pagecode --%><%-- /tpl:metadata --%><%@taglib
	uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/createIncident_mainBodyarea.jsp" type="page"></tiles:put>
	<tiles:put name="leftNavContentArea"
		value="/tilesContent/createIncident_leftNavContentArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="documentTitle"
		value="iQuEST - USA TODAY NCS - New Incident" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/createIncident_headarea.jsp" type="page"></tiles:put>
</tiles:insert>