<%-- tpl:insert page="/theme/JSP-C-blue_mod.jtpl" --%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:view locale="#{userLocaleHandler.locale}">
	<%-- tpl:insert page="/theme/HTML-C-02_blue.htpl" --%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta name="description" content="USA TODAY Customer Sevice Web Portal">
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<script language="JavaScript" src="/natcsweb/scripts/ncswebscripts.js"></script>
<LINK href="/natcsweb/theme/stylesheet.css" rel="stylesheet"
	type="text/css">
<LINK href="/natcsweb/theme/C_master_blue.css" rel="stylesheet"
	type="text/css">
<LINK href="/natcsweb/theme/C_stylesheet_blue.css" rel="stylesheet"
	type="text/css">

<%-- tpl:put name="htmlheadarea" --%>
<f:loadBundle basename="resourcesFile" var="labels" />
<jsp:useBean id="now" class="java.util.Date" scope="session"/>
			<%-- tpl:put name="headarea" --%>
			<TITLE>thankYou</TITLE>
			
			<%-- /tpl:put --%>
<script language="JavaScript"
				src="${pageContext.request.contextPath}/scripts/scriptaculous/prototype.js"></script>
			<script src="${pageContext.request.contextPath}/scripts/keepalive.js"></script>
<script src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="javascript">
	Event.observe(window, 'load', function() {
		startKeepAlive();
	});
</script>
			
<%-- /tpl:put --%>
</HEAD>
<BODY onLoad="_initPage();" onUnload="_unloadPage();">
<!-- start header area -->
<div class="topAreaBox"></div>
<div class="topAreaLogo"><IMG border="0" src="/natcsweb/theme/images/800x125_top.jpg" width="800"
	height="125" usemap="#800x125_top"><MAP name="800x125_top">
	<AREA shape="rect" href="/natcsweb/index.jsp" coords="19,17,265,113">
	<AREA shape="default" nohref="nohref">
</MAP>
</div>
<div class="topAreaLogoLine"><IMG border="0" src="/natcsweb/theme/images/800x20_art2.jpg" height="20" width="800"></div>
<!-- end header area -->

<!-- start header navigation bar -->
<div class="topNavBk"></div>
<%-- tpl:put name="topNavContentArea" --%> <DIV class="topNav"></DIV><div class="topAreaDatePlacement"><fmt:formatDate value="${now}" dateStyle="full"/></div><%-- /tpl:put --%>

<!-- end header navigation bar bar -->

<!-- start left-hand navigation -->
<table class="mainBox" border="0" cellpadding="0" width="100%" height="87%" cellspacing="0">
   <tbody>
      <tr>
         <td class="leftNavTD" align="left" valign="top">
            <div class="leftNavBox">
	         	<%-- tpl:put name="leftNavContentArea" --%>
						<hx:scriptCollector id="scriptCollector2">
							<h:form styleClass="form" id="formLogout">
								<%-- tpl:put name="LeftNavTopMostJSPContentArea" --%>LeftNavTopMostJSPContentArea<%-- /tpl:put --%>
								<div class="noDisplay" style="${user.divCss}">
									<h:outputFormat styleClass="outputFormat"
										id="formatWelcomeMessage" value="Welcome, {0}">
									<f:param name="displayName" value="#{user.userID}"></f:param>
								</h:outputFormat>
									<br>
								<h:commandLink styleClass="commandLink" id="linkLogout"
										action="#{pc_JSPCblue_mod.doLinkLogoutAction}">
										<h:outputText id="textLogoutLink" styleClass="outputText"
											value="#{labels.logoutLink}"></h:outputText>
									</h:commandLink>
									<HR width="95%" align="left">
								</div>
								<siteedit:navbar spec="/natcsweb/theme/nav_vertical_tree_left.jsp"
									targetlevel="1-5" onlychildren="true" navclass="leftNav" />
								<BR>
								<BR>
								<BR>
								<!-- If we add internatiolization
								<h:selectOneMenu styleClass="selectOneMenu" id="menui18nPref" value="#{userLocaleHandler.localeStr}">
									<f:selectItem itemValue="en" itemLabel="English" />
									<f:selectItem itemValue="es" itemLabel="Spanish" />
								</h:selectOneMenu><hx:commandExButton type="submit" value="Go"
									styleClass="commandExButton" id="buttonUpdateLangPref"
									action="#{pc_JSPCblue_mod.doButtonUpdateLangPrefAction}"></hx:commandExButton> -->
								<BR>
							</h:form>
						</hx:scriptCollector>
					<%-- /tpl:put --%>
				<BR>
				<HR width="95%" align="left">
				<B>Related Links:</B><BR>
				<A href="http://usatoday.com">USA TODAY.com</A><BR>
				<A href="https://service.usatoday.com/welcome.jsp">USA TODAY Subscription Services</A><BR>
				<A href="https://www.nexternal.com/myusatoday" target="_blank">USA TODAY Past Issues</A><BR>
				<BR>
				<BR>
				
			</div>
		</td>
<!-- end left-hand navigation -->

<!-- start main content area -->
			<td class="mainContentWideTD" align="left" valign="top" rowspan="2">
				<div class="mainContentWideBox">
					<%-- tpl:put name="MainAreaNavTrailContentArea" --%><!-- bread crumbs if we want em--><div class="navTrailLoc"><siteedit:navtrail start="[" end="]" target="home,parent,ancestor,self" separator="&gt;&gt;" spec="/natcsweb/theme/trail.jsp" topsibling="true"/></div><%-- /tpl:put --%>
					<a name="navskip"><IMG border="0" src="/natcsweb/theme/1x1.gif" width="1" height="1" alt=""></a>
					<%-- tpl:put name="bodyarea" --%><!-- <div class="mainBodyAreaLoc"> --><div class=""><%-- tpl:put name="jspbodyarea" --%>Default content of jspbodyarea
						<%-- /tpl:put --%></div><%-- /tpl:put --%>
				</div>
<!-- end main content area -->
           </td>
       </tr>
		<TR>
			<TD class="leftNavTD" align="left" valign="bottom">
			<div class="bottomNavAreaImage"></div>
			</TD>
		</TR>
		<TR>
			<TD align="left" valign="top" colspan="2" height="20"><img class="footerImage" src="/natcsweb/theme/images/800x20_art2.jpg" /></TD></TR>
		<TR>
			<TD align="center" valign="top"><DIV class="footer">
				</DIV>
			</TD>
			<TD class="mainContentWideTD" align="center" valign="top">
			<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0" width="675">
					<tbody>
						<tr>
						<td class="mainContentWideTD" align="center"><!-- someting in this table drastically impacts how this template works so leave it in for now --></td>
						</tr>
				</tbody></table>
				</div>
				
				</TD>
		</TR>
		<TR>
			<TD valign="top" colspan="2">
				<div class="footer">
				<table border="0" cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
						<td>
							<img src="/natcsweb/theme/1x1.gif" hspace="15">
						</td>
						<td class="mainContentWideTD" align="center">&copy; Copyright 2010 <A
							href="http://www.usatoday.com/marketing/credit.htm" target="_blank">USA
						TODAY</A>, a division of <A
						href="http://www.gannett.com" target="_blank">Gannett
						Co.</A> Inc. Access to this site restricted to authorized users only.</td>
						</tr>
				</tbody></table>
				</div>
			</TD>
		</TR>
	</tbody>
</table>
</BODY>
</HTML><%-- /tpl:insert --%>
</f:view>
<%-- /tpl:insert --%>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/restricted/newOrders/orders/ThankYou.java" --%><%-- /jsf:pagecode --%>