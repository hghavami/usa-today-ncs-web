<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/newOrdersProducts.jsp" type="page"></tiles:put>
	<tiles:put name="leftNavContentArea" value="" direct="true"
		type="string"></tiles:put>
	<tiles:put name="documentTitle" value="Product Selection" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea" value="" direct="true" type="string"></tiles:put>
</tiles:insert>