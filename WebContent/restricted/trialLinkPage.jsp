<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><tiles:insert
	definition="ncsWebRootTemplate">
	<tiles:put name="mainBodyarea"
		value="/tilesContent/restricted/trialLink_mainBodyArea.jsp"
		type="page"></tiles:put>
	<tiles:put name="leftNavContentArea"
		value="/tilesContent/restricted/trialLink_leftNavArea.jsp" type="page"></tiles:put>
	<tiles:put name="documentTitle" value="New Trial" direct="true"
		type="string"></tiles:put>
	<tiles:put name="headarea"
		value="/tilesContent/restricted/trialLink_headerArea.jsp" type="page"></tiles:put>
</tiles:insert>