

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="sitenav" type="com.ibm.etools.siteedit.sitelib.core.SiteNavBean" scope="request"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@page import="com.usatoday.ncs.web.handlers.UserHandler"%><html>
<%
	boolean includeNav = true;

	try {
	    UserHandler userh = (UserHandler)session.getAttribute("user");
			
		if (userh == null || userh.getUser() == null || !userh.getUser().isAuthenticated()) {
		    includeNav = false;
		}
	}
	catch (Exception e) {
		e.printStackTrace();
		includeNav = false;
	}	
 %>
 <head>
<title>vertical-text</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/theme/vertical-text.css">
</head>
<body>
<%
	if (includeNav) {
 %>
<c:forEach var="item" items="${sitenav.items}" begin="0" step="1" varStatus="status">
 <c:if test="${!status.first && !item.group}">
<b> - </b>
 </c:if>
 <c:choose>
  <c:when test="${item.group}">
<span class="CLASS_item_group"><c:out value='${item.label}' escapeXml='false'/></span><br/>
  </c:when>
  <c:otherwise>
<c:out value='<a href="${item.href}" class="CLASS_item_normal">${item.label}</a>' escapeXml='false'/><br/>
  </c:otherwise>
 </c:choose>
</c:forEach>
<%
}
else {
 %>
  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
 <%
 }
  %>
</body>
</html>
