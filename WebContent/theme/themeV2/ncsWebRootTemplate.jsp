<!DOCTYPE HTML>
<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/theme/themeV2/NcsWebRootTemplate.java" --%><%-- /jsf:pagecode --%><%@taglib uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<f:loadBundle basename="resourcesFile" var="labels" />
<jsp:useBean id="now" class="java.util.Date" scope="session"/>
<f:view locale="#{userLocaleHandler.locale}">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<link rel="stylesheet"
	href="/natcsweb/theme/themeV2/css/C_master_blue.css"
	type="text/css">
<link rel="stylesheet"
	href="/natcsweb/theme/themeV2/css/C_stylesheet_blue.css"
	type="text/css">
<title>
<tiles:getAsString name="documentTitle" />
</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script language="JavaScript"
				src="${pageContext.request.contextPath}/scripts/scriptaculous/prototype.js"></script>
			<script src="${pageContext.request.contextPath}/scripts/keepalive.js"></script>
<script src="${pageContext.request.contextPath}/scripts/prototypeUtils.js"></script>
<script language="javascript">
	Event.observe(window, 'load', function() {
		startKeepAlive();
		
		if( typeof window.initPage == "function" ) {
			window.initPage();
		}

	});
</script>
<f:subview id="subviewTemplateHeaderArea">
	<tiles:insert attribute="headarea" flush="false"></tiles:insert>
</f:subview>
<link rel="stylesheet" type="text/css"
	href="/natcsweb/theme/horizontal-trail.css">
<link rel="stylesheet" type="text/css"
	href="/natcsweb/theme/vertical-text.css">
</head>
	<body>
	<hx:scriptCollector id="scriptCollector1">
		<div class="topAreaBox"></div>
		<div class="topAreaLogo"><img border="0"
			src="/natcsweb/theme/images/800x125_top.jpg" width="800" height="125"
			usemap="#800x125_top"><map name="800x125_top">
			<area shape="rect" href="/natcsweb/index.ncs" coords="19,17,265,113">
			<area shape="default" nohref="nohref">
		</map></div>
		<div class="topAreaLogoLine"><img border="0"
			src="/natcsweb/theme/images/800x20_art2.jpg" height="20" width="800"></div>
		<!-- end header area -->

		<!-- start header navigation bar -->
		<div class="topNavBk"></div>
		<DIV class="topNav"></DIV><div class="topAreaDatePlacement"><fmt:formatDate value="${now}" dateStyle="full"/></div>
		<!-- start left-hand navigation -->
			<table class="mainBox" border="0" cellpadding="0" width="100%"
				height="87%" cellspacing="0">
				<tbody>
					<tr>
						<td class="leftNavTD" align="left" valign="top">
						<div class="leftNavBox">
							<f:subview
								id="subviewTemplateLeftNavContentArea">
								<tiles:insert attribute="leftNavContentArea" flush="false"></tiles:insert>
							</f:subview>
							<div class="noDisplay" style="${user.divCss}">
							<h:form styleClass="form" id="formTemplateLogout">	
							<span class="CLASS_item_group">							
							<h:outputFormat
								styleClass="outputFormat" id="formatWelcomeMessage"
								value="Welcome, {0}">
								<f:param name="displayName" value="#{user.userID}"></f:param>
							</h:outputFormat></span> <br>
	
							<h:commandLink styleClass="commandLink" id="linkTemplateLogoutLink" action="#{pc_NcsWebRootTemplate.doLinkTemplateLogoutLinkAction}">
								<h:outputText id="textLogoutLink" styleClass="outputText"
									value="#{labels.logoutLink}">
								</h:outputText>
							</h:commandLink>
							</h:form>
							<hr width="95%" align="left">
						</div>
						<siteedit:navbar target=""
							spec="vertical-text.jsp" targetlevel="1-5"></siteedit:navbar>
						<br>
						<hr width="95%" align="left">
						<b>Related Links:</b><br>
						<a href="http://usatoday.com">USA TODAY.com</a><br>
						<a href="https://service.usatoday.com/welcome.jsp">USA TODAY
						Subscription Services</a><br>
						<a href="https://www.nexternal.com/myusatoday" target="_blank">USA
						TODAY Past Issues</a><br>
						<br>
						<br>
						</div>
						</td>
						<!-- end left-hand navigation -->
						<!-- start main content area -->
						<td class="mainContentWideTD" align="left" valign="top"
							rowspan="2">
						<div class="mainContentWideBox"><siteedit:navtrail
							target="home,parent,ancestor,self" separator="&gt;"
							spec="horizontal-trail.jsp" /> <a name="navskip"><img
							border="0" src="/natcsweb/theme/1x1.gif" width="1" height="1"
							alt=""></a> <f:subview id="subviewMainBodyArea">
							<tiles:insert attribute="mainBodyarea" flush="false"></tiles:insert>
						</f:subview></div>
						<!-- end main content area --></td>
					</tr>
					<tr>
						<td class="leftNavTD" align="left" valign="bottom">
						<div class="bottomNavAreaImage"></div>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" colspan="2" height="20"><img
							class="footerImage" src="/natcsweb/theme/images/800x20_art2.jpg"></td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<div class="footer"></div>
						</td>
						<td class="mainContentWideTD" align="center" valign="top">
						<div class="footer">
						<table border="0" cellspacing="0" cellpadding="0" width="675">
							<tbody>
								<tr>
									<td class="mainContentWideTD" align="center"><!-- someting in this table drastically impacts how this template works so leave it in for now --></td>
								</tr>
							</tbody>
						</table>
						</div>

						</td>
					</tr>
					<tr>
						<td valign="top" colspan="2">
						<div class="footer">
						<table border="0" cellspacing="0" cellpadding="0">
							<tbody>
								<tr>
									<td><img src="/natcsweb/theme/1x1.gif" hspace="15"></td>
									<td class="mainContentWideTD" align="center">&copy;
									Copyright 2007 <a
										href="http://www.usatoday.com/marketing/credit.htm"
										target="_blank">USA TODAY</a>, a division of <a
										href="http://www.gannett.com" target="_blank">Gannett Co.</a>
									Inc. Access to this site restricted to authorized users only.</td>
								</tr>
							</tbody>
						</table>
						</div>
						</td>
					</tr>
				</tbody>
			</table>
	</hx:scriptCollector>
	</body>
</html>
</f:view>
