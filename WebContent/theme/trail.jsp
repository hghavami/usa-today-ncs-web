<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="sitenav" type="com.ibm.etools.siteedit.sitelib.core.SiteNavBean" scope="request"/>
<%@ page language="java" contentType="text/html; charset=SHIFT_JIS" pageEncoding="SHIFT_JIS" %>
<HTML>
<HEAD>
<TITLE>trail</TITLE>
</HEAD>
<BODY>
<DIV>
<c:if test="${!empty sitenav.items[1]}"><c:out value='${sitenav.start}' escapeXml='false'/></c:if>
<c:forEach var="item" items="${sitenav.items}" begin="1" step="1" varStatus="status">
<c:if test="${!status.first}"><c:out value='${sitenav.separator}' escapeXml='false'/></c:if>
<c:if test="${item.label != 'Login'}">
<c:out value='<A href="${item.href}" class="${sitenav.navclass}" style="${sitenav.navstyle}">${item.label}</A>' escapeXml='false'/>
</c:if>
</c:forEach>
<c:if test="${!empty sitenav.items[1]}"><c:out value='${sitenav.end}' escapeXml='false'/></c:if>
</DIV>
</BODY>
</HTML>
