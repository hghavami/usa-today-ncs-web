<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>ExactTargetProxy</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<%@ page import = "com.exacttarget.*,com.exacttarget.*,com.usatoday.businessObjects.util.mail.*,java.io.*" %>
<%
		
		//for testing.  verify your connection by viewing the console
		System.out.println("Generating Exact Target Email...Starting");
    	System.out.println("Begin checkExactTargetWebService...");
    	String returnCode = null;    	
     	
    	ExactTargetTriggeredSend etCheck = new ExactTargetTriggeredSend();
    	  	
    	if (etCheck.checkSystemStatus().equalsIgnoreCase("OK")){
    		returnCode = "OK";
    	} else {
    		returnCode = "Error";
    	}    	
    	
    	//a returnCode of OK means the web service api is available
    	System.out.println("returnCode =   " + returnCode );
    	
    	System.out.println("End checkExactTargetWebService...");
    	
    	
    /*  example triggered send
   
   		System.out.println("Generating Exact Target Email...Starting");
   		
    	returnCode = null;
    	
    	ExactTargetTriggeredSend etSend2 = new ExactTargetTriggeredSend();
    	
    	//email is the only field required by the system
    	etSend2.setEmailAddress("xxxx@usatoday.com");
    	
    	//attributes linked in the outgoing email are required by the email
    	etSend2.setAccntNumber("1373");
    	
    	//CustomerKey refers to an 'Interaction' defined on the Exact Target management site
    	//The "eEditionKey" interaction indicates which email is sent, and which list is written to
    	etSend2.setCustomerKey("eEditionKey");
    	
    	//attributes required for subscriber management process on Exact Target are
    	//will need to be added when available.    	
    	etSend2.setEmailDesc("eEdition New Starts");
    	etSend2.setCustomerKey("eEditionKey");
    	etSend2.setFirstName("Jack");
    	etSend2.setLastName("Kitten");    	
    	etSend2.setStreet("RiverCreek");
    	etSend2.setLink("www.tabby.com");
    	etSend2.setAddress1("Barn");
    	etSend2.setAddress2("Shed");   	
    	
    	if (etSend2.sendExactTargetEmail().equalsIgnoreCase("OK")){
    		returnCode = "OK";
    	}
    	System.out.println("return code..  " + returnCode);
    	
    	System.out.println("Exact Target Email Generated...Completed");
        
	*/

	
%>


</body>
</html>