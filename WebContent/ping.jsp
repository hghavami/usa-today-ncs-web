<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import="org.joda.time.DateTime"%><HTML>
<HEAD>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	//
	String serverName = "";
	String serverIP = "";
	String dateTimeStr = "";
		
	String clientIP = "";
	String clientName = "";
	try {
		java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();
		serverIP = serverAddress.getHostAddress();
		serverName = serverAddress.getHostName();
		
		clientIP = request.getRemoteAddr();
		
		clientName = request.getRemoteHost();
		
		DateTime dt = new DateTime();
		dateTimeStr = dt.toString("M/d/y  h:mm:ss a");
	}
	catch (Exception e) {
		e.printStackTrace();		
	}
%>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META http-equiv="Content-Style-Type" content="text/css">
<meta content="no-cache" http-equiv="Cache-Control"/>
<TITLE>iQuEST Application Ping</TITLE>
</HEAD>
<BODY>
<P></P>
<H2>Web application server is alive. iQuEST</H2>
<p><b>Your are hitting Server :</b>&nbsp;&nbsp;&nbsp;&nbsp; <%=serverName %> &nbsp;</p>
<b>Server Time:</b> <%=dateTimeStr %>
<BR>
<BR>
<BR>
<h3><FONT color="blue"> Client Information:</FONT></h3>
<b>Your IP:</b> <%=clientIP %><BR><br>
<b></b></BODY>
</HTML>
