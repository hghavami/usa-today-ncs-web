<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/EmailPasswordMaint_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="formEmailSearchForm">

			<h:messages styleClass="messages" id="messages1"></h:messages>
			<hx:panelSection styleClass="panelSection" id="sectionEmailSearch"
				initClosed="false"
				style="border-color: #ece9d8; border-style: solid; border-width: 1px">
				<f:facet name="closed">
					<hx:jspPanel id="jspPanel2">
						<hx:graphicImageEx id="imageEx2" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
						<h:outputText id="text2" styleClass="outputText"
							value="Find Email" style="padding-left: 5px"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<f:facet name="opened">
					<hx:jspPanel id="jspPanel1">
						<hx:graphicImageEx id="imageEx1" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
						<h:outputText id="text1" styleClass="outputText"
							value="Find Email" style="padding-left: 5px"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<h:panelGrid styleClass="panelGrid" id="gridEmailFilter" columns="2"
					cellpadding="2">

					<f:facet name="footer">
						<h:panelGrid styleClass="panelGrid" id="gridEmailFilterFooter1"
							columns="1">
							<hx:commandExButton type="submit"
								value="Search For Email Records" styleClass="commandExButton"
								id="buttonSubmitSearch"
								action="#{pc_EmailPasswordMaint.doButtonSubmitSearchAction}"></hx:commandExButton>
							<h:outputText styleClass="outputText" id="text26"
								value="* NOTE: This will search web records only. An email address may exist in One Call but not on the web database."
								escape="false"></h:outputText>
							<h:outputText styleClass="outputText" id="text27"
								value="Customer's may need to 'Set Up Online Account Access' if no email records are found."></h:outputText>
						</h:panelGrid>
					</f:facet>
					<h:outputText styleClass="outputText" id="textEmailLabel"
						value="Email Address Like: "></h:outputText>
					<h:inputText styleClass="inputText" id="textEmailAddressFilter"
						size="40" value="#{emailSearchFilterHandler.emailAddressFilter}"
						tabindex="1"
						title="Searches for emails containing the entered filter"></h:inputText>
					<h:outputText styleClass="outputText" id="textAccountNumberFilter"
						value="Account Number: "></h:outputText>
					<h:inputText styleClass="inputText" id="text4"
						value="#{emailSearchFilterHandler.accountNumber}" tabindex="2"
						title="Exact Match"></h:inputText>
					<h:outputText styleClass="outputText" id="textCheckTrialDB"
						value="Check Trial Users:"></h:outputText>
					<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
						id="checkboxCheckTrialTable"
						value="#{emailSearchFilterHandler.isCheckTrials}" tabindex="3"></h:selectBooleanCheckbox>
				</h:panelGrid>
			</hx:panelSection>
			<br>
			<br>
			<hx:panelSection styleClass="panelSection"
				id="sectionCustomerResultsSection" initClosed="false">
				<hx:dataTableEx border="0" cellpadding="2" cellspacing="2"
					columnClasses="columnClass1" headerClass="headerClass"
					footerClass="footerClass" rowClasses="rowClass1, rowClass3"
					styleClass="dataTableEx" id="tableExEmailSearchResults"
					var="varcustomerSearchResults"
					value="#{emailSearchHandler.customerSearchResults}"
					rendered="#{emailSearchHandler.hasCustomerSearchResults}">
					<hx:columnEx id="columnEx1" valign="top">
						<f:facet name="header">
							<h:outputText id="text3" styleClass="outputText"
								value="Email Address"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textEmailAddress"
							value="#{varcustomerSearchResults.emailAddress}"
							style="font-weight: bold"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx12" valign="top">
						<f:facet name="header">
							<h:outputText value="Password" styleClass="outputText"
								id="text24"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text25"
							value="#{varcustomerSearchResults.password}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx2">
						<f:facet name="header">
							<h:outputText value="Account Number" styleClass="outputText"
								id="text5"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textAcctNum"
							value="#{varcustomerSearchResults.accountNumber}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx3">
						<f:facet name="header">
							<h:outputText value="Publication Code" styleClass="outputText"
								id="text6"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textPubCode"
							value="#{varcustomerSearchResults.pubCode}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx6">
						<f:facet name="header">
							<h:outputText value="Start Date" styleClass="outputText"
								id="text8"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textStartDateText"
							value="#{varcustomerSearchResults.emailRecord.permStartDate}">
						</h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx4">
						<f:facet name="header">
							<h:outputText value="Gift Flag" styleClass="outputText"
								id="text7"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textGiftFlag"
							value="#{varcustomerSearchResults.giftRecordFlag}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx15">
						<f:facet name="header">
							<h:outputText value="DB Primary Key" styleClass="outputText"
								id="text30"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textSerNum"
							value="#{varcustomerSearchResults.emailRecord.serialNumber}"></h:outputText>
					</hx:columnEx>
				</hx:dataTableEx>
				<f:facet name="closed">
					<hx:jspPanel id="jspPanel6">
						<hx:graphicImageEx id="imageEx6" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
						<h:outputText id="text13" styleClass="outputText"
							value="Customer Results (#{emailSearchHandler.numberOfCustomerSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<f:facet name="opened">
					<hx:jspPanel id="jspPanel5">
						<hx:graphicImageEx id="imageEx5" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
						<h:outputText id="text12" styleClass="outputText"
							value="Customer Results (#{emailSearchHandler.numberOfCustomerSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
			</hx:panelSection>


			<br>
			<hx:panelSection styleClass="panelSection"
				id="sectionTrialCustomerSection" initClosed="false">
				<f:facet name="closed">
					<hx:jspPanel id="jspPanel4">
						<hx:graphicImageEx id="imageEx4" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
						<h:outputText id="text10" styleClass="outputText"
							value="Trial Customer Results  (#{emailSearchHandler.numberOfTrialSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<f:facet name="opened">
					<hx:jspPanel id="jspPanel3">
						<hx:graphicImageEx id="imageEx3" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
						<h:outputText id="text9" styleClass="outputText"
							value="Trial Customer Results  (#{emailSearchHandler.numberOfTrialSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
					columnClasses="columnClass1" headerClass="headerClass"
					footerClass="footerClass" rowClasses="rowClass1, rowClass3"
					styleClass="dataTableEx" id="tableExTrialSearchResults"
					rendered="#{emailSearchHandler.hasTrialSearchResults}"
					value="#{emailSearchHandler.trialSearchResults}"
					var="vartrialSearchResults">
					<hx:columnEx id="columnEx16">
						<hx:commandExRowAction id="rowActionTrialRecordDetail"
							action="#{pc_EmailPasswordMaint.doRowAction1Action}">
							<f:param
								value="#{vartrialSearchResults.trialCustomer.emailAddress}"
								name="emailAddress" id="param1"></f:param>
						</hx:commandExRowAction>
						<f:facet name="header"></f:facet>
					</hx:columnEx>
					<hx:columnEx id="columnEx5">
						<f:facet name="header">
							<h:outputText id="text11" styleClass="outputText"
								value="Email Address"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText"
							id="textTrialUserEmailAddress"
							value="#{vartrialSearchResults.trialCustomer.emailAddress}"
							style="font-weight: bold"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx7">
						<f:facet name="header">
							<h:outputText value="Password" styleClass="outputText"
								id="text14"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text19"
							value="#{vartrialSearchResults.trialCustomer.password}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx8">
						<f:facet name="header">
							<h:outputText styleClass="outputText"
								id="textTrialPartnerNameColumnHeader"></h:outputText>
						</f:facet>
						<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
							columnClasses="columnClass1" headerClass="headerClass"
							footerClass="footerClass" rowClasses="rowClass1, rowClass3"
							styleClass="dataTableEx" id="tableExSampleInnerTable"
							value="#{vartrialSearchResults.allTrials}" var="varallTrials"
							style="border-left-color: black; border-left-width: 1px; border-left-style: dotted">
							<hx:columnEx id="columnEx9">
								<f:facet name="header">
									<h:outputText id="text15" styleClass="outputText"
										value="Partner Name"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text20"
									value="#{varallTrials.trial.partner.partnerName}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx10" align="center">
								<f:facet name="header">
									<h:outputText value="Start Date" styleClass="outputText"
										id="text16"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text21"
									value="#{varallTrials.startDate}">
									<hx:convertDateTime dateStyle="medium" />
								</h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx11" align="center">
								<f:facet name="header">
									<h:outputText value="End Date" styleClass="outputText"
										id="text17"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text22"
									value="#{varallTrials.endDate}">
									<hx:convertDateTime dateStyle="medium" />
								</h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx13" align="center">
								<h:outputText styleClass="outputText" id="text23"
									value="#{varallTrials.trialDurationInDays}"></h:outputText>
								<f:facet name="header">
									<h:outputText value="Duration (days)" styleClass="outputText"
										id="text18"></h:outputText>
								</f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx14">
								<f:facet name="header">
									<h:outputText value="NCS Rep ID" styleClass="outputText"
										id="text28"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text29"
									value="#{varallTrials.trial.ncsRepID}"></h:outputText>
							</hx:columnEx>
						</hx:dataTableEx>
					</hx:columnEx>
				</hx:dataTableEx>
			</hx:panelSection>
		</h:form>
		<br>
		<br>
		<br>
		<br>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>