<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/CreateIncident_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" /><hx:scriptCollector
		id="scriptCollector1">
		<h:form styleClass="form" id="form1">
			<h:messages styleClass="messages" id="messages1"
				style="font-size: 14pt" globalOnly="false" layout="list"></h:messages>
			<table class="outlinedTable">
				<tbody>
					<tr bgcolor="#d3dbee">
						<th align="center" colspan="3" bgcolor="#d3dbee" nowrap="nowrap"><h:outputText
								styleClass="outputText_Med" id="textIncidentInfoLabel"
								value="#{labels.incidentHeader}"></h:outputText>
						</th>

					</tr>
					<tr>
						<th colspan="3" align="center"><h:outputText
								styleClass="outputText" id="text14"
								value='"*" Denotes A Required Field'></h:outputText>
						</th>
					</tr>
					<tr>
						<td align="right">Date Of Incident</td>
						<td style="width: 5px"></td>
						<td><h:inputText styleClass="inputText" id="textIncidentDate"
								size="35" value="#{newIncident.incidentDate}">
								<f:convertDateTime dateStyle="full" />
								<hx:inputHelperDatePicker />
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">* Service Category</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:selectOneMenu id="menu1"
								value="#{newIncident.incidentType}" styleClass="selectOneMenu"
								onchange="return func_2(this, event);">
								<f:selectItem itemValue="" itemLabel="" />
								<f:selectItem itemValue="Home Delivery"
									itemLabel="Home Delivery" />
								<f:selectItem itemLabel="Alternate Delivery"
									itemValue="Alternate Delivery" />
								<f:selectItem itemValue="Newsstand" itemLabel="Newsstand" />
								<f:selectItem itemValue="Rack" itemLabel="Rack" />
								<f:selectItem itemValue="Agent Inquiry"
									itemLabel="Agent Inquiry" />
								<f:selectItem itemValue="e-Newspaper" itemLabel="e-Newspaper" />
								<f:selectItem itemLabel="e-Leads" itemValue="e-Leads" />
								<f:selectItem itemValue="Education" itemLabel="Education" />
								<f:selectItem itemValue="Order Form Request"
									itemLabel="Order Form Request" />
								<f:selectItem itemValue="Invoice Request"
									itemLabel="Invoice Request" />
								<f:selectItem itemValue="W9 Form Request"
									itemLabel="W9 Form Request" />
								<f:selectItem itemValue="For National Customer Service"
									itemLabel="For National Customer Service" />
								<f:selectItem itemValue="Blue Chip Airline"
									itemLabel="Blue Chip Airline" />
								<f:selectItem itemValue="Blue Chip Hotel"
									itemLabel="Blue Chip Hotel" />
								<f:selectItem itemValue="Blue Chip One Time Delivery"
									itemLabel="Blue Chip One Time Delivery" />
								<f:selectItem itemValue="Blue Chip Other"
									itemLabel="Blue Chip Other" />
								<f:selectItem itemValue="Blue Chip Restaurant"
									itemLabel="Blue Chip Restaurant" />
								<f:selectItem itemValue="Other" itemLabel="Other" />
							</h:selectOneMenu>
						</td>
					</tr>
					<tr>
						<td align="right" valign="top">Request Type</td>
						<td style="width: 5px"></td>
						<td>
							<div id="divRTBlanks" class="requestTypesClass"
								style="display: inline">
								<h:selectOneMenu styleClass="selectOneMenu" id="menuBlanks">
									<f:selectItem itemValue="" itemLabel="" />
								</h:selectOneMenu>
							</div>
							<div id="divRTHomeDelivery" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentHomeDeliveryRequestType"
									value="#{incidentRequestType.homeDelivery}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Receiving but did not order"
										itemLabel="Receiving but did not order" />
									<f:selectItem
										itemValue="Moved but still delivering to old address"
										itemLabel="Moved but still delivering to old address" />
								</h:selectOneMenu>
							</div>
							<div id="divAlternateDelivery" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuAlternateDeliveryRequestType"
									value="#{incidentRequestType.alternateDelivery}">

									<f:selectItem id="selectItem2" />
									<f:selectItem itemLabel="Agent Inquiry"
										itemValue="Agent Inquiry" id="selectItem5" />
									<f:selectItem itemLabel="Newsstand" itemValue="Newsstand"
										id="selectItem6" />
									<f:selectItem itemLabel="Rack" itemValue="Rack"
										id="selectItem7" />
									<f:selectItem itemLabel="Other" itemValue="Other"
										id="selectItem8" />
								</h:selectOneMenu>
							</div>
							<div id="divRTNewsstand" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentNewsstandRequestType"
									value="#{incidentRequestType.newsstand}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Agent Complaint"
										itemLabel="Agent Complaint" />
									<f:selectItem itemValue="Billing Inquiry"
										itemLabel="Billing Inquiry" />
									<f:selectItem itemValue="Cancel Account"
										itemLabel="Cancel Account" />
									<f:selectItem itemValue="Convert to Rack"
										itemLabel="Convert to Rack" />
									<f:selectItem itemValue="Delivery Location"
										itemLabel="Delivery Location" />
									<f:selectItem itemValue="Draw Request" itemLabel="Draw Request" />
									<f:selectItem itemValue="Incorrect Number Delivered"
										itemLabel="Incorrect Number Delivered" />
									<f:selectItem itemValue="Late Delivery"
										itemLabel="Late Delivery" />
									<f:selectItem itemValue="Miscellaneous"
										itemLabel="Miscellaneous" />
									<f:selectItem itemValue="New Account" itemLabel="New Account" />
									<f:selectItem itemValue="No Paper Delivered"
										itemLabel="No Paper Delivered" />
									<f:selectItem itemValue="Paper Missing Section(s)"
										itemLabel="Paper Missing Section(s)" />
									<f:selectItem itemValue="Restart Service"
										itemLabel="Restart Service" />
									<f:selectItem itemValue="Returns Inquiry"
										itemLabel="Returns Inquiry" />
									<f:selectItem itemValue="Suspend Service"
										itemLabel="Suspend Service" />
								</h:selectOneMenu>
							</div>
							<div id="divRTRack" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentRack" value="#{incidentRequestType.rack}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Agent Complaint"
										itemLabel="Agent Complaint" />
									<f:selectItem itemValue="Coin Box Damaged"
										itemLabel="Coin Box Damaged" />
									<f:selectItem itemValue="Coins Being Returned"
										itemLabel="Coins Being Returned" />
									<f:selectItem itemValue="Convert to Newsstand"
										itemLabel="Convert to Newsstand" />
									<f:selectItem itemValue="Dirty, Please Clean"
										itemLabel="Dirty, Please Clean" />
									<f:selectItem itemValue="Dollars Will Not Work"
										itemLabel="Dollars Will Not Work" />
									<f:selectItem itemValue="Door Glass Broken"
										itemLabel="Door Glass Broken" />
									<f:selectItem itemValue="Door Torn Off"
										itemLabel="Door Torn Off" />
									<f:selectItem itemValue="Door Will Not Open"
										itemLabel="Door Will Not Open" />
									<f:selectItem itemValue="Draw Change Request"
										itemLabel="Draw Change Request" />
									<f:selectItem itemValue="Empty - Papers Sold Out"
										itemLabel="Empty - Papers Sold Out" />
									<f:selectItem itemValue="Empty - Papers Not Delivered"
										itemLabel="Empty - Papers Not Delivered" />
									<f:selectItem itemValue="Found/Recovered Rack"
										itemLabel="Found/Recovered Rack" />
									<f:selectItem itemValue="Graffiti on Rack"
										itemLabel="Graffiti on Rack" />
									<f:selectItem itemValue="Install Request"
										itemLabel="Install Request" />
									<f:selectItem itemValue="Knocked Over" itemLabel="Knocked Over" />
									<f:selectItem itemValue="Labelled Papers in Rack"
										itemLabel="Labelled Papers in Rack" />
									<f:selectItem itemValue="Late Delivery"
										itemLabel="Late Delivery" />
									<f:selectItem itemValue="Miscellanous" itemLabel="Miscellanous" />
									<f:selectItem itemValue="Rack Needs Paint"
										itemLabel="Rack Needs Paint" />
									<f:selectItem itemValue="No Racks in Town"
										itemLabel="No Racks in Town" />
									<f:selectItem itemValue="Not Anchored" itemLabel="Not Anchored" />
									<f:selectItem itemValue="Old And Worn Rack"
										itemLabel="Old And Worn Rack" />
									<f:selectItem itemValue="Old Papers in Rack"
										itemLabel="Old Papers in Rack" />
									<f:selectItem itemValue="Paper Missing Section(s)"
										itemLabel="Paper Missing Section(s)" />
									<f:selectItem itemValue="Rack Stolen" itemLabel="Rack Stolen" />
									<f:selectItem itemValue="Relocate Request"
										itemLabel="Relocate Request" />
									<f:selectItem itemValue="Removal Request"
										itemLabel="Removal Request" />
									<f:selectItem itemValue="Returns Not Collected"
										itemLabel="Returns Not Collected" />
									<f:selectItem itemValue="Rust Damaged" itemLabel="Rust Damaged" />
									<f:selectItem itemValue="Seasonal Start"
										itemLabel="Seasonal Start" />
									<f:selectItem itemValue="Seasonal Stop"
										itemLabel="Seasonal Stop" />
									<f:selectItem itemValue="Unauthorized Rack"
										itemLabel="Unauthorized Rack" />
									<f:selectItem itemValue="Vandalized Rack"
										itemLabel="Vandalized Rack" />
									<f:selectItem itemValue="Why Removed?" itemLabel="Why Removed?" />
									<f:selectItem itemValue="Will Not Accept Coins"
										itemLabel="Will Not Accept Coins" />
									<f:selectItem itemValue="Will Not Return Coins"
										itemLabel="Will Not Return Coins" />
								</h:selectOneMenu>
							</div>
							<div id="divRTAgentInquiry" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentAgentInquiry"
									value="#{incidentRequestType.agentInquiry}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Agent Complaint"
										itemLabel="Agent Complaint" />
									<f:selectItem itemValue="Papers Left in Dumpster"
										itemLabel="Papers Left in Dumpster" />
									<f:selectItem itemValue="Wants to be an Agent"
										itemLabel="Wants to be an Agent" />
								</h:selectOneMenu>
							</div>
							<div id="divEEdition" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuEEditionComments"
									value="#{incidentRequestType.EEdition}">
									<f:selectItem id="selectItem1" />
									<f:selectItem itemValue="Comments and Questions"
										itemLabel="Comments and Questions" />
									<f:selectItem itemValue="Access Request"
										itemLabel="Access Request" />
								</h:selectOneMenu>
							</div>
							<div id="divELeads" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuELeadsRequestType"
									value="#{incidentRequestType.eLeads}">

									<f:selectItem id="selectItem3" />
									<f:selectItem itemLabel="New Start" itemValue="New Start" />
									<f:selectItem itemLabel="Stop" itemValue="Stop" />
									<f:selectItem itemLabel="Change Request"
										itemValue="Change Request" />
								</h:selectOneMenu>
							</div>
							<div id="divRTEducation" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentEducation"
									value="#{incidentRequestType.education}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="3rd Complaint in 10 days"
										itemLabel="3rd Complaint in 10 days" />
									<f:selectItem itemValue="Agent Complaint"
										itemLabel="Agent Complaint" />
									<f:selectItem itemValue="Billing Inquiry"
										itemLabel="Billing Inquiry" />
									<f:selectItem itemValue="Billing Problem"
										itemLabel="Billing Problem" />
									<f:selectItem itemValue="Blown Papers" itemLabel="Blown Papers" />
									<f:selectItem itemValue="Cancel Account"
										itemLabel="Cancel Account" />
									<f:selectItem itemValue="Change Delivery Day"
										itemLabel="Change Delivery Day" />
									<f:selectItem itemValue="Change Address"
										itemLabel="Change Address" />
									<f:selectItem itemValue="Collegiate Readership Program Lead"
										itemLabel="Collegiate Readership Program Lead" />
									<f:selectItem itemValue="Delivery Location"
										itemLabel="Delivery Location" />
									<f:selectItem itemValue="Draw Request" itemLabel="Draw Request" />
									<f:selectItem itemValue="Editorial Comment"
										itemLabel="Editorial Comment" />
									<f:selectItem itemValue="Education Program Lead"
										itemLabel="Education Program Lead" />
									<f:selectItem itemValue="Failed to Start"
										itemLabel="Failed to Start" />
									<f:selectItem itemValue="Failed to Stop"
										itemLabel="Failed to Stop" />
									<f:selectItem itemValue="Incomplete Paper(s) Received"
										itemLabel="Incomplete Paper(s) Received" />
									<f:selectItem itemValue="Incorrect Delivery Location"
										itemLabel="Incorrect Delivery Location" />
									<f:selectItem itemValue="Incorrect Number of Papers Delivered"
										itemLabel="Incorrect Number of Papers Delivered" />
									<f:selectItem itemValue="Incorrect Start Date"
										itemLabel="Incorrect Start Date" />
									<f:selectItem itemValue="Late Delivery"
										itemLabel="Late Delivery" />
									<f:selectItem itemValue="Miscellaneous"
										itemLabel="Miscellaneous" />
									<f:selectItem itemValue="NIE Recipient Request"
										itemLabel="NIE Recipient Request" />
									<f:selectItem itemValue="No Delivery - Credit"
										itemLabel="No Delivery - Credit" />
									<f:selectItem itemValue="No Delivery - Redeliver"
										itemLabel="No Delivery - Redeliver" />
									<f:selectItem itemValue="No Teaching Supplement Received"
										itemLabel="No Teaching Supplement Received" />
									<f:selectItem itemValue="Old Papers Delivered"
										itemLabel="Old Papers Delivered" />
									<f:selectItem itemValue="Poor Service" itemLabel="Poor Service" />
									<f:selectItem itemValue="Production Quality Problem"
										itemLabel="Production Quality Problem" />
									<f:selectItem itemValue="Refund Balance"
										itemLabel="Refund Balance" />
									<f:selectItem itemValue="Supplements Inquiry"
										itemLabel="Supplements Inquiry" />
									<f:selectItem itemValue="Torn Paper" itemLabel="Torn Paper" />
									<f:selectItem itemValue="Vacation Date Information"
										itemLabel="Vacation Date Information" />
									<f:selectItem itemValue="Vacation Hold - Failed to Start"
										itemLabel="Vacation Hold - Failed to Start" />
									<f:selectItem itemValue="Vacation Hold - Failed to Stop"
										itemLabel="Vacation Hold - Failed to Stop" />
									<f:selectItem itemValue="Vacation Hold Not Processed"
										itemLabel="Vacation Hold Not Processed" />
									<f:selectItem itemValue="Welcome Kit Not Received"
										itemLabel="Welcome Kit Not Received" />
									<f:selectItem itemValue="Wet Paper" itemLabel="Wet Paper" />
								</h:selectOneMenu>
							</div>
							<div id="divRTOrderForm" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentOrderFormRequestType"
									value="#{incidentRequestType.orderFormRequest}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Send Order Form"
										itemLabel="Send Order Form" />
								</h:selectOneMenu>
							</div>
							<div id="divRTInvoice" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentInvoiceRequestType"
									value="#{incidentRequestType.invoice}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Send Invoice" itemLabel="Send Invoice" />
								</h:selectOneMenu>
							</div>


							<div id="divRTW9Request" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentW9RequestType"
									value="#{incidentRequestType.w9RequestForm}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Send W9 Form" itemLabel="Send W9 Form" />
								</h:selectOneMenu>
							</div>



							<div id="divRTNatCustSvc" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentNatCustSvc"
									value="#{incidentRequestType.natCust}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Sale Not Closed"
										itemLabel="Sale Not Closed" />
									<f:selectItem itemValue="Content Related Feedback"
										itemLabel="Content Related Feedback" />
									<f:selectItem itemValue="Caller Redirected"
										itemLabel="Caller Redirected" />
									<f:selectItem itemValue="Do Not Contact Requests"
										itemLabel="Do Not Contact Requests" />
									<f:selectItem itemValue="Other" itemLabel="Other" />
								</h:selectOneMenu>
							</div>


							<div id="divRTBlueChip" class="requestTypesClass"
								style="display: none">
								<h:selectOneMenu styleClass="selectOneMenu"
									id="menuIncidentBlueChip"
									value="#{incidentRequestType.blueChip}">
									<f:selectItem itemValue="" itemLabel="" />
									<f:selectItem itemValue="Agent Complaint"
										itemLabel="Agent Complaint" />
									<f:selectItem itemValue="Billiing Inquiry"
										itemLabel="Billiing Inquiry" />
									<f:selectItem itemValue="Cancel Account"
										itemLabel="Cancel Account" />
									<f:selectItem itemValue="Delivery Location"
										itemLabel="Delivery Location" />
									<f:selectItem itemValue="Draw Request" itemLabel="Draw Request" />
									<f:selectItem itemValue="Incorrect Number Delivered"
										itemLabel="Incorrect Number Delivered" />
									<f:selectItem itemValue="Label/Bag Request"
										itemLabel="Label/Bag Request" />
									<f:selectItem itemValue="Late Delivery"
										itemLabel="Late Delivery" />
									<f:selectItem itemValue="Miscellaneous"
										itemLabel="Miscellaneous" />
									<f:selectItem itemValue="Mislabelled Papers"
										itemLabel="Mislabelled Papers" />
									<f:selectItem itemValue="New Account" itemLabel="New Account" />
									<f:selectItem itemValue="No Papers Delivered"
										itemLabel="No Papers Delivered" />
									<f:selectItem itemValue="Old Papers Delivered"
										itemLabel="Old Papers Delivered" />
									<f:selectItem itemValue="Returns Inquiry"
										itemLabel="Returns Inquiry" />
									<f:selectItem itemLabel="Online Account Set-Up"
										itemValue="Online Account Set-Up" />
								</h:selectOneMenu>
							</div></td>
					</tr>
					<tr>
						<td align="right" valign="top">* Notes</td>
						<td style="width: 5px"></td>
						<td><h:inputTextarea id="textarea1"
								value="#{newIncident.notes}" styleClass="inputTextarea"
								cols="40" rows="3" required="true"
								validatorMessage="Maximum of 512 characters in Notes Field exceeded.">
								<f:validateLength maximum="512" minimum="1"></f:validateLength>
							</h:inputTextarea>
						</td>
					</tr>
					<tr>
						<td align="right" valign="top"></td>
						<td style="width: 5px"></td>
						<td><h:message styleClass="message" id="message3"
								for="textarea1"></h:message>
						</td>
					</tr>
					<tr>
						<td align="right">Refund Requested</td>
						<td style="width: 5px"></td>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tbody>
									<tr>
										<td nowrap="nowrap" align="center"><h:selectBooleanCheckbox
												styleClass="selectBooleanCheckbox" id="checkbox1"
												value="#{newIncident.refundRequest}"
												style="margin-left: 4px; margin-right: 4px"></h:selectBooleanCheckbox>
										</td>
										<td align="left"><h:outputText styleClass="outputText"
												id="textRefundCheckboxInfo" value="#{labels.refundMsg}"
												escape="false"></h:outputText>
										</td>
									</tr>
								</tbody>
							</table></td>
					</tr>
					<tr>
						<td align="right">* Incident Location Name</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text1"
								value="#{newIncident.incidentLocationName}"
								styleClass="inputText" size="40" maxlength="60" required="true"
								validatorMessage="Incident Location Name: Maximum of 60 chars exceeded">
								<f:validateLength maximum="60" minimum="1"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right"></td>
						<td style="width: 5px"></td>
						<td><h:message styleClass="message" id="message2" for="text1"></h:message>
						</td>
					</tr>
					<tr>
						<td align="right">Incident Address</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text2"
								value="#{newIncident.incidentAddress}" styleClass="inputText"
								size="40" maxlength="128"
								validatorMessage="IncidentAddress: Maximum of 128 chars exceeded">
								<f:validateLength maximum="128"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Incident City</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text3"
								value="#{newIncident.incidentCity}" styleClass="inputText"
								size="40" maxlength="128"
								validatorMessage="Incident City: Max of 128 chars exceeded.">
								<f:validateLength maximum="128"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Incident State</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:selectOneMenu id="menu2"
								value="#{newIncident.incidentState}" styleClass="selectOneMenu">
								<f:selectItem itemValue="" itemLabel="" />
								<f:selectItem itemValue="Alabama" itemLabel="Alabama" />
								<f:selectItem itemValue="Alaska" itemLabel="Alaska" />
								<f:selectItem itemValue="Arizona" itemLabel="Arizona" />
								<f:selectItem itemValue="Arkansas" itemLabel="Arkansas" />
								<f:selectItem itemValue="(AA) Armed Forces Americas"
									itemLabel="(AA) Armed Forces Americas" />
								<f:selectItem itemValue="(AE) Armed Forces Africa"
									itemLabel="(AE) Armed Forces Africa" />
								<f:selectItem itemValue="(AE) Armed Forces Canada"
									itemLabel="(AE) Armed Forces Canada" />
								<f:selectItem itemValue="(AE) Armed Forces Europe"
									itemLabel="(AE) Armed Forces Europe" />
								<f:selectItem itemValue="(AE) Armed Forces Middle East"
									itemLabel="(AE) Armed Forces Middle East" />
								<f:selectItem itemValue="(AP) Armed Forces Pacific"
									itemLabel="(AP) Armed Forces Pacific" />
								<f:selectItem itemValue="California" itemLabel="California" />
								<f:selectItem itemValue="Colorado" itemLabel="Colorado" />
								<f:selectItem itemValue="Connecticut" itemLabel="Connecticut" />
								<f:selectItem itemValue="Delaware" itemLabel="Delaware" />
								<f:selectItem itemValue="District of Columbia"
									itemLabel="District of Columbia" />
								<f:selectItem itemValue="Florida" itemLabel="Florida" />
								<f:selectItem itemValue="Georgia" itemLabel="Georgia" />
								<f:selectItem itemValue="Hawaii" itemLabel="Hawaii" />
								<f:selectItem itemValue="Idaho" itemLabel="Idaho" />
								<f:selectItem itemValue="Illinois" itemLabel="Illinois" />
								<f:selectItem itemValue="Indiana" itemLabel="Indiana" />
								<f:selectItem itemValue="Iowa" itemLabel="Iowa" />
								<f:selectItem itemValue="Kansas" itemLabel="Kansas" />
								<f:selectItem itemValue="Kentucky" itemLabel="Kentucky" />
								<f:selectItem itemValue="Louisiana" itemLabel="Louisiana" />
								<f:selectItem itemValue="Maine" itemLabel="Maine" />
								<f:selectItem itemValue="Maryland" itemLabel="Maryland" />
								<f:selectItem itemValue="Massachusetts"
									itemLabel="Massachusetts" />
								<f:selectItem itemValue="Michigan" itemLabel="Michigan" />
								<f:selectItem itemValue="Minnesota" itemLabel="Minnesota" />
								<f:selectItem itemValue="Mississippi" itemLabel="Mississippi" />
								<f:selectItem itemValue="Missouri" itemLabel="Missouri" />
								<f:selectItem itemValue="Montana" itemLabel="Montana" />
								<f:selectItem itemValue="Nebraska" itemLabel="Nebraska" />
								<f:selectItem itemValue="Nevada" itemLabel="Nevada" />
								<f:selectItem itemValue="New Hampshire"
									itemLabel="New Hampshire" />
								<f:selectItem itemValue="New Jersey" itemLabel="New Jersey" />
								<f:selectItem itemValue="New Mexico" itemLabel="New Mexico" />
								<f:selectItem itemValue="New York" itemLabel="New York" />
								<f:selectItem itemValue="North Carolina"
									itemLabel="North Carolina" />
								<f:selectItem itemValue="North Dakota" itemLabel="North Dakota" />
								<f:selectItem itemValue="Ohio" itemLabel="Ohio" />
								<f:selectItem itemValue="Oklahoma" itemLabel="Oklahoma" />
								<f:selectItem itemValue="Oregon" itemLabel="Oregon" />
								<f:selectItem itemValue="Pennsylvania" itemLabel="Pennsylvania" />
								<f:selectItem itemValue="Rhode Island" itemLabel="Rhode Island" />
								<f:selectItem itemValue="South Carolina"
									itemLabel="South Carolina" />
								<f:selectItem itemValue="South Dakota" itemLabel="South Dakota" />
								<f:selectItem itemValue="Tennessee" itemLabel="Tennessee" />
								<f:selectItem itemValue="Texas" itemLabel="Texas" />
								<f:selectItem itemValue="Utah" itemLabel="Utah" />
								<f:selectItem itemValue="Vermont" itemLabel="Vermont" />
								<f:selectItem itemValue="Virginia" itemLabel="Virginia" />
								<f:selectItem itemValue="Washington" itemLabel="Washington" />
								<f:selectItem itemValue="West Virginia"
									itemLabel="West Virginia" />
								<f:selectItem itemValue="Wisconsin" itemLabel="Wisconsin" />
								<f:selectItem itemValue="Wyoming" itemLabel="Wyoming" />
							</h:selectOneMenu>
						</td>
					</tr>
					<tr>
						<td align="right">Incident Zip</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text4"
								value="#{newIncident.incidentZip}" styleClass="inputText"
								size="7" maxlength="5"
								validatorMessage="Incident Zip: Invalid format or max of 5 chars  exceeded.">
								<f:validateLength maximum="5" minimum="5"></f:validateLength>
								<hx:validateConstraint regex="^[0-9]+$" />
							</h:inputText> <hx:outputLinkEx styleClass="outputLinkEx" target="_blank"
								value="http://zip4.usps.com/zip4/citytown.jsp" id="linkEx1">
								<h:outputText id="text13" styleClass="outputText"
									value="#{labels.uspsZipLookup}"></h:outputText>
							</hx:outputLinkEx>
						</td>
					</tr>
					<tr>
						<td align="right"></td>
						<td style="width: 5px"></td>
						<td><h:message styleClass="message" id="message1" for="text4"></h:message>
						</td>
					</tr>
					<tr>
						<th align="center" colspan="3" nowrap="nowrap" bgcolor="#d3dbee"><h:outputText
								styleClass="outputText_Med" id="textCustomerHeaderLabel"
								value="#{labels.customerHeader}"></h:outputText>
						</th>
					</tr>
					<tr>

						<td align="right" width="138">Click to copy Incident Address
							Information</td>
						<td style="width: 5px" width="2">&nbsp;</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;<h:selectBooleanCheckbox
								styleClass="selectBooleanCheckbox copyCheckbox" id="checkbox2"
								onclick="javascript:CopyIncidentAddr();"></h:selectBooleanCheckbox>
						</td>
					</tr>


					<tr>
						<td align="right">Customer First Name</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text5"
								value="#{newIncident.customerFirstName}" styleClass="inputText"
								size="40" maxlength="50"
								validatorMessage="Customer First Name: Max of 50 chars exceeded.">
								<f:validateLength maximum="50"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Last Name</td>
						<td style="width: 5px"></td>
						<td><h:inputText styleClass="inputText" id="textCustLastName"
								value="#{newIncident.customerLastName}" size="40" maxlength="50"
								validatorMessage="Customer Last Name: Max of 50 chars exceeded">
								<f:validateLength maximum="50"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Email</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text6"
								value="#{newIncident.customerEmail}" styleClass="inputText"
								size="40" maxlength="60"
								validatorMessage="Customer Email: Max of 60 chars exceeded.">
								<f:validateLength maximum="60"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Phone</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text7"
								value="#{newIncident.customerPhone}" styleClass="inputText"
								size="40" maxlength="20"
								validatorMessage="Customer Phone: Max of 15 chars exceeded.">
								<f:validateLength maximum="15"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Account Number</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text8"
								value="#{newIncident.customerAccountNumber}"
								styleClass="inputText" size="40" maxlength="9"
								validatorMessage="Customer Account Number: Max of 20 chars exceeded.">
								<f:validateLength maximum="20"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Address1</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text9"
								value="#{newIncident.customerAddress1}" styleClass="inputText"
								size="40" maxlength="128"
								validatorMessage="Customer Address 1: Max of 128 chars exceeded.">
								<f:validateLength maximum="128"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Address2</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text10"
								value="#{newIncident.customerAddress2}" styleClass="inputText"
								size="40" maxlength="128"
								validatorMessage="Customer Address 2: Max of 128 chars exceeded.">
								<f:validateLength maximum="128"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer City</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text11"
								value="#{newIncident.customerCity}" styleClass="inputText"
								size="40" maxlength="128"
								validatorMessage="Customer City: Max of 128 chars exceeded.">
								<f:validateLength maximum="128"></f:validateLength>
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="right">Customer State</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:selectOneMenu id="menu3"
								value="#{newIncident.customerState}" styleClass="selectOneMenu">
								<f:selectItem itemValue="" itemLabel="" />
								<f:selectItem itemValue="Alabama" itemLabel="Alabama" />
								<f:selectItem itemValue="Alaska" itemLabel="Alaska" />
								<f:selectItem itemValue="Arizona" itemLabel="Arizona" />
								<f:selectItem itemValue="Arkansas" itemLabel="Arkansas" />
								<f:selectItem itemValue="(AA) Armed Forces Americas"
									itemLabel="(AA) Armed Forces Americas" />
								<f:selectItem itemValue="(AE) Armed Forces Africa"
									itemLabel="(AE) Armed Forces Africa" />
								<f:selectItem itemValue="(AE) Armed Forces Canada"
									itemLabel="(AE) Armed Forces Canada" />
								<f:selectItem itemValue="(AE) Armed Forces Europe"
									itemLabel="(AE) Armed Forces Europe" />
								<f:selectItem itemValue="(AE) Armed Forces Middle East"
									itemLabel="(AE) Armed Forces Middle East" />
								<f:selectItem itemValue="(AP) Armed Forces Pacific"
									itemLabel="(AP) Armed Forces Pacific" />
								<f:selectItem itemValue="California" itemLabel="California" />
								<f:selectItem itemValue="Colorado" itemLabel="Colorado" />
								<f:selectItem itemValue="Connecticut" itemLabel="Connecticut" />
								<f:selectItem itemValue="Delaware" itemLabel="Delaware" />
								<f:selectItem itemValue="District of Columbia"
									itemLabel="District of Columbia" />
								<f:selectItem itemValue="Florida" itemLabel="Florida" />
								<f:selectItem itemValue="Georgia" itemLabel="Georgia" />
								<f:selectItem itemValue="Hawaii" itemLabel="Hawaii" />
								<f:selectItem itemValue="Idaho" itemLabel="Idaho" />
								<f:selectItem itemValue="Illinois" itemLabel="Illinois" />
								<f:selectItem itemValue="Indiana" itemLabel="Indiana" />
								<f:selectItem itemValue="Iowa" itemLabel="Iowa" />
								<f:selectItem itemValue="Kansas" itemLabel="Kansas" />
								<f:selectItem itemValue="Kentucky" itemLabel="Kentucky" />
								<f:selectItem itemValue="Louisiana" itemLabel="Louisiana" />
								<f:selectItem itemValue="Maine" itemLabel="Maine" />
								<f:selectItem itemValue="Maryland" itemLabel="Maryland" />
								<f:selectItem itemValue="Massachusetts"
									itemLabel="Massachusetts" />
								<f:selectItem itemValue="Michigan" itemLabel="Michigan" />
								<f:selectItem itemValue="Minnesota" itemLabel="Minnesota" />
								<f:selectItem itemValue="Mississippi" itemLabel="Mississippi" />
								<f:selectItem itemValue="Missouri" itemLabel="Missouri" />
								<f:selectItem itemValue="Montana" itemLabel="Montana" />
								<f:selectItem itemValue="Nebraska" itemLabel="Nebraska" />
								<f:selectItem itemValue="Nevada" itemLabel="Nevada" />
								<f:selectItem itemValue="New Hampshire"
									itemLabel="New Hampshire" />
								<f:selectItem itemValue="New Jersey" itemLabel="New Jersey" />
								<f:selectItem itemValue="New Mexico" itemLabel="New Mexico" />
								<f:selectItem itemValue="New York" itemLabel="New York" />
								<f:selectItem itemValue="North Carolina"
									itemLabel="North Carolina" />
								<f:selectItem itemValue="North Dakota" itemLabel="North Dakota" />
								<f:selectItem itemValue="Ohio" itemLabel="Ohio" />
								<f:selectItem itemValue="Oklahoma" itemLabel="Oklahoma" />
								<f:selectItem itemValue="Oregon" itemLabel="Oregon" />
								<f:selectItem itemValue="Pennsylvania" itemLabel="Pennsylvania" />
								<f:selectItem itemValue="Rhode Island" itemLabel="Rhode Island" />
								<f:selectItem itemValue="South Carolina"
									itemLabel="South Carolina" />
								<f:selectItem itemValue="South Dakota" itemLabel="South Dakota" />
								<f:selectItem itemValue="Tennessee" itemLabel="Tennessee" />
								<f:selectItem itemValue="Texas" itemLabel="Texas" />
								<f:selectItem itemValue="Utah" itemLabel="Utah" />
								<f:selectItem itemValue="Vermont" itemLabel="Vermont" />
								<f:selectItem itemValue="Virginia" itemLabel="Virginia" />
								<f:selectItem itemValue="Washington" itemLabel="Washington" />
								<f:selectItem itemValue="West Virginia"
									itemLabel="West Virginia" />
								<f:selectItem itemValue="Wisconsin" itemLabel="Wisconsin" />
								<f:selectItem itemValue="Wyoming" itemLabel="Wyoming" />
							</h:selectOneMenu>
						</td>
					</tr>
					<tr>
						<td align="right">Customer Zip</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:inputText id="text12"
								value="#{newIncident.customerZip}" styleClass="inputText"
								size="7" maxlength="5"
								validatorMessage="Customer Zip: Invalid format or max of 5 chars exceeded.">
								<f:validateLength maximum="5" minimum="5"></f:validateLength>
								<hx:validateConstraint regex="^[0-9]+$" />
							</h:inputText>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3"><hx:commandExButton
								id="buttonCreateIncident" styleClass="commandExButton"
								type="submit" value="#{labels.saveIncident}"
								action="#{pc_CreateIncident.doButtonCreateIncidentAction}"
								style="margin-top: 5px" onclick="return func_1(this, event);">
							</hx:commandExButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <hx:commandExButton type="submit"
								value="#{labels.cancel}" styleClass="commandExButton"
								id="buttonCancelNewIncident"
								action="#{pc_CreateIncident.doButtonCancelNewIncidentAction}"
								style="margin-top: 5px" immediate="true"></hx:commandExButton>
						</td>
					</tr>
				</tbody>
			</table>
			<br>

			<hx:inputHelperSetFocus target="menu1"></hx:inputHelperSetFocus>
			<h:inputHidden value="#{uniqueRequest.unique}"
				id="uniqueRequestValue">
				<f:convertNumber />
			</h:inputHidden>

		</h:form>
		<br>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>