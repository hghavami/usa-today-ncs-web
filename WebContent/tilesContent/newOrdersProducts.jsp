<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/NewOrdersProducts.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://www.ibm.com/jsf/html_extended"
	prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragmentProductSelectionFragment"><f:loadBundle basename="resourcesFile" var="labels" />
	<hx:scriptCollector id="scriptCollectorProductSelection">
		<h:panelGrid styleClass="panelGrid" id="gridProductGrid" columns="1"
			cellpadding="2" cellspacing="2">
			<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
				columnClasses="columnClass1" headerClass="headerClass"
				footerClass="footerClass" rowClasses="rowClass1, rowClass2"
				styleClass="dataTableEx" id="tableExAvailableProducts" var="varproducts" value="#{subscriptionProductsHandler.products}">
				<hx:columnEx id="columnExPubCode" valign="top">
					<f:facet name="header">
						<h:outputText id="textProductCodeLabel11" styleClass="outputText"
							value="Product<br>Code" escape="false"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="textPubCode"
						value="#{varproducts.productCode}" style="font-weight: bold"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx1" valign="top" nowrap="true">
					<f:facet name="header">
						<h:outputText value="Product Name" styleClass="outputText"
							id="text2"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="textProductName" value="#{varproducts.productName}" style="font-weight: bold"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx2">
					<f:facet name="header">
						<h:outputText value="Description" styleClass="outputText"
							id="text3"></h:outputText>
					</f:facet>
					<hx:panelSection styleClass="panelSection"
						id="sectionProductDetail" initClosed="false">
						<f:facet name="closed">
							<hx:jspPanel id="jspPanelProductDescriptoinClosed">
								<hx:graphicImageEx id="imageEx2" styleClass="graphicImageEx"
									align="middle"
									value="/natcsweb/theme/arrow-graphic-down-clear.gif"
									style="background-color: gray"></hx:graphicImageEx>
								<h:outputText id="textProductDescriptionClosedHeader"
									styleClass="outputText"
									value="#{varproducts.productDescription }"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<f:facet name="opened">
							<hx:jspPanel id="jspPanelProductDescriptoinOpen">
								<hx:graphicImageEx id="imageEx1" styleClass="graphicImageEx"
									align="middle"
									value="/natcsweb/theme/arrow-graphic-up-clear.gif"
									style="background-color: gray"></hx:graphicImageEx>
								<h:outputText id="text8" styleClass="outputText"
									value="#{varproducts.productDescription }"></h:outputText>
							</hx:jspPanel>
						</f:facet>
						<h:panelGrid styleClass="panelGrid" columnClasses="panelGridCol_TopAlign,panelGridCol_TopAlign"
							id="gridProductDetailGridOpened" columns="2" width="350" cellspacing="2">
							<h:outputText styleClass="outputText"
								id="textProductDetailedDescription"
								value="#{varproducts.productDetailedDescription}" style="font-weight: bold"></h:outputText>
							<hx:graphicImageEx styleClass="graphicImageEx"
								id="imageExProductImage" value="#{varproducts.imageUrl}"
								hspace="0" vspace="0" border="0"></hx:graphicImageEx>
						</h:panelGrid>
					</hx:panelSection>
				</hx:columnEx>
				<hx:columnEx id="columnEx3" valign="top" nowrap="true">
					<f:facet name="header">
						<h:outputText value="Order" styleClass="outputText" id="text4"></h:outputText>
					</f:facet>
					<hx:outputLinkEx value="#{varproducts.orderPageURL}"
						styleClass="outputLinkEx" id="linkExPlaceOrderLink" style="margin-top: 15px">
						<h:outputText id="textOrderLinkLabel" styleClass="outputText"
							value="Enter New Order"></h:outputText>
					</hx:outputLinkEx>
				</hx:columnEx>
			</hx:dataTableEx>
			<f:facet name="header">
				<h:panelGroup styleClass="panelGroup"
					id="groupProductGridHeaderGroup"
					style="width: 100%; text-align: left">
					<h:outputText styleClass="outputText" id="textProductGridHeader"
						value="Choose Product:"
						style="color: #004080; font-weight: bold; font-size: 14pt"></h:outputText>
					<h:messages styleClass="messages" id="messages1"></h:messages>
				</h:panelGroup>
			</f:facet>

		</h:panelGrid>
	</hx:scriptCollector>
</hx:viewFragment>