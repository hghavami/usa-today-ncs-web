<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/AccountMaintHome_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollector1">
		<br>
		<h:messages styleClass="messages" id="messages1"
			style="font-weight: bold; font-size: 12pt"></h:messages>
		<br>
		<h:form styleClass="form" id="form1">
			<h:panelGrid styleClass="panelGrid outlinedTable" id="gridLinksGrid"
				cellpadding="1" cellspacing="2" width="600">
				<f:facet name="header">
					<h:panelGroup styleClass="panelGroup"
						id="groupAccountLinksHeaderGroup">
						<h:panelGrid styleClass="panelGrid" id="gridAcctHeaderGrid"
							width="590" style="background-color: #d3dbee">
							<h:outputText styleClass="outputText" id="textAcctMngmtLabel"
								value="#{labels.accountServices}"
								style="background-color: #d3dbee; font-weight: bold; font-size: 12pt"></h:outputText>
						</h:panelGrid>

					</h:panelGroup>
				</f:facet>
				<hx:outputLinkEx styleClass="outputLinkEx"
					id="linkExRetrievePasswordLink"
					value="/idpassword/forgotpassword.jsp" target="_blank">
					<h:outputText id="textRetrievePwdTxt" styleClass="outputText"
						value="Retrieve Password / E-mail - Subscriber Portal Site" style="font-size: 12pt"></h:outputText>
				</hx:outputLinkEx>
				<hx:outputLinkEx styleClass="outputLinkEx" id="linkExChangeEmailPwd"
					target="_blank" value="/login/login.jsp">
					<h:outputText id="textChangeEmailTxt" styleClass="outputText"
						value="Change E-mail / Password - Subscriber Portal Site" style="font-size: 12pt"></h:outputText>
				</hx:outputLinkEx>
				<hx:outputLinkEx styleClass="outputLinkEx"
					id="linkExSetUpAccountLink" value="/firsttime/first_time.jsp"
					target="_blank">
					<h:outputText id="textAccountSetupTxt" styleClass="outputText"
						value="Online Account Setup (First Time Accessing)"
						style="font-size: 12pt"></h:outputText>
				</hx:outputLinkEx>
				<h:panelGrid styleClass="panelGrid" id="gridEmailPrefGrid"
					columns="2">
					<h:outputText styleClass="outputText" id="textEnterEmailLabel"
						value="Manage Daily Email Reminder Preferences:"
						style="font-weight: bold; font-size: 10pt"></h:outputText>
					<h:inputText styleClass="inputText" id="textEmailAddressForPref"
						size="40" value="enter email address here"></h:inputText>
					<h:outputText styleClass="outputText" id="textFillerTextEmailPref"></h:outputText>
					<hx:commandExButton type="button" value="Open Preference Window"
						styleClass="commandExButton" id="buttonEmailPreferences"
						onclick="return func_2(this, event);">
					</hx:commandExButton>
				</h:panelGrid>
			</h:panelGrid>
		</h:form>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>