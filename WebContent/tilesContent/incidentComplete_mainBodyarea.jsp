<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/IncidentComplete_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" /><hx:scriptCollector
		id="scriptCollector1">
		<h:outputFormat styleClass="outputFormat" id="formatIncidentID"
			value="#{labels.incidentIDLabel}"
			style="font-size: 14pt; font-weight: bold">
			<f:param name="newIncidentID" value="#{newIncident.idAsString}"></f:param>
		</h:outputFormat>
		<h:form styleClass="form" id="form1">
			<table class="outlinedTable" width="500">
				<tbody>
					<tr>
						<th align="center" colspan="3" bgcolor="#d3dbee" nowrap><h:outputText
							styleClass="outputText_Med" id="textIncidentInfoLabel"
							value="#{labels.incidentHeaderReview}"></h:outputText></th>
					</tr>
					<tr>
						<td align="right" nowrap>Customer Name :</td>
						<td style="width: 5px"></td>
						<td nowrap><h:outputText id="text9"
							value="#{newIncident.customerFirstName}" styleClass="outputText">
						</h:outputText>&nbsp;<h:outputText id="text10"
							value="#{newIncident.customerLastName}" styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Date Of Incident :</td>
						<td style="width: 5px"></td>
						<td nowrap><h:outputText id="text1"
							value="#{newIncident.incidentDate}" styleClass="outputText">
							<f:convertDateTime dateStyle="full" />
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident Type :</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:outputText id="text2"
							value="#{newIncident.incidentType}" styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident Request Type:</td>
						<td style="width: 5px"></td>
						<td><h:outputText styleClass="outputText" id="text11"
							value="#{newIncident.incidentRequestType}"></h:outputText></td>
					</tr>
					<tr>
						<td align="right">Notes :</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:outputText id="text3" value="#{newIncident.notes}"
							styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right" nowrap>Refund Requested:</td>
						<td style="width: 5px"></td>
						<td nowrap><h:selectBooleanCheckbox
							styleClass="selectBooleanCheckbox" id="checkbox1" readonly="true"
							value="#{newIncident.refundRequest}" disabled="true"></h:selectBooleanCheckbox></td>
					</tr>
					<tr>
						<td align="right" nowrap>Incident Location Name :</td>
						<td style="width: 5px">&nbsp;</td>
						<td nowrap><h:outputText id="text4"
							value="#{newIncident.incidentLocationName}"
							styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident Address :</td>
						<td style="width: 5px">&nbsp;</td>
						<td nowrap><h:outputText id="text5"
							value="#{newIncident.incidentAddress}" styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident City :</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:outputText id="text6"
							value="#{newIncident.incidentCity}" styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident State :</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:outputText id="text7"
							value="#{newIncident.incidentState}" styleClass="outputText">
						</h:outputText></td>
					</tr>
					<tr>
						<td align="right">Incident Zip :</td>
						<td style="width: 5px">&nbsp;</td>
						<td><h:outputText id="text8"
							value="#{newIncident.incidentZip}" styleClass="outputText">
						</h:outputText></td>
					</tr>
				</tbody>
			</table>
		</h:form>

		<h:form styleClass="form" id="form2">
			<hx:commandExButton type="submit" value="#{labels.createNewLabel}"
				styleClass="commandExButton" id="buttonCreateNewIncident"
				action="#{pc_IncidentComplete.doButtonCreateNewIncidentAction}"
				style="margin: 5px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px; margin-top: 5px"></hx:commandExButton>
		</h:form>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>