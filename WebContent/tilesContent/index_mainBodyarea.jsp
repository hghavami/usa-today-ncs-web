<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/Index_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<c:if test="${user.user.authenticated}">
		<c:redirect url="/restricted/home.ncs"></c:redirect>
	</c:if>
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="loginForm">
			<table width="400" border="0" cellpadding="0" cellspacing="2">
				<tbody>
					<tr>
						<td align="center" colspan="2"><h:messages
							styleClass="messages" id="messages1"></h:messages></td>
					</tr>
					<tr>
						<td align="right">*<h:outputText styleClass="outputText"
							id="textUserIDLabel" value="#{labels.userIDPrompt}:"></h:outputText></td>
						<td align="left"><h:inputText styleClass="inputText"
							id="textUserIDInput" value="#{user.userID}" required="true"
							tabindex="1"></h:inputText></td>
					</tr>
					<tr>
						<td align="right">*<h:outputText styleClass="outputText"
							id="textSiteIDLabel" value="#{labels.siteIDPrompt}:"></h:outputText></td>
						<td><h:inputText styleClass="inputText" id="textSiteIDInput"
							value="#{user.siteID}" size="20" required="true" tabindex="2"></h:inputText></td>
					</tr>
					<tr>
						<td align="right">*<h:outputText styleClass="outputText"
							id="textPasswordLabel" value="#{labels.password}:"></h:outputText></td>
						<td><h:inputSecret styleClass="inputSecret"
							id="secretPasswordInput" size="20" tabindex="3"></h:inputSecret></td>
					</tr>
					<tr>
						<td></td>
						<td><hx:commandExButton type="submit"
							value="#{labels.loginButtonLabel}" styleClass="commandExButton"
							id="buttonAuthenticate" tabindex="4"
							action="#{pc_Index.doButtonAuthenticateAction}"></hx:commandExButton></td>
					</tr>
				</tbody>
			</table>
			<br>



			<br>
			<br>
			<br>
			<hx:inputHelperSetFocus target="textUserIDInput"></hx:inputHelperSetFocus>
			<hx:inputHelperKeybind key="Enter" id="inputHelperKeybind1"
				target="buttonAuthenticate" targetAction="click" />
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>