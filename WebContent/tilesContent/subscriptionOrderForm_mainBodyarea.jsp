<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/SubscriptionOrderForm_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%><%@taglib
	uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragmentOrderForm1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollectorMainOrderEntryCollector"
		preRender="#{pc_SubscriptionOrderForm.onPageLoadBegin}">
	<script
		src="${pageContext.request.contextPath}/scripts/scriptaculous/scriptaculous.js"
		type="text/javascript"></script>
	<script language="JavaScript"
		src="${pageContext.request.contextPath}/scripts/pubDateValidator.js"></script>
	<script type="text/javascript">
function updateDeliveryInfoPanelClass() {
    var checkbox = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:checkboxIsBillDifferentFromDelSelector');
	// gift information
    var panelEmailGiftInformation = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:gridBillingAddress');
    var panelOneTimeBillGift = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:gridEZPAYOptionsGridGift');    
	var panelOneTimeBill = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:gridEZPAYOptionsGrid');
   	panelEmailGiftInformation.hide();    	
   	panelOneTimeBillGift.hide();
   	panelOneTimeBill.hide();

    if (checkbox.checked) {
    	panelEmailGiftInformation.show();    	
    	panelOneTimeBillGift.show();
    	panelOneTimeBill.hide();
    }
    else {
    	panelEmailGiftInformation.hide();
    	panelOneTimeBillGift.hide();
    	panelOneTimeBill.show();
    }
}

function updatePaymentPanelClass(billMeValue) {

   	//var panelCreditCardInformation = document.getElementById("formOrderEntryForm:gridPaymentInformationGrid");
   	var panelCreditCardInformation = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:gridPaymentInformationGrid');
	if (billMeValue == "B") {
    	panelCreditCardInformation.hide();
    }
    else {
    	panelCreditCardInformation.show();
    }	
}

function initPage() {
	
	updateDeliveryInfoPanelClass();
	updatePaymentPanelClass("${newSubscriptionOrderHandler.paymentMethod}");
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDeliveryInfoPanelClass();
}
function func_3(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	window.open('/faq/utfaq.jsp#section6','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
}
// func_4 - enable/disable the credit card payment
function func_4(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updatePaymentPanelClass(thisObj.value);
	  
}


function func_revalidateDelMethod(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
//var delMethodVar = $('formOrderEntryForm:textDeliveryMethodTextDeterminedValue');
var delMethodVar = $('subviewMainBodyArea:viewFragmentOrderForm1:formOrderEntryForm:textDeliveryMethodTextDeterminedValue');
delMethodVar.innerHTML = "Not Determined";
}
</script>
<link rel="stylesheet" type="text/css" title="Style"
		href="${pageContext.request.contextPath}/theme/tabpanel.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/restricted/newOrders/orders/stylesheet.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/restricted/newOrders/orders/basic.css" type="text/css">
			
		<h:form styleClass="form" id="formOrderEntryForm">
			<h:panelGrid styleClass="panelGrid" id="gridMainBodyGrid"
				cellpadding="0" cellspacing="0" columns="1" width="745">


				<h:panelGrid styleClass="panelGrid" id="gridProductInfoGrid"
					columns="2" style="border-color: black; border-width: thin"
					width="400">
					<f:facet name="header">
						<h:panelGroup styleClass="panelGroup" id="groupprodinfo1">
							<h:panelGrid styleClass="panelGrid"
								id="gridProductInfoHeaderGrid33" columns="2">
								<h:outputText styleClass="outputText" id="textProdInfoText5"
									value="Product Information"
									style="font-weight: bold; font-size: 14pt"></h:outputText>
							</h:panelGrid>
						</h:panelGroup>
					</f:facet>
					<h:outputText styleClass="outputText" id="textPubCodeTextLabel"
						value="Product:" style="font-weight: bold"></h:outputText>
					<h:outputText styleClass="outputText" id="textProductCode"
						value="#{currentOfferHandler.currentOffer.pubCode}"></h:outputText>
					<h:outputText styleClass="outputText" id="textProductName"
						value="Product Name: " style="font-weight: bold"></h:outputText>
					<h:outputText styleClass="outputText" id="textOfferProdName4"
						value="#{currentOfferHandler.currentOffer.subscriptionProduct.name}"></h:outputText>
					<h:outputText styleClass="outputText" id="textProductTermSelected"
						value="Term: " style="font-weight: bold"></h:outputText>
					<h:outputText styleClass="outputText" id="textOfferSelectedTerm4" value="#{newSubscriptionOrderHandler.selectedTermHandler.fullTermDescription}"></h:outputText>

				</h:panelGrid>
				<hx:jspPanel id="jspPanelFormPanel">
					<div id="pageContentEEOrderEntryLeft">
					<div id="orderFormBox" style="width: 480px"><hx:panelLayout
						styleClass="panelLayout" id="layoutPageLayout">
						<f:facet name="body">
							<h:panelGroup styleClass="panelGroup" id="groupMainBodyPanel"
								style="margin-bottom: 5px;">
								<!-- insert cart items here -->

								<h:panelGrid styleClass="panelGrid" id="gridDeliveryInformation"
									width="475" columns="1">
									<hx:panelFormBox helpPosition="under" labelPosition="left"
										styleClass="panelFormBox" id="formBoxDeliveryInformation"
										label="#{labels.emailAddressRecipientLabel}" widthLabel="150">

										<hx:formItem styleClass="formItem"
											id="formItemDeliveryFirstName" label="#{labels.nameLabel}"
											errorText="#{labels.requiredFieldMsg}">

											<h:inputText styleClass="inputText"
												id="textDeliveryFirstName" size="10" tabindex="20"
												value="#{newSubscriptionOrderHandler.deliveryFirstName}"
												maxlength="10" required="true"
												requiredMessage="First Name is Required"
												validatorMessage="First Name is Not Valid">
												<f:validateLength minimum="1" maximum="10"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText" id="textDeliveryLastName"
												size="15" style="margin-left: 5px" tabindex="21"
												value="#{newSubscriptionOrderHandler.deliveryLastName}"
												maxlength="15" required="true"
												requiredMessage="Last Name is Required"
												validatorMessage="Last Name is Not Valid">
												<f:validateLength minimum="1" maximum="15"></f:validateLength>
											</h:inputText>

										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemDeliveryCompanyName"
											label="#{labels.companyNameLabel}">
											<h:inputText styleClass="inputText"
												id="textDeliveryCompanyName" size="32" tabindex="22"
												value="#{newSubscriptionOrderHandler.deliveryCompanyName}"
												maxlength="28">
												<f:validateLength maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>

										<hx:formItem styleClass="formItem"
											id="formItemDeliveryAddress1" label="#{labels.address}"
											errorText="#{labels.requiredFieldMsg}">
											<h:inputText styleClass="inputText"
												id="textDeliveryAddress1Text" size="28" tabindex="23"
												value="#{newSubscriptionOrderHandler.deliveryAddress1}"
												maxlength="28" required="true"
												requiredMessage="Address is Required"
												validatorMessage="Address is Not Valid"
												onchange="func_revalidateDelMethod(this, event);">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText" id="textDeliveryAptSuite"
												tabindex="24" size="6" style="margin-left: 5px"
												value="#{newSubscriptionOrderHandler.deliveryAptSuite}"
												maxlength="28"
												onchange="func_revalidateDelMethod(this, event);"></h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemDeliveryAddress2"
											label="#{labels.additionalAddress}" rendered="true">
											<h:inputText styleClass="inputText" id="textDeliveryAddress2"
												size="30" tabindex="25"
												value="#{newSubscriptionOrderHandler.deliveryAddress2}"
												maxlength="28">
												<f:validateLength minimum="0" maximum="28"></f:validateLength>
											</h:inputText>
											<h:outputText escape="false" id="textAddrLearnMore"
												value="#{currentOfferHandler.addressLearnMoreHTML}"
												style="margin-left: 10px; color: #00529b; cursor: pointer"
												styleClass="requestLink"></h:outputText>


										</hx:formItem>

										<hx:formItem styleClass="formItem" id="formItemDeliveryCity"
											label="#{labels.cityLabel}"
											errorText="#{labels.requiredFieldMsg}">
											<h:inputText styleClass="inputText" id="textDeliveryCity"
												size="30" tabindex="26"
												onchange="func_revalidateDelMethod(this, event);"
												value="#{newSubscriptionOrderHandler.deliveryCity}"
												maxlength="28" required="true"
												requiredMessage="City is Required"
												validatorMessage="City is Not Valid">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>

										<hx:formItem styleClass="formItem" id="formItemDeliveryState"
											label="#{labels.StateZipLabel}"
											errorText="#{labels.requiredFieldMsg}">
											<h:selectOneMenu styleClass="selectOneMenu"
												id="menuDeliveryState" tabindex="27"
												onchange="func_revalidateDelMethod(this, event);"
												requiredMessage="State is Required"
												validatorMessage="State is Not Valid" required="true"
												value="#{newSubscriptionOrderHandler.deliveryState}">
												<f:selectItem itemValue="NONE" itemLabel="" />
												<f:selectItem itemValue="AL" itemLabel="Alabama" />
												<f:selectItem itemValue="AK" itemLabel="Alaska" />
												<f:selectItem itemValue="AZ" itemLabel="Arizona" />
												<f:selectItem itemValue="AR" itemLabel="Arkansas" />
												<f:selectItem itemValue="AA"
													itemLabel="(AA) Armed Forces Americas" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Africa" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Canada" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Europe" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Middle East" />
												<f:selectItem itemValue="AP"
													itemLabel="(AP) Armed Forces Pacific" />
												<f:selectItem itemValue="CA" itemLabel="California" />
												<f:selectItem itemValue="CO" itemLabel="Colorado" />
												<f:selectItem itemValue="CT" itemLabel="Connecticut" />
												<f:selectItem itemValue="DE" itemLabel="Delaware" />
												<f:selectItem itemValue="DC"
													itemLabel="District of Columbia" />
												<f:selectItem itemValue="FL" itemLabel="Florida" />
												<f:selectItem itemValue="GA" itemLabel="Georgia" />
												<f:selectItem itemValue="HI" itemLabel="Hawaii" />
												<f:selectItem itemValue="ID" itemLabel="Idaho" />
												<f:selectItem itemValue="IL" itemLabel="Illinois" />
												<f:selectItem itemValue="IN" itemLabel="Indiana" />
												<f:selectItem itemValue="IA" itemLabel="Iowa" />
												<f:selectItem itemValue="KS" itemLabel="Kansas" />
												<f:selectItem itemValue="KY" itemLabel="Kentucky" />
												<f:selectItem itemValue="LA" itemLabel="Louisiana" />
												<f:selectItem itemValue="ME" itemLabel="Maine" />
												<f:selectItem itemValue="MD" itemLabel="Maryland" />
												<f:selectItem itemValue="MA" itemLabel="Massachusetts" />
												<f:selectItem itemValue="MI" itemLabel="Michigan" />
												<f:selectItem itemValue="MN" itemLabel="Minnesota" />
												<f:selectItem itemValue="MS" itemLabel="Mississippi" />
												<f:selectItem itemValue="MO" itemLabel="Missouri" />
												<f:selectItem itemValue="MT" itemLabel="Montana" />
												<f:selectItem itemValue="NE" itemLabel="Nebraska" />
												<f:selectItem itemValue="NV" itemLabel="Nevada" />
												<f:selectItem itemValue="NH" itemLabel="New Hampshire" />
												<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
												<f:selectItem itemValue="NM" itemLabel="New Mexico" />
												<f:selectItem itemValue="NY" itemLabel="New York" />
												<f:selectItem itemValue="NC" itemLabel="North Carolina" />
												<f:selectItem itemValue="ND" itemLabel="North Dakota" />
												<f:selectItem itemValue="OH" itemLabel="Ohio" />
												<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
												<f:selectItem itemValue="OR" itemLabel="Oregon" />
												<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
												<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
												<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
												<f:selectItem itemValue="SC" itemLabel="South Carolina" />
												<f:selectItem itemValue="SD" itemLabel="South Dakota" />
												<f:selectItem itemValue="TN" itemLabel="Tennessee" />
												<f:selectItem itemValue="TX" itemLabel="Texas" />
												<f:selectItem itemValue="UT" itemLabel="Utah" />
												<f:selectItem itemValue="VT" itemLabel="Vermont" />
												<f:selectItem itemValue="VA" itemLabel="Virginia" />
												<f:selectItem itemValue="WA" itemLabel="Washington" />
												<f:selectItem itemValue="WV" itemLabel="West Virginia" />
												<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
												<f:selectItem itemValue="WY" itemLabel="Wyoming" />
											</h:selectOneMenu>
											<h:inputText styleClass="inputText" id="textDeliveryZip"
												onchange="func_revalidateDelMethod(this, event);"
												tabindex="28" size="6" style="margin-left: 5px"
												value="#{newSubscriptionOrderHandler.deliveryZipCode}"
												maxlength="5" required="true"
												requiredMessage="Zip is Required"
												validatorMessage="Zip is Not Valid">
												<f:validateLength minimum="5" maximum="5"></f:validateLength>
												<hx:validateConstraint regex="^[0-9]+$" />
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem" id="formItemDeliveryPhone"
											label="#{labels.phoneLabel}*"
											errorText="#{labels.invalidPhoneFormat}">
											<h:inputText styleClass="inputText"
												id="textDeliveryPhoneAreaCode" size="4" tabindex="29"
												maxlength="3"
												value="#{newSubscriptionOrderHandler.deliveryPhoneAreaCode}"
												required="true" requiredMessage="Phone Number is Required"
												validatorMessage="Phone Number is Not Valid">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist4" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textDeliveryPhoneExchange" size="4"
												style="margin-left: 4px" tabindex="30" maxlength="3"
												readonly="false"
												value="#{newSubscriptionOrderHandler.deliveryPhoneExchange}"
												required="true" requiredMessage="Phone Number is Required"
												validatorMessage="Phone Number is Not Valid">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist5" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textDeliveryPhoneExtension" style="margin-left: 4px"
												size="5" tabindex="31"
												value="#{newSubscriptionOrderHandler.deliveryPhoneExtension}"
												maxlength="4" required="true"
												requiredMessage="Phone Number is Required"
												validatorMessage="Phone Number is Not Valid">

												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="4" maximum="4"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemDeliveryWorkPhone"
											errorText="#{labels.invalidPhoneFormat}"
											label="#{labels.workPhoneLabel}">
											<h:inputText styleClass="inputText"
												id="textDeliveryWorkPhoneAreaCode" size="4" tabindex="32"
												maxlength="3"
												value="#{newSubscriptionOrderHandler.deliveryWorkPhoneAreaCode}"
												validatorMessage="Work Phone Number is Not Valid">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist44" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textDeliveryWorkPhoneExchange" size="4"
												style="margin-left: 4px" tabindex="33" maxlength="3"
												readonly="false"
												value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExchange}"
												validatorMessage="Work Phone Number is Not Valid">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist45" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textDeliveryWorkPhoneExtension" style="margin-left: 4px"
												size="5" tabindex="34"
												value="#{newSubscriptionOrderHandler.deliveryWorkPhoneExtension}"
												maxlength="4"
												validatorMessage="Work Phone Number is Not Valid">

												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="4" maximum="4"></f:validateLength>
											</h:inputText>
										</hx:formItem>


										<hx:formItem styleClass="formItem" id="formItemEmailAddress"
											label="#{labels.Email}"
											errorText="#{labels.requiredFieldMsg}">
											<h:inputText styleClass="inputText"
												id="textEmailAddressRecipient" size="30" tabindex="35"
												title="Delivery Email Address"
												value="#{newSubscriptionOrderHandler.deliveryEmailAddress}"
												maxlength="60"
												validatorMessage="Delivery Email Address is Not Valid">
												<f:validateLength minimum="6" maximum="60"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemEmailAddressConfirm"
											label="#{labels.confirmPurchaserEmailAddressLabel}"
											errorText="#{labels.requiredFieldMsg}">

											<h:inputText styleClass="inputText"
												id="textEmailAddressConfirmRecipient" size="30"
												tabindex="36" title="Confirm Delivery Email Address"
												value="#{newSubscriptionOrderHandler.deliveryEmailAddressConfirmation}"
												maxlength="60"
												validatorMessage="Confirmation Email Address is Not Valid">
												<f:validateLength minimum="6" maximum="60"></f:validateLength>
											</h:inputText>
										</hx:formItem>

										<f:facet name="bottom">
											<h:panelGrid styleClass="panelGrid"
												columnClasses="panelGridColLeftAlign,panelGridColRightAlign"
												id="gridDeliveryAddressBottomFacet" width="100%" columns="2"
												style="top: -20px; line-height: 15px">
												<f:facet name="footer">
													<h:panelGrid styleClass="panelGrid"
														columnClasses="panelGridColCenterAlign200Wide, panelGridColLeftAlignV2"
														id="gridDeliveryInformationPanelFooterGrid" columns="2"
														rendered="#{currentOfferHandler.showDeliveryMethodCheck}"
														cellpadding="1" cellspacing="1">

														<hx:commandExButton type="button"
															styleClass="commandExButton"
															id="buttonGetDeliveryMethodButton" immediate="true"
															value="#{labels.DelMethodButtonLabel}"
															style="width: 200px" tabindex="37"
															title="Determine Delivery Method"
															action="#{pc_SubscriptionOrderForm.doButtonGetDeliveryMethodButtonAction}">
															<hx:behavior event="onclick" id="behavior2" behaviorAction="get" targetAction="gridDelMethodResultsGrid"></hx:behavior>
														</hx:commandExButton>
														<h:panelGrid styleClass="panelGrid"
															columnClasses="panelGridColTopLeftAlignV2"
															id="gridDelMethodResultsGrid" columns="1" width="100%"
															cellpadding="0" cellspacing="0">
															<h:outputText styleClass="outputText"
																id="textDeliveryMethod"
																style="font-weight: bold; font-size: 9pt"
																value="#{labels.DelMethod}:">
															</h:outputText>
															<h:outputText styleClass="outputText"
																id="textDeliveryMethodTextDeterminedValue"
																value="#{newSubscriptionOrderHandler.deliveryMethodText}"
																style="color: navy; font-weight: bold; font-weight: bold; font-size: 9pt"></h:outputText>
														</h:panelGrid>
														<hx:ajaxRefreshSubmit target="gridDelMethodResultsGrid"
															id="ajaxRefreshSubmit1"></hx:ajaxRefreshSubmit>


													</h:panelGrid>

												</f:facet>
												<h:outputText styleClass="outputText" id="text1"
													value="* Required Fields"
													style="font-family: Arial; font-weight: bold; font-size: 7pt"></h:outputText>



											</h:panelGrid>
										</f:facet>
										<f:facet name="top">
											<h:panelGrid styleClass="panelGrid"
												id="gridDeliveryMethodInfoGrid"
												rendered="#{currentOfferHandler.showDeliveryMethodCheck}"
												width="100%" style="text-align: right" cellpadding="0"
												columns="1">
												<hx:outputLinkEx value="javascript:;"
													styleClass="outputLinkEx" id="linkExDelMethodCheckHelpLink">
													<h:outputText styleClass="outputLinkEx"
														id="textDelMethodHelpText" value="#{labels.DelMethodHelp}"
														style="font-family: Arial; font-size: 7pt; cursor: pointer"></h:outputText>
												</hx:outputLinkEx>
											</h:panelGrid>
										</f:facet>
									</hx:panelFormBox>
								</h:panelGrid>
								<h:panelGrid id="panelGridBillingDifferentThanDelGrid"
									width="475" columns="1">
									<hx:panelFormBox helpPosition="over" labelPosition="right"
										styleClass="panelFormBox"
										id="formBoxBillDifferentFromDelSelectionFormBox"
										style="margin-left: 140px">

										<hx:formItem styleClass="formItem"
											id="formItemBillDifferentFromDelSelector"
											label="#{labels.billingDiffersFromDeliveryLabel}">
											<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
												id="checkboxIsBillDifferentFromDelSelector"
												onclick="return func_2(this, event);"
												title="Check if Billing Address is Different Than Delivery Address"
												tabindex="41" style="margin-right: 5px"
												value="#{newSubscriptionOrderHandler.billingDifferentThanDelivery}">
												<hx:behavior event="onselect" id="behavior1"></hx:behavior>
											</h:selectBooleanCheckbox>
										</hx:formItem>
									</hx:panelFormBox>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid" id="gridBillingAddress"
									width="475" columns="1">
									<hx:panelFormBox helpPosition="under" labelPosition="left"
										styleClass="panelFormBox" id="formBoxBillingAddress"
										label="#{labels.BillingAddressLabel}" widthLabel="150">
										<hx:formItem styleClass="formItem"
											id="formItemPayersEmailAddress"
											label="#{labels.emailAddressPayerLabel}"
											infoText="#{labels.BillingEmailInfoMsg}">

											<h:inputText styleClass="inputText"
												id="textPurchaserEmailAddress" size="30" tabindex="50"
												value="#{newSubscriptionOrderHandler.billingEmailAddress}"
												maxlength="60" required="false">
												<f:validateLength minimum="1" maximum="60"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemBillingFirstName" label="#{labels.nameLabel}">

											<h:inputText styleClass="inputText" id="textBillingFirstName"
												title="First Name" tabindex="52"
												value="#{newSubscriptionOrderHandler.billingFirstName}"
												maxlength="10" size="15">
												<f:validateLength minimum="1" maximum="10"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText" id="textBillingLastName"
												size="15" tabindex="53" style="margin-left: 5px"
												title="Last Name"
												value="#{newSubscriptionOrderHandler.billingLastName}"
												maxlength="15">
												<f:validateLength minimum="1" maximum="15"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemBillingCompanyName"
											label="#{labels.companyNameLabel}">
											<h:inputText styleClass="inputText"
												id="textBillingCompanyName" size="32" tabindex="54"
												value="#{newSubscriptionOrderHandler.billingCompanyName}"
												maxlength="28">
												<f:validateLength maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>

										<hx:formItem styleClass="formItem"
											id="formItemBillingAddress1" label="#{labels.address}">
											<h:inputText styleClass="inputText" id="textBillingAddress1"
												size="28" tabindex="55" title="Address 1"
												value="#{newSubscriptionOrderHandler.billingAddress1}"
												maxlength="28">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText" id="textBillingAptSuite"
												size="6" style="margin-left: 5px" title="Apt/Suite"
												tabindex="56"
												value="#{newSubscriptionOrderHandler.billingAptSuite}"
												maxlength="28">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemBillingAddress2" label="#{labels.address2Label}"
											rendered="false">
											<h:inputText styleClass="inputText" id="textBillingAddress2"
												size="30" tabindex="57"
												value="#{newSubscriptionOrderHandler.billingAddress2}"
												maxlength="28">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>

										<hx:formItem styleClass="formItem" id="formItemBillingCity"
											label="#{labels.cityLabel}">
											<h:inputText styleClass="inputText" id="textBillingCity"
												size="30" tabindex="58" title="City"
												value="#{newSubscriptionOrderHandler.billingCity}"
												maxlength="28">
												<f:validateLength minimum="1" maximum="28"></f:validateLength>
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem" id="formItemBillingState"
											label="#{labels.StateZipLabel}">
											<h:selectOneMenu styleClass="selectOneMenu"
												id="menuBillingState" tabindex="58" title="State"
												value="#{newSubscriptionOrderHandler.billingState}">
												<f:selectItem itemValue="" itemLabel="" />
												<f:selectItem itemValue="AL" itemLabel="Alabama" />
												<f:selectItem itemValue="AK" itemLabel="Alaska" />
												<f:selectItem itemValue="AZ" itemLabel="Arizona" />
												<f:selectItem itemValue="AR" itemLabel="Arkansas" />
												<f:selectItem itemValue="AA"
													itemLabel="(AA) Armed Forces Americas" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Africa" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Canada" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Europe" />
												<f:selectItem itemValue="AE"
													itemLabel="(AE) Armed Forces Middle East" />
												<f:selectItem itemValue="AP"
													itemLabel="(AP) Armed Forces Pacific" />
												<f:selectItem itemValue="CA" itemLabel="California" />
												<f:selectItem itemValue="CO" itemLabel="Colorado" />
												<f:selectItem itemValue="CT" itemLabel="Connecticut" />
												<f:selectItem itemValue="DE" itemLabel="Delaware" />
												<f:selectItem itemValue="DC"
													itemLabel="District of Columbia" />
												<f:selectItem itemValue="FL" itemLabel="Florida" />
												<f:selectItem itemValue="GA" itemLabel="Georgia" />
												<f:selectItem itemValue="HI" itemLabel="Hawaii" />
												<f:selectItem itemValue="ID" itemLabel="Idaho" />
												<f:selectItem itemValue="IL" itemLabel="Illinois" />
												<f:selectItem itemValue="IN" itemLabel="Indiana" />
												<f:selectItem itemValue="IA" itemLabel="Iowa" />
												<f:selectItem itemValue="KS" itemLabel="Kansas" />
												<f:selectItem itemValue="KY" itemLabel="Kentucky" />
												<f:selectItem itemValue="LA" itemLabel="Louisiana" />
												<f:selectItem itemValue="ME" itemLabel="Maine" />
												<f:selectItem itemValue="MD" itemLabel="Maryland" />
												<f:selectItem itemValue="MA" itemLabel="Massachusetts" />
												<f:selectItem itemValue="MI" itemLabel="Michigan" />
												<f:selectItem itemValue="MN" itemLabel="Minnesota" />
												<f:selectItem itemValue="MS" itemLabel="Mississippi" />
												<f:selectItem itemValue="MO" itemLabel="Missouri" />
												<f:selectItem itemValue="MT" itemLabel="Montana" />
												<f:selectItem itemValue="NE" itemLabel="Nebraska" />
												<f:selectItem itemValue="NV" itemLabel="Nevada" />
												<f:selectItem itemValue="NH" itemLabel="New Hampshire" />
												<f:selectItem itemValue="NJ" itemLabel="New Jersey" />
												<f:selectItem itemValue="NM" itemLabel="New Mexico" />
												<f:selectItem itemValue="NY" itemLabel="New York" />
												<f:selectItem itemValue="NC" itemLabel="North Carolina" />
												<f:selectItem itemValue="ND" itemLabel="North Dakota" />
												<f:selectItem itemValue="OH" itemLabel="Ohio" />
												<f:selectItem itemValue="OK" itemLabel="Oklahoma" />
												<f:selectItem itemValue="OR" itemLabel="Oregon" />
												<f:selectItem itemValue="PA" itemLabel="Pennsylvania" />
												<f:selectItem itemValue="PR" itemLabel="Puerto Rico" />
												<f:selectItem itemValue="RI" itemLabel="Rhode Island" />
												<f:selectItem itemValue="SC" itemLabel="South Carolina" />
												<f:selectItem itemValue="SD" itemLabel="South Dakota" />
												<f:selectItem itemValue="TN" itemLabel="Tennessee" />
												<f:selectItem itemValue="TX" itemLabel="Texas" />
												<f:selectItem itemValue="UT" itemLabel="Utah" />
												<f:selectItem itemValue="VT" itemLabel="Vermont" />
												<f:selectItem itemValue="VA" itemLabel="Virginia" />
												<f:selectItem itemValue="WA" itemLabel="Washington" />
												<f:selectItem itemValue="WV" itemLabel="West Virginia" />
												<f:selectItem itemValue="WI" itemLabel="Wisconsin" />
												<f:selectItem itemValue="WY" itemLabel="Wyoming" />
											</h:selectOneMenu>
											<h:inputText styleClass="inputText" id="textBillingZipCode"
												size="6" style="margin-left: 5px" title="Zip Code"
												tabindex="59"
												value="#{newSubscriptionOrderHandler.billingZipCode}"
												maxlength="5">
												<f:validateLength minimum="5" maximum="5"></f:validateLength>
												<hx:validateConstraint regex="^[0-9]+$" />
											</h:inputText>
										</hx:formItem>
										<hx:formItem styleClass="formItem"
											id="formItemBillingTelephone"
											label="#{labels.billPhoneLabel}">
											<h:inputText styleClass="inputText"
												id="textBillingPhoneAreaCode" size="4" tabindex="60"
												maxlength="3" title="Billing Phone Area Code"
												value="#{newSubscriptionOrderHandler.billingPhoneAreaCode}">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist2" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textBillingPhoneExchange" size="4"
												style="margin-left: 4px" tabindex="61" maxlength="3"
												title="Billing Phone Exchange"
												value="#{newSubscriptionOrderHandler.billingPhoneExchange}">

												<hx:inputHelperAssist errorClass="inputText_Error"
													autoTab="true" id="assist3" />
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="3" maximum="3"></f:validateLength>
											</h:inputText>
											<h:inputText styleClass="inputText"
												id="textBillingPhoneExtension" style="margin-left: 4px"
												size="5" tabindex="62" title="Billing Phone Last 4 Digits"
												value="#{newSubscriptionOrderHandler.billingPhoneExtension}"
												maxlength="4">

												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="4" maximum="4"></f:validateLength>
											</h:inputText>
										</hx:formItem>
									</hx:panelFormBox>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid"
									id="gridPaymentInformationGridLabelGrid" width="475"
									columns="1">
									<hx:panelFormBox helpPosition="over" labelPosition="left"
										styleClass="panelFormBox" id="formBoxPaymentHeaderFormBox"
										label="#{labels.paymentInfo}" widthLabel="135">
										<f:facet name="top">

										</f:facet>
										<hx:formItem styleClass="formItem" id="formItemBillMeFormItem"
											rendered="#{currentOfferHandler.isShowPaymentOptionSelector}"
											label="#{labels.paymentMethod} :" style="font-weight: bold">
											<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
												enabledClass="selectOneRadio_Enabled"
												styleClass="selectOneRadio" id="radioPaymentMethod"
												layout="lineDirection"
												value="#{newSubscriptionOrderHandler.paymentMethod}"
												onclick="return func_4(this, event);">
												<f:selectItem itemLabel="Credit Card" itemValue="CC"
													id="selectItem1" />
												<f:selectItem itemLabel="Bill Me Later" itemValue="B"
													id="selectItem2" />
											</h:selectOneRadio>
										</hx:formItem>
									</hx:panelFormBox>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid" id="gridForceBillMeInfoGrid"
									width="475" columns="1"
									rendered="#{currentOfferHandler.isForceBillMe}">
									<h:outputText styleClass="outputText" id="textForceBillMeText"
										style="font-weight: bold; font-size: 11pt" escape="false"
										value="#{labels.ForceBillMeInformation}"></h:outputText>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid"
									id="gridPaymentInformationGrid" width="475" columns="1">
									<f:facet name="footer">
										<h:panelGrid styleClass="panelGrid"
											id="gridPaymentGridBottomFacet" width="475" columns="1"
											bgcolor="#96abd8">
											<hx:graphicImageEx styleClass="graphicImageEx"
												id="imageExSpacerImage" value="/theme/img/JSF_1x1.gif"
												width="1" height="1" align="right" hspace="0" border="0"></hx:graphicImageEx>
										</h:panelGrid>
									</f:facet>
									<hx:panelFormBox helpPosition="right" labelPosition="left"
										styleClass="panelFormBox" id="formBoxPaymentInfo"
										widthLabel="150"
										rendered="#{currentOfferHandler.isShowCreditCardPaymentOptionData}">
										<hx:formItem styleClass="formItem"
											id="formItemCreditCardNumber" label="#{labels.CCNum}:">
											<h:inputText styleClass="inputText" id="textCreditCardNumber"
												size="30" tabindex="70" title="Credit Card Number"
												value="#{newSubscriptionOrderHandler.creditCardNumber}"
												maxlength="16"
												validatorMessage="Credit Card Number is Not Valid">
												<hx:validateConstraint regex="^[0-9]+$" />
												<f:validateLength minimum="12" maximum="16"></f:validateLength>
												<hx:inputHelperAssist errorClass="inputText_Error"
													validation="true" id="assist6" />
											</h:inputText>
										</hx:formItem>

										<hx:formItem styleClass="formItem"
											id="formItemCCExpirationDate" label="#{labels.CCExpiration}:">
											<h:selectOneMenu styleClass="selectOneMenu"
												id="menuCCExpireMonth" tabindex="72"
												title="Credit Card Expiration Month"
												value="#{newSubscriptionOrderHandler.creditCardExpirationMonth}">
												<f:selectItem itemLabel="Month" itemValue="-1"
													id="selectItem16" />
												<f:selectItem itemLabel="01" itemValue="01" id="selectItem3" />
												<f:selectItem itemLabel="02" itemValue="02" id="selectItem4" />
												<f:selectItem itemLabel="03" itemValue="03" id="selectItem5" />
												<f:selectItem itemLabel="04" itemValue="04" id="selectItem6" />
												<f:selectItem itemLabel="05" itemValue="05" id="selectItem7" />
												<f:selectItem itemLabel="06" itemValue="06" id="selectItem8" />
												<f:selectItem itemLabel="07" itemValue="07" id="selectItem9" />
												<f:selectItem itemLabel="08" itemValue="08"
													id="selectItem10" />
												<f:selectItem itemLabel="09" itemValue="09"
													id="selectItem11" />
												<f:selectItem itemLabel="10" itemValue="10"
													id="selectItem12" />
												<f:selectItem itemLabel="11" itemValue="11"
													id="selectItem13" />
												<f:selectItem itemLabel="12" itemValue="12"
													id="selectItem14" />
											</h:selectOneMenu>
											<h:selectOneMenu styleClass="selectOneMenu"
												id="menuCCExpireYear" style="margin-left: 5px" tabindex="73"
												title="Credit Card Expiration Year"
												value="#{newSubscriptionOrderHandler.creditCardExpirationYear}">
												<f:selectItems
													value="#{currentOfferHandler.creditCardExpirationYears}"
													id="selectItems1" />
											</h:selectOneMenu>
										</hx:formItem>



										<f:facet name="top">
											<h:panelGrid id="panelGridCreditCardImageGrid" width="85%"
												columns="1" cellspacing="1" cellpadding="1"
												style="text-align: center">

												<hx:jspPanel id="jspPanelCreditCardImages">

													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExAmEx1"
														value="/natcsweb/images/american_express_logo.gif"
														hspace="5" border="0" width="42" height="26"
														rendered="#{currentOfferHandler.isTakesAMEX}"></hx:graphicImageEx>
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExDiscover1"
														value="/natcsweb/images/discover_logo.gif" hspace="5"
														border="0" width="42" height="26"
														rendered="#{currentOfferHandler.isTakesDiscover}"></hx:graphicImageEx>
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExMasterCard1"
														value="/natcsweb/images/mc_logo.gif" hspace="5" border="0"
														width="42" height="26"
														rendered="#{currentOfferHandler.isTakesMasterCard}"></hx:graphicImageEx>
													<hx:graphicImageEx styleClass="graphicImageEx"
														id="imageExVisaLogo1"
														value="/natcsweb/images/visa_logo.gif" hspace="5"
														border="0" width="42" height="26"
														rendered="#{currentOfferHandler.isTakesVisa}"></hx:graphicImageEx>

												</hx:jspPanel>
											</h:panelGrid>
										</f:facet>
									</hx:panelFormBox>
									<h:panelGrid styleClass="panelGrid" id="gridEZPAYOptionsGrid"
										width="475" columns="1">
										<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
											enabledClass="selectOneRadio_Enabled"
											styleClass="selectOneRadio" id="radioRenewalOptions"
											layout="pageDirection"
											value="#{newSubscriptionOrderHandler.renewalMethod}"
											style="font-weight: bold; font-size: 11pt" tabindex="80"
											rendered="#{currentOfferHandler.isNonEZPayRate}">
											<f:selectItem
												itemLabel="Please sign me up for EZ-PAY. Save Time, Save Money.**"
												itemValue="AUTOPAY_PLAN" id="selectItem17" />
											<f:selectItem
												itemLabel="Please charge my credit card for this subscription term only."
												itemValue="ONE_TIME_BILL" id="selectItem18" />
										</h:selectOneRadio>
										<h:outputText styleClass="outputText" id="textCCAgreement"
											value="#{labels.CreditCardAgreementFinePrint}" escape="false"
											style="font-size: 10pt"></h:outputText>
										<h:outputText styleClass="outputText" id="textEZPayConfirm"
											value="#{labels.EzpayFinePrint}"
											rendered="#{not(currentOfferHandler.isNonEZPayRate)}"
											escape="false"></h:outputText>
										<h:outputText styleClass="outputText" id="textEzPayInfo"
											style="width: 470px; font-size: 10pt"
											value="#{labels.EzpayFinePrint}"
											rendered="#{currentOfferHandler.isNonEZPayRate}"></h:outputText>
									</h:panelGrid>
									<h:panelGrid styleClass="panelGrid"
										id="gridEZPAYOptionsGridGift" columns="1" width="475">
										<h:selectOneRadio disabledClass="selectOneRadio_Disabled"
											enabledClass="selectOneRadio_Enabled"
											styleClass="selectOneRadio" id="radioRenewalOptionsGift"
											layout="pageDirection"
											style="font-weight: bold; font-size: 11pt" tabindex="81"
											value="#{newSubscriptionOrderHandler.renewalMethodGift}"
											rendered="#{currentOfferHandler.isNonEZPayRate}">
											<f:selectItem
												itemLabel="Please sign me up for EZ-PAY. Save Time, Save Money.**"
												itemValue="AUTOPAY_PLAN" id="selectItem19" />
											<f:selectItem
												itemLabel="Please charge my credit card for this subscription term only."
												itemValue="ONE_TIME_BILL" id="selectItem20" />
											<f:selectItem itemValue="ONE_TIME_BILL_GIFT"
												id="selectItem15"
												itemLabel="* Please charge my credit card for this subscription term only and send future invoices to the gift recipient." />
										</h:selectOneRadio>
										<h:outputText styleClass="outputText" id="textCCAgreement2"
											value="#{labels.CreditCardAgreementFinePrint}" escape="false"
											style="font-size: 10pt"></h:outputText>
										<h:outputText styleClass="outputText" id="textEzPayInfoGift"
											style="width: 470px; font-size: 10pt"
											value="#{labels.EzpayFinePrint}" escape="false"></h:outputText>
									</h:panelGrid>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid" frame="box"
									id="gridTrialDisclaimerGrid" columns="1" width="460"
									rendered="#{trialPartnerHandler.isShowDisclaimerText}"
									style="margin-left: 3px; margin-top: 5px">
									<h:outputText styleClass="outputText" id="textDisclaimerText"
										value="#{trialPartnerHandler.partner.disclaimerCustomHTML}"
										style="font-size: 9pt" escape="false"></h:outputText>
								</h:panelGrid>
								<h:panelGrid styleClass="panelGrid" frame="box"
									id="gridOfferDisclaimerGrid" columns="1" width="460"
									style="margin-left: 3px; margin-top: 5px"
									rendered="#{currentOfferHandler.isShowOfferDisclaimerText}">
									<h:outputText styleClass="outputText"
										id="textOfferDisclaimerText" style="font-size: 9pt"
										escape="false"
										value="#{currentOfferHandler.onePageDisclaimerText}"></h:outputText>
								</h:panelGrid>
								<h:panelGrid id="panelGridFormSubmissionGrid" columns="3"
									width="475" cellpadding="4">

									<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx3"
										value="/images/1px.gif" hspace="50" width="1" height="1"></hx:graphicImageEx>
									<hx:commandExButton type="submit" value="Place Order"
										styleClass="commandExButton" id="buttonPlaceOrder"
										tabindex="90"
										action="#{pc_SubscriptionOrderForm.doButtonPlaceOrderAction}"></hx:commandExButton>
									<hx:commandExButton type="submit" value="Cancel Order"
										styleClass="commandExButton" id="buttonCancelOrder"
										action="#{pc_SubscriptionOrderForm.doButtonCancelOrderAction}"
										immediate="true"></hx:commandExButton>

								</h:panelGrid>
							</h:panelGroup>
						</f:facet>
						<f:facet name="left">

						</f:facet>
						<f:facet name="right">

						</f:facet>
						<f:facet name="bottom">

						</f:facet>
						<f:facet name="top">
							<h:messages styleClass="messages" id="messagesAllMesssages"></h:messages>
						</f:facet>
					</hx:panelLayout> <!-- end orderFormDiv --></div>

					<p>&nbsp;</p>
					</div>
					<div id="pageContentEEOrderEntryRight" align="center"><h:panelGrid
						id="rightColImageSpot1"
						rendered="#{currentOfferHandler.renderRightColumnImageSpot1}">
						<hx:outputLinkEx styleClass="outputLinkEx"
							id="linkExRightColImageSpot1Link" target="_blank"
							style="border-width: 0px"
							value="#{currentOfferHandler.onePageRightColumnImageSpot1ImageOnClickURL}"
							rendered="#{currentOfferHandler.renderRightColmnImageSpot1ImageAsClickable}">
							<hx:graphicImageEx id="imageExRightColImage1"
								value="#{currentOfferHandler.onePageRightColumnImageSpot1ImagePath}"
								border="0"
								alt="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
								title="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
								style="padding-left: 0px;">
							</hx:graphicImageEx>
						</hx:outputLinkEx>
						<hx:graphicImageEx id="imageExRightColImage1NoLink"
							value="#{currentOfferHandler.onePageRightColumnImageSpot1ImagePath}"
							border="0"
							alt="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
							title="#{currentOfferHandler.onePageRightColumnImageSpot1ImageAltText}"
							style="padding-left: 0px;"
							rendered="#{not currentOfferHandler.renderRightColmnImageSpot1ImageAsClickable}">
						</hx:graphicImageEx>
					</h:panelGrid> <h:panelGrid styleClass="panelGrid" id="gridRightColHTMLSpot1Grid"
						columns="1" style="text-align: center">
						<h:outputText styleClass="" id="RightColHTMLSpot1Text"
							value="#{currentOfferHandler.onePageRightColumnHTMLSpot1HTML}"
							escape="false"></h:outputText>
					</h:panelGrid> <h:panelGrid styleClass="panelGrid" id="gridVideoGrid" columns="1"
						style="text-align: center" width="230"
						rendered="#{currentOfferHandler.renderRightColumnVideoSpot1}">


						<hx:outputLinkEx styleClass="outputLinkEx" id="linkExVideoLink_B"
							onclick="return openVideoScreen('#{currentOfferHandler.onePageRightColumnVideoSpot1ImageOnClickURL}');"
							value=";" style="background-color: white; border-style: none"
							title="#{currentOfferHandler.onePageRightColumnVideoSpot1ImageAltText}"
							tabindex="99">
							<hx:graphicImageEx styleClass="graphicImageEx"
								id="imageExVideoSweepsGraphic_B"
								value="#{currentOfferHandler.onePageRightColumnVideoSpot1ImagePath}"
								border="0" hspace="0" vspace="4"
								style="margin: 0px; float: none; text-align: center;clear: static; padding-left: 0px"
								title="#{currentOfferHandler.onePageRightColumnVideoSpot1ImageAltText}"></hx:graphicImageEx>
						</hx:outputLinkEx>
						<h:outputText styleClass="outputText" id="textEEVideoText"
							value="#{currentOfferHandler.onePageRightColumnVideoSpot1HTML}"
							escape="false"></h:outputText>
					</h:panelGrid> <h:panelGrid id="rightColImageSpot2" columns="1"
						rendered="#{currentOfferHandler.renderRightColumnImageSpot2}">
						<hx:outputLinkEx styleClass="outputLinkEx"
							id="linkExRightColSpot2Link" target="_blank"
							style="border-width: 0px"
							value="#{currentOfferHandler.onePageRightColumnImageSpot2ImageOnClickURL}"
							rendered="#{currentOfferHandler.renderRightColmnImageSpot2ImageAsClickable}">
							<hx:graphicImageEx styleClass="" id="imageExRightColImage2"
								value="#{currentOfferHandler.onePageRightColumnImageSpot2ImagePath}"
								border="0"
								alt="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
								title="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
								style="clear: static; padding-left: 0px">
							</hx:graphicImageEx>

						</hx:outputLinkEx>
						<hx:graphicImageEx styleClass="" id="imageExRightColImage2NoLink"
							value="#{currentOfferHandler.onePageRightColumnImageSpot2ImagePath}"
							border="0"
							alt="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
							title="#{currentOfferHandler.onePageRightColumnImageSpot2ImageAltText}"
							style="clear: static; padding-left: 0px"
							rendered="#{not currentOfferHandler.renderRightColmnImageSpot2ImageAsClickable}">
						</hx:graphicImageEx>
					</h:panelGrid> <h:panelGrid styleClass="panelGrid" id="gridRightColHTMLSpot2Grid"
						columns="1" style="text-align: center">
						<h:outputText styleClass="" id="RightColHTMLSpot2Text"
							value="#{currentOfferHandler.onePageRightColumnHTMLSpot2HTML}"
							escape="false"></h:outputText>
					</h:panelGrid> <h:panelGrid id="rightColImageSpot3" columns="1"
						rendered="#{currentOfferHandler.renderRightColumnImageSpot3}">
						<hx:outputLinkEx styleClass="outputLinkEx"
							id="linkExRightColSpot3Link" style="border-width: 0px"
							value="#{currentOfferHandler.onePageRightColumnImageSpot3ImageOnClickURL}"
							rendered="#{currentOfferHandler.renderRightColmnImageSpot3ImageAsClickable}">
							<hx:graphicImageEx styleClass="" id="imageExRightColImage3"
								value="#{currentOfferHandler.onePageRightColumnImageSpot3ImagePath}"
								border="0"
								alt="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
								title="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
								style="clear: static; padding-left: 0px">
							</hx:graphicImageEx>
						</hx:outputLinkEx>
						<hx:graphicImageEx styleClass="" id="imageExRightColImage3NoLink"
							value="#{currentOfferHandler.onePageRightColumnImageSpot3ImagePath}"
							border="0"
							alt="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
							title="#{currentOfferHandler.onePageRightColumnImageSpot3ImageAltText}"
							style="clear: static; padding-left: 0px"
							rendered="#{not currentOfferHandler.renderRightColmnImageSpot3ImageAsClickable}">
						</hx:graphicImageEx>
					</h:panelGrid></div>

					<!-- End jspPanelFormPanel -->
				</hx:jspPanel>


				<!-- end mainBodyGrid -->
			</h:panelGrid>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<!-- Delivery Method Panel -->
			<hx:panelDialog type="modeless" styleClass="panelDialog"
				id="dialogDelMethodDialog" for="linkExDelMethodCheckHelpLink"
				relativeTo="textDelMethodHelpText" initiallyShow="false"
				title="How will my paper be delivered?    ">
				<h:panelGroup id="groupDelMethod1" styleClass="panelDialog_Footer">
					<h:panelGrid styleClass="panelGrid" id="gridDelMethod2" width="250"
						style="text-align: justify">
						<h:outputText styleClass="outputText" id="textDelMethodDetailHelp"
							value="#{labels.DelMethodHelpDetail}" escape="false"></h:outputText>
					</h:panelGrid>
					<hx:commandExButton id="buttonDelMethod"
						styleClass="commandExButton" type="submit" value="Close">
						<hx:behavior event="onclick" behaviorAction="hide;stop"
							targetAction="dialogDelMethodDialog" id="behaviorDelMethod4"></hx:behavior>
					</hx:commandExButton>
				</h:panelGroup>
			</hx:panelDialog>
			<br>
			<hx:panelDialog type="modeless" styleClass="panelDialog"
				id="dialogAdditionalAddrHelp" relativeTo="formItemDeliveryAddress2"
				for="textAddrLearnMore" title="Learn More">

				<h:panelGroup id="groupAddlAddrHelpGroup1"
					styleClass="panelDialog_Footer">



					<h:panelGrid styleClass="panelGrid" id="grid1" columns="1"
						width="230" style="text-align: justify">
						<h:outputText styleClass="outputText"
							id="textAdditionalAddrLearnMoreInfoText"
							value="#{labels.AdditionalAddrLearnMore}"></h:outputText>

					</h:panelGrid>
					<hx:commandExButton id="button3" styleClass="commandExButton"
						type="submit" value="Close">
						<hx:behavior event="onclick" behaviorAction="hide;stop"
							targetAction="dialogAdditionalAddrHelp" id="behavior3"></hx:behavior>
					</hx:commandExButton>



				</h:panelGroup>
			</hx:panelDialog>
			<br>
			<hx:inputHelperSetFocus id="setFocus1"></hx:inputHelperSetFocus>
			<h:inputHidden value="#{newSubscriptionOrderHandler.startDate}"
				id="startDate" rendered="#{trialCustomerHandler.isTrialConversion}">
			</h:inputHidden>
			<br>
			<br>
			<f:verbatim>

			</f:verbatim>
			<br>
			<br>
			<br>
			<h:inputHidden value="#{currentOfferHandler.isForceBillMe}"
				id="isForceBillMe"></h:inputHidden>
			<h:inputHidden value="#{newSubscriptionOrderHandler.paymentMethod}"
				id="paymentMethodHidden"
				rendered="#{currentOfferHandler.isForceBillMe}"></h:inputHidden>
			<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1"
				behaviorAction="click"></hx:behaviorKeyPress>
		</h:form>
	</hx:scriptCollector>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</hx:viewFragment>