<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/NewOrdersChooseOffer.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://www.ibm.com/jsf/html_extended"
	prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragmentChooseOffer">
<link rel="stylesheet" href="${pageContext.request.contextPath}/restricted/newOrders/orders/stylesheet.css" type="text/css">
<script language="JavaScript" src="${pageContext.request.contextPath}/scripts/pubDateValidator.js"></script>
<script type="text/javascript">
function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

}
function func_7(thisObj, thisEvent, pubCode) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
 
 // validate the date
 dateChanged(thisObj, pubCode);

}
</script>
	<f:loadBundle basename="resourcesFile" var="labels" />
	<hx:scriptCollector id="scriptCollectorProductOffer"
		preRender="#{pc_NewOrdersChooseOffer.onPageLoadBegin}">
		<h:form styleClass="form" id="formOfferSelcection">
			<h:panelGrid styleClass="panelGrid" id="gridOfferBodyGrid" columns="1" width="650">
				<h:panelGrid styleClass="panelGrid" id="gridChosenProductGrid1" columns="3">
					<h:outputText styleClass="outputText" id="textProductNameLabel" value="Product: " style="color: black; font-weight: bold; font-size: 14pt"></h:outputText>
					<h:outputText styleClass="outputText" id="textProductName" value="#{offerSearchHandler.productHandler.productName}" style="color: #004080; font-weight: bold; font-size: 14pt"></h:outputText>
					<h:outputFormat styleClass="outputFormat" id="formatProductDefaultKeycode1" value="(Default Keycode: {0})">
						<f:param
							value="#{offerSearchHandler.productHandler.product.defaultKeycode}"
							id="paramDefaultKeycodeParm4"></f:param>
					</h:outputFormat>
				</h:panelGrid>
				<h:outputText styleClass="outputText" id="textKeycodeOfferCodeLabel"
					value="Change Key Code/Offer Code:"
					style="font-weight: bold; margin-top: 15px"></h:outputText>
				<h:inputText styleClass="inputText" id="textKeycode"
					value="#{offerSearchHandler.keycode}" size="65">
					<hx:inputHelperTypeahead styleClass="inputText_Typeahead"
							id="typeahead1" value="#{offerSearchHandler.typeAhead}" matchWidth="false" startDelay="750" inProgress="blink"></hx:inputHelperTypeahead>
				</h:inputText>
				<hx:commandExButton type="button" value="Show Offer Details"
					styleClass="commandExButton" id="buttonShowOfferDetails" style="margin-top: 10px">
					<hx:behavior event="onclick" id="behavior1"
						behaviorAction="get;stop" targetAction="gridTermSelectionV2;"></hx:behavior>
				</hx:commandExButton>
				<f:facet name="header">
					<h:panelGroup styleClass="panelGroup" id="groupOfferHeaderGroup1"><h:messages styleClass="messages" id="messagesOfferSelectionMsgs1"></h:messages></h:panelGroup>
				</f:facet>
			</h:panelGrid>
			<h:panelGrid styleClass="panelGrid" id="gridTermSelectionV2" columns="1" style="margin-top: 20px">
				<f:facet name="footer">
					<h:panelGroup styleClass="panelGroup" id="groupOfferFooterGroup2">
						<h:panelGrid styleClass="panelGrid" id="gridCheckoutGrid1"
							width="100%" style="text-align: right">
							<hx:commandExButton type="submit" value="Check Out"
								styleClass="commandExButton" id="buttonPlaceOrder" action="#{pc_NewOrdersChooseOffer.doButtonPlaceOrderAction}"></hx:commandExButton>
						</h:panelGrid>
					</h:panelGroup>
				</f:facet>
				<hx:dataTableEx border="0" cellpadding="2" cellspacing="2"
					columnClasses="columnClass1" headerClass="headerClass"
					footerClass="footerClass" rowClasses="rowClass1, rowClass2"
					styleClass="dataTableEx" id="tableExOfferDetailsDataTable"
					value="#{offerSearchHandler.offerTerms}" var="varofferTerms" style="border-bottom-width: thin; border-bottom-style: solid; border-bottom-color: black">
					<f:facet name="header">
						<hx:panelBox styleClass="panelBox" id="boxOfferTermsHeaderDTBox1">
							<h:panelGrid styleClass="panelGrid"
								id="gridfferTermsHeaderDTGrid1" width="500"
								style="background-color: white"
								rendered="#{offerSearchHandler.showOffer}">
								<h:outputText styleClass="outputText" id="textOfferPromoText1"
									escape="false"
									value="#{offerSearchHandler.currentOfferHandler.onePagePromotionText1}"></h:outputText>
							</h:panelGrid>
						</hx:panelBox>
					</f:facet>
					<f:facet name="footer">
						<hx:panelBox styleClass="panelBox" id="boxTermsFooterBox">
							<h:panelGrid styleClass="panelGrid" id="gridOfferFoooterGrid1" columns="2">
								<h:outputText styleClass="outputText" id="textOfferDisclaimerText" value="#{offerSearchHandler.currentOfferHandler.onePageDisclaimerText}"></h:outputText>
								<hx:outputLinkEx value="javascript: ;" styleClass="outputLinkEx" id="linkExEZPayLearnMoreLink1">
									<h:outputText id="text10EZPayInfo" styleClass="outputText"
										value="EZPay Information"></h:outputText>
								</hx:outputLinkEx>
							</h:panelGrid>
						</hx:panelBox>
					</f:facet>
					<hx:columnEx id="columnEx1">
						<hx:inputRowSelect styleClass="inputRowSelect"
							id="rowSelectTermSelection1" selectOne="true" value="#{varofferTerms.selected}"></hx:inputRowSelect>
						<f:facet name="header"></f:facet>
					</hx:columnEx>
					<hx:columnEx id="columnExTermCol1" align="center">
						<f:facet name="header">
							<h:outputText id="text1" styleClass="outputText" value="Term"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text5"
							value="#{varofferTerms.termDuration}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnExPriceCol2" align="center">
						<f:facet name="header">
							<h:outputText value="Price" styleClass="outputText" id="text2"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text6"
							value="#{varofferTerms.termPrice}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnExIssuePriceCol3" align="center">
						<f:facet name="header">
							<h:outputText value="Per Issue Rate" styleClass="outputText"
								id="text3"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text7"
							value="#{varofferTerms.termDailyRate}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnExReqEZPay5" align="center">
						<f:facet name="header">
							<h:outputText value="Requires EZPay" styleClass="outputText"
								id="text8"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="text9"
							value="#{varofferTerms.requiresEZPayString}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnExRenewalOption4">
						<f:facet name="header">
							<h:outputText value="Renewal Option" styleClass="outputText"
								id="text4"></h:outputText>
						</f:facet>
					</hx:columnEx>
				</hx:dataTableEx>
				<h:panelGrid styleClass="panelGrid"
					id="gridQuantityInformation" width="475" columns="1" rendered="#{offerSearchHandler.productHandler.isShowChangeOrderQtySection}">

					<hx:panelFormBox helpPosition="under" labelPosition="left"
						styleClass="panelFormBox" id="formBoxOrderQtyFormBox"
						widthContent="350" widthLabel="100" label=":: Order Quantity  ::">
						<hx:formItem styleClass="formItem" id="formItemQty2"
							label="Num Papers:"
							infoText="The number of papers to be delivered to a single address. For more than the allowed maximum here, use One Call.">
							<h:selectOneMenu styleClass="selectOneMenu"
								id="menuOrderQuantity"
								value="#{newSubscriptionOrderHandler.quantity}">
								<f:selectItems id="selectItemsQty2"
									value="#{offerSearchHandler.productHandler.orderQtySelectItems}" />
							</h:selectOneMenu>

						</hx:formItem>


					</hx:panelFormBox>
					<!-- end grid -->
				</h:panelGrid>
				
				<h:panelGrid styleClass="panelGrid"
					id="gridStartDateInformation" width="475" columns="1">
					<hx:panelFormBox helpPosition="under" labelPosition="left"
						styleClass="panelFormBox" id="formBoxStartDateInformation"
						label=":: Subscription Start Date  ::" widthLabel="100"
						widthContent="350">

						<hx:formItem styleClass="formItem" id="formItemStartDateFormItem"
							label=" #{labels.DateLabel} *"
							infoText="Latest Possible Start Date: #{offerSearchHandler.currentOfferHandler.latestPossibleStartDate}"
							showHelp="always">

							<h:inputText styleClass="inputText" id="textStartDateInput"
								size="14" value="#{newSubscriptionOrderHandler.startDate}"
								tabindex="11" onchange="return func_7(this, event, '#{offerSearchHandler.currentOfferHandler.pubCode}');">
								<hx:convertDateTime pattern="MM/dd/yyyy" />
								<hx:inputHelperDatePicker id="datePicker1" multiLine="false" />
								<hx:validateDateTimeRange
									maximum="#{offerSearchHandler.currentOfferHandler.latestPossibleStartDate}"
									minimum="#{offerSearchHandler.currentOfferHandler.earliestPossibleStartDate}" />
							</h:inputText>

						</hx:formItem>
						<f:facet name="top">
							<h:panelGrid styleClass="panelGrid" id="gridDatePickerHeaderGrid"
								width="95%" columns="1" cellpadding="1" cellspacing="1">
								<hx:panelSection styleClass="panelSection"
									id="sectionStartDateSectionHeaderInfo" initClosed="true"
									rendered="true">
									<f:facet name="closed">
										<hx:jspPanel id="jspPanel2">

											<h:outputText id="text3Closed" styleClass="outputText"
												value="#{labels.StartDateHeaderLabelOnePageClosed}"
												escape="false"></h:outputText>
										</hx:jspPanel>
									</f:facet>
									<f:facet name="opened">
										<hx:jspPanel id="jspPanel1">

											<h:outputText id="text2OpenedHeader" styleClass="outputText"
												value="#{labels.StartDateHeaderLabelOnePageOpened}"
												escape="false"></h:outputText>

										</hx:jspPanel>
									</f:facet>
									<h:panelGrid styleClass="panelGrid" id="gridStartDateInnerGrid"
										columns="1"
										style="border-bottom-width: 1px; border-bottom-style: solid; border-top-color: gray; margin-top: 5px; border-top-width: 1px; border-top-style: solid; border-bottom-color: gray">
										<h:outputFormat styleClass="outputFormat"
											id="formatGiftFutureStartDateText"
											value="#{labels.startDateInfo}" escape="false">
											<f:param value="#{offerSearchHandler.productHandler.productName}"
												id="paramStartDateInfoParm1"></f:param>
										</h:outputFormat>
									</h:panelGrid>
								</hx:panelSection>
							</h:panelGrid>
						</f:facet>
					</hx:panelFormBox>
				</h:panelGrid>										
				<h:panelGrid styleClass="panelGrid"
					id="gridPartnerInformation" width="475" columns="1"
					rendered="#{offerSearchHandler.currentOfferHandler.currentOffer.clubNumberRequired}">
					<hx:panelFormBox helpPosition="under" labelPosition="left"
						styleClass="panelFormBox" id="formBoxPartnerInformation"
						label=":: Program Information ::"
						widthLabel="150">
						<hx:formItem styleClass="formItem"
							id="formItemPartnerClubNumber"
							label="Club Number:"
							infoText="(Enter your member number or claim code here.)">
							<h:inputText styleClass="inputText" id="textClubNumber"
								value="#{newSubscriptionOrderHandler.clubNumber}"
								size="20" tabindex="10" maxlength="15"></h:inputText>
						</hx:formItem>
					</hx:panelFormBox>
				</h:panelGrid>
				
				<f:facet name="header">
					<h:panelGroup styleClass="panelGroup" id="gridTermSelectionHeaderGroupV2" rendered="#{offerSearchHandler.showOffer}">
						<h:outputFormat styleClass="outputFormat"
							id="formatCurrentOfferDesFormat" value=":: {0} for {1} / {2}::"
							style="color: #004080; font-weight: bold; font-size: 14pt">
							<f:param value="#{labels.termLabel}" id="paramTermLabelParm1"></f:param>
							<f:param
								value="#{offerSearchHandler.currentOfferHandler.productName}"
								id="paramTermLabelParm2"></f:param>
							<f:param
								value="#{offerSearchHandler.currentOfferHandler.currentOffer.keyCode}"
								id="paramTermLabelParm3"></f:param>
						</h:outputFormat>
					</h:panelGroup>
				</f:facet>
			</h:panelGrid>
			<hx:ajaxRefreshSubmit target="gridTermSelectionV2"
				id="ajaxRefreshSubmit2"></hx:ajaxRefreshSubmit>
			<br>
			
		<br>
			<br>
			<hx:panelDialog type="modeless" styleClass="panelDialog"
				id="dialogEZPayInformation" for="linkExEZPayLearnMoreLink1" title="EZPay Information" relativeTo="linkExEZPayLearnMoreLink1">
				<h:panelGrid styleClass="panelGrid" id="gridEZPayInfoGrid2" width="250">
					<h:outputText styleClass="outputText" id="textEZPay"
						value="#{labels.EZPayPlanTextUnderTermsMultiPubFAQ}" escape="false"></h:outputText>
				</h:panelGrid>
				<h:panelGroup id="group1" styleClass="panelDialog_Footer">
					<hx:commandExButton id="button3" styleClass="commandExButton"
						type="submit" value="OK">
						<hx:behavior event="onclick" behaviorAction="hide;stop"
							targetAction="dialogEZPayInformation" id="behavior3"></hx:behavior>
					</hx:commandExButton>
					<hx:commandExButton id="button2" styleClass="commandExButton"
						type="reset" value="Cancel">
						<hx:behavior event="onclick" behaviorAction="hide;stop"
							id="behavior2" targetAction="dialogEZPayInformation"></hx:behavior>
					</hx:commandExButton>
				</h:panelGroup>
			</hx:panelDialog>
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>