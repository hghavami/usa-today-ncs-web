<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/Home_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="form1">
			<h:messages styleClass="messages" id="messages1"></h:messages>
			<div id="container">
			<div id="col_one"><hx:jspPanel id="jspPanelLeftWork">
				<h:outputText styleClass="outputText_Med" id="textHomeMsg"
					value="#{labels.homeMessage}"></h:outputText>
			</hx:jspPanel>
					<h:panelGrid styleClass="panelGrid" id="gridEEditionGrid"
						columns="1"
						style="padding-top: 10px; margin-top: 15px; text-align: center">
						<hx:outputLinkEx styleClass="outputLinkEx" id="linkEx2"
							value="#{eeLinkHandler.ENewspaperLink}" target="_blank">
							<hx:graphicImageEx styleClass="graphicImageEx" id="imageEx1"
								value="/natcsweb/images/eEditionSummary229x41.jpg"></hx:graphicImageEx>
						</hx:outputLinkEx>

						<hx:outputLinkEx value="#{eeLinkHandler.ENewspaperLink}"
							styleClass="outputLinkEx" id="linkEx1" target="_blank">
							<h:outputText id="textLinkText" styleClass="outputText"
								value="Open USA TODAY e-Newspaper - Desktop Viewer"></h:outputText>
						</hx:outputLinkEx>
						<hx:outputLinkEx styleClass="outputLinkEx" id="linkEx3" value="#{eeLinkHandler.ENewspaperLinkAlternate}" target="_blank">
							<h:outputText id="text1" styleClass="outputText"
								value="Open USA TODAY e-Newspaper - Mobile Viewer"></h:outputText>
						</hx:outputLinkEx>
					</h:panelGrid>
				</div>
			<div id="col_two">
			<table class="outlinedTable">
				<tbody>
					<tr>
						<th bgcolor="#d3dbee" nowrap="nowrap"><h:outputText
							styleClass="outputText" id="textMarketLookupHeader"
							value="#{labels.marketLookupHeader}"></h:outputText></th>
					</tr>
					<tr>
						<td bgcolor="white" nowrap="nowrap" align="left"><h:outputText
							styleClass="outputText" id="textMarketSearch"
							value="#{labels.marketSeachZip}"></h:outputText></td>
					</tr>
					<tr>
						<td><h:inputText styleClass="inputText" id="textZipCode"
							size="7" maxlength="5">
							<f:validateLength minimum="5" maximum="5"></f:validateLength>
							<hx:validateConstraint regex="^[0-9]+$" />
							<hx:inputHelperAssist autoTab="true" errorClass="inputText_Error" />
						</h:inputText> <hx:commandExButton type="submit"
							value="#{labels.marketSearchButtonText}"
							styleClass="commandExButton" id="buttonLookUpMarket"
							action="#{pc_Home.doButtonLookUpMarketAction}"
							style="margin-left: 5px"></hx:commandExButton></td>
					</tr>
					<tr>
						<td>
						<table>
							<tbody>
								<tr>
									<th align="left" colspan="3" valign="bottom"><b>Last
									Search Results:</b></th>
								</tr>
								<tr>
									<th align="right" nowrap="nowrap">Zip Code:</th>
									<td style="width: 5px">&nbsp;</td>
									<td><h:outputText id="textCurrentMarketZip"
										value="#{currentMarket.zipCode}" styleClass="outputText">
									</h:outputText></td>
								</tr>
								<tr>
									<th align="right" nowrap="nowrap">Market ID:</th>
									<td style="width: 5px">&nbsp;</td>
									<td><h:outputText id="textCurrentMarketID"
										value="#{currentMarket.market.marketID}"
										styleClass="outputText">
									</h:outputText></td>
								</tr>
								<tr>
									<th align="right" nowrap="nowrap">Description:</th>
									<td style="width: 5px">&nbsp;</td>
									<td nowrap="nowrap"><h:outputText
										id="textCurrentMarketDesc"
										value="#{currentMarket.market.description}"
										styleClass="outputText">
									</h:outputText></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</div>
			</div>
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>