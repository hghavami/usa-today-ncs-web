<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/CreateIncident_headarea.java" --%><%-- /jsf:pagecode --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragmentHeadFragment"><jsp:useBean id="now"
		class="java.util.Date" scope="session" />

	<script type="text/javascript">
var divRequestTypesArray = null;
function CopyIncidentAddr() {
var checkBoxElement = document.getElementById("subviewMainBodyArea:viewFragment1:form1:checkbox2");

if (checkBoxElement.checked) {
	Form.Element.setValue('subviewMainBodyArea:viewFragment1:form1:text9',$F('subviewMainBodyArea:viewFragment1:form1:text2'));
	Form.Element.setValue('subviewMainBodyArea:viewFragment1:form1:text11',$F('subviewMainBodyArea:viewFragment1:form1:text3'));
	Form.Element.setValue('subviewMainBodyArea:viewFragment1:form1:menu3',$F('subviewMainBodyArea:viewFragment1:form1:menu2'));
	Form.Element.setValue('subviewMainBodyArea:viewFragment1:form1:text12',$F('subviewMainBodyArea:viewFragment1:form1:text4'));
}
}

function initPage() {
	divRequestTypesArray = $$('div.requestTypesClass');
	var checkBoxElementInit = document.getElementById("subviewMainBodyArea:viewFragment1:form1:checkbox2");
	checkBoxElementInit.checked=false;	
	
	// following 3 lines reset the request id to a new value
	var hiddenField = document.getElementById('subviewMainBodyArea:viewFragment1:form1:uniqueRequestValue');
	var d = new Date();
	hiddenField.value = d.getTime();
	
	var categoryObj = document.getElementById('subviewMainBodyArea:viewFragment1:form1:menu1');
	func_2(categoryObj, null);
}

function func_1(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

var typeObj = document.getElementById("subviewMainBodyArea:viewFragment1:form1:menu1");

if (typeObj.selectedIndex == 0) {
	setTimeout("alert ('Please select a Service Category and Request Type.')", 100);
	typeObj.focus();
	return false;
}

thisObj.label = "Please Wait...";

return true;
}
function func_2(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'

// 5 = eedition
	
	// hide all request type select options
	divRequestTypesArray.invoke('hide');
	if (thisObj.selectedIndex == 0 || thisObj.selectedIndex == 18) {
		divRequestTypesArray[0].show();
	}
	else if (thisObj.selectedIndex == 1) {
		divRequestTypesArray[1].show();
	}
	else if (thisObj.selectedIndex == 2) {
		divRequestTypesArray[2].show();
	}
	else if (thisObj.selectedIndex == 3) {
		divRequestTypesArray[3].show();
	}
	else if (thisObj.selectedIndex == 4) {
		divRequestTypesArray[4].show();
	}
	else if (thisObj.selectedIndex == 5) {
		divRequestTypesArray[5].show();
	}
	else if (thisObj.selectedIndex == 6) {
		divRequestTypesArray[6].show();
	}
	else if (thisObj.selectedIndex == 7) {
		divRequestTypesArray[7].show();
	}
	else if (thisObj.selectedIndex == 8) {
		divRequestTypesArray[8].show();	
	
	}
	else if (thisObj.selectedIndex == 9) {
		divRequestTypesArray[9].show();
	}
	else if (thisObj.selectedIndex == 10) {
		divRequestTypesArray[10].show();
	}
	else if (thisObj.selectedIndex == 11) {
		divRequestTypesArray[11].show();
	}
	else if (thisObj.selectedIndex == 12) {
		divRequestTypesArray[12].show();
	}
	else if (thisObj.selectedIndex >= 13) {
		divRequestTypesArray[13].show();
	}


}</script>
</hx:viewFragment>