<%@taglib uri="http://www.ibm.com/jsf/BrowserFramework" prefix="odc"%><%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/SubscriptionOrderForm_headarea.java" --%><%-- /jsf:pagecode --%><%@taglib
	uri="http://java.sun.com/jsf/core" prefix="f"%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<jsp:useBean id="now"
		class="java.util.Date" scope="session" />
	<title>Subscription Order Form</title>

	<script
		src="${pageContext.request.contextPath}/scripts/scriptaculous/scriptaculous.js"
		type="text/javascript"></script>
	<script language="JavaScript"
		src="${pageContext.request.contextPath}/scripts/pubDateValidator.js"></script>
	<script type="text/javascript">
function updateDeliveryInfoPanelClass() {
    var checkbox = $('formOrderEntryForm:checkboxIsBillDifferentFromDelSelector');
	// gift information
    var panelEmailGiftInformation = $('formOrderEntryForm:gridBillingAddress');
    var panelOneTimeBillGift = $('formOrderEntryForm:gridEZPAYOptionsGridGift');    
	var panelOneTimeBill = $('formOrderEntryForm:gridEZPAYOptionsGrid');
   	panelEmailGiftInformation.hide();    	
   	panelOneTimeBillGift.hide();
   	panelOneTimeBill.hide();

    if (checkbox.checked) {
    	panelEmailGiftInformation.show();    	
    	panelOneTimeBillGift.show();
    	panelOneTimeBill.hide();
    }
    else {
    	panelEmailGiftInformation.hide();
    	panelOneTimeBillGift.hide();
    	panelOneTimeBill.show();
    }
}

function updatePaymentPanelClass(billMeValue) {

   	//var panelCreditCardInformation = document.getElementById("formOrderEntryForm:gridPaymentInformationGrid");
   	var panelCreditCardInformation = $('formOrderEntryForm:gridPaymentInformationGrid');
	if (billMeValue == "B") {
    	panelCreditCardInformation.hide();
    }
    else {
    	panelCreditCardInformation.show();
    }	
}

function initPage() {
	
	updateDeliveryInfoPanelClass();
	updatePaymentPanelClass("${newSubscriptionOrderHandler.paymentMethod}");
}

function func_2(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updateDeliveryInfoPanelClass();
}
function func_3(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	
	window.open('/faq/utfaq.jsp#section6','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no');
}
// func_4 - enable/disable the credit card payment
function func_4(thisObj, thisEvent) {
	//use 'thisObj' to refer directly to this component instead of keyword 'this'
	//use 'thisEvent' to refer to the event generated instead of keyword 'event'
	updatePaymentPanelClass(thisObj.value);
	  
}


function func_revalidateDelMethod(thisObj, thisEvent) {
//use 'thisObj' to refer directly to this component instead of keyword 'this'
//use 'thisEvent' to refer to the event generated instead of keyword 'event'
var delMethodVar = $('formOrderEntryForm:textDeliveryMethodTextDeterminedValue');

delMethodVar.innerHTML = "Not Determined";
}
</script>

	<link rel="stylesheet" type="text/css" title="Style"
		href="${pageContext.request.contextPath}/theme/tabpanel.css">



	<link rel="stylesheet" href="${pageContext.request.contextPath}/restricted/newOrders/orders/stylesheet.css" type="text/css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/restricted/newOrders/orders/basic.css" type="text/css">
