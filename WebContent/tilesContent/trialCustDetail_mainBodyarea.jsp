<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/TrialCustDetail_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollector1">

		<br>
		<h:form styleClass="form" id="form1">
			<hx:panelLayout styleClass="panelLayout"
				id="layoutTrialCustomerOuterLayout">
				<f:facet name="body">
					<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
						columnClasses="columnClass1" headerClass="headerClass"
						footerClass="footerClass" rowClasses="rowClass1, rowClass2"
						styleClass="dataTableEx" id="tableExTrialSubscriptions"
						value="#{trialCustHandler.allTrials}" var="varallTrials">
						<hx:columnEx id="columnEx5" nowrap="true">
							<f:facet name="header">
								<h:outputText value="Partner Name" styleClass="outputText"
									id="text5"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text18"
								value="#{varallTrials.trial.partner.partnerName}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx17" align="center">
							<f:facet name="header">
								<h:outputText value="Pub Code" styleClass="outputText"
									id="text17"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text19"
								value="#{varallTrials.product.productCode}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx1" nowrap="true">
							<f:facet name="header">
								<h:outputText id="text1" styleClass="outputText"
									value="Start Date"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text20"
								value="#{varallTrials.startDate}">
								<hx:convertDateTime />
							</h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx2" nowrap="true">
							<f:facet name="header">
								<h:outputText value="End Date" styleClass="outputText"
									id="text2"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text21"
								value="#{varallTrials.endDate}">
								<hx:convertDateTime />
							</h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx3" align="center">
							<f:facet name="header">
								<h:outputText value="Days Remaining" styleClass="outputText"
									id="text3"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text22"
								value="#{varallTrials.daysRemainingString}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx4" align="center">
							<f:facet name="header">
								<h:outputText value="Subscribed Flag" styleClass="outputText"
									id="text4"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text23"
								value="#{varallTrials.trial.isSubscribed}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx6" align="center">
							<f:facet name="header">
								<h:outputText value="NCS Rep ID" styleClass="outputText"
									id="text6"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text24"
								value="#{varallTrials.trial.ncsRepID}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx7" nowrap="true" align="center">
							<f:facet name="header">
								<h:outputText value="First Name" styleClass="outputText"
									id="text7"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text25"
								value="#{varallTrials.trial.contactInformation.firstName}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx8" align="center">
							<f:facet name="header">
								<h:outputText value="Last Name" styleClass="outputText"
									id="text8"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text26"
								value="#{varallTrials.trial.contactInformation.lastName}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx9" nowrap="true">
							<f:facet name="header">
								<h:outputText value="Address 1" styleClass="outputText"
									id="text9"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text27"
								value="#{varallTrials.trial.contactInformation.uiAddress.address1}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx10" nowrap="true">
							<f:facet name="header">
								<h:outputText value="Address2" styleClass="outputText"
									id="text10"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text28"
								value="#{varallTrials.trial.contactInformation.uiAddress.address2}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx11" align="center" nowrap="true">
							<f:facet name="header">
								<h:outputText value="Apt/Suite" styleClass="outputText"
									id="text11"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text29"
								value="#{varallTrials.trial.contactInformation.uiAddress.aptSuite}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx12" nowrap="true" align="center">
							<f:facet name="header">
								<h:outputText value="City" styleClass="outputText" id="text12"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text30"
								value="#{varallTrials.trial.contactInformation.uiAddress.city}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx13" align="center">
							<f:facet name="header">
								<h:outputText value="State" styleClass="outputText" id="text13"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text31"
								value="#{varallTrials.trial.contactInformation.uiAddress.state}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx14" align="center">
							<f:facet name="header">
								<h:outputText value="Zip" styleClass="outputText" id="text14"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text32"
								value="#{varallTrials.trial.contactInformation.uiAddress.zip}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx15" nowrap="true" align="center">
							<f:facet name="header">
								<h:outputText value="Phone" styleClass="outputText" id="text15"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text33"
								value="#{varallTrials.trial.contactInformation.homePhoneNumberFormatted}"></h:outputText>
						</hx:columnEx>
						<hx:columnEx id="columnEx16" nowrap="true" align="center">
							<f:facet name="header">
								<h:outputText value="Program (Club) Number"
									styleClass="outputText" id="text16"></h:outputText>
							</f:facet>
							<h:outputText styleClass="outputText" id="text34"
								value="#{varallTrials.trial.clubNumber}"></h:outputText>
						</hx:columnEx>
					</hx:dataTableEx>
				</f:facet>
				<f:facet name="left"></f:facet>
				<f:facet name="right"></f:facet>
				<f:facet name="bottom"></f:facet>
				<f:facet name="top">
					<h:panelGrid styleClass="panelGrid" id="gridHeaderGrid"
						cellpadding="1" cellspacing="1" columns="2">
						<h:outputText styleClass="outputText" id="textEmailLabel"
							value="#{labels.emailAddress}:"
							style="font-weight: bold; font-size: 12pt"></h:outputText>
						<h:inputText styleClass="inputText" id="textEmailAddress"
							value="#{trialCustHandler.trialCustomer.emailAddress}" size="60"
							readonly="true"></h:inputText>
					</h:panelGrid>
				</f:facet>
			</hx:panelLayout>
		</h:form>
		<br>
		<br>
	</hx:scriptCollector>
</hx:viewFragment>