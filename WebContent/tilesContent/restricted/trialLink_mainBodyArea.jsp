<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/restricted/TrialLink_mainBodyArea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://www.ibm.com/jsf/html_extended"
	prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragmentMainBody1"><hx:scriptCollector id="scriptCollectorMainBody1">
		<h:form styleClass="form" id="form1">
			<h:panelGrid styleClass="panelGrid outlinedTable" id="gridLinksGrid"
				cellpadding="1" cellspacing="2" width="600">
				<f:facet name="header">
					<h:panelGroup styleClass="panelGroup"
						id="groupAccountLinksHeaderGroup">
						<h:panelGrid styleClass="panelGrid" id="gridAcctHeaderGrid"
							width="590" style="background-color: #d3dbee">
							<h:outputText styleClass="outputText" id="textAcctMngmtLabel"
								value="#{labels.accountServices}"
								style="background-color: #d3dbee; font-weight: bold; font-size: 12pt"></h:outputText>
						</h:panelGrid>
	
					</h:panelGroup>
				</f:facet>
				<hx:outputLinkEx styleClass="outputLinkEx"
					id="linkExSampleSignUpLink"
					value="/electronicSamplePartnerLookup.do?pid=UTCSP001&amp;tcsrid=#{user.repIDForSampleLink}"
					target="_blank">
					<h:outputText id="textSampleSignUpLabel" styleClass="outputText"
						value="Enter Trial Subscription" style="font-size: 12pt"></h:outputText>
				</hx:outputLinkEx>
			</h:panelGrid>		
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>