<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/restricted/accountMaintenance/MultipleEmailUpdate_mainBodyArea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://www.ibm.com/jsf/html_extended"
	prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragmentMainBodyArea1"><hx:scriptCollector id="scriptCollector1">
		<h1><h:outputText styleClass="outputText" id="textHeaderText"
			value=""></h:outputText><h:outputFormat styleClass="outputFormat"
			id="format1"
			value="Multiple Email Records For Email Address,  Number Instances: ({0})">
			<f:param id="param1" value="#{emailSearchHandler.numberOfInstancesOfSelectedEmail}"></f:param>
		</h:outputFormat></h1>
		<h:form styleClass="form" id="form1">
			<h:messages styleClass="messages" id="messagesMainBody1" style="font-size: 12pt"></h:messages>
			<hx:dataTableEx border="0" cellpadding="4" cellspacing="1"
				columnClasses="columnClass1" headerClass="headerClass"
				footerClass="footerClass" rowClasses="rowClass1, rowClass2"
				styleClass="dataTableEx" id="tableExLikeEmailRecords"
				value="#{emailSearchHandler.sameEmailAddressRecords}"
				var="varsameEmailAddressRecords">
				<f:facet name="header">
					<hx:panelBox styleClass="panelBox" id="box2">
						<hx:outputSelecticons styleClass="outputSelecticons"
							id="selecticons1"></hx:outputSelecticons>
						<hx:commandExButton type="submit"
							value="Reset Password On Selected Records"
							styleClass="commandExButton" id="buttonResetSelectedRecords"
							action="#{pc_MultipleEmailUpdate_mainBodyArea.doButtonUpdateAllRecordsAction}"
							confirm="All of the selected records will have their password reset to 'password'. This cannot be undone."
							style="margin-left: 15px; cursor: pointer"></hx:commandExButton>
					</hx:panelBox>
				</f:facet>
				<f:facet name="footer">
					<hx:panelBox styleClass="panelBox" id="box1">
						<hx:outputSelecticons styleClass="outputSelecticons"
							id="selecticons2"></hx:outputSelecticons>
					</hx:panelBox>
				</f:facet>
				<hx:columnEx id="columnEx5">
					<hx:inputRowSelect styleClass="inputRowSelect" id="rowSelect1"
						value="#{varsameEmailAddressRecords.selectedInTable}"></hx:inputRowSelect>
					<f:facet name="header"></f:facet>
				</hx:columnEx>
				<hx:columnEx id="columnEx1">
					<f:facet name="header">
						<h:outputText id="text1" styleClass="outputText"
							value="Email Address"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text5"
						value="#{varsameEmailAddressRecords.emailAddress}"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx3" align="center">
					<f:facet name="header">
						<h:outputText value="Publication" styleClass="outputText"
							id="text3"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text6"
						value="#{varsameEmailAddressRecords.pubCode}"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx2" align="center">
					<f:facet name="header">
						<h:outputText value="Account Number" styleClass="outputText"
							id="text2"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text7"
						value="#{varsameEmailAddressRecords.accountNumber}"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx4" align="center">
					<f:facet name="header">
						<h:outputText value="Gift Giver Flag" styleClass="outputText"
							id="text4"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text8"
						value="#{varsameEmailAddressRecords.giftRecordFlag}"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx7" align="center">
					<f:facet name="header">
						<h:outputText value="Email Date" styleClass="outputText"
							id="text11"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text12"
						value="#{varsameEmailAddressRecords.startDateFormatted}">
					</h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx6" width="200">
					<f:facet name="header">
						<h:outputText value="Status Message" styleClass="outputText"
							id="text9"></h:outputText>
					</f:facet>
					<h:outputText styleClass="outputText" id="text10"
						value="#{varsameEmailAddressRecords.message}"></h:outputText>
				</hx:columnEx>
				<hx:columnEx id="columnEx8" align="center" nowrap="true"
					style="padding-left: 3px; padding-right: 3px">
					<hx:commandExRowEdit styleClass="commandExButton"
						id="rowEditEmailAddress"
						action="#{pc_MultipleEmailUpdate_mainBodyArea.doRowEditEmailAddressAction}"
						title="Click to change the email address of this record."
						editLabel="Change Email" style="padding: 0px;">
						<hx:jspPanel id="jspPanel1">
							<hx:panelFormBox helpPosition="over" labelPosition="left"
								styleClass="panelFormBox" id="formBoxInPlaceEditor"
								label="Change Email Address" style="width: 300px">
								<hx:formItem styleClass="formItem" id="formItemEmailAddress"
									label="New Email:">
									<h:inputText styleClass="inputText" id="text13NewEmail"
										value="#{varsameEmailAddressRecords.emailAddress}" size="30">
										<f:validateLength minimum="4" maximum="60"></f:validateLength>
									</h:inputText>
								</hx:formItem>
								<f:facet name="bottom">
									<h:outputText styleClass="outputText" id="text313"
										value="It may take up to 3 days for the<br />change to take affect for the Daily Buzz."
										escape="false"></h:outputText>
								</f:facet>
							</hx:panelFormBox>
						</hx:jspPanel>
						<f:param
							value="#{varsameEmailAddressRecords.emailRecord.serialNumber}"
							name="serialNumber" id="param2"></f:param>
					</hx:commandExRowEdit>
					<f:facet name="header">
						<h:outputText styleClass="outputText" id="text13"
							value="Change Email"></h:outputText>
					</f:facet>
				</hx:columnEx>
				<hx:columnEx id="columnEx9" align="center">
					<f:facet name="header">
						<h:outputText styleClass="outputText" id="text14"></h:outputText>
					</f:facet>
					<hx:commandExButton type="submit" value="SendConfirmationEmail"
						styleClass="commandExButton" id="buttonSendConfirmationEmail" confirm="This will send an email to the customer with their credentials." action="#{pc_MultipleEmailUpdate_mainBodyArea.doButtonSendConfirmationEmailAction}"></hx:commandExButton>
				</hx:columnEx>
			</hx:dataTableEx>

		</h:form>
		
	</hx:scriptCollector>
</hx:viewFragment>