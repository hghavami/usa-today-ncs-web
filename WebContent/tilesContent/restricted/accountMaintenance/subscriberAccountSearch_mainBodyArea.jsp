<%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/restricted/accountMaintenance/SubscriberAccountSearch_mainBodyArea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib uri="http://java.sun.com/jsf/core"
	prefix="f"%><%@taglib uri="http://www.ibm.com/jsf/html_extended"
	prefix="hx"%><%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment
	id="viewFragmentMainBodyArea"><hx:scriptCollector id="scriptCollectorMainBodyArea1">

		<h:form styleClass="form" id="form1">
			<h:messages styleClass="messages" id="messages1" style="font-weight: bold; font-size: 12pt"></h:messages>
			<hx:panelSection styleClass="panelSection" id="sectionEmailSearch"
			initClosed="false"
			style="border-color: #ece9d8; border-style: solid; border-width: 1px">
			<f:facet name="closed">
				<hx:jspPanel id="jspPanel2">
					<hx:graphicImageEx id="imageEx2" styleClass="graphicImageEx"
						align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
					<h:outputText id="text2" styleClass="outputText" value="Find Email"
						style="padding-left: 5px"></h:outputText>
				</hx:jspPanel>
			</f:facet>
			<f:facet name="opened">
				<hx:jspPanel id="jspPanel1">
					<hx:graphicImageEx id="imageEx1" styleClass="graphicImageEx"
						align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
					<h:outputText id="text1" styleClass="outputText" value="Find Email"
						style="padding-left: 5px"></h:outputText>
				</hx:jspPanel>
			</f:facet>
			<h:panelGrid styleClass="panelGrid" id="gridEmailFilter" columns="2"
				cellpadding="2">

				<f:facet name="footer">
					<h:panelGrid styleClass="panelGrid" id="gridEmailFilterFooter1"
						columns="1">
						<h:panelGrid styleClass="panelGrid" id="gridtwoColumn11" columns="2" cellpadding="3" cellspacing="3">
								<hx:commandExButton type="submit" value="Search For Email Records"
								styleClass="commandExButton" id="buttonSubmitSearch" action="#{pc_SubscriberAccountSearch_mainBodyArea.doButtonSubmitSearchAction}" style="cursor: pointer"></hx:commandExButton>
								<hx:commandExButton type="submit" value="Clear Search Results"
									styleClass="commandExButton" id="buttonClearSearchResults" action="#{pc_SubscriberAccountSearch_mainBodyArea.doButtonClearSearchResultsAction}" style="cursor: pointer"></hx:commandExButton>
							</h:panelGrid>
						<h:outputText styleClass="outputText" id="text26"
							value="* NOTE: This will search web records only. An email address may exist in One Call but not on the web database."
							escape="false"></h:outputText>
						<h:outputText styleClass="outputText" id="text27"
							value="Customer's may need to 'Set Up Online Account Access' if no email records are found."></h:outputText>
					</h:panelGrid>
				</f:facet>
				<h:outputText styleClass="outputText" id="textEmailLabel"
					value="Email Address Like: "></h:outputText>
				<h:inputText styleClass="inputText" id="textEmailAddressFilter"
					size="40" value="#{emailSearchFilterHandler.emailAddressFilter}"
					tabindex="1"
					title="Searches for emails containing the entered filter"></h:inputText>
				<h:outputText styleClass="outputText" id="textAccountNumberFilter"
					value="Account Number: "></h:outputText>
				<h:inputText styleClass="inputText" id="text4"
					value="#{emailSearchFilterHandler.accountNumber}" tabindex="2"
					title="Exact Match"></h:inputText>
				<h:outputText styleClass="outputText" id="textCheckTrialDB"
					value="Check Trial Users:"></h:outputText>
				<h:selectBooleanCheckbox styleClass="selectBooleanCheckbox"
					id="checkboxCheckTrialTable"
					value="#{emailSearchFilterHandler.isCheckTrials}" tabindex="3"></h:selectBooleanCheckbox>
			</h:panelGrid>
		</hx:panelSection>
		<br>
		<hx:panelSection styleClass="panelSection"
				id="sectionCustomerResultsSection" initClosed="false">
				<hx:dataTableEx border="0" cellpadding="4" cellspacing="2"
					columnClasses="columnClass1" headerClass="headerClass"
					footerClass="footerClass" rowClasses="rowClass1, rowClass3"
					styleClass="dataTableEx" id="tableExEmailSearchResults"
					var="varcustomerSearchResults"
					value="#{emailSearchHandler.customerSearchResults}"
					rendered="#{emailSearchHandler.hasCustomerSearchResults}">
					<f:facet name="header">
						<hx:panelBox styleClass="panelBox" id="box2">
							<h:panelGrid styleClass="panelGrid"
								id="gridEmailCustomerHeaderGrid" columns="2">
								<h:outputText styleClass="outputText" id="text25"
									value="Link to Subscriber Login Page: "
									style="font-weight: bold"></h:outputText>
								<hx:outputLinkEx value="/login/login.jsp"
									styleClass="outputLinkEx" id="linkEx2" target="_blank">
									<h:outputText id="text30" styleClass="outputText"
										value="https://service.usatoday.com/login/login.jsp"></h:outputText>
								</hx:outputLinkEx>
							</h:panelGrid>
						</hx:panelBox>
					</f:facet>
					<hx:columnEx id="columnEx1" valign="top">
						<f:facet name="header">
							<h:outputText id="text3" styleClass="outputText"
								value="Email Address"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textEmailAddress"
							value="#{varcustomerSearchResults.emailAddress}"
							style="font-weight: bold"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx2">
						<f:facet name="header">
							<h:outputText value="Account Number" styleClass="outputText"
								id="text5"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textAcctNum"
							value="#{varcustomerSearchResults.accountNumber}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx3">
						<f:facet name="header">
							<h:outputText value="Publication Code" styleClass="outputText"
								id="text6"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textPubCode"
							value="#{varcustomerSearchResults.pubCode}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx4">
						<f:facet name="header">
							<h:outputText value="Gift Flag" styleClass="outputText"
								id="text7"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText" id="textGiftFlag"
							value="#{varcustomerSearchResults.giftRecordFlag}"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx5" align="center">
						<f:facet name="header">
							<h:outputText value="Reset Password" styleClass="outputText"
								id="text8"></h:outputText>
						</f:facet>
						<hx:commandExButton type="submit" value="Reset Password"
							styleClass="commandExButton" id="buttonResetPassword"
							action="#{pc_SubscriberAccountSearch_mainBodyArea.doButtonResetPasswordAction}"
							confirm="This will reset the password on this account to 'password'. If multiples exist you will be taken to a page for further clarification.">
							<f:param value="#{varcustomerSearchResults.emailAddress}"
								name="emailAddress" id="paramSelectedEmailAddress"></f:param>
						</hx:commandExButton>
					</hx:columnEx>
					<hx:columnEx id="columnEx7" align="center" nowrap="true"
						style="padding-left: 3px; padding-right: 3px">
						<hx:commandExRowEdit styleClass="commandExButton"
							id="rowEditEmailAddress" editLabel="Change Email"
							action="#{pc_SubscriberAccountSearch_mainBodyArea.doRowEditEmailAddressAction}"
							style="padding: 0px;">
							<hx:jspPanel id="jspPanel7">
								<hx:panelFormBox helpPosition="over" labelPosition="left"
									styleClass="panelFormBox" id="formBoxEditEmailAddress"
									label="Change Email Address" style="width: 300px">
									<hx:formItem styleClass="formItem" id="formItemEmailAddress"
										label="New Email:" style="padding-right: 10px">
										<h:inputText styleClass="inputText" id="text31NewEmail"
											value="#{varcustomerSearchResults.emailAddress}"></h:inputText>
									</hx:formItem>
									<f:facet name="bottom">
										<h:outputText styleClass="outputText" id="text31"
											value="It may take up to 3 days for the<br />change to take affect for the Daily Buzz."
											escape="false"></h:outputText>
									</f:facet>
								</hx:panelFormBox>
							</hx:jspPanel>
						</hx:commandExRowEdit>
						<f:facet name="header">
							<h:outputText styleClass="outputText" id="text32"
								value="Change Email"></h:outputText>
						</f:facet>
					</hx:columnEx>
					<hx:columnEx id="columnEx15" align="center">
						<f:facet name="header">
						</f:facet>
						<hx:commandExButton type="submit" value="Send Confirmation Email"
							styleClass="commandExButton" id="buttonSendConfirmationEmail"
							action="#{pc_SubscriberAccountSearch_mainBodyArea.doButtonSendConfirmationEmailAction}"
							confirm="This will send an email to the customer with their credentials."></hx:commandExButton>
					</hx:columnEx>
				</hx:dataTableEx>
				<f:facet name="closed">
					<hx:jspPanel id="jspPanel6">
						<hx:graphicImageEx id="imageEx6" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
						<h:outputText id="text13" styleClass="outputText"
							value="Customer Results (#{emailSearchHandler.numberOfCustomerSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<f:facet name="opened">
					<hx:jspPanel id="jspPanel5">
						<hx:graphicImageEx id="imageEx5" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
						<h:outputText id="text12" styleClass="outputText"
							value="Customer Results (#{emailSearchHandler.numberOfCustomerSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
			</hx:panelSection>
			<hx:panelSection styleClass="panelSection"
				id="sectionTrialCustomerSection" initClosed="false" style="margin-top: 20px">
				<f:facet name="closed">
					<hx:jspPanel id="jspPanel4">
						<hx:graphicImageEx id="imageEx4" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_descon.gif"></hx:graphicImageEx>
						<h:outputText id="text10" styleClass="outputText"
							value="Trial Customer Results  (#{emailSearchHandler.numberOfTrialSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<f:facet name="opened">
					<hx:jspPanel id="jspPanel3">
						<hx:graphicImageEx id="imageEx3" styleClass="graphicImageEx"
							align="middle" value="/natcsweb/theme/img/JSF_sort_ascon.gif"></hx:graphicImageEx>
						<h:outputText id="text9" styleClass="outputText"
							value="Trial Customer Results  (#{emailSearchHandler.numberOfTrialSearchResults} Records)"></h:outputText>
					</hx:jspPanel>
				</f:facet>
				<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
					columnClasses="columnClass1" headerClass="headerClass"
					footerClass="footerClass" rowClasses="rowClass1, rowClass3"
					styleClass="dataTableEx" id="tableExTrialSearchResults"
					rendered="#{emailSearchHandler.hasTrialSearchResults}"
					value="#{emailSearchHandler.trialSearchResults}"
					var="vartrialSearchResults">
					<f:facet name="header">
						<hx:panelBox styleClass="panelBox" id="box1">
							<h:panelGrid styleClass="panelGrid" id="gridTrialDTHeaderGrid" columns="2">
								<h:outputText styleClass="outputText" id="text24" value="Link to Trial Login Page:" style="font-weight: bold"></h:outputText>
								<hx:outputLinkEx value="/login/trialLogin.faces"
									styleClass="outputLinkEx" id="linkEx1" target="_blank">
									<h:outputText id="text19" styleClass="outputText"
										value="http://service.usatoday.com/login/trialLogin.faces"></h:outputText>
								</hx:outputLinkEx>
							</h:panelGrid>
						</hx:panelBox>
					</f:facet>
					<hx:columnEx id="columnEx5">
						<f:facet name="header">
							<h:outputText id="text11" styleClass="outputText"
								value="Email Address"></h:outputText>
						</f:facet>
						<h:outputText styleClass="outputText"
							id="textTrialUserEmailAddress"
							value="#{vartrialSearchResults.trialCustomer.emailAddress}"
							style="font-weight: bold"></h:outputText>
					</hx:columnEx>
					<hx:columnEx id="columnEx6">
						<f:facet name="header">
							<h:outputText value="Reset Password" styleClass="outputText"
								id="text14"></h:outputText>
						</f:facet>
						<hx:commandExButton type="submit" value="Reset Password"
							styleClass="commandExButton" id="buttonResetTrialPassword" action="#{pc_SubscriberAccountSearch_mainBodyArea.doButtonResetTrialPasswordAction}" confirm="This will reset the password on this account to 'password'.">
							<f:param value="#{vartrialSearchResults.trialCustomer.emailAddress}" name="EmailAddress" id="param1"></f:param>
						</hx:commandExButton>
					</hx:columnEx>
					<hx:columnEx id="columnEx8">
						<f:facet name="header">
							<h:outputText styleClass="outputText"
								id="textTrialPartnerNameColumnHeader"></h:outputText>
						</f:facet>
						<hx:dataTableEx border="0" cellpadding="2" cellspacing="0"
							columnClasses="columnClass1" headerClass="headerClass"
							footerClass="footerClass" rowClasses="rowClass1, rowClass3"
							styleClass="dataTableEx" id="tableExSampleInnerTable"
							value="#{vartrialSearchResults.allTrials}" var="varallTrials"
							style="border-left-color: black; border-left-width: 1px; border-left-style: dotted">
							<hx:columnEx id="columnEx9" width="250">
								<f:facet name="header">
									<h:outputText id="text15" styleClass="outputText"
										value="Partner Name"></h:outputText>
								</f:facet>
								<h:panelGrid styleClass="panelGrid" id="gridPartnernameurlGrid1" columns="1">
									<h:outputText styleClass="outputText" id="text20"
										value="#{varallTrials.trial.partner.partnerName}" rendered="#{not varallTrials.trialPartnershipActive}"></h:outputText>
									<hx:outputLinkEx styleClass="outputLinkEx" id="linkEx3" value="#{varallTrials.partnerSignUpURL}" target="_blank" rendered="#{varallTrials.trialPartnershipActive}">
										<h:outputText id="text33" styleClass="outputText"
											value="#{varallTrials.trial.partner.partnerName}"></h:outputText>
									</hx:outputLinkEx>
								</h:panelGrid>
							</hx:columnEx>
							<hx:columnEx id="columnEx12">
								<f:facet name="header">
									<h:outputText value="Partner ID" styleClass="outputText"
										id="text34"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text35" value="#{varallTrials.trial.partner.partnerID}"></h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx10" align="center">
								<f:facet name="header">
									<h:outputText value="Start Date" styleClass="outputText"
										id="text16"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text21"
									value="#{varallTrials.startDate}">
									<hx:convertDateTime dateStyle="medium" />
								</h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx11" align="center">
								<f:facet name="header">
									<h:outputText value="End Date" styleClass="outputText"
										id="text17"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text22"
									value="#{varallTrials.endDate}">
									<hx:convertDateTime dateStyle="medium" />
								</h:outputText>
							</hx:columnEx>
							<hx:columnEx id="columnEx13" align="center">
								<h:outputText styleClass="outputText" id="text23"
									value="#{varallTrials.trialDurationInDays}"></h:outputText>
								<f:facet name="header">
									<h:outputText value="Duration (days)" styleClass="outputText"
										id="text18"></h:outputText>
								</f:facet>
							</hx:columnEx>
							<hx:columnEx id="columnEx14">
								<f:facet name="header">
									<h:outputText value="NCS Rep ID" styleClass="outputText"
										id="text28"></h:outputText>
								</f:facet>
								<h:outputText styleClass="outputText" id="text29"
									value="#{varallTrials.trial.ncsRepID}"></h:outputText>
							</hx:columnEx>
						</hx:dataTableEx>
					</hx:columnEx>
				</hx:dataTableEx>
			</hx:panelSection>
		
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>