<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%><%-- tpl:metadata --%>
	<%-- jsf:pagecode language="java" location="/JavaSource/pagecode/tilesContent/Subscriptions_mainBodyarea.java" --%><%-- /jsf:pagecode --%>
<%-- /tpl:metadata --%><%@taglib
	uri="http://www.ibm.com/siteedit/sitelib" prefix="siteedit"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib
	uri="http://java.sun.com/jsf/html" prefix="h"%><%@taglib
	uri="http://www.ibm.com/jsf/html_extended" prefix="hx"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><hx:viewFragment id="viewFragment1"><jsp:useBean
		id="now" class="java.util.Date" scope="session" />
	<hx:scriptCollector id="scriptCollector1">
		<h:form styleClass="form" id="form1">
			<h:messages styleClass="messages" id="messages1"
				style="font-weight: bold; font-size: 12pt"></h:messages>
			<hx:panelFormBox helpPosition="over" labelPosition="left"
				styleClass="panelFormBox outlinedTable" id="formBoxNewSubForm"
				label="#{labels.newSubscriptions}" widthLabel="100">


				<hx:formItem styleClass="formItem" id="formItemPubCodeEntry"
					label="*Publication:" infoText='"*" Denotes a Required Field'>
					<h:selectOneMenu id="menuPubSelectionMenu"
						styleClass="selectOneMenu" value="#{newStartFormHandler.pubCode}">
						<f:selectItem itemLabel="USA TODAY" itemValue="UT"
							id="selectItem2" />
						<f:selectItem itemLabel="Sports Weekly" itemValue="BW"
							id="selectItem3" />
					</h:selectOneMenu>
				</hx:formItem>
				<hx:formItem styleClass="formItem" id="formItemOfferCode"
					label="*Offer Code:">
					<h:inputText styleClass="inputText" id="textOCodeText"
						value="#{newStartFormHandler.offerCode}" maxlength="3" size="5">
						<f:validateLength minimum="3" maximum="3"></f:validateLength>
					</h:inputText>
				</hx:formItem>
				<f:facet name="bottom">
					<hx:commandExButton type="submit"
						value="Check Offer and Display Link" styleClass="commandExButton"
						id="buttonSubmitOfferCheck"
						style="margin-left: 30px; margin-bottom: 4px" action="#{pc_Subscriptions_mainBodyarea.doButtonSubmitOfferCheckAction}"></hx:commandExButton>
				</f:facet>
				<br>
				<f:facet name="right">
					<h:panelGrid styleClass="panelGrid" id="gridRightSideForLink">
						<hx:outputLinkEx value="#{subscriberPortalLink.linkURL}"
							styleClass="outputLinkEx" id="linkExSubscribeLink"
							rendered="#{subscriberPortalLink.showURL}" target="_blank">
							<h:outputText id="textPubLink" styleClass="outputText"
								value="Open Offer (Pub: #{newStartFormHandler.pubCode} Offer Code: #{newStartFormHandler.offerCode})"
								style="font-size: 12pt"></h:outputText>
						</hx:outputLinkEx>
					</h:panelGrid>
				</f:facet>
			</hx:panelFormBox>


			<hx:behaviorKeyPress key="Enter" id="behaviorKeyPress1"
				behaviorAction="click" targetAction="buttonSubmitOfferCheck"></hx:behaviorKeyPress>
			<hx:inputHelperSetFocus id="setFocus1" target="textOCodeText"></hx:inputHelperSetFocus>
			<br>
			
			<br>
			<br>
			<br>
			<br>
			<br>
		</h:form>
	</hx:scriptCollector>
</hx:viewFragment>