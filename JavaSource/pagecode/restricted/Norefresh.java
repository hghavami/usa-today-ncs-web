/*
 * Created on Nov 21, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.restricted;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
/**
 * @author aeast
 * @date Nov 21, 2007
 * @class restrictedNorefresh
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class Norefresh extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText text1;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
}