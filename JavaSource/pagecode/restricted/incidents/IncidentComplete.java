/*
 * Created on Nov 7, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.restricted.incidents;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlCommandExButton;
/**
 * @author aeast
 * @date Nov 7, 2007
 * @class restrictedIncidentComplete
 * 
 * 
 */
public class IncidentComplete extends PageCodeBase {

    protected HtmlOutputText textIncidentInfoLabel;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlOutputText text1;
    protected HtmlOutputText text2;
    protected HtmlOutputText text3;
    protected HtmlOutputFormat formatIncidentID;
    protected HtmlOutputText text4;
    protected HtmlOutputText text5;
    protected HtmlOutputText text6;
    protected HtmlOutputText text7;
    protected HtmlOutputText text8;
    protected HtmlOutputText text9;
    protected HtmlOutputText text10;
    protected HtmlForm form1;
    protected HtmlCommandExButton buttonCreateNewIncident;
    protected HtmlForm form2;
    protected HtmlOutputText text11;
    protected HtmlOutputText getTextIncidentInfoLabel() {
        if (textIncidentInfoLabel == null) {
            textIncidentInfoLabel = (HtmlOutputText) findComponentInRoot("textIncidentInfoLabel");
        }
        return textIncidentInfoLabel;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlOutputText getText1() {
        if (text1 == null) {
            text1 = (HtmlOutputText) findComponentInRoot("text1");
        }
        return text1;
    }
    protected HtmlOutputText getText2() {
        if (text2 == null) {
            text2 = (HtmlOutputText) findComponentInRoot("text2");
        }
        return text2;
    }
    protected HtmlOutputText getText3() {
        if (text3 == null) {
            text3 = (HtmlOutputText) findComponentInRoot("text3");
        }
        return text3;
    }
    protected HtmlOutputFormat getFormatIncidentID() {
        if (formatIncidentID == null) {
            formatIncidentID = (HtmlOutputFormat) findComponentInRoot("formatIncidentID");
        }
        return formatIncidentID;
    }
    protected HtmlOutputText getText4() {
        if (text4 == null) {
            text4 = (HtmlOutputText) findComponentInRoot("text4");
        }
        return text4;
    }
    protected HtmlOutputText getText5() {
        if (text5 == null) {
            text5 = (HtmlOutputText) findComponentInRoot("text5");
        }
        return text5;
    }
    protected HtmlOutputText getText6() {
        if (text6 == null) {
            text6 = (HtmlOutputText) findComponentInRoot("text6");
        }
        return text6;
    }
    protected HtmlOutputText getText7() {
        if (text7 == null) {
            text7 = (HtmlOutputText) findComponentInRoot("text7");
        }
        return text7;
    }
    protected HtmlOutputText getText8() {
        if (text8 == null) {
            text8 = (HtmlOutputText) findComponentInRoot("text8");
        }
        return text8;
    }
    protected HtmlOutputText getText9() {
        if (text9 == null) {
            text9 = (HtmlOutputText) findComponentInRoot("text9");
        }
        return text9;
    }
    protected HtmlOutputText getText10() {
        if (text10 == null) {
            text10 = (HtmlOutputText) findComponentInRoot("text10");
        }
        return text10;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlCommandExButton getButtonCreateNewIncident() {
        if (buttonCreateNewIncident == null) {
            buttonCreateNewIncident = (HtmlCommandExButton) findComponentInRoot("buttonCreateNewIncident");
        }
        return buttonCreateNewIncident;
    }
    public String doButtonCreateNewIncidentAction() {
        // Type Java code that runs when the component is clicked

        this.getRequestScope().remove("newIncident");
        return "success";
    }
    protected HtmlForm getForm2() {
        if (form2 == null) {
            form2 = (HtmlForm) findComponentInRoot("form2");
        }
        return form2;
    }
    protected HtmlOutputText getText11() {
        if (text11 == null) {
            text11 = (HtmlOutputText) findComponentInRoot("text11");
        }
        return text11;
    }
}