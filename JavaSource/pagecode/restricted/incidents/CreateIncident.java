/*
 * Created on Nov 7, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.restricted.incidents;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.ncs.bo.IncidentBO;
import com.usatoday.ncs.bo.MarketBO;
import com.usatoday.ncs.so.MarketLocatorService;
import com.usatoday.ncs.so.MarketNotificationService;
import com.usatoday.ncs.web.handlers.UserHandler;
import com.usatoday.ncs.web.handlers.UniqueRequestHandler;
import com.usatoday.ncs.web.handlers.IncidentRequestTypeHandler;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.UISelectItem;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlCommandLink;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlInputHidden;
/**
 * @author aeast
 * @date Nov 7, 2007
 * @class restrictedCreateIncident
 * 
 * 
 */
public class CreateIncident extends PageCodeBase {

    protected IncidentBO newIncident;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected HtmlMessages messages1;
    protected IncidentRequestTypeHandler incidentRequestType;
    protected HtmlOutputText textIncidentInfoLabel;
    protected HtmlOutputText text14;
    protected HtmlOutputText textCustomerHeaderLabel;
    protected UniqueRequestHandler uniqueRequest;
	protected UniqueRequestHandler lastUniqueRequest;
	protected HtmlInputText textOffer;
	protected HtmlCommandExButton button1;
	protected HtmlCommandExButton button2;
	protected HtmlCommandExButton button3;
	protected HtmlCommandExButton button4;
	protected HtmlSelectOneMenu menuEEditionComments;
	protected HtmlSelectOneMenu menuIncidentEducation;
	protected HtmlSelectOneMenu menuIncidentOrderFormRequestType;
	protected HtmlSelectOneMenu menuIncidentInvoiceRequestType;
	protected HtmlSelectOneMenu menuIncidentEmailPasswordRequestType;
	protected HtmlSelectOneMenu menuIncidentEmailAddressChangeRequestType;
	protected HtmlSelectOneMenu menuIncidentW9RequestType;
	protected HtmlSelectOneMenu menuIncidentBlueChip;
	protected UISelectItem selectItem1;
	protected HtmlScriptCollector scriptCollector2;
	protected HtmlForm formLogout;
	protected HtmlOutputFormat formatWelcomeMessage;
	protected HtmlOutputText textLogoutLink;
	protected HtmlInputText textIncidentDate;
	protected HtmlSelectOneMenu menu1;
	protected HtmlSelectOneMenu menuBlanks;
	protected HtmlSelectOneMenu menuIncidentHomeDeliveryRequestType;
	protected HtmlSelectOneMenu menuIncidentNewsstandRequestType;
	protected HtmlSelectOneMenu menuIncidentRack;
	protected HtmlSelectOneMenu menuIncidentAgentInquiry;
	protected HtmlInputTextarea textarea1;
	protected HtmlMessage message3;
	protected HtmlSelectBooleanCheckbox checkbox1;
	protected HtmlOutputText textRefundCheckboxInfo;
	protected HtmlInputText text1;
	protected HtmlMessage message2;
	protected HtmlInputText text2;
	protected HtmlInputText text3;
	protected HtmlSelectOneMenu menu2;
	protected HtmlInputText text4;
	protected HtmlOutputText text13;
	protected HtmlMessage message1;
	protected HtmlSelectBooleanCheckbox checkbox2;
	protected HtmlInputText text5;
	protected HtmlInputText textCustLastName;
	protected HtmlInputText text6;
	protected HtmlInputText text7;
	protected HtmlInputText text8;
	protected HtmlInputText text9;
	protected HtmlInputText text10;
	protected HtmlInputText text11;
	protected HtmlSelectOneMenu menu3;
	protected HtmlInputText text12;
	protected HtmlCommandExButton buttonCreateIncident;
	protected HtmlCommandLink linkLogout;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlCommandExButton buttonCancelNewIncident;
	protected HtmlInputHidden uniqueRequestValue;
	protected HtmlSelectOneMenu menuIncidentNatCustSvc;
	/** 
     * @managed-bean true
     */

    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    public String doButtonCreateIncidentAction() {
        // Type Java code that runs when the component is clicked

    	try {
	        IncidentBO incident = this.getNewIncident();
	        UserHandler user = (UserHandler)this.getSessionScope().get("user");
	        
	        UniqueRequestHandler lastUnique = this.getLastUniqueRequest();
	         
	        UniqueRequestHandler requestUnique = this.getUniqueRequest();
	        
	        if (lastUnique != null && requestUnique.getUnique().equals(lastUnique.getUnique())) {
	            return "norefresh";
	        }
	        else  {
		        try {
		            
		            incident.setEnteredBy(user.getUser().getUserName());
		            incident.setNcsSiteKey(user.getUser().getSiteKey());
		            
		            // get reqeust type 
		            if (incident.getIncidentType().equalsIgnoreCase("Home Delivery")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getHomeDelivery());
		            }
		            if (incident.getIncidentType().equalsIgnoreCase("Alternate Delivery")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getAlternateDelivery());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Newsstand")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getNewsstand());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Order Form Request")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getOrderFormRequest());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("e-Newspaper")){
		            	//e-Newspaper Feedback
		            	incident.setIncidentRequestType(this.getIncidentRequestType().getEEdition());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("e-Leads")){
		            	//e-Leads Feedback
		            	incident.setIncidentRequestType(this.getIncidentRequestType().geteLeads());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Invoice Request")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getInvoice());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Email Password Request")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getEmailPassword());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Email Address Change Request")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getEmailAddressChange());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("W9 Form Request")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getW9RequestForm());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Rack")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getRack());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Agent Inquiry")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getAgentInquiry());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("Education")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getEducation());
		            }
		            else if (incident.getIncidentType().equalsIgnoreCase("For National Customer Service")) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getNatCust());
		            }
		            else if (incident.getIncidentType().indexOf("Blue Chip") > -1) {
		                incident.setIncidentRequestType(this.getIncidentRequestType().getBlueChip());
		            }
		            
		            MarketLocatorService locator = new MarketLocatorService();
		            MarketBO market = null;
		            
		            // "Order Form Request" types should always go to NCS
		            if (incident.getIncidentType().equalsIgnoreCase("Order Form Request") || 
		            	incident.getIncidentType().equalsIgnoreCase("Invoice Request") || 
		            	incident.getIncidentType().equalsIgnoreCase("Email Password Request") ||
		            	incident.getIncidentType().equalsIgnoreCase("Email Address Change Request") ||
		            	incident.getIncidentType().equalsIgnoreCase("For National Customer Service") ||
		            	incident.getIncidentType().equalsIgnoreCase("e-Newspaper") ||
						incident.getIncidentType().equalsIgnoreCase("W9 Form Request") )  {
		            	try {
			                market = locator.locateMarketByZip("00000");
			                incident.setMarketAssigned(market);
			                incident.setMarketKey(market.getKey());
			            }
			            catch (Exception expe) {
			                // if fail to get a market then use 0s to get the national ncs
			            	  System.out.println("NCS WEB: Market assignation failed  ");
			                  
			            }		            
		            } else {
			            try {
			                market = locator.locateMarketByZip(incident.getIncidentZip());
			                incident.setMarketAssigned(market);
			                incident.setMarketKey(market.getKey());
			            }
			            catch (Exception expe) {
			                // if fail to get a market then use 0s to get the national ncs
			                market = locator.locateMarketByZip("00000");
			                incident.setMarketAssigned(market);
			                incident.setMarketKey(market.getKey());
			            }		            
		            }
		            
		            incident.save();
		            
		            // update the last unique request saved.
		            lastUnique.setUnique(requestUnique.getUnique());
		            
		            MarketNotificationService mns = new MarketNotificationService();
		          
		            try {
		                mns.notifyMarket(incident, market);
		            }
		            catch (Exception emaile) {
	                    System.out.println("NCS WEB: Market notification failed but incident saved successfully. " + emaile.getMessage());
	                }
		            
		        }
		        catch (Exception e) {
		        	System.out.println("NCS WEB: Market notification exception. " + e.getMessage());
		            FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed to Save Incident. " + e.getMessage(), null);
		            context.addMessage(null, message);
		            
		            return "failure";
		        }
	        }
	     }
	     catch (Exception exp) {
	     	System.out.println("NCS WEB: Market notification exception outter catch:  " + exp.getMessage());
		            FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected Exception occurred:  " + exp.getMessage(), null);
		            context.addMessage(null, message);
		            
		            return "failure";
	     }
	    
        return "success";
    }
    public String doButtonCancelNewIncidentAction() {
        // Type Java code that runs when the component is clicked

        return "";
    }
    /** 
     * @managed-bean true
     */
 
    protected HtmlOutputText getTextIncidentInfoLabel() {
        if (textIncidentInfoLabel == null) {
            textIncidentInfoLabel = (HtmlOutputText) findComponentInRoot("textIncidentInfoLabel");
        }
        return textIncidentInfoLabel;
    }
    protected HtmlOutputText getText14() {
        if (text14 == null) {
            text14 = (HtmlOutputText) findComponentInRoot("text14");
        }
        return text14;
    }
    protected HtmlOutputText getTextCustomerHeaderLabel() {
        if (textCustomerHeaderLabel == null) {
            textCustomerHeaderLabel = (HtmlOutputText) findComponentInRoot("textCustomerHeaderLabel");
        }
        return textCustomerHeaderLabel;
    }
    /** 
	 * @managed-bean true
	 */
	protected IncidentBO getNewIncident() {
		if (newIncident == null) {
			newIncident = (IncidentBO) getManagedBean("newIncident");
		}
		return newIncident;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setNewIncident(IncidentBO newIncident) {
		this.newIncident = newIncident;
	}
	/** 
	 * @managed-bean true
	 */
	protected IncidentRequestTypeHandler getIncidentRequestType() {
		if (incidentRequestType == null) {
			incidentRequestType = (IncidentRequestTypeHandler) getManagedBean("incidentRequestType");
		}
		return incidentRequestType;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setIncidentRequestType(
			IncidentRequestTypeHandler incidentRequestType) {
		this.incidentRequestType = incidentRequestType;
	}
	/** 
	 * @managed-bean true
	 */
	protected UniqueRequestHandler getUniqueRequest() {
		if (uniqueRequest == null) {
			uniqueRequest = (UniqueRequestHandler) getManagedBean("uniqueRequest");
		}
		return uniqueRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUniqueRequest(UniqueRequestHandler uniqueRequest) {
		this.uniqueRequest = uniqueRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected UniqueRequestHandler getLastUniqueRequest() {
		if (lastUniqueRequest == null) {
			lastUniqueRequest = (UniqueRequestHandler) getManagedBean("lastUniqueRequest");
		}
		return lastUniqueRequest;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setLastUniqueRequest(UniqueRequestHandler lastUniqueRequest) {
		this.lastUniqueRequest = lastUniqueRequest;
	}
	protected HtmlInputText getTextOffer() {
		if (textOffer == null) {
			textOffer = (HtmlInputText) findComponentInRoot("textOffer");
		}
		return textOffer;
	}
	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}
	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}
	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}
	protected HtmlCommandExButton getButton4() {
		if (button4 == null) {
			button4 = (HtmlCommandExButton) findComponentInRoot("button4");
		}
		return button4;
	}
	protected HtmlSelectOneMenu getMenuEEditionComments() {
		if (menuEEditionComments == null) {
			menuEEditionComments = (HtmlSelectOneMenu) findComponentInRoot("menuEEditionComments");
		}
		return menuEEditionComments;
	}
	protected HtmlSelectOneMenu getMenuIncidentEducation() {
		if (menuIncidentEducation == null) {
			menuIncidentEducation = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentEducation");
		}
		return menuIncidentEducation;
	}
	protected HtmlSelectOneMenu getMenuIncidentOrderFormRequestType() {
		if (menuIncidentOrderFormRequestType == null) {
			menuIncidentOrderFormRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentOrderFormRequestType");
		}
		return menuIncidentOrderFormRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentInvoiceRequestType() {
		if (menuIncidentInvoiceRequestType == null) {
			menuIncidentInvoiceRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentInvoiceRequestType");
		}
		return menuIncidentInvoiceRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentEmailPasswordRequestType() {
		if (menuIncidentEmailPasswordRequestType == null) {
			menuIncidentEmailPasswordRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentEmailPasswordRequestType");
		}
		return menuIncidentEmailPasswordRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentEmailAddressChangeRequestType() {
		if (menuIncidentEmailAddressChangeRequestType == null) {
			menuIncidentEmailAddressChangeRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentEmailAddressChangeRequestType");
		}
		return menuIncidentEmailAddressChangeRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentW9RequestType() {
		if (menuIncidentW9RequestType == null) {
			menuIncidentW9RequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentW9RequestType");
		}
		return menuIncidentW9RequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentBlueChip() {
		if (menuIncidentBlueChip == null) {
			menuIncidentBlueChip = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentBlueChip");
		}
		return menuIncidentBlueChip;
	}
	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}
	protected HtmlScriptCollector getScriptCollector2() {
		if (scriptCollector2 == null) {
			scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
		}
		return scriptCollector2;
	}
	protected HtmlForm getFormLogout() {
		if (formLogout == null) {
			formLogout = (HtmlForm) findComponentInRoot("formLogout");
		}
		return formLogout;
	}
	protected HtmlOutputFormat getFormatWelcomeMessage() {
		if (formatWelcomeMessage == null) {
			formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
		}
		return formatWelcomeMessage;
	}
	protected HtmlOutputText getTextLogoutLink() {
		if (textLogoutLink == null) {
			textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
		}
		return textLogoutLink;
	}
	protected HtmlInputText getTextIncidentDate() {
		if (textIncidentDate == null) {
			textIncidentDate = (HtmlInputText) findComponentInRoot("textIncidentDate");
		}
		return textIncidentDate;
	}
	protected HtmlSelectOneMenu getMenu1() {
		if (menu1 == null) {
			menu1 = (HtmlSelectOneMenu) findComponentInRoot("menu1");
		}
		return menu1;
	}
	protected HtmlSelectOneMenu getMenuBlanks() {
		if (menuBlanks == null) {
			menuBlanks = (HtmlSelectOneMenu) findComponentInRoot("menuBlanks");
		}
		return menuBlanks;
	}
	protected HtmlSelectOneMenu getMenuIncidentHomeDeliveryRequestType() {
		if (menuIncidentHomeDeliveryRequestType == null) {
			menuIncidentHomeDeliveryRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentHomeDeliveryRequestType");
		}
		return menuIncidentHomeDeliveryRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentNewsstandRequestType() {
		if (menuIncidentNewsstandRequestType == null) {
			menuIncidentNewsstandRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentNewsstandRequestType");
		}
		return menuIncidentNewsstandRequestType;
	}
	protected HtmlSelectOneMenu getMenuIncidentRack() {
		if (menuIncidentRack == null) {
			menuIncidentRack = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentRack");
		}
		return menuIncidentRack;
	}
	protected HtmlSelectOneMenu getMenuIncidentAgentInquiry() {
		if (menuIncidentAgentInquiry == null) {
			menuIncidentAgentInquiry = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentAgentInquiry");
		}
		return menuIncidentAgentInquiry;
	}
	protected HtmlInputTextarea getTextarea1() {
		if (textarea1 == null) {
			textarea1 = (HtmlInputTextarea) findComponentInRoot("textarea1");
		}
		return textarea1;
	}
	protected HtmlMessage getMessage3() {
		if (message3 == null) {
			message3 = (HtmlMessage) findComponentInRoot("message3");
		}
		return message3;
	}
	protected HtmlSelectBooleanCheckbox getCheckbox1() {
		if (checkbox1 == null) {
			checkbox1 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox1");
		}
		return checkbox1;
	}
	protected HtmlOutputText getTextRefundCheckboxInfo() {
		if (textRefundCheckboxInfo == null) {
			textRefundCheckboxInfo = (HtmlOutputText) findComponentInRoot("textRefundCheckboxInfo");
		}
		return textRefundCheckboxInfo;
	}
	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}
	protected HtmlMessage getMessage2() {
		if (message2 == null) {
			message2 = (HtmlMessage) findComponentInRoot("message2");
		}
		return message2;
	}
	protected HtmlInputText getText2() {
		if (text2 == null) {
			text2 = (HtmlInputText) findComponentInRoot("text2");
		}
		return text2;
	}
	protected HtmlInputText getText3() {
		if (text3 == null) {
			text3 = (HtmlInputText) findComponentInRoot("text3");
		}
		return text3;
	}
	protected HtmlSelectOneMenu getMenu2() {
		if (menu2 == null) {
			menu2 = (HtmlSelectOneMenu) findComponentInRoot("menu2");
		}
		return menu2;
	}
	protected HtmlInputText getText4() {
		if (text4 == null) {
			text4 = (HtmlInputText) findComponentInRoot("text4");
		}
		return text4;
	}
	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}
	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}
	protected HtmlSelectBooleanCheckbox getCheckbox2() {
		if (checkbox2 == null) {
			checkbox2 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox2");
		}
		return checkbox2;
	}
	protected HtmlInputText getText5() {
		if (text5 == null) {
			text5 = (HtmlInputText) findComponentInRoot("text5");
		}
		return text5;
	}
	protected HtmlInputText getTextCustLastName() {
		if (textCustLastName == null) {
			textCustLastName = (HtmlInputText) findComponentInRoot("textCustLastName");
		}
		return textCustLastName;
	}
	protected HtmlInputText getText6() {
		if (text6 == null) {
			text6 = (HtmlInputText) findComponentInRoot("text6");
		}
		return text6;
	}
	protected HtmlInputText getText7() {
		if (text7 == null) {
			text7 = (HtmlInputText) findComponentInRoot("text7");
		}
		return text7;
	}
	protected HtmlInputText getText8() {
		if (text8 == null) {
			text8 = (HtmlInputText) findComponentInRoot("text8");
		}
		return text8;
	}
	protected HtmlInputText getText9() {
		if (text9 == null) {
			text9 = (HtmlInputText) findComponentInRoot("text9");
		}
		return text9;
	}
	protected HtmlInputText getText10() {
		if (text10 == null) {
			text10 = (HtmlInputText) findComponentInRoot("text10");
		}
		return text10;
	}
	protected HtmlInputText getText11() {
		if (text11 == null) {
			text11 = (HtmlInputText) findComponentInRoot("text11");
		}
		return text11;
	}
	protected HtmlSelectOneMenu getMenu3() {
		if (menu3 == null) {
			menu3 = (HtmlSelectOneMenu) findComponentInRoot("menu3");
		}
		return menu3;
	}
	protected HtmlInputText getText12() {
		if (text12 == null) {
			text12 = (HtmlInputText) findComponentInRoot("text12");
		}
		return text12;
	}
	protected HtmlCommandExButton getButtonCreateIncident() {
		if (buttonCreateIncident == null) {
			buttonCreateIncident = (HtmlCommandExButton) findComponentInRoot("buttonCreateIncident");
		}
		return buttonCreateIncident;
	}
	protected HtmlCommandLink getLinkLogout() {
		if (linkLogout == null) {
			linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
		}
		return linkLogout;
	}
	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}
	protected HtmlCommandExButton getButtonCancelNewIncident() {
		if (buttonCancelNewIncident == null) {
			buttonCancelNewIncident = (HtmlCommandExButton) findComponentInRoot("buttonCancelNewIncident");
		}
		return buttonCancelNewIncident;
	}
	protected HtmlInputHidden getUniqueRequestValue() {
		if (uniqueRequestValue == null) {
			uniqueRequestValue = (HtmlInputHidden) findComponentInRoot("uniqueRequestValue");
		}
		return uniqueRequestValue;
	}
	protected HtmlSelectOneMenu getMenuIncidentNatCustSvc() {
		if (menuIncidentNatCustSvc == null) {
			menuIncidentNatCustSvc = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentNatCustSvc");
		}
		return menuIncidentNatCustSvc;
	}
}