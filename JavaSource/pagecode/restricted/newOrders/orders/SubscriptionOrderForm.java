/**
 * 
 */
package pagecode.restricted.newOrders.orders;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlAjaxRefreshRequest;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.paytec.webapp.util.CreditCardUtility;
import com.usatoday.UsatException;
import com.usatoday.business.interfaces.UIAddressIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.payment.PaymentMethodIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.CustomerBO;
import com.usatoday.businessObjects.customer.CustomerInterimInfoBO;
import com.usatoday.businessObjects.customer.DeliveryMethodBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.UIAddressBO;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.shopping.CheckOutService;
import com.usatoday.businessObjects.shopping.SubscriptionOrderItemBO;
import com.usatoday.businessObjects.shopping.payment.CreditCardPaymentMethodBO;
import com.usatoday.businessObjects.shopping.payment.InvoicePaymentMethodBO;
import com.usatoday.businessObjects.util.KeyCodeParser;
import com.usatoday.ncs.web.subscription.handlers.InterimCustomerHandler;
import com.usatoday.ncs.web.subscription.handlers.NewSubscriptionOrderHandler;
import com.usatoday.ncs.web.subscription.handlers.ShoppingCartHandler;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferHandler;
import com.usatoday.ncs.web.subscription.handlers.UTCommon;
import com.usatoday.util.constants.UsaTodayConstants;

/**
 * @author aeast
 *
 */
public class SubscriptionOrderForm extends PageCodeBase {

	protected HtmlForm formOrderEntryForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlJspPanel jspPanelHeaderPanel;
	protected HtmlOutputText textMainTableHeader;
	protected HtmlPanelLayout layoutPageLayout;
	protected HtmlPanelGroup groupMainBodyPanel;
	protected HtmlPanelSection sectionNewOfferCode1;
	protected HtmlJspPanel jspPanel4;
	protected HtmlOutputText textOfferOverrridePanelHeaderClosedText;
	protected HtmlJspPanel jspPanel3;
	protected HtmlPanelFormBox formBoxOfferCodeEntry1;
	protected HtmlFormItem formItem1;
	protected HtmlInputText textNewOfferCode;
	protected UIParameter custOfferCodeParm;
	protected HtmlPanelGrid gridNewOfferBottomGrid1;
	protected HtmlCommandLink linkRevertOfferLink;
	protected HtmlOutputText textRevertOfferLinkText;
	protected HtmlPanelFormBox formBoxDeliveryInformation;
	protected HtmlFormItem formItemDeliveryFirstName;
	protected HtmlInputText textDeliveryFirstName;
	protected HtmlInputText textDeliveryCompanyName;
	protected HtmlInputText textDeliveryAddress1Text;
	protected HtmlInputText textDeliveryAddress2;
	protected HtmlInputText textDeliveryCity;
	protected HtmlSelectOneMenu menuDeliveryState;
	protected HtmlInputText textDeliveryPhoneAreaCode;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputHelperAssist assist5;
	protected HtmlInputText textDeliveryWorkPhoneAreaCode;
	protected HtmlInputHelperAssist assist44;
	protected HtmlInputHelperAssist assist45;
	protected HtmlInputText textEmailAddressRecipient;
	protected HtmlInputText textEmailAddressConfirmRecipient;
	protected HtmlPanelGrid gridDeliveryAddressBottomFacet;
	protected HtmlPanelFormBox formBoxBillDifferentFromDelSelectionFormBox;
	protected HtmlFormItem formItemBillDifferentFromDelSelector;
	protected HtmlSelectBooleanCheckbox checkboxIsBillDifferentFromDelSelector;
	protected HtmlBehavior behavior1;
	protected HtmlPanelFormBox formBoxBillingAddress;
	protected HtmlFormItem formItemPayersEmailAddress;
	protected HtmlInputText textPurchaserEmailAddress;
	protected HtmlInputText textBillingFirstName;
	protected HtmlInputText textBillingCompanyName;
	protected HtmlInputText textBillingAddress1;
	protected HtmlInputText textBillingAddress2;
	protected HtmlInputText textBillingCity;
	protected HtmlSelectOneMenu menuBillingState;
	protected HtmlInputText textBillingPhoneAreaCode;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputHelperAssist assist3;
	protected HtmlPanelFormBox formBoxPaymentHeaderFormBox;
	protected HtmlSelectOneRadio radioPaymentMethod;
	protected HtmlOutputText textForceBillMeText;
	protected HtmlPanelGrid gridPaymentGridBottomFacet;
	protected HtmlGraphicImageEx imageExSpacerImage;
	protected HtmlFormItem formItemCreditCardNumber;
	protected HtmlInputText textCreditCardNumber;
	protected HtmlSelectOneMenu menuCCExpireMonth;
	protected HtmlPanelGrid panelGridCreditCardImageGrid;
	protected HtmlJspPanel jspPanelCreditCardImages;
	protected HtmlSelectOneRadio radioRenewalOptions;
	protected HtmlSelectOneRadio radioRenewalOptionsGift;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlOutputText textOfferDisclaimerText;
	protected HtmlOutputText textCOPPAText;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlOutputLinkEx linkExRightColImageSpot1Link;
	protected HtmlGraphicImageEx imageExRightColImage1;
	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlOutputLinkEx linkExRightColSpot2Link;
	protected HtmlGraphicImageEx imageExRightColImage2;
	protected HtmlOutputText RightColHTMLSpot2Text;
	protected HtmlOutputLinkEx linkExRightColSpot3Link;
	protected HtmlGraphicImageEx imageExRightColImage3;
	protected HtmlPanelGroup groupAddlAddrHelpGroup1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText textAdditionalAddrLearnMoreInfoText;
	protected HtmlBehavior behavior3;
	protected HtmlPanelGroup groupDelMethod1;
	protected HtmlPanelGroup groupSpecialOfferGroupBox;
	protected HtmlPanelGrid gridSpecialOfferLayoutGrid;
	protected HtmlOutputLinkEx linkExCloseSpecialOffer;
	protected HtmlOutputText textCloseSpecialOfferText;
	protected HtmlGraphicImageEx tooimageExSpecialOfferImage;
	protected HtmlOutputText textScriptInsert1;
	protected HtmlScriptCollector scriptCollectorMainOrderEntryCollector;
	protected HtmlPanelGrid gridOffOverrideGrid;
	protected HtmlInputHelperAssist assist7;
	protected HtmlCommandExButton buttonProcessOfferCode;
	protected HtmlPanelGrid gridDeliveryInformation;
	protected HtmlInputText textDeliveryLastName;
	protected HtmlFormItem formItemDeliveryCompanyName;
	protected HtmlFormItem formItemDeliveryAddress1;
	protected HtmlInputText textDeliveryAptSuite;
	protected HtmlFormItem formItemDeliveryAddress2;
	protected HtmlOutputText textAddrLearnMore;
	protected HtmlFormItem formItemDeliveryCity;
	protected HtmlFormItem formItemDeliveryState;
	protected HtmlInputText textDeliveryZip;
	protected HtmlFormItem formItemDeliveryPhone;
	protected HtmlInputText textDeliveryPhoneExchange;
	protected HtmlInputText textDeliveryPhoneExtension;
	protected HtmlFormItem formItemDeliveryWorkPhone;
	protected HtmlInputText textDeliveryWorkPhoneExchange;
	protected HtmlInputText textDeliveryWorkPhoneExtension;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlFormItem formItemEmailAddressConfirm;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid panelGridBillingDifferentThanDelGrid;
	protected HtmlPanelGrid gridBillingAddress;
	protected HtmlFormItem formItemBillingFirstName;
	protected HtmlInputText textBillingLastName;
	protected HtmlFormItem formItemBillingCompanyName;
	protected HtmlFormItem formItemBillingAddress1;
	protected HtmlInputText textBillingAptSuite;
	protected HtmlFormItem formItemBillingAddress2;
	protected HtmlFormItem formItemBillingCity;
	protected HtmlFormItem formItemBillingState;
	protected HtmlInputText textBillingZipCode;
	protected HtmlFormItem formItemBillingTelephone;
	protected HtmlInputText textBillingPhoneExchange;
	protected HtmlInputText textBillingPhoneExtension;
	protected HtmlPanelGrid gridPaymentInformationGridLabelGrid;
	protected HtmlFormItem formItemBillMeFormItem;
	protected HtmlPanelGrid gridForceBillMeInfoGrid;
	protected HtmlPanelGrid gridPaymentInformationGrid;
	protected HtmlPanelFormBox formBoxPaymentInfo;
	protected HtmlInputHelperAssist assist6;
	protected HtmlFormItem formItemCCExpirationDate;
	protected HtmlSelectOneMenu menuCCExpireYear;
	protected HtmlPanelGrid gridEZPAYOptionsGrid;
	protected HtmlOutputText textCCAgreement;
	protected HtmlOutputText textEZPayConfirm;
	protected HtmlOutputText textEzPayInfo;
	protected HtmlPanelGrid gridEZPAYOptionsGridGift;
	protected HtmlOutputText textCCAgreement2;
	protected HtmlOutputText textEzPayInfoGift;
	protected HtmlPanelGrid gridTrialDisclaimerGrid;
	protected HtmlPanelGrid gridOfferDisclaimerGrid;
	protected HtmlPanelGrid COPPAGrid;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlJspPanel jspPanelGeoTrustPanel;
	protected HtmlPanelGrid rightColImageSpot1;
	protected HtmlGraphicImageEx imageExRightColImage1NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot1Grid;
	protected HtmlPanelGrid gridVideoGrid;
	protected HtmlOutputText textEEVideoText;
	protected HtmlPanelGrid rightColImageSpot2;
	protected HtmlGraphicImageEx imageExRightColImage2NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot2Grid;
	protected HtmlPanelGrid rightColImageSpot3;
	protected HtmlGraphicImageEx imageExRightColImage3NoLink;
	protected HtmlPanelDialog dialogAdditionalAddrHelp;
	protected HtmlCommandExButton button3;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHidden startDate;
	protected HtmlJspPanel jspPanelPopOverlayPanel;
	protected HtmlOutputLinkEx linkExSpecialOfferLink;
	protected HtmlInputHidden showOverlayPopUp;
	protected HtmlInputHidden isForceBillMe;
	protected HtmlInputHidden paymentMethodHidden;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlScriptCollector scriptCollector2;
	protected HtmlForm formLogout;
	protected HtmlOutputFormat formatWelcomeMessage;
	protected HtmlOutputText textLogoutLink;
	protected HtmlPanelGrid gridProductInfoGrid;
	protected HtmlPanelGroup groupprodinfo1;
	protected HtmlCommandLink linkLogout;
	protected HtmlOutputText textPubCodeTextLabel;
	protected HtmlOutputText textProductCode;
	protected HtmlOutputText textProductName;
	protected HtmlOutputText textOfferProdName4;
	protected ShoppingCartHandler shoppingCartHandler;
	protected HtmlPanelGrid gridDeliveryInformationPanelFooterGrid;
	protected HtmlCommandExButton buttonGetDeliveryMethodButton;
	protected HtmlOutputText textDeliveryMethod;
	protected HtmlPanelGrid gridDeliveryMethodInfoGrid;
	protected HtmlOutputLinkEx linkExDelMethodCheckHelpLink;
	protected HtmlOutputText textDelMethodHelpText;
	protected HtmlPanelGrid gridDelMethodResultsGrid;
	protected HtmlOutputText textDeliveryMethodTextDeterminedValue;
	protected HtmlGraphicImageEx imageExAmEx1;
	protected HtmlGraphicImageEx imageExDiscover1;
	protected HtmlGraphicImageEx imageExMasterCard1;
	protected HtmlGraphicImageEx imageExVisaLogo1;
	protected HtmlPanelGrid gridDelMethod2;
	protected HtmlOutputText textDelMethodDetailHelp;
	protected HtmlBehavior behaviorDelMethod4;
	protected HtmlPanelDialog dialogDelMethodDialog;
	protected HtmlCommandExButton buttonDelMethod;
	protected InterimCustomerHandler interimCustomerHandler;
	protected HtmlCommandExButton buttonCancelOrder;
	protected HtmlPanelGrid gridProductInfoHeaderGrid33;
	protected HtmlOutputText textProdInfoText5;
	protected HtmlAjaxRefreshRequest ajaxRefreshRequest1;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlForm getFormOrderEntryForm() {
		if (formOrderEntryForm == null) {
			formOrderEntryForm = (HtmlForm) findComponentInRoot("formOrderEntryForm");
		}
		return formOrderEntryForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlJspPanel getJspPanelHeaderPanel() {
		if (jspPanelHeaderPanel == null) {
			jspPanelHeaderPanel = (HtmlJspPanel) findComponentInRoot("jspPanelHeaderPanel");
		}
		return jspPanelHeaderPanel;
	}

	protected HtmlOutputText getTextMainTableHeader() {
		if (textMainTableHeader == null) {
			textMainTableHeader = (HtmlOutputText) findComponentInRoot("textMainTableHeader");
		}
		return textMainTableHeader;
	}

	protected HtmlPanelLayout getLayoutPageLayout() {
		if (layoutPageLayout == null) {
			layoutPageLayout = (HtmlPanelLayout) findComponentInRoot("layoutPageLayout");
		}
		return layoutPageLayout;
	}

	protected HtmlPanelGroup getGroupMainBodyPanel() {
		if (groupMainBodyPanel == null) {
			groupMainBodyPanel = (HtmlPanelGroup) findComponentInRoot("groupMainBodyPanel");
		}
		return groupMainBodyPanel;
	}

	protected HtmlPanelSection getSectionNewOfferCode1() {
		if (sectionNewOfferCode1 == null) {
			sectionNewOfferCode1 = (HtmlPanelSection) findComponentInRoot("sectionNewOfferCode1");
		}
		return sectionNewOfferCode1;
	}

	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}

	protected HtmlOutputText getTextOfferOverrridePanelHeaderClosedText() {
		if (textOfferOverrridePanelHeaderClosedText == null) {
			textOfferOverrridePanelHeaderClosedText = (HtmlOutputText) findComponentInRoot("textOfferOverrridePanelHeaderClosedText");
		}
		return textOfferOverrridePanelHeaderClosedText;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlPanelFormBox getFormBoxOfferCodeEntry1() {
		if (formBoxOfferCodeEntry1 == null) {
			formBoxOfferCodeEntry1 = (HtmlPanelFormBox) findComponentInRoot("formBoxOfferCodeEntry1");
		}
		return formBoxOfferCodeEntry1;
	}

	protected HtmlFormItem getFormItem1() {
		if (formItem1 == null) {
			formItem1 = (HtmlFormItem) findComponentInRoot("formItem1");
		}
		return formItem1;
	}

	protected HtmlInputText getTextNewOfferCode() {
		if (textNewOfferCode == null) {
			textNewOfferCode = (HtmlInputText) findComponentInRoot("textNewOfferCode");
		}
		return textNewOfferCode;
	}

	protected UIParameter getCustOfferCodeParm() {
		if (custOfferCodeParm == null) {
			custOfferCodeParm = (UIParameter) findComponentInRoot("custOfferCodeParm");
		}
		return custOfferCodeParm;
	}

	protected HtmlPanelGrid getGridNewOfferBottomGrid1() {
		if (gridNewOfferBottomGrid1 == null) {
			gridNewOfferBottomGrid1 = (HtmlPanelGrid) findComponentInRoot("gridNewOfferBottomGrid1");
		}
		return gridNewOfferBottomGrid1;
	}

	protected HtmlCommandLink getLinkRevertOfferLink() {
		if (linkRevertOfferLink == null) {
			linkRevertOfferLink = (HtmlCommandLink) findComponentInRoot("linkRevertOfferLink");
		}
		return linkRevertOfferLink;
	}

	protected HtmlOutputText getTextRevertOfferLinkText() {
		if (textRevertOfferLinkText == null) {
			textRevertOfferLinkText = (HtmlOutputText) findComponentInRoot("textRevertOfferLinkText");
		}
		return textRevertOfferLinkText;
	}

	protected HtmlPanelFormBox getFormBoxDeliveryInformation() {
		if (formBoxDeliveryInformation == null) {
			formBoxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxDeliveryInformation");
		}
		return formBoxDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryFirstName() {
		if (formItemDeliveryFirstName == null) {
			formItemDeliveryFirstName = (HtmlFormItem) findComponentInRoot("formItemDeliveryFirstName");
		}
		return formItemDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryFirstName() {
		if (textDeliveryFirstName == null) {
			textDeliveryFirstName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlInputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlInputText getTextDeliveryAddress1Text() {
		if (textDeliveryAddress1Text == null) {
			textDeliveryAddress1Text = (HtmlInputText) findComponentInRoot("textDeliveryAddress1Text");
		}
		return textDeliveryAddress1Text;
	}

	protected HtmlInputText getTextDeliveryAddress2() {
		if (textDeliveryAddress2 == null) {
			textDeliveryAddress2 = (HtmlInputText) findComponentInRoot("textDeliveryAddress2");
		}
		return textDeliveryAddress2;
	}

	protected HtmlInputText getTextDeliveryCity() {
		if (textDeliveryCity == null) {
			textDeliveryCity = (HtmlInputText) findComponentInRoot("textDeliveryCity");
		}
		return textDeliveryCity;
	}

	protected HtmlSelectOneMenu getMenuDeliveryState() {
		if (menuDeliveryState == null) {
			menuDeliveryState = (HtmlSelectOneMenu) findComponentInRoot("menuDeliveryState");
		}
		return menuDeliveryState;
	}

	protected HtmlInputText getTextDeliveryPhoneAreaCode() {
		if (textDeliveryPhoneAreaCode == null) {
			textDeliveryPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryPhoneAreaCode");
		}
		return textDeliveryPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneAreaCode() {
		if (textDeliveryWorkPhoneAreaCode == null) {
			textDeliveryWorkPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneAreaCode");
		}
		return textDeliveryWorkPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist44() {
		if (assist44 == null) {
			assist44 = (HtmlInputHelperAssist) findComponentInRoot("assist44");
		}
		return assist44;
	}

	protected HtmlInputHelperAssist getAssist45() {
		if (assist45 == null) {
			assist45 = (HtmlInputHelperAssist) findComponentInRoot("assist45");
		}
		return assist45;
	}

	protected HtmlInputText getTextEmailAddressRecipient() {
		if (textEmailAddressRecipient == null) {
			textEmailAddressRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressRecipient");
		}
		return textEmailAddressRecipient;
	}

	protected HtmlInputText getTextEmailAddressConfirmRecipient() {
		if (textEmailAddressConfirmRecipient == null) {
			textEmailAddressConfirmRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressConfirmRecipient");
		}
		return textEmailAddressConfirmRecipient;
	}

	protected HtmlPanelGrid getGridDeliveryAddressBottomFacet() {
		if (gridDeliveryAddressBottomFacet == null) {
			gridDeliveryAddressBottomFacet = (HtmlPanelGrid) findComponentInRoot("gridDeliveryAddressBottomFacet");
		}
		return gridDeliveryAddressBottomFacet;
	}

	protected HtmlPanelFormBox getFormBoxBillDifferentFromDelSelectionFormBox() {
		if (formBoxBillDifferentFromDelSelectionFormBox == null) {
			formBoxBillDifferentFromDelSelectionFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxBillDifferentFromDelSelectionFormBox");
		}
		return formBoxBillDifferentFromDelSelectionFormBox;
	}

	protected HtmlFormItem getFormItemBillDifferentFromDelSelector() {
		if (formItemBillDifferentFromDelSelector == null) {
			formItemBillDifferentFromDelSelector = (HtmlFormItem) findComponentInRoot("formItemBillDifferentFromDelSelector");
		}
		return formItemBillDifferentFromDelSelector;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxIsBillDifferentFromDelSelector() {
		if (checkboxIsBillDifferentFromDelSelector == null) {
			checkboxIsBillDifferentFromDelSelector = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxIsBillDifferentFromDelSelector");
		}
		return checkboxIsBillDifferentFromDelSelector;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlPanelFormBox getFormBoxBillingAddress() {
		if (formBoxBillingAddress == null) {
			formBoxBillingAddress = (HtmlPanelFormBox) findComponentInRoot("formBoxBillingAddress");
		}
		return formBoxBillingAddress;
	}

	protected HtmlFormItem getFormItemPayersEmailAddress() {
		if (formItemPayersEmailAddress == null) {
			formItemPayersEmailAddress = (HtmlFormItem) findComponentInRoot("formItemPayersEmailAddress");
		}
		return formItemPayersEmailAddress;
	}

	protected HtmlInputText getTextPurchaserEmailAddress() {
		if (textPurchaserEmailAddress == null) {
			textPurchaserEmailAddress = (HtmlInputText) findComponentInRoot("textPurchaserEmailAddress");
		}
		return textPurchaserEmailAddress;
	}

	protected HtmlInputText getTextBillingFirstName() {
		if (textBillingFirstName == null) {
			textBillingFirstName = (HtmlInputText) findComponentInRoot("textBillingFirstName");
		}
		return textBillingFirstName;
	}

	protected HtmlInputText getTextBillingCompanyName() {
		if (textBillingCompanyName == null) {
			textBillingCompanyName = (HtmlInputText) findComponentInRoot("textBillingCompanyName");
		}
		return textBillingCompanyName;
	}

	protected HtmlInputText getTextBillingAddress1() {
		if (textBillingAddress1 == null) {
			textBillingAddress1 = (HtmlInputText) findComponentInRoot("textBillingAddress1");
		}
		return textBillingAddress1;
	}

	protected HtmlInputText getTextBillingAddress2() {
		if (textBillingAddress2 == null) {
			textBillingAddress2 = (HtmlInputText) findComponentInRoot("textBillingAddress2");
		}
		return textBillingAddress2;
	}

	protected HtmlInputText getTextBillingCity() {
		if (textBillingCity == null) {
			textBillingCity = (HtmlInputText) findComponentInRoot("textBillingCity");
		}
		return textBillingCity;
	}

	protected HtmlSelectOneMenu getMenuBillingState() {
		if (menuBillingState == null) {
			menuBillingState = (HtmlSelectOneMenu) findComponentInRoot("menuBillingState");
		}
		return menuBillingState;
	}

	protected HtmlInputText getTextBillingPhoneAreaCode() {
		if (textBillingPhoneAreaCode == null) {
			textBillingPhoneAreaCode = (HtmlInputText) findComponentInRoot("textBillingPhoneAreaCode");
		}
		return textBillingPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlPanelFormBox getFormBoxPaymentHeaderFormBox() {
		if (formBoxPaymentHeaderFormBox == null) {
			formBoxPaymentHeaderFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentHeaderFormBox");
		}
		return formBoxPaymentHeaderFormBox;
	}

	protected HtmlSelectOneRadio getRadioPaymentMethod() {
		if (radioPaymentMethod == null) {
			radioPaymentMethod = (HtmlSelectOneRadio) findComponentInRoot("radioPaymentMethod");
		}
		return radioPaymentMethod;
	}

	protected HtmlOutputText getTextForceBillMeText() {
		if (textForceBillMeText == null) {
			textForceBillMeText = (HtmlOutputText) findComponentInRoot("textForceBillMeText");
		}
		return textForceBillMeText;
	}

	protected HtmlPanelGrid getGridPaymentGridBottomFacet() {
		if (gridPaymentGridBottomFacet == null) {
			gridPaymentGridBottomFacet = (HtmlPanelGrid) findComponentInRoot("gridPaymentGridBottomFacet");
		}
		return gridPaymentGridBottomFacet;
	}

	protected HtmlGraphicImageEx getImageExSpacerImage() {
		if (imageExSpacerImage == null) {
			imageExSpacerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSpacerImage");
		}
		return imageExSpacerImage;
	}

	protected HtmlFormItem getFormItemCreditCardNumber() {
		if (formItemCreditCardNumber == null) {
			formItemCreditCardNumber = (HtmlFormItem) findComponentInRoot("formItemCreditCardNumber");
		}
		return formItemCreditCardNumber;
	}

	protected HtmlInputText getTextCreditCardNumber() {
		if (textCreditCardNumber == null) {
			textCreditCardNumber = (HtmlInputText) findComponentInRoot("textCreditCardNumber");
		}
		return textCreditCardNumber;
	}

	protected HtmlSelectOneMenu getMenuCCExpireMonth() {
		if (menuCCExpireMonth == null) {
			menuCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireMonth");
		}
		return menuCCExpireMonth;
	}

	protected HtmlPanelGrid getPanelGridCreditCardImageGrid() {
		if (panelGridCreditCardImageGrid == null) {
			panelGridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("panelGridCreditCardImageGrid");
		}
		return panelGridCreditCardImageGrid;
	}

	protected HtmlJspPanel getJspPanelCreditCardImages() {
		if (jspPanelCreditCardImages == null) {
			jspPanelCreditCardImages = (HtmlJspPanel) findComponentInRoot("jspPanelCreditCardImages");
		}
		return jspPanelCreditCardImages;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptions() {
		if (radioRenewalOptions == null) {
			radioRenewalOptions = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptions");
		}
		return radioRenewalOptions;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptionsGift() {
		if (radioRenewalOptionsGift == null) {
			radioRenewalOptionsGift = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptionsGift");
		}
		return radioRenewalOptionsGift;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlOutputText getTextOfferDisclaimerText() {
		if (textOfferDisclaimerText == null) {
			textOfferDisclaimerText = (HtmlOutputText) findComponentInRoot("textOfferDisclaimerText");
		}
		return textOfferDisclaimerText;
	}

	protected HtmlOutputText getTextCOPPAText() {
		if (textCOPPAText == null) {
			textCOPPAText = (HtmlOutputText) findComponentInRoot("textCOPPAText");
		}
		return textCOPPAText;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlOutputLinkEx getLinkExRightColImageSpot1Link() {
		if (linkExRightColImageSpot1Link == null) {
			linkExRightColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColImageSpot1Link");
		}
		return linkExRightColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1() {
		if (imageExRightColImage1 == null) {
			imageExRightColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1");
		}
		return imageExRightColImage1;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot2Link() {
		if (linkExRightColSpot2Link == null) {
			linkExRightColSpot2Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot2Link");
		}
		return linkExRightColSpot2Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2() {
		if (imageExRightColImage2 == null) {
			imageExRightColImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2");
		}
		return imageExRightColImage2;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot3Link() {
		if (linkExRightColSpot3Link == null) {
			linkExRightColSpot3Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot3Link");
		}
		return linkExRightColSpot3Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3() {
		if (imageExRightColImage3 == null) {
			imageExRightColImage3 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3");
		}
		return imageExRightColImage3;
	}

	protected HtmlPanelGroup getGroupAddlAddrHelpGroup1() {
		if (groupAddlAddrHelpGroup1 == null) {
			groupAddlAddrHelpGroup1 = (HtmlPanelGroup) findComponentInRoot("groupAddlAddrHelpGroup1");
		}
		return groupAddlAddrHelpGroup1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getTextAdditionalAddrLearnMoreInfoText() {
		if (textAdditionalAddrLearnMoreInfoText == null) {
			textAdditionalAddrLearnMoreInfoText = (HtmlOutputText) findComponentInRoot("textAdditionalAddrLearnMoreInfoText");
		}
		return textAdditionalAddrLearnMoreInfoText;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlPanelGroup getGroupDelMethod1() {
		if (groupDelMethod1 == null) {
			groupDelMethod1 = (HtmlPanelGroup) findComponentInRoot("groupDelMethod1");
		}
		return groupDelMethod1;
	}

	protected HtmlPanelGroup getGroupSpecialOfferGroupBox() {
		if (groupSpecialOfferGroupBox == null) {
			groupSpecialOfferGroupBox = (HtmlPanelGroup) findComponentInRoot("groupSpecialOfferGroupBox");
		}
		return groupSpecialOfferGroupBox;
	}

	protected HtmlPanelGrid getGridSpecialOfferLayoutGrid() {
		if (gridSpecialOfferLayoutGrid == null) {
			gridSpecialOfferLayoutGrid = (HtmlPanelGrid) findComponentInRoot("gridSpecialOfferLayoutGrid");
		}
		return gridSpecialOfferLayoutGrid;
	}

	protected HtmlOutputLinkEx getLinkExCloseSpecialOffer() {
		if (linkExCloseSpecialOffer == null) {
			linkExCloseSpecialOffer = (HtmlOutputLinkEx) findComponentInRoot("linkExCloseSpecialOffer");
		}
		return linkExCloseSpecialOffer;
	}

	protected HtmlOutputText getTextCloseSpecialOfferText() {
		if (textCloseSpecialOfferText == null) {
			textCloseSpecialOfferText = (HtmlOutputText) findComponentInRoot("textCloseSpecialOfferText");
		}
		return textCloseSpecialOfferText;
	}

	protected HtmlGraphicImageEx getTooimageExSpecialOfferImage() {
		if (tooimageExSpecialOfferImage == null) {
			tooimageExSpecialOfferImage = (HtmlGraphicImageEx) findComponentInRoot("tooimageExSpecialOfferImage");
		}
		return tooimageExSpecialOfferImage;
	}

	protected HtmlOutputText getTextScriptInsert1() {
		if (textScriptInsert1 == null) {
			textScriptInsert1 = (HtmlOutputText) findComponentInRoot("textScriptInsert1");
		}
		return textScriptInsert1;
	}

	protected HtmlScriptCollector getScriptCollectorMainOrderEntryCollector() {
		if (scriptCollectorMainOrderEntryCollector == null) {
			scriptCollectorMainOrderEntryCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainOrderEntryCollector");
		}
		return scriptCollectorMainOrderEntryCollector;
	}

	protected HtmlPanelGrid getGridOffOverrideGrid() {
		if (gridOffOverrideGrid == null) {
			gridOffOverrideGrid = (HtmlPanelGrid) findComponentInRoot("gridOffOverrideGrid");
		}
		return gridOffOverrideGrid;
	}

	protected HtmlInputHelperAssist getAssist7() {
		if (assist7 == null) {
			assist7 = (HtmlInputHelperAssist) findComponentInRoot("assist7");
		}
		return assist7;
	}

	protected HtmlCommandExButton getButtonProcessOfferCode() {
		if (buttonProcessOfferCode == null) {
			buttonProcessOfferCode = (HtmlCommandExButton) findComponentInRoot("buttonProcessOfferCode");
		}
		return buttonProcessOfferCode;
	}

	protected HtmlPanelGrid getGridDeliveryInformation() {
		if (gridDeliveryInformation == null) {
			gridDeliveryInformation = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformation");
		}
		return gridDeliveryInformation;
	}

	protected HtmlInputText getTextDeliveryLastName() {
		if (textDeliveryLastName == null) {
			textDeliveryLastName = (HtmlInputText) findComponentInRoot("textDeliveryLastName");
		}
		return textDeliveryLastName;
	}

	protected HtmlFormItem getFormItemDeliveryCompanyName() {
		if (formItemDeliveryCompanyName == null) {
			formItemDeliveryCompanyName = (HtmlFormItem) findComponentInRoot("formItemDeliveryCompanyName");
		}
		return formItemDeliveryCompanyName;
	}

	protected HtmlFormItem getFormItemDeliveryAddress1() {
		if (formItemDeliveryAddress1 == null) {
			formItemDeliveryAddress1 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress1");
		}
		return formItemDeliveryAddress1;
	}

	protected HtmlInputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlInputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlFormItem getFormItemDeliveryAddress2() {
		if (formItemDeliveryAddress2 == null) {
			formItemDeliveryAddress2 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress2");
		}
		return formItemDeliveryAddress2;
	}

	protected HtmlOutputText getTextAddrLearnMore() {
		if (textAddrLearnMore == null) {
			textAddrLearnMore = (HtmlOutputText) findComponentInRoot("textAddrLearnMore");
		}
		return textAddrLearnMore;
	}

	protected HtmlFormItem getFormItemDeliveryCity() {
		if (formItemDeliveryCity == null) {
			formItemDeliveryCity = (HtmlFormItem) findComponentInRoot("formItemDeliveryCity");
		}
		return formItemDeliveryCity;
	}

	protected HtmlFormItem getFormItemDeliveryState() {
		if (formItemDeliveryState == null) {
			formItemDeliveryState = (HtmlFormItem) findComponentInRoot("formItemDeliveryState");
		}
		return formItemDeliveryState;
	}

	protected HtmlInputText getTextDeliveryZip() {
		if (textDeliveryZip == null) {
			textDeliveryZip = (HtmlInputText) findComponentInRoot("textDeliveryZip");
		}
		return textDeliveryZip;
	}

	protected HtmlFormItem getFormItemDeliveryPhone() {
		if (formItemDeliveryPhone == null) {
			formItemDeliveryPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryPhone");
		}
		return formItemDeliveryPhone;
	}

	protected HtmlInputText getTextDeliveryPhoneExchange() {
		if (textDeliveryPhoneExchange == null) {
			textDeliveryPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExchange");
		}
		return textDeliveryPhoneExchange;
	}

	protected HtmlInputText getTextDeliveryPhoneExtension() {
		if (textDeliveryPhoneExtension == null) {
			textDeliveryPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExtension");
		}
		return textDeliveryPhoneExtension;
	}

	protected HtmlFormItem getFormItemDeliveryWorkPhone() {
		if (formItemDeliveryWorkPhone == null) {
			formItemDeliveryWorkPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryWorkPhone");
		}
		return formItemDeliveryWorkPhone;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExchange() {
		if (textDeliveryWorkPhoneExchange == null) {
			textDeliveryWorkPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExchange");
		}
		return textDeliveryWorkPhoneExchange;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExtension() {
		if (textDeliveryWorkPhoneExtension == null) {
			textDeliveryWorkPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExtension");
		}
		return textDeliveryWorkPhoneExtension;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlFormItem getFormItemEmailAddressConfirm() {
		if (formItemEmailAddressConfirm == null) {
			formItemEmailAddressConfirm = (HtmlFormItem) findComponentInRoot("formItemEmailAddressConfirm");
		}
		return formItemEmailAddressConfirm;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getPanelGridBillingDifferentThanDelGrid() {
		if (panelGridBillingDifferentThanDelGrid == null) {
			panelGridBillingDifferentThanDelGrid = (HtmlPanelGrid) findComponentInRoot("panelGridBillingDifferentThanDelGrid");
		}
		return panelGridBillingDifferentThanDelGrid;
	}

	protected HtmlPanelGrid getGridBillingAddress() {
		if (gridBillingAddress == null) {
			gridBillingAddress = (HtmlPanelGrid) findComponentInRoot("gridBillingAddress");
		}
		return gridBillingAddress;
	}

	protected HtmlFormItem getFormItemBillingFirstName() {
		if (formItemBillingFirstName == null) {
			formItemBillingFirstName = (HtmlFormItem) findComponentInRoot("formItemBillingFirstName");
		}
		return formItemBillingFirstName;
	}

	protected HtmlInputText getTextBillingLastName() {
		if (textBillingLastName == null) {
			textBillingLastName = (HtmlInputText) findComponentInRoot("textBillingLastName");
		}
		return textBillingLastName;
	}

	protected HtmlFormItem getFormItemBillingCompanyName() {
		if (formItemBillingCompanyName == null) {
			formItemBillingCompanyName = (HtmlFormItem) findComponentInRoot("formItemBillingCompanyName");
		}
		return formItemBillingCompanyName;
	}

	protected HtmlFormItem getFormItemBillingAddress1() {
		if (formItemBillingAddress1 == null) {
			formItemBillingAddress1 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress1");
		}
		return formItemBillingAddress1;
	}

	protected HtmlInputText getTextBillingAptSuite() {
		if (textBillingAptSuite == null) {
			textBillingAptSuite = (HtmlInputText) findComponentInRoot("textBillingAptSuite");
		}
		return textBillingAptSuite;
	}

	protected HtmlFormItem getFormItemBillingAddress2() {
		if (formItemBillingAddress2 == null) {
			formItemBillingAddress2 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress2");
		}
		return formItemBillingAddress2;
	}

	protected HtmlFormItem getFormItemBillingCity() {
		if (formItemBillingCity == null) {
			formItemBillingCity = (HtmlFormItem) findComponentInRoot("formItemBillingCity");
		}
		return formItemBillingCity;
	}

	protected HtmlFormItem getFormItemBillingState() {
		if (formItemBillingState == null) {
			formItemBillingState = (HtmlFormItem) findComponentInRoot("formItemBillingState");
		}
		return formItemBillingState;
	}

	protected HtmlInputText getTextBillingZipCode() {
		if (textBillingZipCode == null) {
			textBillingZipCode = (HtmlInputText) findComponentInRoot("textBillingZipCode");
		}
		return textBillingZipCode;
	}

	protected HtmlFormItem getFormItemBillingTelephone() {
		if (formItemBillingTelephone == null) {
			formItemBillingTelephone = (HtmlFormItem) findComponentInRoot("formItemBillingTelephone");
		}
		return formItemBillingTelephone;
	}

	protected HtmlInputText getTextBillingPhoneExchange() {
		if (textBillingPhoneExchange == null) {
			textBillingPhoneExchange = (HtmlInputText) findComponentInRoot("textBillingPhoneExchange");
		}
		return textBillingPhoneExchange;
	}

	protected HtmlInputText getTextBillingPhoneExtension() {
		if (textBillingPhoneExtension == null) {
			textBillingPhoneExtension = (HtmlInputText) findComponentInRoot("textBillingPhoneExtension");
		}
		return textBillingPhoneExtension;
	}

	protected HtmlPanelGrid getGridPaymentInformationGridLabelGrid() {
		if (gridPaymentInformationGridLabelGrid == null) {
			gridPaymentInformationGridLabelGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGridLabelGrid");
		}
		return gridPaymentInformationGridLabelGrid;
	}

	protected HtmlFormItem getFormItemBillMeFormItem() {
		if (formItemBillMeFormItem == null) {
			formItemBillMeFormItem = (HtmlFormItem) findComponentInRoot("formItemBillMeFormItem");
		}
		return formItemBillMeFormItem;
	}

	protected HtmlPanelGrid getGridForceBillMeInfoGrid() {
		if (gridForceBillMeInfoGrid == null) {
			gridForceBillMeInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridForceBillMeInfoGrid");
		}
		return gridForceBillMeInfoGrid;
	}

	protected HtmlPanelGrid getGridPaymentInformationGrid() {
		if (gridPaymentInformationGrid == null) {
			gridPaymentInformationGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGrid");
		}
		return gridPaymentInformationGrid;
	}

	protected HtmlPanelFormBox getFormBoxPaymentInfo() {
		if (formBoxPaymentInfo == null) {
			formBoxPaymentInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentInfo");
		}
		return formBoxPaymentInfo;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected HtmlFormItem getFormItemCCExpirationDate() {
		if (formItemCCExpirationDate == null) {
			formItemCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemCCExpirationDate");
		}
		return formItemCCExpirationDate;
	}

	protected HtmlSelectOneMenu getMenuCCExpireYear() {
		if (menuCCExpireYear == null) {
			menuCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireYear");
		}
		return menuCCExpireYear;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGrid() {
		if (gridEZPAYOptionsGrid == null) {
			gridEZPAYOptionsGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGrid");
		}
		return gridEZPAYOptionsGrid;
	}

	protected HtmlOutputText getTextCCAgreement() {
		if (textCCAgreement == null) {
			textCCAgreement = (HtmlOutputText) findComponentInRoot("textCCAgreement");
		}
		return textCCAgreement;
	}

	protected HtmlOutputText getTextEZPayConfirm() {
		if (textEZPayConfirm == null) {
			textEZPayConfirm = (HtmlOutputText) findComponentInRoot("textEZPayConfirm");
		}
		return textEZPayConfirm;
	}

	protected HtmlOutputText getTextEzPayInfo() {
		if (textEzPayInfo == null) {
			textEzPayInfo = (HtmlOutputText) findComponentInRoot("textEzPayInfo");
		}
		return textEzPayInfo;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGridGift() {
		if (gridEZPAYOptionsGridGift == null) {
			gridEZPAYOptionsGridGift = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGridGift");
		}
		return gridEZPAYOptionsGridGift;
	}

	protected HtmlOutputText getTextCCAgreement2() {
		if (textCCAgreement2 == null) {
			textCCAgreement2 = (HtmlOutputText) findComponentInRoot("textCCAgreement2");
		}
		return textCCAgreement2;
	}

	protected HtmlOutputText getTextEzPayInfoGift() {
		if (textEzPayInfoGift == null) {
			textEzPayInfoGift = (HtmlOutputText) findComponentInRoot("textEzPayInfoGift");
		}
		return textEzPayInfoGift;
	}

	protected HtmlPanelGrid getGridTrialDisclaimerGrid() {
		if (gridTrialDisclaimerGrid == null) {
			gridTrialDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridTrialDisclaimerGrid");
		}
		return gridTrialDisclaimerGrid;
	}

	protected HtmlPanelGrid getGridOfferDisclaimerGrid() {
		if (gridOfferDisclaimerGrid == null) {
			gridOfferDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridOfferDisclaimerGrid");
		}
		return gridOfferDisclaimerGrid;
	}

	protected HtmlPanelGrid getCOPPAGrid() {
		if (COPPAGrid == null) {
			COPPAGrid = (HtmlPanelGrid) findComponentInRoot("COPPAGrid");
		}
		return COPPAGrid;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlJspPanel getJspPanelGeoTrustPanel() {
		if (jspPanelGeoTrustPanel == null) {
			jspPanelGeoTrustPanel = (HtmlJspPanel) findComponentInRoot("jspPanelGeoTrustPanel");
		}
		return jspPanelGeoTrustPanel;
	}

	protected HtmlPanelGrid getRightColImageSpot1() {
		if (rightColImageSpot1 == null) {
			rightColImageSpot1 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot1");
		}
		return rightColImageSpot1;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1NoLink() {
		if (imageExRightColImage1NoLink == null) {
			imageExRightColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1NoLink");
		}
		return imageExRightColImage1NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot1Grid() {
		if (gridRightColHTMLSpot1Grid == null) {
			gridRightColHTMLSpot1Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot1Grid");
		}
		return gridRightColHTMLSpot1Grid;
	}

	protected HtmlPanelGrid getGridVideoGrid() {
		if (gridVideoGrid == null) {
			gridVideoGrid = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid");
		}
		return gridVideoGrid;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	protected HtmlPanelGrid getRightColImageSpot2() {
		if (rightColImageSpot2 == null) {
			rightColImageSpot2 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot2");
		}
		return rightColImageSpot2;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2NoLink() {
		if (imageExRightColImage2NoLink == null) {
			imageExRightColImage2NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2NoLink");
		}
		return imageExRightColImage2NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot2Grid() {
		if (gridRightColHTMLSpot2Grid == null) {
			gridRightColHTMLSpot2Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot2Grid");
		}
		return gridRightColHTMLSpot2Grid;
	}

	protected HtmlPanelGrid getRightColImageSpot3() {
		if (rightColImageSpot3 == null) {
			rightColImageSpot3 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot3");
		}
		return rightColImageSpot3;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3NoLink() {
		if (imageExRightColImage3NoLink == null) {
			imageExRightColImage3NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3NoLink");
		}
		return imageExRightColImage3NoLink;
	}

	protected HtmlPanelDialog getDialogAdditionalAddrHelp() {
		if (dialogAdditionalAddrHelp == null) {
			dialogAdditionalAddrHelp = (HtmlPanelDialog) findComponentInRoot("dialogAdditionalAddrHelp");
		}
		return dialogAdditionalAddrHelp;
	}

	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHidden getStartDate() {
		if (startDate == null) {
			startDate = (HtmlInputHidden) findComponentInRoot("startDate");
		}
		return startDate;
	}

	protected HtmlJspPanel getJspPanelPopOverlayPanel() {
		if (jspPanelPopOverlayPanel == null) {
			jspPanelPopOverlayPanel = (HtmlJspPanel) findComponentInRoot("jspPanelPopOverlayPanel");
		}
		return jspPanelPopOverlayPanel;
	}

	protected HtmlOutputLinkEx getLinkExSpecialOfferLink() {
		if (linkExSpecialOfferLink == null) {
			linkExSpecialOfferLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSpecialOfferLink");
		}
		return linkExSpecialOfferLink;
	}

	protected HtmlInputHidden getShowOverlayPopUp() {
		if (showOverlayPopUp == null) {
			showOverlayPopUp = (HtmlInputHidden) findComponentInRoot("showOverlayPopUp");
		}
		return showOverlayPopUp;
	}

	protected HtmlInputHidden getIsForceBillMe() {
		if (isForceBillMe == null) {
			isForceBillMe = (HtmlInputHidden) findComponentInRoot("isForceBillMe");
		}
		return isForceBillMe;
	}

	protected HtmlInputHidden getPaymentMethodHidden() {
		if (paymentMethodHidden == null) {
			paymentMethodHidden = (HtmlInputHidden) findComponentInRoot("paymentMethodHidden");
		}
		return paymentMethodHidden;
	}

	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(
			SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(
			NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();
		
		HttpServletRequest req = (HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
		
		SubscriptionOfferIntf currentOffer = UTCommon.getCurrentOfferVersion2(req);
			
		offer.setCurrentOffer(currentOffer);
		NewSubscriptionOrderHandler newOrder = this.getNewSubscriptionOrderHandler();
		
		if (newOrder.getStartDate() == null) {
			newOrder.setStartDate(currentOffer.getSubscriptionProduct().getEarliestPossibleStartDate().toDate());
		}
		
		try {
			if (currentOffer != null) {
				SubscriptionTermsIntf selTerm = currentOffer.getTerm(newOrder.getSelectedTerm());
				
				if (selTerm == null) {
					
					for(SubscriptionTermsIntf term : currentOffer.getTerms()) {
						this.getNewSubscriptionOrderHandler().setSelectedTerm(term.getDurationInWeeks());
						break;
					}
				}
				
				if (currentOffer.isForceBillMe()) {
					newOrder.setPaymentMethod("B");
				}
				this.getIsForceBillMe().resetValue();
			}
		}
		catch (Exception e) {
			; // ignore
		}
	}

	protected HtmlScriptCollector getScriptCollector2() {
		if (scriptCollector2 == null) {
			scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
		}
		return scriptCollector2;
	}

	protected HtmlForm getFormLogout() {
		if (formLogout == null) {
			formLogout = (HtmlForm) findComponentInRoot("formLogout");
		}
		return formLogout;
	}

	protected HtmlOutputFormat getFormatWelcomeMessage() {
		if (formatWelcomeMessage == null) {
			formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
		}
		return formatWelcomeMessage;
	}

	protected HtmlOutputText getTextLogoutLink() {
		if (textLogoutLink == null) {
			textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
		}
		return textLogoutLink;
	}

	protected HtmlPanelGrid getGridProductInfoGrid() {
		if (gridProductInfoGrid == null) {
			gridProductInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridProductInfoGrid");
		}
		return gridProductInfoGrid;
	}

	protected HtmlPanelGroup getGroupprodinfo1() {
		if (groupprodinfo1 == null) {
			groupprodinfo1 = (HtmlPanelGroup) findComponentInRoot("groupprodinfo1");
		}
		return groupprodinfo1;
	}

	protected HtmlCommandLink getLinkLogout() {
		if (linkLogout == null) {
			linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
		}
		return linkLogout;
	}

	protected HtmlOutputText getTextPubCodeTextLabel() {
		if (textPubCodeTextLabel == null) {
			textPubCodeTextLabel = (HtmlOutputText) findComponentInRoot("textPubCodeTextLabel");
		}
		return textPubCodeTextLabel;
	}

	protected HtmlOutputText getTextProductCode() {
		if (textProductCode == null) {
			textProductCode = (HtmlOutputText) findComponentInRoot("textProductCode");
		}
		return textProductCode;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlOutputText getTextOfferProdName4() {
		if (textOfferProdName4 == null) {
			textOfferProdName4 = (HtmlOutputText) findComponentInRoot("textOfferProdName4");
		}
		return textOfferProdName4;
	}

	public String doButtonPlaceOrderAction() {
		// Type Java code that runs when the component is clicked
	
		String responseString = "success";
		NewSubscriptionOrderHandler newSub = this.getNewSubscriptionOrderHandler();
		
		ShoppingCartHandler sch = this.getShoppingCartHandler();
		ShoppingCartIntf cart = sch.getCart();
		cart.setBillingContact(null);
		cart.setDeliveryContact(null);
		cart.setCheckOutErrorMessage(null);
		cart.setPaymentMethod(null);
		
		
		try {
			HttpServletRequest request = (HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();

			
			if (!newSub.isValidDeliveryEmailAndDeliveryConfirmationEmail()) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Email Address does not match confirmation email address.", null);
	            
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;				
			}

			///// Delivery Contact    ///////////
	        ContactBO deliveryContact = new ContactBO();
	        
	        StringBuilder phone = new StringBuilder(newSub.getDeliveryPhoneAreaCode().trim());
	        phone.append(newSub.getDeliveryPhoneExchange().trim());
	        phone.append(newSub.getDeliveryPhoneExtension().trim());
	     
	        deliveryContact.setHomePhone(phone.toString());
	        if (newSub.getDeliveryWorkPhoneAreaCode() != null && newSub.getDeliveryWorkPhoneAreaCode().trim().length() == 3) {
	        	phone = new StringBuilder(newSub.getDeliveryWorkPhoneAreaCode());
	        	phone.append(newSub.getDeliveryWorkPhoneExchange());
	        	phone.append(newSub.getDeliveryWorkPhoneExtension());
	        	if (phone.length() == 10) {
	        		deliveryContact.setBusinessPhone(phone.toString());
	        	}
	        }
	        
	        deliveryContact.setEmailAddress(newSub.getDeliveryEmailAddress());
	        deliveryContact.setFirstName(newSub.getDeliveryFirstName().trim());
	        deliveryContact.setLastName(newSub.getDeliveryLastName().trim());
	        if (newSub.getDeliveryCompanyName() != null) {
	        	deliveryContact.setFirmName(newSub.getDeliveryCompanyName());
	        }
	        
	        if (deliveryContact.getFirstName().length() == 0 || deliveryContact.getLastName().length() == 0) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Delivery First and Last Name are required.", null);
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
	        }
	        
	        UIAddressBO dAddress = new UIAddressBO();
	        dAddress.setAddress1(newSub.getDeliveryAddress1().trim());
	        dAddress.setAddress2(newSub.getDeliveryAddress2().trim());
	        dAddress.setAptSuite(newSub.getDeliveryAptSuite().trim());
	        dAddress.setCity(newSub.getDeliveryCity().trim());
            // Check billing state
            if (newSub.getDeliveryState() == null || newSub.getDeliveryState().trim().equals("")
            		|| newSub.getDeliveryState().trim().equals("NONE") ) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Delivery State is Required", null);
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
            } else {
    	        dAddress.setState(newSub.getDeliveryState());	            	
            }	            

	        dAddress.setZip(newSub.getDeliveryZipCode().trim());
	        
	        deliveryContact.setUIAddress(dAddress);
			
	    	OrderIntf lastOrder = sch.getLastOrder();
	        if (lastOrder != null){
	            // possible refresh of page.
	            if (lastOrder.getDeliveryContact().getUIAddress().equals(dAddress)) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Duplicate form submission detected. An order with this delivery address has already been placed. ", null);
		            
		            context.addMessage(null, facesMsg);
		            responseString = "duplicate";
		            return responseString;
	            }
	        }
	        
	        if (!deliveryContact.validateContact()) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "We are unable to validate the delivery address. Please verify the address you entered.", null);
	            
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
	        }
	        else {
	            // update the bean with the code1 address
	            try {
	                UIAddressIntf correctedAddress = deliveryContact.getPersistentAddress().convertToUIAddress();

	                if (correctedAddress.getAddress1().length() > 0) {
		                newSub.setDeliveryAddress1(correctedAddress.getAddress1());
		                newSub.setDeliveryAddress2(correctedAddress.getAddress2());
		                newSub.setDeliveryAptSuite(correctedAddress.getAptSuite());
		                newSub.setDeliveryCity(correctedAddress.getCity());
		                newSub.setDeliveryZipCode(correctedAddress.getZip());
	                }
	            }
	            catch (Exception e) {
	                // ignore it
	                System.out.println("JSF One Page OrderEntry::Exception setting up corrected address: " + e.getMessage());
	            }
	        }
	        
	        cart.setDeliveryContact(deliveryContact);
	        
            ///////          BILLING Contact        /////////////
            ContactBO billingContact = null;
            if (newSub.isBillingDifferentThanDelivery()) {
	            billingContact = new ContactBO();
	            
	            // Check for billing email address
	            if (newSub.getBillingEmailAddress() == null || newSub.getBillingEmailAddress().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Email Address is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            billingContact.setEmailAddress(newSub.getBillingEmailAddress());	            	
	            }
	            // Check for billing first name
	            if (newSub.getBillingFirstName() == null || newSub.getBillingFirstName().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing First Name is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            billingContact.setFirstName(newSub.getBillingFirstName());	            	
	            }
	            // Check for billing last name
	            if (newSub.getBillingLastName() == null || newSub.getBillingLastName().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Last Name is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            billingContact.setLastName(newSub.getBillingLastName());	            	
	            }

		        if (newSub.getBillingCompanyName() != null) {
		        	billingContact.setFirmName(newSub.getBillingCompanyName());
		        }
	            
	            UIAddressBO bAddress = new UIAddressBO();
	            // Check billing address line
	            if (newSub.getBillingAddress1() == null || newSub.getBillingAddress1().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Address is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            bAddress.setAddress1(newSub.getBillingAddress1());	            	
	            }
	            // Check billing city
	            bAddress.setAddress2(newSub.getBillingAddress2());
	            bAddress.setAptSuite(newSub.getBillingAptSuite());
	            if (newSub.getBillingCity() == null || newSub.getBillingCity().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing City is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            bAddress.setCity(newSub.getBillingCity());	            	
	            }
	            // Check billing state
	            if (newSub.getBillingState() == null || newSub.getBillingState().trim().equals("") 
            		|| newSub.getBillingState().trim().equals("NONE")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing State is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            bAddress.setState(newSub.getBillingState());	            	
	            }	            
	            // Check billing zip
	            if (newSub.getBillingZipCode() == null || newSub.getBillingZipCode().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Billing Zip Code is Required", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
		            bAddress.setZip(newSub.getBillingZipCode());	            	
	            }	            
	            // Check billing phone number.  Not required, but must enter complete phone number
	            if (!newSub.getBillingPhoneAreaCode().trim().equals("") || !newSub.getBillingPhoneExchange().trim().equals("") 
	            		|| !newSub.getBillingPhoneExtension().trim().equals("")) {
		            if (newSub.getBillingPhoneAreaCode().trim().equals("") || newSub.getBillingPhoneExchange().trim().equals("") 
		            		|| newSub.getBillingPhoneExtension().trim().equals("")) {
			    		FacesContext context = FacesContext.getCurrentInstance( );
			            FacesMessage facesMsg = 
			                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Enter Complete Billing Phone Number.", null);
			            
			            context.addMessage(null, facesMsg);
			            responseString = "failure";
			            return responseString;
		            } else {
			            phone = new StringBuilder(newSub.getBillingPhoneAreaCode());
				        phone.append(newSub.getBillingPhoneExchange());
				        phone.append(newSub.getBillingPhoneExtension());
			            billingContact.setHomePhone(phone.toString());
		            }
	            }
	            
	            
	            billingContact.setUIAddress(bAddress);
	            
	            if (!billingContact.validateContact()) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "We are unable to validate the billing address. Please verify that the information entered is correct.", null);
		            
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            }
	            else {
	                // update the bean with the code1 address
	                try {
		                UIAddressIntf correctedAddress = billingContact.getPersistentAddress().convertToUIAddress();
		                
		                if (correctedAddress.getAddress1().length() > 0) {
		                	newSub.setBillingAddress1(correctedAddress.getAddress1());
		                	newSub.setBillingAddress2(correctedAddress.getAddress2());
		                	newSub.setBillingAptSuite(correctedAddress.getAptSuite());
		                	newSub.setBillingCity(correctedAddress.getCity());
		                	newSub.setBillingZipCode(correctedAddress.getZip());
		                }
	                }
	                catch (Exception e) {
	                    // ignore it
		                System.out.println("JSF One Page OrderEntry::Exception setting up corrected billing address: " + e.getMessage());
	                }
	            }
	            cart.setBillingContact(billingContact);
            }
	        
            // ADD The product
            SubscriptionOfferIntf offer = this.getCurrentOfferHandler().getCurrentOffer();
            
            if (offer == null) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected Event. No Current Offer.", null);
	            
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
            }
            
            SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(offer.getPubCode());

            product.applyOffer(offer);
            SubscriptionTermsIntf selectedTerm = offer.getTerm(newSub.getSelectedTerm());

            product.applyTerm(selectedTerm);
            
            SubscriptionOrderItemBO item = new SubscriptionOrderItemBO();

            item.setDeliveryZip(deliveryContact.getUiAddress().getZip());
            item.setProduct(product);
            
            item.setQuantity(newSub.getQuantity());
            
            item.setGiftItem(false);
            if (newSub.isBillingDifferentThanDelivery()) {
            	item.setGiftItem(true);
            }
            
            item.setOnetTimeBill(false);            
            item.setChoosingEZPay(false);
            
            if (newSub.getIsChoseBillMe() && (selectedTerm.requiresEZPAY() || offer.isForceEZPay()) ) {
            	// change user selection to Credit Card payment
            	newSub.setPaymentMethod("CC");
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The selected subscription term requires that you sign up for the EZ-PAY renewal option. Bill Me, is not available for EZ-PAY required term lengths.", null);
	            
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
            	
            }
            if (newSub.getIsChoseBillMe()) {
            	item.setChoosingEZPay(false);
            }
            else {
            	// credit card ... paypal in future
            	
            	// process renewal method for non bill me's
                if (!newSub.isBillingDifferentThanDelivery()) {		// New subscription renewal method
	                if (newSub.getRenewalMethod().equalsIgnoreCase("AUTOPAY_PLAN")) {
	                    item.setChoosingEZPay(true);
	                } else {
	                    // EZ-Pay may be required
	                    if (selectedTerm.requiresEZPAY() || offer.isForceEZPay()) {
	    		    		FacesContext context = FacesContext.getCurrentInstance( );
	    		            FacesMessage facesMsg = 
	    		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The selected subscription term requires that you sign up for the EZ-PAY renewal option.", null);
	    		            
	    		            context.addMessage(null, facesMsg);
	    		            responseString = "failure";
	    		            return responseString;
	                    }
	                }
                } else {		//  Gift transaction renewal method
                    if (newSub.getRenewalMethodGift().equalsIgnoreCase("AUTOPAY_PLAN")) {
                        item.setChoosingEZPay(true);
                    } else {
	                    // EZ-Pay may be required
	                    if (selectedTerm.requiresEZPAY() || offer.isForceEZPay()) {
	    		    		FacesContext context = FacesContext.getCurrentInstance( );
	    		            FacesMessage facesMsg = 
	    		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The selected subscription term requires that you sign up for the EZ-PAY renewal option.", null);
	    		            
	    		            context.addMessage(null, facesMsg);
	    		            responseString = "failure";
	    		            return responseString;
	                    }
                    }
                	if (newSub.getRenewalMethodGift().equalsIgnoreCase("ONE_TIME_BILL_GIFT")) {
                    	item.setOnetTimeBill(true);
                    }
                }
            }
            
            item.setDeliveryEmailAddress(deliveryContact.getEmailAddress());
            item.setKeyCode(offer.getKeyCode());

            String startDate = new DateTime().toString("yyyyMMdd");
            if (newSub.getStartDate() != null) {
            	DateTime requestedStart = new DateTime(newSub.getStartDate());
            	
            	// if requested date is in future use it, otherwise use today
            	if (requestedStart.isAfterNow()) {
            		startDate = requestedStart.toString("yyyyMMdd");
            	}
            	
            }
            
            item.setStartDate(startDate);
            item.setSelectedTerm(selectedTerm);
            item.setOffer(offer);

            // Gannett Unit checks only apply to print products
            if (!product.isElectronicDelivery()){
                if(deliveryContact.getPersistentAddress().isGUIAddress()){
                	newSub.setGannettUnit(true);
                }
                
                if (deliveryContact.getPersistentAddress() != null && newSub.isGannettUnit() && (selectedTerm.requiresEZPAY() || item.isChoosingEZPay()) ) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		    		StringBuilder msgDetail = new StringBuilder();
		    		if (selectedTerm.requiresEZPAY() && item.isChoosingEZPay()) {
		    			msgDetail.append("In order to provide you with early morning delivery, the delivery of your newspaper will be handled by an independent agent. Due to this, EZ-PAY is not an available payment option at this time. Please select another subscription term and renewal option.");
		    		}
		    		else if(selectedTerm.requiresEZPAY()) {
		    			msgDetail.append("In order to provide you with early morning delivery, the delivery of your newspaper will be handled by an independent agent. Due to this, EZ-PAY is not an available payment option at this time. Please select another subscription term.");
		    		}
		    		else if (item.isChoosingEZPay()) {
		    			msgDetail.append("In order to provide you with early morning delivery, the delivery of your newspaper will be handled by an independent agent. Due to this, EZ-PAY is not an available payment option at this time. Please select another renewal option below.");
		    		}
		    		else {
		    			msgDetail.append("In order to provide you with early morning delivery, the delivery of your newspaper will be handled by an independent agent. Due to this, EZ-PAY is not an available payment option at this time. Please select another subscription term and/or renewal option.");		    			
		    		}
		    		
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, msgDetail.toString(), null);
		            
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
                }
            }
            
            // Delivery Method checks
            if (product.isElectronicDelivery()) {
            	// for electronic just set to m
                item.setDeliveryMethod("M");
                item.setDeliveryMethodCheck(false);
                newSub.setDeliveryMethodText("Electronic Delivery");
            }
            else {
            	item.setDeliveryMethodCheck(false);
            	
            	if (newSub.isGannettUnit()) {
            		// always set Gannett Units to C
            		item.setDeliveryMethod("C");
                    newSub.setDeliveryMethodText("Morning Delivery");
                    item.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
            	}
            	else {
                	DeliveryMethodBO deliveryMethodBO = new DeliveryMethodBO();
                    // Get delivery Method, if active
                    try {
                        item.setDeliveryMethodCheck(com.usatoday.util.constants.UsaTodayConstants.DELIVERY_NOTIFY);
                        PromotionSet currentOfferPromotionSet = currentOfferHandler.getCurrentOffer().getPromotionSet();
                        
                        if (currentOfferPromotionSet != null && currentOfferPromotionSet.getDeliveryNotification() != null && 
                             currentOfferPromotionSet.getDeliveryNotification().getFulfillText() != null  && 
                            !currentOfferPromotionSet.getDeliveryNotification().getFulfillText().trim().equals("")) {
                            if (currentOfferPromotionSet.getDeliveryNotification().getFulfillText().equals("ON")) {
                                item.setDeliveryMethodCheck(true);
                            } else {
                                item.setDeliveryMethodCheck(false);
                            }
                        }
                        
                        String deliveryMethod = "";
                        
                        if (item.isDeliveryMethodCheck()) {
                            deliveryMethod = deliveryMethodBO.determineDeliveryMethod(offer.getPubCode(), deliveryContact.getUiAddress());
                            if (deliveryMethod != null) {
                                item.setDeliveryMethod(deliveryMethod);
                                if (deliveryMethod.equalsIgnoreCase("M")) {
                                    newSub.setDeliveryMethodText("Mail Delivery");
                                }
                                else if (deliveryMethod.equalsIgnoreCase("C")) {
                                	newSub.setDeliveryMethodText("Morning Delivery");
                                }
                            }
                        }
                        else {
                        	newSub.setDeliveryMethodText(null);
                        }
                    } catch (Exception e) {
                        System.out.println("JSF One Page OrderEntry:: Could not determine delivery method, because: " + e.getMessage());                
                    }
            	} // end if not a Gannett Unit
            } // end else print product
            
            if (newSub.getClubNumber() != null && newSub.getClubNumber().trim().length() > 0) {
            	item.setClubNumber(newSub.getClubNumber().trim());
            }
            
            // check if paying by credit card or bill me
            PaymentMethodIntf payment = null;
            if (newSub.getIsChoseBillMe()) {
            	payment = new InvoicePaymentMethodBO();
            }
            else {
	            // Setup Payment information
	            CreditCardPaymentMethodBO ccpayment = new CreditCardPaymentMethodBO();
	            // Check credit card number
	            if (newSub.getCreditCardNumber() == null || newSub.getCreditCardNumber().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select a Valid Credit Card Number.", null);
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
	            	ccpayment.setCardNumber(newSub.getCreditCardNumber());
				}
	            if (newSub.isBillingDifferentThanDelivery()) {
	            	ccpayment.setNameOnCard(billingContact.getFirstName() + " " + billingContact.getLastName());
	            }
	            else {
	            	ccpayment.setNameOnCard(deliveryContact.getFirstName() + " " + deliveryContact.getLastName());
	            }
	            
	    	    int ccType = CreditCardUtility.getCardType(newSub.getCreditCardNumber());
	    	    boolean errorEncountered = false;
	    	    
	    	    switch (ccType) {
	    	    	case CreditCardUtility.AMERICAN_EXPRESS:
	    	    		ccpayment.setPaymentType("X");
	    	    		break;
	    	    	case CreditCardUtility.VISA:
	    	    		ccpayment.setPaymentType("V");
	    	    		break;
	    	    	case CreditCardUtility.MASTERCARD:
	    	    		ccpayment.setPaymentType("M");
	    	    		break;
	    	    	case CreditCardUtility.DINERS_CLUB:
	    	    		ccpayment.setPaymentType("C");
	    	    		break;
	    	    	case CreditCardUtility.DISCOVER:
	    	    		ccpayment.setPaymentType("D");
	    	    		break;
	    	    	default: 
			    		FacesContext context = FacesContext.getCurrentInstance( );
			            FacesMessage facesMsg = 
			                    new FacesMessage(FacesMessage.SEVERITY_INFO, "The credit card number you entered is not recognized. Please check the number and try again.", null);
			            
			            context.addMessage(null, facesMsg);
			            responseString = "failure";
			            errorEncountered = true;
	    	    		break;
	    	    } // end switch
	
	    	    if (errorEncountered) {
		            return responseString;    	    	
	    	    }
	    	
	    	    // Check credit card CVV number
	    	    //if (newSub.getCreditCardCVVNumber() == null || newSub.getCreditCardCVVNumber().equals("")) {
		    	//	FacesContext context = FacesContext.getCurrentInstance( );
		         //   FacesMessage facesMsg = 
		          //          new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select a valid Credit Card Verification Number.", null);

//		            context.addMessage(null, facesMsg);
	//	            responseString = "failure";
		//            return responseString;
	      //      } else {
		    //	    ccpayment.setCVV(newSub.getCreditCardCVVNumber());
	    	 //   }

	            // Check credit card expiration
	            String expirationYear = newSub.getCreditCardExpirationYear();
	            String expirationMonth = newSub.getCreditCardExpirationMonth();
	            if (expirationMonth == null || expirationMonth.equalsIgnoreCase("-1") ||
	            		expirationYear == null || expirationYear.equalsIgnoreCase("-1")	) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Please select a valid Credit Card Expiration Date.", null);
		            
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
		            return responseString;
	            } else {
	            	ccpayment.setExpirationMonth(expirationMonth);
	            	ccpayment.setExpirationYear(expirationYear);
	            }

	    	    payment = ccpayment;
            }
            // add the item now that all validations are complete.
            cart.addItem(item);
          
            cart.setPaymentMethod(payment);
            
            
            // CJ Tracking boolean (used to determine if image pixel should be displayed.  in this case yes if 0.0 charge
            sch.setIsZeroDollarAmount(false);
            sch.setIsNotZeroDollar(true);
            if (cart.getTotal() == 0.0) {
    				sch.setIsZeroDollarAmount(true);
    				sch.setIsNotZeroDollar(false);
            }
            
            
            CheckOutService checkout = new CheckOutService();

            // Check for existing accounts for this email.
        	String password = null;
        	boolean sameEmails = true;
        	if (newSub.isBillingDifferentThanDelivery() && billingContact.getEmailAddress() != null && billingContact.getEmailAddress().length() > 0 && !deliveryContact.getEmailAddress().equalsIgnoreCase(billingContact.getEmailAddress())) {
        		sameEmails = false;
        	}
        	String sourceEmail = null;
        	// Determine if any accounts exist, if so, use that password. otherwise create a new one.
        	try {
        		sourceEmail = deliveryContact.getEmailAddress();
        		if (!sameEmails) {
        			sourceEmail = billingContact.getEmailAddress();
        		}
        		Collection<EmailRecordIntf> existingEmails = EmailRecordBO.getEmailRecordsForEmailAddress(sourceEmail);
        		if (existingEmails.size()> 0) {
        			
        			for (EmailRecordIntf anEmail : existingEmails) {
        				if (anEmail.getPassword()!= null && anEmail.getPassword().trim().length() > 0) {
        					password = anEmail.getPassword();
        					break;
        				}
        			}
        			
        			// no password set up on existing accounts - update them
        			if (password == null) {
        				password = CustomerBO.generateNewPassword();
        				for (EmailRecordIntf anEmail : existingEmails) {
        					anEmail.setPassword(password);
        					try {
            					EmailRecordBO eBO = (EmailRecordBO)anEmail;
            					eBO.save();
        					}
        					catch (Exception ue) {
        						; //ignore if update fails
        					}
        				}
        			}
        			else {
        				// update them with new / current password
        				for (EmailRecordIntf anEmail : existingEmails) {
        					anEmail.setPassword(password);
        					try {
            					EmailRecordBO eBO = (EmailRecordBO)anEmail;
            					eBO.save();
        					}
        					catch (Exception ue) {
        						; //ignore if update fails
        					}
        				}
        			}
        		}
        		else {
       				password = CustomerBO.generateNewPassword();
        		}
        	}
        	catch (Exception bummer){
        		password = CustomerBO.generateNewPassword();
        	}
        	
        	// set passwords based on order email scenario
        	if (sameEmails) {
        		if (billingContact != null) {
        			billingContact.setPassword(password);
        		}
        		deliveryContact.setPassword(password);
        	}
        	else {
        		billingContact.setPassword(password);
            	deliveryContact.setPassword(CustomerBO.generateNewPassword());
        	}
            // Done checking for existing passwords

        	cart.setClientIPAddress(request.getRemoteAddr());
            
            OrderIntf order = checkout.checkOutShoppingCart(cart, UsaTodayConstants.BATCH_PROCESS_CC_NETWORK_FAILURES);

            sch.setLastOrder(order);
            
            try {
            	if (this.getInterimCustomerHandler().getInterimCust() != null) {
            		this.getInterimCustomerHandler().getInterimCust().delete(this.getInterimCustomerHandler().getInterimCust());
            		this.getInterimCustomerHandler().setInterimCust(null);
            	}
            }
            catch (Exception e) {
            	; // ignore for now
			}
         
            // clear order entry handler
            newSub.resetForNewOrder();
            
            try {
            	
                order.sendSubscriptionConfirmationEmail();
                
            }
            catch (UsatException usex) {
                ; // ignore for now
            }
            
        } 
        catch (UsatException ue) {
            if (cart != null) {
                if (cart.getCheckOutErrorMessage() != null && !cart.getCheckOutErrorMessage().trim().equals("")) {
		    		FacesContext context = FacesContext.getCurrentInstance( );
		            FacesMessage facesMsg = 
		                    new FacesMessage(FacesMessage.SEVERITY_INFO, cart.getCheckOutErrorMessage(), null);
		            
		            context.addMessage(null, facesMsg);
		            responseString = "failure";
                }
                cart.clearItems();
            }
        }
        catch (Exception e) {
            if (cart != null) {
                cart.clearItems();
            }
            // Report the error using the appropriate name and ID.
    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "An unexpected error occurred during checkout: " + e.getMessage(), null);
            
            context.addMessage(null, facesMsg);
            responseString = "failure";

        }
		
		return responseString;
	}

	/** 
	 * @managed-bean true
	 */
	protected ShoppingCartHandler getShoppingCartHandler() {
		if (shoppingCartHandler == null) {
			shoppingCartHandler = (ShoppingCartHandler) getManagedBean("shoppingCartHandler");
		}
		return shoppingCartHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setShoppingCartHandler(
			ShoppingCartHandler shoppingCartHandler) {
		this.shoppingCartHandler = shoppingCartHandler;
	}

	protected HtmlPanelGrid getGridDeliveryInformationPanelFooterGrid() {
		if (gridDeliveryInformationPanelFooterGrid == null) {
			gridDeliveryInformationPanelFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformationPanelFooterGrid");
		}
		return gridDeliveryInformationPanelFooterGrid;
	}

	protected HtmlCommandExButton getButtonGetDeliveryMethodButton() {
		if (buttonGetDeliveryMethodButton == null) {
			buttonGetDeliveryMethodButton = (HtmlCommandExButton) findComponentInRoot("buttonGetDeliveryMethodButton");
		}
		return buttonGetDeliveryMethodButton;
	}

	protected HtmlOutputText getTextDeliveryMethod() {
		if (textDeliveryMethod == null) {
			textDeliveryMethod = (HtmlOutputText) findComponentInRoot("textDeliveryMethod");
		}
		return textDeliveryMethod;
	}

	protected HtmlPanelGrid getGridDeliveryMethodInfoGrid() {
		if (gridDeliveryMethodInfoGrid == null) {
			gridDeliveryMethodInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryMethodInfoGrid");
		}
		return gridDeliveryMethodInfoGrid;
	}

	protected HtmlOutputLinkEx getLinkExDelMethodCheckHelpLink() {
		if (linkExDelMethodCheckHelpLink == null) {
			linkExDelMethodCheckHelpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExDelMethodCheckHelpLink");
		}
		return linkExDelMethodCheckHelpLink;
	}

	protected HtmlOutputText getTextDelMethodHelpText() {
		if (textDelMethodHelpText == null) {
			textDelMethodHelpText = (HtmlOutputText) findComponentInRoot("textDelMethodHelpText");
		}
		return textDelMethodHelpText;
	}

	protected HtmlPanelGrid getGridDelMethodResultsGrid() {
		if (gridDelMethodResultsGrid == null) {
			gridDelMethodResultsGrid = (HtmlPanelGrid) findComponentInRoot("gridDelMethodResultsGrid");
		}
		return gridDelMethodResultsGrid;
	}

	protected HtmlOutputText getTextDeliveryMethodTextDeterminedValue() {
		if (textDeliveryMethodTextDeterminedValue == null) {
			textDeliveryMethodTextDeterminedValue = (HtmlOutputText) findComponentInRoot("textDeliveryMethodTextDeterminedValue");
		}
		return textDeliveryMethodTextDeterminedValue;
	}

	protected HtmlGraphicImageEx getImageExAmEx1() {
		if (imageExAmEx1 == null) {
			imageExAmEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageExAmEx1");
		}
		return imageExAmEx1;
	}

	protected HtmlGraphicImageEx getImageExDiscover1() {
		if (imageExDiscover1 == null) {
			imageExDiscover1 = (HtmlGraphicImageEx) findComponentInRoot("imageExDiscover1");
		}
		return imageExDiscover1;
	}

	protected HtmlGraphicImageEx getImageExMasterCard1() {
		if (imageExMasterCard1 == null) {
			imageExMasterCard1 = (HtmlGraphicImageEx) findComponentInRoot("imageExMasterCard1");
		}
		return imageExMasterCard1;
	}

	protected HtmlGraphicImageEx getImageExVisaLogo1() {
		if (imageExVisaLogo1 == null) {
			imageExVisaLogo1 = (HtmlGraphicImageEx) findComponentInRoot("imageExVisaLogo1");
		}
		return imageExVisaLogo1;
	}

	protected HtmlPanelGrid getGridDelMethod2() {
		if (gridDelMethod2 == null) {
			gridDelMethod2 = (HtmlPanelGrid) findComponentInRoot("gridDelMethod2");
		}
		return gridDelMethod2;
	}

	protected HtmlOutputText getTextDelMethodDetailHelp() {
		if (textDelMethodDetailHelp == null) {
			textDelMethodDetailHelp = (HtmlOutputText) findComponentInRoot("textDelMethodDetailHelp");
		}
		return textDelMethodDetailHelp;
	}

	protected HtmlBehavior getBehaviorDelMethod4() {
		if (behaviorDelMethod4 == null) {
			behaviorDelMethod4 = (HtmlBehavior) findComponentInRoot("behaviorDelMethod4");
		}
		return behaviorDelMethod4;
	}

	protected HtmlPanelDialog getDialogDelMethodDialog() {
		if (dialogDelMethodDialog == null) {
			dialogDelMethodDialog = (HtmlPanelDialog) findComponentInRoot("dialogDelMethodDialog");
		}
		return dialogDelMethodDialog;
	}

	protected HtmlCommandExButton getButtonDelMethod() {
		if (buttonDelMethod == null) {
			buttonDelMethod = (HtmlCommandExButton) findComponentInRoot("buttonDelMethod");
		}
		return buttonDelMethod;
	}

	public String doButtonGetDeliveryMethodButtonAction() {
		// Type Java code that runs when the component is clicked
	
		String responseString = "failure";
		
		SubscriptionOfferHandler currentOfferHandler = this.getCurrentOfferHandler();
		SubscriptionOfferIntf offer = currentOfferHandler.getCurrentOffer();
		
		NewSubscriptionOrderHandler newSub = this.getNewSubscriptionOrderHandler();
				
    	DeliveryMethodBO deliveryMethodBO = new DeliveryMethodBO();
        // Get delivery Method, if active
        try {
          
            String address1 = getTextDeliveryAddress1Text().getSubmittedValue().toString();
            
            String aptSte = getTextDeliveryAptSuite().getSubmittedValue().toString();
            String city = getTextDeliveryCity().getSubmittedValue().toString();
            String state = getMenuDeliveryState().getSubmittedValue().toString();
            String zip = getTextDeliveryZip().getSubmittedValue().toString();

			///// Delivery Contact    ///////////
	        ContactBO deliveryContact = new ContactBO();
	        
	        UIAddressBO dAddress = new UIAddressBO();
	        dAddress.setAddress1(address1);
	        //dAddress.setAddress2();
	        dAddress.setAptSuite(aptSte);
	        dAddress.setCity(city);
   	        dAddress.setState(state);		            
	        dAddress.setZip(zip);
            
            deliveryContact.setUIAddress(dAddress);
            
	        if (!deliveryContact.validateContact()) {
	    		FacesContext context = FacesContext.getCurrentInstance( );
	            FacesMessage facesMsg = 
	                    new FacesMessage(FacesMessage.SEVERITY_INFO, "We are unable to validate the delivery address. Please verify the address you entered. We cannot determine delivery method without a valid address.", null);
	            
	            context.addMessage(null, facesMsg);
	            responseString = "failure";
	            return responseString;
	        }
	        else {
	            // update the bean with the code1 address
	            try {
	                UIAddressIntf correctedAddress = deliveryContact.getPersistentAddress().convertToUIAddress();

	                if (correctedAddress.getAddress1().length() > 0) {
		                newSub.setDeliveryAddress1(correctedAddress.getAddress1());
		                getTextDeliveryAddress1Text().resetValue();
		                newSub.setDeliveryAddress2(correctedAddress.getAddress2());
		                newSub.setDeliveryAptSuite(correctedAddress.getAptSuite());
		                getTextDeliveryAptSuite().resetValue();
		                newSub.setDeliveryCity(correctedAddress.getCity());
		                getTextDeliveryCity().resetValue();
		                newSub.setDeliveryZipCode(correctedAddress.getZip());
		                getTextDeliveryZip().resetValue();
		                newSub.setDeliveryState(correctedAddress.getState());
		                getMenuDeliveryState().resetValue();
	                }
	            }
	            catch (Exception e) {
	                // ignore it
	                System.out.println("JSF One Page OrderEntry::Exception setting up corrected address: " + e.getMessage());
	            }
	        }
            
            
	        if (deliveryContact.getPersistentAddress().isGUIAddress()) {
	        	newSub.setDeliveryMethodText("Morning delivery via carrier");
	        }
	        else {
	        	deliveryMethodBO.determineDeliveryMethod(offer.getPubCode(), newSub.getDeliveryAddress1(), newSub.getDeliveryAptSuite(), newSub.getDeliveryCity(), newSub.getDeliveryState(), newSub.getDeliveryZipCode());
	            
                newSub.setDeliveryMethodText(deliveryMethodBO.getDeliveryMethodDisplayString());
                    
    	        FacesContext context = FacesContext.getCurrentInstance( );
                FacesMessage facesMsg = 
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Delivery Method: " + newSub.getDeliveryMethodText(), null);
                
                context.addMessage(null, facesMsg);
	        }
	        
	        
	        // capture Interim customer data
	        
	        try {
	        	InterimCustomerHandler interimCustHandler = this.getInterimCustomerHandler();
	        	CustomerInterimInfoBO interimCust = interimCustomerHandler.getInterimCust();
	        	
	        	if (interimCust == null) {
	        		interimCust = new CustomerInterimInfoBO();
	        		interimCustHandler.setInterimCust(interimCust);
	        	}
	        	
	        	KeyCodeParser parser = new KeyCodeParser(offer.getKeyCode());
	        	interimCust.setPublication(offer.getPubCode());
	        	interimCust.setContestCode(parser.getContestCode());
	        	interimCust.setPromoCode(parser.getPromoCode());
	        	interimCust.setSrcOrdCode(parser.getSourceCode());

	        	String tempStr = this.getTextDeliveryFirstName().getSubmittedValue().toString();
	        	interimCust.setFirstName(tempStr);
	        	
	        	tempStr = this.getTextDeliveryLastName().getSubmittedValue().toString();
	        	interimCust.setLastName(tempStr);
	        	
	        	tempStr = this.getTextEmailAddressRecipient().getSubmittedValue().toString();
	        	interimCust.setEmail(tempStr);
	        	
	        	tempStr = this.getTextDeliveryCompanyName().getSubmittedValue().toString();
	        	interimCust.setFirmName(tempStr);
	        	
	        	tempStr = this.getTextDeliveryPhoneAreaCode().getSubmittedValue().toString() + this.getTextDeliveryPhoneExchange().getSubmittedValue().toString() + this.getTextDeliveryPhoneExtension().getSubmittedValue().toString();
	        	interimCust.setHomePhone(tempStr);
	        	
	        	interimCust.setAddress1(newSub.getDeliveryAddress1());
	        	interimCust.setCity(newSub.getDeliveryCity());
	        	interimCust.setState(newSub.getDeliveryState());
	        	interimCust.setZip(newSub.getDeliveryZipCode());

	        	interimCust.save(interimCust);
	        	
	        }
	        catch (Exception e) {
	        	; // ignore issues here
			}
	        
	        responseString = "delMethodSuccess";
        }
        catch (Exception e) {
        	newSub.setDeliveryMethodText("Enter your full delivery address to determine delivery method.");
    		FacesContext context = FacesContext.getCurrentInstance( );
            FacesMessage facesMsg = 
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Unable to determine delivery method. Please verify your full delivery address. " + e.getMessage(), null);
            
            context.addMessage(null, facesMsg);
            responseString = "failure";
		}
		// return "delMethodSuccess";
		// return "failure";
		return responseString;

	}

	/** 
	 * @managed-bean true
	 */
	protected InterimCustomerHandler getInterimCustomerHandler() {
		if (interimCustomerHandler == null) {
			interimCustomerHandler = (InterimCustomerHandler) getManagedBean("interimCustomerHandler");
		}
		return interimCustomerHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setInterimCustomerHandler(
			InterimCustomerHandler interimCustomerHandler) {
		this.interimCustomerHandler = interimCustomerHandler;
	}

	protected HtmlCommandExButton getButtonCancelOrder() {
		if (buttonCancelOrder == null) {
			buttonCancelOrder = (HtmlCommandExButton) findComponentInRoot("buttonCancelOrder");
		}
		return buttonCancelOrder;
	}

	public String doButtonCancelOrderAction() {
		// Type Java code that runs when the component is clicked
	
		// return "success";
		InterimCustomerHandler iCust = this.getInterimCustomerHandler();
		
		iCust.setInterimCust(null);
		
		this.getNewSubscriptionOrderHandler().resetForNewOrder();
		
		return "success";
	}

	protected HtmlPanelGrid getGridProductInfoHeaderGrid33() {
		if (gridProductInfoHeaderGrid33 == null) {
			gridProductInfoHeaderGrid33 = (HtmlPanelGrid) findComponentInRoot("gridProductInfoHeaderGrid33");
		}
		return gridProductInfoHeaderGrid33;
	}

	protected HtmlOutputText getTextProdInfoText5() {
		if (textProdInfoText5 == null) {
			textProdInfoText5 = (HtmlOutputText) findComponentInRoot("textProdInfoText5");
		}
		return textProdInfoText5;
	}

	protected HtmlAjaxRefreshRequest getAjaxRefreshRequest1() {
		if (ajaxRefreshRequest1 == null) {
			ajaxRefreshRequest1 = (HtmlAjaxRefreshRequest) findComponentInRoot("ajaxRefreshRequest1");
		}
		return ajaxRefreshRequest1;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

}