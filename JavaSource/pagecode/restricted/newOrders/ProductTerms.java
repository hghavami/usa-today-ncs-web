/**
 * 
 */
package pagecode.restricted.newOrders;

import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlInputHelperTypeahead;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.ncs.web.subscription.handlers.NewSubscriptionOrderHandler;
import com.usatoday.ncs.web.subscription.handlers.ProductsHandler;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferHandler;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferLocatorHandler;
import com.ibm.faces.component.html.HtmlAjaxRefreshSubmit;

/**
 * @author aeast
 *
 */
public class ProductTerms extends PageCodeBase {

	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm formOfferSelcection;
	protected HtmlOutputText textProductLabel;
	protected UISelectItems selectItemsAvailalbeProducts1;
	protected ProductsHandler subscriptionProductsHandler;
	protected HtmlPanelFormBox formBoxQuantityInformation;
	protected HtmlFormItem formItemQuantityInformation;
	protected HtmlSelectOneMenu menuOrderQuantity;
	protected UISelectItems selectItems2;
	protected HtmlOutputText textNumCopiesLearnMoreLink;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlMessages messages1;
	protected SubscriptionOfferLocatorHandler offerSearchHandler;
	protected HtmlSelectOneMenu menuProductSelection;
	protected HtmlPanelGroup groupProductTermsSelection;
	protected HtmlPanelGrid gridTermsGrid;
	protected HtmlPanelFormBox formBoxTermsFormBox;
	protected HtmlFormItem formItemSubscriptionTerms;
	protected HtmlSelectOneRadio radioTermsSelection;
	protected HtmlInputHelperAssist assist1;
	protected HtmlPanelGrid gridTermsFormBoxBottomFacetGrid;
	protected HtmlPanelGroup group4;
	protected HtmlOutputText textEZPayExplanationRateCustomText;
	protected HtmlMessage termsMessage;
	protected HtmlOutputText textEZPayExplanationMessage;
	protected HtmlOutputText textEZPayLearnMore;
	protected HtmlPanelGrid grid4;
	protected HtmlOutputText textEZPay;
	protected HtmlCommandExButton button1;
	protected HtmlBehavior behavior8;
	protected HtmlPanelDialog dialogEZPayLearn;
	protected HtmlPanelGroup group6;
	protected HtmlPanelBox boxKeycodePanelBox;
	protected HtmlOutputText textKeycodeOfferCodeLabel;
	protected HtmlInputText textKeycode;
	protected HtmlInputHelperTypeahead typeahead1;
	protected HtmlCommandExButton buttonShowOfferDetails;
	protected HtmlAjaxRefreshSubmit ajaxRefreshSubmit1;
	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getFormOfferSelcection() {
		if (formOfferSelcection == null) {
			formOfferSelcection = (HtmlForm) findComponentInRoot("formOfferSelcection");
		}
		return formOfferSelcection;
	}

	protected HtmlOutputText getTextProductLabel() {
		if (textProductLabel == null) {
			textProductLabel = (HtmlOutputText) findComponentInRoot("textProductLabel");
		}
		return textProductLabel;
	}

	protected UISelectItems getSelectItemsAvailalbeProducts1() {
		if (selectItemsAvailalbeProducts1 == null) {
			selectItemsAvailalbeProducts1 = (UISelectItems) findComponentInRoot("selectItemsAvailalbeProducts1");
		}
		return selectItemsAvailalbeProducts1;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getSubscriptionProductsHandler() {
		if (subscriptionProductsHandler == null) {
			subscriptionProductsHandler = (ProductsHandler) getManagedBean("subscriptionProductsHandler");
		}
		return subscriptionProductsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSubscriptionProductsHandler(
			ProductsHandler subscriptionProductsHandler) {
		this.subscriptionProductsHandler = subscriptionProductsHandler;
	}

	protected HtmlPanelFormBox getFormBoxQuantityInformation() {
		if (formBoxQuantityInformation == null) {
			formBoxQuantityInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxQuantityInformation");
		}
		return formBoxQuantityInformation;
	}

	protected HtmlFormItem getFormItemQuantityInformation() {
		if (formItemQuantityInformation == null) {
			formItemQuantityInformation = (HtmlFormItem) findComponentInRoot("formItemQuantityInformation");
		}
		return formItemQuantityInformation;
	}

	protected HtmlSelectOneMenu getMenuOrderQuantity() {
		if (menuOrderQuantity == null) {
			menuOrderQuantity = (HtmlSelectOneMenu) findComponentInRoot("menuOrderQuantity");
		}
		return menuOrderQuantity;
	}

	protected UISelectItems getSelectItems2() {
		if (selectItems2 == null) {
			selectItems2 = (UISelectItems) findComponentInRoot("selectItems2");
		}
		return selectItems2;
	}

	protected HtmlOutputText getTextNumCopiesLearnMoreLink() {
		if (textNumCopiesLearnMoreLink == null) {
			textNumCopiesLearnMoreLink = (HtmlOutputText) findComponentInRoot("textNumCopiesLearnMoreLink");
		}
		return textNumCopiesLearnMoreLink;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(
			SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		// void <method>(FacesContext facescontext)
//		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();
//		SubscriptionOfferIntf tempOffer = offer.getCurrentOffer();
		
//		HttpServletRequest req = (HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
		
//		SubscriptionOfferIntf currentOffer = UTCommon.getCurrentOfferVersion2(req);
		
//		offer.setCurrentOffer(currentOffer);
		
//		NewSubscriptionOrderHandler newOrder = this.getNewSubscriptionOrderHandler();
		
//		if (newOrder.getStartDate() == null) {
//			newOrder.setStartDate(currentOffer.getSubscriptionProduct().getEarliestPossibleStartDate().toDate());
//		}
		
/*		try {
			if (currentOffer != null) {
				SubscriptionTermsIntf selTerm = currentOffer.getTerm(newOrder.getSelectedTerm());
				
				if (selTerm == null) {
					
					for(SubscriptionTermsIntf term : currentOffer.getTerms()) {
						this.getNewSubscriptionOrderHandler().setSelectedTerm(term.getDurationInWeeks());
						this.getRadioTermsSelection().resetValue();
						break;
					}
				}
				
				if (currentOffer.isForceBillMe()) {
					newOrder.setPaymentMethod("B");
				}
			}
		}
		catch (Exception e) {
			; // ignore
		}
*/		
	}

	/** 
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(
			NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferLocatorHandler getOfferSearchHandler() {
		if (offerSearchHandler == null) {
			offerSearchHandler = (SubscriptionOfferLocatorHandler) getManagedBean("offerSearchHandler");
		}
		return offerSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setOfferSearchHandler(
			SubscriptionOfferLocatorHandler offerSearchHandler) {
		this.offerSearchHandler = offerSearchHandler;
	}

	protected HtmlSelectOneMenu getMenuProductSelection() {
		if (menuProductSelection == null) {
			menuProductSelection = (HtmlSelectOneMenu) findComponentInRoot("menuProductSelection");
		}
		return menuProductSelection;
	}

	protected HtmlPanelGroup getGroupProductTermsSelection() {
		if (groupProductTermsSelection == null) {
			groupProductTermsSelection = (HtmlPanelGroup) findComponentInRoot("groupProductTermsSelection");
		}
		return groupProductTermsSelection;
	}

	protected HtmlPanelGrid getGridTermsGrid() {
		if (gridTermsGrid == null) {
			gridTermsGrid = (HtmlPanelGrid) findComponentInRoot("gridTermsGrid");
		}
		return gridTermsGrid;
	}

	protected HtmlPanelFormBox getFormBoxTermsFormBox() {
		if (formBoxTermsFormBox == null) {
			formBoxTermsFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxTermsFormBox");
		}
		return formBoxTermsFormBox;
	}

	protected HtmlFormItem getFormItemSubscriptionTerms() {
		if (formItemSubscriptionTerms == null) {
			formItemSubscriptionTerms = (HtmlFormItem) findComponentInRoot("formItemSubscriptionTerms");
		}
		return formItemSubscriptionTerms;
	}

	protected HtmlSelectOneRadio getRadioTermsSelection() {
		if (radioTermsSelection == null) {
			radioTermsSelection = (HtmlSelectOneRadio) findComponentInRoot("radioTermsSelection");
		}
		return radioTermsSelection;
	}

	protected HtmlInputHelperAssist getAssist1() {
		if (assist1 == null) {
			assist1 = (HtmlInputHelperAssist) findComponentInRoot("assist1");
		}
		return assist1;
	}

	protected HtmlPanelGrid getGridTermsFormBoxBottomFacetGrid() {
		if (gridTermsFormBoxBottomFacetGrid == null) {
			gridTermsFormBoxBottomFacetGrid = (HtmlPanelGrid) findComponentInRoot("gridTermsFormBoxBottomFacetGrid");
		}
		return gridTermsFormBoxBottomFacetGrid;
	}

	protected HtmlPanelGroup getGroup4() {
		if (group4 == null) {
			group4 = (HtmlPanelGroup) findComponentInRoot("group4");
		}
		return group4;
	}

	protected HtmlOutputText getTextEZPayExplanationRateCustomText() {
		if (textEZPayExplanationRateCustomText == null) {
			textEZPayExplanationRateCustomText = (HtmlOutputText) findComponentInRoot("textEZPayExplanationRateCustomText");
		}
		return textEZPayExplanationRateCustomText;
	}

	protected HtmlMessage getTermsMessage() {
		if (termsMessage == null) {
			termsMessage = (HtmlMessage) findComponentInRoot("termsMessage");
		}
		return termsMessage;
	}

	protected HtmlOutputText getTextEZPayExplanationMessage() {
		if (textEZPayExplanationMessage == null) {
			textEZPayExplanationMessage = (HtmlOutputText) findComponentInRoot("textEZPayExplanationMessage");
		}
		return textEZPayExplanationMessage;
	}

	protected HtmlOutputText getTextEZPayLearnMore() {
		if (textEZPayLearnMore == null) {
			textEZPayLearnMore = (HtmlOutputText) findComponentInRoot("textEZPayLearnMore");
		}
		return textEZPayLearnMore;
	}

	protected HtmlPanelGrid getGrid4() {
		if (grid4 == null) {
			grid4 = (HtmlPanelGrid) findComponentInRoot("grid4");
		}
		return grid4;
	}

	protected HtmlOutputText getTextEZPay() {
		if (textEZPay == null) {
			textEZPay = (HtmlOutputText) findComponentInRoot("textEZPay");
		}
		return textEZPay;
	}

	protected HtmlCommandExButton getButton1() {
		if (button1 == null) {
			button1 = (HtmlCommandExButton) findComponentInRoot("button1");
		}
		return button1;
	}

	protected HtmlBehavior getBehavior8() {
		if (behavior8 == null) {
			behavior8 = (HtmlBehavior) findComponentInRoot("behavior8");
		}
		return behavior8;
	}

	protected HtmlPanelDialog getDialogEZPayLearn() {
		if (dialogEZPayLearn == null) {
			dialogEZPayLearn = (HtmlPanelDialog) findComponentInRoot("dialogEZPayLearn");
		}
		return dialogEZPayLearn;
	}

	protected HtmlPanelGroup getGroup6() {
		if (group6 == null) {
			group6 = (HtmlPanelGroup) findComponentInRoot("group6");
		}
		return group6;
	}

	protected HtmlPanelBox getBoxKeycodePanelBox() {
		if (boxKeycodePanelBox == null) {
			boxKeycodePanelBox = (HtmlPanelBox) findComponentInRoot("boxKeycodePanelBox");
		}
		return boxKeycodePanelBox;
	}

	protected HtmlOutputText getTextKeycodeOfferCodeLabel() {
		if (textKeycodeOfferCodeLabel == null) {
			textKeycodeOfferCodeLabel = (HtmlOutputText) findComponentInRoot("textKeycodeOfferCodeLabel");
		}
		return textKeycodeOfferCodeLabel;
	}

	protected HtmlInputText getTextKeycode() {
		if (textKeycode == null) {
			textKeycode = (HtmlInputText) findComponentInRoot("textKeycode");
		}
		return textKeycode;
	}

	protected HtmlInputHelperTypeahead getTypeahead1() {
		if (typeahead1 == null) {
			typeahead1 = (HtmlInputHelperTypeahead) findComponentInRoot("typeahead1");
		}
		return typeahead1;
	}

	protected HtmlCommandExButton getButtonShowOfferDetails() {
		if (buttonShowOfferDetails == null) {
			buttonShowOfferDetails = (HtmlCommandExButton) findComponentInRoot("buttonShowOfferDetails");
		}
		return buttonShowOfferDetails;
	}

	public String doButtonShowOfferDetailsAction() {
		// Type Java code that runs when the component is clicked
	
		SubscriptionOfferLocatorHandler locator = this.getOfferSearchHandler();
		
		/*
		 * 				String keycodeOnly = fullText.substring(0, 5);

				this.typeAhead.setKeyCode(keycodeOnly);
				
				int index = fullText.indexOf("(");
				String newPubCode = fullText.substring(index+1, index+3);

		 */
		String pubCode = locator.getTypeAhead().getPubCode();
		String keyCode = locator.getTypeAhead().getKeyCode();
		
		try {
		
			SubscriptionOfferIntf offer = null;
			
			offer = SubscriptionOfferManager.getInstance().getOffer(keyCode, pubCode);
		
			locator.getCurrentOfferHandler().setCurrentOffer(offer);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	protected HtmlAjaxRefreshSubmit getAjaxRefreshSubmit1() {
		if (ajaxRefreshSubmit1 == null) {
			ajaxRefreshSubmit1 = (HtmlAjaxRefreshSubmit) findComponentInRoot("ajaxRefreshSubmit1");
		}
		return ajaxRefreshSubmit1;
	}

}