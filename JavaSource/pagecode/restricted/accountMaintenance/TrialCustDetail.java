/**
 * 
 */
package pagecode.restricted.accountMaintenance;

import pagecode.PageCodeBase;
import com.usatoday.ncs.web.subscription.handlers.TrialCustomerHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlForm;
import com.ibm.faces.component.html.HtmlPanelLayout;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.UIColumnEx;

/**
 * @author aeast
 *
 */
public class TrialCustDetail extends PageCodeBase {

	protected TrialCustomerHandler trialCustHandler;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlPanelLayout layoutTrialCustomerOuterLayout;
	protected HtmlPanelGrid gridHeaderGrid;
	protected HtmlOutputText textEmailLabel;
	protected HtmlInputText textEmailAddress;
	protected HtmlDataTableEx tableExTrialSubscriptions;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text1;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text3;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text5;
	protected UIColumnEx columnEx5;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx8;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText text10;
	protected UIColumnEx columnEx10;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx11;
	protected UIColumnEx columnEx12;
	protected HtmlOutputText text12;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx13;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx14;
	protected HtmlOutputText text15;
	protected UIColumnEx columnEx15;
	protected HtmlOutputText text16;
	protected UIColumnEx columnEx16;
	protected HtmlOutputText text17;
	protected UIColumnEx columnEx17;
	protected HtmlOutputText text18;
	protected HtmlOutputText text19;
	protected HtmlOutputText text20;
	protected HtmlOutputText text21;
	protected HtmlOutputText text22;
	protected HtmlOutputText text23;
	protected HtmlOutputText text24;
	protected HtmlOutputText text25;
	protected HtmlOutputText text26;
	protected HtmlOutputText text27;
	protected HtmlOutputText text28;
	protected HtmlOutputText text29;
	protected HtmlOutputText text30;
	protected HtmlOutputText text31;
	protected HtmlOutputText text32;
	protected HtmlOutputText text33;
	protected HtmlOutputText text34;
	/** 
	 * @managed-bean true
	 */
	protected TrialCustomerHandler getTrialCustHandler() {
		if (trialCustHandler == null) {
			trialCustHandler = (TrialCustomerHandler) getManagedBean("trialCustHandler");
		}
		return trialCustHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setTrialCustHandler(TrialCustomerHandler trialCustHandler) {
		this.trialCustHandler = trialCustHandler;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelLayout getLayoutTrialCustomerOuterLayout() {
		if (layoutTrialCustomerOuterLayout == null) {
			layoutTrialCustomerOuterLayout = (HtmlPanelLayout) findComponentInRoot("layoutTrialCustomerOuterLayout");
		}
		return layoutTrialCustomerOuterLayout;
	}

	protected HtmlPanelGrid getGridHeaderGrid() {
		if (gridHeaderGrid == null) {
			gridHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridHeaderGrid");
		}
		return gridHeaderGrid;
	}

	protected HtmlOutputText getTextEmailLabel() {
		if (textEmailLabel == null) {
			textEmailLabel = (HtmlOutputText) findComponentInRoot("textEmailLabel");
		}
		return textEmailLabel;
	}

	protected HtmlInputText getTextEmailAddress() {
		if (textEmailAddress == null) {
			textEmailAddress = (HtmlInputText) findComponentInRoot("textEmailAddress");
		}
		return textEmailAddress;
	}

	protected HtmlDataTableEx getTableExTrialSubscriptions() {
		if (tableExTrialSubscriptions == null) {
			tableExTrialSubscriptions = (HtmlDataTableEx) findComponentInRoot("tableExTrialSubscriptions");
		}
		return tableExTrialSubscriptions;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected UIColumnEx getColumnEx10() {
		if (columnEx10 == null) {
			columnEx10 = (UIColumnEx) findComponentInRoot("columnEx10");
		}
		return columnEx10;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx11() {
		if (columnEx11 == null) {
			columnEx11 = (UIColumnEx) findComponentInRoot("columnEx11");
		}
		return columnEx11;
	}

	protected UIColumnEx getColumnEx12() {
		if (columnEx12 == null) {
			columnEx12 = (UIColumnEx) findComponentInRoot("columnEx12");
		}
		return columnEx12;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx13() {
		if (columnEx13 == null) {
			columnEx13 = (UIColumnEx) findComponentInRoot("columnEx13");
		}
		return columnEx13;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx14() {
		if (columnEx14 == null) {
			columnEx14 = (UIColumnEx) findComponentInRoot("columnEx14");
		}
		return columnEx14;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected UIColumnEx getColumnEx15() {
		if (columnEx15 == null) {
			columnEx15 = (UIColumnEx) findComponentInRoot("columnEx15");
		}
		return columnEx15;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected UIColumnEx getColumnEx16() {
		if (columnEx16 == null) {
			columnEx16 = (UIColumnEx) findComponentInRoot("columnEx16");
		}
		return columnEx16;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected UIColumnEx getColumnEx17() {
		if (columnEx17 == null) {
			columnEx17 = (UIColumnEx) findComponentInRoot("columnEx17");
		}
		return columnEx17;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected HtmlOutputText getText21() {
		if (text21 == null) {
			text21 = (HtmlOutputText) findComponentInRoot("text21");
		}
		return text21;
	}

	protected HtmlOutputText getText22() {
		if (text22 == null) {
			text22 = (HtmlOutputText) findComponentInRoot("text22");
		}
		return text22;
	}

	protected HtmlOutputText getText23() {
		if (text23 == null) {
			text23 = (HtmlOutputText) findComponentInRoot("text23");
		}
		return text23;
	}

	protected HtmlOutputText getText24() {
		if (text24 == null) {
			text24 = (HtmlOutputText) findComponentInRoot("text24");
		}
		return text24;
	}

	protected HtmlOutputText getText25() {
		if (text25 == null) {
			text25 = (HtmlOutputText) findComponentInRoot("text25");
		}
		return text25;
	}

	protected HtmlOutputText getText26() {
		if (text26 == null) {
			text26 = (HtmlOutputText) findComponentInRoot("text26");
		}
		return text26;
	}

	protected HtmlOutputText getText27() {
		if (text27 == null) {
			text27 = (HtmlOutputText) findComponentInRoot("text27");
		}
		return text27;
	}

	protected HtmlOutputText getText28() {
		if (text28 == null) {
			text28 = (HtmlOutputText) findComponentInRoot("text28");
		}
		return text28;
	}

	protected HtmlOutputText getText29() {
		if (text29 == null) {
			text29 = (HtmlOutputText) findComponentInRoot("text29");
		}
		return text29;
	}

	protected HtmlOutputText getText30() {
		if (text30 == null) {
			text30 = (HtmlOutputText) findComponentInRoot("text30");
		}
		return text30;
	}

	protected HtmlOutputText getText31() {
		if (text31 == null) {
			text31 = (HtmlOutputText) findComponentInRoot("text31");
		}
		return text31;
	}

	protected HtmlOutputText getText32() {
		if (text32 == null) {
			text32 = (HtmlOutputText) findComponentInRoot("text32");
		}
		return text32;
	}

	protected HtmlOutputText getText33() {
		if (text33 == null) {
			text33 = (HtmlOutputText) findComponentInRoot("text33");
		}
		return text33;
	}

	protected HtmlOutputText getText34() {
		if (text34 == null) {
			text34 = (HtmlOutputText) findComponentInRoot("text34");
		}
		return text34;
	}

}