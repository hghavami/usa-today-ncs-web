/**
 * 
 */
package pagecode.restricted.accountMaintenance;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class AccountMaintHome extends PageCodeBase {

	protected HtmlPanelGroup groupAccountLinksHeaderGroup;
	protected HtmlPanelGrid gridAcctHeaderGrid;
	protected HtmlOutputText textAcctMngmtLabel;
	protected HtmlOutputText textSampleSignUpLabel;
	protected HtmlOutputText textRetrievePwdTxt;
	protected HtmlOutputText textChangeEmailTxt;
	protected HtmlOutputText textAccountSetupTxt;
	protected HtmlOutputText textEnterEmailLabel;
	protected HtmlPanelGrid gridLinksGrid;
	protected HtmlOutputLinkEx linkExSampleSignUpLink;
	protected HtmlOutputLinkEx linkExRetrievePasswordLink;
	protected HtmlOutputLinkEx linkExChangeEmailPwd;
	protected HtmlOutputLinkEx linkExSetUpAccountLink;
	protected HtmlPanelGrid gridEmailPrefGrid;
	protected HtmlInputText textEmailAddressForPref;
	protected HtmlOutputText textFillerTextEmailPref;
	protected HtmlCommandExButton buttonEmailPreferences;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlMessages messages1;

	protected HtmlPanelGroup getGroupAccountLinksHeaderGroup() {
		if (groupAccountLinksHeaderGroup == null) {
			groupAccountLinksHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupAccountLinksHeaderGroup");
		}
		return groupAccountLinksHeaderGroup;
	}

	protected HtmlPanelGrid getGridAcctHeaderGrid() {
		if (gridAcctHeaderGrid == null) {
			gridAcctHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridAcctHeaderGrid");
		}
		return gridAcctHeaderGrid;
	}

	protected HtmlOutputText getTextAcctMngmtLabel() {
		if (textAcctMngmtLabel == null) {
			textAcctMngmtLabel = (HtmlOutputText) findComponentInRoot("textAcctMngmtLabel");
		}
		return textAcctMngmtLabel;
	}

	protected HtmlOutputText getTextSampleSignUpLabel() {
		if (textSampleSignUpLabel == null) {
			textSampleSignUpLabel = (HtmlOutputText) findComponentInRoot("textSampleSignUpLabel");
		}
		return textSampleSignUpLabel;
	}

	protected HtmlOutputText getTextRetrievePwdTxt() {
		if (textRetrievePwdTxt == null) {
			textRetrievePwdTxt = (HtmlOutputText) findComponentInRoot("textRetrievePwdTxt");
		}
		return textRetrievePwdTxt;
	}

	protected HtmlOutputText getTextChangeEmailTxt() {
		if (textChangeEmailTxt == null) {
			textChangeEmailTxt = (HtmlOutputText) findComponentInRoot("textChangeEmailTxt");
		}
		return textChangeEmailTxt;
	}

	protected HtmlOutputText getTextAccountSetupTxt() {
		if (textAccountSetupTxt == null) {
			textAccountSetupTxt = (HtmlOutputText) findComponentInRoot("textAccountSetupTxt");
		}
		return textAccountSetupTxt;
	}

	protected HtmlOutputText getTextEnterEmailLabel() {
		if (textEnterEmailLabel == null) {
			textEnterEmailLabel = (HtmlOutputText) findComponentInRoot("textEnterEmailLabel");
		}
		return textEnterEmailLabel;
	}

	protected HtmlPanelGrid getGridLinksGrid() {
		if (gridLinksGrid == null) {
			gridLinksGrid = (HtmlPanelGrid) findComponentInRoot("gridLinksGrid");
		}
		return gridLinksGrid;
	}

	protected HtmlOutputLinkEx getLinkExSampleSignUpLink() {
		if (linkExSampleSignUpLink == null) {
			linkExSampleSignUpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSampleSignUpLink");
		}
		return linkExSampleSignUpLink;
	}

	protected HtmlOutputLinkEx getLinkExRetrievePasswordLink() {
		if (linkExRetrievePasswordLink == null) {
			linkExRetrievePasswordLink = (HtmlOutputLinkEx) findComponentInRoot("linkExRetrievePasswordLink");
		}
		return linkExRetrievePasswordLink;
	}

	protected HtmlOutputLinkEx getLinkExChangeEmailPwd() {
		if (linkExChangeEmailPwd == null) {
			linkExChangeEmailPwd = (HtmlOutputLinkEx) findComponentInRoot("linkExChangeEmailPwd");
		}
		return linkExChangeEmailPwd;
	}

	protected HtmlOutputLinkEx getLinkExSetUpAccountLink() {
		if (linkExSetUpAccountLink == null) {
			linkExSetUpAccountLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSetUpAccountLink");
		}
		return linkExSetUpAccountLink;
	}

	protected HtmlPanelGrid getGridEmailPrefGrid() {
		if (gridEmailPrefGrid == null) {
			gridEmailPrefGrid = (HtmlPanelGrid) findComponentInRoot("gridEmailPrefGrid");
		}
		return gridEmailPrefGrid;
	}

	protected HtmlInputText getTextEmailAddressForPref() {
		if (textEmailAddressForPref == null) {
			textEmailAddressForPref = (HtmlInputText) findComponentInRoot("textEmailAddressForPref");
		}
		return textEmailAddressForPref;
	}

	protected HtmlOutputText getTextFillerTextEmailPref() {
		if (textFillerTextEmailPref == null) {
			textFillerTextEmailPref = (HtmlOutputText) findComponentInRoot("textFillerTextEmailPref");
		}
		return textFillerTextEmailPref;
	}

	protected HtmlCommandExButton getButtonEmailPreferences() {
		if (buttonEmailPreferences == null) {
			buttonEmailPreferences = (HtmlCommandExButton) findComponentInRoot("buttonEmailPreferences");
		}
		return buttonEmailPreferences;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}