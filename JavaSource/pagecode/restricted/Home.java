/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.restricted;

import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;

import pagecode.PageCodeBase;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.ncs.web.handlers.MarketHandler;
import com.usatoday.ncs.web.handlers.UserHandler;
import com.usatoday.ncs.web.handlers.LocaleHandler;
/**
 * @author aeast
 * @date Nov 6, 2007
 * @class restrictedHome
 * 
 * 
 */
public class Home extends PageCodeBase {

    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm form1;
    protected HtmlMessages messages1;
    protected HtmlOutputText textHomeMsg;
    protected HtmlInputText textZipCode;
    protected HtmlCommandExButton buttonLookUpMarket;
    protected HtmlJspPanel jspPanelLeftWork;
    protected HtmlOutputText textMarketSearch;
    protected HtmlOutputText textMarketLookupHeader;
    protected HtmlOutputText textCurrentMarketZip;
    protected HtmlOutputText textCurrentMarketID;
    protected HtmlOutputText textCurrentMarketDesc;
	protected UserHandler user;
	protected LocaleHandler userLocaleHandler;
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getForm1() {
        if (form1 == null) {
            form1 = (HtmlForm) findComponentInRoot("form1");
        }
        return form1;
    }
    public String doButtonLookUpMarketAction() {
        // Type Java code that runs when the component is clicked
        String marketZip = (String)this.getTextZipCode().getValue();
                
        MarketHandler mh = null;
        
        mh = new MarketHandler();
        mh.setZipCode(marketZip);
        
        this.getRequestScope().put("currentMarket", mh);
        
        return "";
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlOutputText getTextHomeMsg() {
        if (textHomeMsg == null) {
            textHomeMsg = (HtmlOutputText) findComponentInRoot("textHomeMsg");
        }
        return textHomeMsg;
    }
    protected HtmlInputText getTextZipCode() {
        if (textZipCode == null) {
            textZipCode = (HtmlInputText) findComponentInRoot("textZipCode");
        }
        return textZipCode;
    }
    protected HtmlCommandExButton getButtonLookUpMarket() {
        if (buttonLookUpMarket == null) {
            buttonLookUpMarket = (HtmlCommandExButton) findComponentInRoot("buttonLookUpMarket");
        }
        return buttonLookUpMarket;
    }
    protected HtmlJspPanel getJspPanelLeftWork() {
        if (jspPanelLeftWork == null) {
            jspPanelLeftWork = (HtmlJspPanel) findComponentInRoot("jspPanelLeftWork");
        }
        return jspPanelLeftWork;
    }
    protected HtmlOutputText getTextMarketSearch() {
        if (textMarketSearch == null) {
            textMarketSearch = (HtmlOutputText) findComponentInRoot("textMarketSearch");
        }
        return textMarketSearch;
    }
    protected HtmlOutputText getTextMarketLookupHeader() {
        if (textMarketLookupHeader == null) {
            textMarketLookupHeader = (HtmlOutputText) findComponentInRoot("textMarketLookupHeader");
        }
        return textMarketLookupHeader;
    }
    protected HtmlOutputText getTextCurrentMarketZip() {
        if (textCurrentMarketZip == null) {
            textCurrentMarketZip = (HtmlOutputText) findComponentInRoot("textCurrentMarketZip");
        }
        return textCurrentMarketZip;
    }
    protected HtmlOutputText getTextCurrentMarketID() {
        if (textCurrentMarketID == null) {
            textCurrentMarketID = (HtmlOutputText) findComponentInRoot("textCurrentMarketID");
        }
        return textCurrentMarketID;
    }
    protected HtmlOutputText getTextCurrentMarketDesc() {
        if (textCurrentMarketDesc == null) {
            textCurrentMarketDesc = (HtmlOutputText) findComponentInRoot("textCurrentMarketDesc");
        }
        return textCurrentMarketDesc;
    }
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}
	/** 
	 * @managed-bean true
	 */
	protected LocaleHandler getUserLocaleHandler() {
		if (userLocaleHandler == null) {
			userLocaleHandler = (LocaleHandler) getManagedBean("userLocaleHandler");
		}
		return userLocaleHandler;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUserLocaleHandler(LocaleHandler userLocaleHandler) {
		this.userLocaleHandler = userLocaleHandler;
	}
}