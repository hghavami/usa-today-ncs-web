/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlPanelLayout;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectOneMenu;
import com.ibm.faces.component.html.HtmlInputHelperAssist;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import com.ibm.faces.component.html.HtmlBehavior;
import javax.faces.component.html.HtmlSelectOneRadio;
import javax.faces.component.UISelectItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import javax.faces.component.UISelectItems;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlInputHelperSetFocus;
import javax.faces.component.html.HtmlInputHidden;
import com.ibm.faces.component.html.HtmlBehaviorKeyPress;
import com.ibm.faces.component.html.HtmlAjaxRefreshRequest;
import com.ibm.faces.component.html.HtmlAjaxRefreshSubmit;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class SubscriptionOrderForm_mainBodyarea extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorMainOrderEntryCollector;
	protected HtmlForm formOrderEntryForm;
	protected HtmlPanelGrid gridMainBodyGrid;
	protected HtmlPanelGrid gridProductInfoGrid;
	protected HtmlPanelGroup groupprodinfo1;
	protected HtmlPanelGrid gridProductInfoHeaderGrid33;
	protected HtmlOutputText textProdInfoText5;
	protected HtmlOutputText textPubCodeTextLabel;
	protected HtmlOutputText textProductCode;
	protected HtmlOutputText textProductName;
	protected HtmlOutputText textOfferProdName4;
	protected HtmlJspPanel jspPanelFormPanel;
	protected HtmlPanelLayout layoutPageLayout;
	protected HtmlPanelGroup groupMainBodyPanel;
	protected HtmlPanelGrid gridDeliveryInformation;
	protected HtmlPanelFormBox formBoxDeliveryInformation;
	protected HtmlFormItem formItemDeliveryFirstName;
	protected HtmlInputText textDeliveryFirstName;
	protected HtmlInputText textDeliveryLastName;
	protected HtmlFormItem formItemDeliveryCompanyName;
	protected HtmlInputText textDeliveryCompanyName;
	protected HtmlFormItem formItemDeliveryAddress1;
	protected HtmlInputText textDeliveryAddress1Text;
	protected HtmlInputText textDeliveryAptSuite;
	protected HtmlFormItem formItemDeliveryAddress2;
	protected HtmlInputText textDeliveryAddress2;
	protected HtmlOutputText textAddrLearnMore;
	protected HtmlFormItem formItemDeliveryCity;
	protected HtmlInputText textDeliveryCity;
	protected HtmlFormItem formItemDeliveryState;
	protected HtmlSelectOneMenu menuDeliveryState;
	protected HtmlInputText textDeliveryZip;
	protected HtmlFormItem formItemDeliveryPhone;
	protected HtmlInputText textDeliveryPhoneAreaCode;
	protected HtmlInputHelperAssist assist4;
	protected HtmlInputText textDeliveryPhoneExchange;
	protected HtmlInputHelperAssist assist5;
	protected HtmlInputText textDeliveryPhoneExtension;
	protected HtmlFormItem formItemDeliveryWorkPhone;
	protected HtmlInputText textDeliveryWorkPhoneAreaCode;
	protected HtmlInputHelperAssist assist44;
	protected HtmlInputText textDeliveryWorkPhoneExchange;
	protected HtmlInputHelperAssist assist45;
	protected HtmlInputText textDeliveryWorkPhoneExtension;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlInputText textEmailAddressRecipient;
	protected HtmlFormItem formItemEmailAddressConfirm;
	protected HtmlInputText textEmailAddressConfirmRecipient;
	protected HtmlPanelGrid gridDeliveryAddressBottomFacet;
	protected HtmlPanelGrid gridDeliveryInformationPanelFooterGrid;
	protected HtmlCommandExButton buttonGetDeliveryMethodButton;
	protected HtmlPanelGrid gridDelMethodResultsGrid;
	protected HtmlOutputText textDeliveryMethod;
	protected HtmlOutputText textDeliveryMethodTextDeterminedValue;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid gridDeliveryMethodInfoGrid;
	protected HtmlOutputLinkEx linkExDelMethodCheckHelpLink;
	protected HtmlOutputText textDelMethodHelpText;
	protected HtmlPanelGrid panelGridBillingDifferentThanDelGrid;
	protected HtmlPanelFormBox formBoxBillDifferentFromDelSelectionFormBox;
	protected HtmlFormItem formItemBillDifferentFromDelSelector;
	protected HtmlSelectBooleanCheckbox checkboxIsBillDifferentFromDelSelector;
	protected HtmlBehavior behavior1;
	protected HtmlPanelGrid gridBillingAddress;
	protected HtmlPanelFormBox formBoxBillingAddress;
	protected HtmlFormItem formItemPayersEmailAddress;
	protected HtmlInputText textPurchaserEmailAddress;
	protected HtmlFormItem formItemBillingFirstName;
	protected HtmlInputText textBillingFirstName;
	protected HtmlInputText textBillingLastName;
	protected HtmlFormItem formItemBillingCompanyName;
	protected HtmlInputText textBillingCompanyName;
	protected HtmlFormItem formItemBillingAddress1;
	protected HtmlInputText textBillingAddress1;
	protected HtmlInputText textBillingAptSuite;
	protected HtmlFormItem formItemBillingAddress2;
	protected HtmlInputText textBillingAddress2;
	protected HtmlFormItem formItemBillingCity;
	protected HtmlInputText textBillingCity;
	protected HtmlFormItem formItemBillingState;
	protected HtmlSelectOneMenu menuBillingState;
	protected HtmlInputText textBillingZipCode;
	protected HtmlFormItem formItemBillingTelephone;
	protected HtmlInputText textBillingPhoneAreaCode;
	protected HtmlInputHelperAssist assist2;
	protected HtmlInputText textBillingPhoneExchange;
	protected HtmlInputHelperAssist assist3;
	protected HtmlInputText textBillingPhoneExtension;
	protected HtmlPanelGrid gridPaymentInformationGridLabelGrid;
	protected HtmlPanelFormBox formBoxPaymentHeaderFormBox;
	protected HtmlFormItem formItemBillMeFormItem;
	protected HtmlSelectOneRadio radioPaymentMethod;
	protected UISelectItem selectItem1;
	protected UISelectItem selectItem2;
	protected HtmlPanelGrid gridForceBillMeInfoGrid;
	protected HtmlOutputText textForceBillMeText;
	protected HtmlPanelGrid gridPaymentInformationGrid;
	protected HtmlPanelGrid gridPaymentGridBottomFacet;
	protected HtmlGraphicImageEx imageExSpacerImage;
	protected HtmlPanelFormBox formBoxPaymentInfo;
	protected HtmlFormItem formItemCreditCardNumber;
	protected HtmlInputText textCreditCardNumber;
	protected HtmlInputHelperAssist assist6;
	protected HtmlFormItem formItemCCExpirationDate;
	protected HtmlSelectOneMenu menuCCExpireMonth;
	protected UISelectItem selectItem16;
	protected UISelectItem selectItem3;
	protected UISelectItem selectItem4;
	protected UISelectItem selectItem5;
	protected UISelectItem selectItem6;
	protected UISelectItem selectItem7;
	protected UISelectItem selectItem8;
	protected UISelectItem selectItem9;
	protected UISelectItem selectItem10;
	protected UISelectItem selectItem11;
	protected UISelectItem selectItem12;
	protected UISelectItem selectItem13;
	protected UISelectItem selectItem14;
	protected HtmlSelectOneMenu menuCCExpireYear;
	protected UISelectItems selectItems1;
	protected HtmlPanelGrid panelGridCreditCardImageGrid;
	protected HtmlJspPanel jspPanelCreditCardImages;
	protected HtmlGraphicImageEx imageExAmEx1;
	protected HtmlGraphicImageEx imageExDiscover1;
	protected HtmlGraphicImageEx imageExMasterCard1;
	protected HtmlGraphicImageEx imageExVisaLogo1;
	protected HtmlPanelGrid gridEZPAYOptionsGrid;
	protected HtmlSelectOneRadio radioRenewalOptions;
	protected UISelectItem selectItem17;
	protected UISelectItem selectItem18;
	protected HtmlOutputText textCCAgreement;
	protected HtmlOutputText textEZPayConfirm;
	protected HtmlOutputText textEzPayInfo;
	protected HtmlPanelGrid gridEZPAYOptionsGridGift;
	protected HtmlSelectOneRadio radioRenewalOptionsGift;
	protected UISelectItem selectItem19;
	protected UISelectItem selectItem20;
	protected UISelectItem selectItem15;
	protected HtmlOutputText textCCAgreement2;
	protected HtmlOutputText textEzPayInfoGift;
	protected HtmlPanelGrid gridTrialDisclaimerGrid;
	protected HtmlOutputText textDisclaimerText;
	protected HtmlPanelGrid gridOfferDisclaimerGrid;
	protected HtmlOutputText textOfferDisclaimerText;
	protected HtmlPanelGrid panelGridFormSubmissionGrid;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected HtmlCommandExButton buttonCancelOrder;
	protected HtmlMessages messagesAllMesssages;
	protected HtmlPanelGrid rightColImageSpot1;
	protected HtmlOutputLinkEx linkExRightColImageSpot1Link;
	protected HtmlGraphicImageEx imageExRightColImage1;
	protected HtmlGraphicImageEx imageExRightColImage1NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot1Grid;
	protected HtmlOutputText RightColHTMLSpot1Text;
	protected HtmlPanelGrid gridVideoGrid;
	protected HtmlOutputLinkEx linkExVideoLink_B;
	protected HtmlGraphicImageEx imageExVideoSweepsGraphic_B;
	protected HtmlOutputText textEEVideoText;
	protected HtmlPanelGrid rightColImageSpot2;
	protected HtmlOutputLinkEx linkExRightColSpot2Link;
	protected HtmlGraphicImageEx imageExRightColImage2;
	protected HtmlGraphicImageEx imageExRightColImage2NoLink;
	protected HtmlPanelGrid gridRightColHTMLSpot2Grid;
	protected HtmlOutputText RightColHTMLSpot2Text;
	protected HtmlPanelGrid rightColImageSpot3;
	protected HtmlOutputLinkEx linkExRightColSpot3Link;
	protected HtmlGraphicImageEx imageExRightColImage3;
	protected HtmlGraphicImageEx imageExRightColImage3NoLink;
	protected HtmlPanelDialog dialogDelMethodDialog;
	protected HtmlPanelGroup groupDelMethod1;
	protected HtmlPanelGrid gridDelMethod2;
	protected HtmlOutputText textDelMethodDetailHelp;
	protected HtmlCommandExButton buttonDelMethod;
	protected HtmlBehavior behaviorDelMethod4;
	protected HtmlPanelDialog dialogAdditionalAddrHelp;
	protected HtmlPanelGroup groupAddlAddrHelpGroup1;
	protected HtmlPanelGrid grid1;
	protected HtmlOutputText textAdditionalAddrLearnMoreInfoText;
	protected HtmlCommandExButton button3;
	protected HtmlBehavior behavior3;
	protected HtmlInputHelperSetFocus setFocus1;
	protected HtmlInputHidden startDate;
	protected HtmlInputHidden isForceBillMe;
	protected HtmlInputHidden paymentMethodHidden;
	protected HtmlBehaviorKeyPress behaviorKeyPress1;
	protected HtmlOutputText textProductTermSelected;
	protected HtmlOutputText textOfferSelectedTerm4;
	protected HtmlAjaxRefreshRequest ajaxRefreshRequest1;
	protected HtmlAjaxRefreshSubmit ajaxRefreshSubmit1;
	protected HtmlBehavior behavior2;
	protected UINamingContainer viewFragmentOrderForm1;

	protected HtmlScriptCollector getScriptCollectorMainOrderEntryCollector() {
		if (scriptCollectorMainOrderEntryCollector == null) {
			scriptCollectorMainOrderEntryCollector = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainOrderEntryCollector");
		}
		return scriptCollectorMainOrderEntryCollector;
	}

	protected HtmlForm getFormOrderEntryForm() {
		if (formOrderEntryForm == null) {
			formOrderEntryForm = (HtmlForm) findComponentInRoot("formOrderEntryForm");
		}
		return formOrderEntryForm;
	}

	protected HtmlPanelGrid getGridMainBodyGrid() {
		if (gridMainBodyGrid == null) {
			gridMainBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridMainBodyGrid");
		}
		return gridMainBodyGrid;
	}

	protected HtmlPanelGrid getGridProductInfoGrid() {
		if (gridProductInfoGrid == null) {
			gridProductInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridProductInfoGrid");
		}
		return gridProductInfoGrid;
	}

	protected HtmlPanelGroup getGroupprodinfo1() {
		if (groupprodinfo1 == null) {
			groupprodinfo1 = (HtmlPanelGroup) findComponentInRoot("groupprodinfo1");
		}
		return groupprodinfo1;
	}

	protected HtmlPanelGrid getGridProductInfoHeaderGrid33() {
		if (gridProductInfoHeaderGrid33 == null) {
			gridProductInfoHeaderGrid33 = (HtmlPanelGrid) findComponentInRoot("gridProductInfoHeaderGrid33");
		}
		return gridProductInfoHeaderGrid33;
	}

	protected HtmlOutputText getTextProdInfoText5() {
		if (textProdInfoText5 == null) {
			textProdInfoText5 = (HtmlOutputText) findComponentInRoot("textProdInfoText5");
		}
		return textProdInfoText5;
	}

	protected HtmlOutputText getTextPubCodeTextLabel() {
		if (textPubCodeTextLabel == null) {
			textPubCodeTextLabel = (HtmlOutputText) findComponentInRoot("textPubCodeTextLabel");
		}
		return textPubCodeTextLabel;
	}

	protected HtmlOutputText getTextProductCode() {
		if (textProductCode == null) {
			textProductCode = (HtmlOutputText) findComponentInRoot("textProductCode");
		}
		return textProductCode;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlOutputText getTextOfferProdName4() {
		if (textOfferProdName4 == null) {
			textOfferProdName4 = (HtmlOutputText) findComponentInRoot("textOfferProdName4");
		}
		return textOfferProdName4;
	}

	protected HtmlJspPanel getJspPanelFormPanel() {
		if (jspPanelFormPanel == null) {
			jspPanelFormPanel = (HtmlJspPanel) findComponentInRoot("jspPanelFormPanel");
		}
		return jspPanelFormPanel;
	}

	protected HtmlPanelLayout getLayoutPageLayout() {
		if (layoutPageLayout == null) {
			layoutPageLayout = (HtmlPanelLayout) findComponentInRoot("layoutPageLayout");
		}
		return layoutPageLayout;
	}

	protected HtmlPanelGroup getGroupMainBodyPanel() {
		if (groupMainBodyPanel == null) {
			groupMainBodyPanel = (HtmlPanelGroup) findComponentInRoot("groupMainBodyPanel");
		}
		return groupMainBodyPanel;
	}

	protected HtmlPanelGrid getGridDeliveryInformation() {
		if (gridDeliveryInformation == null) {
			gridDeliveryInformation = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformation");
		}
		return gridDeliveryInformation;
	}

	protected HtmlPanelFormBox getFormBoxDeliveryInformation() {
		if (formBoxDeliveryInformation == null) {
			formBoxDeliveryInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxDeliveryInformation");
		}
		return formBoxDeliveryInformation;
	}

	protected HtmlFormItem getFormItemDeliveryFirstName() {
		if (formItemDeliveryFirstName == null) {
			formItemDeliveryFirstName = (HtmlFormItem) findComponentInRoot("formItemDeliveryFirstName");
		}
		return formItemDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryFirstName() {
		if (textDeliveryFirstName == null) {
			textDeliveryFirstName = (HtmlInputText) findComponentInRoot("textDeliveryFirstName");
		}
		return textDeliveryFirstName;
	}

	protected HtmlInputText getTextDeliveryLastName() {
		if (textDeliveryLastName == null) {
			textDeliveryLastName = (HtmlInputText) findComponentInRoot("textDeliveryLastName");
		}
		return textDeliveryLastName;
	}

	protected HtmlFormItem getFormItemDeliveryCompanyName() {
		if (formItemDeliveryCompanyName == null) {
			formItemDeliveryCompanyName = (HtmlFormItem) findComponentInRoot("formItemDeliveryCompanyName");
		}
		return formItemDeliveryCompanyName;
	}

	protected HtmlInputText getTextDeliveryCompanyName() {
		if (textDeliveryCompanyName == null) {
			textDeliveryCompanyName = (HtmlInputText) findComponentInRoot("textDeliveryCompanyName");
		}
		return textDeliveryCompanyName;
	}

	protected HtmlFormItem getFormItemDeliveryAddress1() {
		if (formItemDeliveryAddress1 == null) {
			formItemDeliveryAddress1 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress1");
		}
		return formItemDeliveryAddress1;
	}

	protected HtmlInputText getTextDeliveryAddress1Text() {
		if (textDeliveryAddress1Text == null) {
			textDeliveryAddress1Text = (HtmlInputText) findComponentInRoot("textDeliveryAddress1Text");
		}
		return textDeliveryAddress1Text;
	}

	protected HtmlInputText getTextDeliveryAptSuite() {
		if (textDeliveryAptSuite == null) {
			textDeliveryAptSuite = (HtmlInputText) findComponentInRoot("textDeliveryAptSuite");
		}
		return textDeliveryAptSuite;
	}

	protected HtmlFormItem getFormItemDeliveryAddress2() {
		if (formItemDeliveryAddress2 == null) {
			formItemDeliveryAddress2 = (HtmlFormItem) findComponentInRoot("formItemDeliveryAddress2");
		}
		return formItemDeliveryAddress2;
	}

	protected HtmlInputText getTextDeliveryAddress2() {
		if (textDeliveryAddress2 == null) {
			textDeliveryAddress2 = (HtmlInputText) findComponentInRoot("textDeliveryAddress2");
		}
		return textDeliveryAddress2;
	}

	protected HtmlOutputText getTextAddrLearnMore() {
		if (textAddrLearnMore == null) {
			textAddrLearnMore = (HtmlOutputText) findComponentInRoot("textAddrLearnMore");
		}
		return textAddrLearnMore;
	}

	protected HtmlFormItem getFormItemDeliveryCity() {
		if (formItemDeliveryCity == null) {
			formItemDeliveryCity = (HtmlFormItem) findComponentInRoot("formItemDeliveryCity");
		}
		return formItemDeliveryCity;
	}

	protected HtmlInputText getTextDeliveryCity() {
		if (textDeliveryCity == null) {
			textDeliveryCity = (HtmlInputText) findComponentInRoot("textDeliveryCity");
		}
		return textDeliveryCity;
	}

	protected HtmlFormItem getFormItemDeliveryState() {
		if (formItemDeliveryState == null) {
			formItemDeliveryState = (HtmlFormItem) findComponentInRoot("formItemDeliveryState");
		}
		return formItemDeliveryState;
	}

	protected HtmlSelectOneMenu getMenuDeliveryState() {
		if (menuDeliveryState == null) {
			menuDeliveryState = (HtmlSelectOneMenu) findComponentInRoot("menuDeliveryState");
		}
		return menuDeliveryState;
	}

	protected HtmlInputText getTextDeliveryZip() {
		if (textDeliveryZip == null) {
			textDeliveryZip = (HtmlInputText) findComponentInRoot("textDeliveryZip");
		}
		return textDeliveryZip;
	}

	protected HtmlFormItem getFormItemDeliveryPhone() {
		if (formItemDeliveryPhone == null) {
			formItemDeliveryPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryPhone");
		}
		return formItemDeliveryPhone;
	}

	protected HtmlInputText getTextDeliveryPhoneAreaCode() {
		if (textDeliveryPhoneAreaCode == null) {
			textDeliveryPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryPhoneAreaCode");
		}
		return textDeliveryPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist4() {
		if (assist4 == null) {
			assist4 = (HtmlInputHelperAssist) findComponentInRoot("assist4");
		}
		return assist4;
	}

	protected HtmlInputText getTextDeliveryPhoneExchange() {
		if (textDeliveryPhoneExchange == null) {
			textDeliveryPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExchange");
		}
		return textDeliveryPhoneExchange;
	}

	protected HtmlInputHelperAssist getAssist5() {
		if (assist5 == null) {
			assist5 = (HtmlInputHelperAssist) findComponentInRoot("assist5");
		}
		return assist5;
	}

	protected HtmlInputText getTextDeliveryPhoneExtension() {
		if (textDeliveryPhoneExtension == null) {
			textDeliveryPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryPhoneExtension");
		}
		return textDeliveryPhoneExtension;
	}

	protected HtmlFormItem getFormItemDeliveryWorkPhone() {
		if (formItemDeliveryWorkPhone == null) {
			formItemDeliveryWorkPhone = (HtmlFormItem) findComponentInRoot("formItemDeliveryWorkPhone");
		}
		return formItemDeliveryWorkPhone;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneAreaCode() {
		if (textDeliveryWorkPhoneAreaCode == null) {
			textDeliveryWorkPhoneAreaCode = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneAreaCode");
		}
		return textDeliveryWorkPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist44() {
		if (assist44 == null) {
			assist44 = (HtmlInputHelperAssist) findComponentInRoot("assist44");
		}
		return assist44;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExchange() {
		if (textDeliveryWorkPhoneExchange == null) {
			textDeliveryWorkPhoneExchange = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExchange");
		}
		return textDeliveryWorkPhoneExchange;
	}

	protected HtmlInputHelperAssist getAssist45() {
		if (assist45 == null) {
			assist45 = (HtmlInputHelperAssist) findComponentInRoot("assist45");
		}
		return assist45;
	}

	protected HtmlInputText getTextDeliveryWorkPhoneExtension() {
		if (textDeliveryWorkPhoneExtension == null) {
			textDeliveryWorkPhoneExtension = (HtmlInputText) findComponentInRoot("textDeliveryWorkPhoneExtension");
		}
		return textDeliveryWorkPhoneExtension;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlInputText getTextEmailAddressRecipient() {
		if (textEmailAddressRecipient == null) {
			textEmailAddressRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressRecipient");
		}
		return textEmailAddressRecipient;
	}

	protected HtmlFormItem getFormItemEmailAddressConfirm() {
		if (formItemEmailAddressConfirm == null) {
			formItemEmailAddressConfirm = (HtmlFormItem) findComponentInRoot("formItemEmailAddressConfirm");
		}
		return formItemEmailAddressConfirm;
	}

	protected HtmlInputText getTextEmailAddressConfirmRecipient() {
		if (textEmailAddressConfirmRecipient == null) {
			textEmailAddressConfirmRecipient = (HtmlInputText) findComponentInRoot("textEmailAddressConfirmRecipient");
		}
		return textEmailAddressConfirmRecipient;
	}

	protected HtmlPanelGrid getGridDeliveryAddressBottomFacet() {
		if (gridDeliveryAddressBottomFacet == null) {
			gridDeliveryAddressBottomFacet = (HtmlPanelGrid) findComponentInRoot("gridDeliveryAddressBottomFacet");
		}
		return gridDeliveryAddressBottomFacet;
	}

	protected HtmlPanelGrid getGridDeliveryInformationPanelFooterGrid() {
		if (gridDeliveryInformationPanelFooterGrid == null) {
			gridDeliveryInformationPanelFooterGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryInformationPanelFooterGrid");
		}
		return gridDeliveryInformationPanelFooterGrid;
	}

	protected HtmlCommandExButton getButtonGetDeliveryMethodButton() {
		if (buttonGetDeliveryMethodButton == null) {
			buttonGetDeliveryMethodButton = (HtmlCommandExButton) findComponentInRoot("buttonGetDeliveryMethodButton");
		}
		return buttonGetDeliveryMethodButton;
	}

	protected HtmlPanelGrid getGridDelMethodResultsGrid() {
		if (gridDelMethodResultsGrid == null) {
			gridDelMethodResultsGrid = (HtmlPanelGrid) findComponentInRoot("gridDelMethodResultsGrid");
		}
		return gridDelMethodResultsGrid;
	}

	protected HtmlOutputText getTextDeliveryMethod() {
		if (textDeliveryMethod == null) {
			textDeliveryMethod = (HtmlOutputText) findComponentInRoot("textDeliveryMethod");
		}
		return textDeliveryMethod;
	}

	protected HtmlOutputText getTextDeliveryMethodTextDeterminedValue() {
		if (textDeliveryMethodTextDeterminedValue == null) {
			textDeliveryMethodTextDeterminedValue = (HtmlOutputText) findComponentInRoot("textDeliveryMethodTextDeterminedValue");
		}
		return textDeliveryMethodTextDeterminedValue;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGridDeliveryMethodInfoGrid() {
		if (gridDeliveryMethodInfoGrid == null) {
			gridDeliveryMethodInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridDeliveryMethodInfoGrid");
		}
		return gridDeliveryMethodInfoGrid;
	}

	protected HtmlOutputLinkEx getLinkExDelMethodCheckHelpLink() {
		if (linkExDelMethodCheckHelpLink == null) {
			linkExDelMethodCheckHelpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExDelMethodCheckHelpLink");
		}
		return linkExDelMethodCheckHelpLink;
	}

	protected HtmlOutputText getTextDelMethodHelpText() {
		if (textDelMethodHelpText == null) {
			textDelMethodHelpText = (HtmlOutputText) findComponentInRoot("textDelMethodHelpText");
		}
		return textDelMethodHelpText;
	}

	protected HtmlPanelGrid getPanelGridBillingDifferentThanDelGrid() {
		if (panelGridBillingDifferentThanDelGrid == null) {
			panelGridBillingDifferentThanDelGrid = (HtmlPanelGrid) findComponentInRoot("panelGridBillingDifferentThanDelGrid");
		}
		return panelGridBillingDifferentThanDelGrid;
	}

	protected HtmlPanelFormBox getFormBoxBillDifferentFromDelSelectionFormBox() {
		if (formBoxBillDifferentFromDelSelectionFormBox == null) {
			formBoxBillDifferentFromDelSelectionFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxBillDifferentFromDelSelectionFormBox");
		}
		return formBoxBillDifferentFromDelSelectionFormBox;
	}

	protected HtmlFormItem getFormItemBillDifferentFromDelSelector() {
		if (formItemBillDifferentFromDelSelector == null) {
			formItemBillDifferentFromDelSelector = (HtmlFormItem) findComponentInRoot("formItemBillDifferentFromDelSelector");
		}
		return formItemBillDifferentFromDelSelector;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxIsBillDifferentFromDelSelector() {
		if (checkboxIsBillDifferentFromDelSelector == null) {
			checkboxIsBillDifferentFromDelSelector = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxIsBillDifferentFromDelSelector");
		}
		return checkboxIsBillDifferentFromDelSelector;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlPanelGrid getGridBillingAddress() {
		if (gridBillingAddress == null) {
			gridBillingAddress = (HtmlPanelGrid) findComponentInRoot("gridBillingAddress");
		}
		return gridBillingAddress;
	}

	protected HtmlPanelFormBox getFormBoxBillingAddress() {
		if (formBoxBillingAddress == null) {
			formBoxBillingAddress = (HtmlPanelFormBox) findComponentInRoot("formBoxBillingAddress");
		}
		return formBoxBillingAddress;
	}

	protected HtmlFormItem getFormItemPayersEmailAddress() {
		if (formItemPayersEmailAddress == null) {
			formItemPayersEmailAddress = (HtmlFormItem) findComponentInRoot("formItemPayersEmailAddress");
		}
		return formItemPayersEmailAddress;
	}

	protected HtmlInputText getTextPurchaserEmailAddress() {
		if (textPurchaserEmailAddress == null) {
			textPurchaserEmailAddress = (HtmlInputText) findComponentInRoot("textPurchaserEmailAddress");
		}
		return textPurchaserEmailAddress;
	}

	protected HtmlFormItem getFormItemBillingFirstName() {
		if (formItemBillingFirstName == null) {
			formItemBillingFirstName = (HtmlFormItem) findComponentInRoot("formItemBillingFirstName");
		}
		return formItemBillingFirstName;
	}

	protected HtmlInputText getTextBillingFirstName() {
		if (textBillingFirstName == null) {
			textBillingFirstName = (HtmlInputText) findComponentInRoot("textBillingFirstName");
		}
		return textBillingFirstName;
	}

	protected HtmlInputText getTextBillingLastName() {
		if (textBillingLastName == null) {
			textBillingLastName = (HtmlInputText) findComponentInRoot("textBillingLastName");
		}
		return textBillingLastName;
	}

	protected HtmlFormItem getFormItemBillingCompanyName() {
		if (formItemBillingCompanyName == null) {
			formItemBillingCompanyName = (HtmlFormItem) findComponentInRoot("formItemBillingCompanyName");
		}
		return formItemBillingCompanyName;
	}

	protected HtmlInputText getTextBillingCompanyName() {
		if (textBillingCompanyName == null) {
			textBillingCompanyName = (HtmlInputText) findComponentInRoot("textBillingCompanyName");
		}
		return textBillingCompanyName;
	}

	protected HtmlFormItem getFormItemBillingAddress1() {
		if (formItemBillingAddress1 == null) {
			formItemBillingAddress1 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress1");
		}
		return formItemBillingAddress1;
	}

	protected HtmlInputText getTextBillingAddress1() {
		if (textBillingAddress1 == null) {
			textBillingAddress1 = (HtmlInputText) findComponentInRoot("textBillingAddress1");
		}
		return textBillingAddress1;
	}

	protected HtmlInputText getTextBillingAptSuite() {
		if (textBillingAptSuite == null) {
			textBillingAptSuite = (HtmlInputText) findComponentInRoot("textBillingAptSuite");
		}
		return textBillingAptSuite;
	}

	protected HtmlFormItem getFormItemBillingAddress2() {
		if (formItemBillingAddress2 == null) {
			formItemBillingAddress2 = (HtmlFormItem) findComponentInRoot("formItemBillingAddress2");
		}
		return formItemBillingAddress2;
	}

	protected HtmlInputText getTextBillingAddress2() {
		if (textBillingAddress2 == null) {
			textBillingAddress2 = (HtmlInputText) findComponentInRoot("textBillingAddress2");
		}
		return textBillingAddress2;
	}

	protected HtmlFormItem getFormItemBillingCity() {
		if (formItemBillingCity == null) {
			formItemBillingCity = (HtmlFormItem) findComponentInRoot("formItemBillingCity");
		}
		return formItemBillingCity;
	}

	protected HtmlInputText getTextBillingCity() {
		if (textBillingCity == null) {
			textBillingCity = (HtmlInputText) findComponentInRoot("textBillingCity");
		}
		return textBillingCity;
	}

	protected HtmlFormItem getFormItemBillingState() {
		if (formItemBillingState == null) {
			formItemBillingState = (HtmlFormItem) findComponentInRoot("formItemBillingState");
		}
		return formItemBillingState;
	}

	protected HtmlSelectOneMenu getMenuBillingState() {
		if (menuBillingState == null) {
			menuBillingState = (HtmlSelectOneMenu) findComponentInRoot("menuBillingState");
		}
		return menuBillingState;
	}

	protected HtmlInputText getTextBillingZipCode() {
		if (textBillingZipCode == null) {
			textBillingZipCode = (HtmlInputText) findComponentInRoot("textBillingZipCode");
		}
		return textBillingZipCode;
	}

	protected HtmlFormItem getFormItemBillingTelephone() {
		if (formItemBillingTelephone == null) {
			formItemBillingTelephone = (HtmlFormItem) findComponentInRoot("formItemBillingTelephone");
		}
		return formItemBillingTelephone;
	}

	protected HtmlInputText getTextBillingPhoneAreaCode() {
		if (textBillingPhoneAreaCode == null) {
			textBillingPhoneAreaCode = (HtmlInputText) findComponentInRoot("textBillingPhoneAreaCode");
		}
		return textBillingPhoneAreaCode;
	}

	protected HtmlInputHelperAssist getAssist2() {
		if (assist2 == null) {
			assist2 = (HtmlInputHelperAssist) findComponentInRoot("assist2");
		}
		return assist2;
	}

	protected HtmlInputText getTextBillingPhoneExchange() {
		if (textBillingPhoneExchange == null) {
			textBillingPhoneExchange = (HtmlInputText) findComponentInRoot("textBillingPhoneExchange");
		}
		return textBillingPhoneExchange;
	}

	protected HtmlInputHelperAssist getAssist3() {
		if (assist3 == null) {
			assist3 = (HtmlInputHelperAssist) findComponentInRoot("assist3");
		}
		return assist3;
	}

	protected HtmlInputText getTextBillingPhoneExtension() {
		if (textBillingPhoneExtension == null) {
			textBillingPhoneExtension = (HtmlInputText) findComponentInRoot("textBillingPhoneExtension");
		}
		return textBillingPhoneExtension;
	}

	protected HtmlPanelGrid getGridPaymentInformationGridLabelGrid() {
		if (gridPaymentInformationGridLabelGrid == null) {
			gridPaymentInformationGridLabelGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGridLabelGrid");
		}
		return gridPaymentInformationGridLabelGrid;
	}

	protected HtmlPanelFormBox getFormBoxPaymentHeaderFormBox() {
		if (formBoxPaymentHeaderFormBox == null) {
			formBoxPaymentHeaderFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentHeaderFormBox");
		}
		return formBoxPaymentHeaderFormBox;
	}

	protected HtmlFormItem getFormItemBillMeFormItem() {
		if (formItemBillMeFormItem == null) {
			formItemBillMeFormItem = (HtmlFormItem) findComponentInRoot("formItemBillMeFormItem");
		}
		return formItemBillMeFormItem;
	}

	protected HtmlSelectOneRadio getRadioPaymentMethod() {
		if (radioPaymentMethod == null) {
			radioPaymentMethod = (HtmlSelectOneRadio) findComponentInRoot("radioPaymentMethod");
		}
		return radioPaymentMethod;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	protected HtmlPanelGrid getGridForceBillMeInfoGrid() {
		if (gridForceBillMeInfoGrid == null) {
			gridForceBillMeInfoGrid = (HtmlPanelGrid) findComponentInRoot("gridForceBillMeInfoGrid");
		}
		return gridForceBillMeInfoGrid;
	}

	protected HtmlOutputText getTextForceBillMeText() {
		if (textForceBillMeText == null) {
			textForceBillMeText = (HtmlOutputText) findComponentInRoot("textForceBillMeText");
		}
		return textForceBillMeText;
	}

	protected HtmlPanelGrid getGridPaymentInformationGrid() {
		if (gridPaymentInformationGrid == null) {
			gridPaymentInformationGrid = (HtmlPanelGrid) findComponentInRoot("gridPaymentInformationGrid");
		}
		return gridPaymentInformationGrid;
	}

	protected HtmlPanelGrid getGridPaymentGridBottomFacet() {
		if (gridPaymentGridBottomFacet == null) {
			gridPaymentGridBottomFacet = (HtmlPanelGrid) findComponentInRoot("gridPaymentGridBottomFacet");
		}
		return gridPaymentGridBottomFacet;
	}

	protected HtmlGraphicImageEx getImageExSpacerImage() {
		if (imageExSpacerImage == null) {
			imageExSpacerImage = (HtmlGraphicImageEx) findComponentInRoot("imageExSpacerImage");
		}
		return imageExSpacerImage;
	}

	protected HtmlPanelFormBox getFormBoxPaymentInfo() {
		if (formBoxPaymentInfo == null) {
			formBoxPaymentInfo = (HtmlPanelFormBox) findComponentInRoot("formBoxPaymentInfo");
		}
		return formBoxPaymentInfo;
	}

	protected HtmlFormItem getFormItemCreditCardNumber() {
		if (formItemCreditCardNumber == null) {
			formItemCreditCardNumber = (HtmlFormItem) findComponentInRoot("formItemCreditCardNumber");
		}
		return formItemCreditCardNumber;
	}

	protected HtmlInputText getTextCreditCardNumber() {
		if (textCreditCardNumber == null) {
			textCreditCardNumber = (HtmlInputText) findComponentInRoot("textCreditCardNumber");
		}
		return textCreditCardNumber;
	}

	protected HtmlInputHelperAssist getAssist6() {
		if (assist6 == null) {
			assist6 = (HtmlInputHelperAssist) findComponentInRoot("assist6");
		}
		return assist6;
	}

	protected HtmlFormItem getFormItemCCExpirationDate() {
		if (formItemCCExpirationDate == null) {
			formItemCCExpirationDate = (HtmlFormItem) findComponentInRoot("formItemCCExpirationDate");
		}
		return formItemCCExpirationDate;
	}

	protected HtmlSelectOneMenu getMenuCCExpireMonth() {
		if (menuCCExpireMonth == null) {
			menuCCExpireMonth = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireMonth");
		}
		return menuCCExpireMonth;
	}

	protected UISelectItem getSelectItem16() {
		if (selectItem16 == null) {
			selectItem16 = (UISelectItem) findComponentInRoot("selectItem16");
		}
		return selectItem16;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

	protected UISelectItem getSelectItem4() {
		if (selectItem4 == null) {
			selectItem4 = (UISelectItem) findComponentInRoot("selectItem4");
		}
		return selectItem4;
	}

	protected UISelectItem getSelectItem5() {
		if (selectItem5 == null) {
			selectItem5 = (UISelectItem) findComponentInRoot("selectItem5");
		}
		return selectItem5;
	}

	protected UISelectItem getSelectItem6() {
		if (selectItem6 == null) {
			selectItem6 = (UISelectItem) findComponentInRoot("selectItem6");
		}
		return selectItem6;
	}

	protected UISelectItem getSelectItem7() {
		if (selectItem7 == null) {
			selectItem7 = (UISelectItem) findComponentInRoot("selectItem7");
		}
		return selectItem7;
	}

	protected UISelectItem getSelectItem8() {
		if (selectItem8 == null) {
			selectItem8 = (UISelectItem) findComponentInRoot("selectItem8");
		}
		return selectItem8;
	}

	protected UISelectItem getSelectItem9() {
		if (selectItem9 == null) {
			selectItem9 = (UISelectItem) findComponentInRoot("selectItem9");
		}
		return selectItem9;
	}

	protected UISelectItem getSelectItem10() {
		if (selectItem10 == null) {
			selectItem10 = (UISelectItem) findComponentInRoot("selectItem10");
		}
		return selectItem10;
	}

	protected UISelectItem getSelectItem11() {
		if (selectItem11 == null) {
			selectItem11 = (UISelectItem) findComponentInRoot("selectItem11");
		}
		return selectItem11;
	}

	protected UISelectItem getSelectItem12() {
		if (selectItem12 == null) {
			selectItem12 = (UISelectItem) findComponentInRoot("selectItem12");
		}
		return selectItem12;
	}

	protected UISelectItem getSelectItem13() {
		if (selectItem13 == null) {
			selectItem13 = (UISelectItem) findComponentInRoot("selectItem13");
		}
		return selectItem13;
	}

	protected UISelectItem getSelectItem14() {
		if (selectItem14 == null) {
			selectItem14 = (UISelectItem) findComponentInRoot("selectItem14");
		}
		return selectItem14;
	}

	protected HtmlSelectOneMenu getMenuCCExpireYear() {
		if (menuCCExpireYear == null) {
			menuCCExpireYear = (HtmlSelectOneMenu) findComponentInRoot("menuCCExpireYear");
		}
		return menuCCExpireYear;
	}

	protected UISelectItems getSelectItems1() {
		if (selectItems1 == null) {
			selectItems1 = (UISelectItems) findComponentInRoot("selectItems1");
		}
		return selectItems1;
	}

	protected HtmlPanelGrid getPanelGridCreditCardImageGrid() {
		if (panelGridCreditCardImageGrid == null) {
			panelGridCreditCardImageGrid = (HtmlPanelGrid) findComponentInRoot("panelGridCreditCardImageGrid");
		}
		return panelGridCreditCardImageGrid;
	}

	protected HtmlJspPanel getJspPanelCreditCardImages() {
		if (jspPanelCreditCardImages == null) {
			jspPanelCreditCardImages = (HtmlJspPanel) findComponentInRoot("jspPanelCreditCardImages");
		}
		return jspPanelCreditCardImages;
	}

	protected HtmlGraphicImageEx getImageExAmEx1() {
		if (imageExAmEx1 == null) {
			imageExAmEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageExAmEx1");
		}
		return imageExAmEx1;
	}

	protected HtmlGraphicImageEx getImageExDiscover1() {
		if (imageExDiscover1 == null) {
			imageExDiscover1 = (HtmlGraphicImageEx) findComponentInRoot("imageExDiscover1");
		}
		return imageExDiscover1;
	}

	protected HtmlGraphicImageEx getImageExMasterCard1() {
		if (imageExMasterCard1 == null) {
			imageExMasterCard1 = (HtmlGraphicImageEx) findComponentInRoot("imageExMasterCard1");
		}
		return imageExMasterCard1;
	}

	protected HtmlGraphicImageEx getImageExVisaLogo1() {
		if (imageExVisaLogo1 == null) {
			imageExVisaLogo1 = (HtmlGraphicImageEx) findComponentInRoot("imageExVisaLogo1");
		}
		return imageExVisaLogo1;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGrid() {
		if (gridEZPAYOptionsGrid == null) {
			gridEZPAYOptionsGrid = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGrid");
		}
		return gridEZPAYOptionsGrid;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptions() {
		if (radioRenewalOptions == null) {
			radioRenewalOptions = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptions");
		}
		return radioRenewalOptions;
	}

	protected UISelectItem getSelectItem17() {
		if (selectItem17 == null) {
			selectItem17 = (UISelectItem) findComponentInRoot("selectItem17");
		}
		return selectItem17;
	}

	protected UISelectItem getSelectItem18() {
		if (selectItem18 == null) {
			selectItem18 = (UISelectItem) findComponentInRoot("selectItem18");
		}
		return selectItem18;
	}

	protected HtmlOutputText getTextCCAgreement() {
		if (textCCAgreement == null) {
			textCCAgreement = (HtmlOutputText) findComponentInRoot("textCCAgreement");
		}
		return textCCAgreement;
	}

	protected HtmlOutputText getTextEZPayConfirm() {
		if (textEZPayConfirm == null) {
			textEZPayConfirm = (HtmlOutputText) findComponentInRoot("textEZPayConfirm");
		}
		return textEZPayConfirm;
	}

	protected HtmlOutputText getTextEzPayInfo() {
		if (textEzPayInfo == null) {
			textEzPayInfo = (HtmlOutputText) findComponentInRoot("textEzPayInfo");
		}
		return textEzPayInfo;
	}

	protected HtmlPanelGrid getGridEZPAYOptionsGridGift() {
		if (gridEZPAYOptionsGridGift == null) {
			gridEZPAYOptionsGridGift = (HtmlPanelGrid) findComponentInRoot("gridEZPAYOptionsGridGift");
		}
		return gridEZPAYOptionsGridGift;
	}

	protected HtmlSelectOneRadio getRadioRenewalOptionsGift() {
		if (radioRenewalOptionsGift == null) {
			radioRenewalOptionsGift = (HtmlSelectOneRadio) findComponentInRoot("radioRenewalOptionsGift");
		}
		return radioRenewalOptionsGift;
	}

	protected UISelectItem getSelectItem19() {
		if (selectItem19 == null) {
			selectItem19 = (UISelectItem) findComponentInRoot("selectItem19");
		}
		return selectItem19;
	}

	protected UISelectItem getSelectItem20() {
		if (selectItem20 == null) {
			selectItem20 = (UISelectItem) findComponentInRoot("selectItem20");
		}
		return selectItem20;
	}

	protected UISelectItem getSelectItem15() {
		if (selectItem15 == null) {
			selectItem15 = (UISelectItem) findComponentInRoot("selectItem15");
		}
		return selectItem15;
	}

	protected HtmlOutputText getTextCCAgreement2() {
		if (textCCAgreement2 == null) {
			textCCAgreement2 = (HtmlOutputText) findComponentInRoot("textCCAgreement2");
		}
		return textCCAgreement2;
	}

	protected HtmlOutputText getTextEzPayInfoGift() {
		if (textEzPayInfoGift == null) {
			textEzPayInfoGift = (HtmlOutputText) findComponentInRoot("textEzPayInfoGift");
		}
		return textEzPayInfoGift;
	}

	protected HtmlPanelGrid getGridTrialDisclaimerGrid() {
		if (gridTrialDisclaimerGrid == null) {
			gridTrialDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridTrialDisclaimerGrid");
		}
		return gridTrialDisclaimerGrid;
	}

	protected HtmlOutputText getTextDisclaimerText() {
		if (textDisclaimerText == null) {
			textDisclaimerText = (HtmlOutputText) findComponentInRoot("textDisclaimerText");
		}
		return textDisclaimerText;
	}

	protected HtmlPanelGrid getGridOfferDisclaimerGrid() {
		if (gridOfferDisclaimerGrid == null) {
			gridOfferDisclaimerGrid = (HtmlPanelGrid) findComponentInRoot("gridOfferDisclaimerGrid");
		}
		return gridOfferDisclaimerGrid;
	}

	protected HtmlOutputText getTextOfferDisclaimerText() {
		if (textOfferDisclaimerText == null) {
			textOfferDisclaimerText = (HtmlOutputText) findComponentInRoot("textOfferDisclaimerText");
		}
		return textOfferDisclaimerText;
	}

	protected HtmlPanelGrid getPanelGridFormSubmissionGrid() {
		if (panelGridFormSubmissionGrid == null) {
			panelGridFormSubmissionGrid = (HtmlPanelGrid) findComponentInRoot("panelGridFormSubmissionGrid");
		}
		return panelGridFormSubmissionGrid;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected HtmlCommandExButton getButtonCancelOrder() {
		if (buttonCancelOrder == null) {
			buttonCancelOrder = (HtmlCommandExButton) findComponentInRoot("buttonCancelOrder");
		}
		return buttonCancelOrder;
	}

	protected HtmlMessages getMessagesAllMesssages() {
		if (messagesAllMesssages == null) {
			messagesAllMesssages = (HtmlMessages) findComponentInRoot("messagesAllMesssages");
		}
		return messagesAllMesssages;
	}

	protected HtmlPanelGrid getRightColImageSpot1() {
		if (rightColImageSpot1 == null) {
			rightColImageSpot1 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot1");
		}
		return rightColImageSpot1;
	}

	protected HtmlOutputLinkEx getLinkExRightColImageSpot1Link() {
		if (linkExRightColImageSpot1Link == null) {
			linkExRightColImageSpot1Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColImageSpot1Link");
		}
		return linkExRightColImageSpot1Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1() {
		if (imageExRightColImage1 == null) {
			imageExRightColImage1 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1");
		}
		return imageExRightColImage1;
	}

	protected HtmlGraphicImageEx getImageExRightColImage1NoLink() {
		if (imageExRightColImage1NoLink == null) {
			imageExRightColImage1NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage1NoLink");
		}
		return imageExRightColImage1NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot1Grid() {
		if (gridRightColHTMLSpot1Grid == null) {
			gridRightColHTMLSpot1Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot1Grid");
		}
		return gridRightColHTMLSpot1Grid;
	}

	protected HtmlOutputText getRightColHTMLSpot1Text() {
		if (RightColHTMLSpot1Text == null) {
			RightColHTMLSpot1Text = (HtmlOutputText) findComponentInRoot("RightColHTMLSpot1Text");
		}
		return RightColHTMLSpot1Text;
	}

	protected HtmlPanelGrid getGridVideoGrid() {
		if (gridVideoGrid == null) {
			gridVideoGrid = (HtmlPanelGrid) findComponentInRoot("gridVideoGrid");
		}
		return gridVideoGrid;
	}

	protected HtmlOutputLinkEx getLinkExVideoLink_B() {
		if (linkExVideoLink_B == null) {
			linkExVideoLink_B = (HtmlOutputLinkEx) findComponentInRoot("linkExVideoLink_B");
		}
		return linkExVideoLink_B;
	}

	protected HtmlGraphicImageEx getImageExVideoSweepsGraphic_B() {
		if (imageExVideoSweepsGraphic_B == null) {
			imageExVideoSweepsGraphic_B = (HtmlGraphicImageEx) findComponentInRoot("imageExVideoSweepsGraphic_B");
		}
		return imageExVideoSweepsGraphic_B;
	}

	protected HtmlOutputText getTextEEVideoText() {
		if (textEEVideoText == null) {
			textEEVideoText = (HtmlOutputText) findComponentInRoot("textEEVideoText");
		}
		return textEEVideoText;
	}

	protected HtmlPanelGrid getRightColImageSpot2() {
		if (rightColImageSpot2 == null) {
			rightColImageSpot2 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot2");
		}
		return rightColImageSpot2;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot2Link() {
		if (linkExRightColSpot2Link == null) {
			linkExRightColSpot2Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot2Link");
		}
		return linkExRightColSpot2Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2() {
		if (imageExRightColImage2 == null) {
			imageExRightColImage2 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2");
		}
		return imageExRightColImage2;
	}

	protected HtmlGraphicImageEx getImageExRightColImage2NoLink() {
		if (imageExRightColImage2NoLink == null) {
			imageExRightColImage2NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage2NoLink");
		}
		return imageExRightColImage2NoLink;
	}

	protected HtmlPanelGrid getGridRightColHTMLSpot2Grid() {
		if (gridRightColHTMLSpot2Grid == null) {
			gridRightColHTMLSpot2Grid = (HtmlPanelGrid) findComponentInRoot("gridRightColHTMLSpot2Grid");
		}
		return gridRightColHTMLSpot2Grid;
	}

	protected HtmlOutputText getRightColHTMLSpot2Text() {
		if (RightColHTMLSpot2Text == null) {
			RightColHTMLSpot2Text = (HtmlOutputText) findComponentInRoot("RightColHTMLSpot2Text");
		}
		return RightColHTMLSpot2Text;
	}

	protected HtmlPanelGrid getRightColImageSpot3() {
		if (rightColImageSpot3 == null) {
			rightColImageSpot3 = (HtmlPanelGrid) findComponentInRoot("rightColImageSpot3");
		}
		return rightColImageSpot3;
	}

	protected HtmlOutputLinkEx getLinkExRightColSpot3Link() {
		if (linkExRightColSpot3Link == null) {
			linkExRightColSpot3Link = (HtmlOutputLinkEx) findComponentInRoot("linkExRightColSpot3Link");
		}
		return linkExRightColSpot3Link;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3() {
		if (imageExRightColImage3 == null) {
			imageExRightColImage3 = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3");
		}
		return imageExRightColImage3;
	}

	protected HtmlGraphicImageEx getImageExRightColImage3NoLink() {
		if (imageExRightColImage3NoLink == null) {
			imageExRightColImage3NoLink = (HtmlGraphicImageEx) findComponentInRoot("imageExRightColImage3NoLink");
		}
		return imageExRightColImage3NoLink;
	}

	protected HtmlPanelDialog getDialogDelMethodDialog() {
		if (dialogDelMethodDialog == null) {
			dialogDelMethodDialog = (HtmlPanelDialog) findComponentInRoot("dialogDelMethodDialog");
		}
		return dialogDelMethodDialog;
	}

	protected HtmlPanelGroup getGroupDelMethod1() {
		if (groupDelMethod1 == null) {
			groupDelMethod1 = (HtmlPanelGroup) findComponentInRoot("groupDelMethod1");
		}
		return groupDelMethod1;
	}

	protected HtmlPanelGrid getGridDelMethod2() {
		if (gridDelMethod2 == null) {
			gridDelMethod2 = (HtmlPanelGrid) findComponentInRoot("gridDelMethod2");
		}
		return gridDelMethod2;
	}

	protected HtmlOutputText getTextDelMethodDetailHelp() {
		if (textDelMethodDetailHelp == null) {
			textDelMethodDetailHelp = (HtmlOutputText) findComponentInRoot("textDelMethodDetailHelp");
		}
		return textDelMethodDetailHelp;
	}

	protected HtmlCommandExButton getButtonDelMethod() {
		if (buttonDelMethod == null) {
			buttonDelMethod = (HtmlCommandExButton) findComponentInRoot("buttonDelMethod");
		}
		return buttonDelMethod;
	}

	protected HtmlBehavior getBehaviorDelMethod4() {
		if (behaviorDelMethod4 == null) {
			behaviorDelMethod4 = (HtmlBehavior) findComponentInRoot("behaviorDelMethod4");
		}
		return behaviorDelMethod4;
	}

	protected HtmlPanelDialog getDialogAdditionalAddrHelp() {
		if (dialogAdditionalAddrHelp == null) {
			dialogAdditionalAddrHelp = (HtmlPanelDialog) findComponentInRoot("dialogAdditionalAddrHelp");
		}
		return dialogAdditionalAddrHelp;
	}

	protected HtmlPanelGroup getGroupAddlAddrHelpGroup1() {
		if (groupAddlAddrHelpGroup1 == null) {
			groupAddlAddrHelpGroup1 = (HtmlPanelGroup) findComponentInRoot("groupAddlAddrHelpGroup1");
		}
		return groupAddlAddrHelpGroup1;
	}

	protected HtmlPanelGrid getGrid1() {
		if (grid1 == null) {
			grid1 = (HtmlPanelGrid) findComponentInRoot("grid1");
		}
		return grid1;
	}

	protected HtmlOutputText getTextAdditionalAddrLearnMoreInfoText() {
		if (textAdditionalAddrLearnMoreInfoText == null) {
			textAdditionalAddrLearnMoreInfoText = (HtmlOutputText) findComponentInRoot("textAdditionalAddrLearnMoreInfoText");
		}
		return textAdditionalAddrLearnMoreInfoText;
	}

	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlInputHelperSetFocus getSetFocus1() {
		if (setFocus1 == null) {
			setFocus1 = (HtmlInputHelperSetFocus) findComponentInRoot("setFocus1");
		}
		return setFocus1;
	}

	protected HtmlInputHidden getStartDate() {
		if (startDate == null) {
			startDate = (HtmlInputHidden) findComponentInRoot("startDate");
		}
		return startDate;
	}

	protected HtmlInputHidden getIsForceBillMe() {
		if (isForceBillMe == null) {
			isForceBillMe = (HtmlInputHidden) findComponentInRoot("isForceBillMe");
		}
		return isForceBillMe;
	}

	protected HtmlInputHidden getPaymentMethodHidden() {
		if (paymentMethodHidden == null) {
			paymentMethodHidden = (HtmlInputHidden) findComponentInRoot("paymentMethodHidden");
		}
		return paymentMethodHidden;
	}

	protected HtmlBehaviorKeyPress getBehaviorKeyPress1() {
		if (behaviorKeyPress1 == null) {
			behaviorKeyPress1 = (HtmlBehaviorKeyPress) findComponentInRoot("behaviorKeyPress1");
		}
		return behaviorKeyPress1;
	}

	protected HtmlOutputText getTextProductTermSelected() {
		if (textProductTermSelected == null) {
			textProductTermSelected = (HtmlOutputText) findComponentInRoot("textProductTermSelected");
		}
		return textProductTermSelected;
	}

	protected HtmlOutputText getTextOfferSelectedTerm4() {
		if (textOfferSelectedTerm4 == null) {
			textOfferSelectedTerm4 = (HtmlOutputText) findComponentInRoot("textOfferSelectedTerm4");
		}
		return textOfferSelectedTerm4;
	}

	protected HtmlAjaxRefreshRequest getAjaxRefreshRequest1() {
		if (ajaxRefreshRequest1 == null) {
			ajaxRefreshRequest1 = (HtmlAjaxRefreshRequest) findComponentInRoot("ajaxRefreshRequest1");
		}
		return ajaxRefreshRequest1;
	}

	protected HtmlAjaxRefreshSubmit getAjaxRefreshSubmit1() {
		if (ajaxRefreshSubmit1 == null) {
			ajaxRefreshSubmit1 = (HtmlAjaxRefreshSubmit) findComponentInRoot("ajaxRefreshSubmit1");
		}
		return ajaxRefreshSubmit1;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected UINamingContainer getViewFragmentOrderForm1() {
		if (viewFragmentOrderForm1 == null) {
			viewFragmentOrderForm1 = (UINamingContainer) findComponentInRoot("viewFragmentOrderForm1");
		}
		return viewFragmentOrderForm1;
	}

}