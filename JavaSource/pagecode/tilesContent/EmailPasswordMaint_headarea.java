/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class EmailPasswordMaint_headarea extends PageCodeBase {

	protected UINamingContainer headerViewFragment;

	protected UINamingContainer getHeaderViewFragment() {
		if (headerViewFragment == null) {
			headerViewFragment = (UINamingContainer) findComponentInRoot("headerViewFragment");
		}
		return headerViewFragment;
	}

}