/**
 * 
 */
package pagecode.tilesContent.restricted;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlOutputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.usatoday.ncs.web.handlers.UserHandler;

/**
 * @author aeast
 *
 */
public class TrialLink_mainBodyArea extends PageCodeBase {

	protected UINamingContainer viewFragmentMainBody1;
	protected HtmlScriptCollector scriptCollectorMainBody1;
	protected HtmlForm form1;
	protected HtmlPanelGrid gridLinksGrid;
	protected HtmlPanelGroup groupAccountLinksHeaderGroup;
	protected HtmlPanelGrid gridAcctHeaderGrid;
	protected HtmlOutputText textAcctMngmtLabel;
	protected HtmlOutputText textSampleSignUpLabel;
	protected HtmlOutputLinkEx linkExSampleSignUpLink;
	protected UserHandler user;
	protected UINamingContainer getViewFragmentMainBody1() {
		if (viewFragmentMainBody1 == null) {
			viewFragmentMainBody1 = (UINamingContainer) findComponentInRoot("viewFragmentMainBody1");
		}
		return viewFragmentMainBody1;
	}

	protected HtmlScriptCollector getScriptCollectorMainBody1() {
		if (scriptCollectorMainBody1 == null) {
			scriptCollectorMainBody1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainBody1");
		}
		return scriptCollectorMainBody1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlPanelGrid getGridLinksGrid() {
		if (gridLinksGrid == null) {
			gridLinksGrid = (HtmlPanelGrid) findComponentInRoot("gridLinksGrid");
		}
		return gridLinksGrid;
	}

	protected HtmlPanelGroup getGroupAccountLinksHeaderGroup() {
		if (groupAccountLinksHeaderGroup == null) {
			groupAccountLinksHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupAccountLinksHeaderGroup");
		}
		return groupAccountLinksHeaderGroup;
	}

	protected HtmlPanelGrid getGridAcctHeaderGrid() {
		if (gridAcctHeaderGrid == null) {
			gridAcctHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridAcctHeaderGrid");
		}
		return gridAcctHeaderGrid;
	}

	protected HtmlOutputText getTextAcctMngmtLabel() {
		if (textAcctMngmtLabel == null) {
			textAcctMngmtLabel = (HtmlOutputText) findComponentInRoot("textAcctMngmtLabel");
		}
		return textAcctMngmtLabel;
	}

	protected HtmlOutputText getTextSampleSignUpLabel() {
		if (textSampleSignUpLabel == null) {
			textSampleSignUpLabel = (HtmlOutputText) findComponentInRoot("textSampleSignUpLabel");
		}
		return textSampleSignUpLabel;
	}

	protected HtmlOutputLinkEx getLinkExSampleSignUpLink() {
		if (linkExSampleSignUpLink == null) {
			linkExSampleSignUpLink = (HtmlOutputLinkEx) findComponentInRoot("linkExSampleSignUpLink");
		}
		return linkExSampleSignUpLink;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

}