/**
 * 
 */
package pagecode.tilesContent.restricted.accountMaintenance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.faces.application.FacesMessage;
import javax.faces.component.UINamingContainer;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlCommandExRowEdit;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.businessObjects.customer.TrialCustomerBO;
import com.usatoday.ncs.web.subscription.handlers.EmailRecordHandler;
import com.usatoday.ncs.web.subscription.handlers.EmailSearchFilterHandler;
import com.usatoday.ncs.web.subscription.handlers.EmailSearchHandler;
import com.usatoday.ncs.web.subscription.handlers.TrialCustomerHandler;

/**
 * @author aeast
 *
 */
public class SubscriberAccountSearch_mainBodyArea extends PageCodeBase {

	protected UINamingContainer viewFragmentMainBodyArea;
	protected HtmlPanelSection sectionEmailSearch;
	protected HtmlJspPanel jspPanel2;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlJspPanel jspPanel1;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlScriptCollector scriptCollectorMainBodyArea1;
	protected HtmlForm form1;
	protected HtmlOutputText text2;
	protected HtmlOutputText text1;
	protected HtmlPanelGrid gridEmailFilter;
	protected HtmlPanelGrid gridEmailFilterFooter1;
	protected HtmlOutputText text26;
	protected HtmlOutputText text27;
	protected HtmlOutputText textEmailLabel;
	protected HtmlInputText textEmailAddressFilter;
	protected HtmlOutputText textAccountNumberFilter;
	protected HtmlInputText text4;
	protected HtmlOutputText textCheckTrialDB;
	protected HtmlSelectBooleanCheckbox checkboxCheckTrialTable;
	protected EmailSearchHandler emailSearchHandler;
	protected EmailSearchFilterHandler emailSearchFilterHandler;
	protected HtmlDataTableEx tableExEmailSearchResults;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlJspPanel jspPanel6;
	protected HtmlGraphicImageEx imageEx6;
	protected HtmlJspPanel jspPanel5;
	protected HtmlGraphicImageEx imageEx5;
	protected HtmlPanelSection sectionCustomerResultsSection;
	protected HtmlOutputText textEmailAddress;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText textAcctNum;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText textPubCode;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText textGiftFlag;
	protected HtmlOutputText text13;
	protected HtmlOutputText text12;
	protected HtmlOutputText text8;
	protected UIColumnEx columnEx5;
	protected HtmlCommandExButton buttonResetPassword;
	protected UIParameter paramSelectedEmailAddress;
	protected HtmlMessages messages1;
	protected HtmlJspPanel jspPanel4;
	protected HtmlGraphicImageEx imageEx4;
	protected HtmlJspPanel jspPanel3;
	protected HtmlGraphicImageEx imageEx3;
	protected HtmlOutputText text11;
	protected HtmlOutputText textTrialPartnerNameColumnHeader;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText text15;
	protected HtmlOutputText text16;
	protected HtmlOutputText text17;
	protected HtmlOutputText text23;
	protected HtmlOutputText text18;
	protected HtmlOutputText text28;
	protected HtmlPanelSection sectionTrialCustomerSection;
	protected HtmlOutputText text10;
	protected HtmlOutputText text9;
	protected HtmlDataTableEx tableExTrialSearchResults;
	protected HtmlOutputText textTrialUserEmailAddress;
	protected UIColumnEx columnEx8;
	protected HtmlDataTableEx tableExSampleInnerTable;
	protected UIColumnEx columnEx10;
	protected HtmlOutputText text21;
	protected UIColumnEx columnEx11;
	protected HtmlOutputText text22;
	protected UIColumnEx columnEx13;
	protected UIColumnEx columnEx14;
	protected HtmlOutputText text29;
	protected HtmlOutputText text14;
	protected UIColumnEx columnEx6;
	protected HtmlCommandExButton buttonResetTrialPassword;
	protected UIParameter param1;
	protected HtmlPanelBox box1;
	protected HtmlPanelGrid gridTrialDTHeaderGrid;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText text19;
	protected HtmlOutputText text24;
	protected HtmlPanelBox box2;
	protected HtmlPanelGrid gridEmailCustomerHeaderGrid;
	protected HtmlOutputText text25;
	protected HtmlOutputText text30;
	protected HtmlOutputLinkEx linkEx2;
	protected HtmlCommandExRowEdit rowEditEmailAddress;
	protected HtmlJspPanel jspPanel7;
	protected UIColumnEx columnEx7;
	protected HtmlPanelFormBox formBoxEditEmailAddress;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlInputText text31NewEmail;
	protected HtmlPanelGrid gridtwoColumn11;
	protected HtmlCommandExButton buttonSubmitSearch;
	protected HtmlCommandExButton buttonClearSearchResults;
	protected HtmlOutputText text31;
	protected HtmlOutputText text32;
	protected HtmlPanelGrid gridPartnernameurlGrid1;
	protected HtmlOutputText text20;
	protected HtmlOutputText text33;
	protected HtmlOutputLinkEx linkEx3;
	protected HtmlOutputText text34;
	protected UIColumnEx columnEx12;
	protected HtmlOutputText text35;
	protected UIColumnEx columnEx15;
	protected HtmlCommandExButton buttonSendConfirmationEmail;
	protected UINamingContainer getViewFragmentMainBodyArea() {
		if (viewFragmentMainBodyArea == null) {
			viewFragmentMainBodyArea = (UINamingContainer) findComponentInRoot("viewFragmentMainBodyArea");
		}
		return viewFragmentMainBodyArea;
	}

	protected HtmlPanelSection getSectionEmailSearch() {
		if (sectionEmailSearch == null) {
			sectionEmailSearch = (HtmlPanelSection) findComponentInRoot("sectionEmailSearch");
		}
		return sectionEmailSearch;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlScriptCollector getScriptCollectorMainBodyArea1() {
		if (scriptCollectorMainBodyArea1 == null) {
			scriptCollectorMainBodyArea1 = (HtmlScriptCollector) findComponentInRoot("scriptCollectorMainBodyArea1");
		}
		return scriptCollectorMainBodyArea1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlPanelGrid getGridEmailFilter() {
		if (gridEmailFilter == null) {
			gridEmailFilter = (HtmlPanelGrid) findComponentInRoot("gridEmailFilter");
		}
		return gridEmailFilter;
	}

	protected HtmlPanelGrid getGridEmailFilterFooter1() {
		if (gridEmailFilterFooter1 == null) {
			gridEmailFilterFooter1 = (HtmlPanelGrid) findComponentInRoot("gridEmailFilterFooter1");
		}
		return gridEmailFilterFooter1;
	}

	protected HtmlOutputText getText26() {
		if (text26 == null) {
			text26 = (HtmlOutputText) findComponentInRoot("text26");
		}
		return text26;
	}

	protected HtmlOutputText getText27() {
		if (text27 == null) {
			text27 = (HtmlOutputText) findComponentInRoot("text27");
		}
		return text27;
	}

	protected HtmlOutputText getTextEmailLabel() {
		if (textEmailLabel == null) {
			textEmailLabel = (HtmlOutputText) findComponentInRoot("textEmailLabel");
		}
		return textEmailLabel;
	}

	protected HtmlInputText getTextEmailAddressFilter() {
		if (textEmailAddressFilter == null) {
			textEmailAddressFilter = (HtmlInputText) findComponentInRoot("textEmailAddressFilter");
		}
		return textEmailAddressFilter;
	}

	protected HtmlOutputText getTextAccountNumberFilter() {
		if (textAccountNumberFilter == null) {
			textAccountNumberFilter = (HtmlOutputText) findComponentInRoot("textAccountNumberFilter");
		}
		return textAccountNumberFilter;
	}

	protected HtmlInputText getText4() {
		if (text4 == null) {
			text4 = (HtmlInputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getTextCheckTrialDB() {
		if (textCheckTrialDB == null) {
			textCheckTrialDB = (HtmlOutputText) findComponentInRoot("textCheckTrialDB");
		}
		return textCheckTrialDB;
	}

	protected HtmlSelectBooleanCheckbox getCheckboxCheckTrialTable() {
		if (checkboxCheckTrialTable == null) {
			checkboxCheckTrialTable = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkboxCheckTrialTable");
		}
		return checkboxCheckTrialTable;
	}

	public String doButtonSubmitSearchAction() {
		// Type Java code that runs when the component is clicked
			try {
			ArrayList<EmailRecordHandler> eList = new ArrayList<EmailRecordHandler>();
			ArrayList<TrialCustomerHandler> tList = new ArrayList<TrialCustomerHandler>();

			HashMap<Integer, EmailRecordIntf> emailHash = new HashMap<Integer, EmailRecordIntf>();
			
			String emailFilter = this.getEmailSearchFilterHandler().getEmailAddressFilter();
			
			if (emailFilter != null && emailFilter.trim().length() > 0) {
				Collection<EmailRecordIntf> emails = EmailRecordBO.getEmailRecordsForEmailLike(emailFilter);
				for (EmailRecordIntf e : emails) {
					if (!emailHash.containsKey(e.getSerialNumber())) {
						emailHash.put(e.getSerialNumber(), e);
					}					
				}
				
				// check trial database too if checked
				if (this.getEmailSearchFilterHandler().getIsCheckTrials()) {
					Collection<TrialCustomerIntf> trialCustomers = TrialCustomerBO.getTrialCustomerByEmailLike(emailFilter);
					
					for(TrialCustomerIntf tCust : trialCustomers) {
						TrialCustomerHandler tch = new TrialCustomerHandler();
						tch.setTrialCustomer(tCust);
						tList.add(tch);
					}
					
					
				}
				
			}
			
			
			String accountFilter = this.getEmailSearchFilterHandler().getAccountNumber();
			if (accountFilter != null && accountFilter.trim().length() > 0) {
				Collection<EmailRecordIntf> emails = EmailRecordBO.getEmailRecordsForAccount(accountFilter.trim());
				for (EmailRecordIntf e : emails) {
					if (!emailHash.containsKey(e.getSerialNumber())) {
						emailHash.put(e.getSerialNumber(), e);
					}
				}
			}
			
			for (EmailRecordIntf em : emailHash.values()) {
				EmailRecordHandler eh = new EmailRecordHandler();
				eh.setEmailRecord(em);
				eList.add(eh);				
			}
			this.getEmailSearchHandler().setCustomerSearchResults(eList);
			
			this.getEmailSearchHandler().setTrialSearchResults(tList);
		}
		catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Failed retrieve email records: " + e.getMessage(), null);
            context.addMessage(null, message);
		}
		return "success";
	}

	/** 
	 * @managed-bean true
	 */
	protected EmailSearchHandler getEmailSearchHandler() {
		if (emailSearchHandler == null) {
			emailSearchHandler = (EmailSearchHandler) getManagedBean("emailSearchHandler");
		}
		return emailSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEmailSearchHandler(EmailSearchHandler emailSearchHandler) {
		this.emailSearchHandler = emailSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected EmailSearchFilterHandler getEmailSearchFilterHandler() {
		if (emailSearchFilterHandler == null) {
			emailSearchFilterHandler = (EmailSearchFilterHandler) getManagedBean("emailSearchFilterHandler");
		}
		return emailSearchFilterHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEmailSearchFilterHandler(
			EmailSearchFilterHandler emailSearchFilterHandler) {
		this.emailSearchFilterHandler = emailSearchFilterHandler;
	}

	protected HtmlDataTableEx getTableExEmailSearchResults() {
		if (tableExEmailSearchResults == null) {
			tableExEmailSearchResults = (HtmlDataTableEx) findComponentInRoot("tableExEmailSearchResults");
		}
		return tableExEmailSearchResults;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlJspPanel getJspPanel6() {
		if (jspPanel6 == null) {
			jspPanel6 = (HtmlJspPanel) findComponentInRoot("jspPanel6");
		}
		return jspPanel6;
	}

	protected HtmlGraphicImageEx getImageEx6() {
		if (imageEx6 == null) {
			imageEx6 = (HtmlGraphicImageEx) findComponentInRoot("imageEx6");
		}
		return imageEx6;
	}

	protected HtmlJspPanel getJspPanel5() {
		if (jspPanel5 == null) {
			jspPanel5 = (HtmlJspPanel) findComponentInRoot("jspPanel5");
		}
		return jspPanel5;
	}

	protected HtmlGraphicImageEx getImageEx5() {
		if (imageEx5 == null) {
			imageEx5 = (HtmlGraphicImageEx) findComponentInRoot("imageEx5");
		}
		return imageEx5;
	}

	protected HtmlPanelSection getSectionCustomerResultsSection() {
		if (sectionCustomerResultsSection == null) {
			sectionCustomerResultsSection = (HtmlPanelSection) findComponentInRoot("sectionCustomerResultsSection");
		}
		return sectionCustomerResultsSection;
	}

	protected HtmlOutputText getTextEmailAddress() {
		if (textEmailAddress == null) {
			textEmailAddress = (HtmlOutputText) findComponentInRoot("textEmailAddress");
		}
		return textEmailAddress;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getTextAcctNum() {
		if (textAcctNum == null) {
			textAcctNum = (HtmlOutputText) findComponentInRoot("textAcctNum");
		}
		return textAcctNum;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getTextPubCode() {
		if (textPubCode == null) {
			textPubCode = (HtmlOutputText) findComponentInRoot("textPubCode");
		}
		return textPubCode;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getTextGiftFlag() {
		if (textGiftFlag == null) {
			textGiftFlag = (HtmlOutputText) findComponentInRoot("textGiftFlag");
		}
		return textGiftFlag;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlCommandExButton getButtonResetPassword() {
		if (buttonResetPassword == null) {
			buttonResetPassword = (HtmlCommandExButton) findComponentInRoot("buttonResetPassword");
		}
		return buttonResetPassword;
	}

	protected UIParameter getParamSelectedEmailAddress() {
		if (paramSelectedEmailAddress == null) {
			paramSelectedEmailAddress = (UIParameter) findComponentInRoot("paramSelectedEmailAddress");
		}
		return paramSelectedEmailAddress;
	}

	public String doButtonResetPasswordAction() {
		// Type Java code that runs when the component is clicked
		// return "success"; // global 
		String responseString = "success";
		EmailRecordHandler selectedRecord = null;
		EmailSearchHandler searchResults = this.getEmailSearchHandler();
		try {

			selectedRecord = (EmailRecordHandler)this.getTableExEmailSearchResults().getRowData();
		    
			// retrieve all email records for email address in case the previous search was by account number and missed some.
			Collection<EmailRecordIntf> emailRecs = EmailRecordBO.getEmailRecordsForEmailAddress(selectedRecord.getEmailAddress());
			
			Collection<EmailRecordHandler> matchingRecords = new ArrayList<EmailRecordHandler>();
			
			for (EmailRecordIntf e : emailRecs) {
				EmailRecordHandler eh = new EmailRecordHandler();
				eh.setEmailRecord(e);
				
				matchingRecords.add(eh);
			}
			
			
		    if (matchingRecords.size() > 1) {
		    	searchResults.setSameEmailAddressRecords(matchingRecords);
		    	responseString = "multiple";
		    }
		    else {
		    	EmailRecordBO eRec = (EmailRecordBO)selectedRecord.getEmailRecord();
		    	eRec.setPassword("password");
		    	eRec.save();

		    	FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password for user:  " + selectedRecord.getEmailAddress() + " reset to 'password'", null);
	            this.getMessages1().setInfoStyle("color: blue;");
	            context.addMessage(null, message);
		    }
		}
		catch (Exception e) {
			e.printStackTrace();
			
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to Reset Password. " + e.getMessage(), null);
            this.getMessages1().setInfoStyle("color: red;");
            context.addMessage(null, message);
			
			responseString = "failure";
		}
		return responseString;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlJspPanel getJspPanel4() {
		if (jspPanel4 == null) {
			jspPanel4 = (HtmlJspPanel) findComponentInRoot("jspPanel4");
		}
		return jspPanel4;
	}

	protected HtmlGraphicImageEx getImageEx4() {
		if (imageEx4 == null) {
			imageEx4 = (HtmlGraphicImageEx) findComponentInRoot("imageEx4");
		}
		return imageEx4;
	}

	protected HtmlJspPanel getJspPanel3() {
		if (jspPanel3 == null) {
			jspPanel3 = (HtmlJspPanel) findComponentInRoot("jspPanel3");
		}
		return jspPanel3;
	}

	protected HtmlGraphicImageEx getImageEx3() {
		if (imageEx3 == null) {
			imageEx3 = (HtmlGraphicImageEx) findComponentInRoot("imageEx3");
		}
		return imageEx3;
	}

	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlOutputText getTextTrialPartnerNameColumnHeader() {
		if (textTrialPartnerNameColumnHeader == null) {
			textTrialPartnerNameColumnHeader = (HtmlOutputText) findComponentInRoot("textTrialPartnerNameColumnHeader");
		}
		return textTrialPartnerNameColumnHeader;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getText15() {
		if (text15 == null) {
			text15 = (HtmlOutputText) findComponentInRoot("text15");
		}
		return text15;
	}

	protected HtmlOutputText getText16() {
		if (text16 == null) {
			text16 = (HtmlOutputText) findComponentInRoot("text16");
		}
		return text16;
	}

	protected HtmlOutputText getText17() {
		if (text17 == null) {
			text17 = (HtmlOutputText) findComponentInRoot("text17");
		}
		return text17;
	}

	protected HtmlOutputText getText23() {
		if (text23 == null) {
			text23 = (HtmlOutputText) findComponentInRoot("text23");
		}
		return text23;
	}

	protected HtmlOutputText getText18() {
		if (text18 == null) {
			text18 = (HtmlOutputText) findComponentInRoot("text18");
		}
		return text18;
	}

	protected HtmlOutputText getText28() {
		if (text28 == null) {
			text28 = (HtmlOutputText) findComponentInRoot("text28");
		}
		return text28;
	}

	protected HtmlPanelSection getSectionTrialCustomerSection() {
		if (sectionTrialCustomerSection == null) {
			sectionTrialCustomerSection = (HtmlPanelSection) findComponentInRoot("sectionTrialCustomerSection");
		}
		return sectionTrialCustomerSection;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlDataTableEx getTableExTrialSearchResults() {
		if (tableExTrialSearchResults == null) {
			tableExTrialSearchResults = (HtmlDataTableEx) findComponentInRoot("tableExTrialSearchResults");
		}
		return tableExTrialSearchResults;
	}

	protected HtmlOutputText getTextTrialUserEmailAddress() {
		if (textTrialUserEmailAddress == null) {
			textTrialUserEmailAddress = (HtmlOutputText) findComponentInRoot("textTrialUserEmailAddress");
		}
		return textTrialUserEmailAddress;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlDataTableEx getTableExSampleInnerTable() {
		if (tableExSampleInnerTable == null) {
			tableExSampleInnerTable = (HtmlDataTableEx) findComponentInRoot("tableExSampleInnerTable");
		}
		return tableExSampleInnerTable;
	}

	protected UIColumnEx getColumnEx10() {
		if (columnEx10 == null) {
			columnEx10 = (UIColumnEx) findComponentInRoot("columnEx10");
		}
		return columnEx10;
	}

	protected HtmlOutputText getText21() {
		if (text21 == null) {
			text21 = (HtmlOutputText) findComponentInRoot("text21");
		}
		return text21;
	}

	protected UIColumnEx getColumnEx11() {
		if (columnEx11 == null) {
			columnEx11 = (UIColumnEx) findComponentInRoot("columnEx11");
		}
		return columnEx11;
	}

	protected HtmlOutputText getText22() {
		if (text22 == null) {
			text22 = (HtmlOutputText) findComponentInRoot("text22");
		}
		return text22;
	}

	protected UIColumnEx getColumnEx13() {
		if (columnEx13 == null) {
			columnEx13 = (UIColumnEx) findComponentInRoot("columnEx13");
		}
		return columnEx13;
	}

	protected UIColumnEx getColumnEx14() {
		if (columnEx14 == null) {
			columnEx14 = (UIColumnEx) findComponentInRoot("columnEx14");
		}
		return columnEx14;
	}

	protected HtmlOutputText getText29() {
		if (text29 == null) {
			text29 = (HtmlOutputText) findComponentInRoot("text29");
		}
		return text29;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlCommandExButton getButtonResetTrialPassword() {
		if (buttonResetTrialPassword == null) {
			buttonResetTrialPassword = (HtmlCommandExButton) findComponentInRoot("buttonResetTrialPassword");
		}
		return buttonResetTrialPassword;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	public String doButtonResetTrialPasswordAction() {
		String responseString = "success";
		TrialCustomerHandler selectedRecord = null;
		try {

			selectedRecord = (TrialCustomerHandler)this.getTableExTrialSearchResults().getRowData();
		    
			selectedRecord.getTrialCustomer().setPassword("password");
			
			selectedRecord.getTrialCustomer().save();

	    	FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password for trial customer:  " + selectedRecord.getTrialCustomer().getEmailAddress() + " reset to 'password'", null);
            this.getMessages1().setInfoStyle("color: blue;");
            context.addMessage(null, message);
		}
		catch (Exception e) {
			e.printStackTrace();
			
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to Reset Password. " + e.getMessage(), null);
            this.getMessages1().setInfoStyle("color: red;");
            context.addMessage(null, message);
			
			responseString = "failure";
		}
		return responseString;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlPanelGrid getGridTrialDTHeaderGrid() {
		if (gridTrialDTHeaderGrid == null) {
			gridTrialDTHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridTrialDTHeaderGrid");
		}
		return gridTrialDTHeaderGrid;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputText getText19() {
		if (text19 == null) {
			text19 = (HtmlOutputText) findComponentInRoot("text19");
		}
		return text19;
	}

	protected HtmlOutputText getText24() {
		if (text24 == null) {
			text24 = (HtmlOutputText) findComponentInRoot("text24");
		}
		return text24;
	}

	protected HtmlPanelBox getBox2() {
		if (box2 == null) {
			box2 = (HtmlPanelBox) findComponentInRoot("box2");
		}
		return box2;
	}

	protected HtmlPanelGrid getGridEmailCustomerHeaderGrid() {
		if (gridEmailCustomerHeaderGrid == null) {
			gridEmailCustomerHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridEmailCustomerHeaderGrid");
		}
		return gridEmailCustomerHeaderGrid;
	}

	protected HtmlOutputText getText25() {
		if (text25 == null) {
			text25 = (HtmlOutputText) findComponentInRoot("text25");
		}
		return text25;
	}

	protected HtmlOutputText getText30() {
		if (text30 == null) {
			text30 = (HtmlOutputText) findComponentInRoot("text30");
		}
		return text30;
	}

	protected HtmlOutputLinkEx getLinkEx2() {
		if (linkEx2 == null) {
			linkEx2 = (HtmlOutputLinkEx) findComponentInRoot("linkEx2");
		}
		return linkEx2;
	}

	protected HtmlCommandExRowEdit getRowEditEmailAddress() {
		if (rowEditEmailAddress == null) {
			rowEditEmailAddress = (HtmlCommandExRowEdit) findComponentInRoot("rowEditEmailAddress");
		}
		return rowEditEmailAddress;
	}

	protected HtmlJspPanel getJspPanel7() {
		if (jspPanel7 == null) {
			jspPanel7 = (HtmlJspPanel) findComponentInRoot("jspPanel7");
		}
		return jspPanel7;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlPanelFormBox getFormBoxEditEmailAddress() {
		if (formBoxEditEmailAddress == null) {
			formBoxEditEmailAddress = (HtmlPanelFormBox) findComponentInRoot("formBoxEditEmailAddress");
		}
		return formBoxEditEmailAddress;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlInputText getText31NewEmail() {
		if (text31NewEmail == null) {
			text31NewEmail = (HtmlInputText) findComponentInRoot("text31NewEmail");
		}
		return text31NewEmail;
	}

	public String doRowEditEmailAddressAction() {
		// Type Java code that runs when the component is clicked
		EmailRecordHandler record = (EmailRecordHandler)this.getTableExEmailSearchResults().getRowData();
		
		if (record != null) {
			try {
				EmailRecordBO recBO = (EmailRecordBO)record.getEmailRecord();
				
				if (ContactBO.validateContactEmailAddress(recBO.getEmailAddress())){
					recBO.save();					

					record.setMessage("Email address changed and saved.");
					FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email changed. If multiple emails exist for the new email address, you should 'Reset Password'.", null);
		            this.getMessages1().setInfoStyle("color: blue;");
		            context.addMessage(null, message);		
				}
				else {
					FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Email Address Entered. Record NOT Saved.", null);
		            this.getMessages1().setInfoStyle("color: red;");
		            context.addMessage(null, message);							
				}
			}
			catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to change email address. Possibly invalid email address: " + e.getMessage(), null);
	            this.getMessages1().setInfoStyle("color: red;");
	            context.addMessage(null, message);				
			}
		}
		
		return "success";
	}

	protected HtmlPanelGrid getGridtwoColumn11() {
		if (gridtwoColumn11 == null) {
			gridtwoColumn11 = (HtmlPanelGrid) findComponentInRoot("gridtwoColumn11");
		}
		return gridtwoColumn11;
	}

	protected HtmlCommandExButton getButtonSubmitSearch() {
		if (buttonSubmitSearch == null) {
			buttonSubmitSearch = (HtmlCommandExButton) findComponentInRoot("buttonSubmitSearch");
		}
		return buttonSubmitSearch;
	}

	protected HtmlCommandExButton getButtonClearSearchResults() {
		if (buttonClearSearchResults == null) {
			buttonClearSearchResults = (HtmlCommandExButton) findComponentInRoot("buttonClearSearchResults");
		}
		return buttonClearSearchResults;
	}

	public String doButtonClearSearchResultsAction() {
		this.getEmailSearchHandler().reset();
		return "success";
	}

	protected HtmlOutputText getText31() {
		if (text31 == null) {
			text31 = (HtmlOutputText) findComponentInRoot("text31");
		}
		return text31;
	}

	protected HtmlOutputText getText32() {
		if (text32 == null) {
			text32 = (HtmlOutputText) findComponentInRoot("text32");
		}
		return text32;
	}

	protected HtmlPanelGrid getGridPartnernameurlGrid1() {
		if (gridPartnernameurlGrid1 == null) {
			gridPartnernameurlGrid1 = (HtmlPanelGrid) findComponentInRoot("gridPartnernameurlGrid1");
		}
		return gridPartnernameurlGrid1;
	}

	protected HtmlOutputText getText20() {
		if (text20 == null) {
			text20 = (HtmlOutputText) findComponentInRoot("text20");
		}
		return text20;
	}

	protected HtmlOutputText getText33() {
		if (text33 == null) {
			text33 = (HtmlOutputText) findComponentInRoot("text33");
		}
		return text33;
	}

	protected HtmlOutputLinkEx getLinkEx3() {
		if (linkEx3 == null) {
			linkEx3 = (HtmlOutputLinkEx) findComponentInRoot("linkEx3");
		}
		return linkEx3;
	}

	protected HtmlOutputText getText34() {
		if (text34 == null) {
			text34 = (HtmlOutputText) findComponentInRoot("text34");
		}
		return text34;
	}

	protected UIColumnEx getColumnEx12() {
		if (columnEx12 == null) {
			columnEx12 = (UIColumnEx) findComponentInRoot("columnEx12");
		}
		return columnEx12;
	}

	protected HtmlOutputText getText35() {
		if (text35 == null) {
			text35 = (HtmlOutputText) findComponentInRoot("text35");
		}
		return text35;
	}

	protected UIColumnEx getColumnEx15() {
		if (columnEx15 == null) {
			columnEx15 = (UIColumnEx) findComponentInRoot("columnEx15");
		}
		return columnEx15;
	}

	protected HtmlCommandExButton getButtonSendConfirmationEmail() {
		if (buttonSendConfirmationEmail == null) {
			buttonSendConfirmationEmail = (HtmlCommandExButton) findComponentInRoot("buttonSendConfirmationEmail");
		}
		return buttonSendConfirmationEmail;
	}

	public String doButtonSendConfirmationEmailAction() {
		
		EmailRecordHandler record = (EmailRecordHandler)this.getTableExEmailSearchResults().getRowData();
		
		try {
			if (record != null) {
				
		        record.sendForgotPasswordEmail();
		        
		        FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Confirmation email Sent to: " + record.getEmailRecord().getEmailAddress(), null);
	            this.getMessages1().setInfoStyle("color: blue;");
	            context.addMessage(null, message);							
			}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to send confirmation email: Record was null." , null);
	            this.getMessages1().setInfoStyle("color: red;");
	            context.addMessage(null, message);							
			}
		}
		catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to send confirmation email: " + e.getMessage(), null);
            this.getMessages1().setInfoStyle("color: red;");
            context.addMessage(null, message);							
		}
		return "success";
	}

}