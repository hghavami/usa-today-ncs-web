/**
 * 
 */
package pagecode.tilesContent.restricted.accountMaintenance;

import java.util.Collection;

import javax.faces.application.FacesMessage;
import javax.faces.component.UINamingContainer;
import javax.faces.component.UIParameter;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;

import pagecode.PageCodeBase;

import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlCommandExRowEdit;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlFormItem;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlOutputSelecticons;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.businessObjects.customer.ContactBO;
import com.usatoday.businessObjects.customer.EmailRecordBO;
import com.usatoday.ncs.web.subscription.handlers.EmailRecordHandler;
import com.usatoday.ncs.web.subscription.handlers.EmailSearchHandler;

/**
 * @author aeast
 *
 */
public class MultipleEmailUpdate_mainBodyArea extends PageCodeBase {

	protected UINamingContainer viewFragmentMainBodyArea1;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlOutputText textHeaderText;
	protected HtmlMessages messagesMainBody1;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text1;
	protected HtmlDataTableEx tableExLikeEmailRecords;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text3;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected HtmlInputRowSelect rowSelect1;
	protected UIColumnEx columnEx5;
	protected HtmlPanelBox box1;
	protected HtmlOutputSelecticons selecticons2;
	protected HtmlPanelBox box2;
	protected HtmlCommandExButton buttonResetSelectedRecords;
	protected EmailSearchHandler emailSearchHandler;
	protected HtmlOutputText text9;
	protected UIColumnEx columnEx6;
	protected HtmlOutputText text10;
	protected HtmlOutputText text11;
	protected UIColumnEx columnEx7;
	protected HtmlOutputText text12;
	protected UIColumnEx columnEx8;
	protected HtmlCommandExRowEdit rowEditEmailAddress;
	protected HtmlJspPanel jspPanel1;
	protected HtmlPanelFormBox formBoxInPlaceEditor;
	protected HtmlFormItem formItemEmailAddress;
	protected HtmlInputText text13NewEmail;
	protected HtmlOutputFormat format1;
	protected UIParameter param1;
	protected UIParameter param2;
	protected HtmlOutputText text313;
	protected HtmlOutputText text13;
	protected UIColumnEx columnEx9;
	protected HtmlOutputText text14;
	protected HtmlCommandExButton buttonSendConfirmationEmail;
	protected UINamingContainer getViewFragmentMainBodyArea1() {
		if (viewFragmentMainBodyArea1 == null) {
			viewFragmentMainBodyArea1 = (UINamingContainer) findComponentInRoot("viewFragmentMainBodyArea1");
		}
		return viewFragmentMainBodyArea1;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlOutputText getTextHeaderText() {
		if (textHeaderText == null) {
			textHeaderText = (HtmlOutputText) findComponentInRoot("textHeaderText");
		}
		return textHeaderText;
	}

	protected HtmlMessages getMessagesMainBody1() {
		if (messagesMainBody1 == null) {
			messagesMainBody1 = (HtmlMessages) findComponentInRoot("messagesMainBody1");
		}
		return messagesMainBody1;
	}

	public String doButtonUpdateAllRecordsAction() {
		
		//      Iterate over the selection
		EmailSearchHandler results = this.getEmailSearchHandler();
		
		try {
			Collection<EmailRecordHandler> emailsToReset = results.getSameEmailAddressRecords();
			
			for (EmailRecordHandler record : emailsToReset) {
				if (record.isSelectedInTable()) {
					try {
						EmailRecordBO emailRec = (EmailRecordBO)record.getEmailRecord();
						
						emailRec.setPassword("password");
						emailRec.save();
						record.setMessage("Password Reset");
					}
					catch (Exception e) {
						record.setMessage("Password Reset Failed: "  + e.getMessage());
					}
				}
			}
	    	FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Password reset  to 'password' on selected records.", null);
            this.getMessagesMainBody1().setInfoStyle("color: blue;");
            context.addMessage(null, message);
			
		}
		catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to Reset Password. " + e.getMessage(), null);
            context.addMessage(null, message);
		}
		
		return "success";
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlDataTableEx getTableExLikeEmailRecords() {
		if (tableExLikeEmailRecords == null) {
			tableExLikeEmailRecords = (HtmlDataTableEx) findComponentInRoot("tableExLikeEmailRecords");
		}
		return tableExLikeEmailRecords;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx4() {
		if (columnEx4 == null) {
			columnEx4 = (UIColumnEx) findComponentInRoot("columnEx4");
		}
		return columnEx4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlInputRowSelect getRowSelect1() {
		if (rowSelect1 == null) {
			rowSelect1 = (HtmlInputRowSelect) findComponentInRoot("rowSelect1");
		}
		return rowSelect1;
	}

	protected UIColumnEx getColumnEx5() {
		if (columnEx5 == null) {
			columnEx5 = (UIColumnEx) findComponentInRoot("columnEx5");
		}
		return columnEx5;
	}

	protected HtmlPanelBox getBox1() {
		if (box1 == null) {
			box1 = (HtmlPanelBox) findComponentInRoot("box1");
		}
		return box1;
	}

	protected HtmlOutputSelecticons getSelecticons2() {
		if (selecticons2 == null) {
			selecticons2 = (HtmlOutputSelecticons) findComponentInRoot("selecticons2");
		}
		return selecticons2;
	}

	

	protected HtmlPanelBox getBox2() {
		if (box2 == null) {
			box2 = (HtmlPanelBox) findComponentInRoot("box2");
		}
		return box2;
	}

	protected HtmlCommandExButton getButtonResetSelectedRecords() {
		if (buttonResetSelectedRecords == null) {
			buttonResetSelectedRecords = (HtmlCommandExButton) findComponentInRoot("buttonResetSelectedRecords");
		}
		return buttonResetSelectedRecords;
	}

	/** 
	 * @managed-bean true
	 */
	protected EmailSearchHandler getEmailSearchHandler() {
		if (emailSearchHandler == null) {
			emailSearchHandler = (EmailSearchHandler) getManagedBean("emailSearchHandler");
		}
		return emailSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEmailSearchHandler(EmailSearchHandler emailSearchHandler) {
		this.emailSearchHandler = emailSearchHandler;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected UIColumnEx getColumnEx6() {
		if (columnEx6 == null) {
			columnEx6 = (UIColumnEx) findComponentInRoot("columnEx6");
		}
		return columnEx6;
	}

	protected HtmlOutputText getText10() {
		if (text10 == null) {
			text10 = (HtmlOutputText) findComponentInRoot("text10");
		}
		return text10;
	}


	protected HtmlOutputText getText11() {
		if (text11 == null) {
			text11 = (HtmlOutputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected UIColumnEx getColumnEx7() {
		if (columnEx7 == null) {
			columnEx7 = (UIColumnEx) findComponentInRoot("columnEx7");
		}
		return columnEx7;
	}

	protected HtmlOutputText getText12() {
		if (text12 == null) {
			text12 = (HtmlOutputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected UIColumnEx getColumnEx8() {
		if (columnEx8 == null) {
			columnEx8 = (UIColumnEx) findComponentInRoot("columnEx8");
		}
		return columnEx8;
	}

	protected HtmlCommandExRowEdit getRowEditEmailAddress() {
		if (rowEditEmailAddress == null) {
			rowEditEmailAddress = (HtmlCommandExRowEdit) findComponentInRoot("rowEditEmailAddress");
		}
		return rowEditEmailAddress;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlPanelFormBox getFormBoxInPlaceEditor() {
		if (formBoxInPlaceEditor == null) {
			formBoxInPlaceEditor = (HtmlPanelFormBox) findComponentInRoot("formBoxInPlaceEditor");
		}
		return formBoxInPlaceEditor;
	}

	protected HtmlFormItem getFormItemEmailAddress() {
		if (formItemEmailAddress == null) {
			formItemEmailAddress = (HtmlFormItem) findComponentInRoot("formItemEmailAddress");
		}
		return formItemEmailAddress;
	}

	protected HtmlInputText getText13NewEmail() {
		if (text13NewEmail == null) {
			text13NewEmail = (HtmlInputText) findComponentInRoot("text13NewEmail");
		}
		return text13NewEmail;
	}

	protected HtmlOutputFormat getFormat1() {
		if (format1 == null) {
			format1 = (HtmlOutputFormat) findComponentInRoot("format1");
		}
		return format1;
	}

	protected UIParameter getParam1() {
		if (param1 == null) {
			param1 = (UIParameter) findComponentInRoot("param1");
		}
		return param1;
	}

	public String doRowEditEmailAddressAction() {
		// Type Java code that runs when the component is clicked
		EmailRecordHandler record = (EmailRecordHandler)this.getTableExLikeEmailRecords().getRowData();
		
		if (record != null) {
			try {
				EmailRecordBO recBO = (EmailRecordBO)record.getEmailRecord();
				
				if (ContactBO.validateContactEmailAddress(recBO.getEmailAddress())){
					recBO.save();					

					record.setMessage("Email address changed and saved.");
					FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Email changed.", null);
		            this.getMessagesMainBody1().setInfoStyle("color: blue;");
		            context.addMessage(null, message);		
				}
				else {
					FacesContext context = FacesContext.getCurrentInstance();
		            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid Email Address Entered. Record NOT Saved.", null);
		            this.getMessagesMainBody1().setInfoStyle("color: red;");
		            context.addMessage(null, message);							
				}
			}
			catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to change email address. Possibly invalid email address: " + e.getMessage(), null);
	            this.getMessagesMainBody1().setInfoStyle("color: red;");
	            context.addMessage(null, message);				
			}
		}
		
		return "success";
	}

	protected UIParameter getParam2() {
		if (param2 == null) {
			param2 = (UIParameter) findComponentInRoot("param2");
		}
		return param2;
	}

	protected HtmlOutputText getText313() {
		if (text313 == null) {
			text313 = (HtmlOutputText) findComponentInRoot("text313");
		}
		return text313;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected UIColumnEx getColumnEx9() {
		if (columnEx9 == null) {
			columnEx9 = (UIColumnEx) findComponentInRoot("columnEx9");
		}
		return columnEx9;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlCommandExButton getButtonSendConfirmationEmail() {
		if (buttonSendConfirmationEmail == null) {
			buttonSendConfirmationEmail = (HtmlCommandExButton) findComponentInRoot("buttonSendConfirmationEmail");
		}
		return buttonSendConfirmationEmail;
	}

	public String doButtonSendConfirmationEmailAction() {
		EmailRecordHandler record = (EmailRecordHandler)this.getTableExLikeEmailRecords().getRowData();
		
		try {
			if (record != null) {
				
		        record.sendForgotPasswordEmail();
		        
		        FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Confirmation email Sent to: " + record.getEmailRecord().getEmailAddress(), null);
	            this.getMessagesMainBody1().setInfoStyle("color: blue;");
	            context.addMessage(null, message);							
			}
			else {
				FacesContext context = FacesContext.getCurrentInstance();
	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to send confirmation email: Record was null." , null);
	            this.getMessagesMainBody1().setInfoStyle("color: red;");
	            context.addMessage(null, message);							
			}
		}
		catch (Exception e) {
			FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed to send confirmation email: " + e.getMessage(), null);
            this.getMessagesMainBody1().setInfoStyle("color: red;");
            context.addMessage(null, message);							
		}
		return "success";
	}

}