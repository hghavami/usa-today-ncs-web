/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class Accessdenied_headarea extends PageCodeBase {

	protected UINamingContainer headerAreaFragment;

	protected UINamingContainer getHeaderAreaFragment() {
		if (headerAreaFragment == null) {
			headerAreaFragment = (UINamingContainer) findComponentInRoot("headerAreaFragment");
		}
		return headerAreaFragment;
	}

}