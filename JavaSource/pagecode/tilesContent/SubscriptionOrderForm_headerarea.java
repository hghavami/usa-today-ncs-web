/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class SubscriptionOrderForm_headerarea extends PageCodeBase {

	protected UINamingContainer viewFragmentHeaderFrag1;

	protected UINamingContainer getViewFragmentHeaderFrag1() {
		if (viewFragmentHeaderFrag1 == null) {
			viewFragmentHeaderFrag1 = (UINamingContainer) findComponentInRoot("viewFragmentHeaderFrag1");
		}
		return viewFragmentHeaderFrag1;
	}

}