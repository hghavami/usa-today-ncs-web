/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class AccountMaintHome_headarea extends PageCodeBase {

	protected UINamingContainer viewFragmentHeadFragment;

	protected UINamingContainer getViewFragmentHeadFragment() {
		if (viewFragmentHeadFragment == null) {
			viewFragmentHeadFragment = (UINamingContainer) findComponentInRoot("viewFragmentHeadFragment");
		}
		return viewFragmentHeadFragment;
	}

}