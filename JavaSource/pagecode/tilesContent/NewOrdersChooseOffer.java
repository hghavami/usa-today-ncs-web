/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.ncs.web.subscription.handlers.OfferLocatorTypeAhead;
import com.usatoday.ncs.web.subscription.handlers.ProductHandler;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferLocatorHandler;
import com.usatoday.ncs.web.subscription.handlers.ProductsHandler;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionTermHandler;
import com.usatoday.util.constants.UsaTodayConstants;

import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.component.html.HtmlMessages;
import com.ibm.faces.component.html.HtmlInputHelperTypeahead;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlBehavior;
import com.ibm.faces.component.html.HtmlPanelDialog;
import com.ibm.faces.component.html.HtmlAjaxRefreshSubmit;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.UIParameter;
import com.ibm.faces.component.UIColumnEx;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.html.HtmlInputRowSelect;
import com.usatoday.ncs.web.subscription.handlers.NewSubscriptionOrderHandler;
import com.ibm.faces.component.html.HtmlPanelBox;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferHandler;
import com.ibm.faces.component.html.HtmlPanelFormBox;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlInputHelperDatePicker;
import com.ibm.faces.component.html.HtmlFormItem;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.UISelectItems;
import com.ibm.faces.component.html.HtmlOutputLinkEx;

/**
 * @author aeast
 *
 */
public class NewOrdersChooseOffer extends PageCodeBase {

	protected UINamingContainer viewFragmentChooseOffer;
	protected HtmlScriptCollector scriptCollectorProductOffer;
	protected HtmlForm formOfferSelcection;
	protected SubscriptionOfferLocatorHandler offerSearchHandler;
	protected ProductsHandler subscriptionProductsHandler;
	protected HtmlPanelGrid gridOfferBodyGrid;
	protected HtmlPanelGroup groupOfferHeaderGroup1;
	protected HtmlMessages messagesOfferSelectionMsgs1;
	protected HtmlPanelGrid gridChosenProductGrid1;
	protected HtmlOutputText textProductNameLabel;
	protected HtmlOutputText textProductName;
	protected HtmlOutputText textKeycodeOfferCodeLabel;
	protected HtmlInputHelperTypeahead typeahead1;
	protected HtmlInputText textKeycode;
	protected HtmlCommandExButton buttonShowOfferDetails;
	protected HtmlBehavior behavior1;
	protected HtmlPanelGrid gridTermSelectionV2;
	protected HtmlPanelGroup gridTermSelectionHeaderGroupV2;
	protected HtmlOutputFormat formatCurrentOfferDesFormat;
	protected UIParameter paramTermLabelParm1;
	protected UIParameter paramTermLabelParm2;
	protected UIParameter paramTermLabelParm3;
	protected UIColumnEx columnExTermCol1;
	protected HtmlOutputText text1;
	protected HtmlDataTableEx tableExOfferDetailsDataTable;
	protected HtmlOutputText text2;
	protected UIColumnEx columnExPriceCol2;
	protected HtmlOutputText text3;
	protected UIColumnEx columnExIssuePriceCol3;
	protected HtmlOutputText text4;
	protected UIColumnEx columnExRenewalOption4;
	protected HtmlOutputText text5;
	protected HtmlOutputText text6;
	protected HtmlOutputText text7;
	protected HtmlOutputText text8;
	protected UIColumnEx columnExReqEZPay5;
	protected HtmlOutputText text9;
	protected HtmlAjaxRefreshSubmit ajaxRefreshSubmit2;
	protected HtmlPanelGroup group1;
	protected HtmlCommandExButton button3;
	protected HtmlBehavior behavior3;
	protected HtmlBehavior behavior2;
	protected HtmlPanelDialog dialogEZPayInformation;
	protected HtmlCommandExButton button2;
	protected HtmlPanelGrid gridEZPayInfoGrid2;
	protected HtmlOutputText textEZPay;
	protected HtmlInputRowSelect rowSelectTermSelection1;
	protected UIColumnEx columnEx1;
	protected NewSubscriptionOrderHandler newSubscriptionOrderHandler;
	protected HtmlPanelBox boxTermsFooterBox;
	protected SubscriptionOfferHandler currentOfferHandler;
	protected HtmlPanelBox boxOfferTermsHeaderDTBox1;
	protected HtmlPanelGrid gridfferTermsHeaderDTGrid1;
	protected HtmlOutputText textOfferPromoText1;
	protected HtmlPanelGrid gridOfferFoooterGrid1;
	protected HtmlOutputText textOfferDisclaimerText;
	protected HtmlOutputFormat formatProductDefaultKeycode1;
	protected UIParameter paramDefaultKeycodeParm4;
	protected HtmlPanelFormBox formBoxStartDateInformation;
	protected HtmlPanelGrid gridDatePickerHeaderGrid;
	protected HtmlPanelSection sectionStartDateSectionHeaderInfo;
	protected HtmlJspPanel jspPanel2;
	protected HtmlOutputText text3Closed;
	protected HtmlJspPanel jspPanel1;
	protected HtmlOutputText text2OpenedHeader;
	protected HtmlInputHelperDatePicker datePicker1;
	protected HtmlPanelGrid gridStartDateInnerGrid;
	protected HtmlFormItem formItemStartDateFormItem;
	protected HtmlInputText textStartDateInput;
	protected HtmlPanelGrid gridStartDateInformation;
	protected HtmlPanelFormBox formBoxOrderQtyFormBox;
	protected HtmlFormItem formItemQty2;
	protected HtmlSelectOneMenu menuOrderQuantity;
	protected UISelectItems selectItemsQty2;
	protected HtmlPanelGrid gridQuantityInformation;
	protected HtmlPanelFormBox formBoxPartnerInformation;
	protected HtmlFormItem formItemPartnerClubNumber;
	protected HtmlInputText textClubNumber;
	protected HtmlPanelGrid gridPartnerInformation;
	protected HtmlPanelGroup groupOfferFooterGroup2;
	protected HtmlPanelGrid gridCheckoutGrid1;
	protected HtmlCommandExButton buttonPlaceOrder;
	protected UIParameter paramStartDateInfoParm1;
	protected HtmlOutputText text10EZPayInfo;
	protected HtmlOutputLinkEx linkExEZPayLearnMoreLink1;
	protected UINamingContainer getViewFragmentChooseOffer() {
		if (viewFragmentChooseOffer == null) {
			viewFragmentChooseOffer = (UINamingContainer) findComponentInRoot("viewFragmentChooseOffer");
		}
		return viewFragmentChooseOffer;
	}

	protected HtmlScriptCollector getScriptCollectorProductOffer() {
		if (scriptCollectorProductOffer == null) {
			scriptCollectorProductOffer = (HtmlScriptCollector) findComponentInRoot("scriptCollectorProductOffer");
		}
		return scriptCollectorProductOffer;
	}

	protected HtmlForm getFormOfferSelcection() {
		if (formOfferSelcection == null) {
			formOfferSelcection = (HtmlForm) findComponentInRoot("formOfferSelcection");
		}
		return formOfferSelcection;
	}

	public void onPageLoadBegin(FacesContext facescontext) {
		// Type Java code to handle page load begin event here
	
		// void <method>(FacesContext facescontext)
		try {

			boolean initialRequest = false;
			Object initRequest = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap().get("initialRequest");
			if (initRequest != null && initRequest instanceof String[]) {
				String[] values = (String[])initRequest;
				if (values.length > 0) {
					if (values[0].equalsIgnoreCase("Y")) {
						initialRequest = true;
					}
				}
			}
			
			if (initialRequest) {
				this.getNewSubscriptionOrderHandler().resetForNewOrder();
				
				Object pObject = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap().get("pubCode");

				String pubCode = UsaTodayConstants.UT_PUBCODE;

				if (pObject != null && pObject instanceof String[]) {
					String[] values = (String[])pObject;
					if (values.length > 0) {
						pubCode = ((String[])pObject)[0];
					}
				}
				
				SubscriptionOfferLocatorHandler offerLocator = this.getOfferSearchHandler();

				offerLocator.setTypeAhead(new OfferLocatorTypeAhead());
				offerLocator.setPubCode(pubCode);
				
				ProductHandler ph = this.getSubscriptionProductsHandler().getProductForPub(pubCode);
				offerLocator.setProductHandler(ph);
				
				SubscriptionOfferIntf defaultOffer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubCode);
				SubscriptionOfferHandler soh = new SubscriptionOfferHandler();
				soh.setCurrentOffer(defaultOffer);
				offerLocator.setCurrentOfferHandler(soh);
				offerLocator.setKeycode(ph.getProduct().getDefaultKeycode());
				
				if (ph.getProduct() instanceof SubscriptionProductIntf) {
					SubscriptionProductIntf subProd = (SubscriptionProductIntf)ph.getProduct();
					this.getNewSubscriptionOrderHandler().setStartDate(subProd.getEarliestPossibleStartDate().toDate());
				}

			}
			
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferLocatorHandler getOfferSearchHandler() {
		if (offerSearchHandler == null) {
			offerSearchHandler = (SubscriptionOfferLocatorHandler) getManagedBean("offerSearchHandler");
		}
		return offerSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setOfferSearchHandler(
			SubscriptionOfferLocatorHandler offerSearchHandler) {
		this.offerSearchHandler = offerSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getSubscriptionProductsHandler() {
		if (subscriptionProductsHandler == null) {
			subscriptionProductsHandler = (ProductsHandler) getManagedBean("subscriptionProductsHandler");
		}
		return subscriptionProductsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSubscriptionProductsHandler(
			ProductsHandler subscriptionProductsHandler) {
		this.subscriptionProductsHandler = subscriptionProductsHandler;
	}

	protected HtmlPanelGrid getGridOfferBodyGrid() {
		if (gridOfferBodyGrid == null) {
			gridOfferBodyGrid = (HtmlPanelGrid) findComponentInRoot("gridOfferBodyGrid");
		}
		return gridOfferBodyGrid;
	}

	protected HtmlPanelGroup getGroupOfferHeaderGroup1() {
		if (groupOfferHeaderGroup1 == null) {
			groupOfferHeaderGroup1 = (HtmlPanelGroup) findComponentInRoot("groupOfferHeaderGroup1");
		}
		return groupOfferHeaderGroup1;
	}

	protected HtmlMessages getMessagesOfferSelectionMsgs1() {
		if (messagesOfferSelectionMsgs1 == null) {
			messagesOfferSelectionMsgs1 = (HtmlMessages) findComponentInRoot("messagesOfferSelectionMsgs1");
		}
		return messagesOfferSelectionMsgs1;
	}

	protected HtmlPanelGrid getGridChosenProductGrid1() {
		if (gridChosenProductGrid1 == null) {
			gridChosenProductGrid1 = (HtmlPanelGrid) findComponentInRoot("gridChosenProductGrid1");
		}
		return gridChosenProductGrid1;
	}

	protected HtmlOutputText getTextProductNameLabel() {
		if (textProductNameLabel == null) {
			textProductNameLabel = (HtmlOutputText) findComponentInRoot("textProductNameLabel");
		}
		return textProductNameLabel;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlOutputText getTextKeycodeOfferCodeLabel() {
		if (textKeycodeOfferCodeLabel == null) {
			textKeycodeOfferCodeLabel = (HtmlOutputText) findComponentInRoot("textKeycodeOfferCodeLabel");
		}
		return textKeycodeOfferCodeLabel;
	}

	protected HtmlInputHelperTypeahead getTypeahead1() {
		if (typeahead1 == null) {
			typeahead1 = (HtmlInputHelperTypeahead) findComponentInRoot("typeahead1");
		}
		return typeahead1;
	}

	protected HtmlInputText getTextKeycode() {
		if (textKeycode == null) {
			textKeycode = (HtmlInputText) findComponentInRoot("textKeycode");
		}
		return textKeycode;
	}

	protected HtmlCommandExButton getButtonShowOfferDetails() {
		if (buttonShowOfferDetails == null) {
			buttonShowOfferDetails = (HtmlCommandExButton) findComponentInRoot("buttonShowOfferDetails");
		}
		return buttonShowOfferDetails;
	}

	protected HtmlBehavior getBehavior1() {
		if (behavior1 == null) {
			behavior1 = (HtmlBehavior) findComponentInRoot("behavior1");
		}
		return behavior1;
	}

	protected HtmlPanelGrid getGridTermSelectionV2() {
		if (gridTermSelectionV2 == null) {
			gridTermSelectionV2 = (HtmlPanelGrid) findComponentInRoot("gridTermSelectionV2");
		}
		return gridTermSelectionV2;
	}

	protected HtmlPanelGroup getGridTermSelectionHeaderGroupV2() {
		if (gridTermSelectionHeaderGroupV2 == null) {
			gridTermSelectionHeaderGroupV2 = (HtmlPanelGroup) findComponentInRoot("gridTermSelectionHeaderGroupV2");
		}
		return gridTermSelectionHeaderGroupV2;
	}

	protected HtmlOutputFormat getFormatCurrentOfferDesFormat() {
		if (formatCurrentOfferDesFormat == null) {
			formatCurrentOfferDesFormat = (HtmlOutputFormat) findComponentInRoot("formatCurrentOfferDesFormat");
		}
		return formatCurrentOfferDesFormat;
	}

	protected UIParameter getParamTermLabelParm1() {
		if (paramTermLabelParm1 == null) {
			paramTermLabelParm1 = (UIParameter) findComponentInRoot("paramTermLabelParm1");
		}
		return paramTermLabelParm1;
	}

	protected UIParameter getParamTermLabelParm2() {
		if (paramTermLabelParm2 == null) {
			paramTermLabelParm2 = (UIParameter) findComponentInRoot("paramTermLabelParm2");
		}
		return paramTermLabelParm2;
	}

	protected UIParameter getParamTermLabelParm3() {
		if (paramTermLabelParm3 == null) {
			paramTermLabelParm3 = (UIParameter) findComponentInRoot("paramTermLabelParm3");
		}
		return paramTermLabelParm3;
	}

	protected UIColumnEx getColumnExTermCol1() {
		if (columnExTermCol1 == null) {
			columnExTermCol1 = (UIColumnEx) findComponentInRoot("columnExTermCol1");
		}
		return columnExTermCol1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlDataTableEx getTableExOfferDetailsDataTable() {
		if (tableExOfferDetailsDataTable == null) {
			tableExOfferDetailsDataTable = (HtmlDataTableEx) findComponentInRoot("tableExOfferDetailsDataTable");
		}
		return tableExOfferDetailsDataTable;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnExPriceCol2() {
		if (columnExPriceCol2 == null) {
			columnExPriceCol2 = (UIColumnEx) findComponentInRoot("columnExPriceCol2");
		}
		return columnExPriceCol2;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnExIssuePriceCol3() {
		if (columnExIssuePriceCol3 == null) {
			columnExIssuePriceCol3 = (UIColumnEx) findComponentInRoot("columnExIssuePriceCol3");
		}
		return columnExIssuePriceCol3;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnExRenewalOption4() {
		if (columnExRenewalOption4 == null) {
			columnExRenewalOption4 = (UIColumnEx) findComponentInRoot("columnExRenewalOption4");
		}
		return columnExRenewalOption4;
	}

	protected HtmlOutputText getText5() {
		if (text5 == null) {
			text5 = (HtmlOutputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlOutputText getText6() {
		if (text6 == null) {
			text6 = (HtmlOutputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlOutputText getText7() {
		if (text7 == null) {
			text7 = (HtmlOutputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected UIColumnEx getColumnExReqEZPay5() {
		if (columnExReqEZPay5 == null) {
			columnExReqEZPay5 = (UIColumnEx) findComponentInRoot("columnExReqEZPay5");
		}
		return columnExReqEZPay5;
	}

	protected HtmlOutputText getText9() {
		if (text9 == null) {
			text9 = (HtmlOutputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlAjaxRefreshSubmit getAjaxRefreshSubmit2() {
		if (ajaxRefreshSubmit2 == null) {
			ajaxRefreshSubmit2 = (HtmlAjaxRefreshSubmit) findComponentInRoot("ajaxRefreshSubmit2");
		}
		return ajaxRefreshSubmit2;
	}

	protected HtmlPanelGroup getGroup1() {
		if (group1 == null) {
			group1 = (HtmlPanelGroup) findComponentInRoot("group1");
		}
		return group1;
	}

	protected HtmlCommandExButton getButton3() {
		if (button3 == null) {
			button3 = (HtmlCommandExButton) findComponentInRoot("button3");
		}
		return button3;
	}

	protected HtmlBehavior getBehavior3() {
		if (behavior3 == null) {
			behavior3 = (HtmlBehavior) findComponentInRoot("behavior3");
		}
		return behavior3;
	}

	protected HtmlBehavior getBehavior2() {
		if (behavior2 == null) {
			behavior2 = (HtmlBehavior) findComponentInRoot("behavior2");
		}
		return behavior2;
	}

	protected HtmlPanelDialog getDialogEZPayInformation() {
		if (dialogEZPayInformation == null) {
			dialogEZPayInformation = (HtmlPanelDialog) findComponentInRoot("dialogEZPayInformation");
		}
		return dialogEZPayInformation;
	}

	protected HtmlCommandExButton getButton2() {
		if (button2 == null) {
			button2 = (HtmlCommandExButton) findComponentInRoot("button2");
		}
		return button2;
	}

	protected HtmlPanelGrid getGridEZPayInfoGrid2() {
		if (gridEZPayInfoGrid2 == null) {
			gridEZPayInfoGrid2 = (HtmlPanelGrid) findComponentInRoot("gridEZPayInfoGrid2");
		}
		return gridEZPayInfoGrid2;
	}

	protected HtmlOutputText getTextEZPay() {
		if (textEZPay == null) {
			textEZPay = (HtmlOutputText) findComponentInRoot("textEZPay");
		}
		return textEZPay;
	}

	protected HtmlInputRowSelect getRowSelectTermSelection1() {
		if (rowSelectTermSelection1 == null) {
			rowSelectTermSelection1 = (HtmlInputRowSelect) findComponentInRoot("rowSelectTermSelection1");
		}
		return rowSelectTermSelection1;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	/** 
	 * @managed-bean true
	 */
	protected NewSubscriptionOrderHandler getNewSubscriptionOrderHandler() {
		if (newSubscriptionOrderHandler == null) {
			newSubscriptionOrderHandler = (NewSubscriptionOrderHandler) getManagedBean("newSubscriptionOrderHandler");
		}
		return newSubscriptionOrderHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewSubscriptionOrderHandler(
			NewSubscriptionOrderHandler newSubscriptionOrderHandler) {
		this.newSubscriptionOrderHandler = newSubscriptionOrderHandler;
	}

	protected HtmlPanelBox getBoxTermsFooterBox() {
		if (boxTermsFooterBox == null) {
			boxTermsFooterBox = (HtmlPanelBox) findComponentInRoot("boxTermsFooterBox");
		}
		return boxTermsFooterBox;
	}

	public String doButtonPlaceOrderAction() {
		// Type Java code that runs when the component is clicked
	
		String responseString = "success";
		
		SubscriptionOfferLocatorHandler search = this.getOfferSearchHandler();
		SubscriptionTermHandler termHandler = null;
		
		for (SubscriptionTermHandler temp : search.getOfferTerms()) {
			if (temp.isSelected()) {
				termHandler = temp;
				break;
			}
		}
		
		if (termHandler == null || termHandler.getTerm() == null) {
			// TODO: add faces message error
			
		}
		
		NewSubscriptionOrderHandler newOrderHandler = this.getNewSubscriptionOrderHandler();
		
		// setup current offer
		SubscriptionOfferHandler offer = this.getCurrentOfferHandler();
		offer.setCurrentOffer(search.getCurrentOfferHandler().getCurrentOffer());
		
		// change to set the whole term into the object
		newOrderHandler.setSelectedTermHandler(termHandler);
		newOrderHandler.setSelectedTerm(termHandler.getTerm().getDurationInWeeks());
		
		return responseString;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferHandler getCurrentOfferHandler() {
		if (currentOfferHandler == null) {
			currentOfferHandler = (SubscriptionOfferHandler) getManagedBean("currentOfferHandler");
		}
		return currentOfferHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setCurrentOfferHandler(
			SubscriptionOfferHandler currentOfferHandler) {
		this.currentOfferHandler = currentOfferHandler;
	}

	protected HtmlPanelBox getBoxOfferTermsHeaderDTBox1() {
		if (boxOfferTermsHeaderDTBox1 == null) {
			boxOfferTermsHeaderDTBox1 = (HtmlPanelBox) findComponentInRoot("boxOfferTermsHeaderDTBox1");
		}
		return boxOfferTermsHeaderDTBox1;
	}

	protected HtmlPanelGrid getGridfferTermsHeaderDTGrid1() {
		if (gridfferTermsHeaderDTGrid1 == null) {
			gridfferTermsHeaderDTGrid1 = (HtmlPanelGrid) findComponentInRoot("gridfferTermsHeaderDTGrid1");
		}
		return gridfferTermsHeaderDTGrid1;
	}

	protected HtmlOutputText getTextOfferPromoText1() {
		if (textOfferPromoText1 == null) {
			textOfferPromoText1 = (HtmlOutputText) findComponentInRoot("textOfferPromoText1");
		}
		return textOfferPromoText1;
	}

	protected HtmlPanelGrid getGridOfferFoooterGrid1() {
		if (gridOfferFoooterGrid1 == null) {
			gridOfferFoooterGrid1 = (HtmlPanelGrid) findComponentInRoot("gridOfferFoooterGrid1");
		}
		return gridOfferFoooterGrid1;
	}

	protected HtmlOutputText getTextOfferDisclaimerText() {
		if (textOfferDisclaimerText == null) {
			textOfferDisclaimerText = (HtmlOutputText) findComponentInRoot("textOfferDisclaimerText");
		}
		return textOfferDisclaimerText;
	}

	protected HtmlOutputFormat getFormatProductDefaultKeycode1() {
		if (formatProductDefaultKeycode1 == null) {
			formatProductDefaultKeycode1 = (HtmlOutputFormat) findComponentInRoot("formatProductDefaultKeycode1");
		}
		return formatProductDefaultKeycode1;
	}

	protected UIParameter getParamDefaultKeycodeParm4() {
		if (paramDefaultKeycodeParm4 == null) {
			paramDefaultKeycodeParm4 = (UIParameter) findComponentInRoot("paramDefaultKeycodeParm4");
		}
		return paramDefaultKeycodeParm4;
	}

	protected HtmlPanelFormBox getFormBoxStartDateInformation() {
		if (formBoxStartDateInformation == null) {
			formBoxStartDateInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxStartDateInformation");
		}
		return formBoxStartDateInformation;
	}

	protected HtmlPanelGrid getGridDatePickerHeaderGrid() {
		if (gridDatePickerHeaderGrid == null) {
			gridDatePickerHeaderGrid = (HtmlPanelGrid) findComponentInRoot("gridDatePickerHeaderGrid");
		}
		return gridDatePickerHeaderGrid;
	}

	protected HtmlPanelSection getSectionStartDateSectionHeaderInfo() {
		if (sectionStartDateSectionHeaderInfo == null) {
			sectionStartDateSectionHeaderInfo = (HtmlPanelSection) findComponentInRoot("sectionStartDateSectionHeaderInfo");
		}
		return sectionStartDateSectionHeaderInfo;
	}

	protected HtmlJspPanel getJspPanel2() {
		if (jspPanel2 == null) {
			jspPanel2 = (HtmlJspPanel) findComponentInRoot("jspPanel2");
		}
		return jspPanel2;
	}

	protected HtmlOutputText getText3Closed() {
		if (text3Closed == null) {
			text3Closed = (HtmlOutputText) findComponentInRoot("text3Closed");
		}
		return text3Closed;
	}

	protected HtmlJspPanel getJspPanel1() {
		if (jspPanel1 == null) {
			jspPanel1 = (HtmlJspPanel) findComponentInRoot("jspPanel1");
		}
		return jspPanel1;
	}

	protected HtmlOutputText getText2OpenedHeader() {
		if (text2OpenedHeader == null) {
			text2OpenedHeader = (HtmlOutputText) findComponentInRoot("text2OpenedHeader");
		}
		return text2OpenedHeader;
	}

	protected HtmlInputHelperDatePicker getDatePicker1() {
		if (datePicker1 == null) {
			datePicker1 = (HtmlInputHelperDatePicker) findComponentInRoot("datePicker1");
		}
		return datePicker1;
	}

	protected HtmlPanelGrid getGridStartDateInnerGrid() {
		if (gridStartDateInnerGrid == null) {
			gridStartDateInnerGrid = (HtmlPanelGrid) findComponentInRoot("gridStartDateInnerGrid");
		}
		return gridStartDateInnerGrid;
	}

	protected HtmlFormItem getFormItemStartDateFormItem() {
		if (formItemStartDateFormItem == null) {
			formItemStartDateFormItem = (HtmlFormItem) findComponentInRoot("formItemStartDateFormItem");
		}
		return formItemStartDateFormItem;
	}

	protected HtmlInputText getTextStartDateInput() {
		if (textStartDateInput == null) {
			textStartDateInput = (HtmlInputText) findComponentInRoot("textStartDateInput");
		}
		return textStartDateInput;
	}

	protected HtmlPanelGrid getGridStartDateInformation() {
		if (gridStartDateInformation == null) {
			gridStartDateInformation = (HtmlPanelGrid) findComponentInRoot("gridStartDateInformation");
		}
		return gridStartDateInformation;
	}

	protected HtmlPanelFormBox getFormBoxOrderQtyFormBox() {
		if (formBoxOrderQtyFormBox == null) {
			formBoxOrderQtyFormBox = (HtmlPanelFormBox) findComponentInRoot("formBoxOrderQtyFormBox");
		}
		return formBoxOrderQtyFormBox;
	}

	protected HtmlFormItem getFormItemQty2() {
		if (formItemQty2 == null) {
			formItemQty2 = (HtmlFormItem) findComponentInRoot("formItemQty2");
		}
		return formItemQty2;
	}

	protected HtmlSelectOneMenu getMenuOrderQuantity() {
		if (menuOrderQuantity == null) {
			menuOrderQuantity = (HtmlSelectOneMenu) findComponentInRoot("menuOrderQuantity");
		}
		return menuOrderQuantity;
	}

	protected UISelectItems getSelectItemsQty2() {
		if (selectItemsQty2 == null) {
			selectItemsQty2 = (UISelectItems) findComponentInRoot("selectItemsQty2");
		}
		return selectItemsQty2;
	}

	protected HtmlPanelGrid getGridQuantityInformation() {
		if (gridQuantityInformation == null) {
			gridQuantityInformation = (HtmlPanelGrid) findComponentInRoot("gridQuantityInformation");
		}
		return gridQuantityInformation;
	}

	protected HtmlPanelFormBox getFormBoxPartnerInformation() {
		if (formBoxPartnerInformation == null) {
			formBoxPartnerInformation = (HtmlPanelFormBox) findComponentInRoot("formBoxPartnerInformation");
		}
		return formBoxPartnerInformation;
	}

	protected HtmlFormItem getFormItemPartnerClubNumber() {
		if (formItemPartnerClubNumber == null) {
			formItemPartnerClubNumber = (HtmlFormItem) findComponentInRoot("formItemPartnerClubNumber");
		}
		return formItemPartnerClubNumber;
	}

	protected HtmlInputText getTextClubNumber() {
		if (textClubNumber == null) {
			textClubNumber = (HtmlInputText) findComponentInRoot("textClubNumber");
		}
		return textClubNumber;
	}

	protected HtmlPanelGrid getGridPartnerInformation() {
		if (gridPartnerInformation == null) {
			gridPartnerInformation = (HtmlPanelGrid) findComponentInRoot("gridPartnerInformation");
		}
		return gridPartnerInformation;
	}

	protected HtmlPanelGroup getGroupOfferFooterGroup2() {
		if (groupOfferFooterGroup2 == null) {
			groupOfferFooterGroup2 = (HtmlPanelGroup) findComponentInRoot("groupOfferFooterGroup2");
		}
		return groupOfferFooterGroup2;
	}

	protected HtmlPanelGrid getGridCheckoutGrid1() {
		if (gridCheckoutGrid1 == null) {
			gridCheckoutGrid1 = (HtmlPanelGrid) findComponentInRoot("gridCheckoutGrid1");
		}
		return gridCheckoutGrid1;
	}

	protected HtmlCommandExButton getButtonPlaceOrder() {
		if (buttonPlaceOrder == null) {
			buttonPlaceOrder = (HtmlCommandExButton) findComponentInRoot("buttonPlaceOrder");
		}
		return buttonPlaceOrder;
	}

	protected UIParameter getParamStartDateInfoParm1() {
		if (paramStartDateInfoParm1 == null) {
			paramStartDateInfoParm1 = (UIParameter) findComponentInRoot("paramStartDateInfoParm1");
		}
		return paramStartDateInfoParm1;
	}

	protected HtmlOutputText getText10EZPayInfo() {
		if (text10EZPayInfo == null) {
			text10EZPayInfo = (HtmlOutputText) findComponentInRoot("text10EZPayInfo");
		}
		return text10EZPayInfo;
	}

	protected HtmlOutputLinkEx getLinkExEZPayLearnMoreLink1() {
		if (linkExEZPayLearnMoreLink1 == null) {
			linkExEZPayLearnMoreLink1 = (HtmlOutputLinkEx) findComponentInRoot("linkExEZPayLearnMoreLink1");
		}
		return linkExEZPayLearnMoreLink1;
	}

}