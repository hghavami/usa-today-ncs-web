/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class IncidentComplete_headarea extends PageCodeBase {

	protected UINamingContainer headerSubView;

	protected UINamingContainer getHeaderSubView() {
		if (headerSubView == null) {
			headerSubView = (UINamingContainer) findComponentInRoot("headerSubView");
		}
		return headerSubView;
	}

}