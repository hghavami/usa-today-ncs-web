/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class OnlineAccountAccess_headarea extends PageCodeBase {

	protected UINamingContainer headerFrag;

	protected UINamingContainer getHeaderFrag() {
		if (headerFrag == null) {
			headerFrag = (UINamingContainer) findComponentInRoot("headerFrag");
		}
		return headerFrag;
	}

}