/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class SubscriptionOrderForm_leftNavContentArea extends PageCodeBase {

	protected UINamingContainer viewFragmentLeftNav1;

	protected UINamingContainer getViewFragmentLeftNav1() {
		if (viewFragmentLeftNav1 == null) {
			viewFragmentLeftNav1 = (UINamingContainer) findComponentInRoot("viewFragmentLeftNav1");
		}
		return viewFragmentLeftNav1;
	}

}