/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlInputText;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import com.ibm.faces.component.html.HtmlCommandExButton;
import javax.faces.component.html.HtmlInputHidden;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.UISelectItem;
import javax.faces.component.UINamingContainer;

/**
 * @author aeast
 *
 */
public class CreateIncident_mainBodyarea extends PageCodeBase {

	protected HtmlInputTextarea textarea1;
	protected HtmlMessage message3;
	protected HtmlSelectBooleanCheckbox checkbox1;
	protected HtmlOutputText textRefundCheckboxInfo;
	protected HtmlInputText text1;
	protected HtmlMessage message2;
	protected HtmlInputText text2;
	protected HtmlInputText text3;
	protected HtmlSelectOneMenu menu2;
	protected HtmlInputText text4;
	protected HtmlOutputText text13;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlMessage message1;
	protected HtmlOutputText textCustomerHeaderLabel;
	protected HtmlSelectBooleanCheckbox checkbox2;
	protected HtmlInputText text5;
	protected HtmlInputText textCustLastName;
	protected HtmlInputText text6;
	protected HtmlInputText text7;
	protected HtmlInputText text8;
	protected HtmlInputText text9;
	protected HtmlInputText text10;
	protected HtmlInputText text11;
	protected HtmlSelectOneMenu menu3;
	protected HtmlInputText text12;
	protected HtmlCommandExButton buttonCreateIncident;
	protected HtmlCommandExButton buttonCancelNewIncident;
	protected HtmlInputHidden uniqueRequestValue;
	protected HtmlScriptCollector scriptCollector1;
	protected HtmlForm form1;
	protected HtmlMessages messages1;
	protected HtmlOutputText textIncidentInfoLabel;
	protected HtmlOutputText text14;
	protected HtmlInputText textIncidentDate;
	protected HtmlSelectOneMenu menu1;
	protected HtmlSelectOneMenu menuBlanks;
	protected HtmlSelectOneMenu menuIncidentHomeDeliveryRequestType;
	protected HtmlSelectOneMenu menuIncidentNewsstandRequestType;
	protected HtmlSelectOneMenu menuIncidentRack;
	protected HtmlSelectOneMenu menuIncidentAgentInquiry;
	protected HtmlSelectOneMenu menuEEditionComments;
	protected UISelectItem selectItem1;
	protected UINamingContainer viewFragment1;
	protected HtmlSelectOneMenu menuELeadsRequestType;
	protected HtmlSelectOneMenu menuIncidentEducation;
	protected HtmlSelectOneMenu menuIncidentOrderFormRequestType;
	protected HtmlSelectOneMenu menuIncidentInvoiceRequestType;
	protected HtmlSelectOneMenu menuIncidentW9RequestType;
	protected HtmlSelectOneMenu menuIncidentNatCustSvc;
	protected HtmlSelectOneMenu menuIncidentBlueChip;
	protected HtmlSelectOneMenu menuAlternateDeliveryRequestType;
	protected UISelectItem selectItem5;
	protected UISelectItem selectItem6;
	protected UISelectItem selectItem7;
	protected UISelectItem selectItem8;
	protected UISelectItem selectItem2;
	protected UISelectItem selectItem3;
	protected HtmlInputTextarea getTextarea1() {
		if (textarea1 == null) {
			textarea1 = (HtmlInputTextarea) findComponentInRoot("textarea1");
		}
		return textarea1;
	}

	protected HtmlMessage getMessage3() {
		if (message3 == null) {
			message3 = (HtmlMessage) findComponentInRoot("message3");
		}
		return message3;
	}

	protected HtmlSelectBooleanCheckbox getCheckbox1() {
		if (checkbox1 == null) {
			checkbox1 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox1");
		}
		return checkbox1;
	}

	protected HtmlOutputText getTextRefundCheckboxInfo() {
		if (textRefundCheckboxInfo == null) {
			textRefundCheckboxInfo = (HtmlOutputText) findComponentInRoot("textRefundCheckboxInfo");
		}
		return textRefundCheckboxInfo;
	}

	protected HtmlInputText getText1() {
		if (text1 == null) {
			text1 = (HtmlInputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlMessage getMessage2() {
		if (message2 == null) {
			message2 = (HtmlMessage) findComponentInRoot("message2");
		}
		return message2;
	}

	protected HtmlInputText getText2() {
		if (text2 == null) {
			text2 = (HtmlInputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected HtmlInputText getText3() {
		if (text3 == null) {
			text3 = (HtmlInputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected HtmlSelectOneMenu getMenu2() {
		if (menu2 == null) {
			menu2 = (HtmlSelectOneMenu) findComponentInRoot("menu2");
		}
		return menu2;
	}

	protected HtmlInputText getText4() {
		if (text4 == null) {
			text4 = (HtmlInputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected HtmlOutputText getText13() {
		if (text13 == null) {
			text13 = (HtmlOutputText) findComponentInRoot("text13");
		}
		return text13;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlMessage getMessage1() {
		if (message1 == null) {
			message1 = (HtmlMessage) findComponentInRoot("message1");
		}
		return message1;
	}

	protected HtmlOutputText getTextCustomerHeaderLabel() {
		if (textCustomerHeaderLabel == null) {
			textCustomerHeaderLabel = (HtmlOutputText) findComponentInRoot("textCustomerHeaderLabel");
		}
		return textCustomerHeaderLabel;
	}

	protected HtmlSelectBooleanCheckbox getCheckbox2() {
		if (checkbox2 == null) {
			checkbox2 = (HtmlSelectBooleanCheckbox) findComponentInRoot("checkbox2");
		}
		return checkbox2;
	}

	protected HtmlInputText getText5() {
		if (text5 == null) {
			text5 = (HtmlInputText) findComponentInRoot("text5");
		}
		return text5;
	}

	protected HtmlInputText getTextCustLastName() {
		if (textCustLastName == null) {
			textCustLastName = (HtmlInputText) findComponentInRoot("textCustLastName");
		}
		return textCustLastName;
	}

	protected HtmlInputText getText6() {
		if (text6 == null) {
			text6 = (HtmlInputText) findComponentInRoot("text6");
		}
		return text6;
	}

	protected HtmlInputText getText7() {
		if (text7 == null) {
			text7 = (HtmlInputText) findComponentInRoot("text7");
		}
		return text7;
	}

	protected HtmlInputText getText8() {
		if (text8 == null) {
			text8 = (HtmlInputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlInputText getText9() {
		if (text9 == null) {
			text9 = (HtmlInputText) findComponentInRoot("text9");
		}
		return text9;
	}

	protected HtmlInputText getText10() {
		if (text10 == null) {
			text10 = (HtmlInputText) findComponentInRoot("text10");
		}
		return text10;
	}

	protected HtmlInputText getText11() {
		if (text11 == null) {
			text11 = (HtmlInputText) findComponentInRoot("text11");
		}
		return text11;
	}

	protected HtmlSelectOneMenu getMenu3() {
		if (menu3 == null) {
			menu3 = (HtmlSelectOneMenu) findComponentInRoot("menu3");
		}
		return menu3;
	}

	protected HtmlInputText getText12() {
		if (text12 == null) {
			text12 = (HtmlInputText) findComponentInRoot("text12");
		}
		return text12;
	}

	protected HtmlCommandExButton getButtonCreateIncident() {
		if (buttonCreateIncident == null) {
			buttonCreateIncident = (HtmlCommandExButton) findComponentInRoot("buttonCreateIncident");
		}
		return buttonCreateIncident;
	}

	protected HtmlCommandExButton getButtonCancelNewIncident() {
		if (buttonCancelNewIncident == null) {
			buttonCancelNewIncident = (HtmlCommandExButton) findComponentInRoot("buttonCancelNewIncident");
		}
		return buttonCancelNewIncident;
	}

	protected HtmlInputHidden getUniqueRequestValue() {
		if (uniqueRequestValue == null) {
			uniqueRequestValue = (HtmlInputHidden) findComponentInRoot("uniqueRequestValue");
		}
		return uniqueRequestValue;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected HtmlForm getForm1() {
		if (form1 == null) {
			form1 = (HtmlForm) findComponentInRoot("form1");
		}
		return form1;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

	protected HtmlOutputText getTextIncidentInfoLabel() {
		if (textIncidentInfoLabel == null) {
			textIncidentInfoLabel = (HtmlOutputText) findComponentInRoot("textIncidentInfoLabel");
		}
		return textIncidentInfoLabel;
	}

	protected HtmlOutputText getText14() {
		if (text14 == null) {
			text14 = (HtmlOutputText) findComponentInRoot("text14");
		}
		return text14;
	}

	protected HtmlInputText getTextIncidentDate() {
		if (textIncidentDate == null) {
			textIncidentDate = (HtmlInputText) findComponentInRoot("textIncidentDate");
		}
		return textIncidentDate;
	}

	protected HtmlSelectOneMenu getMenu1() {
		if (menu1 == null) {
			menu1 = (HtmlSelectOneMenu) findComponentInRoot("menu1");
		}
		return menu1;
	}

	protected HtmlSelectOneMenu getMenuBlanks() {
		if (menuBlanks == null) {
			menuBlanks = (HtmlSelectOneMenu) findComponentInRoot("menuBlanks");
		}
		return menuBlanks;
	}

	protected HtmlSelectOneMenu getMenuIncidentHomeDeliveryRequestType() {
		if (menuIncidentHomeDeliveryRequestType == null) {
			menuIncidentHomeDeliveryRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentHomeDeliveryRequestType");
		}
		return menuIncidentHomeDeliveryRequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentNewsstandRequestType() {
		if (menuIncidentNewsstandRequestType == null) {
			menuIncidentNewsstandRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentNewsstandRequestType");
		}
		return menuIncidentNewsstandRequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentRack() {
		if (menuIncidentRack == null) {
			menuIncidentRack = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentRack");
		}
		return menuIncidentRack;
	}

	protected HtmlSelectOneMenu getMenuIncidentAgentInquiry() {
		if (menuIncidentAgentInquiry == null) {
			menuIncidentAgentInquiry = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentAgentInquiry");
		}
		return menuIncidentAgentInquiry;
	}

	protected HtmlSelectOneMenu getMenuEEditionComments() {
		if (menuEEditionComments == null) {
			menuEEditionComments = (HtmlSelectOneMenu) findComponentInRoot("menuEEditionComments");
		}
		return menuEEditionComments;
	}

	protected UISelectItem getSelectItem1() {
		if (selectItem1 == null) {
			selectItem1 = (UISelectItem) findComponentInRoot("selectItem1");
		}
		return selectItem1;
	}

	protected UINamingContainer getViewFragment1() {
		if (viewFragment1 == null) {
			viewFragment1 = (UINamingContainer) findComponentInRoot("viewFragment1");
		}
		return viewFragment1;
	}

	protected HtmlSelectOneMenu getMenuELeadsRequestType() {
		if (menuELeadsRequestType == null) {
			menuELeadsRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuELeadsRequestType");
		}
		return menuELeadsRequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentEducation() {
		if (menuIncidentEducation == null) {
			menuIncidentEducation = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentEducation");
		}
		return menuIncidentEducation;
	}

	protected HtmlSelectOneMenu getMenuIncidentOrderFormRequestType() {
		if (menuIncidentOrderFormRequestType == null) {
			menuIncidentOrderFormRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentOrderFormRequestType");
		}
		return menuIncidentOrderFormRequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentInvoiceRequestType() {
		if (menuIncidentInvoiceRequestType == null) {
			menuIncidentInvoiceRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentInvoiceRequestType");
		}
		return menuIncidentInvoiceRequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentW9RequestType() {
		if (menuIncidentW9RequestType == null) {
			menuIncidentW9RequestType = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentW9RequestType");
		}
		return menuIncidentW9RequestType;
	}

	protected HtmlSelectOneMenu getMenuIncidentNatCustSvc() {
		if (menuIncidentNatCustSvc == null) {
			menuIncidentNatCustSvc = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentNatCustSvc");
		}
		return menuIncidentNatCustSvc;
	}

	protected HtmlSelectOneMenu getMenuIncidentBlueChip() {
		if (menuIncidentBlueChip == null) {
			menuIncidentBlueChip = (HtmlSelectOneMenu) findComponentInRoot("menuIncidentBlueChip");
		}
		return menuIncidentBlueChip;
	}

	protected HtmlSelectOneMenu getMenuAlternateDeliveryRequestType() {
		if (menuAlternateDeliveryRequestType == null) {
			menuAlternateDeliveryRequestType = (HtmlSelectOneMenu) findComponentInRoot("menuAlternateDeliveryRequestType");
		}
		return menuAlternateDeliveryRequestType;
	}

	protected UISelectItem getSelectItem5() {
		if (selectItem5 == null) {
			selectItem5 = (UISelectItem) findComponentInRoot("selectItem5");
		}
		return selectItem5;
	}

	protected UISelectItem getSelectItem6() {
		if (selectItem6 == null) {
			selectItem6 = (UISelectItem) findComponentInRoot("selectItem6");
		}
		return selectItem6;
	}

	protected UISelectItem getSelectItem7() {
		if (selectItem7 == null) {
			selectItem7 = (UISelectItem) findComponentInRoot("selectItem7");
		}
		return selectItem7;
	}

	protected UISelectItem getSelectItem8() {
		if (selectItem8 == null) {
			selectItem8 = (UISelectItem) findComponentInRoot("selectItem8");
		}
		return selectItem8;
	}

	protected UISelectItem getSelectItem2() {
		if (selectItem2 == null) {
			selectItem2 = (UISelectItem) findComponentInRoot("selectItem2");
		}
		return selectItem2;
	}

	protected UISelectItem getSelectItem3() {
		if (selectItem3 == null) {
			selectItem3 = (UISelectItem) findComponentInRoot("selectItem3");
		}
		return selectItem3;
	}

}