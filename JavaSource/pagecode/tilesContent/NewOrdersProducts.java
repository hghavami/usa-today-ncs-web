/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlPanelGrid;
import com.usatoday.ncs.web.subscription.handlers.SubscriptionOfferLocatorHandler;
import javax.faces.component.UINamingContainer;
import com.usatoday.ncs.web.subscription.handlers.ProductsHandler;
import com.ibm.faces.component.html.HtmlDataTableEx;
import com.ibm.faces.component.UIColumnEx;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGroup;
import com.ibm.faces.component.html.HtmlJspPanel;
import com.ibm.faces.component.html.HtmlGraphicImageEx;
import com.ibm.faces.component.html.HtmlPanelSection;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlMessages;

/**
 * @author aeast
 *
 */
public class NewOrdersProducts extends PageCodeBase {

	protected HtmlScriptCollector scriptCollectorProductSelection;
	protected HtmlPanelGrid gridProductGrid;
	protected SubscriptionOfferLocatorHandler offerSearchHandler;
	protected UINamingContainer viewFragmentProductSelectionFragment;
	protected ProductsHandler subscriptionProductsHandler;
	protected HtmlDataTableEx tableExAvailableProducts;
	protected UIColumnEx columnExPubCode;
	protected HtmlOutputText textProductCodeLabel11;
	protected HtmlPanelGroup groupProductGridHeaderGroup;
	protected HtmlOutputText textProductGridHeader;
	protected HtmlOutputText text2;
	protected UIColumnEx columnEx1;
	protected HtmlOutputText text3;
	protected UIColumnEx columnEx2;
	protected HtmlOutputText text4;
	protected UIColumnEx columnEx3;
	protected HtmlOutputText textPubCode;
	protected HtmlOutputText textProductName;
	protected HtmlJspPanel jspPanelProductDescriptoinClosed;
	protected HtmlGraphicImageEx imageEx2;
	protected HtmlJspPanel jspPanelProductDescriptoinOpen;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlPanelSection sectionProductDetail;
	protected HtmlOutputText textProductDescriptionClosedHeader;
	protected HtmlOutputText text8;
	protected HtmlPanelGrid gridProductDetailGridOpened;
	protected HtmlOutputText textProductDetailedDescription;
	protected HtmlGraphicImageEx imageExProductImage;
	protected HtmlOutputText textOrderLinkLabel;
	protected HtmlOutputLinkEx linkExPlaceOrderLink;
	protected HtmlMessages messages1;
	protected HtmlScriptCollector getScriptCollectorProductSelection() {
		if (scriptCollectorProductSelection == null) {
			scriptCollectorProductSelection = (HtmlScriptCollector) findComponentInRoot("scriptCollectorProductSelection");
		}
		return scriptCollectorProductSelection;
	}

	protected HtmlPanelGrid getGridProductGrid() {
		if (gridProductGrid == null) {
			gridProductGrid = (HtmlPanelGrid) findComponentInRoot("gridProductGrid");
		}
		return gridProductGrid;
	}

	/** 
	 * @managed-bean true
	 */
	protected SubscriptionOfferLocatorHandler getOfferSearchHandler() {
		if (offerSearchHandler == null) {
			offerSearchHandler = (SubscriptionOfferLocatorHandler) getManagedBean("offerSearchHandler");
		}
		return offerSearchHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setOfferSearchHandler(
			SubscriptionOfferLocatorHandler offerSearchHandler) {
		this.offerSearchHandler = offerSearchHandler;
	}

	protected UINamingContainer getViewFragmentProductSelectionFragment() {
		if (viewFragmentProductSelectionFragment == null) {
			viewFragmentProductSelectionFragment = (UINamingContainer) findComponentInRoot("viewFragmentProductSelectionFragment");
		}
		return viewFragmentProductSelectionFragment;
	}

	/** 
	 * @managed-bean true
	 */
	protected ProductsHandler getSubscriptionProductsHandler() {
		if (subscriptionProductsHandler == null) {
			subscriptionProductsHandler = (ProductsHandler) getManagedBean("subscriptionProductsHandler");
		}
		return subscriptionProductsHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSubscriptionProductsHandler(
			ProductsHandler subscriptionProductsHandler) {
		this.subscriptionProductsHandler = subscriptionProductsHandler;
	}

	protected HtmlDataTableEx getTableExAvailableProducts() {
		if (tableExAvailableProducts == null) {
			tableExAvailableProducts = (HtmlDataTableEx) findComponentInRoot("tableExAvailableProducts");
		}
		return tableExAvailableProducts;
	}

	protected UIColumnEx getColumnExPubCode() {
		if (columnExPubCode == null) {
			columnExPubCode = (UIColumnEx) findComponentInRoot("columnExPubCode");
		}
		return columnExPubCode;
	}

	protected HtmlOutputText getTextProductCodeLabel11() {
		if (textProductCodeLabel11 == null) {
			textProductCodeLabel11 = (HtmlOutputText) findComponentInRoot("textProductCodeLabel11");
		}
		return textProductCodeLabel11;
	}

	protected HtmlPanelGroup getGroupProductGridHeaderGroup() {
		if (groupProductGridHeaderGroup == null) {
			groupProductGridHeaderGroup = (HtmlPanelGroup) findComponentInRoot("groupProductGridHeaderGroup");
		}
		return groupProductGridHeaderGroup;
	}

	protected HtmlOutputText getTextProductGridHeader() {
		if (textProductGridHeader == null) {
			textProductGridHeader = (HtmlOutputText) findComponentInRoot("textProductGridHeader");
		}
		return textProductGridHeader;
	}

	protected HtmlOutputText getText2() {
		if (text2 == null) {
			text2 = (HtmlOutputText) findComponentInRoot("text2");
		}
		return text2;
	}

	protected UIColumnEx getColumnEx1() {
		if (columnEx1 == null) {
			columnEx1 = (UIColumnEx) findComponentInRoot("columnEx1");
		}
		return columnEx1;
	}

	protected HtmlOutputText getText3() {
		if (text3 == null) {
			text3 = (HtmlOutputText) findComponentInRoot("text3");
		}
		return text3;
	}

	protected UIColumnEx getColumnEx2() {
		if (columnEx2 == null) {
			columnEx2 = (UIColumnEx) findComponentInRoot("columnEx2");
		}
		return columnEx2;
	}

	protected HtmlOutputText getText4() {
		if (text4 == null) {
			text4 = (HtmlOutputText) findComponentInRoot("text4");
		}
		return text4;
	}

	protected UIColumnEx getColumnEx3() {
		if (columnEx3 == null) {
			columnEx3 = (UIColumnEx) findComponentInRoot("columnEx3");
		}
		return columnEx3;
	}

	protected HtmlOutputText getTextPubCode() {
		if (textPubCode == null) {
			textPubCode = (HtmlOutputText) findComponentInRoot("textPubCode");
		}
		return textPubCode;
	}

	protected HtmlOutputText getTextProductName() {
		if (textProductName == null) {
			textProductName = (HtmlOutputText) findComponentInRoot("textProductName");
		}
		return textProductName;
	}

	protected HtmlJspPanel getJspPanelProductDescriptoinClosed() {
		if (jspPanelProductDescriptoinClosed == null) {
			jspPanelProductDescriptoinClosed = (HtmlJspPanel) findComponentInRoot("jspPanelProductDescriptoinClosed");
		}
		return jspPanelProductDescriptoinClosed;
	}

	protected HtmlGraphicImageEx getImageEx2() {
		if (imageEx2 == null) {
			imageEx2 = (HtmlGraphicImageEx) findComponentInRoot("imageEx2");
		}
		return imageEx2;
	}

	protected HtmlJspPanel getJspPanelProductDescriptoinOpen() {
		if (jspPanelProductDescriptoinOpen == null) {
			jspPanelProductDescriptoinOpen = (HtmlJspPanel) findComponentInRoot("jspPanelProductDescriptoinOpen");
		}
		return jspPanelProductDescriptoinOpen;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlPanelSection getSectionProductDetail() {
		if (sectionProductDetail == null) {
			sectionProductDetail = (HtmlPanelSection) findComponentInRoot("sectionProductDetail");
		}
		return sectionProductDetail;
	}

	protected HtmlOutputText getTextProductDescriptionClosedHeader() {
		if (textProductDescriptionClosedHeader == null) {
			textProductDescriptionClosedHeader = (HtmlOutputText) findComponentInRoot("textProductDescriptionClosedHeader");
		}
		return textProductDescriptionClosedHeader;
	}

	protected HtmlOutputText getText8() {
		if (text8 == null) {
			text8 = (HtmlOutputText) findComponentInRoot("text8");
		}
		return text8;
	}

	protected HtmlPanelGrid getGridProductDetailGridOpened() {
		if (gridProductDetailGridOpened == null) {
			gridProductDetailGridOpened = (HtmlPanelGrid) findComponentInRoot("gridProductDetailGridOpened");
		}
		return gridProductDetailGridOpened;
	}

	protected HtmlOutputText getTextProductDetailedDescription() {
		if (textProductDetailedDescription == null) {
			textProductDetailedDescription = (HtmlOutputText) findComponentInRoot("textProductDetailedDescription");
		}
		return textProductDetailedDescription;
	}

	protected HtmlGraphicImageEx getImageExProductImage() {
		if (imageExProductImage == null) {
			imageExProductImage = (HtmlGraphicImageEx) findComponentInRoot("imageExProductImage");
		}
		return imageExProductImage;
	}


	protected HtmlOutputText getTextOrderLinkLabel() {
		if (textOrderLinkLabel == null) {
			textOrderLinkLabel = (HtmlOutputText) findComponentInRoot("textOrderLinkLabel");
		}
		return textOrderLinkLabel;
	}

	protected HtmlOutputLinkEx getLinkExPlaceOrderLink() {
		if (linkExPlaceOrderLink == null) {
			linkExPlaceOrderLink = (HtmlOutputLinkEx) findComponentInRoot("linkExPlaceOrderLink");
		}
		return linkExPlaceOrderLink;
	}

	protected HtmlMessages getMessages1() {
		if (messages1 == null) {
			messages1 = (HtmlMessages) findComponentInRoot("messages1");
		}
		return messages1;
	}

}