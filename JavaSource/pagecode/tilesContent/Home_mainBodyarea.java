/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import javax.faces.component.html.HtmlPanelGrid;
import com.ibm.faces.component.html.HtmlOutputLinkEx;
import javax.faces.component.html.HtmlOutputText;
import com.usatoday.ncs.web.subscription.handlers.EEProductHandler;
import com.ibm.faces.component.html.HtmlGraphicImageEx;

/**
 * @author aeast
 *
 */
public class Home_mainBodyarea extends PageCodeBase {

	protected HtmlPanelGrid gridEEditionGrid;
	protected HtmlOutputLinkEx linkEx1;
	protected HtmlOutputText textLinkText;
	protected EEProductHandler eeLinkHandler;
	protected HtmlOutputLinkEx linkEx2;
	protected HtmlGraphicImageEx imageEx1;
	protected HtmlOutputText text1;
	protected HtmlOutputLinkEx linkEx3;
	protected HtmlPanelGrid getGridEEditionGrid() {
		if (gridEEditionGrid == null) {
			gridEEditionGrid = (HtmlPanelGrid) findComponentInRoot("gridEEditionGrid");
		}
		return gridEEditionGrid;
	}

	protected HtmlOutputLinkEx getLinkEx1() {
		if (linkEx1 == null) {
			linkEx1 = (HtmlOutputLinkEx) findComponentInRoot("linkEx1");
		}
		return linkEx1;
	}

	protected HtmlOutputText getTextLinkText() {
		if (textLinkText == null) {
			textLinkText = (HtmlOutputText) findComponentInRoot("textLinkText");
		}
		return textLinkText;
	}

	/** 
	 * @managed-bean true
	 */
	protected EEProductHandler getEeLinkHandler() {
		if (eeLinkHandler == null) {
			eeLinkHandler = (EEProductHandler) getManagedBean("eeLinkHandler");
		}
		return eeLinkHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setEeLinkHandler(EEProductHandler eeLinkHandler) {
		this.eeLinkHandler = eeLinkHandler;
	}

	protected HtmlOutputLinkEx getLinkEx2() {
		if (linkEx2 == null) {
			linkEx2 = (HtmlOutputLinkEx) findComponentInRoot("linkEx2");
		}
		return linkEx2;
	}

	protected HtmlGraphicImageEx getImageEx1() {
		if (imageEx1 == null) {
			imageEx1 = (HtmlGraphicImageEx) findComponentInRoot("imageEx1");
		}
		return imageEx1;
	}

	protected HtmlOutputText getText1() {
		if (text1 == null) {
			text1 = (HtmlOutputText) findComponentInRoot("text1");
		}
		return text1;
	}

	protected HtmlOutputLinkEx getLinkEx3() {
		if (linkEx3 == null) {
			linkEx3 = (HtmlOutputLinkEx) findComponentInRoot("linkEx3");
		}
		return linkEx3;
	}

}