/**
 * 
 */
package pagecode.tilesContent;

import pagecode.PageCodeBase;
import com.usatoday.ncs.web.handlers.UserHandler;

/**
 * @author aeast
 *
 */
public class Index_mainBodyarea extends PageCodeBase {

	protected UserHandler user;

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

}