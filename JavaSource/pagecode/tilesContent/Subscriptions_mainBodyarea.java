/**
 * 
 */
package pagecode.tilesContent;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.usatoday.ncs.bo.KeycodeBO;
import com.usatoday.ncs.integration.KeycodeDAO;
import com.usatoday.ncs.web.handlers.NewStartOfferCodeSelectionHandler;

import pagecode.PageCodeBase;
import com.usatoday.ncs.web.handlers.NewStartLinkHandler;
import com.usatoday.ncs.web.handlers.UserHandler;

/**
 * @author aeast
 *
 */
public class Subscriptions_mainBodyarea extends PageCodeBase {

	protected NewStartLinkHandler subscriberPortalLink;
	protected NewStartOfferCodeSelectionHandler newStartFormHandler;
	protected UserHandler user;

	public String doButtonSubmitOfferCheckAction() {
		// Type Java code that runs when the component is clicked
	
	
    	String pubCode = "UT";
        
       	
        try {
        	this.getSubscriberPortalLink().clear();
        	
        	NewStartOfferCodeSelectionHandler nsoh = this.getNewStartFormHandler();
        	
        	pubCode = nsoh.getPubCode();
            
            
           	String offerKey = nsoh.getOfferCode().trim();
           
        	KeycodeDAO dao= new KeycodeDAO();
        		
        	KeycodeBO keycode1 = dao.fetchKeycode(pubCode, offerKey);

        	if (keycode1 == null) {
        		// expired key code
                FacesContext context = FacesContext.getCurrentInstance();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "The offer code does not exist or has expired. Pub: " + pubCode + " Offer Code: " + offerKey, null);
                context.addMessage(null, message);
        		this.getSubscriberPortalLink().setShowURL(false);
                
                return "failure";
        	}
        	else {
        		// it's valid set up the link
        		StringBuilder urlString = new StringBuilder("/subscriptions/index.jsp?pub=");
        		urlString.append(pubCode);
        		urlString.append("&keycode=").append(keycode1.getKeyCode());
        		this.getSubscriberPortalLink().setLinkURL(urlString.toString());
        		this.getSubscriberPortalLink().setPubCode(pubCode);
        		this.getSubscriberPortalLink().setKeycode(keycode1.getKeyCode());
        		this.getSubscriberPortalLink().setShowURL(true);
        	}
    		
    	     
         }
         catch (Exception exp) {
         	System.out.println("NCS WEB: Market notification exception outter catch:  " + exp.getMessage());
    	            FacesContext context = FacesContext.getCurrentInstance();
    	            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Unexpected Exception occurred:  " + exp.getMessage(), null);
    	            context.addMessage(null, message);
    	            
    	            return "failure";
         }
         
        
            return "success";

	}

	/** 
	 * @managed-bean true
	 */
	protected NewStartLinkHandler getSubscriberPortalLink() {
		if (subscriberPortalLink == null) {
			subscriberPortalLink = (NewStartLinkHandler) getManagedBean("subscriberPortalLink");
		}
		return subscriberPortalLink;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setSubscriberPortalLink(
			NewStartLinkHandler subscriberPortalLink) {
		this.subscriberPortalLink = subscriberPortalLink;
	}

	/** 
	 * @managed-bean true
	 */
	protected NewStartOfferCodeSelectionHandler getNewStartFormHandler() {
		if (newStartFormHandler == null) {
			newStartFormHandler = (NewStartOfferCodeSelectionHandler) getManagedBean("newStartFormHandler");
		}
		return newStartFormHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setNewStartFormHandler(
			NewStartOfferCodeSelectionHandler newStartFormHandler) {
		this.newStartFormHandler = newStartFormHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

}