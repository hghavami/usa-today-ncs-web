/**
 * 
 */
package pagecode.theme.themeV2;

import pagecode.PageCodeBase;
import com.usatoday.ncs.web.handlers.UserHandler;
import com.usatoday.ncs.web.handlers.LocaleHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;

/**
 * @author aeast
 *
 */
public class NcsWebRootTemplate extends PageCodeBase {

	protected UserHandler user;
	protected LocaleHandler userLocaleHandler;
	protected HtmlScriptCollector scriptCollector1;
	protected UINamingContainer subviewTemplateLeftNavContentArea;
	protected HtmlOutputFormat formatWelcomeMessage;
	protected HtmlOutputText textLogoutLink;
	protected UINamingContainer subviewTemplateHeaderArea;
	protected HtmlCommandLink linkTemplateLogoutLink;
	protected UINamingContainer subviewMainBodyArea;
	protected HtmlForm formTemplateLogout;
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}

	public String doLinkTemplateLogoutLinkAction() {
		// Type Java code that runs when the component is clicked
	
    	//System.out.println("Logging out");
        UserHandler userh = this.getUser();
        
        userh.setUser(null);
        
        return "success";
	}

	/** 
	 * @managed-bean true
	 */
	protected LocaleHandler getUserLocaleHandler() {
		if (userLocaleHandler == null) {
			userLocaleHandler = (LocaleHandler) getManagedBean("userLocaleHandler");
		}
		return userLocaleHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserLocaleHandler(LocaleHandler userLocaleHandler) {
		this.userLocaleHandler = userLocaleHandler;
	}

	protected HtmlScriptCollector getScriptCollector1() {
		if (scriptCollector1 == null) {
			scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
		}
		return scriptCollector1;
	}

	protected UINamingContainer getSubviewTemplateLeftNavContentArea() {
		if (subviewTemplateLeftNavContentArea == null) {
			subviewTemplateLeftNavContentArea = (UINamingContainer) findComponentInRoot("subviewTemplateLeftNavContentArea");
		}
		return subviewTemplateLeftNavContentArea;
	}

	protected HtmlOutputFormat getFormatWelcomeMessage() {
		if (formatWelcomeMessage == null) {
			formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
		}
		return formatWelcomeMessage;
	}

	protected HtmlOutputText getTextLogoutLink() {
		if (textLogoutLink == null) {
			textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
		}
		return textLogoutLink;
	}

	protected UINamingContainer getSubviewTemplateHeaderArea() {
		if (subviewTemplateHeaderArea == null) {
			subviewTemplateHeaderArea = (UINamingContainer) findComponentInRoot("subviewTemplateHeaderArea");
		}
		return subviewTemplateHeaderArea;
	}

	protected HtmlCommandLink getLinkTemplateLogoutLink() {
		if (linkTemplateLogoutLink == null) {
			linkTemplateLogoutLink = (HtmlCommandLink) findComponentInRoot("linkTemplateLogoutLink");
		}
		return linkTemplateLogoutLink;
	}

	protected UINamingContainer getSubviewMainBodyArea() {
		if (subviewMainBodyArea == null) {
			subviewMainBodyArea = (UINamingContainer) findComponentInRoot("subviewMainBodyArea");
		}
		return subviewMainBodyArea;
	}

	protected HtmlForm getFormTemplateLogout() {
		if (formTemplateLogout == null) {
			formTemplateLogout = (HtmlForm) findComponentInRoot("formTemplateLogout");
		}
		return formTemplateLogout;
	}

}