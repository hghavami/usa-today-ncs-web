/*
 * Created on Oct 15, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode.theme;

import com.usatoday.ncs.web.handlers.UserHandler;

import pagecode.PageCodeBase;
import com.usatoday.ncs.web.handlers.LocaleHandler;
import com.ibm.faces.component.html.HtmlScriptCollector;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlCommandLink;
/**
 * @author aeast
 * @date Oct 15, 2007
 * @class themeJSPCblue_mod
 * 
 * 
 */
public class JSPCblue_mod extends PageCodeBase {

    protected LocaleHandler userLocaleHandler;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlOutputText textLogoutLink;
    protected HtmlCommandLink linkLogout;
	protected UserHandler user;
    public String doLinkLogoutAction() {
        // Type Java code that runs when the component is clicked

    	//System.out.println("Logging out");
        UserHandler userh = this.getUser();
        
        userh.setUser(null);
        return "success";
    }

    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }

	/** 
	 * @managed-bean true
	 */
	protected LocaleHandler getUserLocaleHandler() {
		if (userLocaleHandler == null) {
			userLocaleHandler = (LocaleHandler) getManagedBean("userLocaleHandler");
		}
		return userLocaleHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUserLocaleHandler(LocaleHandler userLocaleHandler) {
		this.userLocaleHandler = userLocaleHandler;
	}

	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}

	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}
}