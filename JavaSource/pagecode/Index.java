/*
 * Created on Oct 15, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package pagecode;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputSecret;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlOutputFormat;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import com.ibm.faces.component.html.HtmlCommandExButton;
import com.ibm.faces.component.html.HtmlInputHelperKeybind;
import com.ibm.faces.component.html.HtmlScriptCollector;
import com.usatoday.ncs.bo.KeycodeBO;
import com.usatoday.ncs.bo.UserBO;
import com.usatoday.ncs.util.IPToLongConverter;
import com.usatoday.ncs.web.handlers.MarketCacheHandler;
import com.usatoday.ncs.web.handlers.UserHandler;
/**
 * @author aeast
 * @date Oct 15, 2007
 * @class pagecodeIndex
 * 
 * 
 * 
 */
public class Index extends PageCodeBase {

    protected MarketCacheHandler marketsCache;
    protected UserHandler user;
    protected HtmlInputText text1;
    protected HtmlScriptCollector scriptCollector2;
    protected HtmlForm formLogout;
    protected HtmlOutputFormat formatWelcomeMessage;
    protected HtmlOutputText textLogoutLink;
    protected HtmlScriptCollector scriptCollector1;
    protected HtmlForm loginForm;
    protected HtmlMessages messages1;
    protected HtmlOutputText textUserIDLabel;
    protected HtmlInputText textUserIDInput;
    protected HtmlOutputText textSiteIDLabel;
    protected HtmlInputText textSiteIDInput;
    protected HtmlOutputText textPasswordLabel;
    protected HtmlInputSecret secretPasswordInput;
    protected HtmlCommandExButton buttonAuthenticate;
    protected HtmlCommandLink linkLogout;
    protected HtmlInputHelperKeybind inputHelperKeybind1;
	protected KeycodeBO keycode;
  
    protected HtmlInputText getText1() {
        if (text1 == null) {
            text1 = (HtmlInputText) findComponentInRoot("text1");
        }
        return text1;
    }
    public String doButtonAuthenticateAction() {
        // Type Java code that runs when the component is clicked

        String responeStr = "success";
        //
        if (this.getUser().getClientIP() == null) {
	        HttpServletRequest request = (HttpServletRequest)this.getFacesContext().getExternalContext().getRequest();
	        
	        String clientIP = request.getRemoteAddr();
	        
	        this.getUser().setClientIP(clientIP);
        }        
        
        try {
            
        	
            this.getUser().setUser(UserBO.authenticateSite(this.getUser().getSiteID(), (String)this.getSecretPasswordInput().getValue()));
            
            if (!this.getUser().getUser().isAuthenticated()) {
                FacesContext context = FacesContext.getCurrentInstance();
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Invalid Site ID/Password combination.", null);
                context.addMessage(null, message);
                responeStr = "loginfailed";
            }
            else {
                this.getUser().getUser().setUserName(this.getUser().getUserID());
                
                UserHandler userh = this.getUser();
                
        		try {
        			if (!userh.isUserIPValidated()) {
        			    // indicate ip has been validated so we don't do this every time
        			    userh.setUserIPValidated(true);
        			    String clientIP = userh.getClientIP();
        			    long convertedIP =  IPToLongConverter.convertIPStringToLong(clientIP);
        			    
        			    boolean ipValid = false;
        			    if (convertedIP >= userh.getUser().getLowerIPRange() && convertedIP <= userh.getUser().getUpperIPRange()) {
        			        ipValid = true;
        			    }
        			    
        			    userh.setClientIPInRange(ipValid);
        			}
        			
        			if (!userh.isClientIPInRange()) {
        			    // send to forbidden page nullify user so even if authenticated it will remove them
        			    userh.setUser(null);
            		    responeStr = "badip";
        			    
        			}
        		}
        		catch (Exception e){
        		    System.out.println("NCS WEB: IP Exception: " + e.getMessage());
    			    userh.setUser(null);
        		    responeStr = "badip";
        		}
                
            }
                
        }
        catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Problem Authenticating: " + e.getMessage(), null);
            context.addMessage(null, message);
            responeStr = "loginfailed";
        }
        
        return responeStr;
    }
    protected HtmlScriptCollector getScriptCollector2() {
        if (scriptCollector2 == null) {
            scriptCollector2 = (HtmlScriptCollector) findComponentInRoot("scriptCollector2");
        }
        return scriptCollector2;
    }
    protected HtmlForm getFormLogout() {
        if (formLogout == null) {
            formLogout = (HtmlForm) findComponentInRoot("formLogout");
        }
        return formLogout;
    }
    protected HtmlOutputFormat getFormatWelcomeMessage() {
        if (formatWelcomeMessage == null) {
            formatWelcomeMessage = (HtmlOutputFormat) findComponentInRoot("formatWelcomeMessage");
        }
        return formatWelcomeMessage;
    }
    protected HtmlOutputText getTextLogoutLink() {
        if (textLogoutLink == null) {
            textLogoutLink = (HtmlOutputText) findComponentInRoot("textLogoutLink");
        }
        return textLogoutLink;
    }
    protected HtmlScriptCollector getScriptCollector1() {
        if (scriptCollector1 == null) {
            scriptCollector1 = (HtmlScriptCollector) findComponentInRoot("scriptCollector1");
        }
        return scriptCollector1;
    }
    protected HtmlForm getLoginForm() {
        if (loginForm == null) {
            loginForm = (HtmlForm) findComponentInRoot("loginForm");
        }
        return loginForm;
    }
    protected HtmlMessages getMessages1() {
        if (messages1 == null) {
            messages1 = (HtmlMessages) findComponentInRoot("messages1");
        }
        return messages1;
    }
    protected HtmlOutputText getTextUserIDLabel() {
        if (textUserIDLabel == null) {
            textUserIDLabel = (HtmlOutputText) findComponentInRoot("textUserIDLabel");
        }
        return textUserIDLabel;
    }
    protected HtmlInputText getTextUserIDInput() {
        if (textUserIDInput == null) {
            textUserIDInput = (HtmlInputText) findComponentInRoot("textUserIDInput");
        }
        return textUserIDInput;
    }
    protected HtmlOutputText getTextSiteIDLabel() {
        if (textSiteIDLabel == null) {
            textSiteIDLabel = (HtmlOutputText) findComponentInRoot("textSiteIDLabel");
        }
        return textSiteIDLabel;
    }
    protected HtmlInputText getTextSiteIDInput() {
        if (textSiteIDInput == null) {
            textSiteIDInput = (HtmlInputText) findComponentInRoot("textSiteIDInput");
        }
        return textSiteIDInput;
    }
    protected HtmlOutputText getTextPasswordLabel() {
        if (textPasswordLabel == null) {
            textPasswordLabel = (HtmlOutputText) findComponentInRoot("textPasswordLabel");
        }
        return textPasswordLabel;
    }
    protected HtmlInputSecret getSecretPasswordInput() {
        if (secretPasswordInput == null) {
            secretPasswordInput = (HtmlInputSecret) findComponentInRoot("secretPasswordInput");
        }
        return secretPasswordInput;
    }
    protected HtmlCommandExButton getButtonAuthenticate() {
        if (buttonAuthenticate == null) {
            buttonAuthenticate = (HtmlCommandExButton) findComponentInRoot("buttonAuthenticate");
        }
        return buttonAuthenticate;
    }
    protected HtmlCommandLink getLinkLogout() {
        if (linkLogout == null) {
            linkLogout = (HtmlCommandLink) findComponentInRoot("linkLogout");
        }
        return linkLogout;
    }
    protected HtmlInputHelperKeybind getInputHelperKeybind1() {
        if (inputHelperKeybind1 == null) {
            inputHelperKeybind1 = (HtmlInputHelperKeybind) findComponentInRoot("inputHelperKeybind1");
        }
        return inputHelperKeybind1;
    }
	/** 
	 * @managed-bean true
	 */
	protected MarketCacheHandler getMarketsCache() {
		if (marketsCache == null) {
			marketsCache = (MarketCacheHandler) getManagedBean("marketsCache");
		}
		return marketsCache;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setMarketsCache(MarketCacheHandler marketsCache) {
		this.marketsCache = marketsCache;
	}
	/** 
	 * @managed-bean true
	 */
	protected UserHandler getUser() {
		if (user == null) {
			user = (UserHandler) getManagedBean("user");
		}
		return user;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setUser(UserHandler user) {
		this.user = user;
	}
	/** 
	 * @managed-bean true
	 */
	protected KeycodeBO getKeycode() {
		if (keycode == null) {
			keycode = (KeycodeBO) getManagedBean("keycode");
		}
		return keycode;
	}
	/** 
	 * @managed-bean true
	 */
	protected void setKeycode(KeycodeBO keycode) {
		this.keycode = keycode;
	}
}