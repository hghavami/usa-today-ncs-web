/**
 * Subscription_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Subscription_Helper {
    // Type metadata
    private static final com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(Subscription.class);

    static {
        typeDesc.setOption("buildNum","cf131037.05");
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("subscriptionID");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriptionID"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("emailsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("accountsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AccountsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("advAccountsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AdvAccountsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("LPAccountsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "LPAccountsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("DOTOAccountsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DOTOAccountsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("BUAccountsPurchased");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BUAccountsPurchased"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("beginDate");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BeginDate"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("endDate");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EndDate"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("notes");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Notes"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("period");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Period"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("notificationTitle");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NotificationTitle"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("notificationMessage");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NotificationMessage"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("notificationFlag");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NotificationFlag"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("notificationExpDate");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NotificationExpDate"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("forAccounting");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ForAccounting"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("hasPurchasedEmails");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "HasPurchasedEmails"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("contractNumber");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContractNumber"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("contractModifier");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContractModifier"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("isRenewal");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "IsRenewal"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberofEmails");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberofEmails"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "long"));
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Subscription_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Subscription_Deser(
            javaType, xmlType, typeDesc);
    };

}
