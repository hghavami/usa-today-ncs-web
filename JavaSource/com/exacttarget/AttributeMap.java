/**
 * AttributeMap.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AttributeMap  extends com.exacttarget.APIProperty  {
    private java.lang.String entityName;
    private java.lang.String columnName;
    private java.lang.String columnNameMappedTo;
    private java.lang.String entityNameMappedTo;
    private com.exacttarget.APIProperty[] additionalData;

    public AttributeMap() {
    }

    public java.lang.String getEntityName() {
        return entityName;
    }

    public void setEntityName(java.lang.String entityName) {
        this.entityName = entityName;
    }

    public java.lang.String getColumnName() {
        return columnName;
    }

    public void setColumnName(java.lang.String columnName) {
        this.columnName = columnName;
    }

    public java.lang.String getColumnNameMappedTo() {
        return columnNameMappedTo;
    }

    public void setColumnNameMappedTo(java.lang.String columnNameMappedTo) {
        this.columnNameMappedTo = columnNameMappedTo;
    }

    public java.lang.String getEntityNameMappedTo() {
        return entityNameMappedTo;
    }

    public void setEntityNameMappedTo(java.lang.String entityNameMappedTo) {
        this.entityNameMappedTo = entityNameMappedTo;
    }

    public com.exacttarget.APIProperty[] getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(com.exacttarget.APIProperty[] additionalData) {
        this.additionalData = additionalData;
    }

    public com.exacttarget.APIProperty getAdditionalData(int i) {
        return this.additionalData[i];
    }

    public void setAdditionalData(int i, com.exacttarget.APIProperty value) {
        this.additionalData[i] = value;
    }

}
