package com.exacttarget;

public class SoapProxy implements com.exacttarget.Soap {
  private boolean _useJNDI = true;
  private String _endpoint = null;
  private com.exacttarget.Soap __soap = null;
  
  public SoapProxy() {
    _initSoapProxy();
  }
  
  private void _initSoapProxy() {
  
    if (_useJNDI) {
      try {
        javax.naming.InitialContext ctx = new javax.naming.InitialContext();
        __soap = ((com.exacttarget.PartnerAPI)ctx.lookup("java:comp/env/service/PartnerAPI")).getSoap();
      }
      catch (javax.naming.NamingException namingException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("javax.naming.NamingException: " + namingException.getMessage());
          namingException.printStackTrace(System.out);
        }
      }
      catch (javax.xml.rpc.ServiceException serviceException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("javax.xml.rpc.ServiceException: " + serviceException.getMessage());
          serviceException.printStackTrace(System.out);
        }
      }
    }
    if (__soap == null) {
      try {
        __soap = (new com.exacttarget.PartnerAPILocator()).getSoap();
        
      }
      catch (javax.xml.rpc.ServiceException serviceException) {
        if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
          System.out.println("javax.xml.rpc.ServiceException: " + serviceException.getMessage());
          serviceException.printStackTrace(System.out);
        }
      }
    }
    if (__soap != null) {
      if (_endpoint != null)
        ((javax.xml.rpc.Stub)__soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
      else
        _endpoint = (String)((javax.xml.rpc.Stub)__soap)._getProperty("javax.xml.rpc.service.endpoint.address");
    }
    
  }
  
  
  public void useJNDI(boolean useJNDI) {
    _useJNDI = useJNDI;
    __soap = null;
    
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (__soap != null)
      ((javax.xml.rpc.Stub)__soap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.exacttarget.CreateResponse create(com.exacttarget.CreateRequest parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.create(parameters);
  }
  
  public com.exacttarget.RetrieveResponseMsg retrieve(com.exacttarget.RetrieveRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.retrieve(parameters);
  }
  
  public com.exacttarget.UpdateResponse update(com.exacttarget.UpdateRequest parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.update(parameters);
  }
  
  public com.exacttarget.DeleteResponse delete(com.exacttarget.DeleteRequest parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.delete(parameters);
  }
  
  public com.exacttarget.QueryResponseMsg query(com.exacttarget.QueryRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.query(parameters);
  }
  
  public com.exacttarget.DefinitionResponseMsg describe(com.exacttarget.DefinitionRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.describe(parameters);
  }
  
  public com.exacttarget.ExecuteResponseMsg execute(com.exacttarget.ExecuteRequest[] parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.execute(parameters);
  }
  
  public com.exacttarget.PerformResponseMsg perform(com.exacttarget.PerformRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.perform(parameters);
  }
  
  public com.exacttarget.ConfigureResponseMsg configure(com.exacttarget.ConfigureRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.configure(parameters);
  }
  
  public com.exacttarget.ScheduleResponseMsg schedule(com.exacttarget.ScheduleRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.schedule(parameters);
  }
  
  public com.exacttarget.VersionInfoResponseMsg versionInfo(com.exacttarget.VersionInfoRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.versionInfo(parameters);
  }
  
  public com.exacttarget.ExtractResponseMsg extract(javax.xml.soap.SOAPElement[] parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.extract(parameters);
  }
  
  public com.exacttarget.SystemStatusResponseMsg getSystemStatus(com.exacttarget.SystemStatusRequestMsg parameters) throws java.rmi.RemoteException{
    if (__soap == null)
      _initSoapProxy();
    return __soap.getSystemStatus(parameters);
  }
  
  
  public com.exacttarget.Soap getSoap() {
    if (__soap == null)
      _initSoapProxy();
    return __soap;
  }
  
}