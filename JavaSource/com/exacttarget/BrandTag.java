/**
 * BrandTag.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class BrandTag  extends com.exacttarget.APIObject  {
    private java.lang.Integer brandID;
    private java.lang.String label;
    private java.lang.String data;

    public BrandTag() {
    }

    public java.lang.Integer getBrandID() {
        return brandID;
    }

    public void setBrandID(java.lang.Integer brandID) {
        this.brandID = brandID;
    }

    public java.lang.String getLabel() {
        return label;
    }

    public void setLabel(java.lang.String label) {
        this.label = label;
    }

    public java.lang.String getData() {
        return data;
    }

    public void setData(java.lang.String data) {
        this.data = data;
    }

}
