/**
 * TriggeredSendSummary_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class TriggeredSendSummary_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public TriggeredSendSummary_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_506;
           context.qName2String(elemQName, true);
           elemQName = QName_0_518;
           context.qName2String(elemQName, true);
           elemQName = QName_0_519;
           context.qName2String(elemQName, true);
           elemQName = QName_0_520;
           context.qName2String(elemQName, true);
           elemQName = QName_0_521;
           context.qName2String(elemQName, true);
           elemQName = QName_0_522;
           context.qName2String(elemQName, true);
           elemQName = QName_0_523;
           context.qName2String(elemQName, true);
           elemQName = QName_0_412;
           context.qName2String(elemQName, true);
           elemQName = QName_0_411;
           context.qName2String(elemQName, true);
           elemQName = QName_0_524;
           context.qName2String(elemQName, true);
           elemQName = QName_0_525;
           context.qName2String(elemQName, true);
           elemQName = QName_0_526;
           context.qName2String(elemQName, true);
           elemQName = QName_0_527;
           context.qName2String(elemQName, true);
           elemQName = QName_0_528;
           context.qName2String(elemQName, true);
           elemQName = QName_0_529;
           context.qName2String(elemQName, true);
           elemQName = QName_0_530;
           context.qName2String(elemQName, true);
           elemQName = QName_0_531;
           context.qName2String(elemQName, true);
           elemQName = QName_0_532;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        TriggeredSendSummary bean = (TriggeredSendSummary) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_506;
          propValue = bean.getTriggeredSendDefinition();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_506,
              false,null,context);
          propQName = QName_0_518;
          propValue = bean.getSent();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_519;
          propValue = bean.getNotSentDueToOptOut();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_520;
          propValue = bean.getNotSentDueToUndeliverable();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_521;
          propValue = bean.getBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_522;
          propValue = bean.getOpens();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_523;
          propValue = bean.getClicks();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_412;
          propValue = bean.getUniqueOpens();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_411;
          propValue = bean.getUniqueClicks();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_524;
          propValue = bean.getOptOuts();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_525;
          propValue = bean.getSurveyResponses();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_526;
          propValue = bean.getFTAFRequests();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_527;
          propValue = bean.getFTAFEmailsSent();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_528;
          propValue = bean.getFTAFOptIns();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_529;
          propValue = bean.getConversions();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_530;
          propValue = bean.getUniqueConversions();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_531;
          propValue = bean.getInProcess();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_532;
          propValue = bean.getNotSentDueToError();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_526 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFRequests");
    private final static javax.xml.namespace.QName QName_0_529 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Conversions");
    private final static javax.xml.namespace.QName QName_0_531 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "InProcess");
    private final static javax.xml.namespace.QName QName_0_525 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SurveyResponses");
    private final static javax.xml.namespace.QName QName_0_532 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToError");
    private final static javax.xml.namespace.QName QName_0_523 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Clicks");
    private final static javax.xml.namespace.QName QName_0_519 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToOptOut");
    private final static javax.xml.namespace.QName QName_1_314 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_0_527 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFEmailsSent");
    private final static javax.xml.namespace.QName QName_0_521 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Bounces");
    private final static javax.xml.namespace.QName QName_0_528 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFOptIns");
    private final static javax.xml.namespace.QName QName_0_524 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OptOuts");
    private final static javax.xml.namespace.QName QName_0_518 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Sent");
    private final static javax.xml.namespace.QName QName_0_411 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueClicks");
    private final static javax.xml.namespace.QName QName_0_522 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Opens");
    private final static javax.xml.namespace.QName QName_0_412 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueOpens");
    private final static javax.xml.namespace.QName QName_0_530 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueConversions");
    private final static javax.xml.namespace.QName QName_0_506 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendDefinition");
    private final static javax.xml.namespace.QName QName_0_520 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToUndeliverable");
}
