/**
 * WeekOfMonthEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class WeekOfMonthEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected WeekOfMonthEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _first = "first";
    public static final java.lang.String _second = "second";
    public static final java.lang.String _third = "third";
    public static final java.lang.String _fourth = "fourth";
    public static final java.lang.String _last = "last";
    public static final WeekOfMonthEnum first = new WeekOfMonthEnum(_first);
    public static final WeekOfMonthEnum second = new WeekOfMonthEnum(_second);
    public static final WeekOfMonthEnum third = new WeekOfMonthEnum(_third);
    public static final WeekOfMonthEnum fourth = new WeekOfMonthEnum(_fourth);
    public static final WeekOfMonthEnum last = new WeekOfMonthEnum(_last);
    public java.lang.String getValue() { return _value_;}
    public static WeekOfMonthEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        WeekOfMonthEnum enumeration = (WeekOfMonthEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static WeekOfMonthEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
