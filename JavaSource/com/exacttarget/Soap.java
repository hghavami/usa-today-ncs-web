/**
 * Soap.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public interface Soap extends java.rmi.Remote {

     // Create objects
    public com.exacttarget.CreateResponse create(com.exacttarget.CreateRequest parameters) throws java.rmi.RemoteException;

     // Retrieve objects
    public com.exacttarget.RetrieveResponseMsg retrieve(com.exacttarget.RetrieveRequestMsg parameters) throws java.rmi.RemoteException;

     // Update objects
    public com.exacttarget.UpdateResponse update(com.exacttarget.UpdateRequest parameters) throws java.rmi.RemoteException;

     // Delete objects
    public com.exacttarget.DeleteResponse delete(com.exacttarget.DeleteRequest parameters) throws java.rmi.RemoteException;
    public com.exacttarget.QueryResponseMsg query(com.exacttarget.QueryRequestMsg parameters) throws java.rmi.RemoteException;
    public com.exacttarget.DefinitionResponseMsg describe(com.exacttarget.DefinitionRequestMsg parameters) throws java.rmi.RemoteException;
    public com.exacttarget.ExecuteResponseMsg execute(com.exacttarget.ExecuteRequest[] parameters) throws java.rmi.RemoteException;
    public com.exacttarget.PerformResponseMsg perform(com.exacttarget.PerformRequestMsg parameters) throws java.rmi.RemoteException;
    public com.exacttarget.ConfigureResponseMsg configure(com.exacttarget.ConfigureRequestMsg parameters) throws java.rmi.RemoteException;
    public com.exacttarget.ScheduleResponseMsg schedule(com.exacttarget.ScheduleRequestMsg parameters) throws java.rmi.RemoteException;
    public com.exacttarget.VersionInfoResponseMsg versionInfo(com.exacttarget.VersionInfoRequestMsg parameters) throws java.rmi.RemoteException;

     // Perform ad hoc data extracts
    public com.exacttarget.ExtractResponseMsg extract(javax.xml.soap.SOAPElement[] parameters) throws java.rmi.RemoteException;

     // Get Current System Status
    public com.exacttarget.SystemStatusResponseMsg getSystemStatus(com.exacttarget.SystemStatusRequestMsg parameters) throws java.rmi.RemoteException;
}
