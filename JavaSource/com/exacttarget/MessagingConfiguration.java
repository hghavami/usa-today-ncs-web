/**
 * MessagingConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MessagingConfiguration  extends com.exacttarget.APIObject  {
    private java.lang.String code;
    private com.exacttarget.MessagingVendorKind messagingVendorKind;
    private boolean isActive;
    private java.lang.String url;
    private java.lang.String userName;
    private java.lang.String password;
    private java.lang.String profileID;
    private java.lang.String callbackUrl;
    private java.lang.String mediaTypes;

    public MessagingConfiguration() {
    }

    public java.lang.String getCode() {
        return code;
    }

    public void setCode(java.lang.String code) {
        this.code = code;
    }

    public com.exacttarget.MessagingVendorKind getMessagingVendorKind() {
        return messagingVendorKind;
    }

    public void setMessagingVendorKind(com.exacttarget.MessagingVendorKind messagingVendorKind) {
        this.messagingVendorKind = messagingVendorKind;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.String getUrl() {
        return url;
    }

    public void setUrl(java.lang.String url) {
        this.url = url;
    }

    public java.lang.String getUserName() {
        return userName;
    }

    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    public java.lang.String getPassword() {
        return password;
    }

    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    public java.lang.String getProfileID() {
        return profileID;
    }

    public void setProfileID(java.lang.String profileID) {
        this.profileID = profileID;
    }

    public java.lang.String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(java.lang.String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public java.lang.String getMediaTypes() {
        return mediaTypes;
    }

    public void setMediaTypes(java.lang.String mediaTypes) {
        this.mediaTypes = mediaTypes;
    }

}
