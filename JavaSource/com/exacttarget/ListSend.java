/**
 * ListSend.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ListSend  extends com.exacttarget.APIObject  {
    private java.lang.Integer sendID;
    private com.exacttarget.List list;
    private java.lang.Integer duplicates;
    private java.lang.Integer invalidAddresses;
    private java.lang.Integer existingUndeliverables;
    private java.lang.Integer existingUnsubscribes;
    private java.lang.Integer hardBounces;
    private java.lang.Integer softBounces;
    private java.lang.Integer otherBounces;
    private java.lang.Integer forwardedEmails;
    private java.lang.Integer uniqueClicks;
    private java.lang.Integer uniqueOpens;
    private java.lang.Integer numberSent;
    private java.lang.Integer numberDelivered;
    private java.lang.Integer unsubscribes;
    private java.lang.Integer missingAddresses;
    private java.lang.String previewURL;
    private com.exacttarget.Link[] links;
    private com.exacttarget.TrackingEvent[] events;

    public ListSend() {
    }

    public java.lang.Integer getSendID() {
        return sendID;
    }

    public void setSendID(java.lang.Integer sendID) {
        this.sendID = sendID;
    }

    public com.exacttarget.List getList() {
        return list;
    }

    public void setList(com.exacttarget.List list) {
        this.list = list;
    }

    public java.lang.Integer getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(java.lang.Integer duplicates) {
        this.duplicates = duplicates;
    }

    public java.lang.Integer getInvalidAddresses() {
        return invalidAddresses;
    }

    public void setInvalidAddresses(java.lang.Integer invalidAddresses) {
        this.invalidAddresses = invalidAddresses;
    }

    public java.lang.Integer getExistingUndeliverables() {
        return existingUndeliverables;
    }

    public void setExistingUndeliverables(java.lang.Integer existingUndeliverables) {
        this.existingUndeliverables = existingUndeliverables;
    }

    public java.lang.Integer getExistingUnsubscribes() {
        return existingUnsubscribes;
    }

    public void setExistingUnsubscribes(java.lang.Integer existingUnsubscribes) {
        this.existingUnsubscribes = existingUnsubscribes;
    }

    public java.lang.Integer getHardBounces() {
        return hardBounces;
    }

    public void setHardBounces(java.lang.Integer hardBounces) {
        this.hardBounces = hardBounces;
    }

    public java.lang.Integer getSoftBounces() {
        return softBounces;
    }

    public void setSoftBounces(java.lang.Integer softBounces) {
        this.softBounces = softBounces;
    }

    public java.lang.Integer getOtherBounces() {
        return otherBounces;
    }

    public void setOtherBounces(java.lang.Integer otherBounces) {
        this.otherBounces = otherBounces;
    }

    public java.lang.Integer getForwardedEmails() {
        return forwardedEmails;
    }

    public void setForwardedEmails(java.lang.Integer forwardedEmails) {
        this.forwardedEmails = forwardedEmails;
    }

    public java.lang.Integer getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(java.lang.Integer uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

    public java.lang.Integer getUniqueOpens() {
        return uniqueOpens;
    }

    public void setUniqueOpens(java.lang.Integer uniqueOpens) {
        this.uniqueOpens = uniqueOpens;
    }

    public java.lang.Integer getNumberSent() {
        return numberSent;
    }

    public void setNumberSent(java.lang.Integer numberSent) {
        this.numberSent = numberSent;
    }

    public java.lang.Integer getNumberDelivered() {
        return numberDelivered;
    }

    public void setNumberDelivered(java.lang.Integer numberDelivered) {
        this.numberDelivered = numberDelivered;
    }

    public java.lang.Integer getUnsubscribes() {
        return unsubscribes;
    }

    public void setUnsubscribes(java.lang.Integer unsubscribes) {
        this.unsubscribes = unsubscribes;
    }

    public java.lang.Integer getMissingAddresses() {
        return missingAddresses;
    }

    public void setMissingAddresses(java.lang.Integer missingAddresses) {
        this.missingAddresses = missingAddresses;
    }

    public java.lang.String getPreviewURL() {
        return previewURL;
    }

    public void setPreviewURL(java.lang.String previewURL) {
        this.previewURL = previewURL;
    }

    public com.exacttarget.Link[] getLinks() {
        return links;
    }

    public void setLinks(com.exacttarget.Link[] links) {
        this.links = links;
    }

    public com.exacttarget.Link getLinks(int i) {
        return this.links[i];
    }

    public void setLinks(int i, com.exacttarget.Link value) {
        this.links[i] = value;
    }

    public com.exacttarget.TrackingEvent[] getEvents() {
        return events;
    }

    public void setEvents(com.exacttarget.TrackingEvent[] events) {
        this.events = events;
    }

    public com.exacttarget.TrackingEvent getEvents(int i) {
        return this.events[i];
    }

    public void setEvents(int i, com.exacttarget.TrackingEvent value) {
        this.events[i] = value;
    }

}
