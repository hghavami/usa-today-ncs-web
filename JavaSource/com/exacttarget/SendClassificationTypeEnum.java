/**
 * SendClassificationTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendClassificationTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SendClassificationTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Operational = "Operational";
    public static final java.lang.String _Marketing = "Marketing";
    public static final SendClassificationTypeEnum Operational = new SendClassificationTypeEnum(_Operational);
    public static final SendClassificationTypeEnum Marketing = new SendClassificationTypeEnum(_Marketing);
    public java.lang.String getValue() { return _value_;}
    public static SendClassificationTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SendClassificationTypeEnum enumeration = (SendClassificationTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SendClassificationTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
