/**
 * LogicalOperators.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class LogicalOperators  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LogicalOperators(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _OR = "OR";
    public static final java.lang.String _AND = "AND";
    public static final LogicalOperators OR = new LogicalOperators(_OR);
    public static final LogicalOperators AND = new LogicalOperators(_AND);
    public java.lang.String getValue() { return _value_;}
    public static LogicalOperators fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LogicalOperators enumeration = (LogicalOperators)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LogicalOperators fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
