/**
 * SubscriberSendResult_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SubscriberSendResult_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public SubscriberSendResult_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_511;
           context.qName2String(elemQName, true);
           elemQName = QName_0_178;
           context.qName2String(elemQName, true);
           elemQName = QName_0_214;
           context.qName2String(elemQName, true);
           elemQName = QName_0_512;
           context.qName2String(elemQName, true);
           elemQName = QName_0_513;
           context.qName2String(elemQName, true);
           elemQName = QName_0_514;
           context.qName2String(elemQName, true);
           elemQName = QName_0_420;
           context.qName2String(elemQName, true);
           elemQName = QName_0_515;
           context.qName2String(elemQName, true);
           elemQName = QName_0_516;
           context.qName2String(elemQName, true);
           elemQName = QName_0_23;
           context.qName2String(elemQName, true);
           elemQName = QName_0_22;
           context.qName2String(elemQName, true);
           elemQName = QName_0_437;
           context.qName2String(elemQName, true);
           elemQName = QName_0_411;
           context.qName2String(elemQName, true);
           elemQName = QName_0_186;
           context.qName2String(elemQName, true);
           elemQName = QName_0_517;
           context.qName2String(elemQName, true);
           elemQName = QName_0_407;
           context.qName2String(elemQName, true);
           elemQName = QName_0_408;
           context.qName2String(elemQName, true);
           elemQName = QName_0_409;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        SubscriberSendResult bean = (SubscriberSendResult) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_511;
          propValue = bean.getSend();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_511,
              false,null,context);
          propQName = QName_0_178;
          propValue = bean.getEmail();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_178,
              false,null,context);
          propQName = QName_0_214;
          propValue = bean.getSubscriber();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_214,
              false,null,context);
          propQName = QName_0_512;
          propValue = bean.getClickDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_513;
          propValue = bean.getBounceDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_514;
          propValue = bean.getOpenDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_420;
          propValue = bean.getSentDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_515;
          propValue = bean.getLastAction();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_516;
          propValue = bean.getUnsubscribeDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_23;
          propValue = bean.getFromAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_22;
          propValue = bean.getFromName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_437;
          propValue = bean.getTotalClicks();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_411;
          propValue = bean.getUniqueClicks();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_186;
          propValue = bean.getSubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_517;
          propValue = bean.getViewSentEmailURL();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_407;
          propValue = bean.getHardBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_408;
          propValue = bean.getSoftBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_409;
          propValue = bean.getOtherBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_437 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TotalClicks");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_186 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subject");
    private final static javax.xml.namespace.QName QName_0_407 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HardBounces");
    private final static javax.xml.namespace.QName QName_0_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromName");
    private final static javax.xml.namespace.QName QName_0_516 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UnsubscribeDate");
    private final static javax.xml.namespace.QName QName_0_408 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SoftBounces");
    private final static javax.xml.namespace.QName QName_0_214 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscriber");
    private final static javax.xml.namespace.QName QName_0_511 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Send");
    private final static javax.xml.namespace.QName QName_0_23 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromAddress");
    private final static javax.xml.namespace.QName QName_0_420 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SentDate");
    private final static javax.xml.namespace.QName QName_0_512 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ClickDate");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_1_13 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "dateTime");
    private final static javax.xml.namespace.QName QName_0_513 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BounceDate");
    private final static javax.xml.namespace.QName QName_0_411 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueClicks");
    private final static javax.xml.namespace.QName QName_0_514 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OpenDate");
    private final static javax.xml.namespace.QName QName_0_409 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OtherBounces");
    private final static javax.xml.namespace.QName QName_0_515 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LastAction");
    private final static javax.xml.namespace.QName QName_0_517 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ViewSentEmailURL");
}
