/**
 * SendDefinitionList.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendDefinitionList  extends com.exacttarget.AudienceItem  {
    private com.exacttarget.FilterDefinition filterDefinition;
    private java.lang.Boolean isTestObject;
    private java.lang.String salesForceObjectID;
    private java.lang.String name;
    private com.exacttarget.APIProperty[] parameters;

    public SendDefinitionList() {
    }

    public com.exacttarget.FilterDefinition getFilterDefinition() {
        return filterDefinition;
    }

    public void setFilterDefinition(com.exacttarget.FilterDefinition filterDefinition) {
        this.filterDefinition = filterDefinition;
    }

    public java.lang.Boolean getIsTestObject() {
        return isTestObject;
    }

    public void setIsTestObject(java.lang.Boolean isTestObject) {
        this.isTestObject = isTestObject;
    }

    public java.lang.String getSalesForceObjectID() {
        return salesForceObjectID;
    }

    public void setSalesForceObjectID(java.lang.String salesForceObjectID) {
        this.salesForceObjectID = salesForceObjectID;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public com.exacttarget.APIProperty[] getParameters() {
        return parameters;
    }

    public void setParameters(com.exacttarget.APIProperty[] parameters) {
        this.parameters = parameters;
    }

}
