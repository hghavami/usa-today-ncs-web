/**
 * PrivateIP.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PrivateIP  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.Boolean isActive;
    private java.lang.Short ordinalID;
    private java.lang.String IPAddress;

    public PrivateIP() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.Short getOrdinalID() {
        return ordinalID;
    }

    public void setOrdinalID(java.lang.Short ordinalID) {
        this.ordinalID = ordinalID;
    }

    public java.lang.String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(java.lang.String IPAddress) {
        this.IPAddress = IPAddress;
    }

}
