/**
 * Folder.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Folder  extends com.exacttarget.APIProperty  {
    private int ID;
    private java.lang.Integer parentID;

    public Folder() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public java.lang.Integer getParentID() {
        return parentID;
    }

    public void setParentID(java.lang.Integer parentID) {
        this.parentID = parentID;
    }

}
