/**
 * FileTrigger_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class FileTrigger_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public FileTrigger_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new FileTrigger();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_766) {
          ((FileTrigger)value).setExternalReference(strValue);
          return true;}
        else if (qName==QName_0_346) {
          ((FileTrigger)value).setType(strValue);
          return true;}
        else if (qName==QName_0_150) {
          ((FileTrigger)value).setStatus(strValue);
          return true;}
        else if (qName==QName_0_34) {
          ((FileTrigger)value).setStatusMessage(strValue);
          return true;}
        else if (qName==QName_0_767) {
          ((FileTrigger)value).setRequestParameterDetail(strValue);
          return true;}
        else if (qName==QName_0_768) {
          ((FileTrigger)value).setResponseControlManifest(strValue);
          return true;}
        else if (qName==QName_0_634) {
          ((FileTrigger)value).setFileName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((FileTrigger)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_20) {
          ((FileTrigger)value).setName(strValue);
          return true;}
        else if (qName==QName_0_769) {
          ((FileTrigger)value).setLastPullDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_770) {
          ((FileTrigger)value).setScheduledDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_187) {
          ((FileTrigger)value).setIsActive(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_771) {
          ((FileTrigger)value).setFileTriggerProgramID(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_770 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledDate");
    private final static javax.xml.namespace.QName QName_0_767 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RequestParameterDetail");
    private final static javax.xml.namespace.QName QName_0_771 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileTriggerProgramID");
    private final static javax.xml.namespace.QName QName_0_766 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExternalReference");
    private final static javax.xml.namespace.QName QName_0_187 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsActive");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_346 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Type");
    private final static javax.xml.namespace.QName QName_0_634 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileName");
    private final static javax.xml.namespace.QName QName_0_769 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LastPullDate");
    private final static javax.xml.namespace.QName QName_0_34 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "StatusMessage");
    private final static javax.xml.namespace.QName QName_0_768 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ResponseControlManifest");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
}
