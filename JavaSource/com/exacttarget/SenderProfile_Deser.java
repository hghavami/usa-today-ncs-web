/**
 * SenderProfile_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SenderProfile_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public SenderProfile_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new SenderProfile();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((SenderProfile)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((SenderProfile)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_22) {
          ((SenderProfile)value).setFromName(strValue);
          return true;}
        else if (qName==QName_0_23) {
          ((SenderProfile)value).setFromAddress(strValue);
          return true;}
        else if (qName==QName_0_542) {
          ((SenderProfile)value).setUseDefaultRMMRules(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_543) {
          ((SenderProfile)value).setAutoForwardToEmailAddress(strValue);
          return true;}
        else if (qName==QName_0_544) {
          ((SenderProfile)value).setAutoForwardToName(strValue);
          return true;}
        else if (qName==QName_0_545) {
          ((SenderProfile)value).setDirectForward(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_547) {
          ((SenderProfile)value).setAutoReply(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_549) {
          ((SenderProfile)value).setSenderHeaderEmailAddress(strValue);
          return true;}
        else if (qName==QName_0_550) {
          ((SenderProfile)value).setSenderHeaderName(strValue);
          return true;}
        else if (qName==QName_0_489) {
          ((SenderProfile)value).setDataRetentionPeriodLength(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseShort(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_546) {
          ((SenderProfile)value).setAutoForwardTriggeredSend((com.exacttarget.TriggeredSendDefinition)objValue);
          return true;}
        else if (qName==QName_0_548) {
          ((SenderProfile)value).setAutoReplyTriggeredSend((com.exacttarget.TriggeredSendDefinition)objValue);
          return true;}
        else if (qName==QName_0_490) {
          ((SenderProfile)value).setDataRetentionPeriodUnitOfMeasure((com.exacttarget.RecurrenceTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_551) {
          ((SenderProfile)value).setReplyManagementRuleSet((com.exacttarget.APIObject)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_549 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SenderHeaderEmailAddress");
    private final static javax.xml.namespace.QName QName_0_489 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodLength");
    private final static javax.xml.namespace.QName QName_0_551 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ReplyManagementRuleSet");
    private final static javax.xml.namespace.QName QName_0_23 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromAddress");
    private final static javax.xml.namespace.QName QName_0_490 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodUnitOfMeasure");
    private final static javax.xml.namespace.QName QName_0_548 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplyTriggeredSend");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_547 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReply");
    private final static javax.xml.namespace.QName QName_0_550 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SenderHeaderName");
    private final static javax.xml.namespace.QName QName_0_543 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardToEmailAddress");
    private final static javax.xml.namespace.QName QName_0_544 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardToName");
    private final static javax.xml.namespace.QName QName_0_546 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardTriggeredSend");
    private final static javax.xml.namespace.QName QName_0_542 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UseDefaultRMMRules");
    private final static javax.xml.namespace.QName QName_0_545 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DirectForward");
    private final static javax.xml.namespace.QName QName_0_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromName");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
}
