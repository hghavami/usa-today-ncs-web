/**
 * MonthOfYearEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MonthOfYearEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MonthOfYearEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _January = "January";
    public static final java.lang.String _February = "February";
    public static final java.lang.String _March = "March";
    public static final java.lang.String _April = "April";
    public static final java.lang.String _May = "May";
    public static final java.lang.String _June = "June";
    public static final java.lang.String _July = "July";
    public static final java.lang.String _August = "August";
    public static final java.lang.String _September = "September";
    public static final java.lang.String _October = "October";
    public static final java.lang.String _November = "November";
    public static final java.lang.String _December = "December";
    public static final MonthOfYearEnum January = new MonthOfYearEnum(_January);
    public static final MonthOfYearEnum February = new MonthOfYearEnum(_February);
    public static final MonthOfYearEnum March = new MonthOfYearEnum(_March);
    public static final MonthOfYearEnum April = new MonthOfYearEnum(_April);
    public static final MonthOfYearEnum May = new MonthOfYearEnum(_May);
    public static final MonthOfYearEnum June = new MonthOfYearEnum(_June);
    public static final MonthOfYearEnum July = new MonthOfYearEnum(_July);
    public static final MonthOfYearEnum August = new MonthOfYearEnum(_August);
    public static final MonthOfYearEnum September = new MonthOfYearEnum(_September);
    public static final MonthOfYearEnum October = new MonthOfYearEnum(_October);
    public static final MonthOfYearEnum November = new MonthOfYearEnum(_November);
    public static final MonthOfYearEnum December = new MonthOfYearEnum(_December);
    public java.lang.String getValue() { return _value_;}
    public static MonthOfYearEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        MonthOfYearEnum enumeration = (MonthOfYearEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static MonthOfYearEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
