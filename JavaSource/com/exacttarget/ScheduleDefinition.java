/**
 * ScheduleDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ScheduleDefinition  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private com.exacttarget.Recurrence recurrence;
    private com.exacttarget.RecurrenceTypeEnum recurrenceType;
    private com.exacttarget.RecurrenceRangeTypeEnum recurrenceRangeType;
    private java.util.Calendar startDateTime;
    private java.util.Calendar endDateTime;
    private java.lang.Integer occurrences;
    private java.lang.String keyword;
    private com.exacttarget.TimeZone timeZone;

    public ScheduleDefinition() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.Recurrence getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(com.exacttarget.Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    public com.exacttarget.RecurrenceTypeEnum getRecurrenceType() {
        return recurrenceType;
    }

    public void setRecurrenceType(com.exacttarget.RecurrenceTypeEnum recurrenceType) {
        this.recurrenceType = recurrenceType;
    }

    public com.exacttarget.RecurrenceRangeTypeEnum getRecurrenceRangeType() {
        return recurrenceRangeType;
    }

    public void setRecurrenceRangeType(com.exacttarget.RecurrenceRangeTypeEnum recurrenceRangeType) {
        this.recurrenceRangeType = recurrenceRangeType;
    }

    public java.util.Calendar getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(java.util.Calendar startDateTime) {
        this.startDateTime = startDateTime;
    }

    public java.util.Calendar getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(java.util.Calendar endDateTime) {
        this.endDateTime = endDateTime;
    }

    public java.lang.Integer getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(java.lang.Integer occurrences) {
        this.occurrences = occurrences;
    }

    public java.lang.String getKeyword() {
        return keyword;
    }

    public void setKeyword(java.lang.String keyword) {
        this.keyword = keyword;
    }

    public com.exacttarget.TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(com.exacttarget.TimeZone timeZone) {
        this.timeZone = timeZone;
    }

}
