/**
 * PerformOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PerformOptions  extends com.exacttarget.Options  {
    private java.lang.String explanation;

    public PerformOptions() {
    }

    public java.lang.String getExplanation() {
        return explanation;
    }

    public void setExplanation(java.lang.String explanation) {
        this.explanation = explanation;
    }

}
