/**
 * MonthlyRecurrencePatternTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MonthlyRecurrencePatternTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MonthlyRecurrencePatternTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _ByDay = "ByDay";
    public static final java.lang.String _ByWeek = "ByWeek";
    public static final MonthlyRecurrencePatternTypeEnum ByDay = new MonthlyRecurrencePatternTypeEnum(_ByDay);
    public static final MonthlyRecurrencePatternTypeEnum ByWeek = new MonthlyRecurrencePatternTypeEnum(_ByWeek);
    public java.lang.String getValue() { return _value_;}
    public static MonthlyRecurrencePatternTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        MonthlyRecurrencePatternTypeEnum enumeration = (MonthlyRecurrencePatternTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static MonthlyRecurrencePatternTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
