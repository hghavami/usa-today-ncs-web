/**
 * YearlyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class YearlyRecurrence  extends com.exacttarget.Recurrence  {
    private com.exacttarget.YearlyRecurrencePatternTypeEnum yearlyRecurrencePatternType;
    private java.lang.Integer scheduledDay;
    private com.exacttarget.WeekOfMonthEnum scheduledWeek;
    private com.exacttarget.MonthOfYearEnum scheduledMonth;
    private com.exacttarget.DayOfWeekEnum scheduledDayOfWeek;

    public YearlyRecurrence() {
    }

    public com.exacttarget.YearlyRecurrencePatternTypeEnum getYearlyRecurrencePatternType() {
        return yearlyRecurrencePatternType;
    }

    public void setYearlyRecurrencePatternType(com.exacttarget.YearlyRecurrencePatternTypeEnum yearlyRecurrencePatternType) {
        this.yearlyRecurrencePatternType = yearlyRecurrencePatternType;
    }

    public java.lang.Integer getScheduledDay() {
        return scheduledDay;
    }

    public void setScheduledDay(java.lang.Integer scheduledDay) {
        this.scheduledDay = scheduledDay;
    }

    public com.exacttarget.WeekOfMonthEnum getScheduledWeek() {
        return scheduledWeek;
    }

    public void setScheduledWeek(com.exacttarget.WeekOfMonthEnum scheduledWeek) {
        this.scheduledWeek = scheduledWeek;
    }

    public com.exacttarget.MonthOfYearEnum getScheduledMonth() {
        return scheduledMonth;
    }

    public void setScheduledMonth(com.exacttarget.MonthOfYearEnum scheduledMonth) {
        this.scheduledMonth = scheduledMonth;
    }

    public com.exacttarget.DayOfWeekEnum getScheduledDayOfWeek() {
        return scheduledDayOfWeek;
    }

    public void setScheduledDayOfWeek(com.exacttarget.DayOfWeekEnum scheduledDayOfWeek) {
        this.scheduledDayOfWeek = scheduledDayOfWeek;
    }

}
