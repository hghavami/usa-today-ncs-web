/**
 * Subscriber_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Subscriber_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public Subscriber_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new Subscriber();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_195) {
          ((Subscriber)value).setEmailAddress(strValue);
          return true;}
        else if (qName==QName_0_197) {
          ((Subscriber)value).setSubscriberKey(strValue);
          return true;}
        else if (qName==QName_0_198) {
          ((Subscriber)value).setUnsubscribedDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_199) {
          ((Subscriber)value).setPartnerType(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_150) {
          ((Subscriber)value).setStatus((com.exacttarget.SubscriberStatus)objValue);
          return true;}
        else if (qName==QName_0_200) {
          ((Subscriber)value).setEmailTypePreference((com.exacttarget.EmailType)objValue);
          return true;}
        else if (qName==QName_0_202) {
          ((Subscriber)value).setGlobalUnsubscribeCategory((com.exacttarget.GlobalUnsubscribeCategory)objValue);
          return true;}
        else if (qName==QName_0_203) {
          ((Subscriber)value).setSubscriberTypeDefinition((com.exacttarget.SubscriberTypeDefinition)objValue);
          return true;}
        else if (qName==QName_0_204) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.SubscriberAddress[] array = new com.exacttarget.SubscriberAddress[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((Subscriber)value).setAddresses(array);
          } else { 
            ((Subscriber)value).setAddresses((com.exacttarget.SubscriberAddress[])objValue);}
          return true;}
        else if (qName==QName_0_205) {
          ((Subscriber)value).setPrimarySMSAddress((com.exacttarget.SMSAddress)objValue);
          return true;}
        else if (qName==QName_0_206) {
          ((Subscriber)value).setPrimarySMSPublicationStatus((com.exacttarget.SubscriberAddressStatus)objValue);
          return true;}
        else if (qName==QName_0_207) {
          ((Subscriber)value).setPrimaryEmailAddress((com.exacttarget.EmailAddress)objValue);
          return true;}
        else if (qName==QName_0_679) {
          ((Subscriber)value).setLocale((com.exacttarget.Locale)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_196) {
          com.exacttarget.Attribute[] array = new com.exacttarget.Attribute[listValue.size()];
          listValue.toArray(array);
          ((Subscriber)value).setAttributes(array);
          return true;}
        else if (qName==QName_0_201) {
          com.exacttarget.SubscriberList[] array = new com.exacttarget.SubscriberList[listValue.size()];
          listValue.toArray(array);
          ((Subscriber)value).setLists(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_195 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailAddress");
    private final static javax.xml.namespace.QName QName_0_204 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Addresses");
    private final static javax.xml.namespace.QName QName_0_200 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailTypePreference");
    private final static javax.xml.namespace.QName QName_0_207 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimaryEmailAddress");
    private final static javax.xml.namespace.QName QName_0_201 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Lists");
    private final static javax.xml.namespace.QName QName_0_205 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimarySMSAddress");
    private final static javax.xml.namespace.QName QName_0_199 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerType");
    private final static javax.xml.namespace.QName QName_0_198 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UnsubscribedDate");
    private final static javax.xml.namespace.QName QName_0_197 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberKey");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_203 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberTypeDefinition");
    private final static javax.xml.namespace.QName QName_0_196 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Attributes");
    private final static javax.xml.namespace.QName QName_0_206 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimarySMSPublicationStatus");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_202 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "GlobalUnsubscribeCategory");
}
