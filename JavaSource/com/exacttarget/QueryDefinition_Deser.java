/**
 * QueryDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class QueryDefinition_Deser extends com.exacttarget.InteractionDefinition_Deser {
    /**
     * Constructor
     */
    public QueryDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new QueryDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_646) {
          ((QueryDefinition)value).setQueryText(strValue);
          return true;}
        else if (qName==QName_0_647) {
          ((QueryDefinition)value).setTargetType(strValue);
          return true;}
        else if (qName==QName_0_649) {
          ((QueryDefinition)value).setTargetUpdateType(strValue);
          return true;}
        else if (qName==QName_0_597) {
          ((QueryDefinition)value).setFileSpec(strValue);
          return true;}
        else if (qName==QName_0_598) {
          ((QueryDefinition)value).setFileType(strValue);
          return true;}
        else if (qName==QName_0_150) {
          ((QueryDefinition)value).setStatus(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_648) {
          ((QueryDefinition)value).setDataExtensionTarget((com.exacttarget.InteractionBaseObject)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_597 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileSpec");
    private final static javax.xml.namespace.QName QName_0_649 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TargetUpdateType");
    private final static javax.xml.namespace.QName QName_0_646 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "QueryText");
    private final static javax.xml.namespace.QName QName_0_598 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileType");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_648 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataExtensionTarget");
    private final static javax.xml.namespace.QName QName_0_647 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TargetType");
}
