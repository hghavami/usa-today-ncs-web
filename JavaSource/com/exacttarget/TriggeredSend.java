/**
 * TriggeredSend.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TriggeredSend  extends com.exacttarget.APIObject  {
    private com.exacttarget.TriggeredSendDefinition triggeredSendDefinition;
    private com.exacttarget.Subscriber[] subscribers;
    private com.exacttarget.Attribute[] attributes;

    public TriggeredSend() {
    }

    public com.exacttarget.TriggeredSendDefinition getTriggeredSendDefinition() {
        return triggeredSendDefinition;
    }

    public void setTriggeredSendDefinition(com.exacttarget.TriggeredSendDefinition triggeredSendDefinition) {
        this.triggeredSendDefinition = triggeredSendDefinition;
    }

    public com.exacttarget.Subscriber[] getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(com.exacttarget.Subscriber[] subscribers) {
        this.subscribers = subscribers;
    }

    public com.exacttarget.Subscriber getSubscribers(int i) {
        return this.subscribers[i];
    }

    public void setSubscribers(int i, com.exacttarget.Subscriber value) {
        this.subscribers[i] = value;
    }

    public com.exacttarget.Attribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(com.exacttarget.Attribute[] attributes) {
        this.attributes = attributes;
    }

    public com.exacttarget.Attribute getAttributes(int i) {
        return this.attributes[i];
    }

    public void setAttributes(int i, com.exacttarget.Attribute value) {
        this.attributes[i] = value;
    }

}
