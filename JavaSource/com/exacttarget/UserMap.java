/**
 * UserMap.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UserMap  extends com.exacttarget.APIProperty  {
    private com.exacttarget.AccountUser ETAccountUser;
    private com.exacttarget.APIProperty[] additionalData;

    public UserMap() {
    }

    public com.exacttarget.AccountUser getETAccountUser() {
        return ETAccountUser;
    }

    public void setETAccountUser(com.exacttarget.AccountUser ETAccountUser) {
        this.ETAccountUser = ETAccountUser;
    }

    public com.exacttarget.APIProperty[] getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(com.exacttarget.APIProperty[] additionalData) {
        this.additionalData = additionalData;
    }

    public com.exacttarget.APIProperty getAdditionalData(int i) {
        return this.additionalData[i];
    }

    public void setAdditionalData(int i, com.exacttarget.APIProperty value) {
        this.additionalData[i] = value;
    }

}
