/**
 * ResourceSpecification.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ResourceSpecification  extends com.exacttarget.APIObject  {
    private java.lang.String URN;
    private com.exacttarget.Authentication authentication;

    public ResourceSpecification() {
    }

    public java.lang.String getURN() {
        return URN;
    }

    public void setURN(java.lang.String URN) {
        this.URN = URN;
    }

    public com.exacttarget.Authentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(com.exacttarget.Authentication authentication) {
        this.authentication = authentication;
    }

}
