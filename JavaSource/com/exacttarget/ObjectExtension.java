/**
 * ObjectExtension.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ObjectExtension  extends com.exacttarget.APIObject  {
    private java.lang.String type;
    private com.exacttarget.APIProperty[] properties;

    public ObjectExtension() {
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public com.exacttarget.APIProperty[] getProperties() {
        return properties;
    }

    public void setProperties(com.exacttarget.APIProperty[] properties) {
        this.properties = properties;
    }

}
