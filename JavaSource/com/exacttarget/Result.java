/**
 * Result.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Result  {
    private java.lang.String statusCode;
    private java.lang.String statusMessage;
    private java.lang.Integer ordinalID;
    private java.lang.Integer errorCode;
    private java.lang.String requestID;
    private java.lang.String conversationID;
    private java.lang.String overallStatusCode;
    private com.exacttarget.RequestType requestType;
    private java.lang.String resultType;
    private java.lang.String resultDetailXML;

    public Result() {
    }

    public java.lang.String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }

    public java.lang.String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(java.lang.String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public java.lang.Integer getOrdinalID() {
        return ordinalID;
    }

    public void setOrdinalID(java.lang.Integer ordinalID) {
        this.ordinalID = ordinalID;
    }

    public java.lang.Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(java.lang.Integer errorCode) {
        this.errorCode = errorCode;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

    public java.lang.String getConversationID() {
        return conversationID;
    }

    public void setConversationID(java.lang.String conversationID) {
        this.conversationID = conversationID;
    }

    public java.lang.String getOverallStatusCode() {
        return overallStatusCode;
    }

    public void setOverallStatusCode(java.lang.String overallStatusCode) {
        this.overallStatusCode = overallStatusCode;
    }

    public com.exacttarget.RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(com.exacttarget.RequestType requestType) {
        this.requestType = requestType;
    }

    public java.lang.String getResultType() {
        return resultType;
    }

    public void setResultType(java.lang.String resultType) {
        this.resultType = resultType;
    }

    public java.lang.String getResultDetailXML() {
        return resultDetailXML;
    }

    public void setResultDetailXML(java.lang.String resultDetailXML) {
        this.resultDetailXML = resultDetailXML;
    }

}
