/**
 * ListTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ListTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ListTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Public = "Public";
    public static final java.lang.String _Private = "Private";
    public static final java.lang.String _SalesForce = "SalesForce";
    public static final java.lang.String _GlobalUnsubscribe = "GlobalUnsubscribe";
    public static final java.lang.String _Master = "Master";
    public static final ListTypeEnum Public = new ListTypeEnum(_Public);
    public static final ListTypeEnum Private = new ListTypeEnum(_Private);
    public static final ListTypeEnum SalesForce = new ListTypeEnum(_SalesForce);
    public static final ListTypeEnum GlobalUnsubscribe = new ListTypeEnum(_GlobalUnsubscribe);
    public static final ListTypeEnum Master = new ListTypeEnum(_Master);
    public java.lang.String getValue() { return _value_;}
    public static ListTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ListTypeEnum enumeration = (ListTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ListTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
