/**
 * TriggeredSendStatusEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TriggeredSendStatusEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected TriggeredSendStatusEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _New = "New";
    public static final java.lang.String _Inactive = "Inactive";
    public static final java.lang.String _Active = "Active";
    public static final java.lang.String _Canceled = "Canceled";
    public static final java.lang.String _Deleted = "Deleted";
    public static final TriggeredSendStatusEnum New = new TriggeredSendStatusEnum(_New);
    public static final TriggeredSendStatusEnum Inactive = new TriggeredSendStatusEnum(_Inactive);
    public static final TriggeredSendStatusEnum Active = new TriggeredSendStatusEnum(_Active);
    public static final TriggeredSendStatusEnum Canceled = new TriggeredSendStatusEnum(_Canceled);
    public static final TriggeredSendStatusEnum Deleted = new TriggeredSendStatusEnum(_Deleted);
    public java.lang.String getValue() { return _value_;}
    public static TriggeredSendStatusEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        TriggeredSendStatusEnum enumeration = (TriggeredSendStatusEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static TriggeredSendStatusEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
