/**
 * PartnerAPI.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public interface PartnerAPI extends javax.xml.rpc.Service {

     // ExactTarget Partner API
    public com.exacttarget.Soap getSoap() throws javax.xml.rpc.ServiceException;

    public java.lang.String getSoapAddress();

    public com.exacttarget.Soap getSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
