/**
 * SubscriberAddressStatus.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberAddressStatus  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SubscriberAddressStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _OptedIn = "OptedIn";
    public static final java.lang.String _OptedOut = "OptedOut";
    public static final java.lang.String _InActive = "InActive";
    public static final SubscriberAddressStatus OptedIn = new SubscriberAddressStatus(_OptedIn);
    public static final SubscriberAddressStatus OptedOut = new SubscriberAddressStatus(_OptedOut);
    public static final SubscriberAddressStatus InActive = new SubscriberAddressStatus(_InActive);
    public java.lang.String getValue() { return _value_;}
    public static SubscriberAddressStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SubscriberAddressStatus enumeration = (SubscriberAddressStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SubscriberAddressStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
