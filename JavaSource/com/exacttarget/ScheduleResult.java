/**
 * ScheduleResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ScheduleResult  extends com.exacttarget.Result  {
    private com.exacttarget.ScheduleDefinition object;
    private com.exacttarget.TaskResult task;

    public ScheduleResult() {
    }

    public com.exacttarget.ScheduleDefinition getObject() {
        return object;
    }

    public void setObject(com.exacttarget.ScheduleDefinition object) {
        this.object = object;
    }

    public com.exacttarget.TaskResult getTask() {
        return task;
    }

    public void setTask(com.exacttarget.TaskResult task) {
        this.task = task;
    }

}
