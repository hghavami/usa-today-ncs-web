/**
 * PlatformApplication.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PlatformApplication  extends com.exacttarget.APIObject  {
    private com.exacttarget.PlatformApplicationPackage _package;
    private com.exacttarget.PlatformApplicationPackage[] packages;
    private com.exacttarget.ResourceSpecification resourceSpecification;
    private java.lang.String developerVersion;

    public PlatformApplication() {
    }

    public com.exacttarget.PlatformApplicationPackage get_package() {
        return _package;
    }

    public void set_package(com.exacttarget.PlatformApplicationPackage _package) {
        this._package = _package;
    }

    public com.exacttarget.PlatformApplicationPackage[] getPackages() {
        return packages;
    }

    public void setPackages(com.exacttarget.PlatformApplicationPackage[] packages) {
        this.packages = packages;
    }

    public com.exacttarget.PlatformApplicationPackage getPackages(int i) {
        return this.packages[i];
    }

    public void setPackages(int i, com.exacttarget.PlatformApplicationPackage value) {
        this.packages[i] = value;
    }

    public com.exacttarget.ResourceSpecification getResourceSpecification() {
        return resourceSpecification;
    }

    public void setResourceSpecification(com.exacttarget.ResourceSpecification resourceSpecification) {
        this.resourceSpecification = resourceSpecification;
    }

    public java.lang.String getDeveloperVersion() {
        return developerVersion;
    }

    public void setDeveloperVersion(java.lang.String developerVersion) {
        this.developerVersion = developerVersion;
    }

}
