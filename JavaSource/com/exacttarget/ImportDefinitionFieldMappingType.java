/**
 * ImportDefinitionFieldMappingType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ImportDefinitionFieldMappingType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ImportDefinitionFieldMappingType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _InferFromColumnHeadings = "InferFromColumnHeadings";
    public static final java.lang.String _MapByOrdinal = "MapByOrdinal";
    public static final java.lang.String _ManualMap = "ManualMap";
    public static final ImportDefinitionFieldMappingType InferFromColumnHeadings = new ImportDefinitionFieldMappingType(_InferFromColumnHeadings);
    public static final ImportDefinitionFieldMappingType MapByOrdinal = new ImportDefinitionFieldMappingType(_MapByOrdinal);
    public static final ImportDefinitionFieldMappingType ManualMap = new ImportDefinitionFieldMappingType(_ManualMap);
    public java.lang.String getValue() { return _value_;}
    public static ImportDefinitionFieldMappingType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ImportDefinitionFieldMappingType enumeration = (ImportDefinitionFieldMappingType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ImportDefinitionFieldMappingType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
