/**
 * DeliveryProfile.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DeliveryProfile  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private com.exacttarget.DeliveryProfileSourceAddressTypeEnum sourceAddressType;
    private com.exacttarget.PrivateIP privateIP;
    private com.exacttarget.DeliveryProfileDomainTypeEnum domainType;
    private com.exacttarget.PrivateDomain privateDomain;
    private com.exacttarget.SalutationSourceEnum headerSalutationSource;
    private com.exacttarget.ContentArea headerContentArea;
    private com.exacttarget.SalutationSourceEnum footerSalutationSource;
    private com.exacttarget.ContentArea footerContentArea;
    private java.lang.Boolean subscriberLevelPrivateDomain;
    private com.exacttarget.Certificate SMIMESignatureCertificate;
    private com.exacttarget.PrivateDomainSet privateDomainSet;

    public DeliveryProfile() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.DeliveryProfileSourceAddressTypeEnum getSourceAddressType() {
        return sourceAddressType;
    }

    public void setSourceAddressType(com.exacttarget.DeliveryProfileSourceAddressTypeEnum sourceAddressType) {
        this.sourceAddressType = sourceAddressType;
    }

    public com.exacttarget.PrivateIP getPrivateIP() {
        return privateIP;
    }

    public void setPrivateIP(com.exacttarget.PrivateIP privateIP) {
        this.privateIP = privateIP;
    }

    public com.exacttarget.DeliveryProfileDomainTypeEnum getDomainType() {
        return domainType;
    }

    public void setDomainType(com.exacttarget.DeliveryProfileDomainTypeEnum domainType) {
        this.domainType = domainType;
    }

    public com.exacttarget.PrivateDomain getPrivateDomain() {
        return privateDomain;
    }

    public void setPrivateDomain(com.exacttarget.PrivateDomain privateDomain) {
        this.privateDomain = privateDomain;
    }

    public com.exacttarget.SalutationSourceEnum getHeaderSalutationSource() {
        return headerSalutationSource;
    }

    public void setHeaderSalutationSource(com.exacttarget.SalutationSourceEnum headerSalutationSource) {
        this.headerSalutationSource = headerSalutationSource;
    }

    public com.exacttarget.ContentArea getHeaderContentArea() {
        return headerContentArea;
    }

    public void setHeaderContentArea(com.exacttarget.ContentArea headerContentArea) {
        this.headerContentArea = headerContentArea;
    }

    public com.exacttarget.SalutationSourceEnum getFooterSalutationSource() {
        return footerSalutationSource;
    }

    public void setFooterSalutationSource(com.exacttarget.SalutationSourceEnum footerSalutationSource) {
        this.footerSalutationSource = footerSalutationSource;
    }

    public com.exacttarget.ContentArea getFooterContentArea() {
        return footerContentArea;
    }

    public void setFooterContentArea(com.exacttarget.ContentArea footerContentArea) {
        this.footerContentArea = footerContentArea;
    }

    public java.lang.Boolean getSubscriberLevelPrivateDomain() {
        return subscriberLevelPrivateDomain;
    }

    public void setSubscriberLevelPrivateDomain(java.lang.Boolean subscriberLevelPrivateDomain) {
        this.subscriberLevelPrivateDomain = subscriberLevelPrivateDomain;
    }

    public com.exacttarget.Certificate getSMIMESignatureCertificate() {
        return SMIMESignatureCertificate;
    }

    public void setSMIMESignatureCertificate(com.exacttarget.Certificate SMIMESignatureCertificate) {
        this.SMIMESignatureCertificate = SMIMESignatureCertificate;
    }

    public com.exacttarget.PrivateDomainSet getPrivateDomainSet() {
        return privateDomainSet;
    }

    public void setPrivateDomainSet(com.exacttarget.PrivateDomainSet privateDomainSet) {
        this.privateDomainSet = privateDomainSet;
    }

}
