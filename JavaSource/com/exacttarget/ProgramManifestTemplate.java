/**
 * ProgramManifestTemplate.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ProgramManifestTemplate  extends com.exacttarget.APIObject  {
    private java.lang.String type;
    private java.lang.String operationType;
    private java.lang.String content;

    public ProgramManifestTemplate() {
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public java.lang.String getOperationType() {
        return operationType;
    }

    public void setOperationType(java.lang.String operationType) {
        this.operationType = operationType;
    }

    public java.lang.String getContent() {
        return content;
    }

    public void setContent(java.lang.String content) {
        this.content = content;
    }

}
