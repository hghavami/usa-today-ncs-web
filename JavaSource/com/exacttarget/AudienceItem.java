/**
 * AudienceItem.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AudienceItem  extends com.exacttarget.APIObject  {
    private com.exacttarget.List list;
    private com.exacttarget.SendDefinitionListTypeEnum sendDefinitionListType;
    private java.lang.String customObjectID;
    private com.exacttarget.DataSourceTypeEnum dataSourceTypeID;

    public AudienceItem() {
    }

    public com.exacttarget.List getList() {
        return list;
    }

    public void setList(com.exacttarget.List list) {
        this.list = list;
    }

    public com.exacttarget.SendDefinitionListTypeEnum getSendDefinitionListType() {
        return sendDefinitionListType;
    }

    public void setSendDefinitionListType(com.exacttarget.SendDefinitionListTypeEnum sendDefinitionListType) {
        this.sendDefinitionListType = sendDefinitionListType;
    }

    public java.lang.String getCustomObjectID() {
        return customObjectID;
    }

    public void setCustomObjectID(java.lang.String customObjectID) {
        this.customObjectID = customObjectID;
    }

    public com.exacttarget.DataSourceTypeEnum getDataSourceTypeID() {
        return dataSourceTypeID;
    }

    public void setDataSourceTypeID(com.exacttarget.DataSourceTypeEnum dataSourceTypeID) {
        this.dataSourceTypeID = dataSourceTypeID;
    }

}
