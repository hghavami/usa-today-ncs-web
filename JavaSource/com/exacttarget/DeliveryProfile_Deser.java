/**
 * DeliveryProfile_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class DeliveryProfile_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public DeliveryProfile_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new DeliveryProfile();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((DeliveryProfile)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((DeliveryProfile)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_747) {
          ((DeliveryProfile)value).setSubscriberLevelPrivateDomain(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_448) {
          ((DeliveryProfile)value).setSourceAddressType((com.exacttarget.DeliveryProfileSourceAddressTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_449) {
          ((DeliveryProfile)value).setPrivateIP((com.exacttarget.PrivateIP)objValue);
          return true;}
        else if (qName==QName_0_450) {
          ((DeliveryProfile)value).setDomainType((com.exacttarget.DeliveryProfileDomainTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_451) {
          ((DeliveryProfile)value).setPrivateDomain((com.exacttarget.PrivateDomain)objValue);
          return true;}
        else if (qName==QName_0_452) {
          ((DeliveryProfile)value).setHeaderSalutationSource((com.exacttarget.SalutationSourceEnum)objValue);
          return true;}
        else if (qName==QName_0_453) {
          ((DeliveryProfile)value).setHeaderContentArea((com.exacttarget.ContentArea)objValue);
          return true;}
        else if (qName==QName_0_454) {
          ((DeliveryProfile)value).setFooterSalutationSource((com.exacttarget.SalutationSourceEnum)objValue);
          return true;}
        else if (qName==QName_0_455) {
          ((DeliveryProfile)value).setFooterContentArea((com.exacttarget.ContentArea)objValue);
          return true;}
        else if (qName==QName_0_748) {
          ((DeliveryProfile)value).setSMIMESignatureCertificate((com.exacttarget.Certificate)objValue);
          return true;}
        else if (qName==QName_0_749) {
          ((DeliveryProfile)value).setPrivateDomainSet((com.exacttarget.PrivateDomainSet)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_454 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterSalutationSource");
    private final static javax.xml.namespace.QName QName_0_749 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateDomainSet");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_453 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderContentArea");
    private final static javax.xml.namespace.QName QName_0_448 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SourceAddressType");
    private final static javax.xml.namespace.QName QName_0_452 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderSalutationSource");
    private final static javax.xml.namespace.QName QName_0_451 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateDomain");
    private final static javax.xml.namespace.QName QName_0_748 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SMIMESignatureCertificate");
    private final static javax.xml.namespace.QName QName_0_449 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateIP");
    private final static javax.xml.namespace.QName QName_0_450 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DomainType");
    private final static javax.xml.namespace.QName QName_0_455 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterContentArea");
    private final static javax.xml.namespace.QName QName_0_747 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberLevelPrivateDomain");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
}
