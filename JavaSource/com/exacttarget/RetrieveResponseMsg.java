/**
 * RetrieveResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class RetrieveResponseMsg  {
    private java.lang.String overallStatus;
    private java.lang.String requestID;
    private com.exacttarget.APIObject[] results;

    public RetrieveResponseMsg() {
    }

    public java.lang.String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(java.lang.String overallStatus) {
        this.overallStatus = overallStatus;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

    public com.exacttarget.APIObject[] getResults() {
        return results;
    }

    public void setResults(com.exacttarget.APIObject[] results) {
        this.results = results;
    }

    public com.exacttarget.APIObject getResults(int i) {
        return this.results[i];
    }

    public void setResults(int i, com.exacttarget.APIObject value) {
        this.results[i] = value;
    }

}
