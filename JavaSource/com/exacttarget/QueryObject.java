/**
 * QueryObject.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class QueryObject  {
    private java.lang.String objectType;
    private java.lang.String[] properties;
    private com.exacttarget.QueryObject[] objects;

    public QueryObject() {
    }

    public java.lang.String getObjectType() {
        return objectType;
    }

    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }

    public java.lang.String[] getProperties() {
        return properties;
    }

    public void setProperties(java.lang.String[] properties) {
        this.properties = properties;
    }

    public java.lang.String getProperties(int i) {
        return this.properties[i];
    }

    public void setProperties(int i, java.lang.String value) {
        this.properties[i] = value;
    }

    public com.exacttarget.QueryObject[] getObjects() {
        return objects;
    }

    public void setObjects(com.exacttarget.QueryObject[] objects) {
        this.objects = objects;
    }

    public com.exacttarget.QueryObject getObjects(int i) {
        return this.objects[i];
    }

    public void setObjects(int i, com.exacttarget.QueryObject value) {
        this.objects[i] = value;
    }

}
