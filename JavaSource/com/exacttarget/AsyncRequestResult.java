/**
 * AsyncRequestResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AsyncRequestResult  extends com.exacttarget.APIObject  {
    private java.lang.String status;
    private java.util.Calendar completeDate;
    private java.lang.String callStatus;
    private java.lang.String callMessage;

    public AsyncRequestResult() {
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.util.Calendar getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(java.util.Calendar completeDate) {
        this.completeDate = completeDate;
    }

    public java.lang.String getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(java.lang.String callStatus) {
        this.callStatus = callStatus;
    }

    public java.lang.String getCallMessage() {
        return callMessage;
    }

    public void setCallMessage(java.lang.String callMessage) {
        this.callMessage = callMessage;
    }

}
