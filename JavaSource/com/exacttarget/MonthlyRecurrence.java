/**
 * MonthlyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MonthlyRecurrence  extends com.exacttarget.Recurrence  {
    private com.exacttarget.MonthlyRecurrencePatternTypeEnum monthlyRecurrencePatternType;
    private java.lang.Integer monthlyInterval;
    private java.lang.Integer scheduledDay;
    private com.exacttarget.WeekOfMonthEnum scheduledWeek;
    private com.exacttarget.DayOfWeekEnum scheduledDayOfWeek;

    public MonthlyRecurrence() {
    }

    public com.exacttarget.MonthlyRecurrencePatternTypeEnum getMonthlyRecurrencePatternType() {
        return monthlyRecurrencePatternType;
    }

    public void setMonthlyRecurrencePatternType(com.exacttarget.MonthlyRecurrencePatternTypeEnum monthlyRecurrencePatternType) {
        this.monthlyRecurrencePatternType = monthlyRecurrencePatternType;
    }

    public java.lang.Integer getMonthlyInterval() {
        return monthlyInterval;
    }

    public void setMonthlyInterval(java.lang.Integer monthlyInterval) {
        this.monthlyInterval = monthlyInterval;
    }

    public java.lang.Integer getScheduledDay() {
        return scheduledDay;
    }

    public void setScheduledDay(java.lang.Integer scheduledDay) {
        this.scheduledDay = scheduledDay;
    }

    public com.exacttarget.WeekOfMonthEnum getScheduledWeek() {
        return scheduledWeek;
    }

    public void setScheduledWeek(com.exacttarget.WeekOfMonthEnum scheduledWeek) {
        this.scheduledWeek = scheduledWeek;
    }

    public com.exacttarget.DayOfWeekEnum getScheduledDayOfWeek() {
        return scheduledDayOfWeek;
    }

    public void setScheduledDayOfWeek(com.exacttarget.DayOfWeekEnum scheduledDayOfWeek) {
        this.scheduledDayOfWeek = scheduledDayOfWeek;
    }

}
