/**
 * AccountUser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AccountUser  extends com.exacttarget.APIObject  {
    private java.lang.Integer accountUserID;
    private java.lang.String userID;
    private java.lang.String password;
    private java.lang.String name;
    private java.lang.String email;
    private java.lang.Boolean mustChangePassword;
    private java.lang.Boolean activeFlag;
    private java.lang.String challengePhrase;
    private java.lang.String challengeAnswer;
    private com.exacttarget.UserAccess[] userPermissions;
    private int delete;
    private java.util.Calendar lastSuccessfulLogin;
    private java.lang.Boolean isAPIUser;
    private java.lang.String notificationEmailAddress;
    private java.lang.Boolean isLocked;
    private java.lang.Boolean unlock;
    private java.lang.Integer businessUnit;
    private java.lang.Integer defaultBusinessUnit;
    private com.exacttarget.Locale locale;
    private com.exacttarget.TimeZone timeZone;
    private com.exacttarget.BusinessUnit defaultBusinessUnitObject;
    private com.exacttarget.BusinessUnit[] associatedBusinessUnits;
    private com.exacttarget.Role[] roles;

    public AccountUser() {
    }

    public java.lang.Integer getAccountUserID() {
        return accountUserID;
    }

    public void setAccountUserID(java.lang.Integer accountUserID) {
        this.accountUserID = accountUserID;
    }

    public java.lang.String getUserID() {
        return userID;
    }

    public void setUserID(java.lang.String userID) {
        this.userID = userID;
    }

    public java.lang.String getPassword() {
        return password;
    }

    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getEmail() {
        return email;
    }

    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    public java.lang.Boolean getMustChangePassword() {
        return mustChangePassword;
    }

    public void setMustChangePassword(java.lang.Boolean mustChangePassword) {
        this.mustChangePassword = mustChangePassword;
    }

    public java.lang.Boolean getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(java.lang.Boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    public java.lang.String getChallengePhrase() {
        return challengePhrase;
    }

    public void setChallengePhrase(java.lang.String challengePhrase) {
        this.challengePhrase = challengePhrase;
    }

    public java.lang.String getChallengeAnswer() {
        return challengeAnswer;
    }

    public void setChallengeAnswer(java.lang.String challengeAnswer) {
        this.challengeAnswer = challengeAnswer;
    }

    public com.exacttarget.UserAccess[] getUserPermissions() {
        return userPermissions;
    }

    public void setUserPermissions(com.exacttarget.UserAccess[] userPermissions) {
        this.userPermissions = userPermissions;
    }

    public com.exacttarget.UserAccess getUserPermissions(int i) {
        return this.userPermissions[i];
    }

    public void setUserPermissions(int i, com.exacttarget.UserAccess value) {
        this.userPermissions[i] = value;
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public java.util.Calendar getLastSuccessfulLogin() {
        return lastSuccessfulLogin;
    }

    public void setLastSuccessfulLogin(java.util.Calendar lastSuccessfulLogin) {
        this.lastSuccessfulLogin = lastSuccessfulLogin;
    }

    public java.lang.Boolean getIsAPIUser() {
        return isAPIUser;
    }

    public void setIsAPIUser(java.lang.Boolean isAPIUser) {
        this.isAPIUser = isAPIUser;
    }

    public java.lang.String getNotificationEmailAddress() {
        return notificationEmailAddress;
    }

    public void setNotificationEmailAddress(java.lang.String notificationEmailAddress) {
        this.notificationEmailAddress = notificationEmailAddress;
    }

    public java.lang.Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(java.lang.Boolean isLocked) {
        this.isLocked = isLocked;
    }

    public java.lang.Boolean getUnlock() {
        return unlock;
    }

    public void setUnlock(java.lang.Boolean unlock) {
        this.unlock = unlock;
    }

    public java.lang.Integer getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(java.lang.Integer businessUnit) {
        this.businessUnit = businessUnit;
    }

    public java.lang.Integer getDefaultBusinessUnit() {
        return defaultBusinessUnit;
    }

    public void setDefaultBusinessUnit(java.lang.Integer defaultBusinessUnit) {
        this.defaultBusinessUnit = defaultBusinessUnit;
    }

    public com.exacttarget.Locale getLocale() {
        return locale;
    }

    public void setLocale(com.exacttarget.Locale locale) {
        this.locale = locale;
    }

    public com.exacttarget.TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(com.exacttarget.TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public com.exacttarget.BusinessUnit getDefaultBusinessUnitObject() {
        return defaultBusinessUnitObject;
    }

    public void setDefaultBusinessUnitObject(com.exacttarget.BusinessUnit defaultBusinessUnitObject) {
        this.defaultBusinessUnitObject = defaultBusinessUnitObject;
    }

    public com.exacttarget.BusinessUnit[] getAssociatedBusinessUnits() {
        return associatedBusinessUnits;
    }

    public void setAssociatedBusinessUnits(com.exacttarget.BusinessUnit[] associatedBusinessUnits) {
        this.associatedBusinessUnits = associatedBusinessUnits;
    }

    public com.exacttarget.Role[] getRoles() {
        return roles;
    }

    public void setRoles(com.exacttarget.Role[] roles) {
        this.roles = roles;
    }

}
