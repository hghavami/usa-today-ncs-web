/**
 * DataSourceTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DataSourceTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected DataSourceTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _List = "List";
    public static final java.lang.String _CustomObject = "CustomObject";
    public static final java.lang.String _DomainExclusion = "DomainExclusion";
    public static final java.lang.String _SalesForceReport = "SalesForceReport";
    public static final java.lang.String _SalesForceCampaign = "SalesForceCampaign";
    public static final java.lang.String _FilterDefinition = "FilterDefinition";
    public static final java.lang.String _OptOutList = "OptOutList";
    public static final DataSourceTypeEnum List = new DataSourceTypeEnum(_List);
    public static final DataSourceTypeEnum CustomObject = new DataSourceTypeEnum(_CustomObject);
    public static final DataSourceTypeEnum DomainExclusion = new DataSourceTypeEnum(_DomainExclusion);
    public static final DataSourceTypeEnum SalesForceReport = new DataSourceTypeEnum(_SalesForceReport);
    public static final DataSourceTypeEnum SalesForceCampaign = new DataSourceTypeEnum(_SalesForceCampaign);
    public static final DataSourceTypeEnum FilterDefinition = new DataSourceTypeEnum(_FilterDefinition);
    public static final DataSourceTypeEnum OptOutList = new DataSourceTypeEnum(_OptOutList);
    public java.lang.String getValue() { return _value_;}
    public static DataSourceTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        DataSourceTypeEnum enumeration = (DataSourceTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static DataSourceTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
