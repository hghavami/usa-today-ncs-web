/**
 * VersionInfoResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class VersionInfoResponse  {
    private java.lang.String version;
    private java.util.Calendar versionDate;
    private java.lang.String notes;
    private com.exacttarget.VersionInfoResponse[] versionHistory;

    public VersionInfoResponse() {
    }

    public java.lang.String getVersion() {
        return version;
    }

    public void setVersion(java.lang.String version) {
        this.version = version;
    }

    public java.util.Calendar getVersionDate() {
        return versionDate;
    }

    public void setVersionDate(java.util.Calendar versionDate) {
        this.versionDate = versionDate;
    }

    public java.lang.String getNotes() {
        return notes;
    }

    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }

    public com.exacttarget.VersionInfoResponse[] getVersionHistory() {
        return versionHistory;
    }

    public void setVersionHistory(com.exacttarget.VersionInfoResponse[] versionHistory) {
        this.versionHistory = versionHistory;
    }

    public com.exacttarget.VersionInfoResponse getVersionHistory(int i) {
        return this.versionHistory[i];
    }

    public void setVersionHistory(int i, com.exacttarget.VersionInfoResponse value) {
        this.versionHistory[i] = value;
    }

}
