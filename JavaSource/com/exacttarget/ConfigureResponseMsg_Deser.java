/**
 * ConfigureResponseMsg_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ConfigureResponseMsg_Deser extends com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializer {
    /**
     * Constructor
     */
    public ConfigureResponseMsg_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ConfigureResponseMsg();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_69) {
          ((ConfigureResponseMsg)value).setOverallStatus(strValue);
          return true;}
        else if (qName==QName_0_172) {
          ((ConfigureResponseMsg)value).setOverallStatusMessage(strValue);
          return true;}
        else if (qName==QName_0_37) {
          ((ConfigureResponseMsg)value).setRequestID(strValue);
          return true;}
        return false;
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return false;
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_68) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.ConfigureResult[] array = new com.exacttarget.ConfigureResult[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ConfigureResponseMsg)value).setResults(array);
          } else { 
            ((ConfigureResponseMsg)value).setResults((com.exacttarget.ConfigureResult[])objValue);}
          return true;}
        return false;
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return false;
    }
    private final static javax.xml.namespace.QName QName_0_69 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OverallStatus");
    private final static javax.xml.namespace.QName QName_0_37 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RequestID");
    private final static javax.xml.namespace.QName QName_0_172 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OverallStatusMessage");
    private final static javax.xml.namespace.QName QName_0_68 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Results");
}
