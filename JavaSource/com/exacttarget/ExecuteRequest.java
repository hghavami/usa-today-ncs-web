/**
 * ExecuteRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExecuteRequest  {
    private com.exacttarget.ClientID client;
    private java.lang.String name;
    private com.exacttarget.APIProperty[] parameters;

    public ExecuteRequest() {
    }

    public com.exacttarget.ClientID getClient() {
        return client;
    }

    public void setClient(com.exacttarget.ClientID client) {
        this.client = client;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public com.exacttarget.APIProperty[] getParameters() {
        return parameters;
    }

    public void setParameters(com.exacttarget.APIProperty[] parameters) {
        this.parameters = parameters;
    }

    public com.exacttarget.APIProperty getParameters(int i) {
        return this.parameters[i];
    }

    public void setParameters(int i, com.exacttarget.APIProperty value) {
        this.parameters[i] = value;
    }

}
