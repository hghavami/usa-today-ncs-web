/**
 * Account_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Account_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public Account_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new Account();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_287) {
          ((Account)value).setParentID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_288) {
          ((Account)value).setBrandID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_289) {
          ((Account)value).setPrivateLabelID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_290) {
          ((Account)value).setReportingParentID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_20) {
          ((Account)value).setName(strValue);
          return true;}
        else if (qName==QName_0_178) {
          ((Account)value).setEmail(strValue);
          return true;}
        else if (qName==QName_0_22) {
          ((Account)value).setFromName(strValue);
          return true;}
        else if (qName==QName_0_291) {
          ((Account)value).setBusinessName(strValue);
          return true;}
        else if (qName==QName_0_292) {
          ((Account)value).setPhone(strValue);
          return true;}
        else if (qName==QName_0_293) {
          ((Account)value).setAddress(strValue);
          return true;}
        else if (qName==QName_0_294) {
          ((Account)value).setFax(strValue);
          return true;}
        else if (qName==QName_0_295) {
          ((Account)value).setCity(strValue);
          return true;}
        else if (qName==QName_0_296) {
          ((Account)value).setState(strValue);
          return true;}
        else if (qName==QName_0_297) {
          ((Account)value).setZip(strValue);
          return true;}
        else if (qName==QName_0_298) {
          ((Account)value).setCountry(strValue);
          return true;}
        else if (qName==QName_0_187) {
          ((Account)value).setIsActive(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_299) {
          ((Account)value).setIsTestAccount(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_300) {
          ((Account)value).setOrgID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_301) {
          ((Account)value).setDBID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_302) {
          ((Account)value).setParentName(strValue);
          return true;}
        else if (qName==QName_0_303) {
          ((Account)value).setCustomerID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_304) {
          ((Account)value).setDeletedDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_305) {
          ((Account)value).setEditionID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_311) {
          ((Account)value).setInheritAddress(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_312) {
          ((Account)value).setIsTrialAccount(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_286) {
          ((Account)value).setAccountType((com.exacttarget.AccountTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_307) {
          ((Account)value).setSubscription((com.exacttarget.Subscription)objValue);
          return true;}
        else if (qName==QName_0_679) {
          ((Account)value).setLocale((com.exacttarget.Locale)objValue);
          return true;}
        else if (qName==QName_0_696) {
          ((Account)value).setParentAccount((com.exacttarget.Account)objValue);
          return true;}
        else if (qName==QName_0_680) {
          ((Account)value).setTimeZone((com.exacttarget.TimeZone)objValue);
          return true;}
        else if (qName==QName_0_683) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.Role[] array = new com.exacttarget.Role[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((Account)value).setRoles(array);
          } else { 
            ((Account)value).setRoles((com.exacttarget.Role[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_306) {
          com.exacttarget.AccountDataItem[] array = new com.exacttarget.AccountDataItem[listValue.size()];
          listValue.toArray(array);
          ((Account)value).setChildren(array);
          return true;}
        else if (qName==QName_0_308) {
          com.exacttarget.PrivateLabel[] array = new com.exacttarget.PrivateLabel[listValue.size()];
          listValue.toArray(array);
          ((Account)value).setPrivateLabels(array);
          return true;}
        else if (qName==QName_0_309) {
          com.exacttarget.BusinessRule[] array = new com.exacttarget.BusinessRule[listValue.size()];
          listValue.toArray(array);
          ((Account)value).setBusinessRules(array);
          return true;}
        else if (qName==QName_0_310) {
          com.exacttarget.AccountUser[] array = new com.exacttarget.AccountUser[listValue.size()];
          listValue.toArray(array);
          ((Account)value).setAccountUsers(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_696 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ParentAccount");
    private final static javax.xml.namespace.QName QName_0_286 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountType");
    private final static javax.xml.namespace.QName QName_0_299 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsTestAccount");
    private final static javax.xml.namespace.QName QName_0_309 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BusinessRules");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_303 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CustomerID");
    private final static javax.xml.namespace.QName QName_0_288 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BrandID");
    private final static javax.xml.namespace.QName QName_0_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromName");
    private final static javax.xml.namespace.QName QName_0_307 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscription");
    private final static javax.xml.namespace.QName QName_0_296 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "State");
    private final static javax.xml.namespace.QName QName_0_680 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TimeZone");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_306 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Children");
    private final static javax.xml.namespace.QName QName_0_301 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DBID");
    private final static javax.xml.namespace.QName QName_0_300 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OrgID");
    private final static javax.xml.namespace.QName QName_0_293 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Address");
    private final static javax.xml.namespace.QName QName_0_292 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Phone");
    private final static javax.xml.namespace.QName QName_0_287 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ParentID");
    private final static javax.xml.namespace.QName QName_0_294 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Fax");
    private final static javax.xml.namespace.QName QName_0_305 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EditionID");
    private final static javax.xml.namespace.QName QName_0_302 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ParentName");
    private final static javax.xml.namespace.QName QName_0_298 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Country");
    private final static javax.xml.namespace.QName QName_0_308 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateLabels");
    private final static javax.xml.namespace.QName QName_0_683 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Roles");
    private final static javax.xml.namespace.QName QName_0_290 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ReportingParentID");
    private final static javax.xml.namespace.QName QName_0_310 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountUsers");
    private final static javax.xml.namespace.QName QName_0_291 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BusinessName");
    private final static javax.xml.namespace.QName QName_0_289 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateLabelID");
    private final static javax.xml.namespace.QName QName_0_312 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsTrialAccount");
    private final static javax.xml.namespace.QName QName_0_187 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsActive");
    private final static javax.xml.namespace.QName QName_0_304 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeletedDate");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_311 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "InheritAddress");
    private final static javax.xml.namespace.QName QName_0_297 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Zip");
    private final static javax.xml.namespace.QName QName_0_295 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "City");
}
