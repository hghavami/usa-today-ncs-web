/**
 * FileType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class FileType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected FileType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _CSV = "CSV";
    public static final java.lang.String _TAB = "TAB";
    public static final java.lang.String _Other = "Other";
    public static final FileType CSV = new FileType(_CSV);
    public static final FileType TAB = new FileType(_TAB);
    public static final FileType Other = new FileType(_Other);
    public java.lang.String getValue() { return _value_;}
    public static FileType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        FileType enumeration = (FileType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static FileType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
