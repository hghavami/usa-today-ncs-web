/**
 * RetrieveRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class RetrieveRequest  {
    private com.exacttarget.ClientID[] clientIDs;
    private java.lang.String objectType;
    private java.lang.String[] properties;
    private com.exacttarget.FilterPart filter;
    private com.exacttarget.AsyncResponse[] respondTo;
    private com.exacttarget.APIProperty[] partnerProperties;
    private java.lang.String continueRequest;
    private java.lang.Boolean queryAllAccounts;
    private java.lang.Boolean retrieveAllSinceLastBatch;
    private java.lang.Boolean repeatLastResult;
    private com.exacttarget.Request[] retrieves;
    private com.exacttarget.RetrieveOptions options;

    public RetrieveRequest() {
    }

    public com.exacttarget.ClientID[] getClientIDs() {
        return clientIDs;
    }

    public void setClientIDs(com.exacttarget.ClientID[] clientIDs) {
        this.clientIDs = clientIDs;
    }

    public com.exacttarget.ClientID getClientIDs(int i) {
        return this.clientIDs[i];
    }

    public void setClientIDs(int i, com.exacttarget.ClientID value) {
        this.clientIDs[i] = value;
    }

    public java.lang.String getObjectType() {
        return objectType;
    }

    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }

    public java.lang.String[] getProperties() {
        return properties;
    }

    public void setProperties(java.lang.String[] properties) {
        this.properties = properties;
    }

    public java.lang.String getProperties(int i) {
        return this.properties[i];
    }

    public void setProperties(int i, java.lang.String value) {
        this.properties[i] = value;
    }

    public com.exacttarget.FilterPart getFilter() {
        return filter;
    }

    public void setFilter(com.exacttarget.FilterPart filter) {
        this.filter = filter;
    }

    public com.exacttarget.AsyncResponse[] getRespondTo() {
        return respondTo;
    }

    public void setRespondTo(com.exacttarget.AsyncResponse[] respondTo) {
        this.respondTo = respondTo;
    }

    public com.exacttarget.AsyncResponse getRespondTo(int i) {
        return this.respondTo[i];
    }

    public void setRespondTo(int i, com.exacttarget.AsyncResponse value) {
        this.respondTo[i] = value;
    }

    public com.exacttarget.APIProperty[] getPartnerProperties() {
        return partnerProperties;
    }

    public void setPartnerProperties(com.exacttarget.APIProperty[] partnerProperties) {
        this.partnerProperties = partnerProperties;
    }

    public com.exacttarget.APIProperty getPartnerProperties(int i) {
        return this.partnerProperties[i];
    }

    public void setPartnerProperties(int i, com.exacttarget.APIProperty value) {
        this.partnerProperties[i] = value;
    }

    public java.lang.String getContinueRequest() {
        return continueRequest;
    }

    public void setContinueRequest(java.lang.String continueRequest) {
        this.continueRequest = continueRequest;
    }

    public java.lang.Boolean getQueryAllAccounts() {
        return queryAllAccounts;
    }

    public void setQueryAllAccounts(java.lang.Boolean queryAllAccounts) {
        this.queryAllAccounts = queryAllAccounts;
    }

    public java.lang.Boolean getRetrieveAllSinceLastBatch() {
        return retrieveAllSinceLastBatch;
    }

    public void setRetrieveAllSinceLastBatch(java.lang.Boolean retrieveAllSinceLastBatch) {
        this.retrieveAllSinceLastBatch = retrieveAllSinceLastBatch;
    }

    public java.lang.Boolean getRepeatLastResult() {
        return repeatLastResult;
    }

    public void setRepeatLastResult(java.lang.Boolean repeatLastResult) {
        this.repeatLastResult = repeatLastResult;
    }

    public com.exacttarget.Request[] getRetrieves() {
        return retrieves;
    }

    public void setRetrieves(com.exacttarget.Request[] retrieves) {
        this.retrieves = retrieves;
    }

    public com.exacttarget.RetrieveOptions getOptions() {
        return options;
    }

    public void setOptions(com.exacttarget.RetrieveOptions options) {
        this.options = options;
    }

}
