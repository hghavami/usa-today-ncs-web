/**
 * Group.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Group  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.Integer category;
    private java.lang.String description;
    private com.exacttarget.Subscriber[] subscribers;

    public Group() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.Integer getCategory() {
        return category;
    }

    public void setCategory(java.lang.Integer category) {
        this.category = category;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.Subscriber[] getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(com.exacttarget.Subscriber[] subscribers) {
        this.subscribers = subscribers;
    }

    public com.exacttarget.Subscriber getSubscribers(int i) {
        return this.subscribers[i];
    }

    public void setSubscribers(int i, com.exacttarget.Subscriber value) {
        this.subscribers[i] = value;
    }

}
