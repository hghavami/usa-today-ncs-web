/**
 * AddressStatus.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AddressStatus  {
    private com.exacttarget.SubscriberAddressStatus status;

    public AddressStatus() {
    }

    public com.exacttarget.SubscriberAddressStatus getStatus() {
        return status;
    }

    public void setStatus(com.exacttarget.SubscriberAddressStatus status) {
        this.status = status;
    }

}
