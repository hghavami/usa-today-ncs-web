/**
 * ListAttributeFieldType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ListAttributeFieldType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ListAttributeFieldType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Text = "Text";
    public static final java.lang.String _Number = "Number";
    public static final java.lang.String _Date = "Date";
    public static final java.lang.String _Boolean = "Boolean";
    public static final java.lang.String _Decimal = "Decimal";
    public static final ListAttributeFieldType Text = new ListAttributeFieldType(_Text);
    public static final ListAttributeFieldType Number = new ListAttributeFieldType(_Number);
    public static final ListAttributeFieldType Date = new ListAttributeFieldType(_Date);
    public static final ListAttributeFieldType Boolean = new ListAttributeFieldType(_Boolean);
    public static final ListAttributeFieldType Decimal = new ListAttributeFieldType(_Decimal);
    public java.lang.String getValue() { return _value_;}
    public static ListAttributeFieldType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ListAttributeFieldType enumeration = (ListAttributeFieldType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ListAttributeFieldType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
