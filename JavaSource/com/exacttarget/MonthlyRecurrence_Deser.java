/**
 * MonthlyRecurrence_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class MonthlyRecurrence_Deser extends com.exacttarget.Recurrence_Deser {
    /**
     * Constructor
     */
    public MonthlyRecurrence_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new MonthlyRecurrence();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_260) {
          ((MonthlyRecurrence)value).setMonthlyInterval(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_261) {
          ((MonthlyRecurrence)value).setScheduledDay(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_259) {
          ((MonthlyRecurrence)value).setMonthlyRecurrencePatternType((com.exacttarget.MonthlyRecurrencePatternTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_262) {
          ((MonthlyRecurrence)value).setScheduledWeek((com.exacttarget.WeekOfMonthEnum)objValue);
          return true;}
        else if (qName==QName_0_263) {
          ((MonthlyRecurrence)value).setScheduledDayOfWeek((com.exacttarget.DayOfWeekEnum)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_263 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledDayOfWeek");
    private final static javax.xml.namespace.QName QName_0_262 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledWeek");
    private final static javax.xml.namespace.QName QName_0_259 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MonthlyRecurrencePatternType");
    private final static javax.xml.namespace.QName QName_0_261 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledDay");
    private final static javax.xml.namespace.QName QName_0_260 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MonthlyInterval");
}
