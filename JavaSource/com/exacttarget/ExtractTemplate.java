/**
 * ExtractTemplate.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExtractTemplate  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String configurationPage;
    private java.lang.String packageKey;

    public ExtractTemplate() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getConfigurationPage() {
        return configurationPage;
    }

    public void setConfigurationPage(java.lang.String configurationPage) {
        this.configurationPage = configurationPage;
    }

    public java.lang.String getPackageKey() {
        return packageKey;
    }

    public void setPackageKey(java.lang.String packageKey) {
        this.packageKey = packageKey;
    }

}
