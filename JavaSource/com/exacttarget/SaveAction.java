/**
 * SaveAction.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SaveAction  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SaveAction(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _AddOnly = "AddOnly";
    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _Nothing = "Nothing";
    public static final java.lang.String _UpdateAdd = "UpdateAdd";
    public static final java.lang.String _UpdateOnly = "UpdateOnly";
    public static final java.lang.String _Delete = "Delete";
    public static final SaveAction AddOnly = new SaveAction(_AddOnly);
    public static final SaveAction Default = new SaveAction(_Default);
    public static final SaveAction Nothing = new SaveAction(_Nothing);
    public static final SaveAction UpdateAdd = new SaveAction(_UpdateAdd);
    public static final SaveAction UpdateOnly = new SaveAction(_UpdateOnly);
    public static final SaveAction Delete = new SaveAction(_Delete);
    public java.lang.String getValue() { return _value_;}
    public static SaveAction fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SaveAction enumeration = (SaveAction)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SaveAction fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
