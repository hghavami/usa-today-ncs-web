/**
 * SendDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SendDefinition_Deser extends com.exacttarget.InteractionDefinition_Deser {
    /**
     * Constructor
     */
    public SendDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new SendDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_182) {
          ((SendDefinition)value).setCategoryID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_22) {
          ((SendDefinition)value).setFromName(strValue);
          return true;}
        else if (qName==QName_0_23) {
          ((SendDefinition)value).setFromAddress(strValue);
          return true;}
        else if (qName==QName_0_456) {
          ((SendDefinition)value).setSuppressTracking(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_457) {
          ((SendDefinition)value).setIsSendLogging(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_445) {
          ((SendDefinition)value).setSendClassification((com.exacttarget.SendClassification)objValue);
          return true;}
        else if (qName==QName_0_446) {
          ((SendDefinition)value).setSenderProfile((com.exacttarget.SenderProfile)objValue);
          return true;}
        else if (qName==QName_0_447) {
          ((SendDefinition)value).setDeliveryProfile((com.exacttarget.DeliveryProfile)objValue);
          return true;}
        else if (qName==QName_0_448) {
          ((SendDefinition)value).setSourceAddressType((com.exacttarget.DeliveryProfileSourceAddressTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_449) {
          ((SendDefinition)value).setPrivateIP((com.exacttarget.PrivateIP)objValue);
          return true;}
        else if (qName==QName_0_450) {
          ((SendDefinition)value).setDomainType((com.exacttarget.DeliveryProfileDomainTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_451) {
          ((SendDefinition)value).setPrivateDomain((com.exacttarget.PrivateDomain)objValue);
          return true;}
        else if (qName==QName_0_452) {
          ((SendDefinition)value).setHeaderSalutationSource((com.exacttarget.SalutationSourceEnum)objValue);
          return true;}
        else if (qName==QName_0_453) {
          ((SendDefinition)value).setHeaderContentArea((com.exacttarget.ContentArea)objValue);
          return true;}
        else if (qName==QName_0_454) {
          ((SendDefinition)value).setFooterSalutationSource((com.exacttarget.SalutationSourceEnum)objValue);
          return true;}
        else if (qName==QName_0_455) {
          ((SendDefinition)value).setFooterContentArea((com.exacttarget.ContentArea)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_454 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterSalutationSource");
    private final static javax.xml.namespace.QName QName_0_182 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CategoryID");
    private final static javax.xml.namespace.QName QName_0_23 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromAddress");
    private final static javax.xml.namespace.QName QName_0_446 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SenderProfile");
    private final static javax.xml.namespace.QName QName_0_447 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeliveryProfile");
    private final static javax.xml.namespace.QName QName_0_445 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendClassification");
    private final static javax.xml.namespace.QName QName_0_453 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderContentArea");
    private final static javax.xml.namespace.QName QName_0_448 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SourceAddressType");
    private final static javax.xml.namespace.QName QName_0_452 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderSalutationSource");
    private final static javax.xml.namespace.QName QName_0_451 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateDomain");
    private final static javax.xml.namespace.QName QName_0_456 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SuppressTracking");
    private final static javax.xml.namespace.QName QName_0_449 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateIP");
    private final static javax.xml.namespace.QName QName_0_450 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DomainType");
    private final static javax.xml.namespace.QName QName_0_457 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsSendLogging");
    private final static javax.xml.namespace.QName QName_0_455 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterContentArea");
    private final static javax.xml.namespace.QName QName_0_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromName");
}
