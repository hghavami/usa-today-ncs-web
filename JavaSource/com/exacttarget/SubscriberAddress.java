/**
 * SubscriberAddress.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberAddress  {
    private java.lang.String addressType;
    private java.lang.String address;
    private com.exacttarget.AddressStatus[] statuses;

    public SubscriberAddress() {
    }

    public java.lang.String getAddressType() {
        return addressType;
    }

    public void setAddressType(java.lang.String addressType) {
        this.addressType = addressType;
    }

    public java.lang.String getAddress() {
        return address;
    }

    public void setAddress(java.lang.String address) {
        this.address = address;
    }

    public com.exacttarget.AddressStatus[] getStatuses() {
        return statuses;
    }

    public void setStatuses(com.exacttarget.AddressStatus[] statuses) {
        this.statuses = statuses;
    }

}
