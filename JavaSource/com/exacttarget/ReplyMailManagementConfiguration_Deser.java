/**
 * ReplyMailManagementConfiguration_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ReplyMailManagementConfiguration_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public ReplyMailManagementConfiguration_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ReplyMailManagementConfiguration();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_654) {
          ((ReplyMailManagementConfiguration)value).setEmailDisplayName(strValue);
          return true;}
        else if (qName==QName_0_655) {
          ((ReplyMailManagementConfiguration)value).setReplySubdomain(strValue);
          return true;}
        else if (qName==QName_0_656) {
          ((ReplyMailManagementConfiguration)value).setEmailReplyAddress(strValue);
          return true;}
        else if (qName==QName_0_657) {
          ((ReplyMailManagementConfiguration)value).setDNSRedirectComplete(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_658) {
          ((ReplyMailManagementConfiguration)value).setDeleteAutoReplies(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_659) {
          ((ReplyMailManagementConfiguration)value).setSupportUnsubscribes(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_660) {
          ((ReplyMailManagementConfiguration)value).setSupportUnsubKeyword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_661) {
          ((ReplyMailManagementConfiguration)value).setSupportUnsubscribeKeyword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_662) {
          ((ReplyMailManagementConfiguration)value).setSupportRemoveKeyword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_663) {
          ((ReplyMailManagementConfiguration)value).setSupportOptOutKeyword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_664) {
          ((ReplyMailManagementConfiguration)value).setSupportLeaveKeyword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_665) {
          ((ReplyMailManagementConfiguration)value).setSupportMisspelledKeywords(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_666) {
          ((ReplyMailManagementConfiguration)value).setSendAutoReplies(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_667) {
          ((ReplyMailManagementConfiguration)value).setAutoReplySubject(strValue);
          return true;}
        else if (qName==QName_0_668) {
          ((ReplyMailManagementConfiguration)value).setAutoReplyBody(strValue);
          return true;}
        else if (qName==QName_0_669) {
          ((ReplyMailManagementConfiguration)value).setForwardingAddress(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_667 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplySubject");
    private final static javax.xml.namespace.QName QName_0_663 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportOptOutKeyword");
    private final static javax.xml.namespace.QName QName_0_664 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportLeaveKeyword");
    private final static javax.xml.namespace.QName QName_0_662 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportRemoveKeyword");
    private final static javax.xml.namespace.QName QName_0_656 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailReplyAddress");
    private final static javax.xml.namespace.QName QName_0_660 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubKeyword");
    private final static javax.xml.namespace.QName QName_0_655 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ReplySubdomain");
    private final static javax.xml.namespace.QName QName_0_659 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubscribes");
    private final static javax.xml.namespace.QName QName_0_666 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendAutoReplies");
    private final static javax.xml.namespace.QName QName_0_669 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ForwardingAddress");
    private final static javax.xml.namespace.QName QName_0_661 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubscribeKeyword");
    private final static javax.xml.namespace.QName QName_0_658 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeleteAutoReplies");
    private final static javax.xml.namespace.QName QName_0_668 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplyBody");
    private final static javax.xml.namespace.QName QName_0_657 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DNSRedirectComplete");
    private final static javax.xml.namespace.QName QName_0_665 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportMisspelledKeywords");
    private final static javax.xml.namespace.QName QName_0_654 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailDisplayName");
}
