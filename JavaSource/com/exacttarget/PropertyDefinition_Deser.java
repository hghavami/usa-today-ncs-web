/**
 * PropertyDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PropertyDefinition_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public PropertyDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new PropertyDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_123) {
          ((PropertyDefinition)value).setValueType((com.exacttarget.SoapType)objValue);
          return true;}
        else if (qName==QName_0_124) {
          ((PropertyDefinition)value).setPropertyType((com.exacttarget.PropertyType)objValue);
          return true;}
        else if (qName==QName_0_145) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.PicklistItem[] array = new com.exacttarget.PicklistItem[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((PropertyDefinition)value).setPicklistItems(array);
          } else { 
            ((PropertyDefinition)value).setPicklistItems((com.exacttarget.PicklistItem[])objValue);}
          return true;}
        else if (qName==QName_0_148) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.APIObject[] array = new com.exacttarget.APIObject[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((PropertyDefinition)value).setReferences(array);
          } else { 
            ((PropertyDefinition)value).setReferences((com.exacttarget.APIObject[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_129) {
          com.exacttarget.AttributeMap[] array = new com.exacttarget.AttributeMap[listValue.size()];
          listValue.toArray(array);
          ((PropertyDefinition)value).setAttributeMaps(array);
          return true;}
        else if (qName==QName_0_130) {
          com.exacttarget.APIProperty[] array = new com.exacttarget.APIProperty[listValue.size()];
          listValue.toArray(array);
          ((PropertyDefinition)value).setMarkups(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_124 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PropertyType");
    private final static javax.xml.namespace.QName QName_0_138 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MinValue");
    private final static javax.xml.namespace.QName QName_0_142 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsEditable");
    private final static javax.xml.namespace.QName QName_0_132 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Scale");
    private final static javax.xml.namespace.QName QName_0_113 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsQueryable");
    private final static javax.xml.namespace.QName QName_0_126 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsPartnerProperty");
    private final static javax.xml.namespace.QName QName_0_136 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MinLength");
    private final static javax.xml.namespace.QName QName_0_122 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataType");
    private final static javax.xml.namespace.QName QName_0_129 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AttributeMaps");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_128 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerMap");
    private final static javax.xml.namespace.QName QName_0_139 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxValue");
    private final static javax.xml.namespace.QName QName_0_146 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsSendTime");
    private final static javax.xml.namespace.QName QName_0_144 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsRestrictedPicklist");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_135 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultValue");
    private final static javax.xml.namespace.QName QName_0_143 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsNillable");
    private final static javax.xml.namespace.QName QName_0_147 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DisplayOrder");
    private final static javax.xml.namespace.QName QName_0_130 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Markups");
    private final static javax.xml.namespace.QName QName_0_140 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsRequired");
    private final static javax.xml.namespace.QName QName_0_127 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAccountProperty");
    private final static javax.xml.namespace.QName QName_0_133 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Label");
    private final static javax.xml.namespace.QName QName_0_111 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsUpdatable");
    private final static javax.xml.namespace.QName QName_0_151 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsContextSpecific");
    private final static javax.xml.namespace.QName QName_0_145 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PicklistItems");
    private final static javax.xml.namespace.QName QName_0_141 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsViewable");
    private final static javax.xml.namespace.QName QName_0_149 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RelationshipName");
    private final static javax.xml.namespace.QName QName_0_123 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ValueType");
    private final static javax.xml.namespace.QName QName_0_125 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsFilterable");
    private final static javax.xml.namespace.QName QName_0_148 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "References");
    private final static javax.xml.namespace.QName QName_0_137 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxLength");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_110 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsCreatable");
    private final static javax.xml.namespace.QName QName_0_131 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Precision");
    private final static javax.xml.namespace.QName QName_0_112 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsRetrievable");
}
