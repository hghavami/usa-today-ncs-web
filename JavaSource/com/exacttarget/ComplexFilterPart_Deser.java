/**
 * ComplexFilterPart_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ComplexFilterPart_Deser extends com.exacttarget.FilterPart_Deser {
    /**
     * Constructor
     */
    public ComplexFilterPart_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ComplexFilterPart();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_103) {
          ((ComplexFilterPart)value).setLeftOperand((com.exacttarget.FilterPart)objValue);
          return true;}
        else if (qName==QName_0_104) {
          ((ComplexFilterPart)value).setLogicalOperator((com.exacttarget.LogicalOperators)objValue);
          return true;}
        else if (qName==QName_0_105) {
          ((ComplexFilterPart)value).setRightOperand((com.exacttarget.FilterPart)objValue);
          return true;}
        else if (qName==QName_0_686) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.FilterPart[] array = new com.exacttarget.FilterPart[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ComplexFilterPart)value).setAdditionalOperands(array);
          } else { 
            ((ComplexFilterPart)value).setAdditionalOperands((com.exacttarget.FilterPart[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_103 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LeftOperand");
    private final static javax.xml.namespace.QName QName_0_105 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RightOperand");
    private final static javax.xml.namespace.QName QName_0_686 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AdditionalOperands");
    private final static javax.xml.namespace.QName QName_0_104 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LogicalOperator");
}
