/**
 * ScheduleDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ScheduleDefinition_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public ScheduleDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ScheduleDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((ScheduleDefinition)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((ScheduleDefinition)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_232) {
          ((ScheduleDefinition)value).setStartDateTime(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_233) {
          ((ScheduleDefinition)value).setEndDateTime(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_234) {
          ((ScheduleDefinition)value).setOccurrences(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_163) {
          ((ScheduleDefinition)value).setKeyword(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_229) {
          ((ScheduleDefinition)value).setRecurrence((com.exacttarget.Recurrence)objValue);
          return true;}
        else if (qName==QName_0_230) {
          ((ScheduleDefinition)value).setRecurrenceType((com.exacttarget.RecurrenceTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_231) {
          ((ScheduleDefinition)value).setRecurrenceRangeType((com.exacttarget.RecurrenceRangeTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_680) {
          ((ScheduleDefinition)value).setTimeZone((com.exacttarget.TimeZone)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_229 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Recurrence");
    private final static javax.xml.namespace.QName QName_0_680 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TimeZone");
    private final static javax.xml.namespace.QName QName_0_230 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RecurrenceType");
    private final static javax.xml.namespace.QName QName_0_234 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Occurrences");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_163 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Keyword");
    private final static javax.xml.namespace.QName QName_0_233 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EndDateTime");
    private final static javax.xml.namespace.QName QName_0_232 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "StartDateTime");
    private final static javax.xml.namespace.QName QName_0_231 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RecurrenceRangeType");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
}
