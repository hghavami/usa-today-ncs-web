/**
 * UpdateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UpdateRequest  {
    private com.exacttarget.UpdateOptions options;
    private com.exacttarget.APIObject[] objects;

    public UpdateRequest() {
    }

    public com.exacttarget.UpdateOptions getOptions() {
        return options;
    }

    public void setOptions(com.exacttarget.UpdateOptions options) {
        this.options = options;
    }

    public com.exacttarget.APIObject[] getObjects() {
        return objects;
    }

    public void setObjects(com.exacttarget.APIObject[] objects) {
        this.objects = objects;
    }

    public com.exacttarget.APIObject getObjects(int i) {
        return this.objects[i];
    }

    public void setObjects(int i, com.exacttarget.APIObject value) {
        this.objects[i] = value;
    }

}
