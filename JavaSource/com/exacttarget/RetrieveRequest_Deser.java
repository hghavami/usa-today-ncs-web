/**
 * RetrieveRequest_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class RetrieveRequest_Deser extends com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializer {
    /**
     * Constructor
     */
    public RetrieveRequest_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new RetrieveRequest();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_77) {
          ((RetrieveRequest)value).setObjectType(strValue);
          return true;}
        else if (qName==QName_0_81) {
          ((RetrieveRequest)value).setContinueRequest(strValue);
          return true;}
        else if (qName==QName_0_82) {
          ((RetrieveRequest)value).setQueryAllAccounts(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_83) {
          ((RetrieveRequest)value).setRetrieveAllSinceLastBatch(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_84) {
          ((RetrieveRequest)value).setRepeatLastResult(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return false;
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return false;
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_79) {
          ((RetrieveRequest)value).setFilter((com.exacttarget.FilterPart)objValue);
          return true;}
        else if (qName==QName_0_85) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.Request[] array = new com.exacttarget.Request[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((RetrieveRequest)value).setRetrieves(array);
          } else { 
            ((RetrieveRequest)value).setRetrieves((com.exacttarget.Request[])objValue);}
          return true;}
        else if (qName==QName_0_57) {
          ((RetrieveRequest)value).setOptions((com.exacttarget.RetrieveOptions)objValue);
          return true;}
        return false;
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_76) {
          com.exacttarget.ClientID[] array = new com.exacttarget.ClientID[listValue.size()];
          listValue.toArray(array);
          ((RetrieveRequest)value).setClientIDs(array);
          return true;}
        else if (qName==QName_0_78) {
          java.lang.String[] array = new java.lang.String[listValue.size()];
          listValue.toArray(array);
          ((RetrieveRequest)value).setProperties(array);
          return true;}
        else if (qName==QName_0_80) {
          com.exacttarget.AsyncResponse[] array = new com.exacttarget.AsyncResponse[listValue.size()];
          listValue.toArray(array);
          ((RetrieveRequest)value).setRespondTo(array);
          return true;}
        else if (qName==QName_0_2) {
          com.exacttarget.APIProperty[] array = new com.exacttarget.APIProperty[listValue.size()];
          listValue.toArray(array);
          ((RetrieveRequest)value).setPartnerProperties(array);
          return true;}
        return false;
    }
    private final static javax.xml.namespace.QName QName_0_81 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContinueRequest");
    private final static javax.xml.namespace.QName QName_0_78 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Properties");
    private final static javax.xml.namespace.QName QName_0_85 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Retrieves");
    private final static javax.xml.namespace.QName QName_0_83 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RetrieveAllSinceLastBatch");
    private final static javax.xml.namespace.QName QName_0_77 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ObjectType");
    private final static javax.xml.namespace.QName QName_0_82 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "QueryAllAccounts");
    private final static javax.xml.namespace.QName QName_0_57 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Options");
    private final static javax.xml.namespace.QName QName_0_84 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RepeatLastResult");
    private final static javax.xml.namespace.QName QName_0_2 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerProperties");
    private final static javax.xml.namespace.QName QName_0_76 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ClientIDs");
    private final static javax.xml.namespace.QName QName_0_80 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RespondTo");
    private final static javax.xml.namespace.QName QName_0_79 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Filter");
}
