/**
 * SubscriberList.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberList  extends com.exacttarget.APIObject  {
    private com.exacttarget.SubscriberStatus status;
    private com.exacttarget.List list;
    private java.lang.String action;
    private com.exacttarget.Subscriber subscriber;

    public SubscriberList() {
    }

    public com.exacttarget.SubscriberStatus getStatus() {
        return status;
    }

    public void setStatus(com.exacttarget.SubscriberStatus status) {
        this.status = status;
    }

    public com.exacttarget.List getList() {
        return list;
    }

    public void setList(com.exacttarget.List list) {
        this.list = list;
    }

    public java.lang.String getAction() {
        return action;
    }

    public void setAction(java.lang.String action) {
        this.action = action;
    }

    public com.exacttarget.Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(com.exacttarget.Subscriber subscriber) {
        this.subscriber = subscriber;
    }

}
