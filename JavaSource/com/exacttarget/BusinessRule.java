/**
 * BusinessRule.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class BusinessRule  extends com.exacttarget.APIObject  {
    private int memberBusinessRuleID;
    private int businessRuleID;
    private int data;
    private java.lang.String quality;
    private java.lang.String name;
    private java.lang.String type;
    private java.lang.String description;
    private java.lang.Boolean isViewable;
    private java.lang.Boolean isInheritedFromParent;
    private java.lang.String displayName;
    private java.lang.String productCode;

    public BusinessRule() {
    }

    public int getMemberBusinessRuleID() {
        return memberBusinessRuleID;
    }

    public void setMemberBusinessRuleID(int memberBusinessRuleID) {
        this.memberBusinessRuleID = memberBusinessRuleID;
    }

    public int getBusinessRuleID() {
        return businessRuleID;
    }

    public void setBusinessRuleID(int businessRuleID) {
        this.businessRuleID = businessRuleID;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public java.lang.String getQuality() {
        return quality;
    }

    public void setQuality(java.lang.String quality) {
        this.quality = quality;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.Boolean getIsViewable() {
        return isViewable;
    }

    public void setIsViewable(java.lang.Boolean isViewable) {
        this.isViewable = isViewable;
    }

    public java.lang.Boolean getIsInheritedFromParent() {
        return isInheritedFromParent;
    }

    public void setIsInheritedFromParent(java.lang.Boolean isInheritedFromParent) {
        this.isInheritedFromParent = isInheritedFromParent;
    }

    public java.lang.String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }

    public java.lang.String getProductCode() {
        return productCode;
    }

    public void setProductCode(java.lang.String productCode) {
        this.productCode = productCode;
    }

}
