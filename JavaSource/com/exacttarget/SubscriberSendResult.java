/**
 * SubscriberSendResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberSendResult  extends com.exacttarget.APIObject  {
    private com.exacttarget.Send send;
    private com.exacttarget.Email email;
    private com.exacttarget.Subscriber subscriber;
    private java.util.Calendar clickDate;
    private java.util.Calendar bounceDate;
    private java.util.Calendar openDate;
    private java.util.Calendar sentDate;
    private java.lang.String lastAction;
    private java.util.Calendar unsubscribeDate;
    private java.lang.String fromAddress;
    private java.lang.String fromName;
    private java.lang.Integer totalClicks;
    private java.lang.Integer uniqueClicks;
    private java.lang.String subject;
    private java.lang.String viewSentEmailURL;
    private java.lang.Integer hardBounces;
    private java.lang.Integer softBounces;
    private java.lang.Integer otherBounces;

    public SubscriberSendResult() {
    }

    public com.exacttarget.Send getSend() {
        return send;
    }

    public void setSend(com.exacttarget.Send send) {
        this.send = send;
    }

    public com.exacttarget.Email getEmail() {
        return email;
    }

    public void setEmail(com.exacttarget.Email email) {
        this.email = email;
    }

    public com.exacttarget.Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(com.exacttarget.Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public java.util.Calendar getClickDate() {
        return clickDate;
    }

    public void setClickDate(java.util.Calendar clickDate) {
        this.clickDate = clickDate;
    }

    public java.util.Calendar getBounceDate() {
        return bounceDate;
    }

    public void setBounceDate(java.util.Calendar bounceDate) {
        this.bounceDate = bounceDate;
    }

    public java.util.Calendar getOpenDate() {
        return openDate;
    }

    public void setOpenDate(java.util.Calendar openDate) {
        this.openDate = openDate;
    }

    public java.util.Calendar getSentDate() {
        return sentDate;
    }

    public void setSentDate(java.util.Calendar sentDate) {
        this.sentDate = sentDate;
    }

    public java.lang.String getLastAction() {
        return lastAction;
    }

    public void setLastAction(java.lang.String lastAction) {
        this.lastAction = lastAction;
    }

    public java.util.Calendar getUnsubscribeDate() {
        return unsubscribeDate;
    }

    public void setUnsubscribeDate(java.util.Calendar unsubscribeDate) {
        this.unsubscribeDate = unsubscribeDate;
    }

    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.Integer getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(java.lang.Integer totalClicks) {
        this.totalClicks = totalClicks;
    }

    public java.lang.Integer getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(java.lang.Integer uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

    public java.lang.String getSubject() {
        return subject;
    }

    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }

    public java.lang.String getViewSentEmailURL() {
        return viewSentEmailURL;
    }

    public void setViewSentEmailURL(java.lang.String viewSentEmailURL) {
        this.viewSentEmailURL = viewSentEmailURL;
    }

    public java.lang.Integer getHardBounces() {
        return hardBounces;
    }

    public void setHardBounces(java.lang.Integer hardBounces) {
        this.hardBounces = hardBounces;
    }

    public java.lang.Integer getSoftBounces() {
        return softBounces;
    }

    public void setSoftBounces(java.lang.Integer softBounces) {
        this.softBounces = softBounces;
    }

    public java.lang.Integer getOtherBounces() {
        return otherBounces;
    }

    public void setOtherBounces(java.lang.Integer otherBounces) {
        this.otherBounces = otherBounces;
    }

}
