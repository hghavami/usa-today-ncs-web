/**
 * SimpleFilterPart.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SimpleFilterPart  extends com.exacttarget.FilterPart  {
    private java.lang.String property;
    private com.exacttarget.SimpleOperators simpleOperator;
    private java.lang.String[] value;
    private java.util.Calendar[] dateValue;

    public SimpleFilterPart() {
    }

    public java.lang.String getProperty() {
        return property;
    }

    public void setProperty(java.lang.String property) {
        this.property = property;
    }

    public com.exacttarget.SimpleOperators getSimpleOperator() {
        return simpleOperator;
    }

    public void setSimpleOperator(com.exacttarget.SimpleOperators simpleOperator) {
        this.simpleOperator = simpleOperator;
    }

    public java.lang.String[] getValue() {
        return value;
    }

    public void setValue(java.lang.String[] value) {
        this.value = value;
    }

    public java.lang.String getValue(int i) {
        return this.value[i];
    }

    public void setValue(int i, java.lang.String value) {
        this.value[i] = value;
    }

    public java.util.Calendar[] getDateValue() {
        return dateValue;
    }

    public void setDateValue(java.util.Calendar[] dateValue) {
        this.dateValue = dateValue;
    }

    public java.util.Calendar getDateValue(int i) {
        return this.dateValue[i];
    }

    public void setDateValue(int i, java.util.Calendar value) {
        this.dateValue[i] = value;
    }

}
