/**
 * APIObject.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class APIObject  {
    private com.exacttarget.ClientID client;
    private java.lang.String partnerKey;
    private com.exacttarget.APIProperty[] partnerProperties;
    private java.util.Calendar createdDate;
    private java.util.Calendar modifiedDate;
    private java.lang.Integer ID;
    private java.lang.String objectID;
    private java.lang.String customerKey;
    private com.exacttarget.Owner owner;
    private java.lang.String correlationID;
    private java.lang.String objectState;

    public APIObject() {
    }

    public com.exacttarget.ClientID getClient() {
        return client;
    }

    public void setClient(com.exacttarget.ClientID client) {
        this.client = client;
    }

    public java.lang.String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(java.lang.String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public com.exacttarget.APIProperty[] getPartnerProperties() {
        return partnerProperties;
    }

    public void setPartnerProperties(com.exacttarget.APIProperty[] partnerProperties) {
        this.partnerProperties = partnerProperties;
    }

    public com.exacttarget.APIProperty getPartnerProperties(int i) {
        return this.partnerProperties[i];
    }

    public void setPartnerProperties(int i, com.exacttarget.APIProperty value) {
        this.partnerProperties[i] = value;
    }

    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }

    public java.util.Calendar getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(java.util.Calendar modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public java.lang.Integer getID() {
        return ID;
    }

    public void setID(java.lang.Integer ID) {
        this.ID = ID;
    }

    public java.lang.String getObjectID() {
        return objectID;
    }

    public void setObjectID(java.lang.String objectID) {
        this.objectID = objectID;
    }

    public java.lang.String getCustomerKey() {
        return customerKey;
    }

    public void setCustomerKey(java.lang.String customerKey) {
        this.customerKey = customerKey;
    }

    public com.exacttarget.Owner getOwner() {
        return owner;
    }

    public void setOwner(com.exacttarget.Owner owner) {
        this.owner = owner;
    }

    public java.lang.String getCorrelationID() {
        return correlationID;
    }

    public void setCorrelationID(java.lang.String correlationID) {
        this.correlationID = correlationID;
    }

    public java.lang.String getObjectState() {
        return objectState;
    }

    public void setObjectState(java.lang.String objectState) {
        this.objectState = objectState;
    }

}
