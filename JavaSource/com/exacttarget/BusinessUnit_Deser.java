/**
 * BusinessUnit_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class BusinessUnit_Deser extends com.exacttarget.Account_Deser {
    /**
     * Constructor
     */
    public BusinessUnit_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new BusinessUnit();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_134) {
          ((BusinessUnit)value).setDescription(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_708) {
          ((BusinessUnit)value).setDefaultSendClassification((com.exacttarget.SendClassification)objValue);
          return true;}
        else if (qName==QName_0_709) {
          ((BusinessUnit)value).setDefaultHomePage((com.exacttarget.LandingPage)objValue);
          return true;}
        else if (qName==QName_0_710) {
          ((BusinessUnit)value).setSubscriberFilter((com.exacttarget.FilterPart)objValue);
          return true;}
        else if (qName==QName_0_711) {
          ((BusinessUnit)value).setMasterUnsubscribeBehavior((com.exacttarget.UnsubscribeBehaviorEnum)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_711 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MasterUnsubscribeBehavior");
    private final static javax.xml.namespace.QName QName_0_708 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultSendClassification");
    private final static javax.xml.namespace.QName QName_0_710 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberFilter");
    private final static javax.xml.namespace.QName QName_0_709 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultHomePage");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
}
