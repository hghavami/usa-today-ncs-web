/**
 * ComplexFilterPart_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ComplexFilterPart_Ser extends com.exacttarget.FilterPart_Ser {
    /**
     * Constructor
     */
    public ComplexFilterPart_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_103;
           context.qName2String(elemQName, true);
           elemQName = QName_0_104;
           context.qName2String(elemQName, true);
           elemQName = QName_0_105;
           context.qName2String(elemQName, true);
           elemQName = QName_0_686;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        ComplexFilterPart bean = (ComplexFilterPart) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_103;
          propValue = bean.getLeftOperand();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_86,
              true,null,context);
          propQName = QName_0_104;
          propValue = bean.getLogicalOperator();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_106,
              true,null,context);
          propQName = QName_0_105;
          propValue = bean.getRightOperand();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_86,
              false,null,context);
          propQName = QName_0_686;
          propValue = bean.getAdditionalOperands();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_687,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_103 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LeftOperand");
    private final static javax.xml.namespace.QName QName_0_86 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FilterPart");
    private final static javax.xml.namespace.QName QName_0_105 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RightOperand");
    private final static javax.xml.namespace.QName QName_0_106 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LogicalOperators");
    private final static javax.xml.namespace.QName QName_0_686 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AdditionalOperands");
    private final static javax.xml.namespace.QName QName_0_104 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LogicalOperator");
    private final static javax.xml.namespace.QName QName_0_687 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">ComplexFilterPart>AdditionalOperands");
}
