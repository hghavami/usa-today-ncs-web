/**
 * TriggeredSendDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class TriggeredSendDefinition_Deser extends com.exacttarget.SendDefinition_Deser {
    /**
     * Constructor
     */
    public TriggeredSendDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new TriggeredSendDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_463) {
          ((TriggeredSendDefinition)value).setAutoAddSubscribers(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_464) {
          ((TriggeredSendDefinition)value).setAutoUpdateSubscribers(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_465) {
          ((TriggeredSendDefinition)value).setBatchInterval(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_466) {
          ((TriggeredSendDefinition)value).setBccEmail(strValue);
          return true;}
        else if (qName==QName_0_467) {
          ((TriggeredSendDefinition)value).setEmailSubject(strValue);
          return true;}
        else if (qName==QName_0_468) {
          ((TriggeredSendDefinition)value).setDynamicEmailSubject(strValue);
          return true;}
        else if (qName==QName_0_422) {
          ((TriggeredSendDefinition)value).setIsMultipart(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_469) {
          ((TriggeredSendDefinition)value).setIsWrapped(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_470) {
          ((TriggeredSendDefinition)value).setAllowedSlots(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseShort(strValue));
          return true;}
        else if (qName==QName_0_471) {
          ((TriggeredSendDefinition)value).setNewSlotTrigger(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_423) {
          ((TriggeredSendDefinition)value).setSendLimit(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_424) {
          ((TriggeredSendDefinition)value).setSendWindowOpen(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_425) {
          ((TriggeredSendDefinition)value).setSendWindowClose(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_472) {
          ((TriggeredSendDefinition)value).setSendWindowDelete(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_473) {
          ((TriggeredSendDefinition)value).setRefreshContent(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_474) {
          ((TriggeredSendDefinition)value).setExclusionFilter(strValue);
          return true;}
        else if (qName==QName_0_45) {
          ((TriggeredSendDefinition)value).setPriority(strValue);
          return true;}
        else if (qName==QName_0_475) {
          ((TriggeredSendDefinition)value).setSendSourceCustomerKey(strValue);
          return true;}
        else if (qName==QName_0_477) {
          ((TriggeredSendDefinition)value).setCCEmail(strValue);
          return true;}
        else if (qName==QName_0_426) {
          ((TriggeredSendDefinition)value).setIsAlwaysOn(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_744) {
          ((TriggeredSendDefinition)value).setDisableOnEmailBuildError(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_461) {
          ((TriggeredSendDefinition)value).setTriggeredSendType((com.exacttarget.TriggeredSendTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_462) {
          ((TriggeredSendDefinition)value).setTriggeredSendStatus((com.exacttarget.TriggeredSendStatusEnum)objValue);
          return true;}
        else if (qName==QName_0_178) {
          ((TriggeredSendDefinition)value).setEmail((com.exacttarget.Email)objValue);
          return true;}
        else if (qName==QName_0_390) {
          ((TriggeredSendDefinition)value).setList((com.exacttarget.List)objValue);
          return true;}
        else if (qName==QName_0_478) {
          ((TriggeredSendDefinition)value).setSendSourceDataExtension((com.exacttarget.DataExtension)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_476) {
          com.exacttarget.TriggeredSendExclusionList[] array = new com.exacttarget.TriggeredSendExclusionList[listValue.size()];
          listValue.toArray(array);
          ((TriggeredSendDefinition)value).setExclusionListCollection(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_464 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoUpdateSubscribers");
    private final static javax.xml.namespace.QName QName_0_473 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RefreshContent");
    private final static javax.xml.namespace.QName QName_0_472 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowDelete");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_461 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendType");
    private final static javax.xml.namespace.QName QName_0_469 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsWrapped");
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_467 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailSubject");
    private final static javax.xml.namespace.QName QName_0_470 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AllowedSlots");
    private final static javax.xml.namespace.QName QName_0_462 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendStatus");
    private final static javax.xml.namespace.QName QName_0_45 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Priority");
    private final static javax.xml.namespace.QName QName_0_476 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionListCollection");
    private final static javax.xml.namespace.QName QName_0_475 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendSourceCustomerKey");
    private final static javax.xml.namespace.QName QName_0_474 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionFilter");
    private final static javax.xml.namespace.QName QName_0_468 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DynamicEmailSubject");
    private final static javax.xml.namespace.QName QName_0_477 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CCEmail");
    private final static javax.xml.namespace.QName QName_0_478 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendSourceDataExtension");
    private final static javax.xml.namespace.QName QName_0_465 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BatchInterval");
    private final static javax.xml.namespace.QName QName_0_422 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsMultipart");
    private final static javax.xml.namespace.QName QName_0_426 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAlwaysOn");
    private final static javax.xml.namespace.QName QName_0_424 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowOpen");
    private final static javax.xml.namespace.QName QName_0_425 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowClose");
    private final static javax.xml.namespace.QName QName_0_463 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoAddSubscribers");
    private final static javax.xml.namespace.QName QName_0_471 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NewSlotTrigger");
    private final static javax.xml.namespace.QName QName_0_744 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DisableOnEmailBuildError");
    private final static javax.xml.namespace.QName QName_0_466 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BccEmail");
    private final static javax.xml.namespace.QName QName_0_423 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendLimit");
}
