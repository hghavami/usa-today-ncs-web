/**
 * SenderProfile.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SenderProfile  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.String fromName;
    private java.lang.String fromAddress;
    private java.lang.Boolean useDefaultRMMRules;
    private java.lang.String autoForwardToEmailAddress;
    private java.lang.String autoForwardToName;
    private java.lang.Boolean directForward;
    private com.exacttarget.TriggeredSendDefinition autoForwardTriggeredSend;
    private java.lang.Boolean autoReply;
    private com.exacttarget.TriggeredSendDefinition autoReplyTriggeredSend;
    private java.lang.String senderHeaderEmailAddress;
    private java.lang.String senderHeaderName;
    private java.lang.Short dataRetentionPeriodLength;
    private com.exacttarget.RecurrenceTypeEnum dataRetentionPeriodUnitOfMeasure;
    private com.exacttarget.APIObject replyManagementRuleSet;

    public SenderProfile() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public java.lang.Boolean getUseDefaultRMMRules() {
        return useDefaultRMMRules;
    }

    public void setUseDefaultRMMRules(java.lang.Boolean useDefaultRMMRules) {
        this.useDefaultRMMRules = useDefaultRMMRules;
    }

    public java.lang.String getAutoForwardToEmailAddress() {
        return autoForwardToEmailAddress;
    }

    public void setAutoForwardToEmailAddress(java.lang.String autoForwardToEmailAddress) {
        this.autoForwardToEmailAddress = autoForwardToEmailAddress;
    }

    public java.lang.String getAutoForwardToName() {
        return autoForwardToName;
    }

    public void setAutoForwardToName(java.lang.String autoForwardToName) {
        this.autoForwardToName = autoForwardToName;
    }

    public java.lang.Boolean getDirectForward() {
        return directForward;
    }

    public void setDirectForward(java.lang.Boolean directForward) {
        this.directForward = directForward;
    }

    public com.exacttarget.TriggeredSendDefinition getAutoForwardTriggeredSend() {
        return autoForwardTriggeredSend;
    }

    public void setAutoForwardTriggeredSend(com.exacttarget.TriggeredSendDefinition autoForwardTriggeredSend) {
        this.autoForwardTriggeredSend = autoForwardTriggeredSend;
    }

    public java.lang.Boolean getAutoReply() {
        return autoReply;
    }

    public void setAutoReply(java.lang.Boolean autoReply) {
        this.autoReply = autoReply;
    }

    public com.exacttarget.TriggeredSendDefinition getAutoReplyTriggeredSend() {
        return autoReplyTriggeredSend;
    }

    public void setAutoReplyTriggeredSend(com.exacttarget.TriggeredSendDefinition autoReplyTriggeredSend) {
        this.autoReplyTriggeredSend = autoReplyTriggeredSend;
    }

    public java.lang.String getSenderHeaderEmailAddress() {
        return senderHeaderEmailAddress;
    }

    public void setSenderHeaderEmailAddress(java.lang.String senderHeaderEmailAddress) {
        this.senderHeaderEmailAddress = senderHeaderEmailAddress;
    }

    public java.lang.String getSenderHeaderName() {
        return senderHeaderName;
    }

    public void setSenderHeaderName(java.lang.String senderHeaderName) {
        this.senderHeaderName = senderHeaderName;
    }

    public java.lang.Short getDataRetentionPeriodLength() {
        return dataRetentionPeriodLength;
    }

    public void setDataRetentionPeriodLength(java.lang.Short dataRetentionPeriodLength) {
        this.dataRetentionPeriodLength = dataRetentionPeriodLength;
    }

    public com.exacttarget.RecurrenceTypeEnum getDataRetentionPeriodUnitOfMeasure() {
        return dataRetentionPeriodUnitOfMeasure;
    }

    public void setDataRetentionPeriodUnitOfMeasure(com.exacttarget.RecurrenceTypeEnum dataRetentionPeriodUnitOfMeasure) {
        this.dataRetentionPeriodUnitOfMeasure = dataRetentionPeriodUnitOfMeasure;
    }

    public com.exacttarget.APIObject getReplyManagementRuleSet() {
        return replyManagementRuleSet;
    }

    public void setReplyManagementRuleSet(com.exacttarget.APIObject replyManagementRuleSet) {
        this.replyManagementRuleSet = replyManagementRuleSet;
    }

}
