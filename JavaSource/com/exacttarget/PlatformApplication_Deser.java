/**
 * PlatformApplication_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PlatformApplication_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public PlatformApplication_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new PlatformApplication();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_775) {
          ((PlatformApplication)value).setDeveloperVersion(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_773) {
          ((PlatformApplication)value).set_package((com.exacttarget.PlatformApplicationPackage)objValue);
          return true;}
        else if (qName==QName_0_645) {
          ((PlatformApplication)value).setResourceSpecification((com.exacttarget.ResourceSpecification)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_774) {
          com.exacttarget.PlatformApplicationPackage[] array = new com.exacttarget.PlatformApplicationPackage[listValue.size()];
          listValue.toArray(array);
          ((PlatformApplication)value).setPackages(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_775 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeveloperVersion");
    private final static javax.xml.namespace.QName QName_0_774 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Packages");
    private final static javax.xml.namespace.QName QName_0_773 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Package");
    private final static javax.xml.namespace.QName QName_0_645 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ResourceSpecification");
}
