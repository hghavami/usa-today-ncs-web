/**
 * SendDefinitionList_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SendDefinitionList_Deser extends com.exacttarget.AudienceItem_Deser {
    /**
     * Constructor
     */
    public SendDefinitionList_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new SendDefinitionList();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_561) {
          ((SendDefinitionList)value).setIsTestObject(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_562) {
          ((SendDefinitionList)value).setSalesForceObjectID(strValue);
          return true;}
        else if (qName==QName_0_20) {
          ((SendDefinitionList)value).setName(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_560) {
          ((SendDefinitionList)value).setFilterDefinition((com.exacttarget.FilterDefinition)objValue);
          return true;}
        else if (qName==QName_0_93) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.APIProperty[] array = new com.exacttarget.APIProperty[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((SendDefinitionList)value).setParameters(array);
          } else { 
            ((SendDefinitionList)value).setParameters((com.exacttarget.APIProperty[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_562 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SalesForceObjectID");
    private final static javax.xml.namespace.QName QName_0_560 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FilterDefinition");
    private final static javax.xml.namespace.QName QName_0_561 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsTestObject");
    private final static javax.xml.namespace.QName QName_0_93 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Parameters");
}
