/**
 * DataExtensionCreateResult_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class DataExtensionCreateResult_Deser extends com.exacttarget.CreateResult_Deser {
    /**
     * Constructor
     */
    public DataExtensionCreateResult_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new DataExtensionCreateResult();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_584) {
          ((DataExtensionCreateResult)value).setErrorMessage(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_586) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.DataExtensionError[] array = new com.exacttarget.DataExtensionError[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((DataExtensionCreateResult)value).setKeyErrors(array);
          } else { 
            ((DataExtensionCreateResult)value).setKeyErrors((com.exacttarget.DataExtensionError[])objValue);}
          return true;}
        else if (qName==QName_0_587) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.DataExtensionError[] array = new com.exacttarget.DataExtensionError[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((DataExtensionCreateResult)value).setValueErrors(array);
          } else { 
            ((DataExtensionCreateResult)value).setValueErrors((com.exacttarget.DataExtensionError[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_586 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "KeyErrors");
    private final static javax.xml.namespace.QName QName_0_584 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ErrorMessage");
    private final static javax.xml.namespace.QName QName_0_587 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ValueErrors");
}
