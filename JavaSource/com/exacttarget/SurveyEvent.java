/**
 * SurveyEvent.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SurveyEvent  extends com.exacttarget.TrackingEvent  {
    private java.lang.String question;
    private java.lang.String answer;

    public SurveyEvent() {
    }

    public java.lang.String getQuestion() {
        return question;
    }

    public void setQuestion(java.lang.String question) {
        this.question = question;
    }

    public java.lang.String getAnswer() {
        return answer;
    }

    public void setAnswer(java.lang.String answer) {
        this.answer = answer;
    }

}
