/**
 * SystemStatusResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SystemStatusResult  extends com.exacttarget.Result  {
    private com.exacttarget.SystemStatusType systemStatus;
    private com.exacttarget.SystemOutage[] outages;

    public SystemStatusResult() {
    }

    public com.exacttarget.SystemStatusType getSystemStatus() {
        return systemStatus;
    }

    public void setSystemStatus(com.exacttarget.SystemStatusType systemStatus) {
        this.systemStatus = systemStatus;
    }

    public com.exacttarget.SystemOutage[] getOutages() {
        return outages;
    }

    public void setOutages(com.exacttarget.SystemOutage[] outages) {
        this.outages = outages;
    }

}
