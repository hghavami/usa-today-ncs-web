/**
 * TriggeredSendDefinition_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class TriggeredSendDefinition_Ser extends com.exacttarget.SendDefinition_Ser {
    /**
     * Constructor
     */
    public TriggeredSendDefinition_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_461;
           context.qName2String(elemQName, true);
           elemQName = QName_0_462;
           context.qName2String(elemQName, true);
           elemQName = QName_0_178;
           context.qName2String(elemQName, true);
           elemQName = QName_0_390;
           context.qName2String(elemQName, true);
           elemQName = QName_0_463;
           context.qName2String(elemQName, true);
           elemQName = QName_0_464;
           context.qName2String(elemQName, true);
           elemQName = QName_0_465;
           context.qName2String(elemQName, true);
           elemQName = QName_0_466;
           context.qName2String(elemQName, true);
           elemQName = QName_0_467;
           context.qName2String(elemQName, true);
           elemQName = QName_0_468;
           context.qName2String(elemQName, true);
           elemQName = QName_0_422;
           context.qName2String(elemQName, true);
           elemQName = QName_0_469;
           context.qName2String(elemQName, true);
           elemQName = QName_0_470;
           context.qName2String(elemQName, true);
           elemQName = QName_0_471;
           context.qName2String(elemQName, true);
           elemQName = QName_0_423;
           context.qName2String(elemQName, true);
           elemQName = QName_0_424;
           context.qName2String(elemQName, true);
           elemQName = QName_0_425;
           context.qName2String(elemQName, true);
           elemQName = QName_0_472;
           context.qName2String(elemQName, true);
           elemQName = QName_0_473;
           context.qName2String(elemQName, true);
           elemQName = QName_0_474;
           context.qName2String(elemQName, true);
           elemQName = QName_0_45;
           context.qName2String(elemQName, true);
           elemQName = QName_0_475;
           context.qName2String(elemQName, true);
           elemQName = QName_0_476;
           context.qName2String(elemQName, true);
           elemQName = QName_0_477;
           context.qName2String(elemQName, true);
           elemQName = QName_0_478;
           context.qName2String(elemQName, true);
           elemQName = QName_0_426;
           context.qName2String(elemQName, true);
           elemQName = QName_0_744;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        TriggeredSendDefinition bean = (TriggeredSendDefinition) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_461;
          propValue = bean.getTriggeredSendType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_479,
              false,null,context);
          propQName = QName_0_462;
          propValue = bean.getTriggeredSendStatus();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_480,
              false,null,context);
          propQName = QName_0_178;
          propValue = bean.getEmail();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_178,
              false,null,context);
          propQName = QName_0_390;
          propValue = bean.getList();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_390,
              false,null,context);
          propQName = QName_0_463;
          propValue = bean.getAutoAddSubscribers();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_464;
          propValue = bean.getAutoUpdateSubscribers();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_465;
          propValue = bean.getBatchInterval();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_466;
          propValue = bean.getBccEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_467;
          propValue = bean.getEmailSubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_468;
          propValue = bean.getDynamicEmailSubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_422;
          propValue = bean.getIsMultipart();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_469;
          propValue = bean.getIsWrapped();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_470;
          propValue = bean.getAllowedSlots();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_481,
              false,null,context);
          propQName = QName_0_471;
          propValue = bean.getNewSlotTrigger();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_423;
          propValue = bean.getSendLimit();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_424;
          propValue = bean.getSendWindowOpen();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_433,
              false,null,context);
          propQName = QName_0_425;
          propValue = bean.getSendWindowClose();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_433,
              false,null,context);
          propQName = QName_0_472;
          propValue = bean.getSendWindowDelete();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_473;
          propValue = bean.getRefreshContent();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_474;
          propValue = bean.getExclusionFilter();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_45;
          propValue = bean.getPriority();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_475;
          propValue = bean.getSendSourceCustomerKey();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_476;
          {
            propValue = bean.getExclusionListCollection();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_482,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_477;
          propValue = bean.getCCEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_478;
          propValue = bean.getSendSourceDataExtension();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_483,
              false,null,context);
          propQName = QName_0_426;
          propValue = bean.getIsAlwaysOn();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_744;
          propValue = bean.getDisableOnEmailBuildError();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_471 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NewSlotTrigger");
    private final static javax.xml.namespace.QName QName_0_470 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AllowedSlots");
    private final static javax.xml.namespace.QName QName_0_744 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DisableOnEmailBuildError");
    private final static javax.xml.namespace.QName QName_1_481 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "short");
    private final static javax.xml.namespace.QName QName_0_426 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAlwaysOn");
    private final static javax.xml.namespace.QName QName_0_423 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendLimit");
    private final static javax.xml.namespace.QName QName_0_483 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataExtension");
    private final static javax.xml.namespace.QName QName_0_464 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoUpdateSubscribers");
    private final static javax.xml.namespace.QName QName_0_478 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendSourceDataExtension");
    private final static javax.xml.namespace.QName QName_0_479 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendTypeEnum");
    private final static javax.xml.namespace.QName QName_0_461 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendType");
    private final static javax.xml.namespace.QName QName_0_473 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RefreshContent");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_422 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsMultipart");
    private final static javax.xml.namespace.QName QName_0_465 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BatchInterval");
    private final static javax.xml.namespace.QName QName_0_467 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailSubject");
    private final static javax.xml.namespace.QName QName_0_466 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BccEmail");
    private final static javax.xml.namespace.QName QName_0_480 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendStatusEnum");
    private final static javax.xml.namespace.QName QName_0_468 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DynamicEmailSubject");
    private final static javax.xml.namespace.QName QName_0_424 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowOpen");
    private final static javax.xml.namespace.QName QName_0_462 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendStatus");
    private final static javax.xml.namespace.QName QName_0_425 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowClose");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_476 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionListCollection");
    private final static javax.xml.namespace.QName QName_0_472 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowDelete");
    private final static javax.xml.namespace.QName QName_1_433 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "time");
    private final static javax.xml.namespace.QName QName_0_474 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionFilter");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_463 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoAddSubscribers");
    private final static javax.xml.namespace.QName QName_0_475 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendSourceCustomerKey");
    private final static javax.xml.namespace.QName QName_0_477 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CCEmail");
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_469 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsWrapped");
    private final static javax.xml.namespace.QName QName_0_482 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendExclusionList");
    private final static javax.xml.namespace.QName QName_0_45 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Priority");
}
