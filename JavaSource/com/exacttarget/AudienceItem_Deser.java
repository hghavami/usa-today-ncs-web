/**
 * AudienceItem_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class AudienceItem_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public AudienceItem_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new AudienceItem();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_502) {
          ((AudienceItem)value).setCustomObjectID(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_390) {
          ((AudienceItem)value).setList((com.exacttarget.List)objValue);
          return true;}
        else if (qName==QName_0_501) {
          ((AudienceItem)value).setSendDefinitionListType((com.exacttarget.SendDefinitionListTypeEnum)objValue);
          return true;}
        else if (qName==QName_0_503) {
          ((AudienceItem)value).setDataSourceTypeID((com.exacttarget.DataSourceTypeEnum)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_503 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataSourceTypeID");
    private final static javax.xml.namespace.QName QName_0_501 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendDefinitionListType");
    private final static javax.xml.namespace.QName QName_0_502 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CustomObjectID");
}
