/**
 * Layout.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Layout  extends com.exacttarget.APIObject  {
    private java.lang.String layoutName;

    public Layout() {
    }

    public java.lang.String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(java.lang.String layoutName) {
        this.layoutName = layoutName;
    }

}
