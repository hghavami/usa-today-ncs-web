/**
 * SystemStatusRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SystemStatusRequestMsg  {
    private com.exacttarget.SystemStatusOptions options;

    public SystemStatusRequestMsg() {
    }

    public com.exacttarget.SystemStatusOptions getOptions() {
        return options;
    }

    public void setOptions(com.exacttarget.SystemStatusOptions options) {
        this.options = options;
    }

}
