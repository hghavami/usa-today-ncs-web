/**
 * VoiceTriggeredSend_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class VoiceTriggeredSend_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public VoiceTriggeredSend_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new VoiceTriggeredSend();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_221) {
          ((VoiceTriggeredSend)value).setMessage(strValue);
          return true;}
        else if (qName==QName_0_537) {
          ((VoiceTriggeredSend)value).setNumber(strValue);
          return true;}
        else if (qName==QName_0_745) {
          ((VoiceTriggeredSend)value).setTransferMessage(strValue);
          return true;}
        else if (qName==QName_0_746) {
          ((VoiceTriggeredSend)value).setTransferNumber(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_536) {
          ((VoiceTriggeredSend)value).setVoiceTriggeredSendDefinition((com.exacttarget.VoiceTriggeredSendDefinition)objValue);
          return true;}
        else if (qName==QName_0_214) {
          ((VoiceTriggeredSend)value).setSubscriber((com.exacttarget.Subscriber)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_537 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Number");
    private final static javax.xml.namespace.QName QName_0_745 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TransferMessage");
    private final static javax.xml.namespace.QName QName_0_536 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "VoiceTriggeredSendDefinition");
    private final static javax.xml.namespace.QName QName_0_221 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Message");
    private final static javax.xml.namespace.QName QName_0_214 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscriber");
    private final static javax.xml.namespace.QName QName_0_746 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TransferNumber");
}
