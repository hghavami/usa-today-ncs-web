/**
 * Subscription.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Subscription  {
    private java.lang.Integer subscriptionID;
    private int emailsPurchased;
    private int accountsPurchased;
    private int advAccountsPurchased;
    private int LPAccountsPurchased;
    private int DOTOAccountsPurchased;
    private int BUAccountsPurchased;
    private java.util.Calendar beginDate;
    private java.util.Calendar endDate;
    private java.lang.String notes;
    private java.lang.String period;
    private java.lang.String notificationTitle;
    private java.lang.String notificationMessage;
    private java.lang.String notificationFlag;
    private java.util.Calendar notificationExpDate;
    private java.lang.String forAccounting;
    private boolean hasPurchasedEmails;
    private java.lang.String contractNumber;
    private java.lang.String contractModifier;
    private java.lang.Boolean isRenewal;
    private long numberofEmails;

    public Subscription() {
    }

    public java.lang.Integer getSubscriptionID() {
        return subscriptionID;
    }

    public void setSubscriptionID(java.lang.Integer subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    public int getEmailsPurchased() {
        return emailsPurchased;
    }

    public void setEmailsPurchased(int emailsPurchased) {
        this.emailsPurchased = emailsPurchased;
    }

    public int getAccountsPurchased() {
        return accountsPurchased;
    }

    public void setAccountsPurchased(int accountsPurchased) {
        this.accountsPurchased = accountsPurchased;
    }

    public int getAdvAccountsPurchased() {
        return advAccountsPurchased;
    }

    public void setAdvAccountsPurchased(int advAccountsPurchased) {
        this.advAccountsPurchased = advAccountsPurchased;
    }

    public int getLPAccountsPurchased() {
        return LPAccountsPurchased;
    }

    public void setLPAccountsPurchased(int LPAccountsPurchased) {
        this.LPAccountsPurchased = LPAccountsPurchased;
    }

    public int getDOTOAccountsPurchased() {
        return DOTOAccountsPurchased;
    }

    public void setDOTOAccountsPurchased(int DOTOAccountsPurchased) {
        this.DOTOAccountsPurchased = DOTOAccountsPurchased;
    }

    public int getBUAccountsPurchased() {
        return BUAccountsPurchased;
    }

    public void setBUAccountsPurchased(int BUAccountsPurchased) {
        this.BUAccountsPurchased = BUAccountsPurchased;
    }

    public java.util.Calendar getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(java.util.Calendar beginDate) {
        this.beginDate = beginDate;
    }

    public java.util.Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }

    public java.lang.String getNotes() {
        return notes;
    }

    public void setNotes(java.lang.String notes) {
        this.notes = notes;
    }

    public java.lang.String getPeriod() {
        return period;
    }

    public void setPeriod(java.lang.String period) {
        this.period = period;
    }

    public java.lang.String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(java.lang.String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public java.lang.String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(java.lang.String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public java.lang.String getNotificationFlag() {
        return notificationFlag;
    }

    public void setNotificationFlag(java.lang.String notificationFlag) {
        this.notificationFlag = notificationFlag;
    }

    public java.util.Calendar getNotificationExpDate() {
        return notificationExpDate;
    }

    public void setNotificationExpDate(java.util.Calendar notificationExpDate) {
        this.notificationExpDate = notificationExpDate;
    }

    public java.lang.String getForAccounting() {
        return forAccounting;
    }

    public void setForAccounting(java.lang.String forAccounting) {
        this.forAccounting = forAccounting;
    }

    public boolean isHasPurchasedEmails() {
        return hasPurchasedEmails;
    }

    public void setHasPurchasedEmails(boolean hasPurchasedEmails) {
        this.hasPurchasedEmails = hasPurchasedEmails;
    }

    public java.lang.String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(java.lang.String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public java.lang.String getContractModifier() {
        return contractModifier;
    }

    public void setContractModifier(java.lang.String contractModifier) {
        this.contractModifier = contractModifier;
    }

    public java.lang.Boolean getIsRenewal() {
        return isRenewal;
    }

    public void setIsRenewal(java.lang.Boolean isRenewal) {
        this.isRenewal = isRenewal;
    }

    public long getNumberofEmails() {
        return numberofEmails;
    }

    public void setNumberofEmails(long numberofEmails) {
        this.numberofEmails = numberofEmails;
    }

}
