/**
 * SendDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendDefinition  extends com.exacttarget.InteractionDefinition  {
    private java.lang.Integer categoryID;
    private com.exacttarget.SendClassification sendClassification;
    private com.exacttarget.SenderProfile senderProfile;
    private java.lang.String fromName;
    private java.lang.String fromAddress;
    private com.exacttarget.DeliveryProfile deliveryProfile;
    private com.exacttarget.DeliveryProfileSourceAddressTypeEnum sourceAddressType;
    private com.exacttarget.PrivateIP privateIP;
    private com.exacttarget.DeliveryProfileDomainTypeEnum domainType;
    private com.exacttarget.PrivateDomain privateDomain;
    private com.exacttarget.SalutationSourceEnum headerSalutationSource;
    private com.exacttarget.ContentArea headerContentArea;
    private com.exacttarget.SalutationSourceEnum footerSalutationSource;
    private com.exacttarget.ContentArea footerContentArea;
    private java.lang.Boolean suppressTracking;
    private java.lang.Boolean isSendLogging;

    public SendDefinition() {
    }

    public java.lang.Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(java.lang.Integer categoryID) {
        this.categoryID = categoryID;
    }

    public com.exacttarget.SendClassification getSendClassification() {
        return sendClassification;
    }

    public void setSendClassification(com.exacttarget.SendClassification sendClassification) {
        this.sendClassification = sendClassification;
    }

    public com.exacttarget.SenderProfile getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(com.exacttarget.SenderProfile senderProfile) {
        this.senderProfile = senderProfile;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public com.exacttarget.DeliveryProfile getDeliveryProfile() {
        return deliveryProfile;
    }

    public void setDeliveryProfile(com.exacttarget.DeliveryProfile deliveryProfile) {
        this.deliveryProfile = deliveryProfile;
    }

    public com.exacttarget.DeliveryProfileSourceAddressTypeEnum getSourceAddressType() {
        return sourceAddressType;
    }

    public void setSourceAddressType(com.exacttarget.DeliveryProfileSourceAddressTypeEnum sourceAddressType) {
        this.sourceAddressType = sourceAddressType;
    }

    public com.exacttarget.PrivateIP getPrivateIP() {
        return privateIP;
    }

    public void setPrivateIP(com.exacttarget.PrivateIP privateIP) {
        this.privateIP = privateIP;
    }

    public com.exacttarget.DeliveryProfileDomainTypeEnum getDomainType() {
        return domainType;
    }

    public void setDomainType(com.exacttarget.DeliveryProfileDomainTypeEnum domainType) {
        this.domainType = domainType;
    }

    public com.exacttarget.PrivateDomain getPrivateDomain() {
        return privateDomain;
    }

    public void setPrivateDomain(com.exacttarget.PrivateDomain privateDomain) {
        this.privateDomain = privateDomain;
    }

    public com.exacttarget.SalutationSourceEnum getHeaderSalutationSource() {
        return headerSalutationSource;
    }

    public void setHeaderSalutationSource(com.exacttarget.SalutationSourceEnum headerSalutationSource) {
        this.headerSalutationSource = headerSalutationSource;
    }

    public com.exacttarget.ContentArea getHeaderContentArea() {
        return headerContentArea;
    }

    public void setHeaderContentArea(com.exacttarget.ContentArea headerContentArea) {
        this.headerContentArea = headerContentArea;
    }

    public com.exacttarget.SalutationSourceEnum getFooterSalutationSource() {
        return footerSalutationSource;
    }

    public void setFooterSalutationSource(com.exacttarget.SalutationSourceEnum footerSalutationSource) {
        this.footerSalutationSource = footerSalutationSource;
    }

    public com.exacttarget.ContentArea getFooterContentArea() {
        return footerContentArea;
    }

    public void setFooterContentArea(com.exacttarget.ContentArea footerContentArea) {
        this.footerContentArea = footerContentArea;
    }

    public java.lang.Boolean getSuppressTracking() {
        return suppressTracking;
    }

    public void setSuppressTracking(java.lang.Boolean suppressTracking) {
        this.suppressTracking = suppressTracking;
    }

    public java.lang.Boolean getIsSendLogging() {
        return isSendLogging;
    }

    public void setIsSendLogging(java.lang.Boolean isSendLogging) {
        this.isSendLogging = isSendLogging;
    }

}
