/**
 * ExecuteResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExecuteResponse  {
    private java.lang.String statusCode;
    private java.lang.String statusMessage;
    private java.lang.Integer ordinalID;
    private com.exacttarget.APIProperty[] results;
    private java.lang.Integer errorCode;

    public ExecuteResponse() {
    }

    public java.lang.String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }

    public java.lang.String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(java.lang.String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public java.lang.Integer getOrdinalID() {
        return ordinalID;
    }

    public void setOrdinalID(java.lang.Integer ordinalID) {
        this.ordinalID = ordinalID;
    }

    public com.exacttarget.APIProperty[] getResults() {
        return results;
    }

    public void setResults(com.exacttarget.APIProperty[] results) {
        this.results = results;
    }

    public com.exacttarget.APIProperty getResults(int i) {
        return this.results[i];
    }

    public void setResults(int i, com.exacttarget.APIProperty value) {
        this.results[i] = value;
    }

    public java.lang.Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(java.lang.Integer errorCode) {
        this.errorCode = errorCode;
    }

}
