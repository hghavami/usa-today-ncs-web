/**
 * Locale.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Locale  extends com.exacttarget.APIObject  {
    private java.lang.String localeCode;

    public Locale() {
    }

    public java.lang.String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(java.lang.String localeCode) {
        this.localeCode = localeCode;
    }

}
