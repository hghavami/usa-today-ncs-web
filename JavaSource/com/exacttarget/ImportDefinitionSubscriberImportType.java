/**
 * ImportDefinitionSubscriberImportType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ImportDefinitionSubscriberImportType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ImportDefinitionSubscriberImportType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Email = "Email";
    public static final java.lang.String _SMS = "SMS";
    public static final ImportDefinitionSubscriberImportType Email = new ImportDefinitionSubscriberImportType(_Email);
    public static final ImportDefinitionSubscriberImportType SMS = new ImportDefinitionSubscriberImportType(_SMS);
    public java.lang.String getValue() { return _value_;}
    public static ImportDefinitionSubscriberImportType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ImportDefinitionSubscriberImportType enumeration = (ImportDefinitionSubscriberImportType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ImportDefinitionSubscriberImportType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
