/**
 * ListAttribute_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ListAttribute_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public ListAttribute_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ListAttribute();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((ListAttribute)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((ListAttribute)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_728) {
          ((ListAttribute)value).setFieldLength(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_132) {
          ((ListAttribute)value).setScale(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_138) {
          ((ListAttribute)value).setMinValue(strValue);
          return true;}
        else if (qName==QName_0_139) {
          ((ListAttribute)value).setMaxValue(strValue);
          return true;}
        else if (qName==QName_0_135) {
          ((ListAttribute)value).setDefaultValue(strValue);
          return true;}
        else if (qName==QName_0_729) {
          ((ListAttribute)value).setIsNullable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_730) {
          ((ListAttribute)value).setIsHidden(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_731) {
          ((ListAttribute)value).setIsReadOnly(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_732) {
          ((ListAttribute)value).setInheritable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_733) {
          ((ListAttribute)value).setOverridable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_734) {
          ((ListAttribute)value).setMustOverride(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_510) {
          ((ListAttribute)value).setOrdinal(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_390) {
          ((ListAttribute)value).setList((com.exacttarget.List)objValue);
          return true;}
        else if (qName==QName_0_579) {
          ((ListAttribute)value).setFieldType((com.exacttarget.ListAttributeFieldType)objValue);
          return true;}
        else if (qName==QName_0_735) {
          ((ListAttribute)value).setOverrideType((com.exacttarget.OverrideType)objValue);
          return true;}
        else if (qName==QName_0_737) {
          ((ListAttribute)value).setBaseAttribute((com.exacttarget.ListAttribute)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_736) {
          com.exacttarget.ListAttributeRestrictedValue[] array = new com.exacttarget.ListAttributeRestrictedValue[listValue.size()];
          listValue.toArray(array);
          ((ListAttribute)value).setRestrictedValues(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_735 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OverrideType");
    private final static javax.xml.namespace.QName QName_0_138 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MinValue");
    private final static javax.xml.namespace.QName QName_0_132 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Scale");
    private final static javax.xml.namespace.QName QName_0_733 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Overridable");
    private final static javax.xml.namespace.QName QName_0_729 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsNullable");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_736 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RestrictedValues");
    private final static javax.xml.namespace.QName QName_0_139 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxValue");
    private final static javax.xml.namespace.QName QName_0_732 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Inheritable");
    private final static javax.xml.namespace.QName QName_0_135 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultValue");
    private final static javax.xml.namespace.QName QName_0_579 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FieldType");
    private final static javax.xml.namespace.QName QName_0_731 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsReadOnly");
    private final static javax.xml.namespace.QName QName_0_730 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsHidden");
    private final static javax.xml.namespace.QName QName_0_728 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FieldLength");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_734 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MustOverride");
    private final static javax.xml.namespace.QName QName_0_510 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Ordinal");
    private final static javax.xml.namespace.QName QName_0_737 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BaseAttribute");
}
