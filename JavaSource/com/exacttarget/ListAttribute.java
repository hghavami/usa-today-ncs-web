/**
 * ListAttribute.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ListAttribute  extends com.exacttarget.APIObject  {
    private com.exacttarget.List list;
    private java.lang.String name;
    private java.lang.String description;
    private com.exacttarget.ListAttributeFieldType fieldType;
    private java.lang.Integer fieldLength;
    private java.lang.Integer scale;
    private java.lang.String minValue;
    private java.lang.String maxValue;
    private java.lang.String defaultValue;
    private java.lang.Boolean isNullable;
    private java.lang.Boolean isHidden;
    private java.lang.Boolean isReadOnly;
    private java.lang.Boolean inheritable;
    private java.lang.Boolean overridable;
    private java.lang.Boolean mustOverride;
    private com.exacttarget.OverrideType overrideType;
    private java.lang.Integer ordinal;
    private com.exacttarget.ListAttributeRestrictedValue[] restrictedValues;
    private com.exacttarget.ListAttribute baseAttribute;

    public ListAttribute() {
    }

    public com.exacttarget.List getList() {
        return list;
    }

    public void setList(com.exacttarget.List list) {
        this.list = list;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.ListAttributeFieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(com.exacttarget.ListAttributeFieldType fieldType) {
        this.fieldType = fieldType;
    }

    public java.lang.Integer getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(java.lang.Integer fieldLength) {
        this.fieldLength = fieldLength;
    }

    public java.lang.Integer getScale() {
        return scale;
    }

    public void setScale(java.lang.Integer scale) {
        this.scale = scale;
    }

    public java.lang.String getMinValue() {
        return minValue;
    }

    public void setMinValue(java.lang.String minValue) {
        this.minValue = minValue;
    }

    public java.lang.String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(java.lang.String maxValue) {
        this.maxValue = maxValue;
    }

    public java.lang.String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(java.lang.String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public java.lang.Boolean getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(java.lang.Boolean isNullable) {
        this.isNullable = isNullable;
    }

    public java.lang.Boolean getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(java.lang.Boolean isHidden) {
        this.isHidden = isHidden;
    }

    public java.lang.Boolean getIsReadOnly() {
        return isReadOnly;
    }

    public void setIsReadOnly(java.lang.Boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }

    public java.lang.Boolean getInheritable() {
        return inheritable;
    }

    public void setInheritable(java.lang.Boolean inheritable) {
        this.inheritable = inheritable;
    }

    public java.lang.Boolean getOverridable() {
        return overridable;
    }

    public void setOverridable(java.lang.Boolean overridable) {
        this.overridable = overridable;
    }

    public java.lang.Boolean getMustOverride() {
        return mustOverride;
    }

    public void setMustOverride(java.lang.Boolean mustOverride) {
        this.mustOverride = mustOverride;
    }

    public com.exacttarget.OverrideType getOverrideType() {
        return overrideType;
    }

    public void setOverrideType(com.exacttarget.OverrideType overrideType) {
        this.overrideType = overrideType;
    }

    public java.lang.Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(java.lang.Integer ordinal) {
        this.ordinal = ordinal;
    }

    public com.exacttarget.ListAttributeRestrictedValue[] getRestrictedValues() {
        return restrictedValues;
    }

    public void setRestrictedValues(com.exacttarget.ListAttributeRestrictedValue[] restrictedValues) {
        this.restrictedValues = restrictedValues;
    }

    public com.exacttarget.ListAttributeRestrictedValue getRestrictedValues(int i) {
        return this.restrictedValues[i];
    }

    public void setRestrictedValues(int i, com.exacttarget.ListAttributeRestrictedValue value) {
        this.restrictedValues[i] = value;
    }

    public com.exacttarget.ListAttribute getBaseAttribute() {
        return baseAttribute;
    }

    public void setBaseAttribute(com.exacttarget.ListAttribute baseAttribute) {
        this.baseAttribute = baseAttribute;
    }

}
