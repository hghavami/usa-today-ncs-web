/**
 * AccountUser_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class AccountUser_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public AccountUser_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new AccountUser();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_350) {
          ((AccountUser)value).setAccountUserID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_16) {
          ((AccountUser)value).setUserID(strValue);
          return true;}
        else if (qName==QName_0_351) {
          ((AccountUser)value).setPassword(strValue);
          return true;}
        else if (qName==QName_0_20) {
          ((AccountUser)value).setName(strValue);
          return true;}
        else if (qName==QName_0_178) {
          ((AccountUser)value).setEmail(strValue);
          return true;}
        else if (qName==QName_0_352) {
          ((AccountUser)value).setMustChangePassword(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_353) {
          ((AccountUser)value).setActiveFlag(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_354) {
          ((AccountUser)value).setChallengePhrase(strValue);
          return true;}
        else if (qName==QName_0_355) {
          ((AccountUser)value).setChallengeAnswer(strValue);
          return true;}
        else if (qName==QName_0_340) {
          ((AccountUser)value).setDelete(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_357) {
          ((AccountUser)value).setLastSuccessfulLogin(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_358) {
          ((AccountUser)value).setIsAPIUser(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_359) {
          ((AccountUser)value).setNotificationEmailAddress(strValue);
          return true;}
        else if (qName==QName_0_360) {
          ((AccountUser)value).setIsLocked(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_361) {
          ((AccountUser)value).setUnlock(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_362) {
          ((AccountUser)value).setBusinessUnit(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_363) {
          ((AccountUser)value).setDefaultBusinessUnit(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_679) {
          ((AccountUser)value).setLocale((com.exacttarget.Locale)objValue);
          return true;}
        else if (qName==QName_0_680) {
          ((AccountUser)value).setTimeZone((com.exacttarget.TimeZone)objValue);
          return true;}
        else if (qName==QName_0_681) {
          ((AccountUser)value).setDefaultBusinessUnitObject((com.exacttarget.BusinessUnit)objValue);
          return true;}
        else if (qName==QName_0_682) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.BusinessUnit[] array = new com.exacttarget.BusinessUnit[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((AccountUser)value).setAssociatedBusinessUnits(array);
          } else { 
            ((AccountUser)value).setAssociatedBusinessUnits((com.exacttarget.BusinessUnit[])objValue);}
          return true;}
        else if (qName==QName_0_683) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.Role[] array = new com.exacttarget.Role[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((AccountUser)value).setRoles(array);
          } else { 
            ((AccountUser)value).setRoles((com.exacttarget.Role[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_356) {
          com.exacttarget.UserAccess[] array = new com.exacttarget.UserAccess[listValue.size()];
          listValue.toArray(array);
          ((AccountUser)value).setUserPermissions(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_356 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserPermissions");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_353 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ActiveFlag");
    private final static javax.xml.namespace.QName QName_0_363 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultBusinessUnit");
    private final static javax.xml.namespace.QName QName_0_351 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Password");
    private final static javax.xml.namespace.QName QName_0_680 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TimeZone");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_361 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Unlock");
    private final static javax.xml.namespace.QName QName_0_350 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountUserID");
    private final static javax.xml.namespace.QName QName_0_683 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Roles");
    private final static javax.xml.namespace.QName QName_0_359 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationEmailAddress");
    private final static javax.xml.namespace.QName QName_0_354 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ChallengePhrase");
    private final static javax.xml.namespace.QName QName_0_355 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ChallengeAnswer");
    private final static javax.xml.namespace.QName QName_0_357 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LastSuccessfulLogin");
    private final static javax.xml.namespace.QName QName_0_16 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserID");
    private final static javax.xml.namespace.QName QName_0_360 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsLocked");
    private final static javax.xml.namespace.QName QName_0_682 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AssociatedBusinessUnits");
    private final static javax.xml.namespace.QName QName_0_358 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAPIUser");
    private final static javax.xml.namespace.QName QName_0_681 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultBusinessUnitObject");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_352 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MustChangePassword");
    private final static javax.xml.namespace.QName QName_0_362 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BusinessUnit");
    private final static javax.xml.namespace.QName QName_0_340 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Delete");
}
