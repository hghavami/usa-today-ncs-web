/**
 * Attribute.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Attribute  {
    private java.lang.String name;
    private java.lang.String value;
    private com.exacttarget.CompressionConfiguration compression;

    public Attribute() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getValue() {
        return value;
    }

    public void setValue(java.lang.String value) {
        this.value = value;
    }

    public com.exacttarget.CompressionConfiguration getCompression() {
        return compression;
    }

    public void setCompression(com.exacttarget.CompressionConfiguration compression) {
        this.compression = compression;
    }

}
