/**
 * PropertyDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PropertyDefinition  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String dataType;
    private com.exacttarget.SoapType valueType;
    private com.exacttarget.PropertyType propertyType;
    private java.lang.Boolean isCreatable;
    private java.lang.Boolean isUpdatable;
    private java.lang.Boolean isRetrievable;
    private java.lang.Boolean isQueryable;
    private java.lang.Boolean isFilterable;
    private java.lang.Boolean isPartnerProperty;
    private java.lang.Boolean isAccountProperty;
    private java.lang.String partnerMap;
    private com.exacttarget.AttributeMap[] attributeMaps;
    private com.exacttarget.APIProperty[] markups;
    private java.lang.Integer precision;
    private java.lang.Integer scale;
    private java.lang.String label;
    private java.lang.String description;
    private java.lang.String defaultValue;
    private java.lang.Integer minLength;
    private java.lang.Integer maxLength;
    private java.lang.String minValue;
    private java.lang.String maxValue;
    private java.lang.Boolean isRequired;
    private java.lang.Boolean isViewable;
    private java.lang.Boolean isEditable;
    private java.lang.Boolean isNillable;
    private java.lang.Boolean isRestrictedPicklist;
    private com.exacttarget.PicklistItem[] picklistItems;
    private java.lang.Boolean isSendTime;
    private java.lang.Integer displayOrder;
    private com.exacttarget.APIObject[] references;
    private java.lang.String relationshipName;
    private java.lang.String status;
    private java.lang.Boolean isContextSpecific;

    public PropertyDefinition() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDataType() {
        return dataType;
    }

    public void setDataType(java.lang.String dataType) {
        this.dataType = dataType;
    }

    public com.exacttarget.SoapType getValueType() {
        return valueType;
    }

    public void setValueType(com.exacttarget.SoapType valueType) {
        this.valueType = valueType;
    }

    public com.exacttarget.PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(com.exacttarget.PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public java.lang.Boolean getIsCreatable() {
        return isCreatable;
    }

    public void setIsCreatable(java.lang.Boolean isCreatable) {
        this.isCreatable = isCreatable;
    }

    public java.lang.Boolean getIsUpdatable() {
        return isUpdatable;
    }

    public void setIsUpdatable(java.lang.Boolean isUpdatable) {
        this.isUpdatable = isUpdatable;
    }

    public java.lang.Boolean getIsRetrievable() {
        return isRetrievable;
    }

    public void setIsRetrievable(java.lang.Boolean isRetrievable) {
        this.isRetrievable = isRetrievable;
    }

    public java.lang.Boolean getIsQueryable() {
        return isQueryable;
    }

    public void setIsQueryable(java.lang.Boolean isQueryable) {
        this.isQueryable = isQueryable;
    }

    public java.lang.Boolean getIsFilterable() {
        return isFilterable;
    }

    public void setIsFilterable(java.lang.Boolean isFilterable) {
        this.isFilterable = isFilterable;
    }

    public java.lang.Boolean getIsPartnerProperty() {
        return isPartnerProperty;
    }

    public void setIsPartnerProperty(java.lang.Boolean isPartnerProperty) {
        this.isPartnerProperty = isPartnerProperty;
    }

    public java.lang.Boolean getIsAccountProperty() {
        return isAccountProperty;
    }

    public void setIsAccountProperty(java.lang.Boolean isAccountProperty) {
        this.isAccountProperty = isAccountProperty;
    }

    public java.lang.String getPartnerMap() {
        return partnerMap;
    }

    public void setPartnerMap(java.lang.String partnerMap) {
        this.partnerMap = partnerMap;
    }

    public com.exacttarget.AttributeMap[] getAttributeMaps() {
        return attributeMaps;
    }

    public void setAttributeMaps(com.exacttarget.AttributeMap[] attributeMaps) {
        this.attributeMaps = attributeMaps;
    }

    public com.exacttarget.AttributeMap getAttributeMaps(int i) {
        return this.attributeMaps[i];
    }

    public void setAttributeMaps(int i, com.exacttarget.AttributeMap value) {
        this.attributeMaps[i] = value;
    }

    public com.exacttarget.APIProperty[] getMarkups() {
        return markups;
    }

    public void setMarkups(com.exacttarget.APIProperty[] markups) {
        this.markups = markups;
    }

    public com.exacttarget.APIProperty getMarkups(int i) {
        return this.markups[i];
    }

    public void setMarkups(int i, com.exacttarget.APIProperty value) {
        this.markups[i] = value;
    }

    public java.lang.Integer getPrecision() {
        return precision;
    }

    public void setPrecision(java.lang.Integer precision) {
        this.precision = precision;
    }

    public java.lang.Integer getScale() {
        return scale;
    }

    public void setScale(java.lang.Integer scale) {
        this.scale = scale;
    }

    public java.lang.String getLabel() {
        return label;
    }

    public void setLabel(java.lang.String label) {
        this.label = label;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(java.lang.String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public java.lang.Integer getMinLength() {
        return minLength;
    }

    public void setMinLength(java.lang.Integer minLength) {
        this.minLength = minLength;
    }

    public java.lang.Integer getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(java.lang.Integer maxLength) {
        this.maxLength = maxLength;
    }

    public java.lang.String getMinValue() {
        return minValue;
    }

    public void setMinValue(java.lang.String minValue) {
        this.minValue = minValue;
    }

    public java.lang.String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(java.lang.String maxValue) {
        this.maxValue = maxValue;
    }

    public java.lang.Boolean getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(java.lang.Boolean isRequired) {
        this.isRequired = isRequired;
    }

    public java.lang.Boolean getIsViewable() {
        return isViewable;
    }

    public void setIsViewable(java.lang.Boolean isViewable) {
        this.isViewable = isViewable;
    }

    public java.lang.Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(java.lang.Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public java.lang.Boolean getIsNillable() {
        return isNillable;
    }

    public void setIsNillable(java.lang.Boolean isNillable) {
        this.isNillable = isNillable;
    }

    public java.lang.Boolean getIsRestrictedPicklist() {
        return isRestrictedPicklist;
    }

    public void setIsRestrictedPicklist(java.lang.Boolean isRestrictedPicklist) {
        this.isRestrictedPicklist = isRestrictedPicklist;
    }

    public com.exacttarget.PicklistItem[] getPicklistItems() {
        return picklistItems;
    }

    public void setPicklistItems(com.exacttarget.PicklistItem[] picklistItems) {
        this.picklistItems = picklistItems;
    }

    public java.lang.Boolean getIsSendTime() {
        return isSendTime;
    }

    public void setIsSendTime(java.lang.Boolean isSendTime) {
        this.isSendTime = isSendTime;
    }

    public java.lang.Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(java.lang.Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public com.exacttarget.APIObject[] getReferences() {
        return references;
    }

    public void setReferences(com.exacttarget.APIObject[] references) {
        this.references = references;
    }

    public java.lang.String getRelationshipName() {
        return relationshipName;
    }

    public void setRelationshipName(java.lang.String relationshipName) {
        this.relationshipName = relationshipName;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.Boolean getIsContextSpecific() {
        return isContextSpecific;
    }

    public void setIsContextSpecific(java.lang.Boolean isContextSpecific) {
        this.isContextSpecific = isContextSpecific;
    }

}
