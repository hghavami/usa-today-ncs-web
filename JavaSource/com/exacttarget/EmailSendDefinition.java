/**
 * EmailSendDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class EmailSendDefinition  extends com.exacttarget.SendDefinition  {
    private com.exacttarget.SendDefinitionList[] sendDefinitionList;
    private com.exacttarget.Email email;
    private java.lang.String bccEmail;
    private java.lang.String autoBccEmail;
    private java.lang.String testEmailAddr;
    private java.lang.String emailSubject;
    private java.lang.String dynamicEmailSubject;
    private java.lang.Boolean isMultipart;
    private java.lang.Boolean isWrapped;
    private java.lang.Integer sendLimit;
    private java.util.Calendar sendWindowOpen;
    private java.util.Calendar sendWindowClose;
    private java.lang.Boolean sendWindowDelete;
    private java.lang.Boolean deduplicateByEmail;
    private java.lang.String exclusionFilter;
    private com.exacttarget.TrackingUser[] trackingUsers;
    private java.lang.String additional;
    private java.lang.String CCEmail;

    public EmailSendDefinition() {
    }

    public com.exacttarget.SendDefinitionList[] getSendDefinitionList() {
        return sendDefinitionList;
    }

    public void setSendDefinitionList(com.exacttarget.SendDefinitionList[] sendDefinitionList) {
        this.sendDefinitionList = sendDefinitionList;
    }

    public com.exacttarget.SendDefinitionList getSendDefinitionList(int i) {
        return this.sendDefinitionList[i];
    }

    public void setSendDefinitionList(int i, com.exacttarget.SendDefinitionList value) {
        this.sendDefinitionList[i] = value;
    }

    public com.exacttarget.Email getEmail() {
        return email;
    }

    public void setEmail(com.exacttarget.Email email) {
        this.email = email;
    }

    public java.lang.String getBccEmail() {
        return bccEmail;
    }

    public void setBccEmail(java.lang.String bccEmail) {
        this.bccEmail = bccEmail;
    }

    public java.lang.String getAutoBccEmail() {
        return autoBccEmail;
    }

    public void setAutoBccEmail(java.lang.String autoBccEmail) {
        this.autoBccEmail = autoBccEmail;
    }

    public java.lang.String getTestEmailAddr() {
        return testEmailAddr;
    }

    public void setTestEmailAddr(java.lang.String testEmailAddr) {
        this.testEmailAddr = testEmailAddr;
    }

    public java.lang.String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(java.lang.String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public java.lang.String getDynamicEmailSubject() {
        return dynamicEmailSubject;
    }

    public void setDynamicEmailSubject(java.lang.String dynamicEmailSubject) {
        this.dynamicEmailSubject = dynamicEmailSubject;
    }

    public java.lang.Boolean getIsMultipart() {
        return isMultipart;
    }

    public void setIsMultipart(java.lang.Boolean isMultipart) {
        this.isMultipart = isMultipart;
    }

    public java.lang.Boolean getIsWrapped() {
        return isWrapped;
    }

    public void setIsWrapped(java.lang.Boolean isWrapped) {
        this.isWrapped = isWrapped;
    }

    public java.lang.Integer getSendLimit() {
        return sendLimit;
    }

    public void setSendLimit(java.lang.Integer sendLimit) {
        this.sendLimit = sendLimit;
    }

    public java.util.Calendar getSendWindowOpen() {
        return sendWindowOpen;
    }

    public void setSendWindowOpen(java.util.Calendar sendWindowOpen) {
        this.sendWindowOpen = sendWindowOpen;
    }

    public java.util.Calendar getSendWindowClose() {
        return sendWindowClose;
    }

    public void setSendWindowClose(java.util.Calendar sendWindowClose) {
        this.sendWindowClose = sendWindowClose;
    }

    public java.lang.Boolean getSendWindowDelete() {
        return sendWindowDelete;
    }

    public void setSendWindowDelete(java.lang.Boolean sendWindowDelete) {
        this.sendWindowDelete = sendWindowDelete;
    }

    public java.lang.Boolean getDeduplicateByEmail() {
        return deduplicateByEmail;
    }

    public void setDeduplicateByEmail(java.lang.Boolean deduplicateByEmail) {
        this.deduplicateByEmail = deduplicateByEmail;
    }

    public java.lang.String getExclusionFilter() {
        return exclusionFilter;
    }

    public void setExclusionFilter(java.lang.String exclusionFilter) {
        this.exclusionFilter = exclusionFilter;
    }

    public com.exacttarget.TrackingUser[] getTrackingUsers() {
        return trackingUsers;
    }

    public void setTrackingUsers(com.exacttarget.TrackingUser[] trackingUsers) {
        this.trackingUsers = trackingUsers;
    }

    public java.lang.String getAdditional() {
        return additional;
    }

    public void setAdditional(java.lang.String additional) {
        this.additional = additional;
    }

    public java.lang.String getCCEmail() {
        return CCEmail;
    }

    public void setCCEmail(java.lang.String CCEmail) {
        this.CCEmail = CCEmail;
    }

}
