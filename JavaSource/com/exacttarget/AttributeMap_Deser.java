/**
 * AttributeMap_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class AttributeMap_Deser extends com.exacttarget.APIProperty_Deser {
    /**
     * Constructor
     */
    public AttributeMap_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new AttributeMap();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_156) {
          ((AttributeMap)value).setEntityName(strValue);
          return true;}
        else if (qName==QName_0_157) {
          ((AttributeMap)value).setColumnName(strValue);
          return true;}
        else if (qName==QName_0_158) {
          ((AttributeMap)value).setColumnNameMappedTo(strValue);
          return true;}
        else if (qName==QName_0_159) {
          ((AttributeMap)value).setEntityNameMappedTo(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_160) {
          com.exacttarget.APIProperty[] array = new com.exacttarget.APIProperty[listValue.size()];
          listValue.toArray(array);
          ((AttributeMap)value).setAdditionalData(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_158 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ColumnNameMappedTo");
    private final static javax.xml.namespace.QName QName_0_160 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AdditionalData");
    private final static javax.xml.namespace.QName QName_0_157 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ColumnName");
    private final static javax.xml.namespace.QName QName_0_159 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EntityNameMappedTo");
    private final static javax.xml.namespace.QName QName_0_156 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EntityName");
}
