/**
 * SendDefinitionListTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendDefinitionListTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SendDefinitionListTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _SourceList = "SourceList";
    public static final java.lang.String _ExclusionList = "ExclusionList";
    public static final java.lang.String _DomainExclusion = "DomainExclusion";
    public static final java.lang.String _OptOutList = "OptOutList";
    public static final SendDefinitionListTypeEnum SourceList = new SendDefinitionListTypeEnum(_SourceList);
    public static final SendDefinitionListTypeEnum ExclusionList = new SendDefinitionListTypeEnum(_ExclusionList);
    public static final SendDefinitionListTypeEnum DomainExclusion = new SendDefinitionListTypeEnum(_DomainExclusion);
    public static final SendDefinitionListTypeEnum OptOutList = new SendDefinitionListTypeEnum(_OptOutList);
    public java.lang.String getValue() { return _value_;}
    public static SendDefinitionListTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SendDefinitionListTypeEnum enumeration = (SendDefinitionListTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SendDefinitionListTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
