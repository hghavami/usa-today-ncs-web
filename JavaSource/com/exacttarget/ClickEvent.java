/**
 * ClickEvent.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ClickEvent  extends com.exacttarget.TrackingEvent  {
    private java.lang.Integer URLID;
    private java.lang.String URL;

    public ClickEvent() {
    }

    public java.lang.Integer getURLID() {
        return URLID;
    }

    public void setURLID(java.lang.Integer URLID) {
        this.URLID = URLID;
    }

    public java.lang.String getURL() {
        return URL;
    }

    public void setURL(java.lang.String URL) {
        this.URL = URL;
    }

}
