/**
 * DataFolder.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DataFolder  extends com.exacttarget.APIObject  {
    private com.exacttarget.DataFolder parentFolder;
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.String contentType;
    private java.lang.Boolean isActive;
    private java.lang.Boolean isEditable;
    private java.lang.Boolean allowChildren;

    public DataFolder() {
    }

    public com.exacttarget.DataFolder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(com.exacttarget.DataFolder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getContentType() {
        return contentType;
    }

    public void setContentType(java.lang.String contentType) {
        this.contentType = contentType;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(java.lang.Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public java.lang.Boolean getAllowChildren() {
        return allowChildren;
    }

    public void setAllowChildren(java.lang.Boolean allowChildren) {
        this.allowChildren = allowChildren;
    }

}
