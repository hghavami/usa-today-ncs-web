/**
 * ContainerID.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ContainerID  {
    private com.exacttarget.APIObject APIObject;

    public ContainerID() {
    }

    public com.exacttarget.APIObject getAPIObject() {
        return APIObject;
    }

    public void setAPIObject(com.exacttarget.APIObject APIObject) {
        this.APIObject = APIObject;
    }

}
