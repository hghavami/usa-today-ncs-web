/**
 * Permission.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Permission  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.String objectType;
    private java.lang.String operation;
    private java.lang.Boolean isShareable;
    private java.lang.Boolean isAllowed;
    private java.lang.Boolean isDenied;

    public Permission() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getObjectType() {
        return objectType;
    }

    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }

    public java.lang.String getOperation() {
        return operation;
    }

    public void setOperation(java.lang.String operation) {
        this.operation = operation;
    }

    public java.lang.Boolean getIsShareable() {
        return isShareable;
    }

    public void setIsShareable(java.lang.Boolean isShareable) {
        this.isShareable = isShareable;
    }

    public java.lang.Boolean getIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(java.lang.Boolean isAllowed) {
        this.isAllowed = isAllowed;
    }

    public java.lang.Boolean getIsDenied() {
        return isDenied;
    }

    public void setIsDenied(java.lang.Boolean isDenied) {
        this.isDenied = isDenied;
    }

}
