/**
 * DeliveryProfile_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class DeliveryProfile_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public DeliveryProfile_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_20;
           context.qName2String(elemQName, true);
           elemQName = QName_0_134;
           context.qName2String(elemQName, true);
           elemQName = QName_0_448;
           context.qName2String(elemQName, true);
           elemQName = QName_0_449;
           context.qName2String(elemQName, true);
           elemQName = QName_0_450;
           context.qName2String(elemQName, true);
           elemQName = QName_0_451;
           context.qName2String(elemQName, true);
           elemQName = QName_0_452;
           context.qName2String(elemQName, true);
           elemQName = QName_0_453;
           context.qName2String(elemQName, true);
           elemQName = QName_0_454;
           context.qName2String(elemQName, true);
           elemQName = QName_0_455;
           context.qName2String(elemQName, true);
           elemQName = QName_0_747;
           context.qName2String(elemQName, true);
           elemQName = QName_0_748;
           context.qName2String(elemQName, true);
           elemQName = QName_0_749;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        DeliveryProfile bean = (DeliveryProfile) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_20;
          propValue = bean.getName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_134;
          propValue = bean.getDescription();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_448;
          propValue = bean.getSourceAddressType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_458,
              false,null,context);
          propQName = QName_0_449;
          propValue = bean.getPrivateIP();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_449,
              false,null,context);
          propQName = QName_0_450;
          propValue = bean.getDomainType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_459,
              false,null,context);
          propQName = QName_0_451;
          propValue = bean.getPrivateDomain();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_451,
              false,null,context);
          propQName = QName_0_452;
          propValue = bean.getHeaderSalutationSource();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_460,
              false,null,context);
          propQName = QName_0_453;
          propValue = bean.getHeaderContentArea();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_194,
              false,null,context);
          propQName = QName_0_454;
          propValue = bean.getFooterSalutationSource();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_460,
              false,null,context);
          propQName = QName_0_455;
          propValue = bean.getFooterContentArea();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_194,
              false,null,context);
          propQName = QName_0_747;
          propValue = bean.getSubscriberLevelPrivateDomain();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_748;
          propValue = bean.getSMIMESignatureCertificate();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_750,
              false,null,context);
          propQName = QName_0_749;
          propValue = bean.getPrivateDomainSet();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_749,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_449 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateIP");
    private final static javax.xml.namespace.QName QName_0_458 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeliveryProfileSourceAddressTypeEnum");
    private final static javax.xml.namespace.QName QName_0_748 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SMIMESignatureCertificate");
    private final static javax.xml.namespace.QName QName_0_452 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderSalutationSource");
    private final static javax.xml.namespace.QName QName_0_459 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeliveryProfileDomainTypeEnum");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_448 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SourceAddressType");
    private final static javax.xml.namespace.QName QName_0_454 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterSalutationSource");
    private final static javax.xml.namespace.QName QName_0_747 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberLevelPrivateDomain");
    private final static javax.xml.namespace.QName QName_0_194 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContentArea");
    private final static javax.xml.namespace.QName QName_0_451 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateDomain");
    private final static javax.xml.namespace.QName QName_0_750 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Certificate");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_460 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SalutationSourceEnum");
    private final static javax.xml.namespace.QName QName_0_450 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DomainType");
    private final static javax.xml.namespace.QName QName_0_453 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderContentArea");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_749 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrivateDomainSet");
    private final static javax.xml.namespace.QName QName_0_455 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FooterContentArea");
}
