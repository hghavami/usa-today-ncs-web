/**
 * TaskResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TaskResult  {
    private java.lang.String statusCode;
    private java.lang.String statusMessage;
    private java.lang.Integer ordinalID;
    private java.lang.Integer errorCode;
    private java.lang.String ID;
    private java.lang.String interactionObjectID;

    public TaskResult() {
    }

    public java.lang.String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }

    public java.lang.String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(java.lang.String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public java.lang.Integer getOrdinalID() {
        return ordinalID;
    }

    public void setOrdinalID(java.lang.Integer ordinalID) {
        this.ordinalID = ordinalID;
    }

    public java.lang.Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(java.lang.Integer errorCode) {
        this.errorCode = errorCode;
    }

    public java.lang.String getID() {
        return ID;
    }

    public void setID(java.lang.String ID) {
        this.ID = ID;
    }

    public java.lang.String getInteractionObjectID() {
        return interactionObjectID;
    }

    public void setInteractionObjectID(java.lang.String interactionObjectID) {
        this.interactionObjectID = interactionObjectID;
    }

}
