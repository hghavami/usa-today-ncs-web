/**
 * PartnerAPIInformation.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PartnerAPIInformation implements com.ibm.ws.webservices.multiprotocol.ServiceInformation {

     // ExactTarget Partner API

    private static java.util.Map operationDescriptions;
    private static java.util.Map typeMappings;

    static {
         initOperationDescriptions();
         initTypeMappings();
    }

    private static void initOperationDescriptions() { 
        operationDescriptions = new java.util.HashMap();

        java.util.Map inner0 = new java.util.HashMap();

        java.util.List list0 = new java.util.ArrayList();
        inner0.put("configure", list0);

        com.ibm.ws.webservices.engine.description.OperationDesc configure0Op = _configure0Op();
        list0.add(configure0Op);

        java.util.List list1 = new java.util.ArrayList();
        inner0.put("create", list1);

        com.ibm.ws.webservices.engine.description.OperationDesc create1Op = _create1Op();
        list1.add(create1Op);

        java.util.List list2 = new java.util.ArrayList();
        inner0.put("delete", list2);

        com.ibm.ws.webservices.engine.description.OperationDesc delete2Op = _delete2Op();
        list2.add(delete2Op);

        java.util.List list3 = new java.util.ArrayList();
        inner0.put("describe", list3);

        com.ibm.ws.webservices.engine.description.OperationDesc describe3Op = _describe3Op();
        list3.add(describe3Op);

        java.util.List list4 = new java.util.ArrayList();
        inner0.put("execute", list4);

        com.ibm.ws.webservices.engine.description.OperationDesc execute4Op = _execute4Op();
        list4.add(execute4Op);

        java.util.List list5 = new java.util.ArrayList();
        inner0.put("extract", list5);

        com.ibm.ws.webservices.engine.description.OperationDesc extract5Op = _extract5Op();
        list5.add(extract5Op);

        java.util.List list6 = new java.util.ArrayList();
        inner0.put("getSystemStatus", list6);

        com.ibm.ws.webservices.engine.description.OperationDesc getSystemStatus6Op = _getSystemStatus6Op();
        list6.add(getSystemStatus6Op);

        java.util.List list7 = new java.util.ArrayList();
        inner0.put("perform", list7);

        com.ibm.ws.webservices.engine.description.OperationDesc perform7Op = _perform7Op();
        list7.add(perform7Op);

        java.util.List list8 = new java.util.ArrayList();
        inner0.put("query", list8);

        com.ibm.ws.webservices.engine.description.OperationDesc query8Op = _query8Op();
        list8.add(query8Op);

        java.util.List list9 = new java.util.ArrayList();
        inner0.put("retrieve", list9);

        com.ibm.ws.webservices.engine.description.OperationDesc retrieve9Op = _retrieve9Op();
        list9.add(retrieve9Op);

        java.util.List list10 = new java.util.ArrayList();
        inner0.put("schedule", list10);

        com.ibm.ws.webservices.engine.description.OperationDesc schedule10Op = _schedule10Op();
        list10.add(schedule10Op);

        java.util.List list11 = new java.util.ArrayList();
        inner0.put("update", list11);

        com.ibm.ws.webservices.engine.description.OperationDesc update11Op = _update11Op();
        list11.add(update11Op);

        java.util.List list12 = new java.util.ArrayList();
        inner0.put("versionInfo", list12);

        com.ibm.ws.webservices.engine.description.OperationDesc versionInfo12Op = _versionInfo12Op();
        list12.add(versionInfo12Op);

        operationDescriptions.put("Soap",inner0);
        operationDescriptions = java.util.Collections.unmodifiableMap(operationDescriptions);
    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _configure0Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc configure0Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ConfigureRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ConfigureRequestMsg"), com.exacttarget.ConfigureRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ConfigureRequestMsg");
        _params0[0].setOption("partName","ConfigureRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ConfigureResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ConfigureResponseMsg"), com.exacttarget.ConfigureResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ConfigureResponseMsg");
        _returnDesc0.setOption("partName","ConfigureResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        configure0Op = new com.ibm.ws.webservices.engine.description.OperationDesc("configure", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Configure"), _params0, _returnDesc0, _faults0, null);
        configure0Op.setOption("inoutOrderingReq","false");
        configure0Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        configure0Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "configureResponse"));
        configure0Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        configure0Op.setOption("buildNum","cf131037.05");
        configure0Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        configure0Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "configureRequest"));
        configure0Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return configure0Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _create1Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc create1Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CreateRequest"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">CreateRequest"), com.exacttarget.CreateRequest.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}CreateRequest");
        _params0[0].setOption("partName","CreateRequest");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CreateResponse"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">CreateResponse"), com.exacttarget.CreateResponse.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}CreateResponse");
        _returnDesc0.setOption("partName","CreateResponse");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        create1Op = new com.ibm.ws.webservices.engine.description.OperationDesc("create", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Create"), _params0, _returnDesc0, _faults0, null);
        create1Op.setOption("inoutOrderingReq","false");
        create1Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        create1Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "createResponse"));
        create1Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        create1Op.setOption("buildNum","cf131037.05");
        create1Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        create1Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "createRequest"));
        create1Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return create1Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _delete2Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc delete2Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeleteRequest"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DeleteRequest"), com.exacttarget.DeleteRequest.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}DeleteRequest");
        _params0[0].setOption("partName","DeleteRequest");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeleteResponse"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DeleteResponse"), com.exacttarget.DeleteResponse.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}DeleteResponse");
        _returnDesc0.setOption("partName","DeleteResponse");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        delete2Op = new com.ibm.ws.webservices.engine.description.OperationDesc("delete", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Delete"), _params0, _returnDesc0, _faults0, null);
        delete2Op.setOption("inoutOrderingReq","false");
        delete2Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        delete2Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "deleteResponse"));
        delete2Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        delete2Op.setOption("buildNum","cf131037.05");
        delete2Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        delete2Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "deleteRequest"));
        delete2Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return delete2Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _describe3Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc describe3Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DefinitionRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DefinitionRequestMsg"), com.exacttarget.DefinitionRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}DefinitionRequestMsg");
        _params0[0].setOption("partName","DefinitionRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DefinitionResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DefinitionResponseMsg"), com.exacttarget.DefinitionResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}DefinitionResponseMsg");
        _returnDesc0.setOption("partName","DefinitionResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        describe3Op = new com.ibm.ws.webservices.engine.description.OperationDesc("describe", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Describe"), _params0, _returnDesc0, _faults0, null);
        describe3Op.setOption("inoutOrderingReq","false");
        describe3Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        describe3Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "definitionResponse"));
        describe3Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        describe3Op.setOption("buildNum","cf131037.05");
        describe3Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        describe3Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "definitionRequest"));
        describe3Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return describe3Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _execute4Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc execute4Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExecuteRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExecuteRequestMsg"), com.exacttarget.ExecuteRequest[].class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ExecuteRequestMsg");
        _params0[0].setOption("partName","ExecuteRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExecuteResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExecuteResponseMsg"), com.exacttarget.ExecuteResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ExecuteResponseMsg");
        _returnDesc0.setOption("partName","ExecuteResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        execute4Op = new com.ibm.ws.webservices.engine.description.OperationDesc("execute", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Execute"), _params0, _returnDesc0, _faults0, null);
        execute4Op.setOption("inoutOrderingReq","false");
        execute4Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        execute4Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "executeResponse"));
        execute4Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        execute4Op.setOption("buildNum","cf131037.05");
        execute4Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        execute4Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "executeRequest"));
        execute4Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return execute4Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _extract5Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc extract5Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractRequestMsg"), javax.xml.soap.SOAPElement[].class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ExtractRequestMsg");
        _params0[0].setOption("partName","ExtractRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractResponseMsg"), com.exacttarget.ExtractResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ExtractResponseMsg");
        _returnDesc0.setOption("partName","ExtractResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        extract5Op = new com.ibm.ws.webservices.engine.description.OperationDesc("extract", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Extract"), _params0, _returnDesc0, _faults0, null);
        extract5Op.setOption("inoutOrderingReq","false");
        extract5Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        extract5Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "extractResponse"));
        extract5Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        extract5Op.setOption("buildNum","cf131037.05");
        extract5Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        extract5Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "extractRequest"));
        extract5Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return extract5Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _getSystemStatus6Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc getSystemStatus6Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemStatusRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SystemStatusRequestMsg"), com.exacttarget.SystemStatusRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}SystemStatusRequestMsg");
        _params0[0].setOption("partName","SystemStatusRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemStatusResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SystemStatusResponseMsg"), com.exacttarget.SystemStatusResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}SystemStatusResponseMsg");
        _returnDesc0.setOption("partName","SystemStatusResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        getSystemStatus6Op = new com.ibm.ws.webservices.engine.description.OperationDesc("getSystemStatus", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "GetSystemStatus"), _params0, _returnDesc0, _faults0, null);
        getSystemStatus6Op.setOption("inoutOrderingReq","false");
        getSystemStatus6Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        getSystemStatus6Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "systemStatusResponse"));
        getSystemStatus6Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        getSystemStatus6Op.setOption("buildNum","cf131037.05");
        getSystemStatus6Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        getSystemStatus6Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "systemStatusRequest"));
        getSystemStatus6Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return getSystemStatus6Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _perform7Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc perform7Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PerformRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PerformRequestMsg"), com.exacttarget.PerformRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}PerformRequestMsg");
        _params0[0].setOption("partName","PerformRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PerformResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PerformResponseMsg"), com.exacttarget.PerformResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}PerformResponseMsg");
        _returnDesc0.setOption("partName","PerformResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        perform7Op = new com.ibm.ws.webservices.engine.description.OperationDesc("perform", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Perform"), _params0, _returnDesc0, _faults0, null);
        perform7Op.setOption("inoutOrderingReq","false");
        perform7Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        perform7Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "performResponse"));
        perform7Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        perform7Op.setOption("buildNum","cf131037.05");
        perform7Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        perform7Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "performRequest"));
        perform7Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return perform7Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _query8Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc query8Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">QueryRequestMsg"), com.exacttarget.QueryRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}QueryRequestMsg");
        _params0[0].setOption("partName","QueryRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">QueryResponseMsg"), com.exacttarget.QueryResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}QueryResponseMsg");
        _returnDesc0.setOption("partName","QueryResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        query8Op = new com.ibm.ws.webservices.engine.description.OperationDesc("query", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Query"), _params0, _returnDesc0, _faults0, null);
        query8Op.setOption("inoutOrderingReq","false");
        query8Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        query8Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "queryResponse"));
        query8Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        query8Op.setOption("buildNum","cf131037.05");
        query8Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        query8Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "queryRequest"));
        query8Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return query8Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _retrieve9Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc retrieve9Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">RetrieveRequestMsg"), com.exacttarget.RetrieveRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}RetrieveRequestMsg");
        _params0[0].setOption("partName","RetrieveRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">RetrieveResponseMsg"), com.exacttarget.RetrieveResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}RetrieveResponseMsg");
        _returnDesc0.setOption("partName","RetrieveResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        retrieve9Op = new com.ibm.ws.webservices.engine.description.OperationDesc("retrieve", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Retrieve"), _params0, _returnDesc0, _faults0, null);
        retrieve9Op.setOption("inoutOrderingReq","false");
        retrieve9Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        retrieve9Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "retrieveResponse"));
        retrieve9Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        retrieve9Op.setOption("buildNum","cf131037.05");
        retrieve9Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        retrieve9Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "retrieveRequest"));
        retrieve9Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return retrieve9Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _schedule10Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc schedule10Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ScheduleRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ScheduleRequestMsg"), com.exacttarget.ScheduleRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ScheduleRequestMsg");
        _params0[0].setOption("partName","ScheduleRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ScheduleResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ScheduleResponseMsg"), com.exacttarget.ScheduleResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}ScheduleResponseMsg");
        _returnDesc0.setOption("partName","ScheduleResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        schedule10Op = new com.ibm.ws.webservices.engine.description.OperationDesc("schedule", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Schedule"), _params0, _returnDesc0, _faults0, null);
        schedule10Op.setOption("inoutOrderingReq","false");
        schedule10Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        schedule10Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "scheduleResponse"));
        schedule10Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        schedule10Op.setOption("buildNum","cf131037.05");
        schedule10Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        schedule10Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "scheduleRequest"));
        schedule10Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return schedule10Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _update11Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc update11Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UpdateRequest"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">UpdateRequest"), com.exacttarget.UpdateRequest.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}UpdateRequest");
        _params0[0].setOption("partName","UpdateRequest");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UpdateResponse"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">UpdateResponse"), com.exacttarget.UpdateResponse.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}UpdateResponse");
        _returnDesc0.setOption("partName","UpdateResponse");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        update11Op = new com.ibm.ws.webservices.engine.description.OperationDesc("update", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "Update"), _params0, _returnDesc0, _faults0, null);
        update11Op.setOption("inoutOrderingReq","false");
        update11Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        update11Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "updateResponse"));
        update11Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        update11Op.setOption("buildNum","cf131037.05");
        update11Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        update11Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "updateRequest"));
        update11Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return update11Op;

    }

    private static com.ibm.ws.webservices.engine.description.OperationDesc _versionInfo12Op() {
        com.ibm.ws.webservices.engine.description.OperationDesc versionInfo12Op = null;
        com.ibm.ws.webservices.engine.description.ParameterDesc[]  _params0 = new com.ibm.ws.webservices.engine.description.ParameterDesc[] {
         new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "VersionInfoRequestMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.IN, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">VersionInfoRequestMsg"), com.exacttarget.VersionInfoRequestMsg.class, false, false, false, false, true, false), 
          };
        _params0[0].setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}VersionInfoRequestMsg");
        _params0[0].setOption("partName","VersionInfoRequestMsg");
        com.ibm.ws.webservices.engine.description.ParameterDesc  _returnDesc0 = new com.ibm.ws.webservices.engine.description.ParameterDesc(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "VersionInfoResponseMsg"), com.ibm.ws.webservices.engine.description.ParameterDesc.OUT, com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">VersionInfoResponseMsg"), com.exacttarget.VersionInfoResponseMsg.class, true, false, false, false, true, false); 
        _returnDesc0.setOption("partQNameString","{http://exacttarget.com/wsdl/partnerAPI}VersionInfoResponseMsg");
        _returnDesc0.setOption("partName","VersionInfoResponseMsg");
        com.ibm.ws.webservices.engine.description.FaultDesc[]  _faults0 = new com.ibm.ws.webservices.engine.description.FaultDesc[] {
          };
        versionInfo12Op = new com.ibm.ws.webservices.engine.description.OperationDesc("versionInfo", com.ibm.ws.webservices.engine.utils.QNameTable.createQName("", "VersionInfo"), _params0, _returnDesc0, _faults0, null);
        versionInfo12Op.setOption("inoutOrderingReq","false");
        versionInfo12Op.setOption("portTypeQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Soap"));
        versionInfo12Op.setOption("outputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "versionInfoResponse"));
        versionInfo12Op.setOption("ServiceQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI"));
        versionInfo12Op.setOption("buildNum","cf131037.05");
        versionInfo12Op.setOption("targetNamespace","http://exacttarget.com/wsdl/partnerAPI");
        versionInfo12Op.setOption("inputMessageQName",com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "versionInfoRequest"));
        versionInfo12Op.setStyle(com.ibm.ws.webservices.engine.enumtype.Style.DOCUMENT);
        return versionInfo12Op;

    }


    private static void initTypeMappings() {
        typeMappings = new java.util.HashMap();
        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "APIObject"),
                         com.exacttarget.APIObject.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ClientID"),
                         com.exacttarget.ClientID.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "APIProperty"),
                         com.exacttarget.APIProperty.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Owner"),
                         com.exacttarget.Owner.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NullAPIProperty"),
                         com.exacttarget.NullAPIProperty.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataFolder"),
                         com.exacttarget.DataFolder.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AccountUser"),
                         com.exacttarget.AccountUser.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AsyncResponseType"),
                         com.exacttarget.AsyncResponseType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AsyncResponse"),
                         com.exacttarget.AsyncResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RespondWhen"),
                         com.exacttarget.RespondWhen.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContainerID"),
                         com.exacttarget.ContainerID.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Request"),
                         com.exacttarget.Request.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Result"),
                         com.exacttarget.Result.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RequestType"),
                         com.exacttarget.RequestType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ResultMessage"),
                         com.exacttarget.ResultMessage.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ResultItem"),
                         com.exacttarget.ResultItem.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Priority"),
                         com.exacttarget.Priority.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Options"),
                         com.exacttarget.Options.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Options>SaveOptions"),
                         com.exacttarget.SaveOption[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SaveOption"),
                         com.exacttarget.SaveOption.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TaskResult"),
                         com.exacttarget.TaskResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SaveAction"),
                         com.exacttarget.SaveAction.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">CreateRequest"),
                         com.exacttarget.CreateRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CreateOptions"),
                         com.exacttarget.CreateOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CreateResult"),
                         com.exacttarget.CreateResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">CreateResponse"),
                         com.exacttarget.CreateResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UpdateOptions"),
                         com.exacttarget.UpdateOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">UpdateRequest"),
                         com.exacttarget.UpdateRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UpdateResult"),
                         com.exacttarget.UpdateResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">UpdateResponse"),
                         com.exacttarget.UpdateResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeleteOptions"),
                         com.exacttarget.DeleteOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DeleteRequest"),
                         com.exacttarget.DeleteRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeleteResult"),
                         com.exacttarget.DeleteResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DeleteResponse"),
                         com.exacttarget.DeleteResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveRequest"),
                         com.exacttarget.RetrieveRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FilterPart"),
                         com.exacttarget.FilterPart.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">RetrieveRequest>Retrieves"),
                         com.exacttarget.Request[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveOptions"),
                         com.exacttarget.RetrieveOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">RetrieveRequestMsg"),
                         com.exacttarget.RetrieveRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">RetrieveResponseMsg"),
                         com.exacttarget.RetrieveResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveSingleRequest"),
                         com.exacttarget.RetrieveSingleRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Parameters"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveSingleOptions"),
                         com.exacttarget.RetrieveSingleOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryRequest"),
                         com.exacttarget.QueryRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Query"),
                         com.exacttarget.Query.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">QueryRequestMsg"),
                         com.exacttarget.QueryRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">QueryResponseMsg"),
                         com.exacttarget.QueryResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryObject"),
                         com.exacttarget.QueryObject.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SimpleFilterPart"),
                         com.exacttarget.SimpleFilterPart.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SimpleOperators"),
                         com.exacttarget.SimpleOperators.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TagFilterPart"),
                         com.exacttarget.TagFilterPart.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">TagFilterPart>Tags"),
                         java.lang.String[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ComplexFilterPart"),
                         com.exacttarget.ComplexFilterPart.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "LogicalOperators"),
                         com.exacttarget.LogicalOperators.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ComplexFilterPart>AdditionalOperands"),
                         com.exacttarget.FilterPart[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DefinitionRequestMsg"),
                         com.exacttarget.DefinitionRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ArrayOfObjectDefinitionRequest"),
                         com.exacttarget.ObjectDefinitionRequest[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ObjectDefinitionRequest"),
                         com.exacttarget.ObjectDefinitionRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DefinitionResponseMsg"),
                         com.exacttarget.DefinitionResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ObjectDefinition"),
                         com.exacttarget.ObjectDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PropertyDefinition"),
                         com.exacttarget.PropertyDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SoapType"),
                         com.exacttarget.SoapType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PropertyType"),
                         com.exacttarget.PropertyType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AttributeMap"),
                         com.exacttarget.AttributeMap.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PropertyDefinition>PicklistItems"),
                         com.exacttarget.PicklistItem[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PicklistItem"),
                         com.exacttarget.PicklistItem.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PropertyDefinition>References"),
                         com.exacttarget.APIObject[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ObjectDefinition>ExtendedProperties"),
                         com.exacttarget.PropertyDefinition[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExecuteRequest"),
                         com.exacttarget.ExecuteRequest.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExecuteResponse"),
                         com.exacttarget.ExecuteResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExecuteRequestMsg"),
                         com.exacttarget.ExecuteRequest[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExecuteResponseMsg"),
                         com.exacttarget.ExecuteResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "InteractionBaseObject"),
                         com.exacttarget.InteractionBaseObject.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "InteractionDefinition"),
                         com.exacttarget.InteractionDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PerformOptions"),
                         com.exacttarget.PerformOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CampaignPerformOptions"),
                         com.exacttarget.CampaignPerformOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PerformResult"),
                         com.exacttarget.PerformResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PerformRequestMsg"),
                         com.exacttarget.PerformRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>PerformRequestMsg>Definitions"),
                         com.exacttarget.APIObject[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PerformResponseMsg"),
                         com.exacttarget.PerformResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>PerformResponseMsg>Results"),
                         com.exacttarget.PerformResult[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ValidationAction"),
                         com.exacttarget.ValidationAction.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ValidationAction>ValidationOptions"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SpamAssassinValidation"),
                         com.exacttarget.SpamAssassinValidation.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContentValidation"),
                         com.exacttarget.ContentValidation.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Email"),
                         com.exacttarget.Email.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ContentValidation>Subscribers"),
                         com.exacttarget.Subscriber[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Subscriber"),
                         com.exacttarget.Subscriber.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContentValidationResult"),
                         com.exacttarget.ContentValidationResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ValidationResult"),
                         com.exacttarget.ValidationResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContentValidationTaskResult"),
                         com.exacttarget.ContentValidationTaskResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ContentValidationTaskResult>ValidationResults"),
                         com.exacttarget.ValidationResult[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ConfigureOptions"),
                         com.exacttarget.ConfigureOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ConfigureResult"),
                         com.exacttarget.ConfigureResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ConfigureRequestMsg"),
                         com.exacttarget.ConfigureRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>ConfigureRequestMsg>Configurations"),
                         com.exacttarget.APIObject[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ConfigureResponseMsg"),
                         com.exacttarget.ConfigureResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>ConfigureResponseMsg>Results"),
                         com.exacttarget.ConfigureResult[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ScheduleDefinition"),
                         com.exacttarget.ScheduleDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Recurrence"),
                         com.exacttarget.Recurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RecurrenceTypeEnum"),
                         com.exacttarget.RecurrenceTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RecurrenceRangeTypeEnum"),
                         com.exacttarget.RecurrenceRangeTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TimeZone"),
                         com.exacttarget.TimeZone.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ScheduleOptions"),
                         com.exacttarget.ScheduleOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ScheduleResult"),
                         com.exacttarget.ScheduleResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ScheduleRequestMsg"),
                         com.exacttarget.ScheduleRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>ScheduleRequestMsg>Interactions"),
                         com.exacttarget.APIObject[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ScheduleResponseMsg"),
                         com.exacttarget.ScheduleResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>ScheduleResponseMsg>Results"),
                         com.exacttarget.ScheduleResult[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MinutelyRecurrencePatternTypeEnum"),
                         com.exacttarget.MinutelyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "HourlyRecurrencePatternTypeEnum"),
                         com.exacttarget.HourlyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DailyRecurrencePatternTypeEnum"),
                         com.exacttarget.DailyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeeklyRecurrencePatternTypeEnum"),
                         com.exacttarget.WeeklyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MonthlyRecurrencePatternTypeEnum"),
                         com.exacttarget.MonthlyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeekOfMonthEnum"),
                         com.exacttarget.WeekOfMonthEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DayOfWeekEnum"),
                         com.exacttarget.DayOfWeekEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "YearlyRecurrencePatternTypeEnum"),
                         com.exacttarget.YearlyRecurrencePatternTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MonthOfYearEnum"),
                         com.exacttarget.MonthOfYearEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MinutelyRecurrence"),
                         com.exacttarget.MinutelyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "HourlyRecurrence"),
                         com.exacttarget.HourlyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DailyRecurrence"),
                         com.exacttarget.DailyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeeklyRecurrence"),
                         com.exacttarget.WeeklyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MonthlyRecurrence"),
                         com.exacttarget.MonthlyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "YearlyRecurrence"),
                         com.exacttarget.YearlyRecurrence.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractRequest"),
                         javax.xml.soap.SOAPElement.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractResult"),
                         com.exacttarget.ExtractResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractRequestMsg"),
                         javax.xml.soap.SOAPElement[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractResponseMsg"),
                         com.exacttarget.ExtractResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractOptions"),
                         com.exacttarget.ExtractOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractParameter"),
                         com.exacttarget.ExtractParameter.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractTemplate"),
                         com.exacttarget.ExtractTemplate.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractDescription"),
                         com.exacttarget.ExtractDescription.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractDescription>Parameters"),
                         com.exacttarget.ExtractParameterDescription[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractParameterDescription"),
                         com.exacttarget.ExtractParameterDescription.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractDefinition"),
                         com.exacttarget.ExtractDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractDefinition>Parameters"),
                         com.exacttarget.ExtractParameterDescription[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ExtractDefinition>Values"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExtractParameterDataType"),
                         com.exacttarget.ExtractParameterDataType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ParameterDescription"),
                         com.exacttarget.ParameterDescription.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "VersionInfoResponse"),
                         com.exacttarget.VersionInfoResponse.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">VersionInfoRequestMsg"),
                         com.exacttarget.VersionInfoRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">VersionInfoResponseMsg"),
                         com.exacttarget.VersionInfoResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Locale"),
                         com.exacttarget.Locale.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Account"),
                         com.exacttarget.Account.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AccountTypeEnum"),
                         com.exacttarget.AccountTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AccountDataItem"),
                         com.exacttarget.AccountDataItem.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Subscription"),
                         com.exacttarget.Subscription.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PrivateLabel"),
                         com.exacttarget.PrivateLabel.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BusinessRule"),
                         com.exacttarget.BusinessRule.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Account>Roles"),
                         com.exacttarget.Role[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Role"),
                         com.exacttarget.Role.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BusinessUnit"),
                         com.exacttarget.BusinessUnit.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendClassification"),
                         com.exacttarget.SendClassification.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "LandingPage"),
                         com.exacttarget.LandingPage.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UnsubscribeBehaviorEnum"),
                         com.exacttarget.UnsubscribeBehaviorEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AccountPrivateLabel"),
                         com.exacttarget.AccountPrivateLabel.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UserAccess"),
                         com.exacttarget.UserAccess.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">AccountUser>AssociatedBusinessUnits"),
                         com.exacttarget.BusinessUnit[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">AccountUser>Roles"),
                         com.exacttarget.Role[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Brand"),
                         com.exacttarget.Brand.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BrandTag"),
                         com.exacttarget.BrandTag.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Role>PermissionSets"),
                         com.exacttarget.PermissionSet[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PermissionSet"),
                         com.exacttarget.PermissionSet.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Role>Permissions"),
                         com.exacttarget.Permission[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Permission"),
                         com.exacttarget.Permission.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PermissionSet>PermissionSets"),
                         com.exacttarget.PermissionSet[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">PermissionSet>Permissions"),
                         com.exacttarget.Permission[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContentArea"),
                         com.exacttarget.ContentArea.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "LayoutType"),
                         com.exacttarget.LayoutType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Message"),
                         com.exacttarget.Message.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TrackingEvent"),
                         com.exacttarget.TrackingEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EventType"),
                         com.exacttarget.EventType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "OpenEvent"),
                         com.exacttarget.OpenEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BounceEvent"),
                         com.exacttarget.BounceEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UnsubEvent"),
                         com.exacttarget.UnsubEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ClickEvent"),
                         com.exacttarget.ClickEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SentEvent"),
                         com.exacttarget.SentEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NotSentEvent"),
                         com.exacttarget.NotSentEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SurveyEvent"),
                         com.exacttarget.SurveyEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ForwardedEmailEvent"),
                         com.exacttarget.ForwardedEmailEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ForwardedEmailOptInEvent"),
                         com.exacttarget.ForwardedEmailOptInEvent.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Attribute"),
                         com.exacttarget.Attribute.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberStatus"),
                         com.exacttarget.SubscriberStatus.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailType"),
                         com.exacttarget.EmailType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberList"),
                         com.exacttarget.SubscriberList.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "GlobalUnsubscribeCategory"),
                         com.exacttarget.GlobalUnsubscribeCategory.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberTypeDefinition"),
                         com.exacttarget.SubscriberTypeDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Subscriber>Addresses"),
                         com.exacttarget.SubscriberAddress[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberAddress"),
                         com.exacttarget.SubscriberAddress.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SMSAddress"),
                         com.exacttarget.SMSAddress.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberAddressStatus"),
                         com.exacttarget.SubscriberAddressStatus.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailAddress"),
                         com.exacttarget.EmailAddress.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CompressionConfiguration"),
                         com.exacttarget.CompressionConfiguration.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CompressionType"),
                         com.exacttarget.CompressionType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "CompressionEncoding"),
                         com.exacttarget.CompressionEncoding.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListSubscriber"),
                         com.exacttarget.ListSubscriber.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "List"),
                         com.exacttarget.List.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListTypeEnum"),
                         com.exacttarget.ListTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListClassificationEnum"),
                         com.exacttarget.ListClassificationEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Group"),
                         com.exacttarget.Group.class);

        initTypeMappings2();
    }

    private static void initTypeMappings2() {
        typeMappings = new java.util.HashMap();
        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "OverrideType"),
                         com.exacttarget.OverrideType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListAttributeFieldType"),
                         com.exacttarget.ListAttributeFieldType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListAttribute"),
                         com.exacttarget.ListAttribute.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListAttributeRestrictedValue"),
                         com.exacttarget.ListAttributeRestrictedValue.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Campaign"),
                         com.exacttarget.Campaign.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Send"),
                         com.exacttarget.Send.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Link"),
                         com.exacttarget.Link.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Send>Sources"),
                         com.exacttarget.APIObject[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailSendDefinition"),
                         com.exacttarget.EmailSendDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendSummary"),
                         com.exacttarget.SendSummary.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendDefinition"),
                         com.exacttarget.SendDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendDefinition"),
                         com.exacttarget.TriggeredSendDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendTypeEnum"),
                         com.exacttarget.TriggeredSendTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendStatusEnum"),
                         com.exacttarget.TriggeredSendStatusEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendExclusionList"),
                         com.exacttarget.TriggeredSendExclusionList.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtension"),
                         com.exacttarget.DataExtension.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AudienceItem"),
                         com.exacttarget.AudienceItem.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSend"),
                         com.exacttarget.TriggeredSend.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendCreateResult"),
                         com.exacttarget.TriggeredSendCreateResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberResult"),
                         com.exacttarget.SubscriberResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SubscriberSendResult"),
                         com.exacttarget.SubscriberSendResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TriggeredSendSummary"),
                         com.exacttarget.TriggeredSendSummary.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AsyncRequestResult"),
                         com.exacttarget.AsyncRequestResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "VoiceTriggeredSend"),
                         com.exacttarget.VoiceTriggeredSend.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "VoiceTriggeredSendDefinition"),
                         com.exacttarget.VoiceTriggeredSendDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SMSTriggeredSend"),
                         com.exacttarget.SMSTriggeredSend.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SMSTriggeredSendDefinition"),
                         com.exacttarget.SMSTriggeredSendDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendClassificationTypeEnum"),
                         com.exacttarget.SendClassificationTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SenderProfile"),
                         com.exacttarget.SenderProfile.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeliveryProfile"),
                         com.exacttarget.DeliveryProfile.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeliveryProfileSourceAddressTypeEnum"),
                         com.exacttarget.DeliveryProfileSourceAddressTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PrivateIP"),
                         com.exacttarget.PrivateIP.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DeliveryProfileDomainTypeEnum"),
                         com.exacttarget.DeliveryProfileDomainTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PrivateDomain"),
                         com.exacttarget.PrivateDomain.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SalutationSourceEnum"),
                         com.exacttarget.SalutationSourceEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Certificate"),
                         com.exacttarget.Certificate.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PrivateDomainSet"),
                         com.exacttarget.PrivateDomainSet.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendDefinitionListTypeEnum"),
                         com.exacttarget.SendDefinitionListTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataSourceTypeEnum"),
                         com.exacttarget.DataSourceTypeEnum.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendDefinitionList"),
                         com.exacttarget.SendDefinitionList.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">EmailSendDefinition>TrackingUsers"),
                         com.exacttarget.TrackingUser[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TrackingUser"),
                         com.exacttarget.TrackingUser.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FilterDefinition"),
                         com.exacttarget.FilterDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SendDefinitionList>Parameters"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MessagingVendorKind"),
                         com.exacttarget.MessagingVendorKind.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MessagingConfiguration"),
                         com.exacttarget.MessagingConfiguration.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UserMap"),
                         com.exacttarget.UserMap.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Folder"),
                         com.exacttarget.Folder.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FileTransferLocation"),
                         com.exacttarget.FileTransferLocation.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtractActivity"),
                         com.exacttarget.DataExtractActivity.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MessageSendActivity"),
                         com.exacttarget.MessageSendActivity.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SmsSendActivity"),
                         com.exacttarget.SmsSendActivity.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ReportActivity"),
                         com.exacttarget.ReportActivity.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionField"),
                         com.exacttarget.DataExtensionField.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionTemplate"),
                         com.exacttarget.DataExtensionTemplate.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtension>Fields"),
                         com.exacttarget.DataExtensionField[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DateTimeUnitOfMeasure"),
                         com.exacttarget.DateTimeUnitOfMeasure.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionFieldType"),
                         com.exacttarget.DataExtensionFieldType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ObjectExtension"),
                         com.exacttarget.ObjectExtension.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionObject"),
                         com.exacttarget.DataExtensionObject.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionObject>Keys"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionError"),
                         com.exacttarget.DataExtensionError.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionCreateResult"),
                         com.exacttarget.DataExtensionCreateResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionCreateResult>KeyErrors"),
                         com.exacttarget.DataExtensionError[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionCreateResult>ValueErrors"),
                         com.exacttarget.DataExtensionError[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionUpdateResult"),
                         com.exacttarget.DataExtensionUpdateResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionUpdateResult>KeyErrors"),
                         com.exacttarget.DataExtensionError[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionUpdateResult>ValueErrors"),
                         com.exacttarget.DataExtensionError[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "DataExtensionDeleteResult"),
                         com.exacttarget.DataExtensionDeleteResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">DataExtensionDeleteResult>KeyErrors"),
                         com.exacttarget.DataExtensionError[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FileType"),
                         com.exacttarget.FileType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionSubscriberImportType"),
                         com.exacttarget.ImportDefinitionSubscriberImportType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionUpdateType"),
                         com.exacttarget.ImportDefinitionUpdateType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionColumnBasedAction"),
                         com.exacttarget.ImportDefinitionColumnBasedAction.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionColumnBasedActionType"),
                         com.exacttarget.ImportDefinitionColumnBasedActionType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionFieldMappingType"),
                         com.exacttarget.ImportDefinitionFieldMappingType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FieldMap"),
                         javax.xml.soap.SOAPElement.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionAutoGenerateDestination"),
                         com.exacttarget.ImportDefinitionAutoGenerateDestination.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinition"),
                         com.exacttarget.ImportDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ImportDefinition>FieldMaps"),
                         javax.xml.soap.SOAPElement[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ImportDefinition>ControlColumnActions"),
                         com.exacttarget.ImportDefinitionColumnBasedAction[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportDefinitionFieldMap"),
                         javax.xml.soap.SOAPElement.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ImportResultsSummary"),
                         com.exacttarget.ImportResultsSummary.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "GroupDefinition"),
                         com.exacttarget.GroupDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FileTransferActivity"),
                         com.exacttarget.FileTransferActivity.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ListSend"),
                         com.exacttarget.ListSend.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "LinkSend"),
                         com.exacttarget.LinkSend.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">ObjectExtension>Properties"),
                         com.exacttarget.APIProperty[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PublicKeyManagement"),
                         com.exacttarget.PublicKeyManagement.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SecurityObject"),
                         com.exacttarget.SecurityObject.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemStatusOptions"),
                         com.exacttarget.SystemStatusOptions.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SystemStatusRequestMsg"),
                         com.exacttarget.SystemStatusRequestMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemStatusResult"),
                         com.exacttarget.SystemStatusResult.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemStatusType"),
                         com.exacttarget.SystemStatusType.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SystemStatusResult>Outages"),
                         com.exacttarget.SystemOutage[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SystemOutage"),
                         com.exacttarget.SystemOutage.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SystemStatusResponseMsg"),
                         com.exacttarget.SystemStatusResponseMsg.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">>SystemStatusResponseMsg>Results"),
                         com.exacttarget.SystemStatusResult[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Authentication"),
                         com.exacttarget.Authentication.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UsernameAuthentication"),
                         com.exacttarget.UsernameAuthentication.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ResourceSpecification"),
                         com.exacttarget.ResourceSpecification.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Portfolio"),
                         com.exacttarget.Portfolio.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Layout"),
                         com.exacttarget.Layout.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryDefinition"),
                         com.exacttarget.QueryDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "IntegrationProfile"),
                         com.exacttarget.IntegrationProfile.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "IntegrationProfileDefinition"),
                         com.exacttarget.IntegrationProfileDefinition.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ReplyMailManagementConfiguration"),
                         com.exacttarget.ReplyMailManagementConfiguration.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FileTrigger"),
                         com.exacttarget.FileTrigger.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FileTriggerTypeLastPull"),
                         com.exacttarget.FileTriggerTypeLastPull.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ProgramManifestTemplate"),
                         com.exacttarget.ProgramManifestTemplate.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">SubscriberAddress>Statuses"),
                         com.exacttarget.AddressStatus[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AddressStatus"),
                         com.exacttarget.AddressStatus.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Publication"),
                         com.exacttarget.Publication.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Publication>Subscribers"),
                         com.exacttarget.Subscriber[].class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PublicationSubscriber"),
                         com.exacttarget.PublicationSubscriber.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PlatformApplication"),
                         com.exacttarget.PlatformApplication.class);

        typeMappings.put(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PlatformApplicationPackage"),
                         com.exacttarget.PlatformApplicationPackage.class);

        typeMappings = java.util.Collections.unmodifiableMap(typeMappings);
    }

    public java.util.Map getTypeMappings() {
        return typeMappings;
    }

    public Class getJavaType(javax.xml.namespace.QName xmlName) {
        return (Class) typeMappings.get(xmlName);
    }

    public java.util.Map getOperationDescriptions(String portName) {
        return (java.util.Map) operationDescriptions.get(portName);
    }

    public java.util.List getOperationDescriptions(String portName, String operationName) {
        java.util.Map map = (java.util.Map) operationDescriptions.get(portName);
        if (map != null) {
            return (java.util.List) map.get(operationName);
        }
        return null;
    }

}
