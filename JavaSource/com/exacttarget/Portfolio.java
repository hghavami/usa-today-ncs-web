/**
 * Portfolio.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Portfolio  extends com.exacttarget.APIObject  {
    private com.exacttarget.ResourceSpecification source;
    private java.lang.Integer categoryID;
    private java.lang.String fileName;
    private java.lang.String displayName;
    private java.lang.String description;
    private java.lang.String typeDescription;
    private java.lang.Boolean isUploaded;
    private java.lang.Boolean isActive;
    private java.lang.Integer fileSizeKB;
    private java.lang.Integer thumbSizeKB;
    private java.lang.Integer fileWidthPX;
    private java.lang.Integer fileHeightPX;
    private java.lang.String fileURL;
    private java.lang.String thumbURL;
    private java.util.Calendar cacheClearTime;
    private java.lang.String categoryType;

    public Portfolio() {
    }

    public com.exacttarget.ResourceSpecification getSource() {
        return source;
    }

    public void setSource(com.exacttarget.ResourceSpecification source) {
        this.source = source;
    }

    public java.lang.Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(java.lang.Integer categoryID) {
        this.categoryID = categoryID;
    }

    public java.lang.String getFileName() {
        return fileName;
    }

    public void setFileName(java.lang.String fileName) {
        this.fileName = fileName;
    }

    public java.lang.String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(java.lang.String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public java.lang.Boolean getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(java.lang.Boolean isUploaded) {
        this.isUploaded = isUploaded;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.Integer getFileSizeKB() {
        return fileSizeKB;
    }

    public void setFileSizeKB(java.lang.Integer fileSizeKB) {
        this.fileSizeKB = fileSizeKB;
    }

    public java.lang.Integer getThumbSizeKB() {
        return thumbSizeKB;
    }

    public void setThumbSizeKB(java.lang.Integer thumbSizeKB) {
        this.thumbSizeKB = thumbSizeKB;
    }

    public java.lang.Integer getFileWidthPX() {
        return fileWidthPX;
    }

    public void setFileWidthPX(java.lang.Integer fileWidthPX) {
        this.fileWidthPX = fileWidthPX;
    }

    public java.lang.Integer getFileHeightPX() {
        return fileHeightPX;
    }

    public void setFileHeightPX(java.lang.Integer fileHeightPX) {
        this.fileHeightPX = fileHeightPX;
    }

    public java.lang.String getFileURL() {
        return fileURL;
    }

    public void setFileURL(java.lang.String fileURL) {
        this.fileURL = fileURL;
    }

    public java.lang.String getThumbURL() {
        return thumbURL;
    }

    public void setThumbURL(java.lang.String thumbURL) {
        this.thumbURL = thumbURL;
    }

    public java.util.Calendar getCacheClearTime() {
        return cacheClearTime;
    }

    public void setCacheClearTime(java.util.Calendar cacheClearTime) {
        this.cacheClearTime = cacheClearTime;
    }

    public java.lang.String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(java.lang.String categoryType) {
        this.categoryType = categoryType;
    }

}
