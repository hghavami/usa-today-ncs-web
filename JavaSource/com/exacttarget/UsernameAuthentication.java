/**
 * UsernameAuthentication.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UsernameAuthentication  extends com.exacttarget.Authentication  {
    private java.lang.String userName;
    private java.lang.String passWord;

    public UsernameAuthentication() {
    }

    public java.lang.String getUserName() {
        return userName;
    }

    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }

    public java.lang.String getPassWord() {
        return passWord;
    }

    public void setPassWord(java.lang.String passWord) {
        this.passWord = passWord;
    }

}
