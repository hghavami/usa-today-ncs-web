/**
 * SalutationSourceEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SalutationSourceEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SalutationSourceEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Default = "Default";
    public static final java.lang.String _ContentLibrary = "ContentLibrary";
    public static final java.lang.String _None = "None";
    public static final SalutationSourceEnum Default = new SalutationSourceEnum(_Default);
    public static final SalutationSourceEnum ContentLibrary = new SalutationSourceEnum(_ContentLibrary);
    public static final SalutationSourceEnum None = new SalutationSourceEnum(_None);
    public java.lang.String getValue() { return _value_;}
    public static SalutationSourceEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SalutationSourceEnum enumeration = (SalutationSourceEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SalutationSourceEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
