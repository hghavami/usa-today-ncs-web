/**
 * MinutelyRecurrencePatternTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MinutelyRecurrencePatternTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MinutelyRecurrencePatternTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Interval = "Interval";
    public static final MinutelyRecurrencePatternTypeEnum Interval = new MinutelyRecurrencePatternTypeEnum(_Interval);
    public java.lang.String getValue() { return _value_;}
    public static MinutelyRecurrencePatternTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        MinutelyRecurrencePatternTypeEnum enumeration = (MinutelyRecurrencePatternTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static MinutelyRecurrencePatternTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
