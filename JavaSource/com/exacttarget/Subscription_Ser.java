/**
 * Subscription_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Subscription_Ser extends com.ibm.ws.webservices.engine.encoding.ser.BeanSerializer {
    /**
     * Constructor
     */
    public Subscription_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
           javax.xml.namespace.QName
           elemQName = QName_0_320;
           context.qName2String(elemQName, true);
           elemQName = QName_0_321;
           context.qName2String(elemQName, true);
           elemQName = QName_0_322;
           context.qName2String(elemQName, true);
           elemQName = QName_0_323;
           context.qName2String(elemQName, true);
           elemQName = QName_0_324;
           context.qName2String(elemQName, true);
           elemQName = QName_0_325;
           context.qName2String(elemQName, true);
           elemQName = QName_0_326;
           context.qName2String(elemQName, true);
           elemQName = QName_0_327;
           context.qName2String(elemQName, true);
           elemQName = QName_0_328;
           context.qName2String(elemQName, true);
           elemQName = QName_0_281;
           context.qName2String(elemQName, true);
           elemQName = QName_0_329;
           context.qName2String(elemQName, true);
           elemQName = QName_0_330;
           context.qName2String(elemQName, true);
           elemQName = QName_0_331;
           context.qName2String(elemQName, true);
           elemQName = QName_0_332;
           context.qName2String(elemQName, true);
           elemQName = QName_0_333;
           context.qName2String(elemQName, true);
           elemQName = QName_0_334;
           context.qName2String(elemQName, true);
           elemQName = QName_0_335;
           context.qName2String(elemQName, true);
           elemQName = QName_0_336;
           context.qName2String(elemQName, true);
           elemQName = QName_0_337;
           context.qName2String(elemQName, true);
           elemQName = QName_0_699;
           context.qName2String(elemQName, true);
           elemQName = QName_0_700;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        Subscription bean = (Subscription) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_320;
          propValue = bean.getSubscriptionID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_321;
          propValue = new java.lang.Integer(bean.getEmailsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_322;
          propValue = new java.lang.Integer(bean.getAccountsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_323;
          propValue = new java.lang.Integer(bean.getAdvAccountsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_324;
          propValue = new java.lang.Integer(bean.getLPAccountsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_325;
          propValue = new java.lang.Integer(bean.getDOTOAccountsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_326;
          propValue = new java.lang.Integer(bean.getBUAccountsPurchased());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_327;
          propValue = bean.getBeginDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              true,null,context);
          propQName = QName_0_328;
          propValue = bean.getEndDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              true,null,context);
          propQName = QName_0_281;
          propValue = bean.getNotes();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_329;
          propValue = bean.getPeriod();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_330;
          propValue = bean.getNotificationTitle();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_331;
          propValue = bean.getNotificationMessage();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_332;
          propValue = bean.getNotificationFlag();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_333;
          propValue = bean.getNotificationExpDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_334;
          propValue = bean.getForAccounting();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_335;
          propValue = new java.lang.Boolean(bean.isHasPurchasedEmails());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              true,null,context);
          propQName = QName_0_336;
          propValue = bean.getContractNumber();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_337;
          propValue = bean.getContractModifier();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_699;
          propValue = bean.getIsRenewal();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_700;
          propValue = new java.lang.Long(bean.getNumberofEmails());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              true,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_330 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationTitle");
    private final static javax.xml.namespace.QName QName_0_323 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AdvAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_335 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HasPurchasedEmails");
    private final static javax.xml.namespace.QName QName_0_331 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationMessage");
    private final static javax.xml.namespace.QName QName_0_332 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationFlag");
    private final static javax.xml.namespace.QName QName_1_314 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_0_327 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BeginDate");
    private final static javax.xml.namespace.QName QName_0_321 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailsPurchased");
    private final static javax.xml.namespace.QName QName_0_334 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ForAccounting");
    private final static javax.xml.namespace.QName QName_0_329 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Period");
    private final static javax.xml.namespace.QName QName_0_325 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DOTOAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_322 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountsPurchased");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_1_13 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "dateTime");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_328 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EndDate");
    private final static javax.xml.namespace.QName QName_0_699 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsRenewal");
    private final static javax.xml.namespace.QName QName_0_700 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberofEmails");
    private final static javax.xml.namespace.QName QName_0_337 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContractModifier");
    private final static javax.xml.namespace.QName QName_0_320 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriptionID");
    private final static javax.xml.namespace.QName QName_0_326 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BUAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_336 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContractNumber");
    private final static javax.xml.namespace.QName QName_0_333 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationExpDate");
    private final static javax.xml.namespace.QName QName_0_324 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LPAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_281 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Notes");
}
