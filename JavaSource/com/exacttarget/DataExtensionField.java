/**
 * DataExtensionField.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DataExtensionField  extends com.exacttarget.PropertyDefinition  {
    private java.lang.Integer ordinal;
    private java.lang.Boolean isPrimaryKey;
    private com.exacttarget.DataExtensionFieldType fieldType;
    private com.exacttarget.DataExtension dataExtension;

    public DataExtensionField() {
    }

    public java.lang.Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(java.lang.Integer ordinal) {
        this.ordinal = ordinal;
    }

    public java.lang.Boolean getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public void setIsPrimaryKey(java.lang.Boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    }

    public com.exacttarget.DataExtensionFieldType getFieldType() {
        return fieldType;
    }

    public void setFieldType(com.exacttarget.DataExtensionFieldType fieldType) {
        this.fieldType = fieldType;
    }

    public com.exacttarget.DataExtension getDataExtension() {
        return dataExtension;
    }

    public void setDataExtension(com.exacttarget.DataExtension dataExtension) {
        this.dataExtension = dataExtension;
    }

}
