/**
 * CampaignPerformOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class CampaignPerformOptions  extends com.exacttarget.PerformOptions  {
    private java.lang.String[] occurrenceIDs;
    private java.lang.Integer occurrenceIDsIndex;

    public CampaignPerformOptions() {
    }

    public java.lang.String[] getOccurrenceIDs() {
        return occurrenceIDs;
    }

    public void setOccurrenceIDs(java.lang.String[] occurrenceIDs) {
        this.occurrenceIDs = occurrenceIDs;
    }

    public java.lang.String getOccurrenceIDs(int i) {
        return this.occurrenceIDs[i];
    }

    public void setOccurrenceIDs(int i, java.lang.String value) {
        this.occurrenceIDs[i] = value;
    }

    public java.lang.Integer getOccurrenceIDsIndex() {
        return occurrenceIDsIndex;
    }

    public void setOccurrenceIDsIndex(java.lang.Integer occurrenceIDsIndex) {
        this.occurrenceIDsIndex = occurrenceIDsIndex;
    }

}
