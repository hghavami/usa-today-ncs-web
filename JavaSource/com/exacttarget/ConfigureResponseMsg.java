/**
 * ConfigureResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ConfigureResponseMsg  {
    private com.exacttarget.ConfigureResult[] results;
    private java.lang.String overallStatus;
    private java.lang.String overallStatusMessage;
    private java.lang.String requestID;

    public ConfigureResponseMsg() {
    }

    public com.exacttarget.ConfigureResult[] getResults() {
        return results;
    }

    public void setResults(com.exacttarget.ConfigureResult[] results) {
        this.results = results;
    }

    public java.lang.String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(java.lang.String overallStatus) {
        this.overallStatus = overallStatus;
    }

    public java.lang.String getOverallStatusMessage() {
        return overallStatusMessage;
    }

    public void setOverallStatusMessage(java.lang.String overallStatusMessage) {
        this.overallStatusMessage = overallStatusMessage;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

}
