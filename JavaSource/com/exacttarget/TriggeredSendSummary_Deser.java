/**
 * TriggeredSendSummary_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class TriggeredSendSummary_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public TriggeredSendSummary_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new TriggeredSendSummary();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_518) {
          ((TriggeredSendSummary)value).setSent(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_519) {
          ((TriggeredSendSummary)value).setNotSentDueToOptOut(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_520) {
          ((TriggeredSendSummary)value).setNotSentDueToUndeliverable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_521) {
          ((TriggeredSendSummary)value).setBounces(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_522) {
          ((TriggeredSendSummary)value).setOpens(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_523) {
          ((TriggeredSendSummary)value).setClicks(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_412) {
          ((TriggeredSendSummary)value).setUniqueOpens(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_411) {
          ((TriggeredSendSummary)value).setUniqueClicks(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_524) {
          ((TriggeredSendSummary)value).setOptOuts(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_525) {
          ((TriggeredSendSummary)value).setSurveyResponses(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_526) {
          ((TriggeredSendSummary)value).setFTAFRequests(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_527) {
          ((TriggeredSendSummary)value).setFTAFEmailsSent(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_528) {
          ((TriggeredSendSummary)value).setFTAFOptIns(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_529) {
          ((TriggeredSendSummary)value).setConversions(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_530) {
          ((TriggeredSendSummary)value).setUniqueConversions(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_531) {
          ((TriggeredSendSummary)value).setInProcess(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_532) {
          ((TriggeredSendSummary)value).setNotSentDueToError(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_506) {
          ((TriggeredSendSummary)value).setTriggeredSendDefinition((com.exacttarget.TriggeredSendDefinition)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_526 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFRequests");
    private final static javax.xml.namespace.QName QName_0_529 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Conversions");
    private final static javax.xml.namespace.QName QName_0_531 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "InProcess");
    private final static javax.xml.namespace.QName QName_0_525 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SurveyResponses");
    private final static javax.xml.namespace.QName QName_0_532 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToError");
    private final static javax.xml.namespace.QName QName_0_523 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Clicks");
    private final static javax.xml.namespace.QName QName_0_519 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToOptOut");
    private final static javax.xml.namespace.QName QName_0_527 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFEmailsSent");
    private final static javax.xml.namespace.QName QName_0_521 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Bounces");
    private final static javax.xml.namespace.QName QName_0_528 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FTAFOptIns");
    private final static javax.xml.namespace.QName QName_0_524 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OptOuts");
    private final static javax.xml.namespace.QName QName_0_518 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Sent");
    private final static javax.xml.namespace.QName QName_0_411 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueClicks");
    private final static javax.xml.namespace.QName QName_0_522 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Opens");
    private final static javax.xml.namespace.QName QName_0_412 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueOpens");
    private final static javax.xml.namespace.QName QName_0_530 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueConversions");
    private final static javax.xml.namespace.QName QName_0_506 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendDefinition");
    private final static javax.xml.namespace.QName QName_0_520 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotSentDueToUndeliverable");
}
