/**
 * Portfolio_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Portfolio_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public Portfolio_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new Portfolio();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_182) {
          ((Portfolio)value).setCategoryID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_634) {
          ((Portfolio)value).setFileName(strValue);
          return true;}
        else if (qName==QName_0_348) {
          ((Portfolio)value).setDisplayName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((Portfolio)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_635) {
          ((Portfolio)value).setTypeDescription(strValue);
          return true;}
        else if (qName==QName_0_636) {
          ((Portfolio)value).setIsUploaded(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_187) {
          ((Portfolio)value).setIsActive(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_637) {
          ((Portfolio)value).setFileSizeKB(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_638) {
          ((Portfolio)value).setThumbSizeKB(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_639) {
          ((Portfolio)value).setFileWidthPX(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_640) {
          ((Portfolio)value).setFileHeightPX(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_641) {
          ((Portfolio)value).setFileURL(strValue);
          return true;}
        else if (qName==QName_0_642) {
          ((Portfolio)value).setThumbURL(strValue);
          return true;}
        else if (qName==QName_0_643) {
          ((Portfolio)value).setCacheClearTime(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_644) {
          ((Portfolio)value).setCategoryType(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_633) {
          ((Portfolio)value).setSource((com.exacttarget.ResourceSpecification)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_643 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CacheClearTime");
    private final static javax.xml.namespace.QName QName_0_182 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CategoryID");
    private final static javax.xml.namespace.QName QName_0_644 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CategoryType");
    private final static javax.xml.namespace.QName QName_0_187 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsActive");
    private final static javax.xml.namespace.QName QName_0_637 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileSizeKB");
    private final static javax.xml.namespace.QName QName_0_348 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DisplayName");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_633 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Source");
    private final static javax.xml.namespace.QName QName_0_639 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileWidthPX");
    private final static javax.xml.namespace.QName QName_0_640 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileHeightPX");
    private final static javax.xml.namespace.QName QName_0_638 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ThumbSizeKB");
    private final static javax.xml.namespace.QName QName_0_636 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsUploaded");
    private final static javax.xml.namespace.QName QName_0_634 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileName");
    private final static javax.xml.namespace.QName QName_0_641 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileURL");
    private final static javax.xml.namespace.QName QName_0_635 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TypeDescription");
    private final static javax.xml.namespace.QName QName_0_642 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ThumbURL");
}
