/**
 * QueryRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class QueryRequestMsg  {
    private com.exacttarget.QueryRequest queryRequest;

    public QueryRequestMsg() {
    }

    public com.exacttarget.QueryRequest getQueryRequest() {
        return queryRequest;
    }

    public void setQueryRequest(com.exacttarget.QueryRequest queryRequest) {
        this.queryRequest = queryRequest;
    }

}
