/**
 * AsyncResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AsyncResponseType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AsyncResponseType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _None = "None";
    public static final java.lang.String _email = "email";
    public static final java.lang.String _FTP = "FTP";
    public static final java.lang.String _HTTPPost = "HTTPPost";
    public static final AsyncResponseType None = new AsyncResponseType(_None);
    public static final AsyncResponseType email = new AsyncResponseType(_email);
    public static final AsyncResponseType FTP = new AsyncResponseType(_FTP);
    public static final AsyncResponseType HTTPPost = new AsyncResponseType(_HTTPPost);
    public java.lang.String getValue() { return _value_;}
    public static AsyncResponseType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AsyncResponseType enumeration = (AsyncResponseType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AsyncResponseType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
