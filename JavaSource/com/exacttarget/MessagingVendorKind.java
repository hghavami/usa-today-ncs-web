/**
 * MessagingVendorKind.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MessagingVendorKind  extends com.exacttarget.APIObject  {
    private java.lang.String vendor;
    private java.lang.String kind;
    private boolean isUsernameRequired;
    private boolean isPasswordRequired;
    private boolean isProfileRequired;

    public MessagingVendorKind() {
    }

    public java.lang.String getVendor() {
        return vendor;
    }

    public void setVendor(java.lang.String vendor) {
        this.vendor = vendor;
    }

    public java.lang.String getKind() {
        return kind;
    }

    public void setKind(java.lang.String kind) {
        this.kind = kind;
    }

    public boolean isIsUsernameRequired() {
        return isUsernameRequired;
    }

    public void setIsUsernameRequired(boolean isUsernameRequired) {
        this.isUsernameRequired = isUsernameRequired;
    }

    public boolean isIsPasswordRequired() {
        return isPasswordRequired;
    }

    public void setIsPasswordRequired(boolean isPasswordRequired) {
        this.isPasswordRequired = isPasswordRequired;
    }

    public boolean isIsProfileRequired() {
        return isProfileRequired;
    }

    public void setIsProfileRequired(boolean isProfileRequired) {
        this.isProfileRequired = isProfileRequired;
    }

}
