/**
 * ObjectDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ObjectDefinition  {
    private java.lang.String objectType;
    private java.lang.String name;
    private java.lang.Boolean isCreatable;
    private java.lang.Boolean isUpdatable;
    private java.lang.Boolean isRetrievable;
    private java.lang.Boolean isQueryable;
    private java.lang.Boolean isReference;
    private java.lang.String referencedType;
    private java.lang.String isPropertyCollection;
    private java.lang.Boolean isObjectCollection;
    private com.exacttarget.PropertyDefinition[] properties;
    private com.exacttarget.PropertyDefinition[] extendedProperties;
    private com.exacttarget.ObjectDefinition[] childObjects;

    public ObjectDefinition() {
    }

    public java.lang.String getObjectType() {
        return objectType;
    }

    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.Boolean getIsCreatable() {
        return isCreatable;
    }

    public void setIsCreatable(java.lang.Boolean isCreatable) {
        this.isCreatable = isCreatable;
    }

    public java.lang.Boolean getIsUpdatable() {
        return isUpdatable;
    }

    public void setIsUpdatable(java.lang.Boolean isUpdatable) {
        this.isUpdatable = isUpdatable;
    }

    public java.lang.Boolean getIsRetrievable() {
        return isRetrievable;
    }

    public void setIsRetrievable(java.lang.Boolean isRetrievable) {
        this.isRetrievable = isRetrievable;
    }

    public java.lang.Boolean getIsQueryable() {
        return isQueryable;
    }

    public void setIsQueryable(java.lang.Boolean isQueryable) {
        this.isQueryable = isQueryable;
    }

    public java.lang.Boolean getIsReference() {
        return isReference;
    }

    public void setIsReference(java.lang.Boolean isReference) {
        this.isReference = isReference;
    }

    public java.lang.String getReferencedType() {
        return referencedType;
    }

    public void setReferencedType(java.lang.String referencedType) {
        this.referencedType = referencedType;
    }

    public java.lang.String getIsPropertyCollection() {
        return isPropertyCollection;
    }

    public void setIsPropertyCollection(java.lang.String isPropertyCollection) {
        this.isPropertyCollection = isPropertyCollection;
    }

    public java.lang.Boolean getIsObjectCollection() {
        return isObjectCollection;
    }

    public void setIsObjectCollection(java.lang.Boolean isObjectCollection) {
        this.isObjectCollection = isObjectCollection;
    }

    public com.exacttarget.PropertyDefinition[] getProperties() {
        return properties;
    }

    public void setProperties(com.exacttarget.PropertyDefinition[] properties) {
        this.properties = properties;
    }

    public com.exacttarget.PropertyDefinition getProperties(int i) {
        return this.properties[i];
    }

    public void setProperties(int i, com.exacttarget.PropertyDefinition value) {
        this.properties[i] = value;
    }

    public com.exacttarget.PropertyDefinition[] getExtendedProperties() {
        return extendedProperties;
    }

    public void setExtendedProperties(com.exacttarget.PropertyDefinition[] extendedProperties) {
        this.extendedProperties = extendedProperties;
    }

    public com.exacttarget.ObjectDefinition[] getChildObjects() {
        return childObjects;
    }

    public void setChildObjects(com.exacttarget.ObjectDefinition[] childObjects) {
        this.childObjects = childObjects;
    }

    public com.exacttarget.ObjectDefinition getChildObjects(int i) {
        return this.childObjects[i];
    }

    public void setChildObjects(int i, com.exacttarget.ObjectDefinition value) {
        this.childObjects[i] = value;
    }

}
