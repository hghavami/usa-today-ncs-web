/**
 * Send.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Send  extends com.exacttarget.APIObject  {
    private com.exacttarget.Email email;
    private com.exacttarget.List[] list;
    private java.util.Calendar sendDate;
    private java.lang.String fromAddress;
    private java.lang.String fromName;
    private java.lang.Integer duplicates;
    private java.lang.Integer invalidAddresses;
    private java.lang.Integer existingUndeliverables;
    private java.lang.Integer existingUnsubscribes;
    private java.lang.Integer hardBounces;
    private java.lang.Integer softBounces;
    private java.lang.Integer otherBounces;
    private java.lang.Integer forwardedEmails;
    private java.lang.Integer uniqueClicks;
    private java.lang.Integer uniqueOpens;
    private java.lang.Integer numberSent;
    private java.lang.Integer numberDelivered;
    private java.lang.Integer unsubscribes;
    private java.lang.Integer missingAddresses;
    private java.lang.String subject;
    private java.lang.String previewURL;
    private com.exacttarget.Link[] links;
    private com.exacttarget.TrackingEvent[] events;
    private java.util.Calendar sentDate;
    private java.lang.String emailName;
    private java.lang.String status;
    private java.lang.Boolean isMultipart;
    private java.lang.Integer sendLimit;
    private java.util.Calendar sendWindowOpen;
    private java.util.Calendar sendWindowClose;
    private java.lang.Boolean isAlwaysOn;
    private com.exacttarget.APIObject[] sources;
    private java.lang.Integer numberTargeted;
    private java.lang.Integer numberErrored;
    private java.lang.Integer numberExcluded;
    private java.lang.String additional;
    private java.lang.String bccEmail;
    private com.exacttarget.EmailSendDefinition emailSendDefinition;

    public Send() {
    }

    public com.exacttarget.Email getEmail() {
        return email;
    }

    public void setEmail(com.exacttarget.Email email) {
        this.email = email;
    }

    public com.exacttarget.List[] getList() {
        return list;
    }

    public void setList(com.exacttarget.List[] list) {
        this.list = list;
    }

    public com.exacttarget.List getList(int i) {
        return this.list[i];
    }

    public void setList(int i, com.exacttarget.List value) {
        this.list[i] = value;
    }

    public java.util.Calendar getSendDate() {
        return sendDate;
    }

    public void setSendDate(java.util.Calendar sendDate) {
        this.sendDate = sendDate;
    }

    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.Integer getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(java.lang.Integer duplicates) {
        this.duplicates = duplicates;
    }

    public java.lang.Integer getInvalidAddresses() {
        return invalidAddresses;
    }

    public void setInvalidAddresses(java.lang.Integer invalidAddresses) {
        this.invalidAddresses = invalidAddresses;
    }

    public java.lang.Integer getExistingUndeliverables() {
        return existingUndeliverables;
    }

    public void setExistingUndeliverables(java.lang.Integer existingUndeliverables) {
        this.existingUndeliverables = existingUndeliverables;
    }

    public java.lang.Integer getExistingUnsubscribes() {
        return existingUnsubscribes;
    }

    public void setExistingUnsubscribes(java.lang.Integer existingUnsubscribes) {
        this.existingUnsubscribes = existingUnsubscribes;
    }

    public java.lang.Integer getHardBounces() {
        return hardBounces;
    }

    public void setHardBounces(java.lang.Integer hardBounces) {
        this.hardBounces = hardBounces;
    }

    public java.lang.Integer getSoftBounces() {
        return softBounces;
    }

    public void setSoftBounces(java.lang.Integer softBounces) {
        this.softBounces = softBounces;
    }

    public java.lang.Integer getOtherBounces() {
        return otherBounces;
    }

    public void setOtherBounces(java.lang.Integer otherBounces) {
        this.otherBounces = otherBounces;
    }

    public java.lang.Integer getForwardedEmails() {
        return forwardedEmails;
    }

    public void setForwardedEmails(java.lang.Integer forwardedEmails) {
        this.forwardedEmails = forwardedEmails;
    }

    public java.lang.Integer getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(java.lang.Integer uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

    public java.lang.Integer getUniqueOpens() {
        return uniqueOpens;
    }

    public void setUniqueOpens(java.lang.Integer uniqueOpens) {
        this.uniqueOpens = uniqueOpens;
    }

    public java.lang.Integer getNumberSent() {
        return numberSent;
    }

    public void setNumberSent(java.lang.Integer numberSent) {
        this.numberSent = numberSent;
    }

    public java.lang.Integer getNumberDelivered() {
        return numberDelivered;
    }

    public void setNumberDelivered(java.lang.Integer numberDelivered) {
        this.numberDelivered = numberDelivered;
    }

    public java.lang.Integer getUnsubscribes() {
        return unsubscribes;
    }

    public void setUnsubscribes(java.lang.Integer unsubscribes) {
        this.unsubscribes = unsubscribes;
    }

    public java.lang.Integer getMissingAddresses() {
        return missingAddresses;
    }

    public void setMissingAddresses(java.lang.Integer missingAddresses) {
        this.missingAddresses = missingAddresses;
    }

    public java.lang.String getSubject() {
        return subject;
    }

    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }

    public java.lang.String getPreviewURL() {
        return previewURL;
    }

    public void setPreviewURL(java.lang.String previewURL) {
        this.previewURL = previewURL;
    }

    public com.exacttarget.Link[] getLinks() {
        return links;
    }

    public void setLinks(com.exacttarget.Link[] links) {
        this.links = links;
    }

    public com.exacttarget.Link getLinks(int i) {
        return this.links[i];
    }

    public void setLinks(int i, com.exacttarget.Link value) {
        this.links[i] = value;
    }

    public com.exacttarget.TrackingEvent[] getEvents() {
        return events;
    }

    public void setEvents(com.exacttarget.TrackingEvent[] events) {
        this.events = events;
    }

    public com.exacttarget.TrackingEvent getEvents(int i) {
        return this.events[i];
    }

    public void setEvents(int i, com.exacttarget.TrackingEvent value) {
        this.events[i] = value;
    }

    public java.util.Calendar getSentDate() {
        return sentDate;
    }

    public void setSentDate(java.util.Calendar sentDate) {
        this.sentDate = sentDate;
    }

    public java.lang.String getEmailName() {
        return emailName;
    }

    public void setEmailName(java.lang.String emailName) {
        this.emailName = emailName;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.Boolean getIsMultipart() {
        return isMultipart;
    }

    public void setIsMultipart(java.lang.Boolean isMultipart) {
        this.isMultipart = isMultipart;
    }

    public java.lang.Integer getSendLimit() {
        return sendLimit;
    }

    public void setSendLimit(java.lang.Integer sendLimit) {
        this.sendLimit = sendLimit;
    }

    public java.util.Calendar getSendWindowOpen() {
        return sendWindowOpen;
    }

    public void setSendWindowOpen(java.util.Calendar sendWindowOpen) {
        this.sendWindowOpen = sendWindowOpen;
    }

    public java.util.Calendar getSendWindowClose() {
        return sendWindowClose;
    }

    public void setSendWindowClose(java.util.Calendar sendWindowClose) {
        this.sendWindowClose = sendWindowClose;
    }

    public java.lang.Boolean getIsAlwaysOn() {
        return isAlwaysOn;
    }

    public void setIsAlwaysOn(java.lang.Boolean isAlwaysOn) {
        this.isAlwaysOn = isAlwaysOn;
    }

    public com.exacttarget.APIObject[] getSources() {
        return sources;
    }

    public void setSources(com.exacttarget.APIObject[] sources) {
        this.sources = sources;
    }

    public java.lang.Integer getNumberTargeted() {
        return numberTargeted;
    }

    public void setNumberTargeted(java.lang.Integer numberTargeted) {
        this.numberTargeted = numberTargeted;
    }

    public java.lang.Integer getNumberErrored() {
        return numberErrored;
    }

    public void setNumberErrored(java.lang.Integer numberErrored) {
        this.numberErrored = numberErrored;
    }

    public java.lang.Integer getNumberExcluded() {
        return numberExcluded;
    }

    public void setNumberExcluded(java.lang.Integer numberExcluded) {
        this.numberExcluded = numberExcluded;
    }

    public java.lang.String getAdditional() {
        return additional;
    }

    public void setAdditional(java.lang.String additional) {
        this.additional = additional;
    }

    public java.lang.String getBccEmail() {
        return bccEmail;
    }

    public void setBccEmail(java.lang.String bccEmail) {
        this.bccEmail = bccEmail;
    }

    public com.exacttarget.EmailSendDefinition getEmailSendDefinition() {
        return emailSendDefinition;
    }

    public void setEmailSendDefinition(com.exacttarget.EmailSendDefinition emailSendDefinition) {
        this.emailSendDefinition = emailSendDefinition;
    }

}
