/**
 * SystemStatusType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SystemStatusType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SystemStatusType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _OK = "OK";
    public static final java.lang.String _UnplannedOutage = "UnplannedOutage";
    public static final java.lang.String _InMaintenance = "InMaintenance";
    public static final SystemStatusType OK = new SystemStatusType(_OK);
    public static final SystemStatusType UnplannedOutage = new SystemStatusType(_UnplannedOutage);
    public static final SystemStatusType InMaintenance = new SystemStatusType(_InMaintenance);
    public java.lang.String getValue() { return _value_;}
    public static SystemStatusType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SystemStatusType enumeration = (SystemStatusType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SystemStatusType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
