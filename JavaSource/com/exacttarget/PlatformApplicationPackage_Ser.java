/**
 * PlatformApplicationPackage_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PlatformApplicationPackage_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public PlatformApplicationPackage_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_645;
           context.qName2String(elemQName, true);
           elemQName = QName_0_777;
           context.qName2String(elemQName, true);
           elemQName = QName_0_778;
           context.qName2String(elemQName, true);
           elemQName = QName_0_775;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        PlatformApplicationPackage bean = (PlatformApplicationPackage) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_645;
          propValue = bean.getResourceSpecification();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_645,
              false,null,context);
          propQName = QName_0_777;
          propValue = bean.getSigningKey();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_779,
              false,null,context);
          propQName = QName_0_778;
          propValue = bean.getIsUpgrade();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_775;
          propValue = bean.getDeveloperVersion();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_779 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PublicKeyManagement");
    private final static javax.xml.namespace.QName QName_0_775 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeveloperVersion");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_778 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsUpgrade");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_645 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ResourceSpecification");
    private final static javax.xml.namespace.QName QName_0_777 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SigningKey");
}
