/**
 * AsyncResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AsyncResponse  {
    private com.exacttarget.AsyncResponseType responseType;
    private java.lang.String responseAddress;
    private com.exacttarget.RespondWhen respondWhen;
    private java.lang.Boolean includeResults;
    private java.lang.Boolean includeObjects;
    private java.lang.Boolean onlyIncludeBase;

    public AsyncResponse() {
    }

    public com.exacttarget.AsyncResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(com.exacttarget.AsyncResponseType responseType) {
        this.responseType = responseType;
    }

    public java.lang.String getResponseAddress() {
        return responseAddress;
    }

    public void setResponseAddress(java.lang.String responseAddress) {
        this.responseAddress = responseAddress;
    }

    public com.exacttarget.RespondWhen getRespondWhen() {
        return respondWhen;
    }

    public void setRespondWhen(com.exacttarget.RespondWhen respondWhen) {
        this.respondWhen = respondWhen;
    }

    public java.lang.Boolean getIncludeResults() {
        return includeResults;
    }

    public void setIncludeResults(java.lang.Boolean includeResults) {
        this.includeResults = includeResults;
    }

    public java.lang.Boolean getIncludeObjects() {
        return includeObjects;
    }

    public void setIncludeObjects(java.lang.Boolean includeObjects) {
        this.includeObjects = includeObjects;
    }

    public java.lang.Boolean getOnlyIncludeBase() {
        return onlyIncludeBase;
    }

    public void setOnlyIncludeBase(java.lang.Boolean onlyIncludeBase) {
        this.onlyIncludeBase = onlyIncludeBase;
    }

}
