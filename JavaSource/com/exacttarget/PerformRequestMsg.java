/**
 * PerformRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PerformRequestMsg  {
    private com.exacttarget.PerformOptions options;
    private java.lang.String action;
    private com.exacttarget.APIObject[] definitions;

    public PerformRequestMsg() {
    }

    public com.exacttarget.PerformOptions getOptions() {
        return options;
    }

    public void setOptions(com.exacttarget.PerformOptions options) {
        this.options = options;
    }

    public java.lang.String getAction() {
        return action;
    }

    public void setAction(java.lang.String action) {
        this.action = action;
    }

    public com.exacttarget.APIObject[] getDefinitions() {
        return definitions;
    }

    public void setDefinitions(com.exacttarget.APIObject[] definitions) {
        this.definitions = definitions;
    }

}
