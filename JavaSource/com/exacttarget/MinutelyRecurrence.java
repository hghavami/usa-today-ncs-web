/**
 * MinutelyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class MinutelyRecurrence  extends com.exacttarget.Recurrence  {
    private com.exacttarget.MinutelyRecurrencePatternTypeEnum minutelyRecurrencePatternType;
    private java.lang.Integer minuteInterval;

    public MinutelyRecurrence() {
    }

    public com.exacttarget.MinutelyRecurrencePatternTypeEnum getMinutelyRecurrencePatternType() {
        return minutelyRecurrencePatternType;
    }

    public void setMinutelyRecurrencePatternType(com.exacttarget.MinutelyRecurrencePatternTypeEnum minutelyRecurrencePatternType) {
        this.minutelyRecurrencePatternType = minutelyRecurrencePatternType;
    }

    public java.lang.Integer getMinuteInterval() {
        return minuteInterval;
    }

    public void setMinuteInterval(java.lang.Integer minuteInterval) {
        this.minuteInterval = minuteInterval;
    }

}
