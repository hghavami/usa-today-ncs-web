/**
 * EmailSendDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class EmailSendDefinition_Deser extends com.exacttarget.SendDefinition_Deser {
    /**
     * Constructor
     */
    public EmailSendDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new EmailSendDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_466) {
          ((EmailSendDefinition)value).setBccEmail(strValue);
          return true;}
        else if (qName==QName_0_554) {
          ((EmailSendDefinition)value).setAutoBccEmail(strValue);
          return true;}
        else if (qName==QName_0_555) {
          ((EmailSendDefinition)value).setTestEmailAddr(strValue);
          return true;}
        else if (qName==QName_0_467) {
          ((EmailSendDefinition)value).setEmailSubject(strValue);
          return true;}
        else if (qName==QName_0_468) {
          ((EmailSendDefinition)value).setDynamicEmailSubject(strValue);
          return true;}
        else if (qName==QName_0_422) {
          ((EmailSendDefinition)value).setIsMultipart(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_469) {
          ((EmailSendDefinition)value).setIsWrapped(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_423) {
          ((EmailSendDefinition)value).setSendLimit(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_424) {
          ((EmailSendDefinition)value).setSendWindowOpen(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_425) {
          ((EmailSendDefinition)value).setSendWindowClose(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_472) {
          ((EmailSendDefinition)value).setSendWindowDelete(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_556) {
          ((EmailSendDefinition)value).setDeduplicateByEmail(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_474) {
          ((EmailSendDefinition)value).setExclusionFilter(strValue);
          return true;}
        else if (qName==QName_0_558) {
          ((EmailSendDefinition)value).setAdditional(strValue);
          return true;}
        else if (qName==QName_0_477) {
          ((EmailSendDefinition)value).setCCEmail(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_178) {
          ((EmailSendDefinition)value).setEmail((com.exacttarget.Email)objValue);
          return true;}
        else if (qName==QName_0_557) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.TrackingUser[] array = new com.exacttarget.TrackingUser[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((EmailSendDefinition)value).setTrackingUsers(array);
          } else { 
            ((EmailSendDefinition)value).setTrackingUsers((com.exacttarget.TrackingUser[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_553) {
          com.exacttarget.SendDefinitionList[] array = new com.exacttarget.SendDefinitionList[listValue.size()];
          listValue.toArray(array);
          ((EmailSendDefinition)value).setSendDefinitionList(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_557 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TrackingUsers");
    private final static javax.xml.namespace.QName QName_0_472 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowDelete");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_469 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsWrapped");
    private final static javax.xml.namespace.QName QName_0_467 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailSubject");
    private final static javax.xml.namespace.QName QName_0_555 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TestEmailAddr");
    private final static javax.xml.namespace.QName QName_0_558 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Additional");
    private final static javax.xml.namespace.QName QName_0_468 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DynamicEmailSubject");
    private final static javax.xml.namespace.QName QName_0_474 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionFilter");
    private final static javax.xml.namespace.QName QName_0_477 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CCEmail");
    private final static javax.xml.namespace.QName QName_0_554 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoBccEmail");
    private final static javax.xml.namespace.QName QName_0_422 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsMultipart");
    private final static javax.xml.namespace.QName QName_0_424 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowOpen");
    private final static javax.xml.namespace.QName QName_0_425 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowClose");
    private final static javax.xml.namespace.QName QName_0_553 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendDefinitionList");
    private final static javax.xml.namespace.QName QName_0_466 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BccEmail");
    private final static javax.xml.namespace.QName QName_0_556 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeduplicateByEmail");
    private final static javax.xml.namespace.QName QName_0_423 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendLimit");
}
