/**
 * ExtractDefinition_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ExtractDefinition_Deser extends com.exacttarget.ExtractTemplate_Deser {
    /**
     * Constructor
     */
    public ExtractDefinition_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ExtractDefinition();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_93) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.ExtractParameterDescription[] array = new com.exacttarget.ExtractParameterDescription[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ExtractDefinition)value).setParameters(array);
          } else { 
            ((ExtractDefinition)value).setParameters((com.exacttarget.ExtractParameterDescription[])objValue);}
          return true;}
        else if (qName==QName_0_693) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.APIProperty[] array = new com.exacttarget.APIProperty[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ExtractDefinition)value).setValues(array);
          } else { 
            ((ExtractDefinition)value).setValues((com.exacttarget.APIProperty[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_693 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Values");
    private final static javax.xml.namespace.QName QName_0_93 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Parameters");
}
