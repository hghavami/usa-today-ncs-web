/**
 * Account.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Account  extends com.exacttarget.APIObject  {
    private com.exacttarget.AccountTypeEnum accountType;
    private java.lang.Integer parentID;
    private java.lang.Integer brandID;
    private java.lang.Integer privateLabelID;
    private java.lang.Integer reportingParentID;
    private java.lang.String name;
    private java.lang.String email;
    private java.lang.String fromName;
    private java.lang.String businessName;
    private java.lang.String phone;
    private java.lang.String address;
    private java.lang.String fax;
    private java.lang.String city;
    private java.lang.String state;
    private java.lang.String zip;
    private java.lang.String country;
    private java.lang.Integer isActive;
    private java.lang.Boolean isTestAccount;
    private java.lang.Integer orgID;
    private java.lang.Integer DBID;
    private java.lang.String parentName;
    private java.lang.Long customerID;
    private java.util.Calendar deletedDate;
    private java.lang.Integer editionID;
    private com.exacttarget.AccountDataItem[] children;
    private com.exacttarget.Subscription subscription;
    private com.exacttarget.PrivateLabel[] privateLabels;
    private com.exacttarget.BusinessRule[] businessRules;
    private com.exacttarget.AccountUser[] accountUsers;
    private java.lang.Boolean inheritAddress;
    private java.lang.Boolean isTrialAccount;
    private com.exacttarget.Locale locale;
    private com.exacttarget.Account parentAccount;
    private com.exacttarget.TimeZone timeZone;
    private com.exacttarget.Role[] roles;

    public Account() {
    }

    public com.exacttarget.AccountTypeEnum getAccountType() {
        return accountType;
    }

    public void setAccountType(com.exacttarget.AccountTypeEnum accountType) {
        this.accountType = accountType;
    }

    public java.lang.Integer getParentID() {
        return parentID;
    }

    public void setParentID(java.lang.Integer parentID) {
        this.parentID = parentID;
    }

    public java.lang.Integer getBrandID() {
        return brandID;
    }

    public void setBrandID(java.lang.Integer brandID) {
        this.brandID = brandID;
    }

    public java.lang.Integer getPrivateLabelID() {
        return privateLabelID;
    }

    public void setPrivateLabelID(java.lang.Integer privateLabelID) {
        this.privateLabelID = privateLabelID;
    }

    public java.lang.Integer getReportingParentID() {
        return reportingParentID;
    }

    public void setReportingParentID(java.lang.Integer reportingParentID) {
        this.reportingParentID = reportingParentID;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getEmail() {
        return email;
    }

    public void setEmail(java.lang.String email) {
        this.email = email;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(java.lang.String businessName) {
        this.businessName = businessName;
    }

    public java.lang.String getPhone() {
        return phone;
    }

    public void setPhone(java.lang.String phone) {
        this.phone = phone;
    }

    public java.lang.String getAddress() {
        return address;
    }

    public void setAddress(java.lang.String address) {
        this.address = address;
    }

    public java.lang.String getFax() {
        return fax;
    }

    public void setFax(java.lang.String fax) {
        this.fax = fax;
    }

    public java.lang.String getCity() {
        return city;
    }

    public void setCity(java.lang.String city) {
        this.city = city;
    }

    public java.lang.String getState() {
        return state;
    }

    public void setState(java.lang.String state) {
        this.state = state;
    }

    public java.lang.String getZip() {
        return zip;
    }

    public void setZip(java.lang.String zip) {
        this.zip = zip;
    }

    public java.lang.String getCountry() {
        return country;
    }

    public void setCountry(java.lang.String country) {
        this.country = country;
    }

    public java.lang.Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Integer isActive) {
        this.isActive = isActive;
    }

    public java.lang.Boolean getIsTestAccount() {
        return isTestAccount;
    }

    public void setIsTestAccount(java.lang.Boolean isTestAccount) {
        this.isTestAccount = isTestAccount;
    }

    public java.lang.Integer getOrgID() {
        return orgID;
    }

    public void setOrgID(java.lang.Integer orgID) {
        this.orgID = orgID;
    }

    public java.lang.Integer getDBID() {
        return DBID;
    }

    public void setDBID(java.lang.Integer DBID) {
        this.DBID = DBID;
    }

    public java.lang.String getParentName() {
        return parentName;
    }

    public void setParentName(java.lang.String parentName) {
        this.parentName = parentName;
    }

    public java.lang.Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(java.lang.Long customerID) {
        this.customerID = customerID;
    }

    public java.util.Calendar getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(java.util.Calendar deletedDate) {
        this.deletedDate = deletedDate;
    }

    public java.lang.Integer getEditionID() {
        return editionID;
    }

    public void setEditionID(java.lang.Integer editionID) {
        this.editionID = editionID;
    }

    public com.exacttarget.AccountDataItem[] getChildren() {
        return children;
    }

    public void setChildren(com.exacttarget.AccountDataItem[] children) {
        this.children = children;
    }

    public com.exacttarget.AccountDataItem getChildren(int i) {
        return this.children[i];
    }

    public void setChildren(int i, com.exacttarget.AccountDataItem value) {
        this.children[i] = value;
    }

    public com.exacttarget.Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(com.exacttarget.Subscription subscription) {
        this.subscription = subscription;
    }

    public com.exacttarget.PrivateLabel[] getPrivateLabels() {
        return privateLabels;
    }

    public void setPrivateLabels(com.exacttarget.PrivateLabel[] privateLabels) {
        this.privateLabels = privateLabels;
    }

    public com.exacttarget.PrivateLabel getPrivateLabels(int i) {
        return this.privateLabels[i];
    }

    public void setPrivateLabels(int i, com.exacttarget.PrivateLabel value) {
        this.privateLabels[i] = value;
    }

    public com.exacttarget.BusinessRule[] getBusinessRules() {
        return businessRules;
    }

    public void setBusinessRules(com.exacttarget.BusinessRule[] businessRules) {
        this.businessRules = businessRules;
    }

    public com.exacttarget.BusinessRule getBusinessRules(int i) {
        return this.businessRules[i];
    }

    public void setBusinessRules(int i, com.exacttarget.BusinessRule value) {
        this.businessRules[i] = value;
    }

    public com.exacttarget.AccountUser[] getAccountUsers() {
        return accountUsers;
    }

    public void setAccountUsers(com.exacttarget.AccountUser[] accountUsers) {
        this.accountUsers = accountUsers;
    }

    public com.exacttarget.AccountUser getAccountUsers(int i) {
        return this.accountUsers[i];
    }

    public void setAccountUsers(int i, com.exacttarget.AccountUser value) {
        this.accountUsers[i] = value;
    }

    public java.lang.Boolean getInheritAddress() {
        return inheritAddress;
    }

    public void setInheritAddress(java.lang.Boolean inheritAddress) {
        this.inheritAddress = inheritAddress;
    }

    public java.lang.Boolean getIsTrialAccount() {
        return isTrialAccount;
    }

    public void setIsTrialAccount(java.lang.Boolean isTrialAccount) {
        this.isTrialAccount = isTrialAccount;
    }

    public com.exacttarget.Locale getLocale() {
        return locale;
    }

    public void setLocale(com.exacttarget.Locale locale) {
        this.locale = locale;
    }

    public com.exacttarget.Account getParentAccount() {
        return parentAccount;
    }

    public void setParentAccount(com.exacttarget.Account parentAccount) {
        this.parentAccount = parentAccount;
    }

    public com.exacttarget.TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(com.exacttarget.TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public com.exacttarget.Role[] getRoles() {
        return roles;
    }

    public void setRoles(com.exacttarget.Role[] roles) {
        this.roles = roles;
    }

}
