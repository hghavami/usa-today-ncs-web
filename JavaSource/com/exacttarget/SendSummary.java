/**
 * SendSummary.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendSummary  extends com.exacttarget.APIObject  {
    private java.lang.Integer accountID;
    private java.lang.String accountName;
    private java.lang.String accountEmail;
    private java.lang.Boolean isTestAccount;
    private java.lang.Integer sendID;
    private java.lang.String deliveredTime;
    private java.lang.Integer totalSent;
    private java.lang.Integer transactional;
    private java.lang.Integer nonTransactional;

    public SendSummary() {
    }

    public java.lang.Integer getAccountID() {
        return accountID;
    }

    public void setAccountID(java.lang.Integer accountID) {
        this.accountID = accountID;
    }

    public java.lang.String getAccountName() {
        return accountName;
    }

    public void setAccountName(java.lang.String accountName) {
        this.accountName = accountName;
    }

    public java.lang.String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(java.lang.String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public java.lang.Boolean getIsTestAccount() {
        return isTestAccount;
    }

    public void setIsTestAccount(java.lang.Boolean isTestAccount) {
        this.isTestAccount = isTestAccount;
    }

    public java.lang.Integer getSendID() {
        return sendID;
    }

    public void setSendID(java.lang.Integer sendID) {
        this.sendID = sendID;
    }

    public java.lang.String getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(java.lang.String deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public java.lang.Integer getTotalSent() {
        return totalSent;
    }

    public void setTotalSent(java.lang.Integer totalSent) {
        this.totalSent = totalSent;
    }

    public java.lang.Integer getTransactional() {
        return transactional;
    }

    public void setTransactional(java.lang.Integer transactional) {
        this.transactional = transactional;
    }

    public java.lang.Integer getNonTransactional() {
        return nonTransactional;
    }

    public void setNonTransactional(java.lang.Integer nonTransactional) {
        this.nonTransactional = nonTransactional;
    }

}
