/**
 * BusinessUnit.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class BusinessUnit  extends com.exacttarget.Account  {
    private java.lang.String description;
    private com.exacttarget.SendClassification defaultSendClassification;
    private com.exacttarget.LandingPage defaultHomePage;
    private com.exacttarget.FilterPart subscriberFilter;
    private com.exacttarget.UnsubscribeBehaviorEnum masterUnsubscribeBehavior;

    public BusinessUnit() {
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.SendClassification getDefaultSendClassification() {
        return defaultSendClassification;
    }

    public void setDefaultSendClassification(com.exacttarget.SendClassification defaultSendClassification) {
        this.defaultSendClassification = defaultSendClassification;
    }

    public com.exacttarget.LandingPage getDefaultHomePage() {
        return defaultHomePage;
    }

    public void setDefaultHomePage(com.exacttarget.LandingPage defaultHomePage) {
        this.defaultHomePage = defaultHomePage;
    }

    public com.exacttarget.FilterPart getSubscriberFilter() {
        return subscriberFilter;
    }

    public void setSubscriberFilter(com.exacttarget.FilterPart subscriberFilter) {
        this.subscriberFilter = subscriberFilter;
    }

    public com.exacttarget.UnsubscribeBehaviorEnum getMasterUnsubscribeBehavior() {
        return masterUnsubscribeBehavior;
    }

    public void setMasterUnsubscribeBehavior(com.exacttarget.UnsubscribeBehaviorEnum masterUnsubscribeBehavior) {
        this.masterUnsubscribeBehavior = masterUnsubscribeBehavior;
    }

}
