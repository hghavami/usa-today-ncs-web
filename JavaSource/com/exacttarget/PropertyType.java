/**
 * PropertyType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PropertyType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected PropertyType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _value1 = "string";
    public static final java.lang.String _value2 = "boolean";
    public static final java.lang.String _value3 = "double";
    public static final java.lang.String _value4 = "datetime";
    public static final PropertyType value1 = new PropertyType(_value1);
    public static final PropertyType value2 = new PropertyType(_value2);
    public static final PropertyType value3 = new PropertyType(_value3);
    public static final PropertyType value4 = new PropertyType(_value4);
    public java.lang.String getValue() { return _value_;}
    public static PropertyType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        PropertyType enumeration = (PropertyType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static PropertyType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
