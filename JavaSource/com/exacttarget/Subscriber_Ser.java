/**
 * Subscriber_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Subscriber_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public Subscriber_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_195;
           context.qName2String(elemQName, true);
           elemQName = QName_0_196;
           context.qName2String(elemQName, true);
           elemQName = QName_0_197;
           context.qName2String(elemQName, true);
           elemQName = QName_0_198;
           context.qName2String(elemQName, true);
           elemQName = QName_0_150;
           context.qName2String(elemQName, true);
           elemQName = QName_0_199;
           context.qName2String(elemQName, true);
           elemQName = QName_0_200;
           context.qName2String(elemQName, true);
           elemQName = QName_0_201;
           context.qName2String(elemQName, true);
           elemQName = QName_0_202;
           context.qName2String(elemQName, true);
           elemQName = QName_0_203;
           context.qName2String(elemQName, true);
           elemQName = QName_0_204;
           context.qName2String(elemQName, true);
           elemQName = QName_0_205;
           context.qName2String(elemQName, true);
           elemQName = QName_0_206;
           context.qName2String(elemQName, true);
           elemQName = QName_0_207;
           context.qName2String(elemQName, true);
           elemQName = QName_0_679;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        Subscriber bean = (Subscriber) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_195;
          propValue = bean.getEmailAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_196;
          {
            propValue = bean.getAttributes();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_208,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_197;
          propValue = bean.getSubscriberKey();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_198;
          propValue = bean.getUnsubscribedDate();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_150;
          propValue = bean.getStatus();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_209,
              false,null,context);
          propQName = QName_0_199;
          propValue = bean.getPartnerType();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_200;
          propValue = bean.getEmailTypePreference();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_190,
              false,null,context);
          propQName = QName_0_201;
          {
            propValue = bean.getLists();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_210,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_202;
          propValue = bean.getGlobalUnsubscribeCategory();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_202,
              false,null,context);
          propQName = QName_0_203;
          propValue = bean.getSubscriberTypeDefinition();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_203,
              false,null,context);
          propQName = QName_0_204;
          propValue = bean.getAddresses();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_211,
              false,null,context);
          propQName = QName_0_205;
          propValue = bean.getPrimarySMSAddress();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_212,
              false,null,context);
          propQName = QName_0_206;
          propValue = bean.getPrimarySMSPublicationStatus();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_213,
              false,null,context);
          propQName = QName_0_207;
          propValue = bean.getPrimaryEmailAddress();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_195,
              false,null,context);
          propQName = QName_0_679;
          propValue = bean.getLocale();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_679,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_199 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerType");
    private final static javax.xml.namespace.QName QName_0_208 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Attribute");
    private final static javax.xml.namespace.QName QName_0_212 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SMSAddress");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_207 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimaryEmailAddress");
    private final static javax.xml.namespace.QName QName_0_198 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UnsubscribedDate");
    private final static javax.xml.namespace.QName QName_0_213 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberAddressStatus");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_211 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">Subscriber>Addresses");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_1_13 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "dateTime");
    private final static javax.xml.namespace.QName QName_0_196 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Attributes");
    private final static javax.xml.namespace.QName QName_0_200 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailTypePreference");
    private final static javax.xml.namespace.QName QName_0_197 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberKey");
    private final static javax.xml.namespace.QName QName_0_195 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailAddress");
    private final static javax.xml.namespace.QName QName_0_209 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberStatus");
    private final static javax.xml.namespace.QName QName_0_201 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Lists");
    private final static javax.xml.namespace.QName QName_0_206 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimarySMSPublicationStatus");
    private final static javax.xml.namespace.QName QName_0_204 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Addresses");
    private final static javax.xml.namespace.QName QName_0_202 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "GlobalUnsubscribeCategory");
    private final static javax.xml.namespace.QName QName_0_190 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailType");
    private final static javax.xml.namespace.QName QName_0_205 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PrimarySMSAddress");
    private final static javax.xml.namespace.QName QName_0_203 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberTypeDefinition");
    private final static javax.xml.namespace.QName QName_0_210 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberList");
}
