/**
 * ListAttributeRestrictedValue.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ListAttributeRestrictedValue  extends com.exacttarget.APIObject  {
    private java.lang.String valueName;
    private java.lang.Boolean isDefault;
    private java.lang.Integer displayOrder;
    private java.lang.String description;

    public ListAttributeRestrictedValue() {
    }

    public java.lang.String getValueName() {
        return valueName;
    }

    public void setValueName(java.lang.String valueName) {
        this.valueName = valueName;
    }

    public java.lang.Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(java.lang.Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public java.lang.Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(java.lang.Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

}
