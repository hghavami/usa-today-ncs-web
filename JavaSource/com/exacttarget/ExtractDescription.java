/**
 * ExtractDescription.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExtractDescription  extends com.exacttarget.ExtractTemplate  {
    private com.exacttarget.ExtractParameterDescription[] parameters;

    public ExtractDescription() {
    }

    public com.exacttarget.ExtractParameterDescription[] getParameters() {
        return parameters;
    }

    public void setParameters(com.exacttarget.ExtractParameterDescription[] parameters) {
        this.parameters = parameters;
    }

}
