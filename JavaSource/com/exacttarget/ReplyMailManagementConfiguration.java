/**
 * ReplyMailManagementConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ReplyMailManagementConfiguration  extends com.exacttarget.APIObject  {
    private java.lang.String emailDisplayName;
    private java.lang.String replySubdomain;
    private java.lang.String emailReplyAddress;
    private java.lang.Boolean DNSRedirectComplete;
    private java.lang.Boolean deleteAutoReplies;
    private java.lang.Boolean supportUnsubscribes;
    private java.lang.Boolean supportUnsubKeyword;
    private java.lang.Boolean supportUnsubscribeKeyword;
    private java.lang.Boolean supportRemoveKeyword;
    private java.lang.Boolean supportOptOutKeyword;
    private java.lang.Boolean supportLeaveKeyword;
    private java.lang.Boolean supportMisspelledKeywords;
    private java.lang.Boolean sendAutoReplies;
    private java.lang.String autoReplySubject;
    private java.lang.String autoReplyBody;
    private java.lang.String forwardingAddress;

    public ReplyMailManagementConfiguration() {
    }

    public java.lang.String getEmailDisplayName() {
        return emailDisplayName;
    }

    public void setEmailDisplayName(java.lang.String emailDisplayName) {
        this.emailDisplayName = emailDisplayName;
    }

    public java.lang.String getReplySubdomain() {
        return replySubdomain;
    }

    public void setReplySubdomain(java.lang.String replySubdomain) {
        this.replySubdomain = replySubdomain;
    }

    public java.lang.String getEmailReplyAddress() {
        return emailReplyAddress;
    }

    public void setEmailReplyAddress(java.lang.String emailReplyAddress) {
        this.emailReplyAddress = emailReplyAddress;
    }

    public java.lang.Boolean getDNSRedirectComplete() {
        return DNSRedirectComplete;
    }

    public void setDNSRedirectComplete(java.lang.Boolean DNSRedirectComplete) {
        this.DNSRedirectComplete = DNSRedirectComplete;
    }

    public java.lang.Boolean getDeleteAutoReplies() {
        return deleteAutoReplies;
    }

    public void setDeleteAutoReplies(java.lang.Boolean deleteAutoReplies) {
        this.deleteAutoReplies = deleteAutoReplies;
    }

    public java.lang.Boolean getSupportUnsubscribes() {
        return supportUnsubscribes;
    }

    public void setSupportUnsubscribes(java.lang.Boolean supportUnsubscribes) {
        this.supportUnsubscribes = supportUnsubscribes;
    }

    public java.lang.Boolean getSupportUnsubKeyword() {
        return supportUnsubKeyword;
    }

    public void setSupportUnsubKeyword(java.lang.Boolean supportUnsubKeyword) {
        this.supportUnsubKeyword = supportUnsubKeyword;
    }

    public java.lang.Boolean getSupportUnsubscribeKeyword() {
        return supportUnsubscribeKeyword;
    }

    public void setSupportUnsubscribeKeyword(java.lang.Boolean supportUnsubscribeKeyword) {
        this.supportUnsubscribeKeyword = supportUnsubscribeKeyword;
    }

    public java.lang.Boolean getSupportRemoveKeyword() {
        return supportRemoveKeyword;
    }

    public void setSupportRemoveKeyword(java.lang.Boolean supportRemoveKeyword) {
        this.supportRemoveKeyword = supportRemoveKeyword;
    }

    public java.lang.Boolean getSupportOptOutKeyword() {
        return supportOptOutKeyword;
    }

    public void setSupportOptOutKeyword(java.lang.Boolean supportOptOutKeyword) {
        this.supportOptOutKeyword = supportOptOutKeyword;
    }

    public java.lang.Boolean getSupportLeaveKeyword() {
        return supportLeaveKeyword;
    }

    public void setSupportLeaveKeyword(java.lang.Boolean supportLeaveKeyword) {
        this.supportLeaveKeyword = supportLeaveKeyword;
    }

    public java.lang.Boolean getSupportMisspelledKeywords() {
        return supportMisspelledKeywords;
    }

    public void setSupportMisspelledKeywords(java.lang.Boolean supportMisspelledKeywords) {
        this.supportMisspelledKeywords = supportMisspelledKeywords;
    }

    public java.lang.Boolean getSendAutoReplies() {
        return sendAutoReplies;
    }

    public void setSendAutoReplies(java.lang.Boolean sendAutoReplies) {
        this.sendAutoReplies = sendAutoReplies;
    }

    public java.lang.String getAutoReplySubject() {
        return autoReplySubject;
    }

    public void setAutoReplySubject(java.lang.String autoReplySubject) {
        this.autoReplySubject = autoReplySubject;
    }

    public java.lang.String getAutoReplyBody() {
        return autoReplyBody;
    }

    public void setAutoReplyBody(java.lang.String autoReplyBody) {
        this.autoReplyBody = autoReplyBody;
    }

    public java.lang.String getForwardingAddress() {
        return forwardingAddress;
    }

    public void setForwardingAddress(java.lang.String forwardingAddress) {
        this.forwardingAddress = forwardingAddress;
    }

}
