/**
 * PermissionSet.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PermissionSet  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.Boolean isAllowed;
    private java.lang.Boolean isDenied;
    private com.exacttarget.PermissionSet[] permissionSets;
    private com.exacttarget.Permission[] permissions;

    public PermissionSet() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.Boolean getIsAllowed() {
        return isAllowed;
    }

    public void setIsAllowed(java.lang.Boolean isAllowed) {
        this.isAllowed = isAllowed;
    }

    public java.lang.Boolean getIsDenied() {
        return isDenied;
    }

    public void setIsDenied(java.lang.Boolean isDenied) {
        this.isDenied = isDenied;
    }

    public com.exacttarget.PermissionSet[] getPermissionSets() {
        return permissionSets;
    }

    public void setPermissionSets(com.exacttarget.PermissionSet[] permissionSets) {
        this.permissionSets = permissionSets;
    }

    public com.exacttarget.Permission[] getPermissions() {
        return permissions;
    }

    public void setPermissions(com.exacttarget.Permission[] permissions) {
        this.permissions = permissions;
    }

}
