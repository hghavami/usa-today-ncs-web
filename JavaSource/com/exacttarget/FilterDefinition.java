/**
 * FilterDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class FilterDefinition  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private com.exacttarget.APIObject dataSource;
    private com.exacttarget.FilterPart dataFilter;

    public FilterDefinition() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.APIObject getDataSource() {
        return dataSource;
    }

    public void setDataSource(com.exacttarget.APIObject dataSource) {
        this.dataSource = dataSource;
    }

    public com.exacttarget.FilterPart getDataFilter() {
        return dataFilter;
    }

    public void setDataFilter(com.exacttarget.FilterPart dataFilter) {
        this.dataFilter = dataFilter;
    }

}
