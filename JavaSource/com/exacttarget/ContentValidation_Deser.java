/**
 * ContentValidation_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ContentValidation_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public ContentValidation_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ContentValidation();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_177) {
          ((ContentValidation)value).setValidationAction((com.exacttarget.ValidationAction)objValue);
          return true;}
        else if (qName==QName_0_178) {
          ((ContentValidation)value).setEmail((com.exacttarget.Email)objValue);
          return true;}
        else if (qName==QName_0_179) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.Subscriber[] array = new com.exacttarget.Subscriber[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ContentValidation)value).setSubscribers(array);
          } else { 
            ((ContentValidation)value).setSubscribers((com.exacttarget.Subscriber[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_177 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ValidationAction");
    private final static javax.xml.namespace.QName QName_0_179 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscribers");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
}
