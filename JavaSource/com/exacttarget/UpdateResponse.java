/**
 * UpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UpdateResponse  {
    private com.exacttarget.UpdateResult[] results;
    private java.lang.String requestID;
    private java.lang.String overallStatus;

    public UpdateResponse() {
    }

    public com.exacttarget.UpdateResult[] getResults() {
        return results;
    }

    public void setResults(com.exacttarget.UpdateResult[] results) {
        this.results = results;
    }

    public com.exacttarget.UpdateResult getResults(int i) {
        return this.results[i];
    }

    public void setResults(int i, com.exacttarget.UpdateResult value) {
        this.results[i] = value;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

    public java.lang.String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(java.lang.String overallStatus) {
        this.overallStatus = overallStatus;
    }

}
