/**
 * EmailAddress.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class EmailAddress  extends com.exacttarget.SubscriberAddress  {
    private com.exacttarget.EmailType type;

    public EmailAddress() {
    }

    public com.exacttarget.EmailType getType() {
        return type;
    }

    public void setType(com.exacttarget.EmailType type) {
        this.type = type;
    }

}
