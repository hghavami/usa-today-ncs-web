/**
 * RetrieveOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class RetrieveOptions  extends com.exacttarget.Options  {
    private java.lang.Integer batchSize;
    private java.lang.Boolean includeObjects;
    private java.lang.Boolean onlyIncludeBase;

    public RetrieveOptions() {
    }

    public java.lang.Integer getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(java.lang.Integer batchSize) {
        this.batchSize = batchSize;
    }

    public java.lang.Boolean getIncludeObjects() {
        return includeObjects;
    }

    public void setIncludeObjects(java.lang.Boolean includeObjects) {
        this.includeObjects = includeObjects;
    }

    public java.lang.Boolean getOnlyIncludeBase() {
        return onlyIncludeBase;
    }

    public void setOnlyIncludeBase(java.lang.Boolean onlyIncludeBase) {
        this.onlyIncludeBase = onlyIncludeBase;
    }

}
