/**
 * AccountDataItem.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AccountDataItem  {
    private java.lang.Integer childAccountID;
    private java.lang.Integer brandID;
    private java.lang.Integer privateLabelID;
    private java.lang.Integer accountType;

    public AccountDataItem() {
    }

    public java.lang.Integer getChildAccountID() {
        return childAccountID;
    }

    public void setChildAccountID(java.lang.Integer childAccountID) {
        this.childAccountID = childAccountID;
    }

    public java.lang.Integer getBrandID() {
        return brandID;
    }

    public void setBrandID(java.lang.Integer brandID) {
        this.brandID = brandID;
    }

    public java.lang.Integer getPrivateLabelID() {
        return privateLabelID;
    }

    public void setPrivateLabelID(java.lang.Integer privateLabelID) {
        this.privateLabelID = privateLabelID;
    }

    public java.lang.Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(java.lang.Integer accountType) {
        this.accountType = accountType;
    }

}
