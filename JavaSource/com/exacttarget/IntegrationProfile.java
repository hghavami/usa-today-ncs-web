/**
 * IntegrationProfile.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class IntegrationProfile  extends com.exacttarget.APIObject  {
    private java.lang.String profileID;
    private java.lang.String subscriberKey;
    private java.lang.String externalID;
    private java.lang.String externalType;

    public IntegrationProfile() {
    }

    public java.lang.String getProfileID() {
        return profileID;
    }

    public void setProfileID(java.lang.String profileID) {
        this.profileID = profileID;
    }

    public java.lang.String getSubscriberKey() {
        return subscriberKey;
    }

    public void setSubscriberKey(java.lang.String subscriberKey) {
        this.subscriberKey = subscriberKey;
    }

    public java.lang.String getExternalID() {
        return externalID;
    }

    public void setExternalID(java.lang.String externalID) {
        this.externalID = externalID;
    }

    public java.lang.String getExternalType() {
        return externalType;
    }

    public void setExternalType(java.lang.String externalType) {
        this.externalType = externalType;
    }

}
