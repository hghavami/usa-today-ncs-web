/**
 * ExtractDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExtractDefinition  extends com.exacttarget.ExtractTemplate  {
    private com.exacttarget.ExtractParameterDescription[] parameters;
    private com.exacttarget.APIProperty[] values;

    public ExtractDefinition() {
    }

    public com.exacttarget.ExtractParameterDescription[] getParameters() {
        return parameters;
    }

    public void setParameters(com.exacttarget.ExtractParameterDescription[] parameters) {
        this.parameters = parameters;
    }

    public com.exacttarget.APIProperty[] getValues() {
        return values;
    }

    public void setValues(com.exacttarget.APIProperty[] values) {
        this.values = values;
    }

}
