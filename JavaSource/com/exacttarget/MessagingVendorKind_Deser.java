/**
 * MessagingVendorKind_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class MessagingVendorKind_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public MessagingVendorKind_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new MessagingVendorKind();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_565) {
          ((MessagingVendorKind)value).setVendor(strValue);
          return true;}
        else if (qName==QName_0_566) {
          ((MessagingVendorKind)value).setKind(strValue);
          return true;}
        else if (qName==QName_0_567) {
          ((MessagingVendorKind)value).setIsUsernameRequired(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseboolean(strValue));
          return true;}
        else if (qName==QName_0_568) {
          ((MessagingVendorKind)value).setIsPasswordRequired(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseboolean(strValue));
          return true;}
        else if (qName==QName_0_569) {
          ((MessagingVendorKind)value).setIsProfileRequired(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseboolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_568 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsPasswordRequired");
    private final static javax.xml.namespace.QName QName_0_569 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsProfileRequired");
    private final static javax.xml.namespace.QName QName_0_567 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsUsernameRequired");
    private final static javax.xml.namespace.QName QName_0_565 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Vendor");
    private final static javax.xml.namespace.QName QName_0_566 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Kind");
}
