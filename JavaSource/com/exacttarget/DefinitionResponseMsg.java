/**
 * DefinitionResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DefinitionResponseMsg  {
    private com.exacttarget.ObjectDefinition[] objectDefinition;
    private java.lang.String requestID;

    public DefinitionResponseMsg() {
    }

    public com.exacttarget.ObjectDefinition[] getObjectDefinition() {
        return objectDefinition;
    }

    public void setObjectDefinition(com.exacttarget.ObjectDefinition[] objectDefinition) {
        this.objectDefinition = objectDefinition;
    }

    public com.exacttarget.ObjectDefinition getObjectDefinition(int i) {
        return this.objectDefinition[i];
    }

    public void setObjectDefinition(int i, com.exacttarget.ObjectDefinition value) {
        this.objectDefinition[i] = value;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

}
