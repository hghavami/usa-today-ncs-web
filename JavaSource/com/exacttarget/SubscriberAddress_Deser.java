/**
 * SubscriberAddress_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SubscriberAddress_Deser extends com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializer {
    /**
     * Constructor
     */
    public SubscriberAddress_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new SubscriberAddress();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_394) {
          ((SubscriberAddress)value).setAddressType(strValue);
          return true;}
        else if (qName==QName_0_293) {
          ((SubscriberAddress)value).setAddress(strValue);
          return true;}
        return false;
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return false;
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_395) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.AddressStatus[] array = new com.exacttarget.AddressStatus[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((SubscriberAddress)value).setStatuses(array);
          } else { 
            ((SubscriberAddress)value).setStatuses((com.exacttarget.AddressStatus[])objValue);}
          return true;}
        return false;
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return false;
    }
    private final static javax.xml.namespace.QName QName_0_293 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Address");
    private final static javax.xml.namespace.QName QName_0_395 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Statuses");
    private final static javax.xml.namespace.QName QName_0_394 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AddressType");
}
