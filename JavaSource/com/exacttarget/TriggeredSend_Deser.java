/**
 * TriggeredSend_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class TriggeredSend_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public TriggeredSend_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new TriggeredSend();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_506) {
          ((TriggeredSend)value).setTriggeredSendDefinition((com.exacttarget.TriggeredSendDefinition)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        if (qName==QName_0_179) {
          com.exacttarget.Subscriber[] array = new com.exacttarget.Subscriber[listValue.size()];
          listValue.toArray(array);
          ((TriggeredSend)value).setSubscribers(array);
          return true;}
        else if (qName==QName_0_196) {
          com.exacttarget.Attribute[] array = new com.exacttarget.Attribute[listValue.size()];
          listValue.toArray(array);
          ((TriggeredSend)value).setAttributes(array);
          return true;}
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_179 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscribers");
    private final static javax.xml.namespace.QName QName_0_506 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendDefinition");
    private final static javax.xml.namespace.QName QName_0_196 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Attributes");
}
