/**
 * AccountUser_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class AccountUser_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public AccountUser_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_350;
           context.qName2String(elemQName, true);
           elemQName = QName_0_16;
           context.qName2String(elemQName, true);
           elemQName = QName_0_351;
           context.qName2String(elemQName, true);
           elemQName = QName_0_20;
           context.qName2String(elemQName, true);
           elemQName = QName_0_178;
           context.qName2String(elemQName, true);
           elemQName = QName_0_352;
           context.qName2String(elemQName, true);
           elemQName = QName_0_353;
           context.qName2String(elemQName, true);
           elemQName = QName_0_354;
           context.qName2String(elemQName, true);
           elemQName = QName_0_355;
           context.qName2String(elemQName, true);
           elemQName = QName_0_356;
           context.qName2String(elemQName, true);
           elemQName = QName_0_340;
           context.qName2String(elemQName, true);
           elemQName = QName_0_357;
           context.qName2String(elemQName, true);
           elemQName = QName_0_358;
           context.qName2String(elemQName, true);
           elemQName = QName_0_359;
           context.qName2String(elemQName, true);
           elemQName = QName_0_360;
           context.qName2String(elemQName, true);
           elemQName = QName_0_361;
           context.qName2String(elemQName, true);
           elemQName = QName_0_362;
           context.qName2String(elemQName, true);
           elemQName = QName_0_363;
           context.qName2String(elemQName, true);
           elemQName = QName_0_679;
           context.qName2String(elemQName, true);
           elemQName = QName_0_680;
           context.qName2String(elemQName, true);
           elemQName = QName_0_681;
           context.qName2String(elemQName, true);
           elemQName = QName_0_682;
           context.qName2String(elemQName, true);
           elemQName = QName_0_683;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        AccountUser bean = (AccountUser) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_350;
          propValue = bean.getAccountUserID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_16;
          propValue = bean.getUserID();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_351;
          propValue = bean.getPassword();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_20;
          propValue = bean.getName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_178;
          propValue = bean.getEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_352;
          propValue = bean.getMustChangePassword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_353;
          propValue = bean.getActiveFlag();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_354;
          propValue = bean.getChallengePhrase();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_355;
          propValue = bean.getChallengeAnswer();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_356;
          {
            propValue = bean.getUserPermissions();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_364,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_340;
          propValue = new java.lang.Integer(bean.getDelete());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              true,null,context);
          propQName = QName_0_357;
          propValue = bean.getLastSuccessfulLogin();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_13,
              false,null,context);
          propQName = QName_0_358;
          propValue = bean.getIsAPIUser();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_359;
          propValue = bean.getNotificationEmailAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_360;
          propValue = bean.getIsLocked();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_361;
          propValue = bean.getUnlock();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_362;
          propValue = bean.getBusinessUnit();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_363;
          propValue = bean.getDefaultBusinessUnit();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_679;
          propValue = bean.getLocale();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_679,
              false,null,context);
          propQName = QName_0_680;
          propValue = bean.getTimeZone();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_680,
              false,null,context);
          propQName = QName_0_681;
          propValue = bean.getDefaultBusinessUnitObject();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_362,
              false,null,context);
          propQName = QName_0_682;
          propValue = bean.getAssociatedBusinessUnits();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_684,
              false,null,context);
          propQName = QName_0_683;
          propValue = bean.getRoles();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_685,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_356 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserPermissions");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_353 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ActiveFlag");
    private final static javax.xml.namespace.QName QName_0_363 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultBusinessUnit");
    private final static javax.xml.namespace.QName QName_0_351 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Password");
    private final static javax.xml.namespace.QName QName_0_680 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TimeZone");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_684 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">AccountUser>AssociatedBusinessUnits");
    private final static javax.xml.namespace.QName QName_0_364 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserAccess");
    private final static javax.xml.namespace.QName QName_0_361 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Unlock");
    private final static javax.xml.namespace.QName QName_0_350 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountUserID");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_683 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Roles");
    private final static javax.xml.namespace.QName QName_0_685 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">AccountUser>Roles");
    private final static javax.xml.namespace.QName QName_1_13 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "dateTime");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_359 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationEmailAddress");
    private final static javax.xml.namespace.QName QName_0_354 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ChallengePhrase");
    private final static javax.xml.namespace.QName QName_0_355 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ChallengeAnswer");
    private final static javax.xml.namespace.QName QName_0_357 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LastSuccessfulLogin");
    private final static javax.xml.namespace.QName QName_0_16 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserID");
    private final static javax.xml.namespace.QName QName_0_360 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsLocked");
    private final static javax.xml.namespace.QName QName_0_682 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AssociatedBusinessUnits");
    private final static javax.xml.namespace.QName QName_0_358 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAPIUser");
    private final static javax.xml.namespace.QName QName_0_681 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultBusinessUnitObject");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_352 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MustChangePassword");
    private final static javax.xml.namespace.QName QName_0_362 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BusinessUnit");
    private final static javax.xml.namespace.QName QName_0_340 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Delete");
}
