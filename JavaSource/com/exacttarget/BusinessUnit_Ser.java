/**
 * BusinessUnit_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class BusinessUnit_Ser extends com.exacttarget.Account_Ser {
    /**
     * Constructor
     */
    public BusinessUnit_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_134;
           context.qName2String(elemQName, true);
           elemQName = QName_0_708;
           context.qName2String(elemQName, true);
           elemQName = QName_0_709;
           context.qName2String(elemQName, true);
           elemQName = QName_0_710;
           context.qName2String(elemQName, true);
           elemQName = QName_0_711;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        BusinessUnit bean = (BusinessUnit) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_134;
          propValue = bean.getDescription();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_708;
          propValue = bean.getDefaultSendClassification();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_445,
              false,null,context);
          propQName = QName_0_709;
          propValue = bean.getDefaultHomePage();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_712,
              false,null,context);
          propQName = QName_0_710;
          propValue = bean.getSubscriberFilter();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_86,
              false,null,context);
          propQName = QName_0_711;
          propValue = bean.getMasterUnsubscribeBehavior();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_713,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_86 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FilterPart");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_711 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MasterUnsubscribeBehavior");
    private final static javax.xml.namespace.QName QName_0_445 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendClassification");
    private final static javax.xml.namespace.QName QName_0_712 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LandingPage");
    private final static javax.xml.namespace.QName QName_0_708 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultSendClassification");
    private final static javax.xml.namespace.QName QName_0_709 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DefaultHomePage");
    private final static javax.xml.namespace.QName QName_0_710 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberFilter");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_713 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UnsubscribeBehaviorEnum");
}
