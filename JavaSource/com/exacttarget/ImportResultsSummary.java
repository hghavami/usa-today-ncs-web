/**
 * ImportResultsSummary.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ImportResultsSummary  extends com.exacttarget.APIObject  {
    private java.lang.String importDefinitionCustomerKey;
    private java.lang.String startDate;
    private java.lang.String endDate;
    private java.lang.String destinationID;
    private java.lang.Integer numberSuccessful;
    private java.lang.Integer numberDuplicated;
    private java.lang.Integer numberErrors;
    private java.lang.Integer totalRows;
    private java.lang.String importType;
    private java.lang.String importStatus;
    private java.lang.Integer taskResultID;

    public ImportResultsSummary() {
    }

    public java.lang.String getImportDefinitionCustomerKey() {
        return importDefinitionCustomerKey;
    }

    public void setImportDefinitionCustomerKey(java.lang.String importDefinitionCustomerKey) {
        this.importDefinitionCustomerKey = importDefinitionCustomerKey;
    }

    public java.lang.String getStartDate() {
        return startDate;
    }

    public void setStartDate(java.lang.String startDate) {
        this.startDate = startDate;
    }

    public java.lang.String getEndDate() {
        return endDate;
    }

    public void setEndDate(java.lang.String endDate) {
        this.endDate = endDate;
    }

    public java.lang.String getDestinationID() {
        return destinationID;
    }

    public void setDestinationID(java.lang.String destinationID) {
        this.destinationID = destinationID;
    }

    public java.lang.Integer getNumberSuccessful() {
        return numberSuccessful;
    }

    public void setNumberSuccessful(java.lang.Integer numberSuccessful) {
        this.numberSuccessful = numberSuccessful;
    }

    public java.lang.Integer getNumberDuplicated() {
        return numberDuplicated;
    }

    public void setNumberDuplicated(java.lang.Integer numberDuplicated) {
        this.numberDuplicated = numberDuplicated;
    }

    public java.lang.Integer getNumberErrors() {
        return numberErrors;
    }

    public void setNumberErrors(java.lang.Integer numberErrors) {
        this.numberErrors = numberErrors;
    }

    public java.lang.Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(java.lang.Integer totalRows) {
        this.totalRows = totalRows;
    }

    public java.lang.String getImportType() {
        return importType;
    }

    public void setImportType(java.lang.String importType) {
        this.importType = importType;
    }

    public java.lang.String getImportStatus() {
        return importStatus;
    }

    public void setImportStatus(java.lang.String importStatus) {
        this.importStatus = importStatus;
    }

    public java.lang.Integer getTaskResultID() {
        return taskResultID;
    }

    public void setTaskResultID(java.lang.Integer taskResultID) {
        this.taskResultID = taskResultID;
    }

}
