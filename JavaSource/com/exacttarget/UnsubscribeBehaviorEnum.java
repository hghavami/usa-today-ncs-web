/**
 * UnsubscribeBehaviorEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UnsubscribeBehaviorEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected UnsubscribeBehaviorEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _ENTIRE_ENTERPRISE = "ENTIRE_ENTERPRISE";
    public static final java.lang.String _BUSINESS_UNIT_ONLY = "BUSINESS_UNIT_ONLY";
    public static final UnsubscribeBehaviorEnum ENTIRE_ENTERPRISE = new UnsubscribeBehaviorEnum(_ENTIRE_ENTERPRISE);
    public static final UnsubscribeBehaviorEnum BUSINESS_UNIT_ONLY = new UnsubscribeBehaviorEnum(_BUSINESS_UNIT_ONLY);
    public java.lang.String getValue() { return _value_;}
    public static UnsubscribeBehaviorEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        UnsubscribeBehaviorEnum enumeration = (UnsubscribeBehaviorEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static UnsubscribeBehaviorEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
