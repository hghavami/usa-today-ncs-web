/**
 * ImportDefinitionUpdateType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ImportDefinitionUpdateType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ImportDefinitionUpdateType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _AddAndUpdate = "AddAndUpdate";
    public static final java.lang.String _AddAndDoNotUpdate = "AddAndDoNotUpdate";
    public static final java.lang.String _UpdateButDoNotAdd = "UpdateButDoNotAdd";
    public static final java.lang.String _Merge = "Merge";
    public static final java.lang.String _Overwrite = "Overwrite";
    public static final java.lang.String _ColumnBased = "ColumnBased";
    public static final ImportDefinitionUpdateType AddAndUpdate = new ImportDefinitionUpdateType(_AddAndUpdate);
    public static final ImportDefinitionUpdateType AddAndDoNotUpdate = new ImportDefinitionUpdateType(_AddAndDoNotUpdate);
    public static final ImportDefinitionUpdateType UpdateButDoNotAdd = new ImportDefinitionUpdateType(_UpdateButDoNotAdd);
    public static final ImportDefinitionUpdateType Merge = new ImportDefinitionUpdateType(_Merge);
    public static final ImportDefinitionUpdateType Overwrite = new ImportDefinitionUpdateType(_Overwrite);
    public static final ImportDefinitionUpdateType ColumnBased = new ImportDefinitionUpdateType(_ColumnBased);
    public java.lang.String getValue() { return _value_;}
    public static ImportDefinitionUpdateType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ImportDefinitionUpdateType enumeration = (ImportDefinitionUpdateType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ImportDefinitionUpdateType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
