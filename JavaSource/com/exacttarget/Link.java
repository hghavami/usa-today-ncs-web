/**
 * Link.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Link  extends com.exacttarget.APIObject  {
    private java.util.Calendar lastClicked;
    private java.lang.String alias;
    private java.lang.Integer totalClicks;
    private java.lang.Integer uniqueClicks;
    private java.lang.String URL;
    private com.exacttarget.TrackingEvent[] subscribers;

    public Link() {
    }

    public java.util.Calendar getLastClicked() {
        return lastClicked;
    }

    public void setLastClicked(java.util.Calendar lastClicked) {
        this.lastClicked = lastClicked;
    }

    public java.lang.String getAlias() {
        return alias;
    }

    public void setAlias(java.lang.String alias) {
        this.alias = alias;
    }

    public java.lang.Integer getTotalClicks() {
        return totalClicks;
    }

    public void setTotalClicks(java.lang.Integer totalClicks) {
        this.totalClicks = totalClicks;
    }

    public java.lang.Integer getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(java.lang.Integer uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

    public java.lang.String getURL() {
        return URL;
    }

    public void setURL(java.lang.String URL) {
        this.URL = URL;
    }

    public com.exacttarget.TrackingEvent[] getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(com.exacttarget.TrackingEvent[] subscribers) {
        this.subscribers = subscribers;
    }

    public com.exacttarget.TrackingEvent getSubscribers(int i) {
        return this.subscribers[i];
    }

    public void setSubscribers(int i, com.exacttarget.TrackingEvent value) {
        this.subscribers[i] = value;
    }

}
