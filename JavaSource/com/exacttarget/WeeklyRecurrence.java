/**
 * WeeklyRecurrence.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class WeeklyRecurrence  extends com.exacttarget.Recurrence  {
    private com.exacttarget.WeeklyRecurrencePatternTypeEnum weeklyRecurrencePatternType;
    private java.lang.Integer weekInterval;
    private java.lang.Boolean sunday;
    private java.lang.Boolean monday;
    private java.lang.Boolean tuesday;
    private java.lang.Boolean wednesday;
    private java.lang.Boolean thursday;
    private java.lang.Boolean friday;
    private java.lang.Boolean saturday;

    public WeeklyRecurrence() {
    }

    public com.exacttarget.WeeklyRecurrencePatternTypeEnum getWeeklyRecurrencePatternType() {
        return weeklyRecurrencePatternType;
    }

    public void setWeeklyRecurrencePatternType(com.exacttarget.WeeklyRecurrencePatternTypeEnum weeklyRecurrencePatternType) {
        this.weeklyRecurrencePatternType = weeklyRecurrencePatternType;
    }

    public java.lang.Integer getWeekInterval() {
        return weekInterval;
    }

    public void setWeekInterval(java.lang.Integer weekInterval) {
        this.weekInterval = weekInterval;
    }

    public java.lang.Boolean getSunday() {
        return sunday;
    }

    public void setSunday(java.lang.Boolean sunday) {
        this.sunday = sunday;
    }

    public java.lang.Boolean getMonday() {
        return monday;
    }

    public void setMonday(java.lang.Boolean monday) {
        this.monday = monday;
    }

    public java.lang.Boolean getTuesday() {
        return tuesday;
    }

    public void setTuesday(java.lang.Boolean tuesday) {
        this.tuesday = tuesday;
    }

    public java.lang.Boolean getWednesday() {
        return wednesday;
    }

    public void setWednesday(java.lang.Boolean wednesday) {
        this.wednesday = wednesday;
    }

    public java.lang.Boolean getThursday() {
        return thursday;
    }

    public void setThursday(java.lang.Boolean thursday) {
        this.thursday = thursday;
    }

    public java.lang.Boolean getFriday() {
        return friday;
    }

    public void setFriday(java.lang.Boolean friday) {
        this.friday = friday;
    }

    public java.lang.Boolean getSaturday() {
        return saturday;
    }

    public void setSaturday(java.lang.Boolean saturday) {
        this.saturday = saturday;
    }

}
