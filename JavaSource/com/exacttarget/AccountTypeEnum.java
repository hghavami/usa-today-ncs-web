/**
 * AccountTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class AccountTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected AccountTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _None = "None";
    public static final java.lang.String _EXACTTARGET = "EXACTTARGET";
    public static final java.lang.String _PRO_CONNECT = "PRO_CONNECT";
    public static final java.lang.String _CHANNEL_CONNECT = "CHANNEL_CONNECT";
    public static final java.lang.String _CONNECT = "CONNECT";
    public static final java.lang.String _PRO_CONNECT_CLIENT = "PRO_CONNECT_CLIENT";
    public static final java.lang.String _LP_MEMBER = "LP_MEMBER";
    public static final java.lang.String _DOTO_MEMBER = "DOTO_MEMBER";
    public static final java.lang.String _ENTERPRISE_2 = "ENTERPRISE_2";
    public static final java.lang.String _BUSINESS_UNIT = "BUSINESS_UNIT";
    public static final AccountTypeEnum None = new AccountTypeEnum(_None);
    public static final AccountTypeEnum EXACTTARGET = new AccountTypeEnum(_EXACTTARGET);
    public static final AccountTypeEnum PRO_CONNECT = new AccountTypeEnum(_PRO_CONNECT);
    public static final AccountTypeEnum CHANNEL_CONNECT = new AccountTypeEnum(_CHANNEL_CONNECT);
    public static final AccountTypeEnum CONNECT = new AccountTypeEnum(_CONNECT);
    public static final AccountTypeEnum PRO_CONNECT_CLIENT = new AccountTypeEnum(_PRO_CONNECT_CLIENT);
    public static final AccountTypeEnum LP_MEMBER = new AccountTypeEnum(_LP_MEMBER);
    public static final AccountTypeEnum DOTO_MEMBER = new AccountTypeEnum(_DOTO_MEMBER);
    public static final AccountTypeEnum ENTERPRISE_2 = new AccountTypeEnum(_ENTERPRISE_2);
    public static final AccountTypeEnum BUSINESS_UNIT = new AccountTypeEnum(_BUSINESS_UNIT);
    public java.lang.String getValue() { return _value_;}
    public static AccountTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        AccountTypeEnum enumeration = (AccountTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static AccountTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
