/**
 * SubscriberResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberResult  {
    private com.exacttarget.Subscriber subscriber;
    private java.lang.String errorCode;
    private java.lang.String errorDescription;
    private java.lang.Integer ordinal;

    public SubscriberResult() {
    }

    public com.exacttarget.Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(com.exacttarget.Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public java.lang.String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }

    public java.lang.String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(java.lang.String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public java.lang.Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(java.lang.Integer ordinal) {
        this.ordinal = ordinal;
    }

}
