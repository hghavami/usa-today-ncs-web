/**
 * DataExtension_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class DataExtension_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public DataExtension_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new DataExtension();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((DataExtension)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((DataExtension)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_484) {
          ((DataExtension)value).setIsSendable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_485) {
          ((DataExtension)value).setIsTestable(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_489) {
          ((DataExtension)value).setDataRetentionPeriodLength(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_490) {
          ((DataExtension)value).setDataRetentionPeriodUnitOfMeasure(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_491) {
          ((DataExtension)value).setRowBasedRetention(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_492) {
          ((DataExtension)value).setResetRetentionPeriodOnImport(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_493) {
          ((DataExtension)value).setDeleteAtEndOfRetentionPeriod(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_494) {
          ((DataExtension)value).setRetainUntil(strValue);
          return true;}
        else if (qName==QName_0_182) {
          ((DataExtension)value).setCategoryID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseLong(strValue));
          return true;}
        else if (qName==QName_0_150) {
          ((DataExtension)value).setStatus(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_486) {
          ((DataExtension)value).setSendableDataExtensionField((com.exacttarget.DataExtensionField)objValue);
          return true;}
        else if (qName==QName_0_487) {
          ((DataExtension)value).setSendableSubscriberField((com.exacttarget.Attribute)objValue);
          return true;}
        else if (qName==QName_0_488) {
          ((DataExtension)value).setTemplate((com.exacttarget.DataExtensionTemplate)objValue);
          return true;}
        else if (qName==QName_0_495) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.DataExtensionField[] array = new com.exacttarget.DataExtensionField[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((DataExtension)value).setFields(array);
          } else { 
            ((DataExtension)value).setFields((com.exacttarget.DataExtensionField[])objValue);}
          return true;}
        else if (qName==QName_0_496) {
          ((DataExtension)value).setDataRetentionPeriod((com.exacttarget.DateTimeUnitOfMeasure)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_182 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CategoryID");
    private final static javax.xml.namespace.QName QName_0_489 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodLength");
    private final static javax.xml.namespace.QName QName_0_494 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RetainUntil");
    private final static javax.xml.namespace.QName QName_0_484 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsSendable");
    private final static javax.xml.namespace.QName QName_0_485 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsTestable");
    private final static javax.xml.namespace.QName QName_0_495 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Fields");
    private final static javax.xml.namespace.QName QName_0_491 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RowBasedRetention");
    private final static javax.xml.namespace.QName QName_0_490 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodUnitOfMeasure");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_492 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ResetRetentionPeriodOnImport");
    private final static javax.xml.namespace.QName QName_0_487 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendableSubscriberField");
    private final static javax.xml.namespace.QName QName_0_493 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeleteAtEndOfRetentionPeriod");
    private final static javax.xml.namespace.QName QName_0_486 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendableDataExtensionField");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_496 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriod");
    private final static javax.xml.namespace.QName QName_0_488 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Template");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
}
