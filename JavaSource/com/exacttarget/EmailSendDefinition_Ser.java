/**
 * EmailSendDefinition_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class EmailSendDefinition_Ser extends com.exacttarget.SendDefinition_Ser {
    /**
     * Constructor
     */
    public EmailSendDefinition_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_553;
           context.qName2String(elemQName, true);
           elemQName = QName_0_178;
           context.qName2String(elemQName, true);
           elemQName = QName_0_466;
           context.qName2String(elemQName, true);
           elemQName = QName_0_554;
           context.qName2String(elemQName, true);
           elemQName = QName_0_555;
           context.qName2String(elemQName, true);
           elemQName = QName_0_467;
           context.qName2String(elemQName, true);
           elemQName = QName_0_468;
           context.qName2String(elemQName, true);
           elemQName = QName_0_422;
           context.qName2String(elemQName, true);
           elemQName = QName_0_469;
           context.qName2String(elemQName, true);
           elemQName = QName_0_423;
           context.qName2String(elemQName, true);
           elemQName = QName_0_424;
           context.qName2String(elemQName, true);
           elemQName = QName_0_425;
           context.qName2String(elemQName, true);
           elemQName = QName_0_472;
           context.qName2String(elemQName, true);
           elemQName = QName_0_556;
           context.qName2String(elemQName, true);
           elemQName = QName_0_474;
           context.qName2String(elemQName, true);
           elemQName = QName_0_557;
           context.qName2String(elemQName, true);
           elemQName = QName_0_558;
           context.qName2String(elemQName, true);
           elemQName = QName_0_477;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        EmailSendDefinition bean = (EmailSendDefinition) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_553;
          {
            propValue = bean.getSendDefinitionList();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_553,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_178;
          propValue = bean.getEmail();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_178,
              false,null,context);
          propQName = QName_0_466;
          propValue = bean.getBccEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_554;
          propValue = bean.getAutoBccEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_555;
          propValue = bean.getTestEmailAddr();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_467;
          propValue = bean.getEmailSubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_468;
          propValue = bean.getDynamicEmailSubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_422;
          propValue = bean.getIsMultipart();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_469;
          propValue = bean.getIsWrapped();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_423;
          propValue = bean.getSendLimit();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_424;
          propValue = bean.getSendWindowOpen();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_433,
              false,null,context);
          propQName = QName_0_425;
          propValue = bean.getSendWindowClose();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_433,
              false,null,context);
          propQName = QName_0_472;
          propValue = bean.getSendWindowDelete();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_556;
          propValue = bean.getDeduplicateByEmail();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_474;
          propValue = bean.getExclusionFilter();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_557;
          propValue = bean.getTrackingUsers();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_559,
              false,null,context);
          propQName = QName_0_558;
          propValue = bean.getAdditional();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_477;
          propValue = bean.getCCEmail();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_557 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TrackingUsers");
    private final static javax.xml.namespace.QName QName_0_472 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowDelete");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
    private final static javax.xml.namespace.QName QName_0_469 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsWrapped");
    private final static javax.xml.namespace.QName QName_0_467 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailSubject");
    private final static javax.xml.namespace.QName QName_0_555 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TestEmailAddr");
    private final static javax.xml.namespace.QName QName_0_558 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Additional");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_468 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DynamicEmailSubject");
    private final static javax.xml.namespace.QName QName_0_474 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExclusionFilter");
    private final static javax.xml.namespace.QName QName_1_433 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "time");
    private final static javax.xml.namespace.QName QName_0_477 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CCEmail");
    private final static javax.xml.namespace.QName QName_0_554 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoBccEmail");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_422 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsMultipart");
    private final static javax.xml.namespace.QName QName_0_559 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">EmailSendDefinition>TrackingUsers");
    private final static javax.xml.namespace.QName QName_0_424 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowOpen");
    private final static javax.xml.namespace.QName QName_0_425 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendWindowClose");
    private final static javax.xml.namespace.QName QName_0_553 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendDefinitionList");
    private final static javax.xml.namespace.QName QName_0_466 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BccEmail");
    private final static javax.xml.namespace.QName QName_0_556 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeduplicateByEmail");
    private final static javax.xml.namespace.QName QName_0_423 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendLimit");
}
