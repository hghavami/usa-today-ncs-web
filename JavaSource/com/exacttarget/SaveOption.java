/**
 * SaveOption.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SaveOption  {
    private java.lang.String propertyName;
    private com.exacttarget.SaveAction saveAction;

    public SaveOption() {
    }

    public java.lang.String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(java.lang.String propertyName) {
        this.propertyName = propertyName;
    }

    public com.exacttarget.SaveAction getSaveAction() {
        return saveAction;
    }

    public void setSaveAction(com.exacttarget.SaveAction saveAction) {
        this.saveAction = saveAction;
    }

}
