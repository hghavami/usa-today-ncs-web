/**
 * AudienceItem_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class AudienceItem_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public AudienceItem_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_390;
           context.qName2String(elemQName, true);
           elemQName = QName_0_501;
           context.qName2String(elemQName, true);
           elemQName = QName_0_502;
           context.qName2String(elemQName, true);
           elemQName = QName_0_503;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        AudienceItem bean = (AudienceItem) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_390;
          propValue = bean.getList();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_390,
              false,null,context);
          propQName = QName_0_501;
          propValue = bean.getSendDefinitionListType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_504,
              false,null,context);
          propQName = QName_0_502;
          propValue = bean.getCustomObjectID();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_503;
          propValue = bean.getDataSourceTypeID();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_505,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_505 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataSourceTypeEnum");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_504 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendDefinitionListTypeEnum");
    private final static javax.xml.namespace.QName QName_0_503 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataSourceTypeID");
    private final static javax.xml.namespace.QName QName_0_501 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendDefinitionListType");
    private final static javax.xml.namespace.QName QName_0_502 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CustomObjectID");
}
