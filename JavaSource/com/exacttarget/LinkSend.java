/**
 * LinkSend.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class LinkSend  extends com.exacttarget.APIObject  {
    private java.lang.Integer sendID;
    private com.exacttarget.Link link;

    public LinkSend() {
    }

    public java.lang.Integer getSendID() {
        return sendID;
    }

    public void setSendID(java.lang.Integer sendID) {
        this.sendID = sendID;
    }

    public com.exacttarget.Link getLink() {
        return link;
    }

    public void setLink(com.exacttarget.Link link) {
        this.link = link;
    }

}
