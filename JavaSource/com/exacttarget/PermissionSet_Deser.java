/**
 * PermissionSet_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PermissionSet_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public PermissionSet_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new PermissionSet();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_20) {
          ((PermissionSet)value).setName(strValue);
          return true;}
        else if (qName==QName_0_134) {
          ((PermissionSet)value).setDescription(strValue);
          return true;}
        else if (qName==QName_0_714) {
          ((PermissionSet)value).setIsAllowed(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_715) {
          ((PermissionSet)value).setIsDenied(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_704) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.PermissionSet[] array = new com.exacttarget.PermissionSet[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((PermissionSet)value).setPermissionSets(array);
          } else { 
            ((PermissionSet)value).setPermissionSets((com.exacttarget.PermissionSet[])objValue);}
          return true;}
        else if (qName==QName_0_705) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.Permission[] array = new com.exacttarget.Permission[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((PermissionSet)value).setPermissions(array);
          } else { 
            ((PermissionSet)value).setPermissions((com.exacttarget.Permission[])objValue);}
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_704 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PermissionSets");
    private final static javax.xml.namespace.QName QName_0_714 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsAllowed");
    private final static javax.xml.namespace.QName QName_0_705 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Permissions");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_715 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsDenied");
}
