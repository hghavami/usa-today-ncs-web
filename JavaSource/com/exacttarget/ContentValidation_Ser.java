/**
 * ContentValidation_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ContentValidation_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public ContentValidation_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_177;
           context.qName2String(elemQName, true);
           elemQName = QName_0_178;
           context.qName2String(elemQName, true);
           elemQName = QName_0_179;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        ContentValidation bean = (ContentValidation) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_177;
          propValue = bean.getValidationAction();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_177,
              false,null,context);
          propQName = QName_0_178;
          propValue = bean.getEmail();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_178,
              false,null,context);
          propQName = QName_0_179;
          propValue = bean.getSubscribers();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_180,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_177 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ValidationAction");
    private final static javax.xml.namespace.QName QName_0_179 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Subscribers");
    private final static javax.xml.namespace.QName QName_0_180 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">ContentValidation>Subscribers");
    private final static javax.xml.namespace.QName QName_0_178 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Email");
}
