/**
 * ImportDefinition_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ImportDefinition_Ser extends com.exacttarget.InteractionDefinition_Ser {
    /**
     * Constructor
     */
    public ImportDefinition_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_593;
           context.qName2String(elemQName, true);
           elemQName = QName_0_594;
           context.qName2String(elemQName, true);
           elemQName = QName_0_595;
           context.qName2String(elemQName, true);
           elemQName = QName_0_596;
           context.qName2String(elemQName, true);
           elemQName = QName_0_597;
           context.qName2String(elemQName, true);
           elemQName = QName_0_598;
           context.qName2String(elemQName, true);
           elemQName = QName_0_599;
           context.qName2String(elemQName, true);
           elemQName = QName_0_600;
           context.qName2String(elemQName, true);
           elemQName = QName_0_601;
           context.qName2String(elemQName, true);
           elemQName = QName_0_602;
           context.qName2String(elemQName, true);
           elemQName = QName_0_603;
           context.qName2String(elemQName, true);
           elemQName = QName_0_604;
           context.qName2String(elemQName, true);
           elemQName = QName_0_605;
           context.qName2String(elemQName, true);
           elemQName = QName_0_606;
           context.qName2String(elemQName, true);
           elemQName = QName_0_607;
           context.qName2String(elemQName, true);
           elemQName = QName_0_755;
           context.qName2String(elemQName, true);
           elemQName = QName_0_756;
           context.qName2String(elemQName, true);
           elemQName = QName_0_757;
           context.qName2String(elemQName, true);
           elemQName = QName_0_758;
           context.qName2String(elemQName, true);
           elemQName = QName_0_759;
           context.qName2String(elemQName, true);
           elemQName = QName_0_760;
           context.qName2String(elemQName, true);
           elemQName = QName_0_761;
           context.qName2String(elemQName, true);
           elemQName = QName_0_79;
           context.qName2String(elemQName, true);
           elemQName = QName_0_762;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        ImportDefinition bean = (ImportDefinition) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_593;
          propValue = bean.getAllowErrors();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_594;
          propValue = bean.getDestinationObject();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_32,
              false,null,context);
          propQName = QName_0_595;
          propValue = bean.getFieldMappingType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_608,
              false,null,context);
          propQName = QName_0_596;
          propValue = bean.getFieldMaps();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_609,
              false,null,context);
          propQName = QName_0_597;
          propValue = bean.getFileSpec();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_598;
          propValue = bean.getFileType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_598,
              false,null,context);
          propQName = QName_0_599;
          propValue = bean.getNotification();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_50,
              false,null,context);
          propQName = QName_0_600;
          propValue = bean.getRetrieveFileTransferLocation();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_610,
              false,null,context);
          propQName = QName_0_601;
          propValue = bean.getSubscriberImportType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_611,
              false,null,context);
          propQName = QName_0_602;
          propValue = bean.getUpdateType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_612,
              false,null,context);
          propQName = QName_0_603;
          propValue = bean.getMaxFileAge();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_604;
          propValue = bean.getMaxFileAgeScheduleOffset();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_605;
          propValue = bean.getMaxImportFrequency();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_606;
          propValue = bean.getDelimiter();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_607;
          propValue = bean.getHeaderLines();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_755;
          propValue = bean.getAutoGenerateDestination();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_763,
              false,null,context);
          propQName = QName_0_756;
          propValue = bean.getControlColumn();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_757;
          propValue = bean.getControlColumnDefaultAction();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_753,
              false,null,context);
          propQName = QName_0_758;
          propValue = bean.getControlColumnActions();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_764,
              false,null,context);
          propQName = QName_0_759;
          propValue = bean.getEndOfLineRepresentation();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_760;
          propValue = bean.getNullRepresentation();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_761;
          propValue = bean.getStandardQuotedStrings();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_79;
          propValue = bean.getFilter();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_762;
          propValue = bean.getDateFormattingLocale();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_679,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_753 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionColumnBasedActionType");
    private final static javax.xml.namespace.QName QName_0_679 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Locale");
    private final static javax.xml.namespace.QName QName_0_602 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UpdateType");
    private final static javax.xml.namespace.QName QName_0_758 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ControlColumnActions");
    private final static javax.xml.namespace.QName QName_0_764 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">ImportDefinition>ControlColumnActions");
    private final static javax.xml.namespace.QName QName_0_600 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RetrieveFileTransferLocation");
    private final static javax.xml.namespace.QName QName_0_607 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HeaderLines");
    private final static javax.xml.namespace.QName QName_0_760 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NullRepresentation");
    private final static javax.xml.namespace.QName QName_0_756 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ControlColumn");
    private final static javax.xml.namespace.QName QName_0_609 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">ImportDefinition>FieldMaps");
    private final static javax.xml.namespace.QName QName_0_32 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "APIObject");
    private final static javax.xml.namespace.QName QName_0_50 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AsyncResponse");
    private final static javax.xml.namespace.QName QName_0_593 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AllowErrors");
    private final static javax.xml.namespace.QName QName_0_598 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileType");
    private final static javax.xml.namespace.QName QName_0_606 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Delimiter");
    private final static javax.xml.namespace.QName QName_0_79 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Filter");
    private final static javax.xml.namespace.QName QName_0_601 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriberImportType");
    private final static javax.xml.namespace.QName QName_0_757 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ControlColumnDefaultAction");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_605 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxImportFrequency");
    private final static javax.xml.namespace.QName QName_0_755 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoGenerateDestination");
    private final static javax.xml.namespace.QName QName_0_759 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EndOfLineRepresentation");
    private final static javax.xml.namespace.QName QName_0_762 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DateFormattingLocale");
    private final static javax.xml.namespace.QName QName_0_761 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "StandardQuotedStrings");
    private final static javax.xml.namespace.QName QName_0_594 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DestinationObject");
    private final static javax.xml.namespace.QName QName_0_612 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionUpdateType");
    private final static javax.xml.namespace.QName QName_0_595 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FieldMappingType");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_597 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileSpec");
    private final static javax.xml.namespace.QName QName_0_599 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Notification");
    private final static javax.xml.namespace.QName QName_0_604 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxFileAgeScheduleOffset");
    private final static javax.xml.namespace.QName QName_0_611 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionSubscriberImportType");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_603 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MaxFileAge");
    private final static javax.xml.namespace.QName QName_0_596 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FieldMaps");
    private final static javax.xml.namespace.QName QName_0_763 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionAutoGenerateDestination");
    private final static javax.xml.namespace.QName QName_0_610 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FileTransferLocation");
    private final static javax.xml.namespace.QName QName_0_608 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionFieldMappingType");
}
