/**
 * Query.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Query  {
    private com.exacttarget.QueryObject object;
    private com.exacttarget.FilterPart filter;

    public Query() {
    }

    public com.exacttarget.QueryObject getObject() {
        return object;
    }

    public void setObject(com.exacttarget.QueryObject object) {
        this.object = object;
    }

    public com.exacttarget.FilterPart getFilter() {
        return filter;
    }

    public void setFilter(com.exacttarget.FilterPart filter) {
        this.filter = filter;
    }

}
