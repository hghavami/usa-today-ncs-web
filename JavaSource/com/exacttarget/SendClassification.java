/**
 * SendClassification.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SendClassification  extends com.exacttarget.APIObject  {
    private com.exacttarget.SendClassificationTypeEnum sendClassificationType;
    private java.lang.String name;
    private java.lang.String description;
    private com.exacttarget.SenderProfile senderProfile;
    private com.exacttarget.DeliveryProfile deliveryProfile;
    private java.lang.Boolean honorPublicationListOptOutsForTransactionalSends;

    public SendClassification() {
    }

    public com.exacttarget.SendClassificationTypeEnum getSendClassificationType() {
        return sendClassificationType;
    }

    public void setSendClassificationType(com.exacttarget.SendClassificationTypeEnum sendClassificationType) {
        this.sendClassificationType = sendClassificationType;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public com.exacttarget.SenderProfile getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(com.exacttarget.SenderProfile senderProfile) {
        this.senderProfile = senderProfile;
    }

    public com.exacttarget.DeliveryProfile getDeliveryProfile() {
        return deliveryProfile;
    }

    public void setDeliveryProfile(com.exacttarget.DeliveryProfile deliveryProfile) {
        this.deliveryProfile = deliveryProfile;
    }

    public java.lang.Boolean getHonorPublicationListOptOutsForTransactionalSends() {
        return honorPublicationListOptOutsForTransactionalSends;
    }

    public void setHonorPublicationListOptOutsForTransactionalSends(java.lang.Boolean honorPublicationListOptOutsForTransactionalSends) {
        this.honorPublicationListOptOutsForTransactionalSends = honorPublicationListOptOutsForTransactionalSends;
    }

}
