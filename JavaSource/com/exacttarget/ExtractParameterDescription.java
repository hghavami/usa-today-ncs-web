/**
 * ExtractParameterDescription.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ExtractParameterDescription  extends com.exacttarget.ParameterDescription  {
    private java.lang.String name;
    private com.exacttarget.ExtractParameterDataType dataType;
    private java.lang.String defaultValue;
    private boolean isOptional;
    private java.lang.String dropDownList;

    public ExtractParameterDescription() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public com.exacttarget.ExtractParameterDataType getDataType() {
        return dataType;
    }

    public void setDataType(com.exacttarget.ExtractParameterDataType dataType) {
        this.dataType = dataType;
    }

    public java.lang.String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(java.lang.String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isIsOptional() {
        return isOptional;
    }

    public void setIsOptional(boolean isOptional) {
        this.isOptional = isOptional;
    }

    public java.lang.String getDropDownList() {
        return dropDownList;
    }

    public void setDropDownList(java.lang.String dropDownList) {
        this.dropDownList = dropDownList;
    }

}
