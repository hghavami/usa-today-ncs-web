/**
 * YearlyRecurrencePatternTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class YearlyRecurrencePatternTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected YearlyRecurrencePatternTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _ByDay = "ByDay";
    public static final java.lang.String _ByWeek = "ByWeek";
    public static final java.lang.String _ByMonth = "ByMonth";
    public static final YearlyRecurrencePatternTypeEnum ByDay = new YearlyRecurrencePatternTypeEnum(_ByDay);
    public static final YearlyRecurrencePatternTypeEnum ByWeek = new YearlyRecurrencePatternTypeEnum(_ByWeek);
    public static final YearlyRecurrencePatternTypeEnum ByMonth = new YearlyRecurrencePatternTypeEnum(_ByMonth);
    public java.lang.String getValue() { return _value_;}
    public static YearlyRecurrencePatternTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        YearlyRecurrencePatternTypeEnum enumeration = (YearlyRecurrencePatternTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static YearlyRecurrencePatternTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
