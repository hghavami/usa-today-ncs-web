/**
 * QueryDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class QueryDefinition  extends com.exacttarget.InteractionDefinition  {
    private java.lang.String queryText;
    private java.lang.String targetType;
    private com.exacttarget.InteractionBaseObject dataExtensionTarget;
    private java.lang.String targetUpdateType;
    private java.lang.String fileSpec;
    private java.lang.String fileType;
    private java.lang.String status;

    public QueryDefinition() {
    }

    public java.lang.String getQueryText() {
        return queryText;
    }

    public void setQueryText(java.lang.String queryText) {
        this.queryText = queryText;
    }

    public java.lang.String getTargetType() {
        return targetType;
    }

    public void setTargetType(java.lang.String targetType) {
        this.targetType = targetType;
    }

    public com.exacttarget.InteractionBaseObject getDataExtensionTarget() {
        return dataExtensionTarget;
    }

    public void setDataExtensionTarget(com.exacttarget.InteractionBaseObject dataExtensionTarget) {
        this.dataExtensionTarget = dataExtensionTarget;
    }

    public java.lang.String getTargetUpdateType() {
        return targetUpdateType;
    }

    public void setTargetUpdateType(java.lang.String targetUpdateType) {
        this.targetUpdateType = targetUpdateType;
    }

    public java.lang.String getFileSpec() {
        return fileSpec;
    }

    public void setFileSpec(java.lang.String fileSpec) {
        this.fileSpec = fileSpec;
    }

    public java.lang.String getFileType() {
        return fileType;
    }

    public void setFileType(java.lang.String fileType) {
        this.fileType = fileType;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

}
