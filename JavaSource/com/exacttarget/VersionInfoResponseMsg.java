/**
 * VersionInfoResponseMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class VersionInfoResponseMsg  {
    private com.exacttarget.VersionInfoResponse versionInfo;
    private java.lang.String requestID;

    public VersionInfoResponseMsg() {
    }

    public com.exacttarget.VersionInfoResponse getVersionInfo() {
        return versionInfo;
    }

    public void setVersionInfo(com.exacttarget.VersionInfoResponse versionInfo) {
        this.versionInfo = versionInfo;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

}
