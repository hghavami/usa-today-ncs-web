/**
 * Publication.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Publication  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.Boolean isActive;
    private com.exacttarget.SendClassification sendClassification;
    private com.exacttarget.Subscriber[] subscribers;
    private java.lang.Integer category;

    public Publication() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public com.exacttarget.SendClassification getSendClassification() {
        return sendClassification;
    }

    public void setSendClassification(com.exacttarget.SendClassification sendClassification) {
        this.sendClassification = sendClassification;
    }

    public com.exacttarget.Subscriber[] getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(com.exacttarget.Subscriber[] subscribers) {
        this.subscribers = subscribers;
    }

    public java.lang.Integer getCategory() {
        return category;
    }

    public void setCategory(java.lang.Integer category) {
        this.category = category;
    }

}
