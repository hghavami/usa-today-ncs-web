/**
 * PlatformApplicationPackage.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PlatformApplicationPackage  extends com.exacttarget.APIObject  {
    private com.exacttarget.ResourceSpecification resourceSpecification;
    private com.exacttarget.PublicKeyManagement signingKey;
    private java.lang.Boolean isUpgrade;
    private java.lang.String developerVersion;

    public PlatformApplicationPackage() {
    }

    public com.exacttarget.ResourceSpecification getResourceSpecification() {
        return resourceSpecification;
    }

    public void setResourceSpecification(com.exacttarget.ResourceSpecification resourceSpecification) {
        this.resourceSpecification = resourceSpecification;
    }

    public com.exacttarget.PublicKeyManagement getSigningKey() {
        return signingKey;
    }

    public void setSigningKey(com.exacttarget.PublicKeyManagement signingKey) {
        this.signingKey = signingKey;
    }

    public java.lang.Boolean getIsUpgrade() {
        return isUpgrade;
    }

    public void setIsUpgrade(java.lang.Boolean isUpgrade) {
        this.isUpgrade = isUpgrade;
    }

    public java.lang.String getDeveloperVersion() {
        return developerVersion;
    }

    public void setDeveloperVersion(java.lang.String developerVersion) {
        this.developerVersion = developerVersion;
    }

}
