/**
 * QueryRequest_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class QueryRequest_Helper {
    // Type metadata
    private static final com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(QueryRequest.class);

    static {
        typeDesc.setOption("buildNum","cf131037.05");
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("clientIDs");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ClientIDs"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ClientID"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("query");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Query"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Query"));
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("respondTo");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RespondTo"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "AsyncResponse"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("partnerProperties");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerProperties"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "APIProperty"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("continueRequest");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ContinueRequest"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("queryAllAccounts");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "QueryAllAccounts"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("retrieveAllSinceLastBatch");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "RetrieveAllSinceLastBatch"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new QueryRequest_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new QueryRequest_Deser(
            javaType, xmlType, typeDesc);
    };

}
