/**
 * YearlyRecurrence_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class YearlyRecurrence_Ser extends com.exacttarget.Recurrence_Ser {
    /**
     * Constructor
     */
    public YearlyRecurrence_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_267;
           context.qName2String(elemQName, true);
           elemQName = QName_0_261;
           context.qName2String(elemQName, true);
           elemQName = QName_0_262;
           context.qName2String(elemQName, true);
           elemQName = QName_0_268;
           context.qName2String(elemQName, true);
           elemQName = QName_0_263;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        YearlyRecurrence bean = (YearlyRecurrence) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_267;
          propValue = bean.getYearlyRecurrencePatternType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_269,
              false,null,context);
          propQName = QName_0_261;
          propValue = bean.getScheduledDay();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_262;
          propValue = bean.getScheduledWeek();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_265,
              false,null,context);
          propQName = QName_0_268;
          propValue = bean.getScheduledMonth();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_270,
              false,null,context);
          propQName = QName_0_263;
          propValue = bean.getScheduledDayOfWeek();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_266,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_262 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledWeek");
    private final static javax.xml.namespace.QName QName_0_269 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "YearlyRecurrencePatternTypeEnum");
    private final static javax.xml.namespace.QName QName_0_261 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledDay");
    private final static javax.xml.namespace.QName QName_0_263 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledDayOfWeek");
    private final static javax.xml.namespace.QName QName_0_266 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DayOfWeekEnum");
    private final static javax.xml.namespace.QName QName_0_270 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MonthOfYearEnum");
    private final static javax.xml.namespace.QName QName_0_265 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "WeekOfMonthEnum");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_267 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "YearlyRecurrencePatternType");
    private final static javax.xml.namespace.QName QName_0_268 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ScheduledMonth");
}
