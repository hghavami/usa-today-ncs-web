/**
 * WeeklyRecurrence_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class WeeklyRecurrence_Helper {
    // Type metadata
    private static final com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(WeeklyRecurrence.class);

    static {
        typeDesc.setOption("buildNum","cf131037.05");
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("weeklyRecurrencePatternType");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeeklyRecurrencePatternType"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeeklyRecurrencePatternTypeEnum"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("weekInterval");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "WeekInterval"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sunday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Sunday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("monday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Monday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("tuesday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Tuesday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("wednesday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Wednesday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("thursday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Thursday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("friday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Friday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("saturday");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Saturday"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new WeeklyRecurrence_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new WeeklyRecurrence_Deser(
            javaType, xmlType, typeDesc);
    };

}
