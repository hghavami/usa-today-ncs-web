/**
 * ImportDefinitionColumnBasedAction.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ImportDefinitionColumnBasedAction  {
    private java.lang.String value;
    private com.exacttarget.ImportDefinitionColumnBasedActionType action;

    public ImportDefinitionColumnBasedAction() {
    }

    public java.lang.String getValue() {
        return value;
    }

    public void setValue(java.lang.String value) {
        this.value = value;
    }

    public com.exacttarget.ImportDefinitionColumnBasedActionType getAction() {
        return action;
    }

    public void setAction(com.exacttarget.ImportDefinitionColumnBasedActionType action) {
        this.action = action;
    }

}
