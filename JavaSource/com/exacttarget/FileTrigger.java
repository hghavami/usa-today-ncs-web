/**
 * FileTrigger.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class FileTrigger  extends com.exacttarget.APIObject  {
    private java.lang.String externalReference;
    private java.lang.String type;
    private java.lang.String status;
    private java.lang.String statusMessage;
    private java.lang.String requestParameterDetail;
    private java.lang.String responseControlManifest;
    private java.lang.String fileName;
    private java.lang.String description;
    private java.lang.String name;
    private java.util.Calendar lastPullDate;
    private java.util.Calendar scheduledDate;
    private java.lang.Boolean isActive;
    private java.lang.String fileTriggerProgramID;

    public FileTrigger() {
    }

    public java.lang.String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(java.lang.String externalReference) {
        this.externalReference = externalReference;
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(java.lang.String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public java.lang.String getRequestParameterDetail() {
        return requestParameterDetail;
    }

    public void setRequestParameterDetail(java.lang.String requestParameterDetail) {
        this.requestParameterDetail = requestParameterDetail;
    }

    public java.lang.String getResponseControlManifest() {
        return responseControlManifest;
    }

    public void setResponseControlManifest(java.lang.String responseControlManifest) {
        this.responseControlManifest = responseControlManifest;
    }

    public java.lang.String getFileName() {
        return fileName;
    }

    public void setFileName(java.lang.String fileName) {
        this.fileName = fileName;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.util.Calendar getLastPullDate() {
        return lastPullDate;
    }

    public void setLastPullDate(java.util.Calendar lastPullDate) {
        this.lastPullDate = lastPullDate;
    }

    public java.util.Calendar getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(java.util.Calendar scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.String getFileTriggerProgramID() {
        return fileTriggerProgramID;
    }

    public void setFileTriggerProgramID(java.lang.String fileTriggerProgramID) {
        this.fileTriggerProgramID = fileTriggerProgramID;
    }

}
