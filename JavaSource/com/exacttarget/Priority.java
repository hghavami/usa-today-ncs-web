/**
 * Priority.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Priority  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected Priority(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Low = "Low";
    public static final java.lang.String _Medium = "Medium";
    public static final java.lang.String _High = "High";
    public static final Priority Low = new Priority(_Low);
    public static final Priority Medium = new Priority(_Medium);
    public static final Priority High = new Priority(_High);
    public java.lang.String getValue() { return _value_;}
    public static Priority fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        Priority enumeration = (Priority)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static Priority fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
