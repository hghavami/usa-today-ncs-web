/**
 * MessagingConfiguration_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class MessagingConfiguration_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public MessagingConfiguration_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new MessagingConfiguration();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_570) {
          ((MessagingConfiguration)value).setCode(strValue);
          return true;}
        else if (qName==QName_0_187) {
          ((MessagingConfiguration)value).setIsActive(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseboolean(strValue));
          return true;}
        else if (qName==QName_0_572) {
          ((MessagingConfiguration)value).setUrl(strValue);
          return true;}
        else if (qName==QName_0_573) {
          ((MessagingConfiguration)value).setUserName(strValue);
          return true;}
        else if (qName==QName_0_351) {
          ((MessagingConfiguration)value).setPassword(strValue);
          return true;}
        else if (qName==QName_0_574) {
          ((MessagingConfiguration)value).setProfileID(strValue);
          return true;}
        else if (qName==QName_0_575) {
          ((MessagingConfiguration)value).setCallbackUrl(strValue);
          return true;}
        else if (qName==QName_0_576) {
          ((MessagingConfiguration)value).setMediaTypes(strValue);
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_571) {
          ((MessagingConfiguration)value).setMessagingVendorKind((com.exacttarget.MessagingVendorKind)objValue);
          return true;}
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_574 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ProfileID");
    private final static javax.xml.namespace.QName QName_0_187 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsActive");
    private final static javax.xml.namespace.QName QName_0_576 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MediaTypes");
    private final static javax.xml.namespace.QName QName_0_575 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CallbackUrl");
    private final static javax.xml.namespace.QName QName_0_573 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserName");
    private final static javax.xml.namespace.QName QName_0_570 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Code");
    private final static javax.xml.namespace.QName QName_0_571 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MessagingVendorKind");
    private final static javax.xml.namespace.QName QName_0_351 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Password");
    private final static javax.xml.namespace.QName QName_0_572 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Url");
}
