/**
 * UpdateResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class UpdateResult  extends com.exacttarget.Result  {
    private com.exacttarget.APIObject object;
    private com.exacttarget.UpdateResult[] updateResults;
    private java.lang.String parentPropertyName;

    public UpdateResult() {
    }

    public com.exacttarget.APIObject getObject() {
        return object;
    }

    public void setObject(com.exacttarget.APIObject object) {
        this.object = object;
    }

    public com.exacttarget.UpdateResult[] getUpdateResults() {
        return updateResults;
    }

    public void setUpdateResults(com.exacttarget.UpdateResult[] updateResults) {
        this.updateResults = updateResults;
    }

    public com.exacttarget.UpdateResult getUpdateResults(int i) {
        return this.updateResults[i];
    }

    public void setUpdateResults(int i, com.exacttarget.UpdateResult value) {
        this.updateResults[i] = value;
    }

    public java.lang.String getParentPropertyName() {
        return parentPropertyName;
    }

    public void setParentPropertyName(java.lang.String parentPropertyName) {
        this.parentPropertyName = parentPropertyName;
    }

}
