/**
 * DeleteResponse.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DeleteResponse  {
    private com.exacttarget.DeleteResult[] results;
    private java.lang.String requestID;
    private java.lang.String overallStatus;

    public DeleteResponse() {
    }

    public com.exacttarget.DeleteResult[] getResults() {
        return results;
    }

    public void setResults(com.exacttarget.DeleteResult[] results) {
        this.results = results;
    }

    public com.exacttarget.DeleteResult getResults(int i) {
        return this.results[i];
    }

    public void setResults(int i, com.exacttarget.DeleteResult value) {
        this.results[i] = value;
    }

    public java.lang.String getRequestID() {
        return requestID;
    }

    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }

    public java.lang.String getOverallStatus() {
        return overallStatus;
    }

    public void setOverallStatus(java.lang.String overallStatus) {
        this.overallStatus = overallStatus;
    }

}
