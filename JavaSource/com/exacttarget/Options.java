/**
 * Options.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public abstract class Options  {
    private com.exacttarget.ClientID client;
    private com.exacttarget.AsyncResponse[] sendResponseTo;
    private com.exacttarget.SaveOption[] saveOptions;
    private java.lang.Byte priority;
    private java.lang.String conversationID;
    private java.lang.Integer sequenceCode;
    private java.lang.Integer callsInConversation;
    private java.util.Calendar scheduledTime;
    private com.exacttarget.RequestType requestType;
    private com.exacttarget.Priority queuePriority;

    public Options() {
    }

    public com.exacttarget.ClientID getClient() {
        return client;
    }

    public void setClient(com.exacttarget.ClientID client) {
        this.client = client;
    }

    public com.exacttarget.AsyncResponse[] getSendResponseTo() {
        return sendResponseTo;
    }

    public void setSendResponseTo(com.exacttarget.AsyncResponse[] sendResponseTo) {
        this.sendResponseTo = sendResponseTo;
    }

    public com.exacttarget.AsyncResponse getSendResponseTo(int i) {
        return this.sendResponseTo[i];
    }

    public void setSendResponseTo(int i, com.exacttarget.AsyncResponse value) {
        this.sendResponseTo[i] = value;
    }

    public com.exacttarget.SaveOption[] getSaveOptions() {
        return saveOptions;
    }

    public void setSaveOptions(com.exacttarget.SaveOption[] saveOptions) {
        this.saveOptions = saveOptions;
    }

    public java.lang.Byte getPriority() {
        return priority;
    }

    public void setPriority(java.lang.Byte priority) {
        this.priority = priority;
    }

    public java.lang.String getConversationID() {
        return conversationID;
    }

    public void setConversationID(java.lang.String conversationID) {
        this.conversationID = conversationID;
    }

    public java.lang.Integer getSequenceCode() {
        return sequenceCode;
    }

    public void setSequenceCode(java.lang.Integer sequenceCode) {
        this.sequenceCode = sequenceCode;
    }

    public java.lang.Integer getCallsInConversation() {
        return callsInConversation;
    }

    public void setCallsInConversation(java.lang.Integer callsInConversation) {
        this.callsInConversation = callsInConversation;
    }

    public java.util.Calendar getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(java.util.Calendar scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public com.exacttarget.RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(com.exacttarget.RequestType requestType) {
        this.requestType = requestType;
    }

    public com.exacttarget.Priority getQueuePriority() {
        return queuePriority;
    }

    public void setQueuePriority(com.exacttarget.Priority queuePriority) {
        this.queuePriority = queuePriority;
    }

}
