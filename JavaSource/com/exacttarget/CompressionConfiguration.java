/**
 * CompressionConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class CompressionConfiguration  {
    private com.exacttarget.CompressionType type;
    private com.exacttarget.CompressionEncoding encoding;

    public CompressionConfiguration() {
    }

    public com.exacttarget.CompressionType getType() {
        return type;
    }

    public void setType(com.exacttarget.CompressionType type) {
        this.type = type;
    }

    public com.exacttarget.CompressionEncoding getEncoding() {
        return encoding;
    }

    public void setEncoding(com.exacttarget.CompressionEncoding encoding) {
        this.encoding = encoding;
    }

}
