/**
 * ImportResultsSummary_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ImportResultsSummary_Deser extends com.exacttarget.APIObject_Deser {
    /**
     * Constructor
     */
    public ImportResultsSummary_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ImportResultsSummary();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_613) {
          ((ImportResultsSummary)value).setImportDefinitionCustomerKey(strValue);
          return true;}
        else if (qName==QName_0_614) {
          ((ImportResultsSummary)value).setStartDate(strValue);
          return true;}
        else if (qName==QName_0_328) {
          ((ImportResultsSummary)value).setEndDate(strValue);
          return true;}
        else if (qName==QName_0_615) {
          ((ImportResultsSummary)value).setDestinationID(strValue);
          return true;}
        else if (qName==QName_0_616) {
          ((ImportResultsSummary)value).setNumberSuccessful(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_617) {
          ((ImportResultsSummary)value).setNumberDuplicated(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_618) {
          ((ImportResultsSummary)value).setNumberErrors(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_619) {
          ((ImportResultsSummary)value).setTotalRows(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_620) {
          ((ImportResultsSummary)value).setImportType(strValue);
          return true;}
        else if (qName==QName_0_621) {
          ((ImportResultsSummary)value).setImportStatus(strValue);
          return true;}
        else if (qName==QName_0_622) {
          ((ImportResultsSummary)value).setTaskResultID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        return super.tryElementSetFromString(qName, strValue);
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return super.tryAttributeSetFromString(qName, strValue);
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return super.tryElementSetFromObject(qName, objValue);
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return super.tryElementSetFromList(qName, listValue);
    }
    private final static javax.xml.namespace.QName QName_0_616 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberSuccessful");
    private final static javax.xml.namespace.QName QName_0_622 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TaskResultID");
    private final static javax.xml.namespace.QName QName_0_618 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberErrors");
    private final static javax.xml.namespace.QName QName_0_617 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberDuplicated");
    private final static javax.xml.namespace.QName QName_0_613 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportDefinitionCustomerKey");
    private final static javax.xml.namespace.QName QName_0_615 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DestinationID");
    private final static javax.xml.namespace.QName QName_0_620 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportType");
    private final static javax.xml.namespace.QName QName_0_621 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ImportStatus");
    private final static javax.xml.namespace.QName QName_0_614 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "StartDate");
    private final static javax.xml.namespace.QName QName_0_328 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EndDate");
    private final static javax.xml.namespace.QName QName_0_619 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TotalRows");
}
