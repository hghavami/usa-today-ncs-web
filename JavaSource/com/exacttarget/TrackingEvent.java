/**
 * TrackingEvent.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TrackingEvent  extends com.exacttarget.APIObject  {
    private java.lang.Integer sendID;
    private java.lang.String subscriberKey;
    private java.util.Calendar eventDate;
    private com.exacttarget.EventType eventType;
    private java.lang.String triggeredSendDefinitionObjectID;
    private java.lang.Integer batchID;

    public TrackingEvent() {
    }

    public java.lang.Integer getSendID() {
        return sendID;
    }

    public void setSendID(java.lang.Integer sendID) {
        this.sendID = sendID;
    }

    public java.lang.String getSubscriberKey() {
        return subscriberKey;
    }

    public void setSubscriberKey(java.lang.String subscriberKey) {
        this.subscriberKey = subscriberKey;
    }

    public java.util.Calendar getEventDate() {
        return eventDate;
    }

    public void setEventDate(java.util.Calendar eventDate) {
        this.eventDate = eventDate;
    }

    public com.exacttarget.EventType getEventType() {
        return eventType;
    }

    public void setEventType(com.exacttarget.EventType eventType) {
        this.eventType = eventType;
    }

    public java.lang.String getTriggeredSendDefinitionObjectID() {
        return triggeredSendDefinitionObjectID;
    }

    public void setTriggeredSendDefinitionObjectID(java.lang.String triggeredSendDefinitionObjectID) {
        this.triggeredSendDefinitionObjectID = triggeredSendDefinitionObjectID;
    }

    public java.lang.Integer getBatchID() {
        return batchID;
    }

    public void setBatchID(java.lang.Integer batchID) {
        this.batchID = batchID;
    }

}
