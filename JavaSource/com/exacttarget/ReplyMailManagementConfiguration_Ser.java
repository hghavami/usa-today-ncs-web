/**
 * ReplyMailManagementConfiguration_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ReplyMailManagementConfiguration_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public ReplyMailManagementConfiguration_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_654;
           context.qName2String(elemQName, true);
           elemQName = QName_0_655;
           context.qName2String(elemQName, true);
           elemQName = QName_0_656;
           context.qName2String(elemQName, true);
           elemQName = QName_0_657;
           context.qName2String(elemQName, true);
           elemQName = QName_0_658;
           context.qName2String(elemQName, true);
           elemQName = QName_0_659;
           context.qName2String(elemQName, true);
           elemQName = QName_0_660;
           context.qName2String(elemQName, true);
           elemQName = QName_0_661;
           context.qName2String(elemQName, true);
           elemQName = QName_0_662;
           context.qName2String(elemQName, true);
           elemQName = QName_0_663;
           context.qName2String(elemQName, true);
           elemQName = QName_0_664;
           context.qName2String(elemQName, true);
           elemQName = QName_0_665;
           context.qName2String(elemQName, true);
           elemQName = QName_0_666;
           context.qName2String(elemQName, true);
           elemQName = QName_0_667;
           context.qName2String(elemQName, true);
           elemQName = QName_0_668;
           context.qName2String(elemQName, true);
           elemQName = QName_0_669;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        ReplyMailManagementConfiguration bean = (ReplyMailManagementConfiguration) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_654;
          propValue = bean.getEmailDisplayName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_655;
          propValue = bean.getReplySubdomain();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_656;
          propValue = bean.getEmailReplyAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_657;
          propValue = bean.getDNSRedirectComplete();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_658;
          propValue = bean.getDeleteAutoReplies();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_659;
          propValue = bean.getSupportUnsubscribes();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_660;
          propValue = bean.getSupportUnsubKeyword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_661;
          propValue = bean.getSupportUnsubscribeKeyword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_662;
          propValue = bean.getSupportRemoveKeyword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_663;
          propValue = bean.getSupportOptOutKeyword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_664;
          propValue = bean.getSupportLeaveKeyword();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_665;
          propValue = bean.getSupportMisspelledKeywords();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_666;
          propValue = bean.getSendAutoReplies();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_667;
          propValue = bean.getAutoReplySubject();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_668;
          propValue = bean.getAutoReplyBody();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_669;
          propValue = bean.getForwardingAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_668 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplyBody");
    private final static javax.xml.namespace.QName QName_0_663 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportOptOutKeyword");
    private final static javax.xml.namespace.QName QName_0_655 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ReplySubdomain");
    private final static javax.xml.namespace.QName QName_0_664 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportLeaveKeyword");
    private final static javax.xml.namespace.QName QName_0_658 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeleteAutoReplies");
    private final static javax.xml.namespace.QName QName_0_667 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplySubject");
    private final static javax.xml.namespace.QName QName_0_656 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailReplyAddress");
    private final static javax.xml.namespace.QName QName_0_662 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportRemoveKeyword");
    private final static javax.xml.namespace.QName QName_0_666 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendAutoReplies");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_661 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubscribeKeyword");
    private final static javax.xml.namespace.QName QName_0_660 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubKeyword");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_654 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailDisplayName");
    private final static javax.xml.namespace.QName QName_0_665 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportMisspelledKeywords");
    private final static javax.xml.namespace.QName QName_0_659 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SupportUnsubscribes");
    private final static javax.xml.namespace.QName QName_0_657 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DNSRedirectComplete");
    private final static javax.xml.namespace.QName QName_0_669 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ForwardingAddress");
}
