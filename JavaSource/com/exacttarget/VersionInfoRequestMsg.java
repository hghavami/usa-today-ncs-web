/**
 * VersionInfoRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class VersionInfoRequestMsg  {
    private boolean includeVersionHistory;

    public VersionInfoRequestMsg() {
    }

    public boolean isIncludeVersionHistory() {
        return includeVersionHistory;
    }

    public void setIncludeVersionHistory(boolean includeVersionHistory) {
        this.includeVersionHistory = includeVersionHistory;
    }

}
