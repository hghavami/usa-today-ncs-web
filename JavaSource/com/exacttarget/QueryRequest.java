/**
 * QueryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class QueryRequest  {
    private com.exacttarget.ClientID[] clientIDs;
    private com.exacttarget.Query query;
    private com.exacttarget.AsyncResponse[] respondTo;
    private com.exacttarget.APIProperty[] partnerProperties;
    private java.lang.String continueRequest;
    private java.lang.Boolean queryAllAccounts;
    private java.lang.Boolean retrieveAllSinceLastBatch;

    public QueryRequest() {
    }

    public com.exacttarget.ClientID[] getClientIDs() {
        return clientIDs;
    }

    public void setClientIDs(com.exacttarget.ClientID[] clientIDs) {
        this.clientIDs = clientIDs;
    }

    public com.exacttarget.ClientID getClientIDs(int i) {
        return this.clientIDs[i];
    }

    public void setClientIDs(int i, com.exacttarget.ClientID value) {
        this.clientIDs[i] = value;
    }

    public com.exacttarget.Query getQuery() {
        return query;
    }

    public void setQuery(com.exacttarget.Query query) {
        this.query = query;
    }

    public com.exacttarget.AsyncResponse[] getRespondTo() {
        return respondTo;
    }

    public void setRespondTo(com.exacttarget.AsyncResponse[] respondTo) {
        this.respondTo = respondTo;
    }

    public com.exacttarget.AsyncResponse getRespondTo(int i) {
        return this.respondTo[i];
    }

    public void setRespondTo(int i, com.exacttarget.AsyncResponse value) {
        this.respondTo[i] = value;
    }

    public com.exacttarget.APIProperty[] getPartnerProperties() {
        return partnerProperties;
    }

    public void setPartnerProperties(com.exacttarget.APIProperty[] partnerProperties) {
        this.partnerProperties = partnerProperties;
    }

    public com.exacttarget.APIProperty getPartnerProperties(int i) {
        return this.partnerProperties[i];
    }

    public void setPartnerProperties(int i, com.exacttarget.APIProperty value) {
        this.partnerProperties[i] = value;
    }

    public java.lang.String getContinueRequest() {
        return continueRequest;
    }

    public void setContinueRequest(java.lang.String continueRequest) {
        this.continueRequest = continueRequest;
    }

    public java.lang.Boolean getQueryAllAccounts() {
        return queryAllAccounts;
    }

    public void setQueryAllAccounts(java.lang.Boolean queryAllAccounts) {
        this.queryAllAccounts = queryAllAccounts;
    }

    public java.lang.Boolean getRetrieveAllSinceLastBatch() {
        return retrieveAllSinceLastBatch;
    }

    public void setRetrieveAllSinceLastBatch(java.lang.Boolean retrieveAllSinceLastBatch) {
        this.retrieveAllSinceLastBatch = retrieveAllSinceLastBatch;
    }

}
