/**
 * ValidationResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ValidationResult  {
    private com.exacttarget.Subscriber subscriber;
    private java.util.Calendar checkTime;
    private java.util.Calendar checkTimeUTC;
    private java.lang.Boolean isResultValid;
    private java.lang.Boolean isSpam;
    private java.lang.Double score;
    private java.lang.Double threshold;
    private java.lang.String message;

    public ValidationResult() {
    }

    public com.exacttarget.Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(com.exacttarget.Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public java.util.Calendar getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(java.util.Calendar checkTime) {
        this.checkTime = checkTime;
    }

    public java.util.Calendar getCheckTimeUTC() {
        return checkTimeUTC;
    }

    public void setCheckTimeUTC(java.util.Calendar checkTimeUTC) {
        this.checkTimeUTC = checkTimeUTC;
    }

    public java.lang.Boolean getIsResultValid() {
        return isResultValid;
    }

    public void setIsResultValid(java.lang.Boolean isResultValid) {
        this.isResultValid = isResultValid;
    }

    public java.lang.Boolean getIsSpam() {
        return isSpam;
    }

    public void setIsSpam(java.lang.Boolean isSpam) {
        this.isSpam = isSpam;
    }

    public java.lang.Double getScore() {
        return score;
    }

    public void setScore(java.lang.Double score) {
        this.score = score;
    }

    public java.lang.Double getThreshold() {
        return threshold;
    }

    public void setThreshold(java.lang.Double threshold) {
        this.threshold = threshold;
    }

    public java.lang.String getMessage() {
        return message;
    }

    public void setMessage(java.lang.String message) {
        this.message = message;
    }

}
