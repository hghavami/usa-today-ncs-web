/**
 * Subscription_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Subscription_Deser extends com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializer {
    /**
     * Constructor
     */
    public Subscription_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new Subscription();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_320) {
          ((Subscription)value).setSubscriptionID(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseInteger(strValue));
          return true;}
        else if (qName==QName_0_321) {
          ((Subscription)value).setEmailsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_322) {
          ((Subscription)value).setAccountsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_323) {
          ((Subscription)value).setAdvAccountsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_324) {
          ((Subscription)value).setLPAccountsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_325) {
          ((Subscription)value).setDOTOAccountsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_326) {
          ((Subscription)value).setBUAccountsPurchased(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseint(strValue));
          return true;}
        else if (qName==QName_0_327) {
          ((Subscription)value).setBeginDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_328) {
          ((Subscription)value).setEndDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_281) {
          ((Subscription)value).setNotes(strValue);
          return true;}
        else if (qName==QName_0_329) {
          ((Subscription)value).setPeriod(strValue);
          return true;}
        else if (qName==QName_0_330) {
          ((Subscription)value).setNotificationTitle(strValue);
          return true;}
        else if (qName==QName_0_331) {
          ((Subscription)value).setNotificationMessage(strValue);
          return true;}
        else if (qName==QName_0_332) {
          ((Subscription)value).setNotificationFlag(strValue);
          return true;}
        else if (qName==QName_0_333) {
          ((Subscription)value).setNotificationExpDate(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseDateTimeToCalendar(strValue));
          return true;}
        else if (qName==QName_0_334) {
          ((Subscription)value).setForAccounting(strValue);
          return true;}
        else if (qName==QName_0_335) {
          ((Subscription)value).setHasPurchasedEmails(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseboolean(strValue));
          return true;}
        else if (qName==QName_0_336) {
          ((Subscription)value).setContractNumber(strValue);
          return true;}
        else if (qName==QName_0_337) {
          ((Subscription)value).setContractModifier(strValue);
          return true;}
        else if (qName==QName_0_699) {
          ((Subscription)value).setIsRenewal(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parseBoolean(strValue));
          return true;}
        else if (qName==QName_0_700) {
          ((Subscription)value).setNumberofEmails(com.ibm.ws.webservices.engine.encoding.ser.SimpleDeserializer.parselong(strValue));
          return true;}
        return false;
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return false;
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        return false;
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return false;
    }
    private final static javax.xml.namespace.QName QName_0_330 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationTitle");
    private final static javax.xml.namespace.QName QName_0_323 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AdvAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_335 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HasPurchasedEmails");
    private final static javax.xml.namespace.QName QName_0_331 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationMessage");
    private final static javax.xml.namespace.QName QName_0_332 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationFlag");
    private final static javax.xml.namespace.QName QName_0_327 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BeginDate");
    private final static javax.xml.namespace.QName QName_0_321 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailsPurchased");
    private final static javax.xml.namespace.QName QName_0_334 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ForAccounting");
    private final static javax.xml.namespace.QName QName_0_329 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Period");
    private final static javax.xml.namespace.QName QName_0_325 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DOTOAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_322 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_328 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EndDate");
    private final static javax.xml.namespace.QName QName_0_699 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsRenewal");
    private final static javax.xml.namespace.QName QName_0_700 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberofEmails");
    private final static javax.xml.namespace.QName QName_0_337 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContractModifier");
    private final static javax.xml.namespace.QName QName_0_320 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SubscriptionID");
    private final static javax.xml.namespace.QName QName_0_326 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "BUAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_336 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ContractNumber");
    private final static javax.xml.namespace.QName QName_0_333 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NotificationExpDate");
    private final static javax.xml.namespace.QName QName_0_324 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "LPAccountsPurchased");
    private final static javax.xml.namespace.QName QName_0_281 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Notes");
}
