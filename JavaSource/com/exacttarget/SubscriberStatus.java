/**
 * SubscriberStatus.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SubscriberStatus  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SubscriberStatus(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Active = "Active";
    public static final java.lang.String _Bounced = "Bounced";
    public static final java.lang.String _Held = "Held";
    public static final java.lang.String _Unsubscribed = "Unsubscribed";
    public static final java.lang.String _Deleted = "Deleted";
    public static final SubscriberStatus Active = new SubscriberStatus(_Active);
    public static final SubscriberStatus Bounced = new SubscriberStatus(_Bounced);
    public static final SubscriberStatus Held = new SubscriberStatus(_Held);
    public static final SubscriberStatus Unsubscribed = new SubscriberStatus(_Unsubscribed);
    public static final SubscriberStatus Deleted = new SubscriberStatus(_Deleted);
    public java.lang.String getValue() { return _value_;}
    public static SubscriberStatus fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        SubscriberStatus enumeration = (SubscriberStatus)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SubscriberStatus fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
