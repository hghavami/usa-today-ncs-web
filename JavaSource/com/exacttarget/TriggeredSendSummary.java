/**
 * TriggeredSendSummary.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TriggeredSendSummary  extends com.exacttarget.APIObject  {
    private com.exacttarget.TriggeredSendDefinition triggeredSendDefinition;
    private java.lang.Long sent;
    private java.lang.Long notSentDueToOptOut;
    private java.lang.Long notSentDueToUndeliverable;
    private java.lang.Long bounces;
    private java.lang.Long opens;
    private java.lang.Long clicks;
    private java.lang.Long uniqueOpens;
    private java.lang.Long uniqueClicks;
    private java.lang.Long optOuts;
    private java.lang.Long surveyResponses;
    private java.lang.Long FTAFRequests;
    private java.lang.Long FTAFEmailsSent;
    private java.lang.Long FTAFOptIns;
    private java.lang.Long conversions;
    private java.lang.Long uniqueConversions;
    private java.lang.Long inProcess;
    private java.lang.Long notSentDueToError;

    public TriggeredSendSummary() {
    }

    public com.exacttarget.TriggeredSendDefinition getTriggeredSendDefinition() {
        return triggeredSendDefinition;
    }

    public void setTriggeredSendDefinition(com.exacttarget.TriggeredSendDefinition triggeredSendDefinition) {
        this.triggeredSendDefinition = triggeredSendDefinition;
    }

    public java.lang.Long getSent() {
        return sent;
    }

    public void setSent(java.lang.Long sent) {
        this.sent = sent;
    }

    public java.lang.Long getNotSentDueToOptOut() {
        return notSentDueToOptOut;
    }

    public void setNotSentDueToOptOut(java.lang.Long notSentDueToOptOut) {
        this.notSentDueToOptOut = notSentDueToOptOut;
    }

    public java.lang.Long getNotSentDueToUndeliverable() {
        return notSentDueToUndeliverable;
    }

    public void setNotSentDueToUndeliverable(java.lang.Long notSentDueToUndeliverable) {
        this.notSentDueToUndeliverable = notSentDueToUndeliverable;
    }

    public java.lang.Long getBounces() {
        return bounces;
    }

    public void setBounces(java.lang.Long bounces) {
        this.bounces = bounces;
    }

    public java.lang.Long getOpens() {
        return opens;
    }

    public void setOpens(java.lang.Long opens) {
        this.opens = opens;
    }

    public java.lang.Long getClicks() {
        return clicks;
    }

    public void setClicks(java.lang.Long clicks) {
        this.clicks = clicks;
    }

    public java.lang.Long getUniqueOpens() {
        return uniqueOpens;
    }

    public void setUniqueOpens(java.lang.Long uniqueOpens) {
        this.uniqueOpens = uniqueOpens;
    }

    public java.lang.Long getUniqueClicks() {
        return uniqueClicks;
    }

    public void setUniqueClicks(java.lang.Long uniqueClicks) {
        this.uniqueClicks = uniqueClicks;
    }

    public java.lang.Long getOptOuts() {
        return optOuts;
    }

    public void setOptOuts(java.lang.Long optOuts) {
        this.optOuts = optOuts;
    }

    public java.lang.Long getSurveyResponses() {
        return surveyResponses;
    }

    public void setSurveyResponses(java.lang.Long surveyResponses) {
        this.surveyResponses = surveyResponses;
    }

    public java.lang.Long getFTAFRequests() {
        return FTAFRequests;
    }

    public void setFTAFRequests(java.lang.Long FTAFRequests) {
        this.FTAFRequests = FTAFRequests;
    }

    public java.lang.Long getFTAFEmailsSent() {
        return FTAFEmailsSent;
    }

    public void setFTAFEmailsSent(java.lang.Long FTAFEmailsSent) {
        this.FTAFEmailsSent = FTAFEmailsSent;
    }

    public java.lang.Long getFTAFOptIns() {
        return FTAFOptIns;
    }

    public void setFTAFOptIns(java.lang.Long FTAFOptIns) {
        this.FTAFOptIns = FTAFOptIns;
    }

    public java.lang.Long getConversions() {
        return conversions;
    }

    public void setConversions(java.lang.Long conversions) {
        this.conversions = conversions;
    }

    public java.lang.Long getUniqueConversions() {
        return uniqueConversions;
    }

    public void setUniqueConversions(java.lang.Long uniqueConversions) {
        this.uniqueConversions = uniqueConversions;
    }

    public java.lang.Long getInProcess() {
        return inProcess;
    }

    public void setInProcess(java.lang.Long inProcess) {
        this.inProcess = inProcess;
    }

    public java.lang.Long getNotSentDueToError() {
        return notSentDueToError;
    }

    public void setNotSentDueToError(java.lang.Long notSentDueToError) {
        this.notSentDueToError = notSentDueToError;
    }

}
