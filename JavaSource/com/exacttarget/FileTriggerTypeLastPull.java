/**
 * FileTriggerTypeLastPull.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class FileTriggerTypeLastPull  extends com.exacttarget.APIObject  {
    private java.lang.String externalReference;
    private java.lang.String type;
    private java.util.Calendar lastPullDate;

    public FileTriggerTypeLastPull() {
    }

    public java.lang.String getExternalReference() {
        return externalReference;
    }

    public void setExternalReference(java.lang.String externalReference) {
        this.externalReference = externalReference;
    }

    public java.lang.String getType() {
        return type;
    }

    public void setType(java.lang.String type) {
        this.type = type;
    }

    public java.util.Calendar getLastPullDate() {
        return lastPullDate;
    }

    public void setLastPullDate(java.util.Calendar lastPullDate) {
        this.lastPullDate = lastPullDate;
    }

}
