/**
 * ScheduleRequestMsg_Deser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ScheduleRequestMsg_Deser extends com.ibm.ws.webservices.engine.encoding.ser.BeanDeserializer {
    /**
     * Constructor
     */
    public ScheduleRequestMsg_Deser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    /**
     * Create instance of java bean
     */
    public void createValue() {
        value = new ScheduleRequestMsg();
    }
    protected boolean tryElementSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        if (qName==QName_0_70) {
          ((ScheduleRequestMsg)value).setAction(strValue);
          return true;}
        return false;
    }
    protected boolean tryAttributeSetFromString(javax.xml.namespace.QName qName, java.lang.String strValue) {
        return false;
    }
    protected boolean tryElementSetFromObject(javax.xml.namespace.QName qName, java.lang.Object objValue) {
        if (qName==QName_0_57) {
          ((ScheduleRequestMsg)value).setOptions((com.exacttarget.ScheduleOptions)objValue);
          return true;}
        else if (qName==QName_0_238) {
          ((ScheduleRequestMsg)value).setSchedule((com.exacttarget.ScheduleDefinition)objValue);
          return true;}
        else if (qName==QName_0_239) {
          if (objValue instanceof java.util.List) {
            com.exacttarget.APIObject[] array = new com.exacttarget.APIObject[((java.util.List)objValue).size()];
            ((java.util.List)objValue).toArray(array);
            ((ScheduleRequestMsg)value).setInteractions(array);
          } else { 
            ((ScheduleRequestMsg)value).setInteractions((com.exacttarget.APIObject[])objValue);}
          return true;}
        return false;
    }
    protected boolean tryElementSetFromList(javax.xml.namespace.QName qName, java.util.List listValue) {
        return false;
    }
    private final static javax.xml.namespace.QName QName_0_239 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Interactions");
    private final static javax.xml.namespace.QName QName_0_238 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Schedule");
    private final static javax.xml.namespace.QName QName_0_70 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Action");
    private final static javax.xml.namespace.QName QName_0_57 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Options");
}
