/**
 * DataExtensionDeleteResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DataExtensionDeleteResult  extends com.exacttarget.DeleteResult  {
    private java.lang.String errorMessage;
    private com.exacttarget.DataExtensionError[] keyErrors;

    public DataExtensionDeleteResult() {
    }

    public java.lang.String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public com.exacttarget.DataExtensionError[] getKeyErrors() {
        return keyErrors;
    }

    public void setKeyErrors(com.exacttarget.DataExtensionError[] keyErrors) {
        this.keyErrors = keyErrors;
    }

}
