/**
 * LayoutType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class LayoutType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LayoutType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _HTMLWrapped = "HTMLWrapped";
    public static final java.lang.String _RawText = "RawText";
    public static final java.lang.String _SMS = "SMS";
    public static final LayoutType HTMLWrapped = new LayoutType(_HTMLWrapped);
    public static final LayoutType RawText = new LayoutType(_RawText);
    public static final LayoutType SMS = new LayoutType(_SMS);
    public java.lang.String getValue() { return _value_;}
    public static LayoutType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LayoutType enumeration = (LayoutType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LayoutType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
