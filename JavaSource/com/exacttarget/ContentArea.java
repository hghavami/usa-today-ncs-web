/**
 * ContentArea.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ContentArea  extends com.exacttarget.APIObject  {
    private java.lang.String key;
    private java.lang.String content;
    private java.lang.Boolean isBlank;
    private java.lang.Integer categoryID;
    private java.lang.String name;
    private com.exacttarget.LayoutType layout;
    private java.lang.Boolean isDynamicContent;
    private java.lang.Boolean isSurvey;

    public ContentArea() {
    }

    public java.lang.String getKey() {
        return key;
    }

    public void setKey(java.lang.String key) {
        this.key = key;
    }

    public java.lang.String getContent() {
        return content;
    }

    public void setContent(java.lang.String content) {
        this.content = content;
    }

    public java.lang.Boolean getIsBlank() {
        return isBlank;
    }

    public void setIsBlank(java.lang.Boolean isBlank) {
        this.isBlank = isBlank;
    }

    public java.lang.Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(java.lang.Integer categoryID) {
        this.categoryID = categoryID;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public com.exacttarget.LayoutType getLayout() {
        return layout;
    }

    public void setLayout(com.exacttarget.LayoutType layout) {
        this.layout = layout;
    }

    public java.lang.Boolean getIsDynamicContent() {
        return isDynamicContent;
    }

    public void setIsDynamicContent(java.lang.Boolean isDynamicContent) {
        this.isDynamicContent = isDynamicContent;
    }

    public java.lang.Boolean getIsSurvey() {
        return isSurvey;
    }

    public void setIsSurvey(java.lang.Boolean isSurvey) {
        this.isSurvey = isSurvey;
    }

}
