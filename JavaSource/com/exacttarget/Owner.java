/**
 * Owner.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Owner  {
    private com.exacttarget.ClientID client;
    private java.lang.String fromName;
    private java.lang.String fromAddress;
    private com.exacttarget.AccountUser user;

    public Owner() {
    }

    public com.exacttarget.ClientID getClient() {
        return client;
    }

    public void setClient(com.exacttarget.ClientID client) {
        this.client = client;
    }

    public java.lang.String getFromName() {
        return fromName;
    }

    public void setFromName(java.lang.String fromName) {
        this.fromName = fromName;
    }

    public java.lang.String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(java.lang.String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public com.exacttarget.AccountUser getUser() {
        return user;
    }

    public void setUser(com.exacttarget.AccountUser user) {
        this.user = user;
    }

}
