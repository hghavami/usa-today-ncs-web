/**
 * ListSend_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ListSend_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public ListSend_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_376;
           context.qName2String(elemQName, true);
           elemQName = QName_0_390;
           context.qName2String(elemQName, true);
           elemQName = QName_0_403;
           context.qName2String(elemQName, true);
           elemQName = QName_0_404;
           context.qName2String(elemQName, true);
           elemQName = QName_0_405;
           context.qName2String(elemQName, true);
           elemQName = QName_0_406;
           context.qName2String(elemQName, true);
           elemQName = QName_0_407;
           context.qName2String(elemQName, true);
           elemQName = QName_0_408;
           context.qName2String(elemQName, true);
           elemQName = QName_0_409;
           context.qName2String(elemQName, true);
           elemQName = QName_0_410;
           context.qName2String(elemQName, true);
           elemQName = QName_0_411;
           context.qName2String(elemQName, true);
           elemQName = QName_0_412;
           context.qName2String(elemQName, true);
           elemQName = QName_0_413;
           context.qName2String(elemQName, true);
           elemQName = QName_0_414;
           context.qName2String(elemQName, true);
           elemQName = QName_0_415;
           context.qName2String(elemQName, true);
           elemQName = QName_0_416;
           context.qName2String(elemQName, true);
           elemQName = QName_0_417;
           context.qName2String(elemQName, true);
           elemQName = QName_0_418;
           context.qName2String(elemQName, true);
           elemQName = QName_0_419;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        ListSend bean = (ListSend) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_376;
          propValue = bean.getSendID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_390;
          propValue = bean.getList();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_390,
              false,null,context);
          propQName = QName_0_403;
          propValue = bean.getDuplicates();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_404;
          propValue = bean.getInvalidAddresses();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_405;
          propValue = bean.getExistingUndeliverables();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_406;
          propValue = bean.getExistingUnsubscribes();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_407;
          propValue = bean.getHardBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_408;
          propValue = bean.getSoftBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_409;
          propValue = bean.getOtherBounces();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_410;
          propValue = bean.getForwardedEmails();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_411;
          propValue = bean.getUniqueClicks();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_412;
          propValue = bean.getUniqueOpens();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_413;
          propValue = bean.getNumberSent();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_414;
          propValue = bean.getNumberDelivered();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_415;
          propValue = bean.getUnsubscribes();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_416;
          propValue = bean.getMissingAddresses();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_417;
          propValue = bean.getPreviewURL();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_418;
          {
            propValue = bean.getLinks();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_431,
                    true,null,context);
              }
            }
          }
          propQName = QName_0_419;
          {
            propValue = bean.getEvents();
            if (propValue != null) {
              for (int i=0; i<java.lang.reflect.Array.getLength(propValue); i++) {
                serializeChild(propQName, null, 
                    java.lang.reflect.Array.get(propValue, i), 
                    QName_0_432,
                    true,null,context);
              }
            }
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_419 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Events");
    private final static javax.xml.namespace.QName QName_0_407 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "HardBounces");
    private final static javax.xml.namespace.QName QName_0_405 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExistingUndeliverables");
    private final static javax.xml.namespace.QName QName_0_408 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SoftBounces");
    private final static javax.xml.namespace.QName QName_0_404 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "InvalidAddresses");
    private final static javax.xml.namespace.QName QName_0_431 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Link");
    private final static javax.xml.namespace.QName QName_0_390 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "List");
    private final static javax.xml.namespace.QName QName_0_415 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Unsubscribes");
    private final static javax.xml.namespace.QName QName_0_410 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ForwardedEmails");
    private final static javax.xml.namespace.QName QName_0_376 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendID");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_406 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ExistingUnsubscribes");
    private final static javax.xml.namespace.QName QName_0_403 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Duplicates");
    private final static javax.xml.namespace.QName QName_0_413 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberSent");
    private final static javax.xml.namespace.QName QName_0_417 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PreviewURL");
    private final static javax.xml.namespace.QName QName_0_416 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MissingAddresses");
    private final static javax.xml.namespace.QName QName_0_411 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueClicks");
    private final static javax.xml.namespace.QName QName_0_418 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Links");
    private final static javax.xml.namespace.QName QName_0_412 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UniqueOpens");
    private final static javax.xml.namespace.QName QName_0_414 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "NumberDelivered");
    private final static javax.xml.namespace.QName QName_0_409 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "OtherBounces");
    private final static javax.xml.namespace.QName QName_0_432 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TrackingEvent");
}
