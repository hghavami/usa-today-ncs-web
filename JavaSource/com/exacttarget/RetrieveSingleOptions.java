/**
 * RetrieveSingleOptions.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class RetrieveSingleOptions  extends com.exacttarget.Options  {
    private com.exacttarget.APIProperty[] parameters;

    public RetrieveSingleOptions() {
    }

    public com.exacttarget.APIProperty[] getParameters() {
        return parameters;
    }

    public void setParameters(com.exacttarget.APIProperty[] parameters) {
        this.parameters = parameters;
    }

}
