/**
 * InteractionDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class InteractionDefinition  extends com.exacttarget.InteractionBaseObject  {
    private java.lang.String interactionObjectID;

    public InteractionDefinition() {
    }

    public java.lang.String getInteractionObjectID() {
        return interactionObjectID;
    }

    public void setInteractionObjectID(java.lang.String interactionObjectID) {
        this.interactionObjectID = interactionObjectID;
    }

}
