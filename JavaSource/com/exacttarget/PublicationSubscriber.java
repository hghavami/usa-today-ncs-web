/**
 * PublicationSubscriber.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class PublicationSubscriber  extends com.exacttarget.APIObject  {
    private com.exacttarget.Publication publication;
    private com.exacttarget.Subscriber subscriber;

    public PublicationSubscriber() {
    }

    public com.exacttarget.Publication getPublication() {
        return publication;
    }

    public void setPublication(com.exacttarget.Publication publication) {
        this.publication = publication;
    }

    public com.exacttarget.Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(com.exacttarget.Subscriber subscriber) {
        this.subscriber = subscriber;
    }

}
