/**
 * EmailAddress_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class EmailAddress_Ser extends com.exacttarget.SubscriberAddress_Ser {
    /**
     * Constructor
     */
    public EmailAddress_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_346;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        EmailAddress bean = (EmailAddress) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_346;
          propValue = bean.getType();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_190,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_190 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EmailType");
    private final static javax.xml.namespace.QName QName_0_346 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Type");
}
