/**
 * ScheduleRequestMsg.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ScheduleRequestMsg  {
    private com.exacttarget.ScheduleOptions options;
    private java.lang.String action;
    private com.exacttarget.ScheduleDefinition schedule;
    private com.exacttarget.APIObject[] interactions;

    public ScheduleRequestMsg() {
    }

    public com.exacttarget.ScheduleOptions getOptions() {
        return options;
    }

    public void setOptions(com.exacttarget.ScheduleOptions options) {
        this.options = options;
    }

    public java.lang.String getAction() {
        return action;
    }

    public void setAction(java.lang.String action) {
        this.action = action;
    }

    public com.exacttarget.ScheduleDefinition getSchedule() {
        return schedule;
    }

    public void setSchedule(com.exacttarget.ScheduleDefinition schedule) {
        this.schedule = schedule;
    }

    public com.exacttarget.APIObject[] getInteractions() {
        return interactions;
    }

    public void setInteractions(com.exacttarget.APIObject[] interactions) {
        this.interactions = interactions;
    }

}
