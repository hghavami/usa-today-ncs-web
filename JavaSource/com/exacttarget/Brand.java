/**
 * Brand.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Brand  extends com.exacttarget.APIObject  {
    private java.lang.Integer brandID;
    private java.lang.String label;
    private java.lang.String comment;
    private com.exacttarget.BrandTag[] brandTags;

    public Brand() {
    }

    public java.lang.Integer getBrandID() {
        return brandID;
    }

    public void setBrandID(java.lang.Integer brandID) {
        this.brandID = brandID;
    }

    public java.lang.String getLabel() {
        return label;
    }

    public void setLabel(java.lang.String label) {
        this.label = label;
    }

    public java.lang.String getComment() {
        return comment;
    }

    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    public com.exacttarget.BrandTag[] getBrandTags() {
        return brandTags;
    }

    public void setBrandTags(com.exacttarget.BrandTag[] brandTags) {
        this.brandTags = brandTags;
    }

    public com.exacttarget.BrandTag getBrandTags(int i) {
        return this.brandTags[i];
    }

    public void setBrandTags(int i, com.exacttarget.BrandTag value) {
        this.brandTags[i] = value;
    }

}
