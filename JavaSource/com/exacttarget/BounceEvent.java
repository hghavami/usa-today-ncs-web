/**
 * BounceEvent.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class BounceEvent  extends com.exacttarget.TrackingEvent  {
    private java.lang.String SMTPCode;
    private java.lang.String bounceCategory;
    private java.lang.String SMTPReason;
    private java.lang.String bounceType;

    public BounceEvent() {
    }

    public java.lang.String getSMTPCode() {
        return SMTPCode;
    }

    public void setSMTPCode(java.lang.String SMTPCode) {
        this.SMTPCode = SMTPCode;
    }

    public java.lang.String getBounceCategory() {
        return bounceCategory;
    }

    public void setBounceCategory(java.lang.String bounceCategory) {
        this.bounceCategory = bounceCategory;
    }

    public java.lang.String getSMTPReason() {
        return SMTPReason;
    }

    public void setSMTPReason(java.lang.String SMTPReason) {
        this.SMTPReason = SMTPReason;
    }

    public java.lang.String getBounceType() {
        return bounceType;
    }

    public void setBounceType(java.lang.String bounceType) {
        this.bounceType = bounceType;
    }

}
