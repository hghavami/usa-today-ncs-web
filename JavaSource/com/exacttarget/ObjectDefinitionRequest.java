/**
 * ObjectDefinitionRequest.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ObjectDefinitionRequest  {
    private com.exacttarget.ClientID client;
    private java.lang.String objectType;

    public ObjectDefinitionRequest() {
    }

    public com.exacttarget.ClientID getClient() {
        return client;
    }

    public void setClient(com.exacttarget.ClientID client) {
        this.client = client;
    }

    public java.lang.String getObjectType() {
        return objectType;
    }

    public void setObjectType(java.lang.String objectType) {
        this.objectType = objectType;
    }

}
