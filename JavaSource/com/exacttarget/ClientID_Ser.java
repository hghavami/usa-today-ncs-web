/**
 * ClientID_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class ClientID_Ser extends com.ibm.ws.webservices.engine.encoding.ser.BeanSerializer {
    /**
     * Constructor
     */
    public ClientID_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
           javax.xml.namespace.QName
           elemQName = QName_0_10;
           context.qName2String(elemQName, true);
           elemQName = QName_0_5;
           context.qName2String(elemQName, true);
           elemQName = QName_0_15;
           context.qName2String(elemQName, true);
           elemQName = QName_0_16;
           context.qName2String(elemQName, true);
           elemQName = QName_0_17;
           context.qName2String(elemQName, true);
           elemQName = QName_0_18;
           context.qName2String(elemQName, true);
           elemQName = QName_0_19;
           context.qName2String(elemQName, true);
           elemQName = QName_0_673;
           context.qName2String(elemQName, true);
           elemQName = QName_0_7;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        ClientID bean = (ClientID) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_10;
          propValue = bean.getClientID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_5;
          propValue = bean.getID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_15;
          propValue = bean.getPartnerClientKey();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_16;
          propValue = bean.getUserID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_17;
          propValue = bean.getPartnerUserKey();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_18;
          propValue = bean.getCreatedBy();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_19;
          propValue = bean.getModifiedBy();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_673;
          propValue = bean.getEnterpriseID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_7;
          propValue = bean.getCustomerKey();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_10 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ClientID");
    private final static javax.xml.namespace.QName QName_0_7 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CustomerKey");
    private final static javax.xml.namespace.QName QName_0_18 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CreatedBy");
    private final static javax.xml.namespace.QName QName_0_17 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerUserKey");
    private final static javax.xml.namespace.QName QName_0_16 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserID");
    private final static javax.xml.namespace.QName QName_0_19 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ModifiedBy");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_1_314 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_0_673 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "EnterpriseID");
    private final static javax.xml.namespace.QName QName_0_15 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "PartnerClientKey");
    private final static javax.xml.namespace.QName QName_0_5 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ID");
}
