/**
 * DataExtension.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class DataExtension  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.Boolean isSendable;
    private java.lang.Boolean isTestable;
    private com.exacttarget.DataExtensionField sendableDataExtensionField;
    private com.exacttarget.Attribute sendableSubscriberField;
    private com.exacttarget.DataExtensionTemplate template;
    private java.lang.Integer dataRetentionPeriodLength;
    private java.lang.Integer dataRetentionPeriodUnitOfMeasure;
    private java.lang.Boolean rowBasedRetention;
    private java.lang.Boolean resetRetentionPeriodOnImport;
    private java.lang.Boolean deleteAtEndOfRetentionPeriod;
    private java.lang.String retainUntil;
    private com.exacttarget.DataExtensionField[] fields;
    private com.exacttarget.DateTimeUnitOfMeasure dataRetentionPeriod;
    private java.lang.Long categoryID;
    private java.lang.String status;

    public DataExtension() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.Boolean getIsSendable() {
        return isSendable;
    }

    public void setIsSendable(java.lang.Boolean isSendable) {
        this.isSendable = isSendable;
    }

    public java.lang.Boolean getIsTestable() {
        return isTestable;
    }

    public void setIsTestable(java.lang.Boolean isTestable) {
        this.isTestable = isTestable;
    }

    public com.exacttarget.DataExtensionField getSendableDataExtensionField() {
        return sendableDataExtensionField;
    }

    public void setSendableDataExtensionField(com.exacttarget.DataExtensionField sendableDataExtensionField) {
        this.sendableDataExtensionField = sendableDataExtensionField;
    }

    public com.exacttarget.Attribute getSendableSubscriberField() {
        return sendableSubscriberField;
    }

    public void setSendableSubscriberField(com.exacttarget.Attribute sendableSubscriberField) {
        this.sendableSubscriberField = sendableSubscriberField;
    }

    public com.exacttarget.DataExtensionTemplate getTemplate() {
        return template;
    }

    public void setTemplate(com.exacttarget.DataExtensionTemplate template) {
        this.template = template;
    }

    public java.lang.Integer getDataRetentionPeriodLength() {
        return dataRetentionPeriodLength;
    }

    public void setDataRetentionPeriodLength(java.lang.Integer dataRetentionPeriodLength) {
        this.dataRetentionPeriodLength = dataRetentionPeriodLength;
    }

    public java.lang.Integer getDataRetentionPeriodUnitOfMeasure() {
        return dataRetentionPeriodUnitOfMeasure;
    }

    public void setDataRetentionPeriodUnitOfMeasure(java.lang.Integer dataRetentionPeriodUnitOfMeasure) {
        this.dataRetentionPeriodUnitOfMeasure = dataRetentionPeriodUnitOfMeasure;
    }

    public java.lang.Boolean getRowBasedRetention() {
        return rowBasedRetention;
    }

    public void setRowBasedRetention(java.lang.Boolean rowBasedRetention) {
        this.rowBasedRetention = rowBasedRetention;
    }

    public java.lang.Boolean getResetRetentionPeriodOnImport() {
        return resetRetentionPeriodOnImport;
    }

    public void setResetRetentionPeriodOnImport(java.lang.Boolean resetRetentionPeriodOnImport) {
        this.resetRetentionPeriodOnImport = resetRetentionPeriodOnImport;
    }

    public java.lang.Boolean getDeleteAtEndOfRetentionPeriod() {
        return deleteAtEndOfRetentionPeriod;
    }

    public void setDeleteAtEndOfRetentionPeriod(java.lang.Boolean deleteAtEndOfRetentionPeriod) {
        this.deleteAtEndOfRetentionPeriod = deleteAtEndOfRetentionPeriod;
    }

    public java.lang.String getRetainUntil() {
        return retainUntil;
    }

    public void setRetainUntil(java.lang.String retainUntil) {
        this.retainUntil = retainUntil;
    }

    public com.exacttarget.DataExtensionField[] getFields() {
        return fields;
    }

    public void setFields(com.exacttarget.DataExtensionField[] fields) {
        this.fields = fields;
    }

    public com.exacttarget.DateTimeUnitOfMeasure getDataRetentionPeriod() {
        return dataRetentionPeriod;
    }

    public void setDataRetentionPeriod(com.exacttarget.DateTimeUnitOfMeasure dataRetentionPeriod) {
        this.dataRetentionPeriod = dataRetentionPeriod;
    }

    public java.lang.Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(java.lang.Long categoryID) {
        this.categoryID = categoryID;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

}
