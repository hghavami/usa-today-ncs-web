/**
 * PartnerAPILocator.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class PartnerAPILocator extends com.ibm.ws.webservices.multiprotocol.AgnosticService implements com.ibm.ws.webservices.multiprotocol.GeneratedService, com.exacttarget.PartnerAPI {

     // ExactTarget Partner API

    public PartnerAPILocator() {
        super(com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
           "http://exacttarget.com/wsdl/partnerAPI",
           "PartnerAPI"));

        context.setLocatorName("com.exacttarget.PartnerAPILocator");
    }

    public PartnerAPILocator(com.ibm.ws.webservices.multiprotocol.ServiceContext ctx) {
        super(ctx);
        context.setLocatorName("com.exacttarget.PartnerAPILocator");
    }

    // Use to get a proxy class for soap
    private final java.lang.String soap_address = "https://webservice.exacttarget.com/Service.asmx";

    public java.lang.String getSoapAddress() {
        if (context.getOverriddingEndpointURIs() == null) {
            return soap_address;
        }
        String overriddingEndpoint = (String) context.getOverriddingEndpointURIs().get("Soap");
        if (overriddingEndpoint != null) {
            return overriddingEndpoint;
        }
        else {
            return soap_address;
        }
    }

    private java.lang.String soapPortName = "Soap";

    // The WSDD port name defaults to the port name.
    private java.lang.String soapWSDDPortName = "Soap";

    public java.lang.String getSoapWSDDPortName() {
        return soapWSDDPortName;
    }

    public void setSoapWSDDPortName(java.lang.String name) {
        soapWSDDPortName = name;
    }

    public com.exacttarget.Soap getSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getSoapAddress());
        }
        catch (java.net.MalformedURLException e) {
            return null; // unlikely as URL was validated in WSDL2Java
        }
        return getSoap(endpoint);
    }

    public com.exacttarget.Soap getSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        com.exacttarget.Soap _stub =
            (com.exacttarget.Soap) getStub(
                soapPortName,
                (String) getPort2NamespaceMap().get(soapPortName),
                com.exacttarget.Soap.class,
                "com.exacttarget.SoapBindingStub",
                portAddress.toString());
        if (_stub instanceof com.ibm.ws.webservices.engine.client.Stub) {
            ((com.ibm.ws.webservices.engine.client.Stub) _stub).setPortName(soapWSDDPortName);
        }
        return _stub;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.exacttarget.Soap.class.isAssignableFrom(serviceEndpointInterface)) {
                return getSoap();
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("WSWS3273E: Error: There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        String inputPortName = portName.getLocalPart();
        if ("Soap".equals(inputPortName)) {
            return getSoap();
        }
        else  {
            throw new javax.xml.rpc.ServiceException();
        }
    }

    public void setPortNamePrefix(java.lang.String prefix) {
        soapWSDDPortName = prefix + "/" + soapPortName;
    }

    public javax.xml.namespace.QName getServiceName() {
        return com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PartnerAPI");
    }

    private java.util.Map port2NamespaceMap = null;

    protected synchronized java.util.Map getPort2NamespaceMap() {
        if (port2NamespaceMap == null) {
            port2NamespaceMap = new java.util.HashMap();
            port2NamespaceMap.put(
               "Soap",
               "http://schemas.xmlsoap.org/wsdl/soap/");
        }
        return port2NamespaceMap;
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            String serviceNamespace = getServiceName().getNamespaceURI();
            for (java.util.Iterator i = getPort2NamespaceMap().keySet().iterator(); i.hasNext(); ) {
                ports.add(
                    com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                        serviceNamespace,
                        (String) i.next()));
            }
        }
        return ports.iterator();
    }

    public javax.xml.rpc.Call[] getCalls(javax.xml.namespace.QName portName) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
        if  (portName.getLocalPart().equals("Soap")) {
            return new javax.xml.rpc.Call[] {
                createCall(portName, "Create", "null"),
                createCall(portName, "Retrieve", "null"),
                createCall(portName, "Update", "null"),
                createCall(portName, "Delete", "null"),
                createCall(portName, "Query", "null"),
                createCall(portName, "Describe", "null"),
                createCall(portName, "Execute", "null"),
                createCall(portName, "Perform", "null"),
                createCall(portName, "Configure", "null"),
                createCall(portName, "Schedule", "null"),
                createCall(portName, "VersionInfo", "null"),
                createCall(portName, "Extract", "null"),
                createCall(portName, "GetSystemStatus", "null"),
            };
        }
        else {
            throw new javax.xml.rpc.ServiceException("WSWS3062E: Error: portName should not be null.");
        }
    }
}
