/**
 * OverrideType.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class OverrideType  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected OverrideType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _DoNotOverride = "DoNotOverride";
    public static final java.lang.String _Override = "Override";
    public static final java.lang.String _OverrideExceptWhenNull = "OverrideExceptWhenNull";
    public static final OverrideType DoNotOverride = new OverrideType(_DoNotOverride);
    public static final OverrideType Override = new OverrideType(_Override);
    public static final OverrideType OverrideExceptWhenNull = new OverrideType(_OverrideExceptWhenNull);
    public java.lang.String getValue() { return _value_;}
    public static OverrideType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        OverrideType enumeration = (OverrideType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static OverrideType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
