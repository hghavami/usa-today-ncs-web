/**
 * Subscriber.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Subscriber  extends com.exacttarget.APIObject  {
    private java.lang.String emailAddress;
    private com.exacttarget.Attribute[] attributes;
    private java.lang.String subscriberKey;
    private java.util.Calendar unsubscribedDate;
    private com.exacttarget.SubscriberStatus status;
    private java.lang.String partnerType;
    private com.exacttarget.EmailType emailTypePreference;
    private com.exacttarget.SubscriberList[] lists;
    private com.exacttarget.GlobalUnsubscribeCategory globalUnsubscribeCategory;
    private com.exacttarget.SubscriberTypeDefinition subscriberTypeDefinition;
    private com.exacttarget.SubscriberAddress[] addresses;
    private com.exacttarget.SMSAddress primarySMSAddress;
    private com.exacttarget.SubscriberAddressStatus primarySMSPublicationStatus;
    private com.exacttarget.EmailAddress primaryEmailAddress;
    private com.exacttarget.Locale locale;

    public Subscriber() {
    }

    public java.lang.String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(java.lang.String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public com.exacttarget.Attribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(com.exacttarget.Attribute[] attributes) {
        this.attributes = attributes;
    }

    public com.exacttarget.Attribute getAttributes(int i) {
        return this.attributes[i];
    }

    public void setAttributes(int i, com.exacttarget.Attribute value) {
        this.attributes[i] = value;
    }

    public java.lang.String getSubscriberKey() {
        return subscriberKey;
    }

    public void setSubscriberKey(java.lang.String subscriberKey) {
        this.subscriberKey = subscriberKey;
    }

    public java.util.Calendar getUnsubscribedDate() {
        return unsubscribedDate;
    }

    public void setUnsubscribedDate(java.util.Calendar unsubscribedDate) {
        this.unsubscribedDate = unsubscribedDate;
    }

    public com.exacttarget.SubscriberStatus getStatus() {
        return status;
    }

    public void setStatus(com.exacttarget.SubscriberStatus status) {
        this.status = status;
    }

    public java.lang.String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(java.lang.String partnerType) {
        this.partnerType = partnerType;
    }

    public com.exacttarget.EmailType getEmailTypePreference() {
        return emailTypePreference;
    }

    public void setEmailTypePreference(com.exacttarget.EmailType emailTypePreference) {
        this.emailTypePreference = emailTypePreference;
    }

    public com.exacttarget.SubscriberList[] getLists() {
        return lists;
    }

    public void setLists(com.exacttarget.SubscriberList[] lists) {
        this.lists = lists;
    }

    public com.exacttarget.SubscriberList getLists(int i) {
        return this.lists[i];
    }

    public void setLists(int i, com.exacttarget.SubscriberList value) {
        this.lists[i] = value;
    }

    public com.exacttarget.GlobalUnsubscribeCategory getGlobalUnsubscribeCategory() {
        return globalUnsubscribeCategory;
    }

    public void setGlobalUnsubscribeCategory(com.exacttarget.GlobalUnsubscribeCategory globalUnsubscribeCategory) {
        this.globalUnsubscribeCategory = globalUnsubscribeCategory;
    }

    public com.exacttarget.SubscriberTypeDefinition getSubscriberTypeDefinition() {
        return subscriberTypeDefinition;
    }

    public void setSubscriberTypeDefinition(com.exacttarget.SubscriberTypeDefinition subscriberTypeDefinition) {
        this.subscriberTypeDefinition = subscriberTypeDefinition;
    }

    public com.exacttarget.SubscriberAddress[] getAddresses() {
        return addresses;
    }

    public void setAddresses(com.exacttarget.SubscriberAddress[] addresses) {
        this.addresses = addresses;
    }

    public com.exacttarget.SMSAddress getPrimarySMSAddress() {
        return primarySMSAddress;
    }

    public void setPrimarySMSAddress(com.exacttarget.SMSAddress primarySMSAddress) {
        this.primarySMSAddress = primarySMSAddress;
    }

    public com.exacttarget.SubscriberAddressStatus getPrimarySMSPublicationStatus() {
        return primarySMSPublicationStatus;
    }

    public void setPrimarySMSPublicationStatus(com.exacttarget.SubscriberAddressStatus primarySMSPublicationStatus) {
        this.primarySMSPublicationStatus = primarySMSPublicationStatus;
    }

    public com.exacttarget.EmailAddress getPrimaryEmailAddress() {
        return primaryEmailAddress;
    }

    public void setPrimaryEmailAddress(com.exacttarget.EmailAddress primaryEmailAddress) {
        this.primaryEmailAddress = primaryEmailAddress;
    }

    public com.exacttarget.Locale getLocale() {
        return locale;
    }

    public void setLocale(com.exacttarget.Locale locale) {
        this.locale = locale;
    }

}
