/**
 * Email.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Email  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String folder;
    private java.lang.Integer categoryID;
    private java.lang.String HTMLBody;
    private java.lang.String textBody;
    private com.exacttarget.ContentArea[] contentAreas;
    private java.lang.String subject;
    private java.lang.Boolean isActive;
    private java.lang.Boolean isHTMLPaste;
    private java.lang.Integer clonedFromID;
    private java.lang.String status;
    private java.lang.String emailType;
    private java.lang.String characterSet;
    private java.lang.Boolean hasDynamicSubjectLine;
    private java.lang.String contentCheckStatus;

    public Email() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getFolder() {
        return folder;
    }

    public void setFolder(java.lang.String folder) {
        this.folder = folder;
    }

    public java.lang.Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(java.lang.Integer categoryID) {
        this.categoryID = categoryID;
    }

    public java.lang.String getHTMLBody() {
        return HTMLBody;
    }

    public void setHTMLBody(java.lang.String HTMLBody) {
        this.HTMLBody = HTMLBody;
    }

    public java.lang.String getTextBody() {
        return textBody;
    }

    public void setTextBody(java.lang.String textBody) {
        this.textBody = textBody;
    }

    public com.exacttarget.ContentArea[] getContentAreas() {
        return contentAreas;
    }

    public void setContentAreas(com.exacttarget.ContentArea[] contentAreas) {
        this.contentAreas = contentAreas;
    }

    public com.exacttarget.ContentArea getContentAreas(int i) {
        return this.contentAreas[i];
    }

    public void setContentAreas(int i, com.exacttarget.ContentArea value) {
        this.contentAreas[i] = value;
    }

    public java.lang.String getSubject() {
        return subject;
    }

    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }

    public java.lang.Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(java.lang.Boolean isActive) {
        this.isActive = isActive;
    }

    public java.lang.Boolean getIsHTMLPaste() {
        return isHTMLPaste;
    }

    public void setIsHTMLPaste(java.lang.Boolean isHTMLPaste) {
        this.isHTMLPaste = isHTMLPaste;
    }

    public java.lang.Integer getClonedFromID() {
        return clonedFromID;
    }

    public void setClonedFromID(java.lang.Integer clonedFromID) {
        this.clonedFromID = clonedFromID;
    }

    public java.lang.String getStatus() {
        return status;
    }

    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    public java.lang.String getEmailType() {
        return emailType;
    }

    public void setEmailType(java.lang.String emailType) {
        this.emailType = emailType;
    }

    public java.lang.String getCharacterSet() {
        return characterSet;
    }

    public void setCharacterSet(java.lang.String characterSet) {
        this.characterSet = characterSet;
    }

    public java.lang.Boolean getHasDynamicSubjectLine() {
        return hasDynamicSubjectLine;
    }

    public void setHasDynamicSubjectLine(java.lang.Boolean hasDynamicSubjectLine) {
        this.hasDynamicSubjectLine = hasDynamicSubjectLine;
    }

    public java.lang.String getContentCheckStatus() {
        return contentCheckStatus;
    }

    public void setContentCheckStatus(java.lang.String contentCheckStatus) {
        this.contentCheckStatus = contentCheckStatus;
    }

}
