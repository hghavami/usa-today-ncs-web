/**
 * SMSAddress.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class SMSAddress  extends com.exacttarget.SubscriberAddress  {
    private java.lang.String carrier;

    public SMSAddress() {
    }

    public java.lang.String getCarrier() {
        return carrier;
    }

    public void setCarrier(java.lang.String carrier) {
        this.carrier = carrier;
    }

}
