/**
 * RecurrenceTypeEnum.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class RecurrenceTypeEnum  {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected RecurrenceTypeEnum(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    };

    public static final java.lang.String _Secondly = "Secondly";
    public static final java.lang.String _Minutely = "Minutely";
    public static final java.lang.String _Hourly = "Hourly";
    public static final java.lang.String _Daily = "Daily";
    public static final java.lang.String _Weekly = "Weekly";
    public static final java.lang.String _Monthly = "Monthly";
    public static final java.lang.String _Yearly = "Yearly";
    public static final RecurrenceTypeEnum Secondly = new RecurrenceTypeEnum(_Secondly);
    public static final RecurrenceTypeEnum Minutely = new RecurrenceTypeEnum(_Minutely);
    public static final RecurrenceTypeEnum Hourly = new RecurrenceTypeEnum(_Hourly);
    public static final RecurrenceTypeEnum Daily = new RecurrenceTypeEnum(_Daily);
    public static final RecurrenceTypeEnum Weekly = new RecurrenceTypeEnum(_Weekly);
    public static final RecurrenceTypeEnum Monthly = new RecurrenceTypeEnum(_Monthly);
    public static final RecurrenceTypeEnum Yearly = new RecurrenceTypeEnum(_Yearly);
    public java.lang.String getValue() { return _value_;}
    public static RecurrenceTypeEnum fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        RecurrenceTypeEnum enumeration = (RecurrenceTypeEnum)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static RecurrenceTypeEnum fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}

}
