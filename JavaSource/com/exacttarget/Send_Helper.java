/**
 * Send_Helper.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class Send_Helper {
    // Type metadata
    private static final com.ibm.ws.webservices.engine.description.TypeDesc typeDesc =
        new com.ibm.ws.webservices.engine.description.TypeDesc(Send.class);

    static {
        typeDesc.setOption("buildNum","cf131037.05");
        com.ibm.ws.webservices.engine.description.FieldDesc field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("email");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Email"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Email"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("list");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "List"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "List"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sendDate");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendDate"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("fromAddress");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FromAddress"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("fromName");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "FromName"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("duplicates");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Duplicates"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("invalidAddresses");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "InvalidAddresses"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("existingUndeliverables");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExistingUndeliverables"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("existingUnsubscribes");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ExistingUnsubscribes"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("hardBounces");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "HardBounces"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("softBounces");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SoftBounces"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("otherBounces");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "OtherBounces"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("forwardedEmails");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "ForwardedEmails"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("uniqueClicks");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UniqueClicks"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("uniqueOpens");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "UniqueOpens"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberSent");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberSent"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberDelivered");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberDelivered"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("unsubscribes");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Unsubscribes"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("missingAddresses");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "MissingAddresses"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("subject");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Subject"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("previewURL");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "PreviewURL"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("links");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Links"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Link"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("events");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Events"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "TrackingEvent"));
        field.setMinOccursIs0(true);
        field.setMaxOccurs(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sentDate");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SentDate"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("emailName");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailName"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("status");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Status"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("isMultipart");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "IsMultipart"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sendLimit");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendLimit"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sendWindowOpen");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendWindowOpen"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "time"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sendWindowClose");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "SendWindowClose"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "time"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("isAlwaysOn");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "IsAlwaysOn"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "boolean"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("sources");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Sources"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", ">Send>Sources"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberTargeted");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberTargeted"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberErrored");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberErrored"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("numberExcluded");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "NumberExcluded"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "int"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("additional");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "Additional"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("bccEmail");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "BccEmail"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://www.w3.org/2001/XMLSchema", "string"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
        field = new com.ibm.ws.webservices.engine.description.ElementDesc();
        field.setFieldName("emailSendDefinition");
        field.setXmlName(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailSendDefinition"));
        field.setXmlType(com.ibm.ws.webservices.engine.utils.QNameTable.createQName("http://exacttarget.com/wsdl/partnerAPI", "EmailSendDefinition"));
        field.setMinOccursIs0(true);
        typeDesc.addFieldDesc(field);
    };

    /**
     * Return type metadata object
     */
    public static com.ibm.ws.webservices.engine.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Send_Ser(
            javaType, xmlType, typeDesc);
    };

    /**
     * Get Custom Deserializer
     */
    public static com.ibm.ws.webservices.engine.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class javaType,  
           javax.xml.namespace.QName xmlType) {
        return 
          new Send_Deser(
            javaType, xmlType, typeDesc);
    };

}
