/**
 * MessagingConfiguration_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class MessagingConfiguration_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public MessagingConfiguration_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_570;
           context.qName2String(elemQName, true);
           elemQName = QName_0_571;
           context.qName2String(elemQName, true);
           elemQName = QName_0_187;
           context.qName2String(elemQName, true);
           elemQName = QName_0_572;
           context.qName2String(elemQName, true);
           elemQName = QName_0_573;
           context.qName2String(elemQName, true);
           elemQName = QName_0_351;
           context.qName2String(elemQName, true);
           elemQName = QName_0_574;
           context.qName2String(elemQName, true);
           elemQName = QName_0_575;
           context.qName2String(elemQName, true);
           elemQName = QName_0_576;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        MessagingConfiguration bean = (MessagingConfiguration) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_570;
          propValue = bean.getCode();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_571;
          propValue = bean.getMessagingVendorKind();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_571,
              true,null,context);
          propQName = QName_0_187;
          propValue = new java.lang.Boolean(bean.isIsActive());
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              true,null,context);
          propQName = QName_0_572;
          propValue = bean.getUrl();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_573;
          propValue = bean.getUserName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_351;
          propValue = bean.getPassword();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_574;
          propValue = bean.getProfileID();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              true,null,context);
          }
          propQName = QName_0_575;
          propValue = bean.getCallbackUrl();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_576;
          propValue = bean.getMediaTypes();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_574 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ProfileID");
    private final static javax.xml.namespace.QName QName_0_187 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsActive");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_576 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MediaTypes");
    private final static javax.xml.namespace.QName QName_0_575 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CallbackUrl");
    private final static javax.xml.namespace.QName QName_0_573 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UserName");
    private final static javax.xml.namespace.QName QName_0_570 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Code");
    private final static javax.xml.namespace.QName QName_0_571 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "MessagingVendorKind");
    private final static javax.xml.namespace.QName QName_0_351 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Password");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_572 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Url");
}
