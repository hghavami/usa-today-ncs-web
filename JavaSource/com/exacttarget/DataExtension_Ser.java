/**
 * DataExtension_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class DataExtension_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public DataExtension_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_20;
           context.qName2String(elemQName, true);
           elemQName = QName_0_134;
           context.qName2String(elemQName, true);
           elemQName = QName_0_484;
           context.qName2String(elemQName, true);
           elemQName = QName_0_485;
           context.qName2String(elemQName, true);
           elemQName = QName_0_486;
           context.qName2String(elemQName, true);
           elemQName = QName_0_487;
           context.qName2String(elemQName, true);
           elemQName = QName_0_488;
           context.qName2String(elemQName, true);
           elemQName = QName_0_489;
           context.qName2String(elemQName, true);
           elemQName = QName_0_490;
           context.qName2String(elemQName, true);
           elemQName = QName_0_491;
           context.qName2String(elemQName, true);
           elemQName = QName_0_492;
           context.qName2String(elemQName, true);
           elemQName = QName_0_493;
           context.qName2String(elemQName, true);
           elemQName = QName_0_494;
           context.qName2String(elemQName, true);
           elemQName = QName_0_495;
           context.qName2String(elemQName, true);
           elemQName = QName_0_496;
           context.qName2String(elemQName, true);
           elemQName = QName_0_182;
           context.qName2String(elemQName, true);
           elemQName = QName_0_150;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        DataExtension bean = (DataExtension) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_20;
          propValue = bean.getName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_134;
          propValue = bean.getDescription();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_484;
          propValue = bean.getIsSendable();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_485;
          propValue = bean.getIsTestable();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_486;
          propValue = bean.getSendableDataExtensionField();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_497,
              false,null,context);
          propQName = QName_0_487;
          propValue = bean.getSendableSubscriberField();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_208,
              false,null,context);
          propQName = QName_0_488;
          propValue = bean.getTemplate();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_498,
              false,null,context);
          propQName = QName_0_489;
          propValue = bean.getDataRetentionPeriodLength();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_490;
          propValue = bean.getDataRetentionPeriodUnitOfMeasure();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_14,
              false,null,context);
          propQName = QName_0_491;
          propValue = bean.getRowBasedRetention();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_492;
          propValue = bean.getResetRetentionPeriodOnImport();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_493;
          propValue = bean.getDeleteAtEndOfRetentionPeriod();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_494;
          propValue = bean.getRetainUntil();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_495;
          propValue = bean.getFields();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_499,
              false,null,context);
          propQName = QName_0_496;
          propValue = bean.getDataRetentionPeriod();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_500,
              false,null,context);
          propQName = QName_0_182;
          propValue = bean.getCategoryID();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_314,
              false,null,context);
          propQName = QName_0_150;
          propValue = bean.getStatus();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
        }
    }
    private final static javax.xml.namespace.QName QName_0_484 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsSendable");
    private final static javax.xml.namespace.QName QName_0_494 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RetainUntil");
    private final static javax.xml.namespace.QName QName_0_208 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Attribute");
    private final static javax.xml.namespace.QName QName_0_497 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataExtensionField");
    private final static javax.xml.namespace.QName QName_0_500 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DateTimeUnitOfMeasure");
    private final static javax.xml.namespace.QName QName_0_489 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodLength");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_487 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendableSubscriberField");
    private final static javax.xml.namespace.QName QName_1_314 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "long");
    private final static javax.xml.namespace.QName QName_0_493 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DeleteAtEndOfRetentionPeriod");
    private final static javax.xml.namespace.QName QName_0_488 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Template");
    private final static javax.xml.namespace.QName QName_0_150 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Status");
    private final static javax.xml.namespace.QName QName_0_485 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "IsTestable");
    private final static javax.xml.namespace.QName QName_0_486 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SendableDataExtensionField");
    private final static javax.xml.namespace.QName QName_0_498 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataExtensionTemplate");
    private final static javax.xml.namespace.QName QName_0_182 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "CategoryID");
    private final static javax.xml.namespace.QName QName_1_14 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "int");
    private final static javax.xml.namespace.QName QName_0_496 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriod");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_492 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ResetRetentionPeriodOnImport");
    private final static javax.xml.namespace.QName QName_0_491 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RowBasedRetention");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_499 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  ">DataExtension>Fields");
    private final static javax.xml.namespace.QName QName_0_495 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Fields");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_490 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodUnitOfMeasure");
}
