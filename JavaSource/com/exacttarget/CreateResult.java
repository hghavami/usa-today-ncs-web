/**
 * CreateResult.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class CreateResult  extends com.exacttarget.Result  {
    private int newID;
    private java.lang.String newObjectID;
    private java.lang.String partnerKey;
    private com.exacttarget.APIObject object;
    private com.exacttarget.CreateResult[] createResults;
    private java.lang.String parentPropertyName;

    public CreateResult() {
    }

    public int getNewID() {
        return newID;
    }

    public void setNewID(int newID) {
        this.newID = newID;
    }

    public java.lang.String getNewObjectID() {
        return newObjectID;
    }

    public void setNewObjectID(java.lang.String newObjectID) {
        this.newObjectID = newObjectID;
    }

    public java.lang.String getPartnerKey() {
        return partnerKey;
    }

    public void setPartnerKey(java.lang.String partnerKey) {
        this.partnerKey = partnerKey;
    }

    public com.exacttarget.APIObject getObject() {
        return object;
    }

    public void setObject(com.exacttarget.APIObject object) {
        this.object = object;
    }

    public com.exacttarget.CreateResult[] getCreateResults() {
        return createResults;
    }

    public void setCreateResults(com.exacttarget.CreateResult[] createResults) {
        this.createResults = createResults;
    }

    public com.exacttarget.CreateResult getCreateResults(int i) {
        return this.createResults[i];
    }

    public void setCreateResults(int i, com.exacttarget.CreateResult value) {
        this.createResults[i] = value;
    }

    public java.lang.String getParentPropertyName() {
        return parentPropertyName;
    }

    public void setParentPropertyName(java.lang.String parentPropertyName) {
        this.parentPropertyName = parentPropertyName;
    }

}
