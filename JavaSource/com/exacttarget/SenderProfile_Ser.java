/**
 * SenderProfile_Ser.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf131037.05 v92410165505
 */

package com.exacttarget;

public class SenderProfile_Ser extends com.exacttarget.APIObject_Ser {
    /**
     * Constructor
     */
    public SenderProfile_Ser(
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType, 
           com.ibm.ws.webservices.engine.description.TypeDesc _typeDesc) {
        super(_javaType, _xmlType, _typeDesc);
    }
    public void serialize(
        javax.xml.namespace.QName name,
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        context.startElement(name, addAttributes(attributes, value, context));
        addElements(value, context);
        context.endElement();
    }
    protected org.xml.sax.Attributes addAttributes(
        org.xml.sax.Attributes attributes,
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        attributes = super.addAttributes(attributes, value, context);
           javax.xml.namespace.QName
           elemQName = QName_0_20;
           context.qName2String(elemQName, true);
           elemQName = QName_0_134;
           context.qName2String(elemQName, true);
           elemQName = QName_0_22;
           context.qName2String(elemQName, true);
           elemQName = QName_0_23;
           context.qName2String(elemQName, true);
           elemQName = QName_0_542;
           context.qName2String(elemQName, true);
           elemQName = QName_0_543;
           context.qName2String(elemQName, true);
           elemQName = QName_0_544;
           context.qName2String(elemQName, true);
           elemQName = QName_0_545;
           context.qName2String(elemQName, true);
           elemQName = QName_0_546;
           context.qName2String(elemQName, true);
           elemQName = QName_0_547;
           context.qName2String(elemQName, true);
           elemQName = QName_0_548;
           context.qName2String(elemQName, true);
           elemQName = QName_0_549;
           context.qName2String(elemQName, true);
           elemQName = QName_0_550;
           context.qName2String(elemQName, true);
           elemQName = QName_0_489;
           context.qName2String(elemQName, true);
           elemQName = QName_0_490;
           context.qName2String(elemQName, true);
           elemQName = QName_0_551;
           context.qName2String(elemQName, true);
        return attributes;
    }
    protected void addElements(
        java.lang.Object value,
        com.ibm.ws.webservices.engine.encoding.SerializationContext context)
        throws java.io.IOException
    {
        super.addElements(value, context);
        SenderProfile bean = (SenderProfile) value;
        java.lang.Object propValue;
        javax.xml.namespace.QName propQName;
        {
          propQName = QName_0_20;
          propValue = bean.getName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_134;
          propValue = bean.getDescription();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_22;
          propValue = bean.getFromName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_23;
          propValue = bean.getFromAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_542;
          propValue = bean.getUseDefaultRMMRules();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_543;
          propValue = bean.getAutoForwardToEmailAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_544;
          propValue = bean.getAutoForwardToName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_545;
          propValue = bean.getDirectForward();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_546;
          propValue = bean.getAutoForwardTriggeredSend();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_506,
              false,null,context);
          propQName = QName_0_547;
          propValue = bean.getAutoReply();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_31,
              false,null,context);
          propQName = QName_0_548;
          propValue = bean.getAutoReplyTriggeredSend();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_506,
              false,null,context);
          propQName = QName_0_549;
          propValue = bean.getSenderHeaderEmailAddress();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_550;
          propValue = bean.getSenderHeaderName();
          if (propValue != null && !context.shouldSendXSIType()) {
            context.simpleElement(propQName, null, propValue.toString()); 
          } else {
            serializeChild(propQName, null, 
              propValue, 
              QName_1_11,
              false,null,context);
          }
          propQName = QName_0_489;
          propValue = bean.getDataRetentionPeriodLength();
          serializeChild(propQName, null, 
              propValue, 
              QName_1_481,
              false,null,context);
          propQName = QName_0_490;
          propValue = bean.getDataRetentionPeriodUnitOfMeasure();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_235,
              false,null,context);
          propQName = QName_0_551;
          propValue = bean.getReplyManagementRuleSet();
          serializeChild(propQName, null, 
              propValue, 
              QName_0_32,
              false,null,context);
        }
    }
    private final static javax.xml.namespace.QName QName_0_550 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SenderHeaderName");
    private final static javax.xml.namespace.QName QName_0_542 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "UseDefaultRMMRules");
    private final static javax.xml.namespace.QName QName_0_22 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromName");
    private final static javax.xml.namespace.QName QName_0_545 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DirectForward");
    private final static javax.xml.namespace.QName QName_0_489 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodLength");
    private final static javax.xml.namespace.QName QName_0_547 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReply");
    private final static javax.xml.namespace.QName QName_0_134 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Description");
    private final static javax.xml.namespace.QName QName_0_23 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "FromAddress");
    private final static javax.xml.namespace.QName QName_0_546 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardTriggeredSend");
    private final static javax.xml.namespace.QName QName_0_551 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "ReplyManagementRuleSet");
    private final static javax.xml.namespace.QName QName_0_548 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoReplyTriggeredSend");
    private final static javax.xml.namespace.QName QName_1_11 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "string");
    private final static javax.xml.namespace.QName QName_0_543 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardToEmailAddress");
    private final static javax.xml.namespace.QName QName_0_544 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "AutoForwardToName");
    private final static javax.xml.namespace.QName QName_1_31 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "boolean");
    private final static javax.xml.namespace.QName QName_0_235 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "RecurrenceTypeEnum");
    private final static javax.xml.namespace.QName QName_0_549 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "SenderHeaderEmailAddress");
    private final static javax.xml.namespace.QName QName_1_481 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://www.w3.org/2001/XMLSchema",
                  "short");
    private final static javax.xml.namespace.QName QName_0_20 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "Name");
    private final static javax.xml.namespace.QName QName_0_32 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "APIObject");
    private final static javax.xml.namespace.QName QName_0_490 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "DataRetentionPeriodUnitOfMeasure");
    private final static javax.xml.namespace.QName QName_0_506 = 
           com.ibm.ws.webservices.engine.utils.QNameTable.createQName(
                  "http://exacttarget.com/wsdl/partnerAPI",
                  "TriggeredSendDefinition");
}
