/**
 * TriggeredSendDefinition.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class TriggeredSendDefinition  extends com.exacttarget.SendDefinition  {
    private com.exacttarget.TriggeredSendTypeEnum triggeredSendType;
    private com.exacttarget.TriggeredSendStatusEnum triggeredSendStatus;
    private com.exacttarget.Email email;
    private com.exacttarget.List list;
    private java.lang.Boolean autoAddSubscribers;
    private java.lang.Boolean autoUpdateSubscribers;
    private java.lang.Integer batchInterval;
    private java.lang.String bccEmail;
    private java.lang.String emailSubject;
    private java.lang.String dynamicEmailSubject;
    private java.lang.Boolean isMultipart;
    private java.lang.Boolean isWrapped;
    private java.lang.Short allowedSlots;
    private java.lang.Integer newSlotTrigger;
    private java.lang.Integer sendLimit;
    private java.util.Calendar sendWindowOpen;
    private java.util.Calendar sendWindowClose;
    private java.lang.Boolean sendWindowDelete;
    private java.lang.Boolean refreshContent;
    private java.lang.String exclusionFilter;
    private java.lang.String priority;
    private java.lang.String sendSourceCustomerKey;
    private com.exacttarget.TriggeredSendExclusionList[] exclusionListCollection;
    private java.lang.String CCEmail;
    private com.exacttarget.DataExtension sendSourceDataExtension;
    private java.lang.Boolean isAlwaysOn;
    private java.lang.Boolean disableOnEmailBuildError;

    public TriggeredSendDefinition() {
    }

    public com.exacttarget.TriggeredSendTypeEnum getTriggeredSendType() {
        return triggeredSendType;
    }

    public void setTriggeredSendType(com.exacttarget.TriggeredSendTypeEnum triggeredSendType) {
        this.triggeredSendType = triggeredSendType;
    }

    public com.exacttarget.TriggeredSendStatusEnum getTriggeredSendStatus() {
        return triggeredSendStatus;
    }

    public void setTriggeredSendStatus(com.exacttarget.TriggeredSendStatusEnum triggeredSendStatus) {
        this.triggeredSendStatus = triggeredSendStatus;
    }

    public com.exacttarget.Email getEmail() {
        return email;
    }

    public void setEmail(com.exacttarget.Email email) {
        this.email = email;
    }

    public com.exacttarget.List getList() {
        return list;
    }

    public void setList(com.exacttarget.List list) {
        this.list = list;
    }

    public java.lang.Boolean getAutoAddSubscribers() {
        return autoAddSubscribers;
    }

    public void setAutoAddSubscribers(java.lang.Boolean autoAddSubscribers) {
        this.autoAddSubscribers = autoAddSubscribers;
    }

    public java.lang.Boolean getAutoUpdateSubscribers() {
        return autoUpdateSubscribers;
    }

    public void setAutoUpdateSubscribers(java.lang.Boolean autoUpdateSubscribers) {
        this.autoUpdateSubscribers = autoUpdateSubscribers;
    }

    public java.lang.Integer getBatchInterval() {
        return batchInterval;
    }

    public void setBatchInterval(java.lang.Integer batchInterval) {
        this.batchInterval = batchInterval;
    }

    public java.lang.String getBccEmail() {
        return bccEmail;
    }

    public void setBccEmail(java.lang.String bccEmail) {
        this.bccEmail = bccEmail;
    }

    public java.lang.String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(java.lang.String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public java.lang.String getDynamicEmailSubject() {
        return dynamicEmailSubject;
    }

    public void setDynamicEmailSubject(java.lang.String dynamicEmailSubject) {
        this.dynamicEmailSubject = dynamicEmailSubject;
    }

    public java.lang.Boolean getIsMultipart() {
        return isMultipart;
    }

    public void setIsMultipart(java.lang.Boolean isMultipart) {
        this.isMultipart = isMultipart;
    }

    public java.lang.Boolean getIsWrapped() {
        return isWrapped;
    }

    public void setIsWrapped(java.lang.Boolean isWrapped) {
        this.isWrapped = isWrapped;
    }

    public java.lang.Short getAllowedSlots() {
        return allowedSlots;
    }

    public void setAllowedSlots(java.lang.Short allowedSlots) {
        this.allowedSlots = allowedSlots;
    }

    public java.lang.Integer getNewSlotTrigger() {
        return newSlotTrigger;
    }

    public void setNewSlotTrigger(java.lang.Integer newSlotTrigger) {
        this.newSlotTrigger = newSlotTrigger;
    }

    public java.lang.Integer getSendLimit() {
        return sendLimit;
    }

    public void setSendLimit(java.lang.Integer sendLimit) {
        this.sendLimit = sendLimit;
    }

    public java.util.Calendar getSendWindowOpen() {
        return sendWindowOpen;
    }

    public void setSendWindowOpen(java.util.Calendar sendWindowOpen) {
        this.sendWindowOpen = sendWindowOpen;
    }

    public java.util.Calendar getSendWindowClose() {
        return sendWindowClose;
    }

    public void setSendWindowClose(java.util.Calendar sendWindowClose) {
        this.sendWindowClose = sendWindowClose;
    }

    public java.lang.Boolean getSendWindowDelete() {
        return sendWindowDelete;
    }

    public void setSendWindowDelete(java.lang.Boolean sendWindowDelete) {
        this.sendWindowDelete = sendWindowDelete;
    }

    public java.lang.Boolean getRefreshContent() {
        return refreshContent;
    }

    public void setRefreshContent(java.lang.Boolean refreshContent) {
        this.refreshContent = refreshContent;
    }

    public java.lang.String getExclusionFilter() {
        return exclusionFilter;
    }

    public void setExclusionFilter(java.lang.String exclusionFilter) {
        this.exclusionFilter = exclusionFilter;
    }

    public java.lang.String getPriority() {
        return priority;
    }

    public void setPriority(java.lang.String priority) {
        this.priority = priority;
    }

    public java.lang.String getSendSourceCustomerKey() {
        return sendSourceCustomerKey;
    }

    public void setSendSourceCustomerKey(java.lang.String sendSourceCustomerKey) {
        this.sendSourceCustomerKey = sendSourceCustomerKey;
    }

    public com.exacttarget.TriggeredSendExclusionList[] getExclusionListCollection() {
        return exclusionListCollection;
    }

    public void setExclusionListCollection(com.exacttarget.TriggeredSendExclusionList[] exclusionListCollection) {
        this.exclusionListCollection = exclusionListCollection;
    }

    public com.exacttarget.TriggeredSendExclusionList getExclusionListCollection(int i) {
        return this.exclusionListCollection[i];
    }

    public void setExclusionListCollection(int i, com.exacttarget.TriggeredSendExclusionList value) {
        this.exclusionListCollection[i] = value;
    }

    public java.lang.String getCCEmail() {
        return CCEmail;
    }

    public void setCCEmail(java.lang.String CCEmail) {
        this.CCEmail = CCEmail;
    }

    public com.exacttarget.DataExtension getSendSourceDataExtension() {
        return sendSourceDataExtension;
    }

    public void setSendSourceDataExtension(com.exacttarget.DataExtension sendSourceDataExtension) {
        this.sendSourceDataExtension = sendSourceDataExtension;
    }

    public java.lang.Boolean getIsAlwaysOn() {
        return isAlwaysOn;
    }

    public void setIsAlwaysOn(java.lang.Boolean isAlwaysOn) {
        this.isAlwaysOn = isAlwaysOn;
    }

    public java.lang.Boolean getDisableOnEmailBuildError() {
        return disableOnEmailBuildError;
    }

    public void setDisableOnEmailBuildError(java.lang.Boolean disableOnEmailBuildError) {
        this.disableOnEmailBuildError = disableOnEmailBuildError;
    }

}
