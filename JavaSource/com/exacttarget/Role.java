/**
 * Role.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class Role  extends com.exacttarget.APIObject  {
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.Boolean isPrivate;
    private java.lang.Boolean isSystemDefined;
    private java.lang.Boolean forceInheritance;
    private com.exacttarget.PermissionSet[] permissionSets;
    private com.exacttarget.Permission[] permissions;

    public Role() {
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    public java.lang.Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(java.lang.Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public java.lang.Boolean getIsSystemDefined() {
        return isSystemDefined;
    }

    public void setIsSystemDefined(java.lang.Boolean isSystemDefined) {
        this.isSystemDefined = isSystemDefined;
    }

    public java.lang.Boolean getForceInheritance() {
        return forceInheritance;
    }

    public void setForceInheritance(java.lang.Boolean forceInheritance) {
        this.forceInheritance = forceInheritance;
    }

    public com.exacttarget.PermissionSet[] getPermissionSets() {
        return permissionSets;
    }

    public void setPermissionSets(com.exacttarget.PermissionSet[] permissionSets) {
        this.permissionSets = permissionSets;
    }

    public com.exacttarget.Permission[] getPermissions() {
        return permissions;
    }

    public void setPermissions(com.exacttarget.Permission[] permissions) {
        this.permissions = permissions;
    }

}
