/**
 * ClientID.java
 *
 * This file was auto-generated from WSDL
 * by the IBM Web services WSDL2Java emitter.
 * cf250920.22 v6109135024
 */

package com.exacttarget;

public class ClientID  {
    private java.lang.Integer clientID;
    private java.lang.Integer ID;
    private java.lang.String partnerClientKey;
    private java.lang.Integer userID;
    private java.lang.String partnerUserKey;
    private java.lang.Integer createdBy;
    private java.lang.Integer modifiedBy;
    private java.lang.Long enterpriseID;
    private java.lang.String customerKey;

    public ClientID() {
    }

    public java.lang.Integer getClientID() {
        return clientID;
    }

    public void setClientID(java.lang.Integer clientID) {
        this.clientID = clientID;
    }

    public java.lang.Integer getID() {
        return ID;
    }

    public void setID(java.lang.Integer ID) {
        this.ID = ID;
    }

    public java.lang.String getPartnerClientKey() {
        return partnerClientKey;
    }

    public void setPartnerClientKey(java.lang.String partnerClientKey) {
        this.partnerClientKey = partnerClientKey;
    }

    public java.lang.Integer getUserID() {
        return userID;
    }

    public void setUserID(java.lang.Integer userID) {
        this.userID = userID;
    }

    public java.lang.String getPartnerUserKey() {
        return partnerUserKey;
    }

    public void setPartnerUserKey(java.lang.String partnerUserKey) {
        this.partnerUserKey = partnerUserKey;
    }

    public java.lang.Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(java.lang.Integer createdBy) {
        this.createdBy = createdBy;
    }

    public java.lang.Integer getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(java.lang.Integer modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public java.lang.Long getEnterpriseID() {
        return enterpriseID;
    }

    public void setEnterpriseID(java.lang.Long enterpriseID) {
        this.enterpriseID = enterpriseID;
    }

    public java.lang.String getCustomerKey() {
        return customerKey;
    }

    public void setCustomerKey(java.lang.String customerKey) {
        this.customerKey = customerKey;
    }

}
