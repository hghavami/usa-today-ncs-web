/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.transferObjects;

import java.io.Serializable;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketTO
 * 
 * 
 */
public class MarketTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7360367302053980902L;
	private long id = 0;
    private String marketID = null;
    private String description = null;
    private String phone = null;
    /**
     * 
     */
    public MarketTO() {
        super();
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return Returns the id.
     */
    public long getId() {
        return this.id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return Returns the marketID.
     */
    public String getMarketID() {
        return this.marketID;
    }
    /**
     * @param marketID The marketID to set.
     */
    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @return Returns the phone.
     */
    public String getPhone() {
        return this.phone;
    }
    /**
     * @param phone The phone to set.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
