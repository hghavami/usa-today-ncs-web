/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.transferObjects;

import java.io.Serializable;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketContactTO
 * 
 * 
 */
public class MarketContactTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 46520689514080676L;
	private long id = 0;
    private long owningMarketKey = 0;
    private boolean active = true;
    private boolean primaryContact = false;
    private String firstName = "";
    private String lastName = "";
    private String phone = "";
    private String phoneExtension = "";
    private String email = "";
    /**
     * 
     */
    public MarketContactTO() {
        super();
    }

    /**
     * @return Returns the active.
     */
    public boolean isActive() {
        return this.active;
    }
    /**
     * @param active The active to set.
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return this.email;
    }
    /**
     * @param email The email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * @return Returns the firstName.
     */
    public String getFirstName() {
        return this.firstName;
    }
    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return Returns the id.
     */
    public long getId() {
        return this.id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return Returns the lastName.
     */
    public String getLastName() {
        return this.lastName;
    }
    /**
     * @param lastName The lastName to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return Returns the owningMarketKey.
     */
    public long getOwningMarketKey() {
        return this.owningMarketKey;
    }
    /**
     * @param owningMarketKey The owningMarketKey to set.
     */
    public void setOwningMarketKey(long owningMarketKey) {
        this.owningMarketKey = owningMarketKey;
    }
    /**
     * @return Returns the phone.
     */
    public String getPhone() {
        return this.phone;
    }
    /**
     * @param phone The phone to set.
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    /**
     * @return Returns the phoneExtension.
     */
    public String getPhoneExtension() {
        return this.phoneExtension;
    }
    /**
     * @param phoneExtension The phoneExtension to set.
     */
    public void setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
    }
    /**
     * @return Returns the primaryContact.
     */
    public boolean isPrimaryContact() {
        return this.primaryContact;
    }
    /**
     * @param primaryContact The primaryContact to set.
     */
    public void setPrimaryContact(boolean primaryContact) {
        this.primaryContact = primaryContact;
    }
}
