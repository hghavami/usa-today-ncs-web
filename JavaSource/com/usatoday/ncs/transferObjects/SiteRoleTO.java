package com.usatoday.ncs.transferObjects;

import java.io.Serializable;

public class SiteRoleTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	long siteID = 0;
	String roleID = null;
	
	
	public String getRoleID() {
		return roleID;
	}
	public void setRoleID(String roleID) {
		this.roleID = roleID;
	}
	public long getSiteID() {
		return siteID;
	}
	public void setSiteID(long siteID) {
		this.siteID = siteID;
	}
	
	
}
