/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.transferObjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author aeast
 * @date Nov 6, 2007
 * @class CallCenterSiteTO
 * 
 * 
 */
public class CallCenterSiteTO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6314138782905958691L;
	private long key = 0;
    private boolean active = true;
    private boolean usatLocation = false;
    
    private String siteID = null;
    private String password = null;
    
    private String ipRestrictionLower = null;
    private String ipRestrictionUpper = null;
    
    private String description = null;
    
    private Collection <SiteRoleTO> roles = null;
    
    /**
     * 
     */
    public CallCenterSiteTO() {
        super();
    }

    /**
     * @return Returns the active.
     */
    public boolean isActive() {
        return this.active;
    }
    /**
     * @param active The active to set.
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Returns the key.
     */
    public long getKey() {
        return this.key;
    }
    /**
     * @param key The key to set.
     */
    public void setKey(long key) {
        this.key = key;
    }
    /**
     * @return Returns the password.
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
    /**
     * @return Returns the siteID.
     */
    public String getSiteID() {
        return this.siteID;
    }
    /**
     * @param siteID The siteID to set.
     */
    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }
    /**
     * @return Returns the ipRestrictionLower.
     */
    public String getIpRestrictionLower() {
        return this.ipRestrictionLower;
    }
    /**
     * @param ipRestrictionLower The ipRestrictionLower to set.
     */
    public void setIpRestrictionLower(String ipRestrictionLower) {
        this.ipRestrictionLower = ipRestrictionLower;
    }
    /**
     * @return Returns the ipRestrictionUpper.
     */
    public String getIpRestrictionUpper() {
        return this.ipRestrictionUpper;
    }
    /**
     * @param ipRestrictionUpper The ipRestrictionUpper to set.
     */
    public void setIpRestrictionUpper(String ipRestrictionUpper) {
        this.ipRestrictionUpper = ipRestrictionUpper;
    }
    /**
     * @return Returns the usatLocation.
     */
    public boolean isUsatLocation() {
        return this.usatLocation;
    }
    /**
     * @param usatLocation The usatLocation to set.
     */
    public void setUsatLocation(boolean usatLocation) {
        this.usatLocation = usatLocation;
    }

	public Collection<SiteRoleTO> getRoles() {
		if (roles == null) {
			roles = new ArrayList<SiteRoleTO>();
		}
		return roles;
	}

	public void setRoles(Collection<SiteRoleTO> roles) {
		if (this.roles != null) {
			this.roles.clear();
		}
		this.roles = roles;
	}

	
}
