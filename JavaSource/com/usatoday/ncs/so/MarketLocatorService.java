/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.so;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.ncs.bo.MarketBO;
import com.usatoday.ncs.integration.MarketDAO;
import com.usatoday.ncs.util.ZeroFilledNumeric;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketLocatorService
 * 
 * This service class can be used to locate the market for a particular zip code.
 * Markets are refreshed every day from the database.
 * 
 */
public class MarketLocatorService {

    private static HashMap<String, MarketBO> markets = new HashMap<String, MarketBO>(); 
    private static DateTime dateLoaded = null;
    private static DateTime nextReload = null;
    /**
     * 
     */
    public MarketLocatorService() {
        super();
        
        
        if (MarketLocatorService.markets.size() == 0) {
	        try {
	            Collection<MarketBO> m = MarketBO.fetchMarkets();
	            
	            Iterator<MarketBO> itr = m.iterator();
	            while(itr.hasNext()) {
	               MarketBO market = itr.next();
	               MarketLocatorService.markets.put(market.getMarketID().trim(), market);
	            }
	            MarketLocatorService.dateLoaded = new DateTime();
	            MarketLocatorService.nextReload = MarketLocatorService.dateLoaded.plusDays(1);
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
        }
    }

    public MarketBO locateMarketByZip(String zipCode) throws Exception {
        MarketBO market = null;
        
        if (zipCode != null && zipCode.trim().length() == 5) {

            DateTime now = new DateTime();
            
            if (MarketLocatorService.dateLoaded == null || now.isAfter(MarketLocatorService.nextReload)) {
                MarketLocatorService.markets.clear();
	            Collection<MarketBO> m = MarketBO.fetchMarkets();
	            
	            Iterator<MarketBO> itr = m.iterator();
	            while(itr.hasNext()) {
	               MarketBO tempMarket = itr.next();
	               MarketLocatorService.markets.put(tempMarket.getMarketID().trim(), tempMarket);
	            }
	            MarketLocatorService.dateLoaded = new DateTime();
	            MarketLocatorService.nextReload = MarketLocatorService.dateLoaded.plusDays(1);
            }
            MarketDAO dao = new MarketDAO();
            String mID = dao.getMarketIDByZip(zipCode.trim());
            
            if (mID != null) {
                mID = mID.trim();
                mID = ZeroFilledNumeric.getValue(2, mID);
                
                market = MarketLocatorService.markets.get(mID);
                
                if (market == null) {
                    market = MarketLocatorService.markets.get("99"); // get national
                }
            }
            else {
                // use default market
                //System.out.println("No Market for Zip: " + zipCode);
                market = MarketLocatorService.markets.get("99");
            }
        }
        else {
            throw new Exception("Invalid Zip Code: " + zipCode);
        }
        return market;
    }
}
