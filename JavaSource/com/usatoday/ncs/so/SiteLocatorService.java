/*
 * Created on Dec 18, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.so;

import java.util.HashMap;

import org.joda.time.DateTime;

import com.usatoday.ncs.bo.SiteBO;

/**
 * @author aeast
 * @date Dec 18, 2007
 * @class SiteLocatorService
 * 
 * This service class can be used to locate a NCS site.
 * 
 */
public class SiteLocatorService {

    private static HashMap<Long, SiteBO> sites = null; 
    private static DateTime dateLoaded = null;
    private static DateTime nextReload = null;
    /**
     * 
     */
    public SiteLocatorService() {
        super();
        if (SiteLocatorService.sites == null || SiteLocatorService.sites.size() == 0) {
	        try {
	            SiteLocatorService.sites = SiteBO.getAllSites();
	            
	            SiteLocatorService.dateLoaded = new DateTime();
	            SiteLocatorService.nextReload = dateLoaded.plusDays(1);
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
        }
    }

    public SiteBO getCachedSite(long siteKey) throws Exception {

        DateTime now = new DateTime();
        
        if (SiteLocatorService.dateLoaded == null || now.isAfter(SiteLocatorService.nextReload)) {
            SiteLocatorService.sites = SiteBO.getAllSites();
            
            SiteLocatorService.dateLoaded = new DateTime();
            SiteLocatorService.nextReload = dateLoaded.plusDays(1);
        }

        Long key = new Long(siteKey);
        SiteBO s = (SiteBO)SiteLocatorService.sites.get(key);
        
        if (s == null) {
            // check db for site
            s = SiteBO.fetchSite(siteKey);
            if (s != null) {
                SiteLocatorService.sites.put(key, s);
            }
        }
        return s;
    }
}
