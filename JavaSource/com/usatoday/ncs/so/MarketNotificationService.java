/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.so;

import java.util.Iterator;

import org.joda.time.DateTime;

import com.usatoday.ncs.bo.IncidentBO;
import com.usatoday.ncs.bo.MarketBO;
import com.usatoday.ncs.bo.SiteBO;
import com.usatoday.ncs.transferObjects.MarketContactTO;
import com.usatoday.ncs.util.SmtpMailSender;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketNotificationService
 * 
 * Handles email notfications
 * 
 */
public class MarketNotificationService {

    private static final String PROD_EMAIL_SENDER = "incorres@usatoday.com"; 
    private static final String DEV_EMAIL_SENDER = "aeast@usatoday.com"; 
    private static String EMAIL_SENDER = null;
    
    /**
     * 
     */
    public MarketNotificationService() {
        super();

        if (MarketNotificationService.EMAIL_SENDER == null) {
	        try {
	            java.net.InetAddress serverAddress = java.net.InetAddress.getLocalHost();
	            String serverName = serverAddress.getHostName();
	            serverName = serverName.toUpperCase();
			
	            // if running on prod
	            if (serverName.indexOf("MOC-") > -1) {
	                MarketNotificationService.EMAIL_SENDER = MarketNotificationService.PROD_EMAIL_SENDER;
	            }
	            else {
	                MarketNotificationService.EMAIL_SENDER = MarketNotificationService.DEV_EMAIL_SENDER;
	            }
	        }
	        catch (Exception e) {
	            System.out.println("NCS WEB: Failed to init MarketNotificationService: " + e.getMessage());
	        }
        }
    }

    public void notifyMarket(IncidentBO incident, MarketBO market) {
        
        MarketBO m = incident.getMarketAssigned();
        
        // if market not set on incident check parameter
        if (m == null) {
            m = market;
        }
        
        if (m != null && m.getContacts().size() > 0) {
            
            try {
                SmtpMailSender mail = new SmtpMailSender();
            
                mail.setSender(MarketNotificationService.EMAIL_SENDER);
                
                String siteDescription = "UNKNOWN ORIGIN";
                try {
                    SiteLocatorService siteService = new SiteLocatorService();
                    SiteBO site = siteService.getCachedSite(incident.getNcsSiteKey());
                    siteDescription = site.getDescription();
                }
                catch (Exception exp) {
                    // ignore
                }
                StringBuffer msgSubject = new StringBuffer(incident.getIncidentType().toUpperCase());
                msgSubject.append(" Request from '" + siteDescription + "', ID#: ");
                msgSubject.append(incident.getIdAsString());
                
                mail.setMessageSubject(msgSubject.toString());
                
                Iterator<MarketContactTO> itr = m.getContacts().iterator();
                
                while (itr.hasNext()) {
                    MarketContactTO contact = itr.next();
                    String emailAddr = contact.getEmail();
                    if (emailAddr != null && emailAddr.length() > 0) {
                        mail.addTORecipient(emailAddr);
                    }
                }
                
                String emailBody = this.generateEmailBody(incident, m);
                mail.setMessageText(emailBody);
                
                mail.sendMessage();
            }
            catch (Exception e) {
                System.out.println("Failed to Send Market Notification: " + e.getMessage());
                System.out.println(incident.toString());
            }
            
        }
        
    }
    
    private String generateEmailBody(IncidentBO incident, MarketBO m) {
        
        StringBuffer body = new StringBuffer("#\n");
        try {
	        body.append("#  NCS Request for Market ID: ").append(m.getMarketID()).append(", '").append(m.getDescription().trim()).append("'\n");
	        body.append("#\n");
	        DateTime tempDate = new DateTime(incident.getIncidentDate());
	        body.append("Incident ID:        ").append(incident.getIdAsString()).append("\n");
	        body.append("Request Date:       ").append(tempDate.toString("MM/dd/yyyy")).append("\n");
	        body.append("Service Category:   ").append(incident.getIncidentType().toUpperCase()).append("\n");
	        try {
	        	body.append("Request Type:       ").append(incident.getIncidentRequestType().toUpperCase()).append("\n");
	        }
	        catch (Exception e) {
	        	// no request type set
	        	body.append("[no request type selected]\n");
			}
	        body.append("CSP:                ").append(incident.getEnteredBy()).append("\n");
	        body.append("Location Name:      ").append(incident.getIncidentLocationName()).append("\n");
	        body.append("Address:            ").append(incident.getIncidentAddress()).append("\n");
	        body.append("City:               ").append(incident.getIncidentCity()).append("\n");
	        body.append("State:              ").append(incident.getIncidentState()).append("\n");
	        body.append("ZIP:                ").append(incident.getIncidentZip()).append("\n");
	        body.append("Comments:           ").append(incident.getNotes().toUpperCase());
	        body.append("\n#\n");
	        body.append("# Customer Name and Address ").append("\n");
	        body.append("# ==========================").append("\n");
	        body.append("Name:           ").append(incident.getCustomerFirstName()).append(" ").append(incident.getCustomerLastName()).append("\n");
	        body.append("Adress1:        ").append(incident.getCustomerAddress1()).append("\n");
	        if (incident.getCustomerAddress2() != null && incident.getCustomerAddress2().trim().length() > 0) {
	            body.append("Address2:        ").append(incident.getCustomerAddress2()).append("\n");            
	        }
	        body.append("City/State/Zip: ").append(incident.getCustomerCity()).append(", ").append(incident.getCustomerState()).append(" ").append(incident.getCustomerZip()).append("\n");
	        body.append("Phone:          ").append(incident.getCustomerPhone()).append("\n");
	        body.append("Email:          ").append(incident.getCustomerEmail()).append("\n");
	        if (incident.getCustomerAccountNumber() != null && incident.getCustomerAccountNumber().trim().length() > 0) {
	            body.append("Account Number: ").append(incident.getCustomerAccountNumber()).append("\n");
	        }
        }
        catch (Exception e) {
        	body.append("\n Problem constructing email body..check SharePoint Site. Incident ID: ").append(incident.getIdAsString());
		}
        return body.toString();
    }
}
