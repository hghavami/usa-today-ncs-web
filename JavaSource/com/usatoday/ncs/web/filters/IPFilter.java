package com.usatoday.ncs.web.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.usatoday.ncs.web.handlers.UserHandler;

public class IPFilter implements Filter {
	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public IPFilter() {
		super();
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#init(FilterConfig arg0)
	 */
	public void init(FilterConfig arg0) throws ServletException {
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	    HttpServletRequest req = (HttpServletRequest)request;
		UserHandler userh = (UserHandler)req.getSession().getAttribute("user");
		
		if (userh == null || userh.getUser() == null || !userh.getUser().isAuthenticated()) {
		    // redirect to login page
		    if (userh != null) {
		        userh.setUserIPValidated(false);
		    }
		    HttpServletResponse res = (HttpServletResponse)response;
		    res.sendRedirect("/natcsweb/index.ncs");
		    return;
		}
		
		chain.doFilter(request, response);
	}

	/* (non-Java-doc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

}