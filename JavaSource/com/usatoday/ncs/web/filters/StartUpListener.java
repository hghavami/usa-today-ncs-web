package com.usatoday.ncs.web.filters;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Application Lifecycle Listener implementation class StartUpListener
 *
 */
public class StartUpListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public StartUpListener() {
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce) {
        
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce) {
    	
    	
    	try {
			// Start the cach Cleaner
			System.out.println("NCS WEB - Initializing Middle Tier");
	       	com.usatoday.util.constants.UsaTodayConstants.loadProperties();
			System.out.println("NCS WEB - Done Initializing Middle Tier");
    	}
    	catch (Exception e) {
    		e.printStackTrace();
		}
    	
    }
	
}
