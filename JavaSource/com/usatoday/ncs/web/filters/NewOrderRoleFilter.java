package com.usatoday.ncs.web.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.usatoday.ncs.web.handlers.UserHandler;

/**
 * Servlet Filter implementation class NewOrderRoleFilter
 */
public class NewOrderRoleFilter implements Filter {

    /**
     * Default constructor. 
     */
    public NewOrderRoleFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	    HttpServletRequest req = (HttpServletRequest)request;
		UserHandler userh = (UserHandler)req.getSession().getAttribute("user");
		
		if (userh == null || userh.getUser() == null || !userh.getUser().isAuthenticated()) {
		    // redirect to login page
		    if (userh != null) {
		        userh.setUserIPValidated(false);
		    }
		    request.getRequestDispatcher("/index.ncs").forward(request, response);
		    return;
		}

		if (!userh.getUser().getSite().isInNewOrderRole()) {
			request.getRequestDispatcher("/notAuthorized.ncs").forward(request, response);
		    return;
		}
		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
