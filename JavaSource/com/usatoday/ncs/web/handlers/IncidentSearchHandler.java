/*
 * Created on Nov 26, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

import java.util.Collection;

import com.usatoday.ncs.bo.IncidentBO;

/**
 * @author aeast
 * @date Nov 26, 2007
 * @class IncidentSearchHandler
 * 
 * 
 * 
 */
public class IncidentSearchHandler {

    private Collection<IncidentBO> searchResults = null;

    private String searchCriteria = null;
    
    
    /**
     * 
     */
    public IncidentSearchHandler() {
        super();
    }

    /**
     * @return Returns the searchCriteria.
     */
    public String getSearchCriteria() {
        return this.searchCriteria;
    }
    /**
     * @param searchCriteria The searchCriteria to set.
     */
    public void setSearchCriteria(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }
    /**
     * @return Returns the searchResults.
     */
    public Collection<IncidentBO> getSearchResults() {
        return this.searchResults;
    }
    /**
     * @param searchResults The searchResults to set.
     */
    public void setSearchResults(Collection<IncidentBO> searchResults) {
        this.searchResults = searchResults;
    }
}
