/*
 * Created on Jun 25, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

import java.util.Locale;

/**
 * @author aeast
 * @date Jun 25, 2007
 * @class LocaleHandler
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class LocaleHandler {

    private Locale l = null;
    
    /**
     * 
     */
    public LocaleHandler() {
        super();
        l = new Locale("en");
    }

    public void setLocale(Locale locale) {
        this.l = locale;
    }
    
    public Locale getLocale() {
        return l;
    }
    
    public String getLocaleStr() {
    	if (this.l != null) {
    		if (this.l.getLanguage() != null) {
    			return this.l.getLanguage();
    		}
    	}
       return  "en";
    }
    
    public void setLocaleStr(String localStr) {
    	if (localStr != null) {
    		this.l = new Locale(localStr);
    	}
    }
}
