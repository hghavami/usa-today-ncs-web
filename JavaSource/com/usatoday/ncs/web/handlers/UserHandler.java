/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

import com.usatoday.ncs.bo.UserBO;

/**
 * @author aeast
 * @date Nov 6, 2007
 * @class UserHandler
 * 
 * 
 */
public class UserHandler {

    private UserBO user = null;
    
    private String siteID = null;
    private String userID = null;
    private String clientIP = null;
    
    private boolean userIPValidated = false;
    private boolean clientIPInRange = false;
    
    /**
     * 
     */
    public UserHandler() {
        super();
    }

    
    /**
     * @return Returns the user.
     */
    public UserBO getUser() {
        return this.user;
    }
    /**
     * @param user The user to set.
     */
    public void setUser(UserBO user) {
        if (user == null) {
            this.clientIP = null;
            this.clientIPInRange = false;
            this.userIPValidated = false;
        }
        this.user = user;
    }
    /**
     * @return Returns the userID.
     */
    public String getUserID() {
        return this.userID;
    }
    /**
     * @param userID The userID to set.
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }
    /**
     * @return Returns the siteID.
     */
    public String getSiteID() {
        return this.siteID;
    }
    /**
     * @param siteID The siteID to set.
     */
    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }
    
    public String getDivCss() {
        String css = "display: inline";
        if (this.user == null || !this.user.isAuthenticated()) {
            css = "";
        }
        return css;
    }
    /**
     * @return Returns the clientIP.
     */
    public String getClientIP() {
        return this.clientIP;
    }
    /**
     * @param clientIP The clientIP to set.
     */
    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }
    /**
     * @return Returns the userIPValidated.
     */
    public boolean isUserIPValidated() {
        return this.userIPValidated;
    }
    /**
     * @param userIPValidated The userIPValidated to set.
     */
    public void setUserIPValidated(boolean userIPValidated) {
        this.userIPValidated = userIPValidated;
    }
    /**
     * @return Returns the clientIPInRange.
     */
    public boolean isClientIPInRange() {
        return this.clientIPInRange;
    }
    /**
     * @param clientIPInRange The clientIPInRange to set.
     */
    public void setClientIPInRange(boolean clientIPInRange) {
        this.clientIPInRange = clientIPInRange;
    }
    
    public String getRepIDForSampleLink() {
    	String repID = this.userID;
    	
    	// or maybe
    	// repID = this.siteID + "." + this.userID;
    	
    	return repID;
    }
}
