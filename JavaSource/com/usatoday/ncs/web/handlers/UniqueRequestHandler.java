/*
 * Created on Nov 20, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;


/**
 * @author aeast
 * @date Nov 20, 2007
 * @class UniqueRequestHandler
 * 
 *
 * 
 */
public class UniqueRequestHandler {
 
    Long unique = null;
    /**
     * 
     */
    public UniqueRequestHandler() {
        super();
        unique = new Long(System.currentTimeMillis());
    }

    /**
     * @return Returns the unique.
     */
    public Long getUnique() {
        return this.unique;
    }
    /**
     * @param unique The unique to set.
     */
    public void setUnique(Long unique) {
        this.unique = unique;
    }
}
