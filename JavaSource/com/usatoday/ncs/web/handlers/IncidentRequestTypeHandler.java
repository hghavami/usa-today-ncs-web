/*
 * Created on Dec 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

	
/**
 * @author aeast
 * @date Dec 6, 2007
 * @class IncidentRequestTypeHandler
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class IncidentRequestTypeHandler {

    private String homeDelivery = null;
    private String alternateDelivery = null;
    private String orderformrequest = null;
    private String newsstand = null;
    private String emailpassword = null;
    private String emailAddressChange = null;
    private String eEdition = null;
    private String eLeads = null;
    private String w9RequestForm = null;
    private String invoice = null;
    private String blueChip = null;
    private String rack = null;
    private String agentInquiry = null;
    private String education = null;
    private String natcustsvc = null;
    
    /**
     * 
     */
    public IncidentRequestTypeHandler() {
        super();
       
    }

    /**
     * @return Returns the agentInquiry.
     */
    public String getAgentInquiry() {
        return this.agentInquiry;
    }
    /**
     * @param agentInquiry The agentInquiry to set.
     */
    public void setAgentInquiry(String agentInquiry) {
        this.agentInquiry = agentInquiry;
    }
    /**
     * @return Returns the blueChip.
     */
    public String getBlueChip() {
        return this.blueChip;
    }
    /**
     * @param blueChip The blueChip to set.
     */
    public void setBlueChip(String blueChip) {
        this.blueChip = blueChip;
    }
    /**
     * @return Returns the homeDelivery.
     */
    public String getHomeDelivery() {
        return this.homeDelivery;
    }
    /**
     * @param homeDelivery The homeDelivery to set.
     */
    public void setHomeDelivery(String homeDelivery) {
        this.homeDelivery = homeDelivery;
    }
    /**
     * @return Returns the homeDelivery.
     */
    public String getEmailPassword() {
        return this.emailpassword;
    }
    /**
     * @param homeDelivery The homeDelivery to set.
     */
    public void setEmailPassword(String emailpassword) {
        this.emailpassword = emailpassword;
    }
    /**
     * @return Returns the invoice.
     */
    public String getInvoice() {
        return this.invoice;
    }
    /**
     * @param homeDelivery The homeDelivery to set.
     */
    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }
    /**
     * @return Returns the newsstand.
     */
    public String getOrderFormRequest() {
        return this.orderformrequest;
    }
    /**
     * @param newsstand The newsstand to set.
     */
    public void setOrderFormRequest(String orderformrequest) {
        this.orderformrequest = orderformrequest;
    }
    /**
     * @return Returns the newsstand.
     */
    public String getNewsstand() {
        return this.newsstand;
    }
    /*
     * @param newsstand The newsstand to set.
     */
    public void setNewsstand(String newsstand) {
        this.newsstand = newsstand;
    }
    /**
     * @return Returns the rack.
     */
    public String getRack() {
        return this.rack;
    }
    /**
     * @param rack The rack to set.
     */
    public void setRack(String rack) {
        this.rack = rack;
    }
    /**
     * @return Returns the education.
     */
    public String getEducation() {
        return this.education;
    }
    /**
     * @param education The education to set.
     */
    public void setEducation(String education) {
        this.education = education;
    }
	/**
	 * @return Returns the w9RequestForm.
	 */
	public String getW9RequestForm() {
		return w9RequestForm;
	}
	/**
	 * @param requestForm The w9RequestForm to set.
	 */
	public void setW9RequestForm(String requestForm) {
		w9RequestForm = requestForm;
	}

	/**
	 * @return Returns the emailAddressChange.
	 */
	public String getEmailAddressChange() {
		return emailAddressChange;
	}
	/**
	 * @param emailAddressChange The emailAddressChange to set.
	 */
	public void setEmailAddressChange(String emailAddressChange) {
		this.emailAddressChange = emailAddressChange;
	}

	public String getEEdition() {
		return eEdition;
	}

	public void setEEdition(String edition) {
		eEdition = edition;
	}
	
	/**
     * 
     */
    public String getNatCust() {
        return this.natcustsvc;
    }
    /**
     * @param 
     */
    public void setNatCust(String natcust) {
        this.natcustsvc = natcust;
    }

	public String geteLeads() {
		return eLeads;
	}

	public void seteLeads(String eLeads) {
		this.eLeads = eLeads;
	}

	public String getAlternateDelivery() {
		return alternateDelivery;
	}

	public void setAlternateDelivery(String alternateDelivery) {
		this.alternateDelivery = alternateDelivery;
	}
   
	
}
