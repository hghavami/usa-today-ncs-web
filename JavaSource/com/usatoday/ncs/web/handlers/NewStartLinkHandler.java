package com.usatoday.ncs.web.handlers;

public class NewStartLinkHandler {

	private String pubCode = "";
	private String keycode = "";
	private String linkURL = "#";
	private boolean showURL = false;
	
	public String getLinkURL() {
		return linkURL;
	}
	public void setLinkURL(String linkURL) {
		this.linkURL = linkURL;
	}
	public boolean getShowURL() {
		return showURL;
	}
	public void setShowURL(boolean showURL) {
		this.showURL = showURL;
	}
	public String getPubCode() {
		return pubCode;
	}
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}
	public String getKeycode() {
		return keycode;
	}
	public void setKeycode(String keycode) {
		this.keycode = keycode;
	}
	
	public void clear() {
		pubCode = "";
		keycode = "";
		linkURL = "#";
		showURL = false;
	}
}
