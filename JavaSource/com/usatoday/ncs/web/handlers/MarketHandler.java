/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

import com.usatoday.ncs.bo.MarketBO;
import com.usatoday.ncs.so.MarketLocatorService;

/**
 * @author aeast
 * @date Nov 6, 2007
 * @class MarketHandler
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class MarketHandler {

    private MarketBO market = null;
    
    private String zipCode = null;
    
    /**
     * 
     */
    public MarketHandler() {
        super();
    }

    /**
     * @return Returns the zipCode.
     */
    public String getZipCode() {
        return this.zipCode;
    }
    /**
     * @param zipCode The zipCode to set.
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
        
        if (zipCode != null) {
	        MarketLocatorService locator = new MarketLocatorService();
	        try {
	            this.market = locator.locateMarketByZip(zipCode);
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	        }
        }
    }
    /**
     * @return Returns the market.
     */
    public MarketBO getMarket() {
        return this.market;
    }
}
