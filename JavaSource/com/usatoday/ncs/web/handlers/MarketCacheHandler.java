/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.web.handlers;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.ncs.bo.MarketBO;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketCacheHandler
 * 
 * 
 * 
 */
public class MarketCacheHandler {

    DateTime dateLoaded = null;
    
    Collection<MarketBO> markets = null;
    /**
     * 
     */
    public MarketCacheHandler() {
        super();
        
        try {
            markets = MarketBO.fetchMarkets();
            dateLoaded = new DateTime();
        }
        catch (Exception e) {
            e.printStackTrace();
            markets = new ArrayList<MarketBO>();
        }
    }

    /**
     * @return Returns the markets.
     */
    public Collection<MarketBO> getMarkets() {
       
        // reload market data daily
        if (dateLoaded == null || dateLoaded.isAfter(dateLoaded.plusDays(1))) {
            this.markets.clear();
            try {
                markets = MarketBO.fetchMarkets();
                dateLoaded = new DateTime();
            }
            catch (Exception e) {
                System.out.println("Failed to load market cache: " + e.getMessage());
                markets = new ArrayList<MarketBO>();
            }
        }
        return this.markets;
    }
}
