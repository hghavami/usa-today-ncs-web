package com.usatoday.ncs.web.handlers;

public class NewStartOfferCodeSelectionHandler {

	private String pubCode = "UT";
	private String offerCode = "";
	public String getPubCode() {
		return pubCode;
	}
	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}
	public String getOfferCode() {
		return offerCode;
	}
	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}
	
}
