package com.usatoday.ncs.web.subscription.handlers;

import javax.servlet.http.HttpServletRequest;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.util.constants.UsaTodayConstants;

public class UTCommon {

	
    public static SubscriptionOfferIntf getCurrentOfferVersion2(HttpServletRequest request) {
    	SubscriptionOfferIntf offer = null;

        // attempt to get an override from the request
		String pubCode = request.getParameter("pub");
		SubscriptionProductIntf product = null;
		if (pubCode != null) {
		    pubCode = pubCode.toUpperCase();
		    try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			} catch (UsatException e) {
				;
			}
		}
		
		boolean keyCodeInRequest = false;
		String keyCode = request.getParameter("keycode");
		if (keyCode != null) {
		    keyCode = keyCode.toUpperCase();
		    keyCodeInRequest = true;
		}
		
		
		// otherwise get default of UT
		if (pubCode == null) {
            pubCode = UsaTodayConstants.UT_PUBCODE;
		    try {
				product = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			} catch (UsatException e) {
				;
			}
		}

        if (keyCode == null) {
            keyCode = product.getDefaultKeycode();
        }
        
        try {
            offer = SubscriptionOfferManager.getInstance().getOffer(keyCode, pubCode);
        }
        catch (Exception e) {
            //System.out.println("UTCOmmon::getCurrentOffer() Pub: " + pubCode + ", Keycode: " + keyCode + "  Exception: " + e.getMessage());
        }

        try {
            if (offer == null) {
                if (keyCodeInRequest) {
                    // expired promotion
                	
                    String expiredPromoKeycode = product.getExpiredOfferKeycode();
                    try {
                        offer = SubscriptionOfferManager.getInstance().getOffer(expiredPromoKeycode, pubCode);
                    }
                    catch (Exception eee) {
                        // last chance go for normal default
                        offer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubCode);
                    }
                }
                else {
                    // get default keycode
                    offer = SubscriptionOfferManager.getInstance().getDefaultOffer(pubCode);
                }
            }
            
        }
        catch (UsatException e) {
            System.out.println("UTCommon::getDefaultOfferV2 - NCS WEb() Pub: " + pubCode + ", Keycode: " + keyCode + "  Exception: " + e.getMessage());
        }

        return offer;
    }
	
}
