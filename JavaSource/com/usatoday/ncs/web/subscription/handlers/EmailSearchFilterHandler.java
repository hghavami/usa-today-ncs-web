package com.usatoday.ncs.web.subscription.handlers;

public class EmailSearchFilterHandler {

	private String emailAddressFilter = null;
	private String accountNumber = null;
	private boolean checkTrials = true;
	
	public String getEmailAddressFilter() {
		return emailAddressFilter;
	}
	public void setEmailAddressFilter(String emailAddressFilter) {
		this.emailAddressFilter = emailAddressFilter;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public boolean getIsCheckTrials() {
		return checkTrials;
	}
	public void setIsCheckTrials(boolean checkTrials) {
		this.checkTrials = checkTrials;
	}
	
	public Boolean getIsCheckTrialsObj() {
		return Boolean.valueOf(this.checkTrials);
	}
	
	public void setIsCheckTrialsObj(Boolean check) {
		this.checkTrials = check.booleanValue();
	}
	
	
	
	
}
