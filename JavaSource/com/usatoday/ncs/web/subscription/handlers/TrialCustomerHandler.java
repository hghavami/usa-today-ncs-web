package com.usatoday.ncs.web.subscription.handlers;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.business.interfaces.customer.TrialCustomerIntf;
import com.usatoday.business.interfaces.customer.TrialInstanceIntf;

public class TrialCustomerHandler {

	private TrialCustomerIntf trialCustomer = null;
	
	public TrialCustomerIntf getTrialCustomer() {
		return trialCustomer;
	}

	public void setTrialCustomer(TrialCustomerIntf trialCustomer) {
		this.trialCustomer = trialCustomer;
	}
	
	public String getExactTargetEmailPreferenceLink() {
		String etLink = "#";
		try {
			
			etLink = "http://pages.exacttarget.com/page.aspx?QS=3935619f7de112ef775078742a3b88d821295ee4dbc792deb89bccf5ead4c967&email=" + URLEncoder.encode(this.trialCustomer.getCurrentInstance().getContactInformation().getEmailAddress(), "UTF8");
		}
		catch (Exception e) {
		}
		return etLink;
	}


	public TrialCustomerInstanceHandler getCurrentInstance() {
		TrialCustomerInstanceHandler tcih = null;
		tcih = new TrialCustomerInstanceHandler();
		
		if (this.trialCustomer != null) {
			if (this.trialCustomer.getCurrentInstance() != null) {
				tcih.setTrial(this.trialCustomer.getCurrentInstance());
			}
		}
		
		return tcih;
	}
	
	public Collection<TrialCustomerInstanceHandler> getAllTrials() {
		Collection<TrialCustomerInstanceHandler> trials = new ArrayList<TrialCustomerInstanceHandler>();
		
		if (this.trialCustomer != null) {
			Collection<TrialInstanceIntf> tInstCol = this.trialCustomer.getAllTrialInstances();
			
			if (tInstCol.size() > 0) {
				for (TrialInstanceIntf ti : tInstCol) {
					TrialCustomerInstanceHandler tcih = new TrialCustomerInstanceHandler();
					tcih.setTrial(ti);
					trials.add(tcih);
				}
			}
		}
		
		return trials;
	}
}
