package com.usatoday.ncs.web.subscription.handlers;

import com.usatoday.businessObjects.customer.CustomerInterimInfoBO;

public class InterimCustomerHandler {

	CustomerInterimInfoBO interimCust = null;

	public InterimCustomerHandler() {
		super();
		
	}

	public CustomerInterimInfoBO getInterimCust() {
		return interimCust;
	}

	public void setInterimCust(CustomerInterimInfoBO interimCust) {
		this.interimCust = interimCust;
	}
	
	
}
