package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import com.usatoday.businessObjects.products.SubscriptionOfferManager;

public class SubscriptionOfferLocatorHandler {
	
	private String pubCode = "UT";
	private String keycode = "";
	
	private OfferLocatorTypeAhead typeAhead = null;
	private SubscriptionOfferHandler currentOfferHandler = null;
	private ProductHandler productHandler = null;
	
	
	public SubscriptionOfferLocatorHandler() {
		this.currentOfferHandler = new SubscriptionOfferHandler();
		this.typeAhead = new OfferLocatorTypeAhead();
	}


	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
		this.typeAhead.setPubCode(pubCode);
	}

	public String getKeycode() {
		return keycode;
	}

	public void setKeycode(String keycode) {
		if (keycode != null && !keycode.equalsIgnoreCase(this.keycode) && keycode.trim().length() > 14) {
			String fullText = keycode.trim();
			this.keycode = keycode;
			try {
				String keycodeOnly = fullText.substring(0, 5);

				this.typeAhead.setKeyCode(keycodeOnly);
				
				this.currentOfferHandler.setCurrentOffer(SubscriptionOfferManager.getInstance().getOffer(this.typeAhead.getKeyCode(), this.pubCode));
				
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			if (keycode == null || !keycode.equalsIgnoreCase(this.keycode)) {
				this.currentOfferHandler.setCurrentOffer(null);
			}
			this.keycode = keycode;
			
			this.typeAhead.setKeyCode("");
		}

	}

	public SubscriptionOfferHandler getCurrentOfferHandler() {
		try {
			if (this.currentOfferHandler.getCurrentOffer() == null) {
				if (this.typeAhead.getPubCode() != null && this.typeAhead.getKeyCode() != null && this.typeAhead.getKeyCode().length() == 5) {
					this.currentOfferHandler.setCurrentOffer(SubscriptionOfferManager.getInstance().getOffer(this.typeAhead.getKeyCode(), this.pubCode));
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return currentOfferHandler;
	}

	public void setCurrentOfferHandler(SubscriptionOfferHandler currentOffer) {
		this.currentOfferHandler = currentOffer;
	}

	public Collection<SubscriptionTermHandler> getOfferTerms() {
		if (this.currentOfferHandler != null) {
			return this.currentOfferHandler.getOfferTerms();
		}
		return new ArrayList<SubscriptionTermHandler>();
	}
	
    public ArrayList<SelectItem> getOfferTermsSelectItems() {

    	if (this.currentOfferHandler != null && this.currentOfferHandler.getCurrentOffer() != null) {
    		return this.currentOfferHandler.getOfferTermsSelectItems();
    	}
        return null;
    }
    
    public boolean getShowOffer() {
    	if (this.currentOfferHandler != null && this.currentOfferHandler.getCurrentOffer() != null) {
    		return true;
    	}
    	return false;
    }


	public OfferLocatorTypeAhead getTypeAhead() {
		return typeAhead;
	}


	public void setTypeAhead(OfferLocatorTypeAhead typeAhead) {
		this.typeAhead = typeAhead;
	}


	public ProductHandler getProductHandler() {
		return productHandler;
	}


	public void setProductHandler(ProductHandler productHandler) {
		this.productHandler = productHandler;
	}
	
}
