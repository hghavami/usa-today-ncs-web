package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.util.constants.UsaTodayConstants;

public class ProductHandler {

	private ProductIntf product = null;

	public ProductIntf getProduct() {
		return product;
	}

	public void setProduct(ProductIntf product) {
		this.product = product;
	}
	
	public String getImageUrl() {
		String url = "/natcsweb/theme/1x1.gif";
		
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		boolean secure = request.isSecure();
		String host = request.getServerName();
		String protocol = "http://";
		if (secure) {
			protocol = "https://";
		}
		
		if (this.product.getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
			url = protocol + host + "/images/marketingimages/usat_covers.jpg";
		}
		else if (this.product.getProductCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
			url = protocol + host + "/images/marketingimages/bbw_covers.jpg";
		}
		else if (this.product.getProductCode().equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
			url = protocol + host + "/images/eEdition/Subscriber_Portal_Dual_Sales_page_EE_255x233.jpg";
		}
		
		return url;
	}
	
	public String getProductName() {
		String value = "";
		if (this.product != null) {
			value = this.product.getName();
		}
		return value;
		
	}

	public String getProductDescription() {
		String value = "";
		if (this.product != null) {
			value = this.product.getDescription();
		}
		return value;
	}
	
	public String getProductDetailedDescription() {
		String value = "";
		if (this.product != null) {
			value = this.product.getDetailedDescription();
		}
		return value;
	}

	public String getProductCode() {
		String value = "";
		if (this.product != null) {
			value = this.product.getProductCode();
		}
		return value;
	}
	
	public String getOrderPageURL() {
		String value = "#";
		if (this.product != null) {
			value = "/natcsweb/restricted/newOrders/orders/chooseOffer.ncs?initialRequest=Y&pubCode=" + this.product.getProductCode();
		}
		return value;
	}

	public String getMaximumQuantityPerOrder() {
		int max = 10;

		if (this.product != null && this.product.getMaxQuantityPerOrder()>1) {
			max = this.product.getMaxQuantityPerOrder();
		}
		return String.valueOf(max);
	}

    public ArrayList<SelectItem> getOrderQtySelectItems() {
        
        ArrayList<SelectItem> items = new ArrayList<SelectItem>();
        
        
        try {
        	if (this.product != null) {
        		for (int i = 1; i <= this.product.getMaxQuantityPerOrder(); i++) {
        			SelectItem si = new SelectItem();
        			String strVal = String.valueOf(i);
        			
        			si.setValue(strVal);
        			si.setLabel(strVal);
        			items.add(si);
        		}
        	}
        	else {
        		//limit to one
    			SelectItem si = new SelectItem();
    			si.setDescription("1");
    			si.setLabel("1");
    			si.setValue("1");
    			items.add(si);
        	}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to build quantity items: " + e.getMessage());
        }
        return items;
    }	
    
	public boolean getIsShowChangeOrderQtySection() {
		boolean showSection = false;

		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {
			showSection = true;
		}
		return showSection;
	}
	
    
}
