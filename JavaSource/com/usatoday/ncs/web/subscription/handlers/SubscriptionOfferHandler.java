package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.faces.model.SelectItem;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionOfferIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.CreditCardPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.HTMLPromotionIntf;
import com.usatoday.business.interfaces.products.promotions.ImagePromotionIntf;
import com.usatoday.business.interfaces.products.promotions.PromotionIntf;
import com.usatoday.businessObjects.products.SubscriptionOfferManager;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.util.constants.UsaTodayConstants;

public class SubscriptionOfferHandler {

	public static String DEFAULT_UT_BRAND_NUM_COPIES_LEARN_MORE_HTML = "&nbsp;<a onclick=\"javascript:window.open('/faq/utfaq.jsp#section29','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\" href=\"javascript:;\"><span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";
	public static String DEFAULT_BW_BRAND_NUM_COPIES_LEARN_MORE_HTML = "&nbsp;<a onclick=\"javascript:window.open('/faq/bwfaq.jsp#section29','FAQ', 'width=800,height=800,menubar=no,titlebar=yes,scrollbars=yes,status=no,location=no,directories=no')\" href=\"javascript:;\"><span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";

	public static String ADDRESS_LEARN_MORE_HTML = "<a <span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";
	public static String NUM_COPIES_LEARN_MORE = "<a <span style=\"font-family: Arial; font-size: 7pt;\">Learn More...</span></a>";

	private SubscriptionOfferIntf currentOffer = null;
	
	private SubscriptionOfferIntf previousOffer = null;
	
	private SubscriptionProductIntf product = null;

	private Collection<SubscriptionTermHandler> terms = null;
	
	private String pubCode = null;
	
	private String overrideOfferCode = null;
	
	public SubscriptionOfferIntf getCurrentOffer() {
		return currentOffer;
	}

	public void setCurrentOffer(SubscriptionOfferIntf cOffer) {
		
		if (cOffer != null && this.currentOffer != null && !cOffer.getKeyCode().equalsIgnoreCase(this.currentOffer.getKeyCode())) {
			this.previousOffer = this.currentOffer;
		}
		
		this.currentOffer = cOffer;
		if (cOffer != null) {
			try {
				product = SubscriptionProductBO.getSubscriptionProduct(cOffer.getPubCode());
				this.pubCode = product.getProductCode();

				if (this.terms != null) {
					this.terms.clear();
				}
				else {
					this.terms = new ArrayList<SubscriptionTermHandler>();
				}
				
				for (SubscriptionTermsIntf aTerm : cOffer.getTerms()) {
					SubscriptionTermHandler tH = new SubscriptionTermHandler();
					tH.setParent(this);
					tH.setTerm(aTerm);
					this.terms.add(tH);
				}
			}
			catch (Exception e) {
				product = null;
			}
		}
		else {
			product = null;
		}
		
	}
	
    public ArrayList<SelectItem> getCreditCardExpirationYears() {
        
        ArrayList<SelectItem> items = new ArrayList<SelectItem>();
        
        
        try {
			SelectItem si = new SelectItem();
			si.setDescription("Year");
			si.setLabel("Year");
			si.setValue("-1");
            items.add(si);

        	DateTime today = new DateTime();
        		
        		for (int c = 0; c < 8; c++) {
        			String year = today.toString("yyyy");
        			
        			si = new SelectItem();
		            
        			si.setDescription(year);
        			si.setLabel(year);
        			si.setValue(year);
		            
		            items.add(si);
		            
		            today = today.plusYears(1);
        		}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to build terms items: " + e.getMessage());
        }
        return items;
    }
	
	
    public Collection<SubscriptionTermHandler> getOfferTerms() {
    	if (this.terms != null) {
    		return this.terms;
    	}
    	else {
    		return new ArrayList<SubscriptionTermHandler>();
    	}
    }
    
    public ArrayList<SelectItem> getOfferTermsSelectItems() {
        
        ArrayList<SelectItem> items = new ArrayList<SelectItem>();
        
        java.text.NumberFormat currencyFormat = java.text.NumberFormat.getCurrencyInstance();
        
        try {
        	if (this.currentOffer != null) {
        		
        		for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
        			SelectItem si = new SelectItem();
        			si.setEscape(false);
		            
		            StringBuilder sb = new StringBuilder(aTerm.getDescription());
		            if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()){
		            	sb.append("<font color=\"navy\">&nbsp;*</font>");
		            }

        			sb.append(" - ").append(currencyFormat.format(aTerm.getDailyRate())).append(" per issue");
        			
        			SubscriptionTermsIntf renewsTo = this.currentOffer.getRenewalTerm(aTerm.getDurationInWeeks());
        			if (renewsTo != null) {
        				sb.append("<br><span class=\"selectOneRadioTermRenewTo\">(Renews to: ").append(renewsTo.getDescription()).append(" - ").append(currencyFormat.format(renewsTo.getDailyRate())).append(" per issue").append(")</span>");
        			}
        			
        			si.setDescription(sb.toString());		            
        			si.setLabel(sb.toString());
        			si.setValue(aTerm.getDurationInWeeks());
		            
		            items.add(si);
        		}
        	}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to build terms items: " + e.getMessage());
        }
        return items;
    }

    public boolean getIsForceEZPAY() {
		boolean isForceEzPAY = false;
		if (this.currentOffer!=null) {
			isForceEzPAY = this.currentOffer.isForceEZPay();
		}
		return isForceEzPAY;
	}
	
    public boolean getIsForceEZPAYRate() {
		boolean isForceEzPayRate = false;
        try {
        	if (this.currentOffer != null) {
        		for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
		            if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()){
		            	isForceEzPayRate = true;
		            }
        		}
        	}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to derive Force EZPay terms: " + e.getMessage());
        }		
		
		return isForceEzPayRate;
	}
    
    public String getForceEZPayCustomTermsText() {
    	String text = "";
        try {
        	if (this.currentOffer != null) {
        		text = this.currentOffer.getPromotionSet().getPromoOnePageForceEZPayCustomTermsText().getPromotionalHTML();
        	}
        }
        catch (Exception e) {
            text = "";
        }		
    		
    	return text;
    }
	
    public boolean getIsNonEZPayRate() {
		boolean isNonEzPayRate = false;

        try {
        	if (this.currentOffer != null) {
        		for (SubscriptionTermsIntf aTerm : this.currentOffer.getTerms()) {
		            if (this.currentOffer.isForceEZPay() || aTerm.requiresEZPAY()){
		            // Do nothing
		            } else {
		            	isNonEzPayRate = true;
		            }
        		}
        	}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to derive non-EZPay terms: " + e.getMessage());
        }		
		
        return isNonEzPayRate;
    }
	
	public boolean getIsBillMeAllowed() {
		boolean isAllowed = false;
		if (this.currentOffer!=null) {
			isAllowed = this.currentOffer.isBillMeAllowed();
		}
		return isAllowed;
	}
	
	public boolean getIsShowPaymentOptionSelector() {
		boolean showOptions = false;
		try {
			if (this.currentOffer != null) {
				if (this.currentOffer.isBillMeAllowed() && !this.currentOffer.isForceBillMe()) {
					// if bill me is allowed, but not forced then show options
					showOptions = true;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return showOptions;
	}

	public boolean getIsShowCreditCardPaymentOptionData() {
		boolean showOptions = true;
		try {
			if (this.currentOffer != null) {
				if (this.currentOffer.isForceEZPay() || !this.currentOffer.isForceBillMe()) {
					// force ezpay or not a force bill me, then show CC info
					showOptions = true;
				}
				else {
					showOptions = false;
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return showOptions;
	}
	
	
	public boolean getIsForceBillMe() {
		boolean isForced = false;
		if (this.currentOffer!=null) {
			isForced = this.currentOffer.isForceBillMe();
		}
		return isForced;
	}
	
	public boolean getIsTakesVisa() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer!=null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsVisa();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesMasterCard() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer!=null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsMasterCard();
			}
		}
		return takesCard;
	}
	
	public boolean getIsTakesAMEX() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer!=null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsAmEx();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesDiscover() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer!=null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsDiscovery();
			}
		}
		return takesCard;
	}

	public boolean getIsTakesDiners() {
		// default to true
		boolean takesCard = true;
		if (this.currentOffer!=null) {
			CreditCardPromotionIntf ccPromo = this.currentOffer.getPromotionSet().getPromoCreditCard();
			if (ccPromo != null) {
				takesCard = ccPromo.acceptsDiners();
			}
		}
		return takesCard;
	}

	public void setIsForceBillMe(boolean value) {
		; // ignore, only here to appease JSF
	}
	
    public boolean getIsClubNumberRequired() {
		boolean isClubNumberRequired = false;
		if (this.currentOffer!=null) {
			isClubNumberRequired = this.currentOffer.isClubNumberRequired();
		}
		return isClubNumberRequired;
	}
    
	public boolean getIsShowOfferOverride() {
		boolean showOverride = false;
		try {
			if (this.currentOffer != null ) {
				PromotionIntf promo =  this.currentOffer.getPromotionSet().getAllowOfferOverrides();
				if (promo != null && promo.getFulfillText() != null && promo.getFulfillText().equalsIgnoreCase("Y")) {
					showOverride = true;
				}
			}
			
		}
		catch (Exception e) {
			showOverride = false;
		}
		return showOverride;
	}

    public String getCustomerServicePhone() {
    	String custServicePhone = "";
    	if (this.currentOffer != null) {
			// Now check if the keycode source matches any in the Promo table for customer service phone number display
			if (this.currentOffer.getPromotionSet().getCustomerServicePhoneCheck() != null) {			// Flag to be used through our the app
				custServicePhone = this.currentOffer.getPromotionSet().getCustomerServicePhoneCheck().getFulfillText();
			} 
    	}
		return custServicePhone;
    }
    
    public boolean getCJCustomerPhoneNumDisplay() {
    	boolean cjCustomerPhoneNumDisplay = false;
    	if (this.currentOffer != null) {
    		if (this.currentOffer.getKeyCode().substring(0,1).equals(com.usatoday.util.constants.UsaTodayConstants.CJ_CUST_SERV_PHONE_CK_SRC_PROP)) {
    			cjCustomerPhoneNumDisplay =  true;
    		}
    	}
    	return cjCustomerPhoneNumDisplay;
    }
	
	public String getProductName() {
		String prodName = "None Selected";
		if (product != null) {
			prodName = product.getName();
		}
		return prodName;
	}
	
	public String getProductDescription() {
		String prodDes = "";
		if (product != null) {
			prodDes = product.getDescription();
		}
		return prodDes;
	}
	
	public String getProductSampleURL() {
		String url = "";
		try {
			url = this.product.getSupplier().getSampleURL();
			if (url == null) {
				url = "";
			}
		}
		catch (Exception e) {
			; // ignore

		}
		return url;
	}
	
	public SubscriptionOfferHandler getElectronicEditionOffer() {
		SubscriptionOfferHandler soh = null;
		
		try {
			if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)) {
				
				if (this.product.isElectronicDelivery()) {
					soh = this;
				}
				else {
					SubscriptionOfferIntf offer = null;
					try {
						offer = SubscriptionOfferManager.getInstance().getEEOffer(this.currentOffer.getKeyCode());
					}
					catch (Exception e) {
					}
	
					if (offer == null) {
						try {
							offer = SubscriptionOfferManager.getInstance().getEEOffer(this.product.getDefaultKeycode());
						}
						catch (Exception e) {
						}						
					}
					soh = new SubscriptionOfferHandler();
					soh.setCurrentOffer(offer);
				}
			}
			/* will need to add SW later*/
		}
		catch (Exception e) {
			soh = this;
		}
		
		return soh;
	}
	
	public String getDualPagePromotionText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoLandingPageText().getPromotionalHTML();
			}
			else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(UsaTodayConstants.UT_PUBCODE);
				html = o.getPromotionSet().getPromoLandingPageText().getPromotionalHTML();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}
	
	
	public String getOnePagePromotionText1() {
		String html = "&nbsp;";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageText1().getPromotionalHTML();
			}
			else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(this.product.getBrandingPubCode());
				html = o.getPromotionSet().getPromoOnePageText1().getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "&nbsp;";
		}
		return html;
	}
	
	public String getOnePageDisclaimerText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageDisclaimerText().getPromotionalHTML();
			}
			else {
				SubscriptionOfferIntf o = SubscriptionOfferManager.getInstance().getDefaultOffer(this.product.getBrandingPubCode());
				html = o.getPromotionSet().getPromoOnePageDisclaimerText().getPromotionalHTML();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageJavaScriptText() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageJavaScript1Text().getJavaScript();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageThankYouJavaScriptText1() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouJavaScript1Text().getJavaScript();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageThankYouJavaScriptText2() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouJavaScript2Text().getJavaScript();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}
	
	public boolean isRenderRightColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageRightColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					if (image.getImagePathString().startsWith("/", 0)) {
						path = "https://service.usatoday.com" + image.getImagePathString();
					}
					else {
						path = image.getImagePathString();
					}
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderRightColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}
	
	public String getOnePageRightColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnHTMLSpot1HTML() {
		String html = "";
		try {
			if (this.currentOffer != null ) {
				HTMLPromotionIntf htmlPromo =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public String getOnePageRightColumnHTMLSpot2HTML() {
		String html = "";
		try {
			if (this.currentOffer != null ) {
				HTMLPromotionIntf htmlPromo =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2HTML();
				html = htmlPromo.getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}
	
	public boolean isRenderRightColumnImageSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageRightColumnImageSpot2ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					if (image.getImagePathString().startsWith("/", 0)) {
						path = "https://service.usatoday.com" + image.getImagePathString();
					}
					else {
						path = image.getImagePathString();
					}
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderRightColmnImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}
	

	public String getOnePageRightColumnImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot2Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	
	public boolean isRenderRightColumnImageSpot3() {
		boolean render = false;
		try {
			
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageRightColumnImageSpot3ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					if (image.getImagePathString().startsWith("/", 0)) {
						path = "https://service.usatoday.com" + image.getImagePathString();
					}
					else {
						path = image.getImagePathString();
					}
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderRightColmnImageSpot3ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}
	
	
	public String getOnePageRightColumnImageSpot3ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnImageSpot3ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColSpot3Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	
	public boolean isRenderRightColumnVideoSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageRightColumnVideoSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnVideoSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageRightColumnVideoSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	
	public String getOnePageRightColumnVideoSpot1HTML() {
		String html = "";
		try {
			if (this.currentOffer != null ) {
				HTMLPromotionIntf htmlPromo =  this.currentOffer.getPromotionSet().getPromoOnePageRightColVideoSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public boolean isRenderLeftColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageLeftColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderLeftColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}

	public String getOnePageLeftColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePgLftClSpt1HTM() {
		String html = "";
		try {
			if (this.currentOffer != null ) {
				HTMLPromotionIntf htmlPromo =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public boolean isRenderLeftColumnVideoSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageLeftColumnVideoSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnVideoSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnVideoSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	
	/**
	 * 
	 * @return  Left column video spot HTML
	 */
	public String getonePgLtClVdSpt1HT() {
		String html = "";
		try {
			if (this.currentOffer != null ) {
				HTMLPromotionIntf htmlPromo =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColVideoSpot1HTML();
				html = htmlPromo.getPromotionalHTML();
			}
			
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public boolean isRenderLeftColumnImageSpot2() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}
	
	public String getOnePageLeftColumnImageSpot2ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public boolean isRenderLeftColmnImageSpot2ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}

	public String getOnePageLeftColumnImageSpot2ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageLeftColumnImageSpot2ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageLeftColSpot2Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	

	public String getOnePageThankYouRightColHTMLSpot1() {
		String html = "";
		try {
			if (this.currentOffer != null) {
				html = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1HTML().getPromotionalHTML();
			}
		}
		catch (Exception e) {
			html = "";
		}
		return html;
	}

	public boolean isRenderOnePaqeThankYouRightColumnImageSpot1() {
		boolean render = false;
		try {
			if (this.currentOffer != null && this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image() != null) {
				ImagePromotionIntf p = this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (p.getImagePathString() != null && p.getImagePathString().length() > 0) {
					render = true;
				}
			}
			
		}
		catch (Exception e) {
			render = false;
		}
		return render;
	}

	public boolean isRenderOnePageThankYouRightColmnImageSpot1ImageAsClickable() {
		boolean clickable = false;
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					clickable = true;
				}
			}
			
		}
		catch (Exception e) {
			clickable = false;
		}
		
		return clickable;
	}
	
	public String getOnePageThankYouRightColumnImageSpot1ImagePath() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImagePathString() != null && image.getImagePathString().length() > 0) {
					path = image.getImagePathString();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot1ImageOnClickURL() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageLinkToURL() != null && image.getImageLinkToURL().length() > 0) {
					path = image.getImageLinkToURL();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}

	public String getOnePageThankYouRightColumnImageSpot1ImageAltText() {
		String path = "";
		try {
			if (this.currentOffer != null ) {
				ImagePromotionIntf image =  this.currentOffer.getPromotionSet().getPromoOnePageThankYouRightColSpot1Image();
				if (image.getImageAltText() != null && image.getImageAltText().length() > 0) {
					path = image.getImageAltText();
				}
			}
			
		}
		catch (Exception e) {
			path = "";
		}
		return path;
	}
	
	public boolean getIsShowOrderPathOverlayPopUp() {
		boolean showKeyCodeOverlay = false;
		try {
			if (this.currentOffer != null) {
				// first check offer for this pub/keycode
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();
				
				if (imagePromo != null) {
					String fulfillText = imagePromo.getFulfillText();
					if (fulfillText != null && !fulfillText.trim().equalsIgnoreCase("HIDE")) {
						showKeyCodeOverlay = true;
					}
				}
				
			}
			
		}
		catch (Exception e) {
			showKeyCodeOverlay = false;
		}
		return showKeyCodeOverlay;
	}

	public void setIsShowOrderPathOverlayPopUp(boolean newValue) {
		; // ignore, no update allowed
	}
	public String getOrderPathOverlayPopUpImagePath() {
		String imagePath = null;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();
				
				if (imagePromo != null) {
					imagePath = imagePromo.getImagePathString();
				}
				
			}
			
		}
		catch (Exception e) {
			imagePath = null;
		}
		return imagePath;
	}

	public String getOrderPathOverlayPopUpImageLinkURL() {
		String imageURL = null;
		try {
			if (this.currentOffer != null) {
				ImagePromotionIntf imagePromo = this.currentOffer.getPromotionSet().getOrderPathPopOverlayPromoImage();
				
				if (imagePromo != null) {
					imageURL = imagePromo.getImageLinkToURL();
				}
				
			}
			
		}
		catch (Exception e) {
			imageURL = null;
		}
		return imageURL;
	}
	
	public boolean getIsShowOfferDisclaimerText() {
		boolean showDisclaimer = false;
		try {
			if (this.currentOffer != null) {
				HTMLPromotionIntf disclaimerTextPromo = this.currentOffer.getPromotionSet().getPromoOnePageDisclaimerText();
				if (disclaimerTextPromo == null || disclaimerTextPromo.getPromotionalHTML().length()==0) {
					showDisclaimer = false;
				}
				else {
					showDisclaimer = true;
				}
			}
		}
		catch (Exception e) {
			showDisclaimer = false;
		}
		return showDisclaimer;
	}	
	
	public Date getEarliestPossibleStartDate() {
		Date start = null;
		
		// this is for calendar validation only
		
		DateTime today = new DateTime();
		start = today.minusDays(1).toDate();
		
		return start;
	}

	public String getEarliestPossibleStartDateString() {
		String earliestDate = null;
		if (this.product != null) {
			earliestDate = this.product.getEarliestPossibleStartDate().toString("mm/DD/yyyy");
		}
		
		if (earliestDate == null) {
			DateTime now = new DateTime();
			earliestDate = now.toString("mm/DD/yyyy");
		}
		return earliestDate;
	}
	
	public void setEarliestPossibleStartDate(Date d) {
		;// ignore
	}
	
	public Date getLatestPossibleStartDate() {
		Date end = null;
		
		if (this.product != null) {
			end = this.product.getLatestPossibleStartDate().toDate();
		}
		
		if (end == null) {
			DateTime now = new DateTime();
			end = now.plusDays(30).toDate();
		}
		return end;
	}

	public String getLatestPossibleStartDateString() {

		String latestDate = null;
		if (this.product != null) {
			latestDate = this.product.getLatestPossibleStartDate().toString("mm/DD/yyyy");
		}
		
		if (latestDate == null) {
			DateTime now = new DateTime();
			latestDate = now.plusDays(30).toString("mm/DD/yyyy");
		}
		return latestDate;
	}
	
	public void setLatestPossibleStartDate(Date d) {
		;// ignore
	}
	
	public String getMaximumQuantityPerOrder() {
		int max = 10;

		if (this.product != null && this.product.getMaxQuantityPerOrder()>1) {
			max = this.product.getMaxQuantityPerOrder();
		}
		return String.valueOf(max);
	}

    public ArrayList<SelectItem> getOrderQtySelectItems() {
        
        ArrayList<SelectItem> items = new ArrayList<SelectItem>();
        
        
        try {
        	if (this.product != null) {
        		for (int i = 1; i <= this.product.getMaxQuantityPerOrder(); i++) {
        			SelectItem si = new SelectItem();
        			String strVal = String.valueOf(i);
        			
        			si.setValue(strVal);
        			si.setLabel(strVal);
        			items.add(si);
        		}
        	}
        	else {
        		//limit to one
    			SelectItem si = new SelectItem();
    			si.setDescription("1");
    			si.setLabel("1");
    			si.setValue("1");
    			items.add(si);
        	}
        }
        catch (Exception e) {
            System.out.println("SubscriptionOfferHandler:  Failed to build quantity items: " + e.getMessage());
        }
        return items;
    }
	
	public boolean getIsShowChangeOrderQtySection() {
		boolean showSection = false;

		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {
			showSection = true;
		}
		return showSection;
	}
	
	public String getQuantityLearnMoreHTML() {
		String learnMore = SubscriptionOfferHandler.DEFAULT_UT_BRAND_NUM_COPIES_LEARN_MORE_HTML;
		if (this.product != null && this.product.getMaxQuantityPerOrder() > 1) {
		
			if (this.product.getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
				learnMore = SubscriptionOfferHandler.DEFAULT_BW_BRAND_NUM_COPIES_LEARN_MORE_HTML;
			}
			
		}
		return learnMore;
	}

	
	public String getAddressLearnMoreHTML() {
		String learnMoreAddress = SubscriptionOfferHandler.ADDRESS_LEARN_MORE_HTML;
		
		return learnMoreAddress;
	}
	
	
	


	public SubscriptionOfferIntf getPreviousOffer() {
		return previousOffer;
	}

	public void setPreviousOffer(SubscriptionOfferIntf previousOffer) {
		this.previousOffer = previousOffer;
	}
	
	public boolean isShowPreviousOfferRevertLink() {
		if (this.previousOffer != null) {
			return true;
		}
		return false;
	}
	
	public boolean isShowDeliveryMethodCheck() {
		boolean showDelCheck = false;
		
		if (this.currentOffer != null && this.currentOffer.getSubscriptionProduct().getProductCode().equalsIgnoreCase("UT")) {
			
			// default to show it if not EE
			showDelCheck = true;
			
            PromotionSet currentOfferPromotionSet = currentOffer.getPromotionSet();
            
            if (currentOfferPromotionSet != null && currentOfferPromotionSet.getDeliveryNotification() != null && 
                 currentOfferPromotionSet.getDeliveryNotification().getFulfillText() != null  && 
                !currentOfferPromotionSet.getDeliveryNotification().getFulfillText().trim().equals("")) {
                if (currentOfferPromotionSet.getDeliveryNotification().getFulfillText().equals("ON")) {
                	showDelCheck = true;
                } else {
                	showDelCheck = false;
                }
            }
			
		}
		return showDelCheck;
	}

	public String getOverrideOfferCode() {
		return overrideOfferCode;
	}

	public void setOverrideOfferCode(String overrideOfferCodeNewValue) {
		
		if (overrideOfferCodeNewValue != null && !overrideOfferCodeNewValue.equalsIgnoreCase(this.overrideOfferCode) && overrideOfferCodeNewValue.trim().length() > 14) {
			this.overrideOfferCode = overrideOfferCodeNewValue.trim();
			
			/*try {
				String keycode = this.overrideOfferCode.substring(0, 5);
				
				int index = this.overrideOfferCode.indexOf("(");
				String newPubCode = this.overrideOfferCode.substring(index+1, index+3);
				
				SubscriptionOfferIntf newOffer = SubscriptionOfferManager.getInstance().getOffer(keycode, newPubCode);
				
				if (newOffer != null) {
					this.setCurrentOffer(newOffer);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			*/
		}
		else {
			this.overrideOfferCode = overrideOfferCodeNewValue;
		}
	}

//	public SubscriptionOfferLocatorHandler getOfferLocator() {
//		return offerLocator;
//	}

	public void setOfferLocator(SubscriptionOfferLocatorHandler offerLocator) {
		;
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCodeNew) {
		
//		if (this.pubCode != null && !this.pubCode.equalsIgnoreCase(pubCodeNew)) {
//			this.offerLocator.setPubCode(pubCodeNew);
//		}
		this.pubCode = pubCodeNew;
	}

}
