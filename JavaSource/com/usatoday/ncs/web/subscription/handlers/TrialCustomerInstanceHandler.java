package com.usatoday.ncs.web.subscription.handlers;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.customer.TrialInstanceIntf;
import com.usatoday.business.interfaces.partners.TrialPartnerIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;

public class TrialCustomerInstanceHandler {

	private TrialInstanceIntf trial = null;
	
	public TrialInstanceIntf getTrial() {
		return trial;
	}

	public void setTrial(TrialInstanceIntf trial) {
		this.trial = trial;
	}
	
	public java.util.Date getStartDate() {
		if (this.trial != null) {
			return this.trial.getStartDate().toDate();
		}
		return null;
	}
	
	public java.util.Date getEndDate() {
		if (this.trial != null) {
			return this.trial.getEndDate().toDate();
		}
		return null;
	}
	
	public SubscriptionProductIntf getProduct() {
		
		SubscriptionProductIntf prod = null;
		
		if (this.trial != null) {
			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
			}
			catch (Exception e) {
				;
			}
		}
		
		return prod;
	}

	public String getTrialDurationInDays()  {
		String duration = null;
		if (this.trial != null) {
			TrialPartnerIntf partner = this.trial.getPartner();
			duration = String.valueOf(partner.getDurationInDays());
		}
		return duration;
	}
	
	public String getElectronicReadNowURL() {
		String eEditionLink = null;

		SubscriptionProductIntf prod = null;
		
		if (this.trial != null) {
			String pubCode = this.trial.getPubCode();
			try {
				prod = SubscriptionProductBO.getSubscriptionProduct(pubCode);
				eEditionLink = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(prod, this.trial.getContactInformation().getEmailAddress(), null,null, false);
			}
			catch (Exception e) {
				;
			}
		}
		
		return eEditionLink;
	}
	
	public String getDaysRemainingString() {
		String daysLeft = "0";
		
		if (this.trial != null) {
			DateTime now = new DateTime();
			DateTime endDate = this.trial.getEndDate();
			
			if (endDate != null) {
				int numDays = 0;
				if (now.isBefore(endDate)) {
					while (now.isBefore(endDate)) {
						numDays++;
						now = now.plusDays(1);
					}
					daysLeft = String.valueOf(numDays);
				}
			}
			else {
				daysLeft = "No End Date";
			}
		}
		return daysLeft;
	}

	public boolean getIsShowClubNumber() {
		boolean showClubNumber = false;
		if (this.trial != null) {
			if (this.trial.getClubNumber() != null && this.trial.getClubNumber().trim().length() > 0) {
				showClubNumber = true;
			}
		}
		return showClubNumber;
	}

	public String getClubNumberLabel() {
		String label = "";
		if (this.trial != null) {
			try {
				if (this.trial.getPartner().getClubNumberLabel() != null) {
					label = this.trial.getPartner().getClubNumberLabel();
				}
			}
			catch (Exception e) {
				// 
				label = "";
			}
		}
		return label;
	}

	public void setIsShowClubNumber(boolean showIt) {
		; // ignore
	}
	
	public String getPartnerSignUpURL() {
		String url = "";
		
		if (this.trial != null) {
			try {
				if (this.trial.getPartner().getIsOfferCurrentlyRunning()) {
					url = "/electronicSamplePartnerLookup.do?pid=" + this.trial.getPartner().getPartnerID();
				}
				else {
					// offer expired
					url = "#";
				}
			}
			catch (Exception e) {
			}
		}
		
		return url;
	}

	public boolean isTrialPartnershipActive() {
		boolean active = false;
		if (this.trial != null) {
			active = this.trial.getPartner().getIsOfferCurrentlyRunning();
		}
			
		return active;	
	}
}
