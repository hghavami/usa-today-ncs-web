package com.usatoday.ncs.web.subscription.handlers;

import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.businessObjects.util.mail.SmtpMailSender;
import com.usatoday.util.constants.UsaTodayConstants;

public class EmailRecordHandler {

	private EmailRecordIntf emailRecord = null;
	private boolean selectedInTable = true;
	private String message = "";
	
	public EmailRecordIntf getEmailRecord() {
		return emailRecord;
	}

	public void setEmailRecord(EmailRecordIntf emailRecord) {
		this.emailRecord = emailRecord;
	}
	
	public String getEmailAddress () {
		
		if (this.emailRecord != null) {
			return this.emailRecord.getEmailAddress();
		}
		return null;
	}

	public void setEmailAddress(String newEmail){
		
		if (this.emailRecord != null) {
			try {
				this.emailRecord.setEmailAddress(newEmail);
			}
			catch (Exception e) {				
				e.printStackTrace();
			}
		}
	}
	
	public String getAccountNumber() {
		if (this.emailRecord != null) {
			return this.emailRecord.getAccountNumber();
		}
		return null;
	}
	
	public String getPubCode() {
		if (this.emailRecord != null) {
			return this.emailRecord.getPubCode();
		}
		return null;
	}
	
	public String getPassword() {
		
		if (this.emailRecord != null) {
			return this.emailRecord.getPassword();
		}
		return null;
		
	}
	
	public void setPassword(String newPassword) {
		if (this.emailRecord != null) {
			this.emailRecord.setPassword(newPassword);
		}
	}
	
	public String getGiftRecordFlag() {
		if (this.emailRecord != null) {
			if (this.emailRecord.isGift()) {
				return "Yes";
			}
			return "No";
		}
		return null;
	}
	
	public void setGiftRecordFlag(String indicator) {
		if (this.emailRecord != null) {			
			if ("no".equalsIgnoreCase(indicator)) {
				this.emailRecord.setGift(false);
			}
			else {
				this.emailRecord.setGift(true);
			}
		}
	}

	public boolean isSelectedInTable() {
		return selectedInTable;
	}

	public void setSelectedInTable(boolean selectedInTable) {
		this.selectedInTable = selectedInTable;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getStartDateFormatted() {
		StringBuilder sb = new StringBuilder();
		if (this.emailRecord != null ) {
			String sDate = this.emailRecord.getPermStartDate();
			if (sDate != null && sDate.length() == 8) {
				try {
					// 20100812
					sb.append(sDate.substring(4, 6));
					sb.append("/");
					sb.append(sDate.substring(6));
					sb.append("/");
					sb.append(sDate.substring(0, 4));
				}
				catch (Exception e) {
					sb = new StringBuilder();
					sb.append(sDate);
				}
			}
		}
		return sb.toString();
	}
	
	public void sendForgotPasswordEmail() throws Exception {
		String pubName = "USA TODAY";
		String phone = "1-800-872-0001";
		String footer = "USA TODAY, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		
		SmtpMailSender mail = new SmtpMailSender();
		mail.setMailServerHost(UsaTodayConstants.EMAIL_SERVER);
		mail.setSender(UsaTodayConstants.EMAIL_DEFAULT_FROM_ADDRESS);
		
		String messageSubject = null;
		if (this.getEmailRecord().getPubCode().equalsIgnoreCase(UsaTodayConstants.BW_PUBCODE)) {
			messageSubject = "Sports Weekly Subscription Notification";
			pubName = "Sports Weekly";
			phone = "1-800-872-1415";
			footer = "USA TODAY Sports Weekly, 7950 Jones Branch Dr, McLean, Virginia, 22108";
		} else {
			messageSubject = "USA TODAY Subscription Notification";
		}
		
		mail.setMessageSubject(messageSubject);

        StringBuilder forgotPasswordEmailText = new StringBuilder();
        
        forgotPasswordEmailText.append("\n\nThank you for informing ");
        forgotPasswordEmailText.append(pubName);
        forgotPasswordEmailText.append(" of your subscription needs.  Our records indicate your email address and password are as follows: \n\n");
        forgotPasswordEmailText.append("Email Address: ").append(this.getEmailRecord().getEmailAddress());
        forgotPasswordEmailText.append("\nPassword: ").append(this.getEmailRecord().getPassword());
        forgotPasswordEmailText.append("\n\nIf you have any questions about your subscription, please call us at ");
        forgotPasswordEmailText.append(phone);
        forgotPasswordEmailText.append(".");
        forgotPasswordEmailText.append("\n\n");
        forgotPasswordEmailText.append(footer);
 			
        mail.setMessageText(forgotPasswordEmailText.toString());

        mail.addTORecipient(this.getEmailRecord().getEmailAddress());

        mail.sendMessage();
		
	}
}
