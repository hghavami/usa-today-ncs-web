package com.usatoday.ncs.web.subscription.handlers;

import com.usatoday.business.interfaces.partners.TrialPartnerIntf;

public class PartnerHandler {

	private TrialPartnerIntf partner = null;

	public TrialPartnerIntf getPartner() {
		return partner;
	}

	public void setPartner(TrialPartnerIntf partner) {
		this.partner = partner;
	}
	
	public boolean getIncludesCompletePageCustomText() {
		boolean includesText = false;
		
		if (this.partner != null && this.partner.getCompletePageCustomHTML() != null && this.partner.getCompletePageCustomHTML().length() > 0) {
			includesText = true;
		}
		return includesText;
	}

	public boolean getIncludesDisclaimerCustomText() {
		boolean includesText = false;
		
		if (this.partner != null && this.partner.getDisclaimerCustomHTML() != null && this.partner.getDisclaimerCustomHTML().length() > 0) {
			includesText = true;
		}
		return includesText;
	}
	
	public String getPartnerID() {
		String pID = "";
		if (this.partner != null) {
			pID = this.partner.getPartnerID();
		}
		return pID;
	}
	
	public void setPartnerID(String pID) {
		; // never set the partner id, this method exist so faces works right.
		  // if we ever allow customer to choose partner then we will use this method.
	}
	
	public boolean getIsPartnerValid() {
		boolean pValid = false;
		
		if (this.partner != null && this.partner.getIsOfferCurrentlyRunning() && this.partner.getActive()) {
			pValid = true;
		}
		return pValid;
	}
	
	/**
	 * 		
	 * @return
	 */
	public boolean getIsShowLogoImage() {
		boolean showIt = false;
		
		if (this.partner != null && this.partner.getLogoImageURL() != null && this.partner.getLogoImageURL().length() > 0) {
			showIt = true;
		}
		return showIt;		
	}
	
	public boolean getIsShowLogoImageURL() {
		boolean showIt = false;
		
		if (this.partner != null && this.partner.getLogoLinkURL() != null && this.partner.getLogoLinkURL().length() > 0) {
			showIt = true;
		}
		return showIt;		
	}
	
	public String getParnterLogoLinkURL () {
		String url = "#";
		if (this.partner != null && this.partner.getLogoLinkURL() != null) {
			url = this.partner.getLogoLinkURL();
		}
		return url;
	}
	
	public String getClubNumberFieldLabel() {
		String label = "";
		if (this.partner != null && this.partner.getClubNumberLabel() != null) {
			label = this.partner.getClubNumberLabel();
		}
		return label;
	}
}
