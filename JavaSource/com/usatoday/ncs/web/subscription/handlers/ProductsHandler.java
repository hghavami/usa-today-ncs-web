package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;
import java.util.Collection;

import javax.faces.model.SelectItem;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.PersistentProductIntf;
import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.integration.ProductDAO;

public class ProductsHandler {

	private Collection<ProductHandler> products = null;

	public Collection<ProductHandler> getProducts() {
		if (this.products == null) {
			this.products = new ArrayList<ProductHandler>();
			try {
			    ProductDAO pDAO = new ProductDAO();
			    Collection<PersistentProductIntf> productIntfs = pDAO.searchForProducts("");
		        try {
		        	for (PersistentProductIntf pProd : productIntfs) {
		        		SubscriptionProductIntf tempProd = new SubscriptionProductBO(pProd);
		        		ProductHandler ph = new ProductHandler();
		        		ph.setProduct(tempProd);
		        		
		        		products.add(ph);
		        		
		        	}
		        }
		        catch(Exception e) {
		            throw new UsatException(e);
		        }
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return products;
	}

	public void setProducts(Collection<ProductHandler> products) {
		this.products = products;
	}
		
	public Collection<SelectItem> getAvailableProductsSelectItems() {
		Collection<SelectItem> items = new ArrayList<SelectItem>();
		
		Collection<ProductHandler> productCol = this.getProducts();
		
		for(ProductHandler productHandler : productCol) {
			ProductIntf p = productHandler.getProduct();
			SubscriptionProductIntf product = null;
			if (p instanceof SubscriptionProductIntf) {
				product = (SubscriptionProductIntf)p;
			}
			else {
				continue;
			}
	        SelectItem si = new javax.faces.model.SelectItem();
	        si.setDescription(product.getDescription());
	        si.setLabel(product.getName());
	        si.setValue(product.getProductCode());
	        items.add(si);
		}
		return items;
	}
	
	public ProductHandler getProductForPub(String pubCode) {
		ProductHandler prod = null;
		
		for (ProductHandler p : this.products) {
			if (p.getProductCode().equalsIgnoreCase(pubCode)) {
				prod = p;
			}
		}
		
		return prod;
	}
	
	public void refresh() {
		if (this.products != null) {
			this.products.clear();
			this.products = null;
		}
		this.getProducts();
	}
}
