package com.usatoday.ncs.web.subscription.handlers;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import com.usatoday.integration.KeycodeTO;
import com.usatoday.integration.SubscriptionTermsDAO;

@SuppressWarnings("unchecked")
public class OfferLocatorTypeAhead extends AbstractMap {

	private String keyCode = null;
	private String pubCode = null;
	
	
	@Override 
	public Set entrySet() {
		
		return null;
	}

	@Override
	public Object get(Object key) {
		
		String filter = null;
		Collection<String> suggestions = new ArrayList<String>();
		
		try {
			filter = (String) key;
		
			if (filter != null && (filter.equalsIgnoreCase("pubcode") || filter.equalsIgnoreCase("keycode"))) {
				filter = null;
			}
			
			if (filter != null) {
				filter = filter.trim();
			}
			
			SubscriptionTermsDAO dao = new SubscriptionTermsDAO();
			
			if (this.pubCode == null) {
				this.pubCode = "UT";
			}
			
			if (filter != null) {
				filter = filter.trim();
				if (filter.length() > 0) {
					String srcCode = null;
					String promoCode = "%";
					String contestCode = "%";
					String offerCode = "%";
					
					srcCode = filter.substring(0, 1);
					
					if (filter.length() > 3) {
						// keycode entered clear offer code, 99999 should result in 0 results
						offerCode = "99999";
					}
					else {
						offerCode = filter + "%";
					}
					
					try {
						if (filter.length() == 2){
							promoCode = filter.substring(1) + "%";
						}
						else if (filter.length() > 2) {
							promoCode = filter.substring(1, 3);
						}
					}
					catch (Exception e) {
						// ignore : handle exception
					}
					
					try {
						if (filter.length() == 4){
							contestCode = filter.substring(3) + "%";
						}
						else if (filter.length() > 4) {
							contestCode = filter.substring(3, 5);
						}
					}
					catch (Exception e) {
						// ignore : handle exception
					}
					
					// return a collection of suggestions for the specified key (filter);
					Collection<KeycodeTO> offers = dao.getOffersForCriteria(this.pubCode, srcCode, promoCode, contestCode, offerCode);
					for (KeycodeTO offer : offers) {
						StringBuilder offerString = new StringBuilder();
						
						offerString.append(offer.getKeyCode());
						offerString.append(" | ");
						if (offer.getOfferCode() == null || offer.getOfferCode().trim().length() == 0) {
							offerString.append("n/a");
						}
						else {
							offerString.append(offer.getOfferCode());
						}
						offerString.append(" (").append(offer.getPubCode());
						offerString.append(" - ").append(offer.getDescription()).append(")");
						
						suggestions.add(offerString.toString()); 			
						
					}
				}
				// end if filter entered
				
			}
			
		}
		catch (Exception e) {
			System.out.println("CampaignGroupNameSuggestionsHandler(): Failed to generate suggestions. " + e.getMessage());
			e.printStackTrace();
		}
		
		if (suggestions.size() == 0) {
			suggestions.add("No matching offers");
		}
		
		return suggestions;
	}

	public String getKeyCode() {
		return keyCode;
	}

	public void setKeyCode(String keyCode) {
		this.keyCode = keyCode;
	}

	public String getPubCode() {
		return pubCode;
	}

	public void setPubCode(String pubCode) {
		this.pubCode = pubCode;
	}
	
}
