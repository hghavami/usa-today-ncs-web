package com.usatoday.ncs.web.subscription.handlers;

import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;

public class SubscriptionTermHandler {

	private SubscriptionTermsIntf term = null;
	private SubscriptionOfferHandler parent = null;
    private static final java.text.NumberFormat currencyFormat = java.text.NumberFormat.getCurrencyInstance();
	private boolean selected = false;

	public SubscriptionTermsIntf getTerm() {
		return term;
	}

	public void setTerm(SubscriptionTermsIntf term) {
		this.term = term;
	}

	public SubscriptionOfferHandler getParent() {
		return parent;
	}

	public void setParent(SubscriptionOfferHandler parent) {
		this.parent = parent;
	}
	
	public String getTermDuration() {
		String value = "";
		if (this.term != null) {
			value = this.term.getDurationInWeeks() + " Weeks";
		}
		return value;
	}
	
	public String getTermDailyRate() {
		String value = "";
		if (this.term != null) {
			value = SubscriptionTermHandler.currencyFormat.format(this.term.getDailyRate()) + " per issue";
		}
		return value;
	}

	public String getTermPrice() {
		String value = "";
		if (this.term != null) {
			value = SubscriptionTermHandler.currencyFormat.format(this.term.getPrice());
		}
		return value;
	}
	
	public String getRequiresEZPayString() {
		String value = "unknown";
		if (this.term != null) {
			if (this.term.requiresEZPAY() || this.parent.getIsForceEZPAY()) {
				value = "YES";
			}
			else {
				value = "NO";
			}
		}
		return value;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	public String getFullTermDescription() {
		StringBuilder desBuilder = new StringBuilder();
		
		desBuilder.append(this.getTermDuration());
		desBuilder.append(" for ");
		desBuilder.append(this.getTermPrice());
		desBuilder.append("  (").append(this.getTermDailyRate()).append(")");
		
		
		return desBuilder.toString();
	}
}
