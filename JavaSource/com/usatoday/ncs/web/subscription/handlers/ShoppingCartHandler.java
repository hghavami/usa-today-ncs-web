package com.usatoday.ncs.web.subscription.handlers;

import java.util.Date;

import org.joda.time.DateTime;

import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.business.interfaces.products.SubscriptionTermsIntf;
import com.usatoday.business.interfaces.products.promotions.JavaScriptPromotionIntf;
import com.usatoday.business.interfaces.shopping.OrderIntf;
import com.usatoday.business.interfaces.shopping.OrderItemIntf;
import com.usatoday.business.interfaces.shopping.ShoppingCartIntf;
import com.usatoday.business.interfaces.shopping.SubscriptionOrderItemIntf;
import com.usatoday.business.interfaces.shopping.payment.CreditCardPaymentMethodIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.promotions.PromotionSet;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.businessObjects.shopping.ShoppingCart;
import com.usatoday.businessObjects.shopping.SubscriptionOrderItemBO;
import com.usatoday.util.constants.UsaTodayConstants;

public class ShoppingCartHandler {

	private ShoppingCartIntf cart = null;
	private OrderIntf lastOrder = null;
	private boolean isZeroDollarAmount = false;
	private boolean isNotZeroDollar = true;
	private boolean isListServicesEEKeycode = false;
	private boolean isListServicesUTKeycode = false;
	

	public ShoppingCartHandler() {
		super();
		// initialize the cart. The order will not be set until an order is placed and the cart is cleared
		this.cart = new ShoppingCart();
	}

	public boolean getIsZeroDollarAmount() {			
		return this.isZeroDollarAmount;
	}
	
	public void setIsZeroDollarAmount(boolean isZeroDollarAmount) {
		this.isZeroDollarAmount = isZeroDollarAmount;
	}
	
	public void setIsNotZeroDollar(boolean isNotZeroDollar) {
		this.isNotZeroDollar = isNotZeroDollar;
	}
	
	public boolean getIsNotZeroDollar() {			
		return this.isNotZeroDollar;
	}
	
		
	public ShoppingCartIntf getCart() {
		return cart;
	}

	public void setCart(ShoppingCartIntf cart) {
		this.cart = cart;
	}

	public OrderIntf getLastOrder() {
		return lastOrder;
	}

	public void setLastOrder(OrderIntf lastOrder) {
		this.lastOrder = lastOrder;
	}
	
	public String getNewUTeEditionLink() {
		String link = "#";
		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery() || (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE) && UsaTodayConstants.UT_EE_COMPANION_ACTIVE)) {
					try {
						String email = this.lastOrder.getDeliveryContact().getEmailAddress();
						SubscriptionProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);
						link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, email, null,null, false);
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					break;
				}
			}
		}
		return link;
	}

	public String getNewUTTutorialLink() {
		String link = "#";
		if (this.lastOrder != null) {
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item.getProduct().isElectronicDelivery()) {
					try {
						link = item.getProduct().getSupplier().getTutorialURL();
					}
					catch (Exception e){
						;
					}
				}
			}
		}
		return link;
	}
	
	public SubscriptionOrderItemIntf getOrderedSubscriptionItem() {
		SubscriptionOrderItemIntf item = null;
		try {
			if (this.lastOrder != null) {
				for (OrderItemIntf tempItem : this.lastOrder.getOrderedItems()) {
					if (tempItem instanceof SubscriptionOrderItemIntf) {
						item = (SubscriptionOrderItemIntf)tempItem;
						break;
					}
				}
			}			
		}
		catch (Exception e) {
			
		}
		return item;
	}
	
	public boolean getPaidByCreditCard() {
		boolean paidByCredit = true;
		try {
			if (!this.cart.getPaymentMethod().getPaymentType().requiresPaymentProcessing()) {
				paidByCredit = false;
			}
		}
		catch (Exception e) {
			;
		}
		return paidByCredit;
	}
	
	public String getCreditCardExpirationString() {
		String expString = "";
		try {
			if (this.cart.getPaymentMethod().getPaymentType().requiresPaymentProcessing()) {
				CreditCardPaymentMethodIntf ccMethod = (CreditCardPaymentMethodIntf)this.cart.getPaymentMethod();
				expString = ccMethod.getExpirationMonth() + "/" + ccMethod.getExpirationYear();
			}			
		}
		catch (Exception e) {
			;
		}
		
		return expString;
	}
	
	public Date getDateOrderPlaced() {
		try {
			return this.lastOrder.getDateTimePlaced().toDate();
		}
		catch (Exception e) {
			DateTime dt = new DateTime();
			return dt.toDate();
		}
		
	}
	
	public Date getStartDateOfSub() {
		try {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			
			
			return item.getStartDateObj().toDate();
		}
		catch (Exception e) {
			
		}
		return null;		
	}
	
	public String getSelectedEZPayString() {
		String choseEZPay = "No";
		try {
			
			for (OrderItemIntf item : this.lastOrder.getOrderedItems()) {
				if (item instanceof SubscriptionOrderItemIntf) {
					SubscriptionOrderItemIntf sItem = (SubscriptionOrderItemIntf)item;
					if (sItem.isChoosingEZPay()) {
						choseEZPay = "Yes";
						break;
					}
				}
			}
		}
		catch (Exception e) {
		}
		return choseEZPay;
	}

	/**
	 * @param isLinkServicesKeycode the isLinkServicesKeycode to set
	 */
	public void setListServicesEEKeycode(boolean isLinkServicesKeycode) {
		this.isListServicesEEKeycode = isLinkServicesKeycode;
	}

	/**
	 * @return the isLinkServicesKeycode
	 */
	public boolean isListServicesEEKeycode() {
		isListServicesEEKeycode = false;
        if (UsaTodayConstants.LIST_SERVICES_EE_KEYCODES.indexOf(this.getOrderedSubscriptionItem().getKeyCode()) > -1) {
        	isListServicesEEKeycode = true;
        }
		return isListServicesEEKeycode;
	}

	/**
	 * @param isLinkServicesUTKeycode the isLinkServicesUTKeycode to set
	 */
	public void setListServicesUTKeycode(boolean isLinkServicesUTKeycode) {
		this.isListServicesUTKeycode = isLinkServicesUTKeycode;
	}

	/**
	 * @return the isLinkServicesUTKeycode
	 */
	public boolean isListServicesUTKeycode() {
		isListServicesUTKeycode = false;
        if (UsaTodayConstants.LIST_SERVICES_UT_KEYCODES.indexOf(this.getOrderedSubscriptionItem().getKeyCode()) > -1) {
    		isListServicesUTKeycode = true;
    	}
		return isListServicesUTKeycode;
	}
	
	public boolean isShowElectronicLinks() {
		boolean showLinks = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getProduct().isElectronicDelivery()) {
				showLinks = true;
			}
			else if (item.getProduct().getBrandingPubCode().equalsIgnoreCase(UsaTodayConstants.UT_PUBCODE)&& com.usatoday.util.constants.UsaTodayConstants.UT_EE_COMPANION_ACTIVE) {
				showLinks = true;
			}
		}
		
		return showLinks;
	}
	
	public boolean isShowEEWelcomePanel() {
		boolean showPanel = false;

		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			if (item.getProduct().getProductCode().equalsIgnoreCase(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE)) {
				showPanel = true;
			}
		}
		
		return showPanel;
	}
	
	public String getLastSubscriptionItemTermDuration() {
		String termDes = "";
		if (this.lastOrder != null) {
			SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
			SubscriptionTermsIntf term = item.getSelectedTerm();
			
			termDes = term.getDurationInWeeks();
			int firstIndex = termDes.indexOf('0');
			if (firstIndex > -1) {
				int index = 0;
				boolean trimmed = false;
				while (index < termDes.length() && !trimmed) {
					if (termDes.charAt(index) == '0') {
						index++;
					}
					else {
						termDes = termDes.substring(index);
						trimmed = true;
					}
				}
			}
				
		}
		return termDes;
	}
	
	public String getOnePageThankYouJavaScript1Text() {
		String jScript = null;
		
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
				
				PromotionSet pSet = item.getOffer().getPromotionSet();
				
				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript1Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(), this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(), item.getDeliveryEmailAddress());					
				}
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript2Text() {
		String jScript = null;
		
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
				
				PromotionSet pSet = item.getOffer().getPromotionSet();
				
				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript2Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(), this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(), item.getDeliveryEmailAddress());					
				}
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
		}
		return jScript;
	}

	public String getOnePageThankYouJavaScript3Text() {
		String jScript = null;
		
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
				
				PromotionSet pSet = item.getOffer().getPromotionSet();
				
				JavaScriptPromotionIntf jPromo = pSet.getPromoOnePageThankYouJavaScript3Text();
				if (jPromo != null) {
					jScript = jPromo.getDynamicJavaScript(this.lastOrder.getOrderID(), this.lastOrder.getTotal(), this.lastOrder.getSubTotal(), item.getKeyCode(), item.getProduct().getProductCode(), item.getDeliveryEmailAddress());					
				}
			}
		}
		catch (Exception e) {
			//e.printStackTrace();
		}
		return jScript;
	}
	
	public String getDeliveryMethodDescription() {

		String deliveryMethod = "";
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
				
				String method = item.getDeliveryMethod();
				if (method != null) {
					if (item.getProduct().isElectronicDelivery()) {
						deliveryMethod = "Electronic Delivery";
					}
					else if (method.equalsIgnoreCase("M")) {
						deliveryMethod = "Mail Delivery";
					}
					else if (method.equalsIgnoreCase("C")) {
						deliveryMethod = "Morning Delivery";
					}
					
				}
			}
		}
		catch (Exception e) {
		}
		return deliveryMethod;
	}
	
	public boolean isShowDeliveryMethod() {

		boolean showIt = false;
		
		try {
			if (this.lastOrder != null) {
				SubscriptionOrderItemIntf item = this.getOrderedSubscriptionItem();
				
				if (item.getProduct().isElectronicDelivery()) {
					showIt = true;
				}
				else {
					
					if (item instanceof SubscriptionOrderItemBO) {
						SubscriptionOrderItemBO itemBO = (SubscriptionOrderItemBO)item;
						if (itemBO.isDeliveryMethodCheck()) {
							showIt = true;
						}
					}
				}
			}
		}
		catch (Exception e) {
		}
		
		return showIt;
	}
}
