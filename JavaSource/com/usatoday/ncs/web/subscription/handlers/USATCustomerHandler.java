package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.business.interfaces.customer.CustomerIntf;
import com.usatoday.business.interfaces.customer.EmailRecordIntf;
import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;

public class USATCustomerHandler {

	private CustomerIntf customer = null;
	
	public CustomerIntf getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerIntf customer) {
		this.customer = customer;
	}

	public String getCurrentAccountEZPAyStatusString() {
		String ezPay = "No";
		try {
			if (this.customer != null) {
				SubscriberAccountIntf acct = this.customer.getCurrentAccount();
				if (acct != null) {
					if( acct.isOnEZPay()) {
						ezPay = "Yes";
					}
					}
				}
			}
		catch (Exception e) {
			ezPay = "";
		}
		return ezPay;
	}
	
	public String getCustomerEmailAddress() {
		// we can do this because customers only sign on with a single email address
		String value = "";
		try {
			value = this.customer.getCurrentEmailRecord().getEmailAddress();
		}
		catch (Exception e) {
		}
		return value;
	}
	
	public Collection<SubscriptionAccountHandler> getCustomerAccounts() {
		ArrayList<SubscriptionAccountHandler> accounts = new ArrayList<SubscriptionAccountHandler>();
		try {
			for(SubscriberAccountIntf acct : this.customer.getAccounts().values()) {
				SubscriptionAccountHandler sah = new SubscriptionAccountHandler();
				sah.setAccount(acct);
				accounts.add(sah);
			}
		}
		catch (Exception e) {
		}
		return accounts;		
	}
	
	public int getNumberOfAccounts() {
		int numAccounts = 0;
		try {
			numAccounts = this.customer.getNumberOfAccounts();			
		}
		catch (Exception e) {
		}		
		return numAccounts;
	}
	
	public boolean hasNewOrExpiredAccount() {
		boolean hasNewOrExpired = false;
		try {
			hasNewOrExpired = this.customer.hasAccountWithNoAccountNumber();		
		}
		catch (Exception e) {
		}		
		return hasNewOrExpired;
	}
	
	public SubscriptionAccountHandler getCurrentAccount() {
		SubscriptionAccountHandler sah = null;
		try {
			SubscriberAccountIntf sa = this.customer.getCurrentAccount();
			if (sa != null) {
				sah = new SubscriptionAccountHandler();
				sah.setAccount(sa);
			}
		}
		catch (Exception e) {
		}		
		return sah;
	
	}

	public EmailRecordHandler getCurrentEmailRecord() {
		EmailRecordHandler eh = null;
		try {
			EmailRecordIntf er = this.customer.getCurrentEmailRecord();
			if (er != null) {
				eh = new EmailRecordHandler(); 
				eh.setEmailRecord(er);
			}
		}
		catch (Exception e) {
		}		
		return eh;
	
	}
	
	
}
