package com.usatoday.ncs.web.subscription.handlers;

import javax.el.ELContext;
import javax.faces.context.FacesContext;

import com.usatoday.business.interfaces.products.ProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.products.utils.ElectronicEditionFulfillmentHelper;
import com.usatoday.ncs.web.handlers.UserHandler;
import com.usatoday.util.constants.UsaTodayConstants;

public class EEProductHandler {

	
	public String getENewspaperLink() {
		String  link = "";
		try {
			
			ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			UserHandler uh 
			    = (UserHandler) FacesContext.getCurrentInstance().getApplication()
			        .getELResolver().getValue(elContext, null, "user");			
			
			String uid = uh.getSiteID()+ "-" + uh.getUserID();
			link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, uid, null, null, false);

		}
		catch (Exception e) {
			link = "";
		}		
		return link;
	}
	
	public String getENewspaperLinkAlternate() {
		String  link = "";
		try {
			
			ProductIntf p = SubscriptionProductBO.getSubscriptionProduct(UsaTodayConstants.DEFAULT_UT_EE_PUBCODE);

			ELContext elContext = FacesContext.getCurrentInstance().getELContext();
			UserHandler uh 
			    = (UserHandler) FacesContext.getCurrentInstance().getApplication()
			        .getELResolver().getValue(elContext, null, "user");			
			
			String uid = uh.getSiteID()+ "-" + uh.getUserID();
			link = ElectronicEditionFulfillmentHelper.buildOliveFulfillmentURL(p, uid, null, null, true);

		}
		catch (Exception e) {
			link = "";
		}		
		return link;
	}

}
