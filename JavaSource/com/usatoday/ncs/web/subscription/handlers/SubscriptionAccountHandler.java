package com.usatoday.ncs.web.subscription.handlers;

import com.usatoday.business.interfaces.customer.SubscriberAccountIntf;

public class SubscriptionAccountHandler {

		SubscriberAccountIntf account = null;

		public SubscriberAccountIntf getAccount() {
			return account;
		}

		public void setAccount(SubscriberAccountIntf account) {
			this.account = account;
		}
		
		
}
