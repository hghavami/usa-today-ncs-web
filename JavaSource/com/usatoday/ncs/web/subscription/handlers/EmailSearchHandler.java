package com.usatoday.ncs.web.subscription.handlers;

import java.util.ArrayList;
import java.util.Collection;

public class EmailSearchHandler {
	
	
	private Collection<EmailRecordHandler> customerSearchResults = null;
	
	private Collection<TrialCustomerHandler> trialSearchResults = null;
	
	private int numRecordsPerPage = 25;
	
	private Collection<EmailRecordHandler> sameEmailAddressRecords = null;
	
	public Collection<EmailRecordHandler> getCustomerSearchResults() {
		return customerSearchResults;
	}

	public Collection<EmailRecordHandler> getCustomerSearchResultsMatchingEmail(String filterEmailAddress) {
		ArrayList<EmailRecordHandler> matchingRecords = new ArrayList<EmailRecordHandler>();
		
		if (this.customerSearchResults != null && this.customerSearchResults.size() > 0) {
			for (EmailRecordHandler rec : this.customerSearchResults) {
				if (rec.getEmailAddress().equalsIgnoreCase(filterEmailAddress)) {
					rec.setMessage(""); // clear any message on this record
					matchingRecords.add(rec);
				}
			}
		}
		return matchingRecords;
	}
	
	public void setCustomerSearchResults(Collection<EmailRecordHandler> customerSearchResults) {
		if (this.customerSearchResults != null) {
			this.customerSearchResults.clear();
		}
		this.customerSearchResults = customerSearchResults;
	}
	public int getNumRecordsPerPage() {
		return numRecordsPerPage;
	}
	public void setNumRecordsPerPage(int numRecordsPerPage) {
		this.numRecordsPerPage = numRecordsPerPage;
	}

	public int getNumberOfSearchResults() {
		int numResults = 0;
		if (this.customerSearchResults != null) {
			numResults += this.customerSearchResults.size();
		}
		if (this.trialSearchResults != null)  {
			numResults += this.trialSearchResults.size();
		}
		
		return numResults;
	}

	public int getNumberOfCustomerSearchResults() {
		int numResults = 0;
		if (this.customerSearchResults != null) {
			numResults = this.customerSearchResults.size();
		}
		return numResults;
	}
	
	public int getNumberOfTrialSearchResults() {
		int numResults = 0;
		if (this.trialSearchResults != null)  {
			numResults = this.trialSearchResults.size();
		}
		
		return numResults;
	}
	
	public int getNumberOfInstancesOfSelectedEmail() {
		int numResults = 0;
		if (this.sameEmailAddressRecords != null) {
			numResults = this.sameEmailAddressRecords.size();
		}
		return numResults;
	}

	public boolean getHasSearchResults() {
		boolean hasResults = false;
		if (this.customerSearchResults != null && this.customerSearchResults.size() > 0) {
			hasResults = true;
		}
		
		if (!hasResults) {
			if (this.trialSearchResults != null && this.trialSearchResults.size() > 0) {
				hasResults = true;
			}
		}
		return hasResults;
	}
	
	public boolean getHasCustomerSearchResults() {
		boolean hasResults = false;
		if (this.customerSearchResults != null && this.customerSearchResults.size() > 0) {
			hasResults = true;
		}
		return hasResults;
	}

	public boolean getHasTrialSearchResults() {
		boolean hasResults = false;
		if (this.trialSearchResults != null && this.trialSearchResults.size() > 0) {
			hasResults = true;
		}
		return hasResults;
	}
	
	
	public Collection<TrialCustomerHandler> getTrialSearchResults() {
		return trialSearchResults;
	}
	public void setTrialSearchResults(
			Collection<TrialCustomerHandler> trialSearchResults) {
		if (this.trialSearchResults != null) {
			this.trialSearchResults.clear();
		}
		this.trialSearchResults = trialSearchResults;
	}

	public Collection<EmailRecordHandler> getSameEmailAddressRecords() {
		return sameEmailAddressRecords;
	}

	public void setSameEmailAddressRecords(
			Collection<EmailRecordHandler> sameEmailAddressRecords) {
		this.sameEmailAddressRecords = sameEmailAddressRecords;
	}
	
	public void reset() {
		if (this.customerSearchResults != null) {
			this.customerSearchResults.clear();
		}
		if (this.trialSearchResults != null) {
			this.trialSearchResults.clear();
		}
		if (this.sameEmailAddressRecords != null) {
			this.sameEmailAddressRecords.clear();
		}
	}
}
