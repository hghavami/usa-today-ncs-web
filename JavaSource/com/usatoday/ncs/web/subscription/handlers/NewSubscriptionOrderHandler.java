package com.usatoday.ncs.web.subscription.handlers;

import java.util.Calendar;

import com.usatoday.business.interfaces.shopping.payment.PaymentTypeIntf;

public class NewSubscriptionOrderHandler {

	private String customerEnteredOfferCode = null;
	
	private String selectedTerm = null;  // default to the 4 week term
	
	private String password = null;
	private String confirmPassword = null;
	
	private boolean startedAsFreeSample = false;
	private String partnerName = null;
	private String clubNumber = null;
	
	private java.util.Date startDate = null;
	
	private String deliveryEmailAddress = null;
	private String deliveryEmailAddressConfirmation = null;
	private String deliveryFirstName = null;
	private String deliveryLastName = null;
	private String deliveryCompanyName = null;
	private String deliveryAddress1 = null;
	private String deliveryAddress2 = "";
	private String deliveryAptSuite = null;
	private String deliveryCity = null;
	private String deliveryState = null;
	private String deliveryZipCode = null;
	private String deliveryPhoneAreaCode = null;
	private String deliveryPhoneExchange = null;
	private String deliveryPhoneExtension = null;
	private String deliveryWorkPhoneAreaCode = null;
	private String deliveryWorkPhoneExchange = null;
	private String deliveryWorkPhoneExtension = null;
	
	private String deliveryMethodText = "Not Determined";
	
	private boolean billingDifferentThanDelivery = false;
	private boolean giftSubscription = false;
	private boolean gannettUnit = false;
	
	private String billingEmailAddress = null;
	private String billingEmailAddressConfirmation = null;
	private String billingFirstName = null;
	private String billingLastName = null;
	private String billingCompanyName = null;
	private String billingAddress1 = null;
	private String billingAddress2 = "";
	private String billingAptSuite = null;
	private String billingCity = null;
	private String billingState = null;
	private String billingZipCode = null;
	private String billingPhoneAreaCode = null;
	private String billingPhoneExchange = null;
	private String billingPhoneExtension = null;
	
	private String paymentMethod = "CC";
	
	private String creditCardNumber = null;
	private String creditCardExpirationMonth = null;
	private String creditCardExpirationYear = null;
	private String creditCardCVVNumber = null;
	
	private String renewalMethod = "AUTOPAY_PLAN";
	private String renewalMethodGift = "AUTOPAY_PLAN";	

	private int quantity = 1;
	
	private SubscriptionTermHandler selectedTermHandler = null;
	
	public NewSubscriptionOrderHandler() {
		super();
	}

	public String getRenewalMethodGift() {
		return renewalMethodGift;
	}

	public void setRenewalMethodGift(String renewalMethodGift) {
		this.renewalMethodGift = renewalMethodGift;
	}

	public String getSelectedTerm() {
		return selectedTerm;
	}

	public void setSelectedTerm(String selectedTerm) {
		this.selectedTerm = selectedTerm;
	}

	public String getDeliveryEmailAddress() {
		return deliveryEmailAddress;
	}

	public void setDeliveryEmailAddress(String deliveryEmailAddress) {
		this.deliveryEmailAddress = deliveryEmailAddress;
	}

	public String getDeliveryEmailAddressConfirmation() {
		return deliveryEmailAddressConfirmation;
	}

	public void setDeliveryEmailAddressConfirmation(
			String deliveryEmailAddressConfirmation) {
		this.deliveryEmailAddressConfirmation = deliveryEmailAddressConfirmation;
	}

	public String getDeliveryFirstName() {
		return deliveryFirstName;
	}

	public void setDeliveryFirstName(String deliveryFirstName) {
		this.deliveryFirstName = deliveryFirstName;
	}

	public String getDeliveryLastName() {
		return deliveryLastName;
	}

	public void setDeliveryLastName(String deliveryLastName) {
		this.deliveryLastName = deliveryLastName;
	}

	public String getDeliveryAddress1() {
		return deliveryAddress1;
	}

	public void setDeliveryAddress1(String deliveryAddress1) {
		this.deliveryAddress1 = deliveryAddress1;
	}

	public String getDeliveryAddress2() {
		return deliveryAddress2;
	}

	public void setDeliveryAddress2(String deliveryAddress2) {
		this.deliveryAddress2 = deliveryAddress2;
	}

	public String getDeliveryAptSuite() {
		return deliveryAptSuite;
	}

	public void setDeliveryAptSuite(String deliveryAptSuite) {
		this.deliveryAptSuite = deliveryAptSuite;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public String getDeliveryState() {
		return deliveryState;
	}

	public void setDeliveryState(String deliveryState) {
		this.deliveryState = deliveryState;
	}

	public String getDeliveryZipCode() {
		return deliveryZipCode;
	}

	public void setDeliveryZipCode(String deliveryZipCode) {
		this.deliveryZipCode = deliveryZipCode;
	}

	public String getDeliveryPhoneAreaCode() {
		return deliveryPhoneAreaCode;
	}

	public void setDeliveryPhoneAreaCode(String deliveryPhoneAreaCode) {
		this.deliveryPhoneAreaCode = deliveryPhoneAreaCode;
	}

	public String getDeliveryPhoneExchange() {
		return deliveryPhoneExchange;
	}

	public void setDeliveryPhoneExchange(String deliveryPhoneExchange) {
		this.deliveryPhoneExchange = deliveryPhoneExchange;
	}

	public String getDeliveryPhoneExtension() {
		return deliveryPhoneExtension;
	}

	public void setDeliveryPhoneExtension(String deliveryPhoneExtension) {
		this.deliveryPhoneExtension = deliveryPhoneExtension;
	}

	public boolean isBillingDifferentThanDelivery() {
		return billingDifferentThanDelivery;
	}

	public void setBillingDifferentThanDelivery(boolean billingDifferentThanDelivery) {
		this.billingDifferentThanDelivery = billingDifferentThanDelivery;
	}

	public String getBillingEmailAddress() {
		return billingEmailAddress;
	}

	public void setBillingEmailAddress(String billingEmailAddress) {
		this.billingEmailAddress = billingEmailAddress;
	}

	public String getBillingEmailAddressConfirmation() {
		return billingEmailAddressConfirmation;
	}

	public void setBillingEmailAddressConfirmation(
			String billingEmailAddressConfirmation) {
		this.billingEmailAddressConfirmation = billingEmailAddressConfirmation;
	}

	public String getBillingFirstName() {
		return billingFirstName;
	}

	public void setBillingFirstName(String billingFirstName) {
		this.billingFirstName = billingFirstName;
	}

	public String getBillingLastName() {
		return billingLastName;
	}

	public void setBillingLastName(String billingLastName) {
		this.billingLastName = billingLastName;
	}

	public String getBillingAddress1() {
		return billingAddress1;
	}

	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}

	public String getBillingAddress2() {
		return billingAddress2;
	}

	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}

	public String getBillingAptSuite() {
		return billingAptSuite;
	}

	public void setBillingAptSuite(String billingAptSuite) {
		this.billingAptSuite = billingAptSuite;
	}

	public String getBillingCity() {
		return billingCity;
	}

	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}

	public String getBillingState() {
		return billingState;
	}

	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}

	public String getBillingZipCode() {
		return billingZipCode;
	}

	public void setBillingZipCode(String billingZipCode) {
		this.billingZipCode = billingZipCode;
	}

	public String getBillingPhoneAreaCode() {
		return billingPhoneAreaCode;
	}

	public void setBillingPhoneAreaCode(String billingPhoneAreaCode) {
		this.billingPhoneAreaCode = billingPhoneAreaCode;
	}

	public String getBillingPhoneExchange() {
		return billingPhoneExchange;
	}

	public void setBillingPhoneExchange(String billingPhoneExchange) {
		this.billingPhoneExchange = billingPhoneExchange;
	}

	public String getBillingPhoneExtension() {
		return billingPhoneExtension;
	}

	public void setBillingPhoneExtension(String billingPhoneExtension) {
		this.billingPhoneExtension = billingPhoneExtension;
	}

	public boolean getIsChoseBillMe() {
		if (this.paymentMethod.equalsIgnoreCase(PaymentTypeIntf.INVOICE_PAYMENT_TYPE)) {
			return true;
		}
		
		return false;
	}

	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	public String getCreditCardExpirationMonth() {
		return creditCardExpirationMonth;
	}

	public void setCreditCardExpirationMonth(String creditCardExpirationMonth) {
		this.creditCardExpirationMonth = creditCardExpirationMonth;
	}

	public String getCreditCardExpirationYear() {
		return creditCardExpirationYear;
	}

	public void setCreditCardExpirationYear(String creditCardExpirationYear) {
		this.creditCardExpirationYear = creditCardExpirationYear;
	}

	public String getCreditCardCVVNumber() {
		return creditCardCVVNumber;
	}

	public void setCreditCardCVVNumber(String creditCardCVVNumber) {
		this.creditCardCVVNumber = creditCardCVVNumber;
	}

	public String getRenewalMethod() {
		return renewalMethod;
	}

	public void setRenewalMethod(String renewalMethod) {
		this.renewalMethod = renewalMethod;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void resetForNewOrder() {
		this.billingAddress1 = "";
		this.billingAddress2 = "";
		this.billingAptSuite = "";
		this.billingCity = "";
		this.billingDifferentThanDelivery = false;
		this.billingEmailAddress = "";
		this.billingFirstName = "";
		this.billingLastName = "";
		this.billingPhoneAreaCode = "";
		this.billingPhoneExchange = "";
		this.billingPhoneExtension = "";
		this.billingState = "";
		this.creditCardCVVNumber = "";
		this.creditCardNumber = "";
		this.creditCardExpirationMonth = "";
		this.creditCardExpirationYear = "";
		this.deliveryAddress1 = "";
		this.deliveryAddress2 = "";
		this.deliveryAptSuite = "";
		this.deliveryCity = "";
		this.deliveryEmailAddress = "";
		this.deliveryEmailAddressConfirmation = "";
		this.deliveryFirstName = "";
		this.deliveryLastName = "";
		this.deliveryPhoneAreaCode = "";
		this.deliveryPhoneExchange = "";
		this.deliveryPhoneExtension = "";
		this.deliveryState = "";
		this.deliveryZipCode = "";
		this.paymentMethod = "CC";
		this.renewalMethod = "AUTOPAY_PLAN";
		this.renewalMethodGift = "AUTOPAY_PLAN";
		this.selectedTerm = null;
		this.clubNumber = "";
		this.partnerName = "";
		this.startDate = Calendar.getInstance().getTime();
		this.deliveryMethodText = null;
		this.gannettUnit = false;
		this.quantity = 1;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getClubNumber() {
		return clubNumber;
	}

	public void setClubNumber(String clubNumber) {
		this.clubNumber = clubNumber;
	}

	public java.util.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public String getPartnerName() {
		if (partnerName != null) {
			return partnerName;
		}
		else {
			return "";
		}
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public boolean getIsStartedAsFreeSample() {
		return startedAsFreeSample;
	}

	public void setStartedAsFreeSample(boolean startedAsFreeSample) {
		this.startedAsFreeSample = startedAsFreeSample;
	}
	
	public boolean isGiftSubscription() {
		return giftSubscription;
	}

	public void setGiftSubscription(boolean giftSubscription) {
		this.giftSubscription = giftSubscription;
	}

	public boolean isValidDeliveryEmailAndDeliveryConfirmationEmail() {
		// validate delivery email
		String e1 = this.getDeliveryEmailAddress();
		String e2 = this.getDeliveryEmailAddressConfirmation();

		boolean validDeliveryEmail = true;
		
		if (e1 == null || e2 == null) {
			validDeliveryEmail = false;
		}
		else {
			e1 = e1.trim();
			e2 = e2.trim();
			if (!e1.equalsIgnoreCase(e2)) {
				validDeliveryEmail = false;
			}
			else {
				this.setDeliveryEmailAddress(e1);
				this.setDeliveryEmailAddressConfirmation(e2);
			}
		}
		
		return validDeliveryEmail;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isGannettUnit() {
		return gannettUnit;
	}

	public void setGannettUnit(boolean gannettUnit) {
		this.gannettUnit = gannettUnit;
	}

	public String getDeliveryMethodText() {
		return deliveryMethodText;
	}

	public void setDeliveryMethodText(String deliveryMethodText) {
		this.deliveryMethodText = deliveryMethodText;
	}

	public String getDeliveryCompanyName() {
		return deliveryCompanyName;
	}

	public void setDeliveryCompanyName(String deliveryCompanyName) {
		this.deliveryCompanyName = deliveryCompanyName;
	}

	public String getBillingCompanyName() {
		return billingCompanyName;
	}

	public void setBillingCompanyName(String billingCompanyName) {
		this.billingCompanyName = billingCompanyName;
	}

	public String getDeliveryWorkPhoneAreaCode() {
		return deliveryWorkPhoneAreaCode;
	}

	public void setDeliveryWorkPhoneAreaCode(String deliveryWorkPhoneAreaCode) {
		this.deliveryWorkPhoneAreaCode = deliveryWorkPhoneAreaCode;
	}

	public String getDeliveryWorkPhoneExchange() {
		return deliveryWorkPhoneExchange;
	}

	public void setDeliveryWorkPhoneExchange(String deliveryWorkPhoneExchange) {
		this.deliveryWorkPhoneExchange = deliveryWorkPhoneExchange;
	}

	public String getDeliveryWorkPhoneExtension() {
		return deliveryWorkPhoneExtension;
	}

	public void setDeliveryWorkPhoneExtension(String deliveryWorkPhoneExtension) {
		this.deliveryWorkPhoneExtension = deliveryWorkPhoneExtension;
	}

	public String getCustomerEnteredOfferCode() {
		return customerEnteredOfferCode;
	}

	public void setCustomerEnteredOfferCode(String customerEnteredOfferCode) {
		this.customerEnteredOfferCode = customerEnteredOfferCode;
	}

	public void setSelectedTermHandler(SubscriptionTermHandler selectedTerm) {
		this.selectedTermHandler = selectedTerm;
	}

	public SubscriptionTermHandler getSelectedTermHandler() {
		return this.selectedTermHandler;
	}
}
