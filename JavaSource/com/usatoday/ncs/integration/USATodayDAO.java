/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.integration;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class USATodayDAO
 * 
 * Base class for classes dealing with a DB.
 * 
 */
public class USATodayDAO {

    protected static String jndiDataSourceName = "java:comp/env/esub";
  	protected static String jndiDataSourceNameV2 = "jdbc/esub";
    private static DataSource db = null;
    

    /**
     * 
     */
    public USATodayDAO() {
        super();
        
        if (USATodayDAO.db == null) {
	        try {
	            Context ctx = new InitialContext();
	            USATodayDAO.db = (DataSource) ctx.lookup(USATodayDAO.jndiDataSourceName);
	        }
	        catch (Exception e) {
	            System.out.println("NCS APPLICATION: Failed to lookup Data source: " + USATodayDAO.jndiDataSourceName + " Message: " + e.getMessage() + ".  trying secondary.");
	            try {
		            Context ctx = new InitialContext();
		            USATodayDAO.db = (DataSource) ctx.lookup(USATodayDAO.jndiDataSourceNameV2);
				} catch (Exception e2) {
					System.out.println("NCS APPLICATION: Failed to lookup Secondary Data source: " + USATodayDAO.jndiDataSourceNameV2 + " Message: " + e.getMessage() );				}
	        }
        }
    }

    protected Connection getDBConnection() throws Exception {
        Connection connection = null;
        
        connection = db.getConnection();
        
        return connection;
    }
    
    protected void cleanupConnection(Connection c) throws Exception {
        if (c != null) {
            c.close();
        }
    }

    /**
     * 
     * @param inputStr
     * @return
     */
    protected static String escapeApostrophes(String inputStr) {
        if (inputStr != null) {
            return inputStr.replaceAll("'", "''");
        }
        return null;
    }
    
}
