/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.ncs.transferObjects.CallCenterSiteTO;
import com.usatoday.ncs.transferObjects.SiteRoleTO;

/**
 * @author aeast
 * @date Nov 6, 2007
 * @class CallCenterSiteDAO
 * 
 * Represents a call center site or usat customer service site
 * 
 */
public class CallCenterSiteDAO extends USATodayDAO {

    private static final String SITE_FIELDS = "ID, active, usatLocation, siteID, password, ip_restriction_lower, ip_restriction_upper, description";
    private static final String SITE_TABLE = "NCS_Site";
    private static final String SITE_ROLE_XREF_FIELDS = "site_id, role_id";
    private static final String SITE_ROLE_XREF_TABLE = "NCS_Site_Role_Xref";
    
    /**
     * 
     */
    public CallCenterSiteDAO() {
        super();
  
    }

    public CallCenterSiteTO authenticateSite(String siteID, String password) throws Exception {
        CallCenterSiteTO site = null;
        
        Collection<CallCenterSiteTO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();
    		
    		siteID = siteID.toUpperCase();
    		
    		// prevent SQL Injection
    		siteID = USATodayDAO.escapeApostrophes(siteID);
    		password = USATodayDAO.escapeApostrophes(password);
    		
    		String sql = "select " + CallCenterSiteDAO.SITE_FIELDS + " from " + CallCenterSiteDAO.SITE_TABLE + " where siteID = '" + siteID + "' and password = '" + password + "' and active = 'Y'";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
            
            statement.close();

            this.cleanupConnection(conn);
            
            if (records.size() == 1) {
                this.loadRoles(records);
                
                site = (CallCenterSiteTO)records.iterator().next();
            }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("CallCenterSiteDAO : authenticateSite() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return site;
    }
    
    /**
     * 
     * @param siteKey
     * @return
     * @throws Exception
     */
    public CallCenterSiteTO fetchSite(long siteKey) throws Exception {
        CallCenterSiteTO site = null;
        
        Collection<CallCenterSiteTO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();
    		    		
    		String sql = "select " + CallCenterSiteDAO.SITE_FIELDS + " from " + CallCenterSiteDAO.SITE_TABLE + " where ID = " + siteKey;
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
            
            this.cleanupConnection(conn);
            
            if (records.size() == 1) {
                this.loadRoles(records);
                
                site = (CallCenterSiteTO)records.iterator().next();
            }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("CallCenterSiteDAO : fetchSite() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return site;
    }

    /**
     * 
     * @return
     * @throws Exception
     */
    public Collection<CallCenterSiteTO> fetchAllSites() throws Exception {
        Collection<CallCenterSiteTO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();
    		    		
    		String sql = "select " + CallCenterSiteDAO.SITE_FIELDS + " from " + CallCenterSiteDAO.SITE_TABLE;
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
            
            this.cleanupConnection(conn);
            
            this.loadRoles(records);
               		       		    
    	}
    	catch (Exception e) {
            System.out.println("CallCenterSiteDAO : fetchAllSites() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return records;
    }
    
    /*
     * 
     */
    private void loadRoles(Collection<CallCenterSiteTO> sites) throws Exception {
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

	    	for (CallCenterSiteTO site : sites) {
	            // populate roles
	    		String sql = "select " + CallCenterSiteDAO.SITE_ROLE_XREF_FIELDS + " from " + CallCenterSiteDAO.SITE_ROLE_XREF_TABLE + " where site_id = " + site.getKey();
	    		
	    		PreparedStatement statement = conn.prepareStatement(sql);
	            
	            ResultSet rs = statement.executeQuery();
	            
	            Collection<SiteRoleTO> roles = this.objectFactoryRoles(rs);
	            
	            site.setRoles(roles);
	    	}
    	}
    	catch (Exception e) {
            System.out.println("CallCenterSiteDAO : loadRoles() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
    	
    }
    
    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<CallCenterSiteTO> objectFactory(ResultSet rs) throws Exception {
        Collection<CallCenterSiteTO> records = new ArrayList<CallCenterSiteTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                CallCenterSiteTO to = new CallCenterSiteTO();
                
                //ID, active, siteID, password, ip_restriction, description

                long id = rs.getLong("ID");
                to.setKey(id);
                
                String tempStr = rs.getString("active"); 
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    to.setActive(true);
                }
                else {
                    to.setActive(false);
                }
                
                tempStr = rs.getString("usatLocation"); 
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    to.setUsatLocation(true);
                }
                else {
                    to.setUsatLocation(false);
                }

                tempStr = rs.getString("siteID");  //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    to.setSiteID(tempStr.trim());
                }
                
                tempStr = rs.getString("description");  //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    to.setDescription(tempStr.trim());
                }
                else {
                    to.setDescription("");
                }
                
                tempStr = rs.getString("password");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    to.setPassword(tempStr.trim());
                }
                else {
                    to.setPassword("");
                }
                
                tempStr = rs.getString("ip_restriction_lower");
                if (tempStr != null && tempStr.trim().length() > 1) {
                    to.setIpRestrictionLower(tempStr.trim());
                }
                
                tempStr = rs.getString("ip_restriction_upper");
                if (tempStr != null && tempStr.trim().length() > 1) {
                    to.setIpRestrictionUpper(tempStr.trim());
                }

                records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("MarketDAO::objectFactoryMarkets() - Failed to create Market TO object: " + e.getMessage());
        }
        
        return records;
    }

    private Collection<SiteRoleTO> objectFactoryRoles(ResultSet rs) throws Exception {
        Collection<SiteRoleTO> records = new ArrayList<SiteRoleTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){
            	SiteRoleTO to = new SiteRoleTO();
            	
            	String tempStr = rs.getString("role_id");
            	to.setRoleID(tempStr.trim());
            	
            	long siteID = rs.getLong("site_id");
            	to.setSiteID(siteID);
            	
            	records.add(to);
            }
        }
        catch (Exception e) {
            System.out.println("MarketDAO::objectFactoryMarkets() - Failed to create Market TO object: " + e.getMessage());
        }
        
        return records;
    }
    
}
