/*
 * Created on Nov 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.ncs.bo.KeycodeBO;

/**
 * @author aeast
 * @date Nov 8, 2007
 * @class IncidentDAO
 * 
 * Interfaces with the Incident DB
 * 
 */
public class KeycodeDAO extends USATodayDAO {

    public static final String TABLE_NAME = "XTRNTKEYRT";
   
	private static final String XTRNTKEYRT_OPERID_FIELDS = "PubCode,SrcOrderCode,PromoCode,ContestCode,OfferDesc,OperatorNum";
    
    /**
     * 
     */
    public KeycodeDAO() {
        super();
    }

    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<KeycodeBO> objectFactory(ResultSet rs) throws Exception {
        Collection<KeycodeBO> records = new ArrayList<KeycodeBO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

               KeycodeBO keycode = new KeycodeBO();
                
               String tempStr = rs.getString("PubCode");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setPubCode(tempStr.trim());                   
               }
                              
               tempStr = rs.getString("SrcOrderCode");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setSourceCode(tempStr.trim());                   
               }
               
               tempStr = rs.getString("PromoCode");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setPromoCode(tempStr.trim());                   
               }
               

               tempStr = rs.getString("ContestCode");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setContestCode(tempStr.trim());                   
               }
               

               tempStr = rs.getString("OfferDesc");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setOfferDesc(tempStr.trim());                   
               }
               
               tempStr = rs.getString("OperatorNum");    
               if (tempStr != null && tempStr.trim().length() > 0) {
                   // description
            	   keycode.setOfferKey(tempStr.trim());                   
               }
               
               records.add(keycode);
            }
        }
        catch (Exception e) {
            System.out.println("KeycodeDAO::objectFactory() - Failed to create Keycode object: " + e.getMessage());
        }
        
        return records;
    }

	
    
    public KeycodeBO fetchKeycode(String pubCode, String offerKey) throws Exception {
        Collection<KeycodeBO> records = null;
        KeycodeBO keycode = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + KeycodeDAO.XTRNTKEYRT_OPERID_FIELDS + " from " + KeycodeDAO.TABLE_NAME + " where OperatorNum = ?" + " and PubCode = ?";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    statement.setString(1, offerKey);
    	    statement.setString(2, pubCode);
    	    
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
   		    
            if (records.size() == 1) {
                keycode = (KeycodeBO)records.iterator().next();
            }
    	}
    	catch (Exception e) {
            System.out.println("IncidentDAO : fetchIncident() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return keycode;
        
    }
}
