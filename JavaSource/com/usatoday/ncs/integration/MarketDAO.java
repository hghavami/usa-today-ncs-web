/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.integration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import com.usatoday.ncs.transferObjects.MarketContactTO;
import com.usatoday.ncs.transferObjects.MarketTO;
import com.usatoday.ncs.util.ZeroFilledNumeric;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketDAO
 * 
 * Retrieves data from market table
 * 
 */
public class MarketDAO extends USATodayDAO {

    public static final String MARKET_FIELDS = "ID, marketID, phone, description";
    public static String MARKET_TABLE_NAME = "NCS_market";

    public static final String MARKET_CONTACT_FIELDS = "ID, owningMarket, active, primaryContact, firstName, lastName, phone, phone_extension, email";
    public static String MARKET_CONTACT_TABLE_NAME = "NCS_market_contact";
    
    private static final String MARKET_LOCATOR_BY_ZIP = "{call USAT_NCS_LOCATE_MARKET(?, ?)}";
    
    /**
     * 
     */
    public MarketDAO() {
        super();
    }


    /**
     * 
     * @return A collection of valid markets.
     * @throws Exception
     */
    public Collection<MarketTO> fetchMarkets() throws Exception {
        Collection<MarketTO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + MarketDAO.MARKET_FIELDS + " from " + MarketDAO.MARKET_TABLE_NAME + " order by marketID";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactoryMarkets(rs);
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarkets() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }

    /**
     * 
     * @param key
     * @return
     * @throws Exception
     */
    public MarketTO fetchMarket(long key) throws Exception {
        Collection<MarketTO> records = null;
       	java.sql.Connection conn = null;
       	MarketTO market = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + MarketDAO.MARKET_FIELDS + " from " + MarketDAO.MARKET_TABLE_NAME + " where ID = " + key;
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactoryMarkets(rs);
            
            if (records.size() == 1) {
                market = records.iterator().next();
            }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarket() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return market;
    }
    
    /**
     * 
     * @param marketID
     * @return
     * @throws Exception
     */
    public MarketTO fetchMarketByMarketID(String marketID) throws Exception {
        Collection<MarketTO> records = null;
       	java.sql.Connection conn = null;
       	MarketTO market = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		marketID = ZeroFilledNumeric.getValue(2, marketID);
    		marketID = USATodayDAO.escapeApostrophes(marketID);
    		
    		String sql = "select " + MarketDAO.MARKET_FIELDS + " from " + MarketDAO.MARKET_TABLE_NAME + " where marketID = '" + marketID + "'";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactoryMarkets(rs);
            
            if (records.size() == 1) {
                market = records.iterator().next();
            }
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarket() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return market;
    }
    
    /**
     * 
     * @param marketID
     * @return
     * @throws Exception
     */
    public Collection<MarketContactTO> fetchMarketContacts(long marketID) throws Exception {
        Collection<MarketContactTO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + MarketDAO.MARKET_CONTACT_FIELDS + " from " + MarketDAO.MARKET_CONTACT_TABLE_NAME + " where owningMarket = " + marketID + " and active = 'Y'";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactoryMarketContacts(rs);
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : fetchMarketContacts() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
    }

/**
 * 
 * @param zip
 * @return
 * @throws Exception
 */
    public String getMarketIDByZip(String zip) throws Exception {
        
        String marketID = "";
       	java.sql.Connection conn = null;

       	try {
    	    
    		conn = this.getDBConnection();

            java.sql.CallableStatement statement = conn.prepareCall(MarketDAO.MARKET_LOCATOR_BY_ZIP);
            
            statement.setString(1, USATodayDAO.escapeApostrophes(zip));
            statement.registerOutParameter(2, Types.CHAR);
   		    
            statement.execute();
            
            marketID = statement.getString(2);
            
    	}
    	catch (Exception e) {
            System.out.println("MarketDAO : locateMarketIDByZip() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return marketID;
    }
    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<MarketTO> objectFactoryMarkets(ResultSet rs) throws Exception {
        Collection<MarketTO> records = new ArrayList<MarketTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                MarketTO mTO = new MarketTO();
                
                String tempStr = rs.getString("marketID"); // CHAR 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    mTO.setMarketID(tempStr.trim());
                }
                else {
                    mTO.setMarketID("");
                }
                
                tempStr = rs.getString("description");  //  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    mTO.setDescription(tempStr.trim());
                }
                else {
                    mTO.setDescription("");
                }
                
                tempStr = rs.getString("phone");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setPhone(tempStr.trim());
                }
                else {
                    mTO.setPhone("");
                }

                long id = rs.getLong("ID");
                mTO.setId(id);
                
                records.add(mTO);
            }
        }
        catch (Exception e) {
            System.out.println("MarketDAO::objectFactoryMarkets() - Failed to create Market TO object: " + e.getMessage());
        }
        
        return records;
    }

    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<MarketContactTO> objectFactoryMarketContacts(ResultSet rs) throws Exception {
        Collection<MarketContactTO> records = new ArrayList<MarketContactTO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                MarketContactTO mTO = new MarketContactTO();
                
                // ID, owningMarket, active, primaryContact, firstName, 
                // lastName, phone, phone_extension, email
                long id = rs.getLong("ID");
                mTO.setId(id);
                
                id = rs.getLong("owningMarket");
                mTO.setOwningMarketKey(id);
                
                String tempStr = rs.getString("active"); // CHAR 
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    mTO.setActive(true);
                }
                else {
                    mTO.setActive(false);
                }
                
                tempStr = rs.getString("primaryContact");  //  
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    // description
                    mTO.setPrimaryContact(true);
                }
                else {
                    mTO.setPrimaryContact(false);
                }
                
                tempStr = rs.getString("firstName");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    mTO.setFirstName(tempStr.trim());
                }
                else {
                    mTO.setFirstName("");
                }
                
                tempStr = rs.getString("lastName");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    mTO.setLastName(tempStr.trim());
                }
                else {
                    mTO.setLastName("");
                }
                
                tempStr = rs.getString("phone");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setPhone(tempStr.trim());
                }
                else {
                    mTO.setPhone("");
                }

                tempStr = rs.getString("phone_extension");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setPhoneExtension(tempStr.trim());
                }
                else {
                    mTO.setPhoneExtension("");
                }
                
                tempStr = rs.getString("email");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    mTO.setEmail(tempStr.trim());
                }
                else {
                    mTO.setEmail("");
                }

                records.add(mTO);
            }
        }
        catch (Exception e) {
            System.out.println("MarketDAO::objectFactoryMarketContacts() - Failed to create Market Contact TO object: " + e.getMessage());
        }
        
        return records;
    }
    
}
