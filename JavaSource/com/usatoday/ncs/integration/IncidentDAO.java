/*
 * Created on Nov 8, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.integration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;

import com.usatoday.ncs.bo.IncidentBO;

/**
 * @author aeast
 * @date Nov 8, 2007
 * @class IncidentDAO
 * 
 * Interfaces with the Incident DB
 * 
 */
public class IncidentDAO extends USATodayDAO {

    public final static String NEW_INCIDENT = "NEW";
    public final static String ACKNOWLEDGED_INCIDENT = "ACKNOWLEDGED";
    public final static String WORKING_INCIDENT = "WORKING";
    public final static String RESOLVED_INCIDENT = "RESOLVED";
    
    public static final String TABLE_NAME = "NCS_incident";
    
    public static final String INCIDENT_FIELDS = "ID, marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType, incidentRequestType," +
    		"incidentLocName, insert_timestamp, update_timestamp, incidentDate, updatedBy, enteredBy, incidentAddress, incidentCity," +
    		"incidentState, custAccountNum, custFirstName, custLastName, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, " +
    		"deptid, deptName, deptContactID, deptContact, deptEmailAddress, incidentNotes, responseNotes";

    private static final String UPDATE = "update NCS_incident set marketKey=?, ncsSiteKey=?, incidentZip=?, status=?, incidentType=?, " +
    		"incidentLocName=?, update_timestamp=?, incidentDate=?, updatedBy=?, incidentAddress=?, incidentCity=?, " +
    		"incidentState=?, custAccountNum=?, custFirstName=?, custLastName=?, custAdd1=?, custAdd2=?, custCity=?, custState=?, custZip=?, custPhone=?, custEmail=?, " +
    		"deptid=?, deptName=?, deptContactID=?, deptContact=?, deptEmailAddress=?, incidentNotes=?, responseNotes=?,  incidentRequestType=?, refundRequest=? where id = ?";
    
    /**
     * 
     */
    public IncidentDAO() {
        super();
    }

    /**
     * 
     * @param rs
     * @return
     * @throws Exception
     */
    private Collection<IncidentBO> objectFactory(ResultSet rs) throws Exception {
        Collection<IncidentBO> records = new ArrayList<IncidentBO>();
        
        try {
            // iterate over result set and build objects
            
            while (rs.next()){

                IncidentBO incident = new IncidentBO();

            //  "ID, marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType,incidentRequestType" +
        	//	"incidentLocName, insert_timestamp, update_timestamp, incidentDate, updatedBy, enteredBy, incidentAddress, incidentCity," +
        	//	"incidentState, custFirstName, custLastName, custAccountNum, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, " +
        	//	"deptid, deptName, deptContactID, deptContact, deptEmailAddress, incidentNotes, responseNotes";
                
                long id = rs.getLong("ID");
                incident.setId(id);
                
                id = rs.getLong("marketKey");
                incident.setMarketKey(id);
                
                id = rs.getLong("ncsSiteKey");
                incident.setNcsSiteKey(id);
                
                String tempStr = rs.getString("incidentZip");  
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentZip(tempStr);
                }
                else {
                    incident.setIncidentZip("");
                }
                
                tempStr = rs.getString("refundRequest");    
                if (tempStr != null && tempStr.trim().equalsIgnoreCase("Y")) {
                    // description
                    incident.setRefundRequest(true);
                }
                else {
                    incident.setRefundRequest(false);
                }
                
                tempStr = rs.getString("status");    
                if (tempStr != null && tempStr.trim().length() > 0) {
                    // description
                    incident.setStatus(tempStr);
                }
                else {
                    incident.setStatus("");
                }
                
                tempStr = rs.getString("incidentType");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentType(tempStr);
                }
                else {
                    incident.setIncidentType("");
                }

                tempStr = rs.getString("incidentRequestType");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentRequestType(tempStr);
                }
                else {
                    incident.setIncidentRequestType("");
                }
                
                tempStr = rs.getString("incidentLocName");  // 
                if (tempStr != null && tempStr.trim().length() > 0) {
                    incident.setIncidentLocationName(tempStr);
                }
                else {
                    incident.setIncidentLocationName("");
                }
                
                Timestamp ts = rs.getTimestamp("insert_timestamp");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setInserted(dt);
                }

                ts = rs.getTimestamp("update_timestamp");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setUpdated(dt);
                }

                ts = rs.getTimestamp("incidentDate");
                if (ts != null) {
                    DateTime dt = new DateTime(ts.getTime());
                    incident.setIncidentDate(dt.toDate());
                }
                
                tempStr = rs.getString("updatedBy");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setUpdatedBy(tempStr);
                }
                else {
                    incident.setUpdatedBy("");
                }
                
                tempStr = rs.getString("enteredBy");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setEnteredBy(tempStr.trim());
                }
                else {
                    incident.setEnteredBy("");
                }

                tempStr = rs.getString("incidentAddress");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentAddress(tempStr.trim());
                }
                else {
                    incident.setIncidentAddress("");
                }
                
                tempStr = rs.getString("incidentCity");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentCity(tempStr.trim());
                }
                else {
                    incident.setIncidentCity("");
                }
                tempStr = rs.getString("incidentState");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setIncidentState(tempStr.trim());
                }
                else {
                    incident.setIncidentState("");
                }
                
                tempStr = rs.getString("custFirstName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerFirstName(tempStr.trim());
                }
                else {
                    incident.setCustomerFirstName("");
                }

                tempStr = rs.getString("custLastName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerLastName(tempStr.trim());
                }
                else {
                    incident.setCustomerLastName("");
                }
                
                tempStr = rs.getString("custAccountNum");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAccountNumber(tempStr.trim());
                }
                else {
                    incident.setCustomerAccountNumber("");
                }

                tempStr = rs.getString("custAddr1");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAddress1(tempStr.trim());
                }
                else {
                    incident.setCustomerAddress1("");
                }

                tempStr = rs.getString("custAddr2");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerAddress2(tempStr.trim());
                }
                else {
                    incident.setCustomerAddress2("");
                }

                tempStr = rs.getString("custCity");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerCity(tempStr.trim());
                }
                else {
                    incident.setCustomerCity("");
                }

                tempStr = rs.getString("custState");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerState(tempStr.trim());
                }
                else {
                    incident.setCustomerState("");
                }
                
                tempStr = rs.getString("custZip");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerZip(tempStr.trim());
                }
                else {
                    incident.setCustomerZip("");
                }
                
                tempStr = rs.getString("custPhone");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerPhone(tempStr.trim());
                }
                else {
                    incident.setCustomerPhone("");
                }

                tempStr = rs.getString("custEmail");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setCustomerEmail(tempStr.trim());
                }
                else {
                    incident.setCustomerEmail("");
                }

                tempStr = rs.getString("deptid");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptID(tempStr.trim());
                }
                else {
                    incident.setDeptID("");
                }

                tempStr = rs.getString("deptName");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptName(tempStr.trim());
                }
                else {
                    incident.setDeptName("");
                }

                tempStr = rs.getString("deptContactID");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptContactID(tempStr.trim());
                }
                else {
                    incident.setDeptContactID("");
                }

                tempStr = rs.getString("deptContact");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptContact(tempStr.trim());
                }
                else {
                    incident.setDeptContact("");
                }

                tempStr = rs.getString("deptEmailAddress");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setDeptEmailAddress(tempStr.trim());
                }
                else {
                    incident.setDeptEmailAddress("");
                }

                tempStr = rs.getString("incidentNotes");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setNotes(tempStr.trim());
                }
                else {
                    incident.setNotes("");
                }

                tempStr = rs.getString("responseNotes");  // 
                if (tempStr != null && tempStr.trim().length() > 1) {
                    incident.setResponseNotes(tempStr.trim());
                }
                else {
                    incident.setResponseNotes("");
                }

                records.add(incident);
            }
        }
        catch (Exception e) {
            System.out.println("IncidentDAO::objectFactory() - Failed to create Incident object: " + e.getMessage());
        }
        
        return records;
    }

	/**
	 * 
	 * @param incident
	 * @return
	 * @throws Exception
	 */
    public IncidentBO insertIncident(IncidentBO incident) throws Exception {

        Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();
        
    		java.sql.Statement statement = conn.createStatement();
    		
    		StringBuffer sql = new StringBuffer("insert into NCS_incident (marketKey, ncsSiteKey, refundRequest, incidentZip, status, incidentType, incidentRequestType, incidentLocName, insert_timestamp, update_timestamp, incidentDate, enteredBy, incidentAddress, incidentCity, " +
    				"incidentState, custFirstName, custLastName, custAccountNum, custAddr1, custAddr2, custCity, custState, custZip, custPhone, custEmail, incidentNotes, responseNotes) values (");
		
    		if (incident.getMarketKey() > 0) {
    		    sql.append("").append(incident.getMarketKey()).append(",");
    		}
    		else {
    		    // default market key should be Headquarters
    		    sql.append("1,");
    		}
    		
    		if (incident.getNcsSiteKey() > 0) {
    		    sql.append("").append(incident.getNcsSiteKey().longValue()).append(",");
    		}
    		else {
    		    // default to first site
    		    sql.append("1,");
    		}
    		
    		if (incident.isRefundRequest()){
    		    sql.append("'Y'").append(",");
    		}
    		else {
    		    sql.append("'N'").append(",");
    		}
    		
    		if (incident.getIncidentZip() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentZip())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getStatus() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getStatus())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentType() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentType())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentRequestType() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentRequestType())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentLocationName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentLocationName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		// insert timestamp
    		Timestamp ts = new Timestamp(System.currentTimeMillis());
    		DateTime insertTS = new DateTime(ts.getTime());
    		sql.append("'").append(ts.toString());
    		sql.append("',");

    		// update timestamp
    		sql.append("'").append(ts.toString());
    		sql.append("',");
    		
    		// incident date
    		if (incident.getIncidentDate() != null) {
    		    Timestamp tempts = new Timestamp(incident.getIncidentDate().getTime());
    		    sql.append("'").append(tempts.toString());
    		    sql.append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getEnteredBy() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getEnteredBy())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentAddress() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentAddress())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getIncidentCity() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentCity())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		if (incident.getIncidentState() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getIncidentState())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerFirstName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerFirstName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerLastName() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerLastName())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerAccountNumber() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAccountNumber())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerAddress1() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAddress1())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerAddress2() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerAddress2())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}

    		// 	"custCity, custState, custZip, custPhone, custEmail, incidentNotes, responseNotes) values (");
    		if (incident.getCustomerCity() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerCity())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerState() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerState())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getCustomerZip() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerZip())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerPhone() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerPhone())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		if (incident.getCustomerEmail() != null) {
    		    sql.append("'").append(USATodayDAO.escapeApostrophes(incident.getCustomerEmail())).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getNotes() != null) {
    		    String notes = USATodayDAO.escapeApostrophes(incident.getNotes());
    		    sql.append("'").append(notes).append("',");
    		}
    		else {
    		    sql.append("null,");
    		}
    		
    		if (incident.getResponseNotes() != null) {
    		    String notes = USATodayDAO.escapeApostrophes(incident.getResponseNotes());
    		    sql.append("'").append(notes).append("',");
    		}
    		else {
    		    sql.append("null)");
    		}
    		
    		//execute the SQL
    		int rowsAffected = statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS );

    		if (rowsAffected != 1) {
    		    throw new Exception("Incident Failed to Insert Or other error and yet no exception condition exists: Number rows affected: " + rowsAffected);
    		}

    		ResultSet rs = statement.getGeneratedKeys();
		
    		rs.next();
    		long primaryKey = rs.getLong(1);
		
    		incident.setId(primaryKey);
    		incident.setInserted(insertTS);
    		incident.setUpdated(insertTS);
		
    		statement.close();

    	}
    	catch (Exception e) {
            System.out.println("IncidentDAO : insertIncident() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        
        return incident;
    }
    
    /**
     * 
     * @param incident
     * @return
     * @throws Exception
     */
    public IncidentBO updateIncident(IncidentBO incident) throws Exception {

        int rowsAffected = 0;

        if (incident == null || incident.getId() <= 0) {
            throw new Exception("Null or Unsaved Incident object passed in for update.");
        }
        
    	java.sql.Connection conn = null;
    	try {

    		conn = this.getDBConnection();

    		java.sql.PreparedStatement statement = conn.prepareStatement(IncidentDAO.UPDATE);
    		
    	    // " marketKey=?, ncsSiteKey=?, incidentZip=?, status=?, incidentType=?, " +
    		// "incidentLocName=?, update_timestamp=?, incidentDate=?, updatedBy=?, incidentAddress=?, incidentCity=?, " +
    		// "incidentState=?, custAccountNum=?, custFirstName=?, custLastName=?, custAdd1=?, custAdd2=?, custCity=?, custState=?, custZip=?, custPhone=?, custEmail=?, " +
    		// "incidentNotes=?, responseNotes=?, incidentRequestType=?, refundRequest=? where id = ?";
    		
    		statement.setLong(1, incident.getMarketKey());
    		statement.setLong(2, incident.getNcsSiteKey());
    		
    		
    		if (incident.getIncidentZip() != null) {
          		statement.setString(3, USATodayDAO.escapeApostrophes(incident.getIncidentZip()).trim());
    		}
    		else {
    		    statement.setNull(3, Types.CHAR);
    		}

    		if (incident.getStatus() != null) {
          		statement.setString(4, USATodayDAO.escapeApostrophes(incident.getStatus()));
    		}
    		else {
    		    statement.setNull(4, Types.CHAR);
    		}

    		if (incident.getIncidentType() != null) {
          		statement.setString(5, USATodayDAO.escapeApostrophes(incident.getIncidentType()));
    		}
    		else {
    		    statement.setNull(5, Types.CHAR);
    		}
    		
    		if (incident.getIncidentLocationName() != null) {
          		statement.setString(6, USATodayDAO.escapeApostrophes(incident.getIncidentLocationName()));
    		}
    		else {
    		    statement.setNull(6, Types.CHAR);
    		}
   		    
    		Timestamp ts = new Timestamp(System.currentTimeMillis());
    		
   		    statement.setTimestamp(7, ts);
   		    
   		    if (incident.getIncidentDate() != null) {
   		        Timestamp iDate = new Timestamp(incident.getIncidentDate().getTime());
   		        statement.setTimestamp(8, iDate);
   		    }
   		    else {
   		        statement.setNull(8, Types.TIMESTAMP);
   		    }
   		    
    		if (incident.getUpdatedBy() != null) {
    		    statement.setString(9, USATodayDAO.escapeApostrophes(incident.getUpdatedBy()));
    		}
    		else {
    		    statement.setNull(9, Types.CHAR);
    		}

    		if (incident.getIncidentAddress() != null) {
    		    statement.setString(10, USATodayDAO.escapeApostrophes(incident.getIncidentAddress()));
    		}
    		else {
    		    statement.setNull(10, Types.CHAR);
    		}

    		if (incident.getIncidentCity() != null) {
    		    statement.setString(11, USATodayDAO.escapeApostrophes(incident.getIncidentCity()));
    		}
    		else {
    		    statement.setNull(11, Types.CHAR);
    		}

    		if ( incident.getIncidentState() != null) {
    		    statement.setString(12, USATodayDAO.escapeApostrophes(incident.getIncidentState()));
    		}
    		else {
    		    statement.setNull(12, Types.CHAR);
    		}
    		
    		if (incident.getCustomerAccountNumber() != null) {
    		    statement.setString(13, USATodayDAO.escapeApostrophes(incident.getCustomerAccountNumber()));
    		}
    		else {
    		    statement.setNull(13, Types.CHAR);

    		}

    		if (incident.getCustomerFirstName() != null) {
    		    statement.setString(14, USATodayDAO.escapeApostrophes(incident.getCustomerFirstName()));
    		}
    		else {
    		    statement.setNull(14, Types.CHAR);
    		}

    		if (incident.getCustomerLastName() != null) {
    		    statement.setString(15, USATodayDAO.escapeApostrophes(incident.getCustomerLastName()));
    		}
    		else {
    		    statement.setNull(15, Types.CHAR);
    		}

    		if (incident.getCustomerAddress1()!= null) {
    		    statement.setString(16, USATodayDAO.escapeApostrophes(incident.getCustomerAddress1()));
    		}
    		else {
    		    statement.setNull(16, Types.CHAR);
    		}
    		
    		if (incident.getCustomerAddress2() != null) {
    		    statement.setString(17, USATodayDAO.escapeApostrophes(incident.getCustomerAddress2()));
    		}
    		else {
    		    statement.setNull(17, Types.CHAR);
    		}
    		
    		if (incident.getCustomerCity() != null) {
    		    statement.setString(18, USATodayDAO.escapeApostrophes(incident.getCustomerCity()));
    		}
    		else {
    		    statement.setNull(18, Types.CHAR);
    		}
    		
    		if (incident.getCustomerState() != null) {
    		    statement.setString(19, USATodayDAO.escapeApostrophes(incident.getCustomerState()));
    		}
    		else {
    		    statement.setNull(19, Types.CHAR);
    		}
    		
    		if (incident.getCustomerZip() != null) {
    		    statement.setString(20, USATodayDAO.escapeApostrophes(incident.getCustomerZip()));
    		}
    		else {
    		    statement.setNull(20, Types.CHAR);
    		}
    		
    		if (incident.getCustomerPhone() != null) {
    		    statement.setString(21, USATodayDAO.escapeApostrophes(incident.getCustomerPhone()));
    		}
    		else {
    		    statement.setNull(21, Types.CHAR);
    		}

    		if (incident.getCustomerEmail() != null) {
    		    statement.setString(22, USATodayDAO.escapeApostrophes(incident.getCustomerEmail()));
    		}
    		else {
    		    statement.setNull(22, Types.CHAR);
    		}

    		if (incident.getDeptID() != null) {
    		    statement.setString(23, incident.getDeptID());
    		}
    		else {
    		    statement.setNull(23, Types.CHAR);
    		}
    		
    		if (incident.getDeptName() != null) {
    		    statement.setString(24, USATodayDAO.escapeApostrophes(incident.getDeptName()));
    		}
    		else {
    		    statement.setNull(24, Types.CHAR);
    		}
    		
    		if (incident.getDeptContactID() != null) {
    		    statement.setString(25, USATodayDAO.escapeApostrophes(incident.getDeptContactID()));
    		}
    		else {
    		    statement.setNull(25, Types.CHAR);
    		}
    		
    		if (incident.getDeptContact() != null) {
    		    statement.setString(26, USATodayDAO.escapeApostrophes(incident.getDeptContact()));
    		}
    		else {
    		    statement.setNull(26, Types.CHAR);
    		}

    		if (incident.getDeptEmailAddress() != null) {
    		    statement.setString(27, USATodayDAO.escapeApostrophes(incident.getDeptEmailAddress()));
    		}
    		else {
    		    statement.setNull(27, Types.CHAR);
    		}

    		if (incident.getNotes() != null) {
    		    statement.setString(28, USATodayDAO.escapeApostrophes(incident.getNotes().trim()));
    		}
    		else {
    		    statement.setNull(28, Types.CHAR);
    		}

    		if (incident.getResponseNotes() != null) {
    		    statement.setString(29, USATodayDAO.escapeApostrophes(incident.getResponseNotes().trim()));
    		}
    		else {
    		    statement.setNull(29, Types.CHAR);
    		}

    		if (incident.getIncidentRequestType() != null) {
    		    statement.setString(30, USATodayDAO.escapeApostrophes(incident.getIncidentRequestType().trim()));
    		}
    		else {
    		    statement.setNull(30, Types.CHAR);
    		}

    		if (incident.isRefundRequest()) {
    		    statement.setString(31, "Y");
    		}
    		else {
    		    statement.setString(31, "N");    		    
    		}
    		
    		// where clause
    		statement.setLong(32, incident.getId());
    		
    		// execute the SQL
    		rowsAffected = statement.executeUpdate();

    		if (rowsAffected != 1) {
    			throw new Exception("Incident Not Updated: Rows Affected=" + rowsAffected + " incident id=" + incident.getId());
    		}
    		
    		incident.setUpdated(new DateTime(ts.getTime()));
    		
    		statement.close();
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    		System.out.println( e.getMessage());
    		throw e;
    	}
    	finally {
    		this.cleanupConnection(conn);
    	}
                
        return incident;
    }
    

    /**
     * 
     * @return
     * @throws Exception
     */
    public Collection<IncidentBO> fetchNewIncidents() throws Exception {
        Collection<IncidentBO> records = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + IncidentDAO.INCIDENT_FIELDS + " from " + IncidentDAO.TABLE_NAME + " where status = 'NEW' order by ID";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
   		       		    
    	}
    	catch (Exception e) {
            System.out.println("IncidentDAO : fetchNewIncidents() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return records;
        
    }
    
    public IncidentBO fetchIncident(long incidentKey) throws Exception {
        Collection<IncidentBO> records = null;
        IncidentBO incident = null;
       	java.sql.Connection conn = null;
    	try {
    	    
    		conn = this.getDBConnection();

    		String sql = "select " + IncidentDAO.INCIDENT_FIELDS + " from " + IncidentDAO.TABLE_NAME + " where ID = ?";
    		
    	    PreparedStatement statement = conn.prepareStatement(sql);
            
    	    statement.setLong(1, incidentKey);
    	    
            ResultSet rs = statement.executeQuery();
   		    
            records = this.objectFactory(rs);
   		    
            if (records.size() == 1) {
                incident = (IncidentBO)records.iterator().next();
            }
    	}
    	catch (Exception e) {
            System.out.println("IncidentDAO : fetchIncident() " + e.getMessage());
            throw e;
        }
    	finally {
    	    if (conn != null){
    	        this.cleanupConnection(conn);
    	    }
    	}
        return incident;
        
    }
}
