/*
 * Created on Nov 7, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.util;

import java.util.StringTokenizer;

/**
 * @author aeast
 * @date Nov 7, 2007
 * @class IPToLongConverter
 * 
 * TODO Add a brief description of this class.
 * 
 */
public class IPToLongConverter {

    /**
     * 
     */
    public IPToLongConverter() {
        super();
    }

    /**
     * 
     * @param ip Must be in format of ###.###.###.###
     * @return
     */
    public static long convertIPStringToLong(String ip) {
        long ipAsLong = 0;
        if (ip == null) {
            return ipAsLong;
        }

        long octet1 = 0;
        long octet2 = 0;
        long octet3 = 0;
        long octet4 = 0;
        
        StringTokenizer tokenizer = new StringTokenizer(ip, ".");
        
        // if valid ip address
        if (tokenizer.countTokens() == 4) {
            // first octet
            String token = tokenizer.nextToken();
            octet1 = Long.parseLong(token);
            
            // second octet
            token = tokenizer.nextToken();
            octet2 = Long.parseLong(token);
            
            // third octet
            token = tokenizer.nextToken();
            octet3 = Long.parseLong(token);

            // fourth octet
            token = tokenizer.nextToken();
            octet4 = Long.parseLong(token);
            
            ipAsLong = (octet1 * 16777216) + (octet2 * 65536) + (octet3 * 256) + octet4;
        }
        
        return ipAsLong;
    }
}
