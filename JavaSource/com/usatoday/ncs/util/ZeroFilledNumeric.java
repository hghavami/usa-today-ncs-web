package com.usatoday.ncs.util;

/**
 * @author aeast
 *
 * To change this generated comment edit the template variable "typecomment":
 * Window>Preferences>Java>Templates.
 * To enable and disable the creation of type comments go to
 * Window>Preferences>Java>Code Generation.
 */
public class ZeroFilledNumeric {
	private int numberOfDigits = 0;
	private String filledValue = new String();
/**
 * ZeroFilledNumericObject constructor.
 */
private ZeroFilledNumeric(int numDigits) throws Exception {
	super();
	this.setNumberOfDigits(numDigits);
	this.setValue("");
}
/**
 * ZeroFilledNumeric constructor.
 */
private ZeroFilledNumeric(int numDigits, int intValue) throws Exception {
	this(numDigits);
	this.setValue(intValue);
}
/**
 * ZeroFilledNumeric constructor.
 */
private ZeroFilledNumeric(int numDigits, long longValue) throws Exception {
	this(numDigits);
	this.setValue(longValue);
}
/**
 * ZeroFilledNumeric constructor.
 */
private ZeroFilledNumeric(int numDigits, String stringValue) throws Exception {
	this(numDigits);
	this.setValue(stringValue);
}
/**
 * ZeroFilledNumeric constructor.
 */
private ZeroFilledNumeric(int numDigits, java.math.BigDecimal newValue) throws Exception {
	this(numDigits);
	this.setValue(newValue);
}
/**
 * This method was created in WSSD.
 * @return java.lang.String
 */
private String getFilledValue() {
	return filledValue;
}

/**
 * This method was created in WSSD.
 * @return int
 */
private int getNumberOfDigits() {
	return numberOfDigits;
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
private String getValue() {
	return this.getFilledValue();
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
public static String getValue(int numDigits, int intValue) throws Exception {
	return (new ZeroFilledNumeric(numDigits, intValue)).getValue();
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
public static String getValue(int numDigits, long longValue) throws Exception {
	return (new ZeroFilledNumeric(numDigits, longValue)).getValue();
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
public static String getValue(int numDigits, String stringValue) throws Exception {
	return (new ZeroFilledNumeric(numDigits, stringValue)).getValue();
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
public static String getValue(int numDigits, java.math.BigDecimal decimalValue) throws Exception {
	return (new ZeroFilledNumeric(numDigits, decimalValue)).getValue();
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
private void setFilledValue(String newValue) {
	this.filledValue = newValue;
}
/**
 * This method was created in WSSD.
 * @param newValue int
 */
private void setNumberOfDigits(int newValue) {
	this.numberOfDigits = newValue;
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
private void setValue(int newValue) throws Exception {
	String intString = String.valueOf(newValue);
	this.setValue(intString);
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
private void setValue(long newValue) throws Exception {
	String longString = String.valueOf(newValue);
	this.setValue(longString);
}
/**
 * This method was created in WSSD.
 * @param newValue java.lang.String
 */
private void setValue(String newValue) throws Exception {
	StringBuffer buffer = new StringBuffer(this.getNumberOfDigits());
	
	if (newValue == null)
		newValue = new String();
		
	int length = newValue.length();
	
	// now check that all entries are numeric
	for (int i = 0; i < length; i++) {
		if (!Character.isDigit(newValue.charAt(i)))
			throw new Exception("Non-numeric value is illegal");
	}

	// make sure value isn't too big
	if (length > this.getNumberOfDigits())
			throw new Exception("Value is out or range");
	
	
	for (int i = 0; i < 	 this.getNumberOfDigits() - length; i++)
		buffer.append('0');
		
	buffer.append(newValue);
	this.setFilledValue(buffer.toString());
}
/**
 * Create a zero filled string for the non-decimal portion of the BigDecimal
 * @param newValue java.math.BigDecimal
 */
private void setValue(java.math.BigDecimal newValue) throws Exception {
	String longString = String.valueOf(newValue.longValue());
	this.setValue(longString);
}
}
