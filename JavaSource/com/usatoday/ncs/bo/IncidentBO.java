/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.bo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import com.usatoday.ncs.integration.IncidentDAO;
import com.usatoday.ncs.so.MarketLocatorService;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class IncidentBO
 * 

 * 
 */
public class IncidentBO implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7359150944439260458L;
	private long id = 0;
    private long marketKey = 0;
    private Long ncsSiteKey = new Long(0);
    
    MarketBO marketAssigned = null;
    // ncs site that took incident
    
    private String status = "NEW";
    
    // specific to incident area
    private String incidentZip = null;
    private String incidentType = null;
    private String incidentRequestType = "";
    private boolean refundRequest = false;
    private String incidentLocationName = null;
    private String incidentAddress = null;
    private String incidentCity = null;
    private String incidentState = "";
    private String notes = null;
    private Date incidentDate = Calendar.getInstance().getTime();
    
    private DateTime inserted = null;
    private DateTime updated = null;
    
    private String enteredBy = null;
    private String updatedBy = null;
    
    // customer data
    private String customerFirstName = null;
    private String customerLastName = null;
    private String customerAddress1 = null;
    private String customerAddress2 = null;
    private String customerCity = null;
    private String customerState = "";
    private String customerZip = null;
    private String customerPhone = null;
    private String customerEmail = null;
    private String customerAccountNumber = null;
    
    private String responseNotes = null;
    
    // department data
    private String deptID = null;
    private String deptName = null;
    private String deptContactID = null;
    private String deptContact = null;
    private String deptEmailAddress = null;
    
    /**
     * 
     */
    public IncidentBO() {
        super();
        
    }

    /**
     * @return Returns the customerAccountNumber.
     */
    public String getCustomerAccountNumber() {
        return this.customerAccountNumber;
    }
    /**
     * @param customerAccountNumber The customerAccountNumber to set.
     */
    public void setCustomerAccountNumber(String customerAccountNumber) {
        if (customerAccountNumber != null) {
            this.customerAccountNumber = customerAccountNumber.toUpperCase();
        }
        else {
            this.customerAccountNumber = customerAccountNumber;
        }
    }
    /**
     * @return Returns the customerAddress1.
     */
    public String getCustomerAddress1() {
        return this.customerAddress1;
    }
    /**
     * @param customerAddress1 The customerAddress1 to set.
     */
    public void setCustomerAddress1(String customerAddress1) {
        this.customerAddress1 = customerAddress1 == null ? null : customerAddress1.toUpperCase();
    }
    /**
     * @return Returns the customerAddress2.
     */
    public String getCustomerAddress2() {
        return this.customerAddress2;
    }
    /**
     * @param customerAddress2 The customerAddress2 to set.
     */
    public void setCustomerAddress2(String customerAddress2) {
        this.customerAddress2 = customerAddress2 == null ? null : customerAddress2.toUpperCase();
    }
    /**
     * @return Returns the customerCity.
     */
    public String getCustomerCity() {
        return this.customerCity;
    }
    /**
     * @param customerCity The customerCity to set.
     */
    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity == null ? null : customerCity.toUpperCase();
    }
    /**
     * @return Returns the customerEmail.
     */
    public String getCustomerEmail() {
        return this.customerEmail;
    }
    /**
     * @param customerEmail The customerEmail to set.
     */
    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail == null ? null : customerEmail.toUpperCase();
    }
    /**
     * @return Returns the customerFirstName.
     */
    public String getCustomerFirstName() {
        return this.customerFirstName;
    }
    /**
     * @param customerFirstName The customerFirstName to set.
     */
    public void setCustomerFirstName(String customerName) {
        this.customerFirstName = customerName == null ? null : customerName.toUpperCase();
    }
    /**
     * @return Returns the customerPhone.
     */
    public String getCustomerPhone() {
        return this.customerPhone;
    }
    /**
     * @param customerPhone The customerPhone to set.
     */
    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }
    /**
     * @return Returns the customerState.
     */
    public String getCustomerState() {
        return this.customerState;
    }
    /**
     * @param customerState The customerState to set.
     */
    public void setCustomerState(String customerState) {
        this.customerState = customerState == null ? null : customerState.toUpperCase();
    }
    /**
     * @return Returns the customerZip.
     */
    public String getCustomerZip() {
        return this.customerZip;
    }
    /**
     * @param customerZip The customerZip to set.
     */
    public void setCustomerZip(String customerZip) {
        this.customerZip = customerZip;
    }
    /**
     * @return Returns the enteredBy.
     */
    public String getEnteredBy() {
        return this.enteredBy;
    }
    /**
     * @param enteredBy The enteredBy to set.
     */
    public void setEnteredBy(String enteredBy) {
        this.enteredBy = enteredBy == null ? null : enteredBy.toUpperCase();
    }
    /**
     * @return Returns the id.
     */
    public long getId() {
        return this.id;
    }
    
    public String getIdAsString() {
    	String idAsString = "";
    	try {
    		idAsString = String.valueOf(this.id); 
    	}
    	catch (Exception e) {
			; // ignore
		}
        return idAsString;
    }
    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return Returns the incidentAddress.
     */
    public String getIncidentAddress() {
        return this.incidentAddress;
    }
    /**
     * @param incidentAddress The incidentAddress to set.
     */
    public void setIncidentAddress(String incidentAddress) {
        this.incidentAddress = incidentAddress == null ? null : incidentAddress.toUpperCase();
    }
    /**
     * @return Returns the incidentCity.
     */
    public String getIncidentCity() {
        return this.incidentCity;
    }
    /**
     * @param incidentCity The incidentCity to set.
     */
    public void setIncidentCity(String incidentCity) {
        this.incidentCity = incidentCity == null ? null : incidentCity.toUpperCase();
    }
    /**
     * @return Returns the incidentLocationName.
     */
    public String getIncidentLocationName() {
        return this.incidentLocationName;
    }
    /**
     * @param incidentLocationName The incidentLocationName to set.
     */
    public void setIncidentLocationName(String incidentLocationName) {
        this.incidentLocationName = incidentLocationName == null ? null : incidentLocationName.toUpperCase();
    }
    /**
     * @return Returns the incidentState.
     */
    public String getIncidentState() {
        return this.incidentState;
    }
    /**
     * @param incidentState The incidentState to set.
     */
    public void setIncidentState(String incidentState) {
        this.incidentState = incidentState == null ? null : incidentState.toUpperCase();
    }
    /**
     * @return Returns the incidentType.
     */
    public String getIncidentType() {
        return this.incidentType;
    }
    /**
     * @param incidentType The incidentType to set.
     */
    public void setIncidentType(String incidentType) {
        this.incidentType = incidentType;
    }
    /**
     * @return Returns the incidentZip.
     */
    public String getIncidentZip() {
        return this.incidentZip;
    }
    /**
     * @param incidentZip The incidentZip to set.
     */
    public void setIncidentZip(String incidentZip) {
        this.incidentZip = incidentZip;
    }
    /**
     * @return Returns the inserted.
     */
    public DateTime getInserted() {
        return this.inserted;
    }
    /**
     * @param inserted The inserted to set.
     */
    public void setInserted(DateTime inserted) {
        this.inserted = inserted;
    }
    /**
     * @return Returns the marketAssigned.
     */
    public MarketBO getMarketAssigned() {
        return this.marketAssigned;
    }
    /**
     * @param marketAssigned The marketAssigned to set.
     */
    public void setMarketAssigned(MarketBO marketAssigned) {
        this.marketAssigned = marketAssigned;
    }
    /**
     * @return Returns the marketKey.
     */
    public long getMarketKey() {
        return this.marketKey;
    }
    /**
     * @param marketKey The marketKey to set.
     */
    public void setMarketKey(long marketKey) {
        this.marketKey = marketKey;
    }
    /**
     * @return Returns the ncsSiteKey.
     */
    public Long getNcsSiteKey() {
        return this.ncsSiteKey;
    }
    /**
     * @param ncsSiteKey The ncsSiteKey to set.
     */
    public void setNcsSiteKey(Long ncsSiteKey) {
        this.ncsSiteKey = ncsSiteKey;
    }
    /**
     * @return Returns the notes.
     */
    public String getNotes() {
        return this.notes;
    }
    /**
     * @param notes The notes to set.
     */
    public void setNotes(String notes) {
        if (notes != null) {
            this.notes = StringUtils.replace(notes, "\n", " ");
            this.notes = StringUtils.replace(this.notes, "\r", "");
            this.notes = this.notes.toUpperCase();
        }
        else {
            this.notes = null;
        }
    }
    /**
     * @return Returns the responseNotes.
     */
    public String getResponseNotes() {
        return this.responseNotes;
    }
    /**
     * @param responseNotes The responseNotes to set.
     */
    public void setResponseNotes(String responseNotes) {
        if (responseNotes != null) {
            this.responseNotes = StringUtils.replace(responseNotes, "\n", " ");
            this.responseNotes = StringUtils.replace(this.responseNotes, "\r", "");
            this.responseNotes = this.responseNotes.toUpperCase();
        }
        else {
            this.responseNotes = null;
        }
    }
    /**
     * @return Returns the status.
     */
    public String getStatus() {
        return this.status;
    }
    /**
     * @param status The status to set.
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.toUpperCase();
    }
    /**
     * @return Returns the updated.
     */
    public DateTime getUpdated() {
        return this.updated;
    }
    /**
     * @param updated The updated to set.
     */
    public void setUpdated(DateTime updated) {
        this.updated = updated;
    }
    /**
     * @return Returns the updatedBy.
     */
    public String getUpdatedBy() {
        return this.updatedBy;
    }
    /**
     * @param updatedBy The updatedBy to set.
     */
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    public void save() throws Exception {
        IncidentDAO dao = new IncidentDAO();
        if (this.id > 0) {
            // perform an update
            dao.updateIncident(this);
        }
        else {
            //insert record
            @SuppressWarnings("unused")
			IncidentBO i = dao.insertIncident(this);
        }
    }
    /**
     * @return Returns the incidentDate.
     */
    public Date getIncidentDate() {
        return this.incidentDate;
    }
    /**
     * @param incidentDate The incidentDate to set.
     */
    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }
    /**
     * @return Returns the customerLastName.
     */
    public String getCustomerLastName() {
        return this.customerLastName;
    }
    /**
     * @param customerLastName The customerLastName to set.
     */
    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }
    /**
     * @return Returns the deptContact.
     */
    public String getDeptContact() {
        return this.deptContact;
    }
    /**
     * @param deptContact The deptContact to set.
     */
    public void setDeptContact(String deptContact) {
        this.deptContact = deptContact == null ? null : deptContact.toUpperCase();
    }
    /**
     * @return Returns the deptContactID.
     */
    public String getDeptContactID() {
        return this.deptContactID;
    }
    /**
     * @param deptContactID The deptContactID to set.
     */
    public void setDeptContactID(String deptContactID) {
        this.deptContactID = deptContactID;
    }
    /**
     * @return Returns the deptEmailAddress.
     */
    public String getDeptEmailAddress() {
        return this.deptEmailAddress;
    }
    /**
     * @param deptEmailAddress The deptEmailAddress to set.
     */
    public void setDeptEmailAddress(String deptEmailAddress) {
        this.deptEmailAddress = deptEmailAddress == null ? null : deptEmailAddress.toUpperCase();
    }
    /**
     * @return Returns the deptID.
     */
    public String getDeptID() {
        return this.deptID;
    }
    /**
     * @param deptID The deptID to set.
     */
    public void setDeptID(String deptID) {
        this.deptID = deptID;
    }
    /**
     * @return Returns the deptName.
     */
    public String getDeptName() {
        return this.deptName;
    }
    /**
     * @param deptName The deptName to set.
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.toUpperCase();
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer str = new StringBuffer();
        try {
	        str.append("Incident ID: ").append(this.getId());
	        str.append("; Incident Type: ").append(this.getIncidentType());
	        str.append("; Incident Request Type: ").append(this.getIncidentRequestType());
	        str.append("; Incident LocName: ").append(this.getIncidentLocationName());
	        str.append("; Incident Zip: ").append(this.getIncidentZip());
	        str.append("; Incident Status: ").append(this.getIncidentState());
	        str.append("; Incident Address: ").append(this.getIncidentAddress());
	
	        
	        MarketBO m = this.getMarketAssigned();
	        if (m == null) {
	            try {
	                m = (new MarketLocatorService()).locateMarketByZip(this.getIncidentZip());
	            }
	            catch (Exception e) {
	                
	            }
	        }
	        
	        if (m == null) {
	            str.append("; Market: null");
	        }
	        else {
	            str.append("; Market ID: ").append(m.getMarketID());
	        }
	        
	        str.append("; Entered By: ").append(this.getEnteredBy());
	        str.append("; NCS Site Unique ID: ").append(this.getNcsSiteKey());
	        str.append("; Customer Name: ").append(this.getCustomerFirstName()).append(" ").append(this.getCustomerLastName());
	        str.append("; Customer email: ").append(this.getCustomerEmail());
	        str.append("; Customer Phone: ").append(this.getCustomerPhone());
	        str.append("; Customer Zip: ").append(this.getCustomerZip());
	        str.append("; Customer Address1: ").append(this.getCustomerAddress1());
	        
	        str.append("; Incident Date: ").append(this.getIncidentDate());
	        str.append("; Incidnet Notes: ").append(this.getNotes());
	        str.append("; Incident Response Notes: ").append(this.getResponseNotes());
        }
        catch (Exception e) {
        	str.append("; Exception in IncidentBO::toString() " + e.getMessage());
		}
        
        return str.toString();
    }
    /**
     * @return Returns the incidentRequestType.
     */
    public String getIncidentRequestType() {
        return this.incidentRequestType;
    }
    /**
     * @param incidentRequestType The incidentRequestType to set.
     */
    public void setIncidentRequestType(String incidentRequestType) {
        this.incidentRequestType = incidentRequestType;
    }
    /**
     * @return Returns the refundRequest.
     */
    public boolean isRefundRequest() {
        return this.refundRequest;
    }
    /**
     * @param refundRequest The refundRequest to set.
     */
    public void setRefundRequest(boolean refundRequest) {
        this.refundRequest = refundRequest;
    }
}
