/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.usatoday.ncs.integration.MarketDAO;
import com.usatoday.ncs.transferObjects.MarketContactTO;
import com.usatoday.ncs.transferObjects.MarketTO;

/**
 * @author aeast
 * @date Nov 5, 2007
 * @class MarketBO
 * 
 * This class represents a market
 * 
 */
public class MarketBO {

    private long key = 0;
    private String marketID = "";
    private String description = "";
    private String phone = "";
    
    private Collection<MarketContactTO> contacts = null;
    
    /**
     * 
     */
    private MarketBO() {
        super();
    }

    public static MarketBO fetchMarket(long key) throws Exception {
        MarketDAO dao = new MarketDAO();
        
        MarketTO to = dao.fetchMarket(key);
        
        MarketBO bo = new MarketBO();
        
        bo.setKey(to.getId());
        bo.setDescription(to.getDescription());
        bo.setMarketID(to.getMarketID());
        bo.setPhone(to.getPhone());
        
        return bo;
    }

    public static MarketBO fetchMarket(String marketID) throws Exception {
        MarketDAO dao = new MarketDAO();
        
        System.out.println("Looking up market: " + marketID);
        MarketTO to = dao.fetchMarketByMarketID(marketID);
        MarketBO bo = null;
        if (to != null) {
            bo = new MarketBO();
        
        
            bo.setKey(to.getId());
            bo.setDescription(to.getDescription());
            bo.setMarketID(to.getMarketID());
            bo.setPhone(to.getPhone());
        }
        
        return bo;
    }
    
    public static Collection<MarketBO> fetchMarkets() throws Exception {
    
        MarketDAO dao = new MarketDAO();
        
        Collection<MarketTO> marketTOs = dao.fetchMarkets();
        
        ArrayList<MarketBO> markets = new ArrayList<MarketBO>();
        
        Iterator<MarketTO> itr = marketTOs.iterator();
        while (itr.hasNext()) {
            MarketTO to = itr.next();
            MarketBO bo = new MarketBO();
            
            bo.setKey(to.getId());
            bo.setDescription(to.getDescription());
            bo.setMarketID(to.getMarketID());
            bo.setPhone(to.getPhone());
            
            markets.add(bo);
            
        }
        
        return markets;
    }
    
    
    /**
     * @return Returns the contacts.
     */
    public Collection<MarketContactTO> getContacts() {
        
        if (this.contacts == null) {
            
            MarketDAO dao = new MarketDAO();
            
            try {
                Collection<MarketContactTO> tos = dao.fetchMarketContacts(this.getKey());
                this.contacts = tos;
            }
            catch (Exception e) {
                System.out.println("Exception getting market contacts: " + e.getMessage());
                e.printStackTrace();
            }
            
        }
        return this.contacts;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @return Returns the key.
     */
    public long getKey() {
        return this.key;
    }
    /**
     * @return Returns the marketID.
     */
    public String getMarketID() {
        return this.marketID;
    }
    /**
     * @return Returns the phone.
     */
    public String getPhone() {
        return this.phone;
    }
 
    /**
     * @param description The description to set.
     */
    private void setDescription(String description) {
        this.description = description;
    }
    /**
     * @param key The key to set.
     */
    private void setKey(long key) {
        this.key = key;
    }
    /**
     * @param marketID The marketID to set.
     */
    private void setMarketID(String marketID) {
        this.marketID = marketID;
    }
    /**
     * @param phone The phone to set.
     */
    private void setPhone(String phone) {
        this.phone = phone;
    }
}
