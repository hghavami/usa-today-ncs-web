/*
 * Created on Nov 6, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.bo;

import com.usatoday.ncs.integration.CallCenterSiteDAO;
import com.usatoday.ncs.transferObjects.CallCenterSiteTO;
import com.usatoday.ncs.util.IPToLongConverter;

/**
 * @author aeast
 * @date Nov 6, 2007
 * @class UserBO
 * 
 * This class represents a user of the NCS system
 * 
 */
public class UserBO {

    // folllowing attributes are from the site table
    private long siteKey = 0;
    private String siteID = null;
    private String password = null;
    private String description = null;
    private boolean active = true;
    private long lowerIPRange = 0;
    private long upperIPRange = Long.MAX_VALUE;
    private boolean ipRestricted = false;
    private boolean authenticated = false;
    
    private boolean allowViewAllIncidents = false;
    private boolean allowEditAllIncidentFields = false;
    private SiteBO site = null;
    
    // following attributes are not persisted
    private String userName = null;
    
    /**
     * 
     */
    public UserBO() {
        super();
       
    }

    public static UserBO authenticateSite(String id, String password) throws Exception {
        UserBO user = null;
        
        CallCenterSiteDAO dao = new CallCenterSiteDAO();
        
        CallCenterSiteTO to = dao.authenticateSite(id, password);
        
        if (to != null) {
            user = new UserBO();
            user.siteID = to.getSiteID();
            user.active = to.isActive();
            user.authenticated = true;
            user.description = to.getDescription();
            user.password = to.getPassword();
            user.siteKey = to.getKey();
            
            String lowerIP = to.getIpRestrictionLower();
            String upperIP = to.getIpRestrictionUpper();
            if (lowerIP != null && lowerIP.trim().length() > 0) {
                user.ipRestricted = true;

                user.lowerIPRange = IPToLongConverter.convertIPStringToLong(lowerIP.trim());
                
                if (upperIP != null && upperIP.trim().length() > 0) {
                    user.upperIPRange = IPToLongConverter.convertIPStringToLong(upperIP);
                    
                }
                else {
                    // only a single ip is valid
                    user.upperIPRange = user.lowerIPRange;
                }
                
            }
            else {
                user.ipRestricted = false;
            }
            
            user.site = new SiteBO(to);
            
            
        }
        else {
            user = new UserBO();
            user.authenticated = false;
            user.siteID = id;
        }
        
        return user;
    }
    
    /**
     * @return Returns the active.
     */
    public boolean isActive() {
        return this.active;
    }
    /**
     * @return Returns the authenticated.
     */
    public boolean isAuthenticated() {
        return this.authenticated;
    }
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @return Returns the password.
     */
    public String getPassword() {
        return this.password;
    }
    /**
     * @return Returns the siteID.
     */
    public String getSiteID() {
        return this.siteID;
    }
    /**
     * @return Returns the siteKey.
     */
    public long getSiteKey() {
        return this.siteKey;
    }
    /**
     * @return Returns the ipRestricted.
     */
    public boolean isIpRestricted() {
        return this.ipRestricted;
    }
    /**
     * @return Returns the lowerIPRange.
     */
    public long getLowerIPRange() {
        return this.lowerIPRange;
    }
    /**
     * @return Returns the upperIPRange.
     */
    public long getUpperIPRange() {
        return this.upperIPRange;
    }
    /**
     * @return Returns the userName.
     */
    public String getUserName() {
        return this.userName;
    }
    /**
     * @param userName The userName to set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return Returns the allowEditAllIncidentFields.
     */
    public boolean isAllowEditAllIncidentFields() {
        return this.allowEditAllIncidentFields;
    }
    /**
     * @return Returns the allowViewAllIncidents.
     */
    public boolean isAllowViewAllIncidents() {
        return this.allowViewAllIncidents;
    }

	public SiteBO getSite() {
		return site;
	}

	public void setSite(SiteBO site) {
		this.site = site;
	}
}
