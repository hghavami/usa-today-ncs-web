/*
 * Created on Nov 5, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.bo;



/**
 * @author aeast
 * @date Nov 5, 2007
 * @class IncidentBO
 * 

 * 
 */
public class KeycodeBO  {

    /**
	 * 
	 */
	
	private String offerKey = "";
	private String pubCode = "UT";
	private String sourceCode = null;
	private String promoCode = null;
	private String contestCode = null;
	private String offerDesc = null;
	
	String offerURL = null;
	
  
    
    /**
     * 
     */
    public KeycodeBO() {
        super();
        
    }

    /**
     * @return Returns the offerKey
     */
    public String getOfferKey() {
        return this.offerKey;
    }
    
    /**
     * @param id The id to set.
     */
    public void setOfferKey(String offerKey) {
        this.offerKey = offerKey;
  //      this.offerKey = "offer";
    }
    /**
     * @return Returns the offerKey
     */
    public String getPubCode() {
        return this.pubCode;
    }
    
    /**
     * @param id The id to set.
     */
    public void setPubCode(String pubCode) {
        this.pubCode = pubCode;
    //    this.pubCode = "QQ";
    }
    
    /**
     * @return Returns the offerKey
     */
    public String getSourceCode() {
        return this.sourceCode;
    }
    
    /**
     * @param id The id to set.
     */
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
    
    /**
     * @return Returns the offerKey
     */
    public String getContestCode() {
        return this.contestCode;
    }
    
    /**
     * @param id The id to set.
     */
    public void setContestCode(String contestCode) {
        this.contestCode = contestCode;
    }
    
    /**
     * @return Returns the offerKey
     */
    public String getPromoCode() {
        return this.promoCode;
    }
    
    /**
     * @param id The id to set.
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
    
    
    /**
     * @return Returns the offerKey
     */
    public String getOfferDesc() {
        return this.offerDesc;
    }
    
    /**
     * @param id The id to set.
     */
    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }
    
    /**
     * @return Returns the offerKey
     */
    public String getKeyCode() {
        return this.sourceCode + this.promoCode + this.contestCode;
    }

	public String getOfferURL() {
		return offerURL;
	}

	public void setOfferURL(String offerURL) {
		this.offerURL = offerURL;
	}
    
    
}
     
