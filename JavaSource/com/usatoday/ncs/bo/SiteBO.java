/*
 * Created on Dec 18, 2007
 * @copyright Copyright 2007 USA TODAY. All rights reserved.
 */
package com.usatoday.ncs.bo;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.usatoday.ncs.integration.CallCenterSiteDAO;
import com.usatoday.ncs.transferObjects.CallCenterSiteTO;
import com.usatoday.ncs.transferObjects.SiteRoleTO;

/**
 * @author aeast
 * @date Dec 18, 2007
 * @class SiteBO
 * 
 * 
 */
public class SiteBO {
	
	// Accout Maintenance Roles
	public static final String ACCT_MAINT_ROLE = "ACCT_MAINT";
	public static final String ACCT_MAINT_MNGR_ROLE = "ACCT_MAINT_MNGR";
	
	// Market Incidents Role
	public static final String INCIDENT_ROLE = "INCIDENT";
	
	// New Orders Role
	public static final String NEW_ORDERS_ROLE = "NEW_ORDER";
	
    private long key = 0;
    private String siteID = "";
    private String sitePassword = "";
    private String description = "";
    
    private boolean active = true;
    private boolean usatLocation = false;
    
    private String lowerIPRestriction = "0.0.0.0";
    private String upperIPRestriction = "255.255.255.255";
    
    private HashMap<String, SiteRoleTO> roles = null;
    
    
    /**
     * 
     */
    public SiteBO() {
        super();
        roles = new HashMap<String, SiteRoleTO>();
    }

    public SiteBO(CallCenterSiteTO source) {
        this.key = source.getKey();
        this.siteID = source.getSiteID();
        this.sitePassword = source.getPassword();
        this.description = source.getDescription();
        this.active = source.isActive();
        this.lowerIPRestriction = source.getIpRestrictionLower();
        this.upperIPRestriction = source.getIpRestrictionUpper();
        this.usatLocation = source.isUsatLocation();
        
        roles = new HashMap<String, SiteRoleTO>();
        
        for (SiteRoleTO role : source.getRoles()) {
        	roles.put(role.getRoleID(), role);
        }
    }
    
    /**
     * 
     * @return A hashmap of sites, keyed off the java.util.Long siteKey value.
     * @throws Exception
     */
    public static HashMap<Long, SiteBO> getAllSites() throws Exception {
        
        CallCenterSiteDAO dao = new CallCenterSiteDAO();
        
        Collection<CallCenterSiteTO> sitesTOs = dao.fetchAllSites();
        HashMap<Long, SiteBO> sites = new HashMap<Long, SiteBO>();
        
        Iterator<CallCenterSiteTO> itr = sitesTOs.iterator();
        while (itr.hasNext()) {
            CallCenterSiteTO to = itr.next();
            SiteBO s = new SiteBO(to);
            
            Long l = new Long(s.getKey());
            
            sites.put(l, s);
        }
        
        return sites;
    }
    
    /**
     * 
     * @param siteKey
     * @return
     * @throws Exception
     */
    public static SiteBO fetchSite(long siteKey) throws Exception {
        CallCenterSiteDAO dao = new CallCenterSiteDAO();
        
        CallCenterSiteTO to = dao.fetchSite(siteKey);
        SiteBO bo = null;
        if (to != null) {
            bo = new SiteBO(to);
        }
        return bo;
    }
    /**
     * @return Returns the active.
     */
    public boolean isActive() {
        return this.active;
    }
   
    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return this.description;
    }
    /**
     * @return Returns the key.
     */
    public long getKey() {
        return this.key;
    }
    /**
     * @return Returns the lowerIPRestriction.
     */
    public String getLowerIPRestriction() {
        return this.lowerIPRestriction;
    }
    /**
     * @return Returns the siteID.
     */
    public String getSiteID() {
        return this.siteID;
    }
    /**
     * @return Returns the sitePassword.
     */
    public String getSitePassword() {
        return this.sitePassword;
    }
    /**
     * @return Returns the upperIPRestriction.
     */
    public String getUpperIPRestriction() {
        return this.upperIPRestriction;
    }
    /**
     * @return Returns the usatLocation.
     */
    public boolean isUsatLocation() {
        return this.usatLocation;
    }
    /**
     * @param active The active to set.
     */
    public void setActive(boolean active) {
        this.active = active;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
   
    /**
     * @param lowerIPRestriction The lowerIPRestriction to set.
     */
    public void setLowerIPRestriction(String lowerIPRestriction) {
        this.lowerIPRestriction = lowerIPRestriction;
    }
    /**
     * @param siteID The siteID to set.
     */
    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }
    /**
     * @param sitePassword The sitePassword to set.
     */
    public void setSitePassword(String sitePassword) {
        this.sitePassword = sitePassword;
    }
    /**
     * @param upperIPRestriction The upperIPRestriction to set.
     */
    public void setUpperIPRestriction(String upperIPRestriction) {
        this.upperIPRestriction = upperIPRestriction;
    }
    /**
     * @param usatLocation The usatLocation to set.
     */
    public void setUsatLocation(boolean usatLocation) {
        this.usatLocation = usatLocation;
    }

	public boolean isSiteInRole(String role) {
		boolean hasRole = false;
		if (this.roles.containsKey(role)) {
			hasRole = true;
		}
		return hasRole;
	}
	
	public boolean isInAccountMainenanceRole() {
		boolean hasRole = false;
		if (this.roles.containsKey(SiteBO.ACCT_MAINT_ROLE)) {
			hasRole = true;
		}
		return hasRole;
	}

	public boolean isInIncidentRole() {
		boolean hasRole = false;
		if (this.roles.containsKey(SiteBO.INCIDENT_ROLE)) {
			hasRole = true;
		}
		return hasRole;
	}
	
	public boolean isInNewOrderRole() {
		boolean hasRole = false;
		if (this.roles.containsKey(SiteBO.NEW_ORDERS_ROLE)) {
			hasRole = true;
		}
		return hasRole;
	}
	
}
