package com.usatoday.businessObjects.util.mail;

import java.rmi.RemoteException;

import javax.servlet.http.HttpSession;

import com.exacttarget.APIObject;
import com.exacttarget.Attribute;
import com.exacttarget.CreateOptions;
import com.exacttarget.CreateRequest;
import com.exacttarget.CreateResponse;
import com.exacttarget.CreateResult;
import com.exacttarget.Email;
import com.exacttarget.SoapProxy;
import com.exacttarget.Subscriber;
import com.exacttarget.SystemStatusOptions;
import com.exacttarget.SystemStatusRequestMsg;
import com.exacttarget.SystemStatusResponseMsg;
import com.exacttarget.SystemStatusResult;
import com.exacttarget.TriggeredSend;
import com.exacttarget.TriggeredSendDefinition;
import com.exacttarget.TriggeredSendStatusEnum;
import com.exacttarget.TriggeredSendTypeEnum;
import com.usatoday.util.constants.UsaTodayConstants;

public class ExactTargetPrintTargetedAccount {
	/**
	 * 
	 */	
	private Integer emailID = null;
	private String emailAddress = null;
	private  String firstName = null;
	private  String lastName = null;
	private String accountNumber = null;
	private String ccType = null;
	private String accntAmount = null;
	private String homePhone = null;
	private String tenure = "tee";
	private String company = "tet";
	private String homephone = "7034444444";
	private String amount = "rere";
	private String cctype = "rere";
	private String streetName = null;
	private String cityName = null;
	private String currentDate = null;
	private String zipCode = null;
	private String delvMethod = null;
	private String expireDate = null;
	private String startType = null;
	private String subTerm = null;
	private String dueDate = null;
	private String ccLast4 = null;
	private String startDate = null;
	private String state = null;
	private String urlLink = null;
	private String gpName = null;
	private String companyName = null;
	private String address1 = null;
	private String address2 = null;
	private String pubCode = null;
	private String miscText1 = null;
	private  String returnCode = null;
	private  String customerKey = null;
	private  String ezpay = null;
	private  String tax = null;
	private  String emailDesc = null;
	private String password = null;
	private String futureStartDate = null;
	private String orderTotal = null;
	private String subType = null;
	private String isGuiZip = null;
	private String webPartner = null;
	private String dateFiller = "99/99/9999";
	private String webTrialStart = null;
	private String webTrialEnd = null;
	private String webPartnerID = null;
	Attribute [] attArray = new Attribute[24];		
	Subscriber[] subArray = new Subscriber[1];
    
	 
	
	public ExactTargetPrintTargetedAccount() {
		super();
		
	}
	
	
	/**
	 * @return
	 */
	public String getWebPartnerID() {
		return this.webPartnerID;
	}
	
	/**
	 * @param string
	 */
	public void setWebPartnerID(String string) {
		
		this.webPartnerID = string;
		
	}	
	
	
	/**
	 * @return
	 */
	public String getWebTrialEnd() {
		return this.webTrialEnd;
	}
	
	/**
	 * @param string
	 */
	public void setWebTrialEnd(String string) {
		
		//Make Sure this is in Date Format.  Or you will get an invalid Sub message
		if (string.equalsIgnoreCase("") || string.equals(null)){
			this.webTrialEnd = this.dateFiller;			
		} else {
			this.webTrialEnd = string;
		}
		
	}	
	
	/**
	 * @return
	 */
	public String getWebTrialStart() {
		return this.webTrialStart;
	}
	
	/**
	 * @param string
	 */
	public void setWebTrialStart(String string) {
		//Make Sure this is in Date Format.  Or you will get an invalid Sub message
		if (string.equalsIgnoreCase("") || string.equals(null)){
			this.webTrialStart = this.dateFiller;			
		} else {
			this.webTrialStart = string;
		}
	
	}	
	
	/**
	 * @return
	 */
	public String getWebPartner() {
		return this.webPartner;
	}
	
	/**
	 * @param string
	 */
	public void setWebPartner(String string) {
		this.webPartner = string;
	}	
	
	/**
	 * @return
	 */
	public Integer getEmailID() {
		return this.emailID;
	}
	
	/**
	 * @param string
	 */
	public void setEmailID(Integer integer) {
		this.emailID = integer;
	}	
	
	/**
	 * @return
	 */
	public String getGuiZip() {
		return this.isGuiZip;
	}
	
	/**
	 * @param string
	 */
	public void setGuiZip(String string) {
		this.isGuiZip = string;
	}	
	

	
	/**
	 * @return
	 */
	public String getFutureStartDate() {
		return this.futureStartDate;
	}
	
	/**
	 * @param string
	 */
	public void setFutureStartDate(String string) {
		this.futureStartDate = string;
	}	
	
	/**
	 * @return
	 */
	public String getState() {
		return this.state;
	}
	
	/**
	 * @param string
	 */
	public void setState(String string) {
		this.state = string;
	}	
	
	/**
	 * @return
	 */
	public String getOrderTotal() {
		return this.orderTotal;
	}
	
	/**
	 * @param string
	 */
	public void setOrderTotal(String string) {
		this.orderTotal = string;
	}	
	/**
	 * @return
	 */
	public String getSubType() {
		return this.subType;
	}
	
	/**
	 * @param string
	 */
	public void setSubType(String string) {
		this.subType = string;
	}	
	
	
	/**
	 * @return
	 */
	public String getTerm() {
		return this.subTerm;
	}
	
	/**
	 * @param string
	 */
	public void setTerm(String string) {
		this.subTerm = string;
	}	
	
	/**
	 * @return
	 */
	public String getCurrentDate() {
		return this.currentDate;
	}
	
	/**
	 * @param string
	 */
	public void setCurrentDate(String string) {
		this.currentDate = string;
	}	
	
	/**
	 * @param string
	 */
	public void setTax(String string) {
		this.tax = string;
	}
	
	
	/**
	 * @return
	 */
	public String getTax() {
		return this.tax;
	}
	
	/**
	 * @param string
	 */
	public void setEZPay(String string) {
		this.ezpay = string;
	}	
	
	/**
	 * @return
	 */
	public String getEZPay() {
		return this.ezpay;
	}
	
	
	
	/**
	 * @return
	 */
	public String getDueDate() {
		return this.dueDate;
	}
	
	/**
	 * @param string
	 */
	public void setDueDate(String string) {
		this.dueDate = string;
	}	
	
	
	
	
	/**
	 * @return
	 */
	public String getMiscText1() {
		return this.miscText1;
	}
	
	/**
	 * @param string
	 */
	public void setMiscText1(String string) {
		this.miscText1 = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getPub() {
		return this.pubCode;
	}
	
	/**
	 * @param string
	 */
	public void setPub(String string) {
		this.pubCode = string;
	}	
	
	
	
	
	
	/**
	 * @return
	 */
	public String getAddress2() {
		return this.address2;
	}
	
	/**
	 * @param string
	 */
	public void setAddress2(String string) {
		this.address2 = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getAddress1() {
		return this.address1;
	}
	
	/**
	 * @param string
	 */
	public void setAddress1(String string) {
		this.address1 = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getCompany() {
		return this.companyName;
	}
	
	/**
	 * @param string
	 */
	public void setCompany(String string) {
		this.companyName = string;
	}	
	
	
	
	
	/**
	 * @return
	 */
	public String getGPFullName() {
		return this.gpName;
	}
	
	/**
	 * @param string
	 */
	public void setGPFullName(String string) {
		this.gpName = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getLink() {
		return this.urlLink;
	}
	
	/**
	 * @param string
	 */
	public void setLink(String string) {
		this.urlLink = string;
	}	
	
	/**
	 * @return
	 */
	public String getPassword() {
		return this.password;
	}
	
	/**
	 * @param string
	 */
	public void setPassword(String string) {
		this.password = string;
	}	
	
	
	
	
	/**
	 * @return
	 */
	public String getStartType() {
		return this.startType;
	}
	
	/**
	 * @param string
	 */
	public void setStartType(String string) {
		this.startType = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getExpireDate() {
		return this.expireDate;
	}
	
	/**
	 * @param string
	 */
	public void setExpireDate(String string) {
		this.expireDate = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getDelm() {
		return this.delvMethod;
	}
	
	/**
	 * @param string
	 */
	public void setDelm(String string) {
		this.delvMethod = string;
	}	
	
	
	
	/**
	 * @return
	 */
	public String getZip() {
		return this.zipCode;
	}
	
	/**
	 * @param string
	 */
	public void setZip(String string) {
		this.zipCode = string;
	}	
	
	/**
	 * @return
	 */
	public String getCity() {
		return this.cityName;
	}
	
	/**
	 * @param string
	 */
	public void setCity(String string) {
		this.cityName = string;
	}	
	
	/**
	 * @return
	 */
	public String getCCLast4() {
		return this.ccLast4;
	}
	
	/**
	 * @param string
	 */
	public void setCCLast4(String string) {
		this.ccLast4 = string;
	}	
	
	
	/**
	 * @return
	 */
	public String getStreet() {
		return this.streetName;
	}
	
	/**
	 * @param string
	 */
	public void setStreet(String string) {
		this.streetName = string;
	}	
	
	
	/**
	 * @return
	 */
	public String getAmnt() {
		return this.accntAmount;
	}
	
	/**
	 * @param string
	 */
	public void setHomePhone(String string) {
		this.homePhone = string;
	}
	
	/**
	 * @return
	 */
	public String getHomePhone() {
		return this.homePhone;
	}
	
	/**
	 * @param string
	 */
	public void setAmnt(String string) {
		this.accntAmount = string;
	}
	
	
	/**
	 * @return
	 */
	public String getCCType() {
		return this.ccType;
	}
	
	/**
	 * @param string
	 */
	public void setCCType(String string) {
		this.ccType = string;
	}
	
	
	
	
	
	/**
	 * @return
	 */
	public String getAccntNumber() {
		return this.accountNumber;
	}
	
	/**
	 * @param string
	 */
	public void setAccntNumber(String string) {
		this.accountNumber = string;
	}
	
	
	
	
	
	
	
	
	

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * @return
	 */
	public String getCustomerKey() {
		return this.customerKey;
	}
	
	/**
	 * @return
	 */
	public String getEmailDesc() {
		return this.emailDesc;
	}
	
	/**
	 * @param string
	 */
	public void setEmailDesc(String string) {
		this.emailDesc = string;
	}
	
	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		this.emailAddress = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		this.firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		this.lastName = string;
	}
	
	/**
	 * @param string
	 */
	public void setCustomerKey(String string) {
		this.customerKey = string;
	}


    public static String checkExactTargetWebService(HttpSession session1) throws Exception {
        
       	if (UsaTodayConstants.debug) {
       		System.out.println("Begin checkExactTargetWebService...");
            	}
      
    	  	
     	String returnCode = null;
    	ExactTargetPrintTargetedAccount etCheck = new ExactTargetPrintTargetedAccount();
    	  	
    	if (etCheck.checkSystemStatus().equalsIgnoreCase("OK")){
    		returnCode = "OK";
    	} else {
    		returnCode = "Error";
    	}    	
    	
    	//a returnCode of OK means the web service api is available
      	if (UsaTodayConstants.debug) {
      		System.out.println("returnCode =   " + returnCode );
        	
        	System.out.println("End checkExactTargetWebService...");
            	}
     
    	
        return returnCode;
    }
    
    
    
    
    
	public String checkSystemStatus() {
		
		returnCode = null;
		//configure proxy
		SoapProxy webservice = new SoapProxy();
		SystemStatusRequestMsg statusMsg = new SystemStatusRequestMsg();
		SystemStatusResponseMsg responseMsg = new SystemStatusResponseMsg();
		SystemStatusOptions options = new SystemStatusOptions();
	
		statusMsg.setOptions(options);
		
		//call the webservice
		try {
			responseMsg = webservice.getSystemStatus(statusMsg);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
		 	if (UsaTodayConstants.debug) {
		 		System.out.println( "[webservice status message] " + "Error checking ET Webservice" );			
			  	}
			e.printStackTrace();
			
		}
		
		//process the webservice call results
		SystemStatusResult[] results = responseMsg.getResults();
								
		String systemSystemStatus = results[0].getStatusCode().toString();						
		
		if (systemSystemStatus.equalsIgnoreCase("OK")){
			returnCode = "OK";
			
		} else {
			if (UsaTodayConstants.debug) {
				System.out.println( "[webservice status message] " + returnCode );			
				System.out.println( "[webservice status message] Exact Target Web Service Down" );			
				  	}
			returnCode = "Error";
		}							
		return returnCode;		
	}
	
	 
  
   
    
    public static String sendExactTargetPrintTargeted( String interaction, String toEmail, String link, String firstName, String lastName, String amount, String ccType, String address1, String address2, String street, String eEditionID, String eEditionDESC, String pubCode, String city, String company, String term, String zip, String password, String delm, String tax, String ezpay, String currentdate, String ccLastFour, String futureStartDate, String orderTotal, String subType, String phone, String state, String gpname, String guizip, String webpartner, String webtrialstart, String webtrialend, String accntnumber, String partnerid) throws Exception {
    	   
    	String returnCode = null;
    	
    	if (UsaTodayConstants.debug) {
    		 System.out.println("Generating Exact Target Email...Starting");
    	   		  	}
       
    	ExactTargetPrintTargetedAccount etSend2 = new ExactTargetPrintTargetedAccount();
        	//email is the only field required by the system
    	etSend2.setEmailAddress("swong@usatoday.com");
    	
    	//attributes linked in the outgoing email are required by the email
    	
    	//CustomerKey refers to an 'Interaction' defined on the Exact Target management site
    	//The "eEditionKey" interaction indicates which email is sent, and which list is written to
    	etSend2.setCustomerKey("testEEMailOrdConf");
    	
    	//attributes required for subscriber management process on Exact Target are
    	//will need to be added when available.    	
    	etSend2.setEmailDesc("desc");
    	String firstNL = firstName.toLowerCase();
    	String firstNameU = firstNL.substring(0,1).toUpperCase() + firstNL.substring(1);
    	String lastNL = lastName.toLowerCase();
    	String lastNameU = lastNL.substring(0,1).toUpperCase() + lastNL.substring(1);
    	etSend2.setFirstName(firstNameU);
    	etSend2.setLastName(lastNameU);  
    	etSend2.setPub(pubCode);
    	etSend2.setAmnt(amount);
    	etSend2.setCCType(ccType);
    	etSend2.setAddress1(address1);
    	etSend2.setAddress2(address2);
    	etSend2.setCity(city);
    	etSend2.setCompany(company);
    	etSend2.setStreet(street);
    	etSend2.setTerm(term);
    	etSend2.setZip(zip);
    	etSend2.setPassword(password);
    	etSend2.setDelm(delm);    
    	etSend2.setLink(link);
    	etSend2.setTax(tax);
    	etSend2.setEZPay(ezpay);
    	etSend2.setCurrentDate(currentdate); 
    	etSend2.setCCLast4(ccLastFour);	
    	etSend2.setFutureStartDate(futureStartDate);
    	etSend2.setOrderTotal(orderTotal);
    	etSend2.setSubType(subType);
    	etSend2.setHomePhone(phone);
    	etSend2.setState(state);
    	etSend2.setGPFullName(gpname);
    	etSend2.setGuiZip(guizip);
    	etSend2.setWebPartner(webpartner);
    	etSend2.setWebTrialStart(webtrialstart);
   	etSend2.setWebTrialEnd(webtrialend);
    	etSend2.setExpireDate(webtrialend);
    	etSend2.setAccntNumber(accntnumber);
    	etSend2.setWebPartnerID(partnerid);
    	
    	
    	if (etSend2.sendExactTargetPrintEmail().equalsIgnoreCase("OK")){
    		returnCode = "OK";
    	}
    	
    	if (UsaTodayConstants.debug) {
    		System.out.println("return code..  " + returnCode);
        	
        	System.out.println("Exact Target Email Generated...Completed");
      		  	}
          return returnCode;
    }
    
	 
    
    
	public String sendExactTargetPrintEmail() {
		returnCode = null;
		Subscriber[] subscriberArray = new Subscriber[1];
		
		//configure soap proxy
		SoapProxy webservice = new SoapProxy();
		SystemStatusRequestMsg statusMsg = new SystemStatusRequestMsg();
		SystemStatusOptions options = new SystemStatusOptions();
	
		statusMsg.setOptions(options);
	
		//create Subscriber object
	//	Subscriber subscriber = new Subscriber();
	//	subscriber.setEmailAddress(this.emailAddress);		
	//	subscriber.setSubscriberKey(this.emailAddress);
		
		//create Subscriber object
		Subscriber subscriber = new Subscriber();
		subscriber.setEmailAddress("swong@usatoday.com");		
		subscriber.setSubscriberKey("swong@usatoday.com");
		
		
		//assign values to attributes
		Attribute attribute1 = new Attribute();
		attribute1.setName("Full Name");
		attribute1.setValue("fname");
		
		Attribute attribute2 = new Attribute();
		attribute2.setName("First Name");
		attribute2.setValue("blast");
		
		Attribute attribute3 = new Attribute();
		attribute3.setName("Last Name");
		attribute3.setValue("dog");
		
		Attribute attribute4 = new Attribute();
		attribute4.setName("Account Number");
		attribute4.setValue("999999999");
		
		Attribute attribute5 = new Attribute();
		attribute5.setName("link");
		attribute5.setValue("link");
				
		Attribute attribute6 = new Attribute();
		attribute6.setName("PUB");
		attribute6.setValue("UT");
		
		Attribute attribute7 = new Attribute();
		attribute7.setName("Tenure");
		attribute7.setValue("Tenure");	 
		
		Attribute attribute8 = new Attribute();
		attribute8.setName("Company");
		attribute8.setValue("Company");
		
		Attribute attribute9 = new Attribute();
		attribute9.setName("Address 1");
		attribute9.setValue(this.address1);	
		
		Attribute attribute10 = new Attribute();
		attribute10.setName("ADDRESS 2");
		attribute10.setValue("ADDRESS 2");
			
		Attribute attribute11 = new Attribute();
		attribute11.setName("STREET");
		attribute11.setValue("STREET");
		
		Attribute attribute12 = new Attribute();
		attribute12.setName("CITY");
		attribute12.setValue("CITY");
		
		Attribute attribute13 = new Attribute();
		attribute13.setName("ZIP");
		attribute13.setValue("ZIP");		
		
		Attribute attribute14 = new Attribute();
		attribute14.setName("HOME PHONE");
		attribute14.setValue("HOME PHONE");
				
		Attribute attribute15 = new Attribute();
		attribute15.setName("GP FULL NAME");
		attribute15.setValue("GP FULL NAME");
		
		Attribute attribute16 = new Attribute();
		attribute16.setName("TERM");
		attribute16.setValue("TERM");
				
		Attribute attribute17 = new Attribute();
		attribute17.setName("AMOUNT");
		attribute17.setValue("AMOUNT");
		
		Attribute attribute18 = new Attribute();
		attribute18.setName("CC LAST 4");
		attribute18.setValue(this.ccLast4);
		
		Attribute attribute19 = new Attribute();
		attribute19.setName("CC TYPE");
		attribute19.setValue("CC TYPE");
		
		Attribute attribute20 = new Attribute();
		attribute20.setName("DELIVERY METHOD");
		attribute20.setValue("DELIVERY METHOD");
				
		Attribute attribute21 = new Attribute();
		attribute21.setName("DUE DATE");
		attribute21.setValue(dateFiller);	 
		
		Attribute attribute22 = new Attribute();
		attribute22.setName("START TYPE");
		attribute22.setValue("START TYPE");
		
		Attribute attribute23 = new Attribute();
		attribute23.setName("EXPIRE DATE");
		attribute23.setValue(dateFiller);	 

		
		Attribute attribute24 = new Attribute();
		attribute24.setName("START DATE");
		attribute24.setValue(dateFiller);
		
			
		
		
		//Populate attribute array
        attArray[0] = attribute1;
       	attArray[1] = attribute2;
       	attArray[2] = attribute3;
       	attArray[3] = attribute4;
       	attArray[4] = attribute5;
       	
     	attArray[5] = attribute6;
       	attArray[6] = attribute7;
       	attArray[7] = attribute8;
       	attArray[8] = attribute9;
    	attArray[9] = attribute10;
       	attArray[10] = attribute11;
       	attArray[11] = attribute12;
       	attArray[12] = attribute13;
       	attArray[13] = attribute14;
    	attArray[14] = attribute15;
       	attArray[15] = attribute16;
       	attArray[16] = attribute17;
       	
       	attArray[17] = attribute18;
       	attArray[18] = attribute19;
       	attArray[19] = attribute20;
       	attArray[20] = attribute21;
    	attArray[21] = attribute22;
       	attArray[22] = attribute23;
     	attArray[23] = attribute24;
    //ttArray[24] = attribute25;
		
       	//assign attribute array to subscriber object
       	subscriber.setAttributes(attArray);
		
    	Email etEmail = new Email();
		etEmail.setID(emailID);		
       	
		
		TriggeredSendStatusEnum Active = TriggeredSendStatusEnum.Active;		
		TriggeredSendTypeEnum eType = TriggeredSendTypeEnum.Continuous;	
		
		//define the triggered send status definition
		TriggeredSendDefinition tsd = new TriggeredSendDefinition();
		tsd.setCustomerKey(customerKey);
	
		tsd.setTriggeredSendStatus(Active);	
		tsd.setDescription(emailDesc);	
		tsd.setEmail(etEmail);
		tsd.setTriggeredSendType(eType);		
		
		//define the triggered send
		TriggeredSend ts = new TriggeredSend();
		ts.setTriggeredSendDefinition(tsd);
		
		//Populate array of Subscribers
		 subscriberArray[0] = subscriber;		      
        
		//set subscriber array to triggered send		
        ts.setSubscribers( subscriberArray );		
		
		
        //execute request		
		CreateRequest cRequest = new CreateRequest();	
		
		CreateOptions cOptions = new CreateOptions();
		
		cRequest.setOptions(cOptions);		
		cRequest.setObjects(new APIObject[]{ ts });
		
		CreateResponse cResponse;
		try {
			cResponse = webservice.create(cRequest);
			
			//overall status
			if (cResponse.getOverallStatus().equalsIgnoreCase("OK")){
				returnCode = "OK";				
			} else {
				returnCode = "Error";
			}
			
			 //individual results
			CreateResult[] createResult = cResponse.getResults();
			 
			for ( CreateResult dumpStepThrough : createResult )
	        {
				
				if (UsaTodayConstants.debug) {
			        System.out.println( "[create status message] " + dumpStepThrough.getStatusMessage() );
		              System.out.println( "[create status code] " + dumpStepThrough.getStatusCode() );
		    			}
	          } 
			 
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			if (UsaTodayConstants.debug) {
			      System.out.println( "[Error Exact Target Triggered Send].  Call IT Developer.");
				}
             returnCode = "Error";
			e.printStackTrace();			
		}		
		
		return returnCode;
	}	

}
