/*
 * Created on Mar 24, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.businessObjects.util.mail;

/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EmailAlert {

	private String receiverList = null;
	private String subject = null;
	private String sender = null;
	private String bodyText = null;
	
	/**
	 * 
	 */
	public EmailAlert() {
		super();
		
	}

	/**
	 * @return
	 */
	public String getBodyText() {
		return bodyText;
	}

	/**
	 * @return
	 */
	public String getReceiverList() {
		return receiverList;
	}

	/**
	 * @return
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param string
	 */
	public void setBodyText(String string) {
		bodyText = string;
	}

	/**
	 * @param string
	 */
	public void setReceiverList(String string) {
		receiverList = string;
	}

	/**
	 * @param string
	 */
	public void setSender(String string) {
		sender = string;
	}

	/**
	 * @param string
	 */
	public void setSubject(String string) {
		subject = string;
	}

	public void sendAlert() {
		try {
			SmtpMailSender mailer = new SmtpMailSender();
			
			mailer.setSender(this.getSender());
			mailer.addTORecipient(this.getReceiverList());
			mailer.setMessageSubject(this.getSubject());
			mailer.setMessageText(this.getBodyText());
			
			mailer.sendMessage();
			
		}
		catch (Exception e){
			System.out.println("FAILED TO SEND ALERT: " + this.getBodyText() + ";   EXCEPTION: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
