package com.gannett.usatoday.ncs.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.joda.time.DateTime;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.products.SubscriptionProductIntf;
import com.usatoday.businessObjects.products.SubscriptionProductBO;
import com.usatoday.businessObjects.util.UsatDateTimeFormatter;

/**
 * @version 	1.0
 * @author
 */
public class ValidatePubStartDateAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
	    HttpServletRequest request, HttpServletResponse response)
	    throws Exception {

        ActionForward forward = null; // return value
        StringBuilder sb = new StringBuilder("<pubDateValid>");
    
        String pub = null;
        try {
    
            String earliestStart = "";
            String targetDate = request.getParameter("selectedDate");
    	    pub = request.getParameter("pubCode");
    	    pub = pub == null ? "UT" : pub;
    
    	    SubscriptionProductIntf product = SubscriptionProductBO.getSubscriptionProduct(pub);
    	    
            if (!UsatDateTimeFormatter.verifyInputDate(targetDate)) {
    	        sb.append("<datevalid>false</datevalid>");
    	        sb.append("<nextValidDate>");
    	        DateTime dt = product.getEarliestPossibleStartDate();
    	        sb.append(dt.toString("MM/dd/yyyy"));
    	        
    	        sb.append("</nextValidDate>");
    	        sb.append("<message>Invalid start date format. Must be in MM/DD/YYYY form.</message>");
        	    sb.append("</pubDateValid>");
                response.setContentType("text/xml"); 
                response.setHeader("Cache-Control", "no-cache"); 
                response.getWriter().write(sb.toString());
                response.flushBuffer();
                response.getWriter().close();
                return null;
            }
            
            DateTime requestedStartDate = null;
        	try {
        		// MM/DD/YYYY
        		requestedStartDate = UsatDateTimeFormatter.convertMMDDYYYYToDateTime(targetDate);
        		
        		if (product.isStartDateValid(requestedStartDate)) {
                	sb.append("<datevalid>true</datevalid>");
        		}
        		// need method to get next valid start date from requested.
        		
        	}
        	catch (UsatException ue) {
        		if (requestedStartDate != null) {
	        		earliestStart = product.getFirstValidStartDateAfter(requestedStartDate).toString("MM/dd/yyyy");
        		}
        		else {
        			earliestStart = product.getEarliestPossibleStartDate().toString("MM/dd/yyyy");
        		}
        		
    	        sb.append("<datevalid>false</datevalid>");
    	        sb.append("<nextValidDate>");
    	        sb.append(earliestStart);
    	        sb.append("</nextValidDate>");
    	        sb.append("<message>");
        		sb.append(ue.getMessage()).append("</message>");
        		
        	}
	        	
    	    sb.append("</pubDateValid>");
    	    
            response.setContentType("text/xml"); 
            response.setHeader("Cache-Control", "no-cache"); 
            response.getWriter().write(sb.toString()); 
            response.getWriter().close();
    
        } catch (Exception e) {
    
            try {
                if (response.isCommitted() == false) {
            	    
                    sb = new StringBuilder("<pubDateValid><datevalid>false</datevalid><nextValidDate>");
                    sb.append(" ").append("</nextValidDate><message>We are unable to verify the specified publication date. It is likely too far into the future.</message></pubDateValid>");
                    response.setContentType("text/xml"); 
                    response.setHeader("Cache-Control", "no-cache"); 
                    response.getWriter().write(sb.toString()); 
                    response.getWriter().close();
                }
            }catch (Exception exp) {
                System.out.println("ActionValidateSubscriptionStartDate() - Failed to validate pub date. Original Exception: " + e.getMessage() + " \nSecondary Exception: " + exp.getMessage());
            }
        }
    
        // Finish with
        return (forward);
    }
 
}
