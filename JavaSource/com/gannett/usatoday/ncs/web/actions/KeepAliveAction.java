package com.gannett.usatoday.ncs.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @version 	1.0
 * @author
 */
public class KeepAliveAction extends Action

{

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        //ActionForward forward = new ActionForward(); // return value
        ActionForward forward = null; // return value

        //if (true) {
        //    System.out.println("Keeping Alive Sesion ID: " + request.getSession().getId());
        //}
        try {

        	StringBuffer sb = new StringBuffer("<SessionKeepAlive>true</SessionKeepAlive>");

        	try {
        	    response.setContentType("text/xml"); 
        	    response.setHeader("Cache-Control", "no-cache"); 
        	    response.getWriter().write(sb.toString()); 
        	    response.getWriter().close();
        	}
        	catch (Exception e) {
        		System.out.println("NAT CS WEB (keep alive):" + e.getMessage());
        	}

        } catch (Exception e) {

            // Report the error using the appropriate name and ID.
            //errors.add("name", new ActionError("id"));

        }

        // Finish with return null
        return (forward);

    }
}
